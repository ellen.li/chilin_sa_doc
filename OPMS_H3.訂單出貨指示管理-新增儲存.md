# 訂單出貨指示管理-新增儲存
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:
新增訂單出貨指示管理的儲存功能


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230621   | 新增規格 | shawn  |
| 20230719   | 移除opDataFlag參數 | shawn  |
| 20230907   | 加回opDataFlag參數 | shawn  |
| 20230921 | 新增LOG說明 | Nick   |

## 來源URL及資料格式

| 項目   | 說明         |
| ------ | ------------ |
| URL    | /sis/save_so |
| method | post         |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             | 名稱     | 資料型別 | 必填  | 資料儲存 & 說明 |
| ---------------- | -------- | :------: | :---: | --------------- |
| opSoNo           | 銷售單號 |  string  |   M   |
| opOrganizationId | 工廠別   |  string  |   M   |
| opCustNo         | 客戶代號 |  string  |   O   |
| opCustName       | 客戶簡稱 |  string  |   O   |
| opCustPoNo       | 客戶po   |  string  |   O   |
| opChannel        | channel  |  string  |   O   |
| opOrderList      | 訂單資料 |  array   |   M    |

### 【opOrderList】array
| 欄位         | 名稱     | 資料型別 | 必填  | 資料儲存 & 說明 |
| ------------ | -------- | -------- | ---  | --------------- |
| opSoSeq      | 項次     | string   | M |soSeq           |
| opItemNo     | 料號     | string   | O |itemNo          |
| opItemDesc   | 料號摘要 | string   | O |itemDesc        |
| opCustItemNo | 客戶料號 | string   | O |custItemNo      |
| opQty        | 數量     | string   | O |qty             |
| opColor      | 色相     | string   | O |color           |
| opMtrName    | 原料     | string   | O |mtrName         |
| opPatternNo  | 色號     | string   | O |patternNo       |
| opDeep       | 厚度     | string   | O |deep            |
| opWidth      | 寬度     | string   | O|width           |
| opLength     | 長度     | string   | O |length          |
| opSpec       | 規格     | string   | O |spec            |
| opSisFlag |  前端傳值 | string | O |Y|
| opDataFlag | 前端傳值 | string | O |N/D/M |
#### Request 範例

```json
{
  "opSoNo":"xxx123",
  "opOrganizationId":"5000",
  "opCustNo":"277723",
  "opCustName":"xx",
  "opCustPoNo":"OB123456",
  "opChannel":"20",
  "opOrderList":[
    {
      "opSoSeq":"1",
      "opItemNo":null,  
      "opItemDesc":null,
      "opCustItemNo":null,
      "opQty":"100",
      "opColor":null,
      "opMtrName":null,
      "opPatternNo":null,
      "opDeep":null,     
      "opWidth":null,    
      "opLength":null,   
      "opSpec":null,
      "opSisFlag":null,
      "opDataFlag":"D"
    },
    ...
  ]
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性

* 檢查訂單是否存在，查詢sql-1，有存在執行sql-2，無執行sql-3
* 以下 opOrderList 迴圈執行
* 查詢sql-5是否存在，
  * 有存在
    * if {opDataFlag}等於null或不等於"D"，執行sql-6
    * else 執行sql-7,8
  * else
    * 執行sql-9
    * 查詢sql-10，如果{opPatternNo} 不等於null且查詢結果=false，執行sql-11
    * 查詢sql-12，如果查詢結果為true執行sql-13，為false執行sql-14
* 上述過程如有執行任何資料庫任何異動，執行 SQL-LOG 紀錄 LOG，{DATAKEY} = {opSoNo}
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容
  
# Request 後端邏輯說明
sql-1
```sql 
-- SoQyeryDao.checkSoHexist
SELECT COUNT(*) FROM SO_H A WHERE SO_NO={opSoNo}
```
sql-2
```sql
-- SoModifyDao.updateSoH
UPDATE SO_H 
SET CUST_NO={opCustNo}, 
  CUST_NAME={opCustName},
  CUST_PO_NO={opCustPoNo}, 
  CHANNEL={opChannel},
  UDT=GETDATE(), 
  UPDATE_BY={登入者使用者ID}	
WHERE SO_NO={opSoNo}
```
sql-3
```sql
-- SoModifyDao.insertSoH
INSERT INTO SO_H(
  SO_NO, 
  ORGANIZATION_ID, 
  CUST_NO,
  CUST_NAME,
  CUST_PO_NO,
  CHANNEL,
  ERP_FLAG, 
  SIS_FLAG, 
  CDT,
  CREATE_BY, 
  UDT,
  UPDATE_BY
) 
VALUES(
  {opSoNo},
  {opOrganizationId},
  {opCustNo}, 
  {opCustName},
  {opCustPoNo},
  {opChannel},
  'N',
  'N',
  GETDATE(), 
  {登入者使用者ID},
  GETDATE(), 
  {登入者使用者ID}
) 
```
## opOrderList 迴圈執行
sql-5
```sql
-- SoQueryDao.checkSoDExist
SELECT COUNT(*) FROM SO_D A 
WHERE SO_NO={opSoNo} 
AND SO_SEQ={opSoSeq}
```

sql-6
```sql
-- SoModifyDao.updateSoD
UPDATE SO_D SET 
  ITEM_NO={opItemNo},
  ITEM_DESC={opItemDesc},
  CUST_ITEM_NO={opCustItemNo}, 
  QTY={opQty},
  PATTERN_NO={opPatternNo},
  COLOR={opColor},
  MTR_NAME={opMtrName},
  DEEP={opDeep},
  WIDTH={opWidth},
  LENGTH={opLength},
  SPEC={opSpec},
  LBL_PATTERN_NO=null, 
  LBL_COLOR=null, 
  LBL_MTR_NAME=null, 
  LBL_DEEP=null, 
  LBL_WIDTH=null, 
  LBL_LENGTH=null, 
  LBL_SPEC=null, 
  SIS_FLAG={opSisFlag},
  ERP_FLAG = null,
  UDT=GETDATE(),
  UPDATE_BY={登入者使用者ID} 
 WHERE SO_NO={opSoNo} AND SO_SEQ={opSoSeq}
```
sql-7
```sql
--SisSoModifyDao.deleteSisSoL
DELETE SO_SIS WHERE SO_NO={opSoNo} AND SO_SEQ={opSoSeq}
```
sql-8
```sql
--SisSoModifyDao.deleteSoD
DELETE SO_D WHERE SO_NO={opSoNo} AND SO_SEQ={opSoSeq}
```

sql-9
```sql
--SoModifyDao.insertSoD
INSERT INTO SO_D(
  SO_NO, 
  SO_SEQ, 
  STATUS, 
  ITEM_NO, 
  ITEM_DESC, 
  CUST_ITEM_NO,
  QTY, 
  PATTERN_NO, 
  COLOR, 
  MTR_NAME, 
  DEEP, 
  WIDTH, 
  LENGTH, 
  SPEC, 
  LBL_PATTERN_NO, 
  LBL_COLOR, 
  LBL_MTR_NAME, 
  LBL_DEEP, 
  LBL_WIDTH, 
  LBL_LENGTH, 
  LBL_SPEC, 
  DD_CUSINV,
  ERP_FLAG,
  SIS_FLAG, 
  CDT, 
  CREATE_BY, 
  UDT, 
  UPDATE_BY) 
VALUES(
  {opSoNo}, 
  {opSoSeq},
  'Y', 
  {opItemNo},
  {opItemDesc},
  {opCustItemNo},
  {opQty},
  {opPatternNo},
  {opColor},
  {opMtrName},
  {opDeep},
  {opWidth},
  {opLength},
  {opSpec},
  null,null,null,null,null,null,null,null,null,
  {opSisFlag},
  GETDATE(), 
  {登入者使用者ID}, 
  GETDATE(), 
  {登入者使用者ID}
)

```
sql-10
```sql
--PatternQueryDao.checkPatternExist
SELECT COUNT(*) CNT FROM PAT_H A WHERE PATTERN_NO={opPatternNo}
```   
sql-11
```sql
--PatternModifyDao.insertPattern
INSERT INTO PAT_H (
  PATTERN_NO,
  CUST_NO,
  CUST_NAME,
  COLOR,
  MTR_TYPE,
  REMARK,
  CDT,
  CREATE_BY,
  UDT,
  UPDATE_BY)
VALUES(
  {opPatternNo},
  null,
  'NA',
  {opColor},
  'NA',
  null,
  GETDATE(),
  {登入者使用者ID},
  GETDATE(),
  {登入者使用者ID}
)
```

sql-12
```sql
--ItemQueryDao.checkItemExist
SELECT COUNT(*) CNT FROM ITEM_H A WHERE ITEM_NO={opItemNo}
```
sql-13
```sql
--ItemModifyDao.updateItem
UPDATE ITEM_H SET PATTERN_NO={opPatternNo}, --{opPatternNo}判斷如為null給'NA'
ITEM_DESC={opItemDesc},
MTR_NAME={opMtrName},
UDT=GETDATE(),
UPDATE_BY= {登入者使用者ID}
WHERE ITEM_NO={opItemNo}
```
sql-14
```sql
--ItemModifyDao.insertItem
INSERT INTO ITEM_H (
  ITEM_NO,
  PATTERN_NO,
  ITEM_DESC,
  UNIT,
  WEIGHT,
  PACKING_WEIGHT,
  CMDI_NO,
  CMDI_NAME,
  MTR_NAME,
  CDT,
  CREATE_BY,
  UDT,
  UPDATE_BY)
VALUES(
  {opItemNo},
  {opPatternNo},--{opPatternNo}判斷如為null給'NA'
  {opItemDesc},
  'KG',
  '1',
  null,
  null,
  null,
  {opMtrName},  
  GETDATE(),
  {登入者使用者ID},
  GETDATE(),
  {登入者使用者ID}
)
```
SQL-LOG:
```sql
INSERT INTO  SYS_MODI_LOG (CATEGORY,DATAKEY,USERID,USER_NAME,LOG) VALUES ('SISSOMANAGE',{DATAKEY},{登入者使用者ID},{登入者使用者名稱}, '訂單比對帶入')
```


# Response 欄位
| 欄位        | 名稱     | 資料型別 | 資料儲存 & 說明 |
| ----------- | -------- | -------- | --------------- |
| opSoNo    | 銷售單號 | string   |           |

#### Response 範例

```json
{
  "msgCode": null,
  "result":{
    "content":{
        "opSoNo":"277723"       
    }
  }
}
```

## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考

* SisSoManageAction.saveSo
