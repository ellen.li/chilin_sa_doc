# 報表-色號銷售統計表-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)


**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230821 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /report/pattern_sales_list  |
| method | post                        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱    |  資料型別 | 必填 | 資料儲存 & 說明  |
|--------------|---------|----------|:----:|-----------------|
| opYear       | 年      | int      |  M   |                 |
| opMonth      | 月      | int      |  M   |                 |
| opPatternNo  | 色號    | string   |  M   |                 |

#### Request 範例

```json
{
  "opYear": "2020",
  "opMonth": "12",
  "opPatternNo": "FG-0205B"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 取 { 登入者工廠別 } SYS_USER.ORGANIZATION_ID 欄位
* 參考SQL-1取得指定年月之資料
  * opSum1 ~ opSum12 轉為千分位整數
  * sum(量)和次(qty) 若為 0 時則不顯示，改回傳null
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
-- ReportQueryDao.getPatternSalesReport
SELECT I.PATTERN_NO, H.CUST_NAME,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '01' THEN H.RAW_QTY ELSE 0 END),0)) AS SUM1,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '02' THEN H.RAW_QTY ELSE 0 END),0)) AS SUM2,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '03' THEN H.RAW_QTY ELSE 0 END),0)) AS SUM3,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '04' THEN H.RAW_QTY ELSE 0 END),0)) AS SUM4,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '05' THEN H.RAW_QTY ELSE 0 END),0)) AS SUM5,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '06' THEN H.RAW_QTY ELSE 0 END),0)) AS SUM6,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '07' THEN H.RAW_QTY ELSE 0 END),0)) AS SUM7,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '08' THEN H.RAW_QTY ELSE 0 END),0)) AS SUM8,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '09' THEN H.RAW_QTY ELSE 0 END),0)) AS SUM9,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '10' THEN H.RAW_QTY ELSE 0 END),0)) AS SUM10,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '11' THEN H.RAW_QTY ELSE 0 END),0)) AS SUM11,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '12' THEN H.RAW_QTY ELSE 0 END),0)) AS SUM12,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '01' THEN 1 ELSE 0 END),0)) AS QTY1,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '02' THEN 1 ELSE 0 END),0)) AS QTY2,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '03' THEN 1 ELSE 0 END),0)) AS QTY3,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '04' THEN 1 ELSE 0 END),0)) AS QTY4,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '05' THEN 1 ELSE 0 END),0)) AS QTY5,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '06' THEN 1 ELSE 0 END),0)) AS QTY6,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '07' THEN 1 ELSE 0 END),0)) AS QTY7,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '08' THEN 1 ELSE 0 END),0)) AS QTY8,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '09' THEN 1 ELSE 0 END),0)) AS QTY9,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '10' THEN 1 ELSE 0 END),0)) AS QTY10,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '11' THEN 1 ELSE 0 END),0)) AS QTY11,
  CONVERT(float,ROUND(SUM(CASE WHEN  CONVERT(VARCHAR(2), WO_DATE, 1) = '12' THEN 1 ELSE 0 END),0)) AS QTY12,
FROM WO_H H
INNER JOIN BOM_MTM B ON H.BOM_UID = B.BOM_UID
INNER JOIN ITEM_H I ON B.ITEM_NO = I.ITEM_NO
WHERE H.WO_DATE >= '{ opYear }/01/01'
AND H.ORGANIZATION_ID = { 登入者工廠別 }
AND I.PATTERN_NO = { opPatternNo }

-- opMonth = 12
AND H.WO_DATE <= '{ opYear }/12/31'
-- opMonth != 12
AND H.WO_DATE < '{ opYear }/{ opMonth + 1 }(若為個位數要補0至兩位)/01'

GROUP BY I.PATTERN_NO, H.CUST_NAME
ORDER BY I.PATTERN_NO, H.CUST_NAME
```

# Response 欄位
| 欄位          | 名稱       | 資料型別 | 資料儲存 & 說明         |
|---------------|-----------|---------|------------------------|
| opPatternNo   | 色號       | string  | ITEM_H.PATTERN_NO      |
| opCustName    | 客戶       | string  | WO_H.CUST_NAME         |
| opSum1        | 一月(量)   | string  | SUM1 千分位整數         |
| opSum2        | 二月(量)   | string  | SUM2 千分位整數         |
| opSum3        | 三月(量)   | string  | SUM3 千分位整數         |
| opSum4        | 四月(量)   | string  | SUM4 千分位整數         |
| opSum5        | 五月(量)   | string  | SUM5 千分位整數         |
| opSum6        | 六月(量)   | string  | SUM6 千分位整數         |
| opSum7        | 七月(量)   | string  | SUM7 千分位整數         |
| opSum8        | 八月(量)   | string  | SUM8 千分位整數         |
| opSum9        | 九月(量)   | string  | SUM9 千分位整數         |
| opSum10       | 十月(量)   | string  | SUM10 千分位整數        |
| opSum11       | 十一月(量) | string  | SUM11 千分位整數        |
| opSum12       | 十二月(量) | string  | SUM12 千分位整數        |
| opQty1        | 一月(次)   | int     | QTY1                   |
| opQty2        | 二月(次)   | int     | QTY2                   |
| opQty3        | 三月(次)   | int     | QTY3                   |
| opQty4        | 四月(次)   | int     | QTY4                   |
| opQty5        | 五月(次)   | int     | QTY5                   |
| opQty6        | 六月(次)   | int     | QTY6                   |
| opQty7        | 七月(次)   | int     | QTY7                   |
| opQty8        | 八月(次)   | int     | QTY8                   |
| opQty9        | 九月(次)   | int     | QTY9                   |
| opQty10       | 十月(次)   | int     | QTY10                  |
| opQty11       | 十一月(次) | int     | QTY11                  |
| opQty12       | 十二月(次) | int     | QTY12                  |

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content": [
      {
        "opPatternNo": "FG-0205B",
        "opCustName": "佛山群志",
        "opSum1": "2,225",
        "opSum2": "19,039",
        "opSum3": "8,850",
        "opSum4": "12,000",
        "opSum5": "9,000",
        "opSum6": null,
        "opSum7": "45,226",
        "opSum8": "18,175",
        "opSum9": null,
        "opSum10": null,
        "opSum11": null,
        "opSum12": null,
        "opQty1": "1",
        "opQty2": "2",
        "opQty3": "1",
        "opQty4": "1",
        "opQty5": "1",
        "opQty6": null,
        "opQty7": "5",
        "opQty8": "2",
        "opQty9": null,
        "opQty10": null,
        "opQty11": null,
        "opQty12": null,
      },
      {
        "opPatternNo": "FG-0205B",
        "opCustName": "兆笠",
        "opSum1": null,
        "opSum2": null,
        "opSum3": null,
        "opSum4": null,
        "opSum5": null,
        "opSum6": null,
        "opSum7": "900",
        "opSum8": null,
        "opSum9": null,
        "opSum10": null,
        "opSum11": null,
        "opSum12": null,
        "opQty1": null,
        "opQty2": null,
        "opQty3": null,
        "opQty4": null,
        "opQty5": null,
        "opQty6": null,
        "opQty7": "1",
        "opQty8": null,
        "opQty9": null,
        "opQty10": null,
        "opQty11": null,
        "opQty12": null,
      },
      {
        "opPatternNo": "FG-0205B",
        "opCustName": "博原",
        "opSum1": "650",
        "opSum2": "900",
        "opSum3": "3600",
        "opSum4": null,
        "opSum5": "6000",
        "opSum6": null,
        "opSum7": null,
        "opSum8": null,
        "opSum9": null,
        "opSum10": null,
        "opSum11": null,
        "opSum12": null,
        "opQty1": "1",
        "opQty2": "1",
        "opQty3": "1",
        "opQty4": null,
        "opQty5": "1",
        "opQty6": null,
        "opQty7": null,
        "opQty8": null,
        "opQty9": null,
        "opQty10": null,
        "opQty11": null,
        "opQty12": null,
      }
    ]
  }
}
```