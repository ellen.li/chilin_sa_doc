# 報表-色號銷售統計表-匯出excel
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)


**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230821 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /report/pattern_sales_list_excel  |
| method | post                        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱    |  資料型別 | 必填 | 資料儲存 & 說明 |
|--------------|---------|----------|:----:|-----------------|
| 原列表REQUEST參數 |     |          |      |                 |

# Request 後端流程說明

* 參考[OPMS_J8.報表-色號銷售統計表-列表 /report/pattern_sales_list](./OPMS_J8.報表-色號銷售統計表-列表.md)，用相同方式取得資料。
  * sum(量)和次(qty) 為 0 時不必轉null
* 將資料轉為excel表格匯出，不需設定檔名
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

參考[OPMS_J8.報表-色號銷售統計表-列表 /report/pattern_sales_list](./OPMS_J8.報表-色號銷售統計表-列表.md)

# Excel 欄位

# Response 欄位

| 欄位      | 資料型別 | 資料儲存 & 說明         |
|-----------|---------|------------------------|
| 色號       | string  | ITEM_H.PATTERN_NO      |
| 客戶       | string  | WO_H.CUST_NAME         |
| 一月(量)   | string  | SUM1 千分位整數         |
| 二月(量)   | string  | SUM2 千分位整數         |
| 三月(量)   | string  | SUM3 千分位整數         |
| 四月(量)   | string  | SUM4 千分位整數         |
| 五月(量)   | string  | SUM5 千分位整數         |
| 六月(量)   | string  | SUM6 千分位整數         |
| 七月(量)   | string  | SUM7 千分位整數         |
| 八月(量)   | string  | SUM8 千分位整數         |
| 九月(量)   | string  | SUM9 千分位整數         |
| 十月(量)   | string  | SUM10 千分位整數        |
| 十一月(量) | string  | SUM11 千分位整數        |
| 十二月(量) | string  | SUM12 千分位整數        |
| 一月(次)   | int     | QTY1                   |
| 二月(次)   | int     | QTY2                   |
| 三月(次)   | int     | QTY3                   |
| 四月(次)   | int     | QTY4                   |
| 五月(次)   | int     | QTY5                   |
| 六月(次)   | int     | QTY6                   |
| 七月(次)   | int     | QTY7                   |
| 八月(次)   | int     | QTY8                   |
| 九月(次)   | int     | QTY9                   |
| 十月(次)   | int     | QTY10                  |
| 十一月(次) | int     | QTY11                  |
| 十二月(次) | int     | QTY12                  |