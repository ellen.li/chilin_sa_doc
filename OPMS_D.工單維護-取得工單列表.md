# 工單維護-取得工單列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得工單列表

## 修改歷程

| 修改時間 | 內容                               | 修改者 |
|----------|----------------------------------|--------|
| 20230720 | 新增規格                           | Ellen  |
| 20230726 | 調整Response                       | Ellen  |
| 20230802 | 調整排序方式                       | Ellen  |
| 20230809 | 回傳增加銷售數量                   | Ellen  |
| 20230822 | API不做分頁，由前端控，SAP最多取1000 | Nick   |



## 來源URL及資料格式

| 項目   | 說明     |
| ------ | -------- |
| URL    | /wo/list |
| method | post     |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。  
| 欄位         | 名稱         | 資料型別 | 必填  | 來源資料 & 說明                                                                       |
| ------------ | ------------ | -------- | :---: | ------------------------------------------------------------------------------------- |
| opPatternNo  | 色號         | string   |   O   | 預設 `null`，模糊搜尋                                                                 |
| opItemNo     | 料號         | string   |   O   | 預設 `null`，模糊搜尋                                                                 |
| opWoNo       | 工單號碼     | string   |   O   | 預設 `null`，模糊搜尋                                                                 |
| opActiveFlag | 狀態         | string   |   O   | 預設 `null`，CRTD: 已開立未核發 / REL: 已核發未結案 / TECO: 結案 / DLFL: 已上刪除旗標 |
| opCustPoNo   | 客戶po       | string   |   O   | 預設 `null`，模糊搜尋                                                                 |
| opBDate      | 開始時間     | string   |   O   | 預設 `null`   yyyy/MM/dd                                                             |
| opEDate      | 結束時間     | string   |   O   | 預設 `9999/12/31`   yyyy/MM/dd                                                        |


#### Request 範例

```json
{
  "opPatternNo":null,
  "opItemNo":null,
  "opWoNo":null,
  "opActiveFlag":"CRTD",
  "opBDate":null,
  "opEDate":null
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* {organizationId} 為登入者工廠別ID
* 使用前端給的條件，參考 SAP-1 自組【搜尋條件】，如為模糊搜尋條件加上`*`號，如為 null 代表不需組條件
* {offset} 為起始列印 row ，第一頁為 0，第二頁後為 0 + (opPageSize*(opPage-1))
* 參考 SAP-1 查回結果 result(只取1000筆)
* 參考 SQL-1取得opms維護資料(只取第一筆)
* 參考 SQL-2 確認工單 BOM_NO 是否存在OPMS，不存在的不允許編輯
* 依照 Response 回傳資料
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SAP-1：
可參考舊版 WoQueryRfc_getWoQueryResultList
程序如有任何異常，紀錄錯誤LOG並回傳NULL

**獲得BAPI方法**
|              | 型態   | 參數值                     | 說明 |
| ------------ | ------ | -------------------------- | ---- |
| BAPIFunction | string | Z_PP_OPMS_PRODUCTION_ORDER |      |

**設定輸入參數**
| 參數名稱   | 參數值           | 型態         | 說明                                   |
| ---------- | ---------------- | ------------ | -------------------------------------- |
| I_WERKS    | {organizationId} | string       |                                        |
| I_STATUS   | {opActiveFlag}   | string       | 如{opActiveFlag}非 null 則加入以下參數 |
| I_AUFNR    | {opWoNo}         | string       | 如{opWoNo}非 null 則加入以下參數       |
| I_MATNR    | {opItemNo}       | string       | 如{opItemNo}非 null 則加入以下參數     |
| I_COLOR_NO | {opPatternNo}    | string       | 如{opPatternNo}非 null 則加入以下參數  |
| I_CUSTPO   | {opCustPoNo}     | string       | 如{opCustPoNo}非 null 則加入以下參數   |
| I_ERDAT    |                  | JCoStructure |                                        |

I_ERDAT JCoStructure 參數
| 參數名稱 | 參數值    | 型態   | 說明 |
| -------- | --------- | ------ | ---- |
| ERDAT_FR | {opBDate} | string | 轉為 yyyyMMdd |
| ERDAT_TO | {opEDate} | string | 轉為 yyyyMMdd |


| 取得回傳表格名稱 |
| ---------------- |
| T_ORDER          |

| SAP欄位名稱 | SAP資料型別 | 欄位值   | 說明                               |
| ----------- | :---------: | -------- | ---------------------------------- |
| AUFNR       |   string    | 工單號碼 | WoNo，過濾前綴0                    |
| STATUS      |   string    | 狀態     | activeFlag                         |
| GSTRP       |    Date     | 使用日期 | date                               |
| COLOR       |   string    | 顏色     |                                    |
| COLOR_NO    |   string    | 色號     | patternNo,優先以opms patternNo為主 |
| MATNR       |   string    | 料號     | itemNo                             |
| STLAL       |   string    | BOM      | bomNo                              |
| GRADE       |   string    | 原料     | gradeNo                            |
| STAND_T     |   string    | 生產單位 | produnitName                       |
| SORT2       |   string    | 客戶     | custName                           |
| IHREZ       |   string    | 間接客戶 | indirCust                          |
| GAMNG       |   double    | 工單數量 | woQty，取至小數點後第三位          |
| CUSTPO      |   string    | 客戶PO   | custPoNo                           |
| SOQTY       |   string    | 銷售數量 | salesQty                           |


SQL-1:

```sql
SELECT
	H.WO_UID,
	H.WO_SEQ,
	H.RAW_QTY,
	H.WO_NO,
	I.PATTERN_NO,
	S.ASS_DESC OS_LOT,
	O.CUST_ITEM_NO
FROM
	WO_H H
	INNER JOIN BOM_MTM B ON H.BOM_UID= B.BOM_UID
	INNER JOIN ITEM_H I ON B.ITEM_NO= I.ITEM_NO
	LEFT JOIN SO_D O ON H.SALES_ORDER_NO = O.SO_NO AND H.SALES_ORDER_SEQ = O.SO_SEQ
	LEFT JOIN ( SELECT DISTINCT WO_UID, ASS_DESC FROM WO_SIS WHERE LIST_HEADER_ID = '159' ) S ON H.WO_UID= S.WO_UID 
WHERE
	WO_NO = {SAP-1.AUFNR}
```

SQL-2 確認 BOM_NO 是否存在OPMS
```sql
SELECT
	BOM_UID 
FROM
	BOM_MTM 
WHERE
	ITEM_NO = {itemNo} 
	AND ORGANIZATION_ID = {organizationId}
	AND BOM_NO = {bomNo}
```

# Response 欄位
#### 【content】
| 欄位           | 名稱              | 資料型別 | 來源資料 & 說明                         |
| -------------- | ----------------- | -------- | --------------------------------------- |
| opWoSeq        | 序號              | string   | H.WO_SEQ(SQL-1)，不存在填0              |
| opWoUid        | opms工單ID (維護) | string   | H.WO_UID(SQL-1)，不為空代表存在opms維護 |
| opActiveFlag   | 狀態              | string   | STATUS(SAP-1)                           |
| opPatternNo    | 色號              | string   | I.PATTERN_NO(SQL-1)                     |
| opItemNo       | 料號              | string   | MATNR(SAP-1)                            |
| opWoNo         | 工單號碼          | string   | AUFNR(SAP-1)，過濾前綴0                 |
| opProdunitName | 生產單位          | string   | STAND_T(SAP-1)                          |
| opOsLot        | 委外標籤LOT       | string   | OS_LOT(SQL-1)                           |
| opGradeNo      | 原料 (gradeNo)    | string   | GRADE(SAP-1)                            |
| opBomNo        | BOM代碼           | string   | STLAL(SAP-1)                            |
| opWoQty        | 工單數量          | string   | GAMNG(SAP-1)，取至小數點後第三位        |
| opRawQty       | 投入原料量        | string   | H.RAW_QTY(SQL-1)，取至小數點後第二位    |
| opSalesQty     | 銷售數量          | string   | SOQTY(SAP-1)，取至小數點後第五位        |
| opCustName     | 客戶              | string   | SORT2(SAP-1)                            |
| opCustPoNo     | 客戶po            | string   | CUSTPO(SAP-1)                           |
| opIndirCust    | 間接客戶          | string   | IHREZ(SAP-1)                            |
| opOpmsHasBom   | Bom是否存在opms   | string   | (SQL-2)，Y/N                            |
| opDate         | 使用日期          | string   | GSTRP(SAP-1) (YYYY/MM/DD)               |


#### Response 範例

```json
{
    "msgCode": null,
    "result": {
      "content": [
        {
          "opWoSeq": "31",
          "opWoUid": "01B9E3DF-68E5-4A3C-BDD1-84F8EA308C7B",
          "opActiveFlag": "REL",
          "opPatternNo": "H17307B6",
          "opItemNo": "L80NXXXMAX25H16548",
          "opWoNo": "58028534",
          "opProdunitName": "染色課",
          "opOsLot": "0T53Y075",
          "opGradeNo": "GA-1535",
          "opBomNo": "10",
          "opWoQty": "50.623",
          "opRawQty": "275.2",
          "opCustName": "奇美實業",
          "opCustPoNo": "5500040161",
          "opIndirCust": "",
          "opOpmsHasBom": "Y",
          "opDate": "2016/06/13"
        }
      ]
    }
}
```

