# 產量回報-取得回報紀錄by工單
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得回報紀錄by工單


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230830 | 新增規格 | Ellen  |

## 來源URL及資料格式

| 項目   | 說明                   |
| ------ | ---------------------- |
| URL    | /psr/list_report_by_wo |
| method | post                   |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位   | 名稱     | 資料型別 | 必填  | 資料儲存 & 說明 |
| ------ | -------- | :------: | :---: | --------------- |
| opWoNo | 工單號碼 |  string  |   M   |                 |

#### Request 範例

```json
{
  "opWoNo": "52019203"
}
```

# Request 後端流程說明

可參考舊版 PsManageAction.queryReportListForWo
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 依傳入條件查詢sql-1，回傳列表資料
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
SELECT
  R.PSR_UID,
  R.EQUIP_ID,
  E.EQUIP_NAME,
  CONVERT(VARCHAR (10), R.REP_MFG_DATE, 111) REP_MFG_DATE,
  R.REP_SHIFT_CODE,
  CASE R.REP_SHIFT_CODE WHEN 'A' THEN '早'
  WHEN 'B' THEN '中'
  WHEN 'C' THEN '晚'
  ELSE NULL END REP_SHIFT_CODE_NAME,
  R.WO_UID,
  R.REP_SHIFT_HOURS,
  R.REP_TIME_LINE,
  R.REP_MIN_MFG_QTY,
  R.SHIFT_MFG_QTY,
  R.SHIFT_PRE_QTY,
  R.REP_CTN_DESC,
  R.WRN_FLAG,
  R.WRN_REMARK,
  R.ERP_FLAG 
FROM
  PS_R R
  INNER JOIN EQUIP_H E ON R.EQUIP_ID= E.EQUIP_ID
  INNER JOIN PRODUNIT U ON E.ORGANIZATION_ID= U.ORGANIZATION_ID AND E.PRODUNIT_NO= U.PRODUNIT_NO
  INNER JOIN WO_H W ON R.WO_UID= W.WO_UID 
WHERE
  W.WO_NO = {opWoNo}
ORDER BY
  R.EQUIP_ID, R.REP_MFG_DATE, R.REP_SHIFT_CODE, R.REP_TIME_LINE
```

# Response 欄位
| 欄位               | 名稱        | 資料型別 | 資料儲存 & 說明                       |
| ------------------ | ----------- | -------- | ------------------------------------- |
| opPsrUid           | 產量回報UID | string   | PSR_UID(SQL-1)                        |
| opEquipId          | 機台代號    | string   | EQUIP_ID(SQL-1)                       |
| opEquipName        | 機台名稱    | string   | EQUIP_NAME(SQL-1)                     |
| opRepMfgDate       | 日期        | string   | REP_MFG_DATE(SQL-1)                   |
| opRepShiftCode     | 班別代碼    | string   | REP_SHIFT_CODE(SQL-1)                 |
| opRepShiftCodeName | 班別名稱    | string   | REP_SHIFT_CODE_NAME(SQL-1)            |
| opWoUid            | 工單ID      | string   | WO_UID(SQL-1)                         |
| opRepShiftHours    | 時數        | string   | REP_SHIFT_HOURS(SQL-1)                |
| opRepTimeLine      | 期間        | string   | REP_TIME_LINE(SQL-1)                  |
| opRepMinMfgQty     | 吐出量      | string   | REP_MIN_MFG_QTY(SQL-1)                |
| opShiftMfgQty      | 實際產量    | string   | SHIFT_MFG_QTY(SQL-1)                  |
| opShiftPreQty      | 預染數量    | string   | SHIFT_PRE_QTY(SQL-1)                  |
| opRepCtnDesc       | 桶次        | string   | REP_CTN_DESC(SQL-1)                   |
| opWrnFlag          | 是否為異常  | string   | WRN_FLAG(SQL-1)    Y/N  Y: 是 / N: 否 |
| opWrnRemark        | 異常回報    | string   | WRN_REMARK(SQL-1)                     |
| opErpFlag          | 是否拋轉SAP | string   | ERP_FLAG(SQL-1)   Y/N  Y: 是 / N: 否  |


#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":[
      {
        "opPsrUid": "E07128B8-DC16-456F-A8E1-3EC089ADFFDA",
        "opEquipId": "1",
        "opEquipName": "染色#1",
        "opRepMfgDate": "2022/11/28",
        "opRepShiftCode": "C",
        "opRepShiftCodeName": "晚",
        "opWoUid": "9B1217A6-0A2A-4BB2-9477-61EB59700090",
        "opRepShiftHours": "5",
        "opRepTimeLine": "02:00~07:00",
        "opRepMinMfgQty": "5.65",
        "opShiftMfgQty": "1600",
        "opShiftPreQty": "",
        "opRepCtnDesc": "1~3",
        "opWrnFlag": "N",
        "opWrnRemark": "改善拖痕 黑點中",
        "opErpFlag": "Y",
      }
    ]
  }
}
```