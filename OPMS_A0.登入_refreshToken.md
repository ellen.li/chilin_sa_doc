# 使用者登入-refreshToken

###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:


更新 refresh Token 使用

## 修改歷程
| 修改時間 | 內容 | 修改者 |
| ---- | ---- | -------- |
| 20230323 | 新增規格 | Nick |
| 20230515 | 因JWT驗證已會驗證使用者資訊，這邊移除驗證 | Nick|


## 來源URL及資料格式
| 項目   | 說明                |
| ------ | ------------------ |
| URL    | /sso/refreshToken   |
| method | get               |
## Header 定義

|Header Key  | Header Value |說明|
| -| -|-|
| Content-Type| application/json; charset=UTF-8 ||
| Authorization| Bearer refreshToken值 ||
| x-uid  | 後端給的uid ||
## Request 範例
N/A

## Request 後端流程說明
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 return 401)
* 回傳結果：  
* 有系統錯誤 return 500,"SYSTEM_ERROR",，
  正常回傳 200，並將 重新產生的Token & 重新產生的 refreshToken 回傳
* 產生 Token(Bearer Token)
  1. 使用 SYS_USER.USER_ID 重新產生的Token(JWT Token)  
  2. expired 15M
* 產生 refreshToken
  1. 使用 SYS_USER.USER_ID 重新產生refreshToken (JWT Token)
  2. expired 45M
## Request 後端邏輯說明
N/A
## Response 欄位
| 欄位       | 名稱         | 資料型別    | 資料說明            |
| ---------- | ------------ | ----------- | -------------------------- |
| opToken      | token        | string      |   重新產生的Token                         |
| opRefreshToken | refreshToken | string    |  重新產生的refreshToken|
## Response 範例
```json
{
  "msgCode": null, 
  "result":{
    "content": {
      "opToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJfaWQiOjEsInVzZXJfbmFtZSI6IkFuZHkxMCIsInVzZXJfbWFpbCI6ImFuZHlAZ21haWwuY29tIn0sImV4cCI6MTUxNTczODUxOSwiaWF0IjoxNTE1NzM3NjE5fQ.CLPeXhcxl2mdsL6-sUNFHFYABkTxmzx3YxEPyNih_FM",
      "opRefreshToken": "eyAAAhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJfaWQiOjEsInVzZXJfbmFtZSI6IkFuZHkxMCIsInVzZXJfbWFpbCI6ImFuZHlAZ21haWwuY29tIn0sImV4cCI6MTUxNTczODUxOSwiaWF0IjoxNTE1NzM3NjE5fQ.CLPeXhcxl2mdsL6-sUNFHFYABkTxmzx3YxEPyNih_FM"
    }
  }
}
```