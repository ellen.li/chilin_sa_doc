[返回總覽](./OPMS_00.總覽.md)  

登入驗證機制

```flow
st=>start: 開始
e=>end: 結束
op0=>operation: 輸入帳號密碼
op1=>operation: 走密碼驗證
op2=>operation: 走AD驗證
op3=>operation: 前端取得新的 JWT Token跟 refreshToken
op4=>operation: 依API功能實作
op5=>operation: 取得JWT Token 跟 refreshToken
op6=>operation: 401 Unauthorized
ca2=>operation: 員工登入
ca3=>operation: 登入成功回應用戶ID
ck1=>condition: RefreshToken 成功?
ck2=>condition: 驗證帳密成功?
ck3=>condition: 存在 RefreshToken?
ck4=>condition: userid 對應的 ADSite
有設定 AD 主機 ?
ck5=>condition: AD 驗證成功?

st->op0->ck4
ck4(no)->op1->ck2
ck4(yes)->op2->ck5
ck5(yes)->op5
ck5(no,left)->op6->e
ck2(yes,left)->op5->e
ck2(no)->op6->e

```

程式 token 驗證流程

```flow
st=>start: 開始
e=>end: 結束
op0=>operation: 登入驗證機制
op1=>operation: refreshToken 機制
op2=>operation: 依原功能往下
op3=>operation: 401 Unauthorized
ck0=>condition: 確認 URL 為 
/opms/sso/verify?
ck1=>condition: 確認 URL 為
/opms/sso/refreshToken?
ck2=>condition: 解 Token，驗證通過?

st->ck2
ck2(yes)->ck0
ck2(no)->ck0
ck0(no)->ck1
ck0(yes)->op0->e
ck1(yes)->op1->e
ck1(no)->op3->e

```

refreshToken 機制

```flow
st=>start: 開始
e=>end: 結束
op0=>operation: 從 header 擷取 Bearer Token
op1=>operation: 重新取得 refreshToken，寫入 redis.database:1
op2=>operation: 重新取得 Token，寫入 redis.database:0
op3=>operation: 401 Unauthorized
op4=>operation: redis.database:1 刪除舊 refreshtoken
op5=>operation: 回傳 新的 token 跟 refreshToken
ck1=>condition: 解 Token，驗證通過?
ck2=>condition: 從 redis.database:1 
找到 refreshToken?

st->op0->ck1
ck1(yes)->ck2
ck2(no)->op3
ck2(yes)->op4->op1->op2->op5->e
ck1(no)->op3->e



```
------------------------------------------------------------------------
登入流程
```mermaid
graph TD
    A[開始] --> B[輸入帳號密碼]
    B -->C{驗證帳密成功?}
    C -->|是| E[取得JWT Token 跟 refreshToken]
    C -->|否| F[結束]
    E -->F[結束]
```
```mermaid
sequenceDiagram
autonumber
    participant A as 使用者
    participant B as Web(前端)
    participant C as API
    participant D as DB
    participant E as Redis
    A->>B: 使用者查詢
    B->>B: 判斷測試
    B->>C: 打API啦
    C->>D: 查詢資料庫
    D-->>C: 成功
    C->>E:  INSERT Redis
    E-->>C: 成功
    C-->>B: 成功
    B-->>A: 回覆使用者
```

```flow
st=>start: 開始
e=>end: 結束
op0=>operation: 輸入帳號密碼
op1=>operation: 走密碼驗證
op2=>operation: 走AD驗證
op3=>operation: 前端取得新的 JWT Token跟 refreshToken
op4=>operation: 依API功能實作
op5=>operation: 取得JWT Token 跟 refreshToken
ca2=>operation: 員工登入
ca3=>operation: 登入成功回應用戶ID
ck1=>condition: RefreshToken 成功?
ck2=>condition: 驗證帳密成功?
ck3=>condition: 存在 RefreshToken?
ck4=>condition: userid 對應的 ADSite
為 null ?
ck5=>condition: AD 驗證成功?
nodeVar(spec1, spec2)=>conditionaa

st->op0->ck4
ck4(yes)->op1->ck2
ck4(no)->op2->ck5
ck5(yes)->op5
ck5(no)->e
ck2(no)->ck1
ck2(yes,left)->op5->e
ck2(no)->e

```


API 認證流程

```mermaid
graph TD
start[開始] --> op0[OPMS API URL] -->ck2{Token 過期?}
ck2{Token 過期?}--> |是| ck1{RefreshToken 成功?}
ck2{Token 過期?}--> |否| op4[依API功能實作] -->e[結束]
ck1{RefreshToken 成功?}--> |是| op3[前端取得新的 JWT Token跟 refreshToken] 
ck1{RefreshToken 成功?}--> |否| e[結束]
op3[前端取得新的 JWT Token跟 refreshToken] -->op4[依API功能實作]
```


```flow
st=>start: 開始
e=>end: 結束
op0=>operation: OPMS API URL
op3=>operation: 前端取得新的 JWT Token跟 refreshToken
op4=>operation: 依API功能實作
ca2=>operation: 員工登入
ca3=>operation: 登入成功回應用戶ID
ck1=>condition: RefreshToken 成功?
ck2=>condition: Token 過期?
ck3=>condition: 存在 RefreshToken?

st->op0->ck2
ck2(yes)->ck1
ck2(no,left)->op4->e
ck1(yes)->op3(left)->op4
ck1(no)->e
ca3(right)->op0
```

```flow
s=>start: 開始
e=>end: 結束
op1=>operation: 取得 JWT Token & refreshToken
ck1=>condition: 驗證帳密

s=>ck1
```

