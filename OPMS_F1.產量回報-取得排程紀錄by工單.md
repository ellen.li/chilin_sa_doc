# 產量回報-取得排程紀錄by工單
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

產量回報-取得排程紀錄by工單

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230830 | 新增規格 | Ellen  |


## 來源URL及資料格式

| 項目   | 說明                     |
| ------ | ------------------------ |
| URL    | /psr/list_schedule_by_wo |
| method | post                     |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位   | 名稱     | 資料型別 | 必填  | 資料儲存 & 說明 |
| ------ | -------- | :------: | :---: | --------------- |
| opWoNo | 工單號碼 |  string  |   M   |                 |

#### Request 範例

```json
{ 
  "opWoNo": "52019203"
}
```

# Request 後端流程說明

可參考舊版 PsManageAction.QueryScheduleListForWo
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* {organizationId} 為登入者工廠別ID
* 使用前端給的條件，參考 SQL-1 自組【搜尋條件】，如為 null 代表不需組條件
* 參考 SQL-1 查回結果 result
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1: 
```sql
SELECT
  H.PS_UID,
  H.PS_SEQ,
  H.EQUIP_ID,
  E.EQUIP_NAME,
  CONVERT ( VARCHAR ( 10 ), H.MFG_DATE, 111 ) MFG_DATE,
  H.SHIFT_CODE,
  CASE H.SHIFT_CODE WHEN 'A' THEN '早'
  WHEN 'B' THEN '中'
  WHEN 'C' THEN '晚'
  ELSE NULL END SHIFT_CODE_NAME,
  H.WO_UID,
  H.SHIFT_HOURS,
  H.WO_QTY,
  H.SHIFT_STD_QTY,
  H.SHIFT_PLN_QTY,
  H.REMARK 
FROM
  PS_H H
  INNER JOIN EQUIP_H E ON H.EQUIP_ID= E.EQUIP_ID
  INNER JOIN PRODUNIT U ON E.ORGANIZATION_ID= U.ORGANIZATION_ID AND E.PRODUNIT_NO= U.PRODUNIT_NO
  INNER JOIN WO_H W ON H.WO_UID= W.WO_UID 
WHERE
  W.WO_NO = {opWoNo} 
ORDER BY
  H.EQUIP_ID, H.MFG_DATE, H.SHIFT_CODE, H.PS_SEQ
```

# Response 欄位
| 欄位            | 名稱        | 資料型別 | 來源資料 & 說明        |
| --------------- | ----------- | -------- | ---------------------- |
| opPsUid         | 生管排程UID | string   | PS_UID(SQL-1)          |
| opPsSeq         | 序號        | string   | PS_SEQ(SQL-1)          |
| opEquipId       | 機台        | string   | EQUIP_ID(SQL-1)        |
| opEquipName     | 機台名稱    | string   | EQUIP_NAME(SQL-1)      |
| opMfgDate       | 生產日期    | string   | MFG_DATE(SQL-1)        |
| opShiftCode     | 班別代碼    | string   | SHIFT_CODE(SQL-1)      |
| opShiftCodeName | 班別名稱    | string   | SHIFT_CODE_NAME(SQL-1) |
| opWoUid         | 工單ID      | string   | WO_UID(SQL-1)          |
| opShiftHours    | 時數        | string   | SHIFT_HOURS(SQL-1)     |
| opWoQty         | 訂單未做    | string   | WO_QTY(SQL-1)          |
| opShiftStdQty   | 標準產量    | string   | SHIFT_STD_QTY(SQL-1)   |
| opShiftPlnQty   | 預計產量    | string   | SHIFT_PLN_QTY(SQL-1)   |
| opRemark        | 排程說明    | string   | REMARK(SQL-1)          |


#### Response 範例

```json
{
    "msgCode": null,
    "result": {
      "content":[
        {
          "opPsUid": "B77CF423-8BAB-495D-AE4E-597E93C4E775",
          "opPsSeq": "0",
          "opEquipId": "45",
          "opEquipName": "",
          "opMfgDate": "2022/12/05",
          "opShiftCode": "A",
          "opShiftCodeName": "早",
          "opWoUid": "",
          "opShiftHours": "2",
          "opWoQty": "0",
          "opShiftStdQty": "0",
          "opShiftPlnQty": "0",
          "opRemark": ""
        }
      ]
    }
}
```
 