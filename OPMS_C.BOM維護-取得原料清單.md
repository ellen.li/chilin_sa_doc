# BOM維護-取得原料清單
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得MtrList資訊

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230525 | 新增規格 | Tim   |
| 20230530 | 調整邏輯與欄位說明 | Tim   |

## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    | /bom/get_mtr_list | 
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。  
| 欄位| 名稱| 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明|
| --                    | --           | --     |:-:|               - |
|  opItemNo             | 料號          |string  |M|                   |
|  opBomNo              | 品號          |string  |M|                   |
|  opOrganizationId     | 工廠別        |string  |M| 預設 null ， null:不指定 / 5000:奇菱 / 6000:菱翔 |



#### Request 範例

```json
{
  "opItemNo":"H16548MM",
  "opBomNo":"01",
  "opOrganizationId":"5000"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 依據前端給的條件查詢 SAP-1[Z_PP_OPMS_BOM]資料 Request-> I_WERKS = opOrganizationId, I_MATNR = opItemNo,I_STLAL = opBomNo 回傳SAP-API的查詢結果
* 根據 opPartNo 在組 json 實作升序排序  (後端排序)
* {opDataFlag}如SAP-API有回傳資料，判斷欄位[Sortf]是否有值同時值又等於"A"或欄位[PartNo]的值起頭是否為"92-" 如是則回傳給前端並將欄位[opDataFlag]資料調整為"N"
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SAP-1：
可參考舊版 rfc_getBomList
程序如有任何異常，紀錄錯誤LOG並回傳NULL
查詢SAP-API回傳結果[T_BOM]參數如下
|            | 型態    | 參數值  | 說明        |
| ------------- | ------- | ------- | -------------------- |
| BAPIFunction    |string   | Z_PP_OPMS_BOM  |           |

| 參數名稱     | 參數值    | 型態  | 說明        |
| ------------- | ------- | ------- | -------------------- |
| I_WERKS    |{opOrganizationId}   | string  |           |
| I_MATNR    |{opItemNo}   | string  |           |
| I_STLAL    |{opBomNo}   | string  | 如果{opBomNo}只有一位數，需補足兩位，ex:01 |

| 取得回傳表格名稱 |
| ------------- |
| T_BOM         |

如果回傳資料數量為0則回傳null，否則依下面物件集合回傳
| 物件元素名稱   | 資料型別    | SAP欄位名稱  | SAP資料型別  | 物件元素回傳值  | 說明       |
| ------------ | --------- | ----------- | ----------- | ------------- | -------- |
|PartNo       |string  | IDNRK  | string  | 同SAP          |          |
|Unit         |string   | MEINS  | string  | 同SAP          |          |
|Sortf        |string  | SORTF  | string  | 同SAP          |          |
|PartName     |string   | MAKTX  | string  | 同SAP          |          |
|QuantityPer  |BigDecimal   | MENGE | string  | SAP["MENGE"]/1000 (結果四捨五入到小數點五位) | 回傳值必須格式化為0.00000 |



# Response 欄位
| 欄位          | 名稱        | 資料型別     | 資料儲存 & 說明          |
| ------------ | ---------- | ----------- | ------------------------ |
|content       |內容         | array       |                         |



#### 【content】array
| 欄位           | 名稱    | 資料型別  | 必填(非必填節點不需存在) | 來源資料 & 說明        |
| ------------- | ------- | ------- | -------------------- | -------------------- |
|opDataFlag     |資料註記   | string  |O                     | N:新增 M:修改 D:刪除   |
|opPartNo       |BOM 元件  | string  |O                     | T_BOM.IDNRK          |
|opQuantityPer  |元件數量   | int     |O                     | T_BOM.MENGE          |
|opUnit         |計量單位   | string  |O                     | T_BOM.MEINS          |





#### Response 範例

```json
{
    "msgCode": null,
    "result": {
        "content": [
            {
                "opDataFlag": "N",
                "opPartNo": "21565",
                "opQuantityPer": 5,
                "opUnit": "1"
            },
            {                
                "opDataFlag": "N",
                "opPartNo": "64123",
                "opQuantityPer": 4,
                "opUnit": "151",
            }
        ]
    }
}
```

