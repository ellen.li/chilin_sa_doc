# 染色-SOC管理-新增修改存檔
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容                      | 修改者 |
| -------- | ------------------------- | ------ |
| 20230829 | 新增規格                  | 黃東俞 |
| 20231024 | 調整欄位中文名稱opSocDesc | Nick   |
| 20231030 | 狀態預設為N               | Ellen  |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /soc/soc_c_save             |
| method | post                        |


## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             | 名稱             | 資料型別 | 必填 | 資料儲存 & 說明                                     |
| ---------------- | ---------------- | -------- | :--: | --------------------------------------------------- |
| opSocCUid        | ID               | string   |  O   | 新增時為 null                                       |
| opEquipId        | 機台             | int      |  M   |                                                     |
| opItemNo         | 料號             | string   |  M   |                                                     |
| opStatus         | 狀態             | string   |  O   | N:改版中 / W:簽核中 / Y:已發行 / D:已失效  預設:`N` |
| opMfgQty         | 每分鐘產出量     | float    |  O   |                                                     |
| opMoldScreenSize | 模頭濾網         | string   |  O   |                                                     |
| opSoakLength     | 大槽膠條浸水長度 | string   |  O   |                                                     |
| opScrewType      | 螺桿型號         | string   |  O   |                                                     |
| opMoldHole       | 模嘴孔數         | int      |  O   |                                                     |
| opMoldApeture    | 模嘴孔徑         | float    |  O   |                                                     |
| opCutterType     | 切刀型態         | string   |  O   |                                                     |
| opScreenApeture  | 篩網孔徑         | int      |  O   |                                                     |
| opSocDesc        | 原料說明         | string   |  O   |                                                     |
| opProdDesc       | 產品說明         | string   |  O   |                                                     |
| opC1Std          | C1標準           | int      |  O   |                                                     |
| opC1Max          | C1上限           | int      |  O   |                                                     |
| opC1Min          | C1下限           | int      |  O   |                                                     |
| opC2Std          | C2標準           | int      |  O   |                                                     |
| opC2Max          | C2上限           | int      |  O   |                                                     |
| opC2Min          | C2下限           | int      |  O   |                                                     |
| opC3Std          | C3標準           | int      |  O   |                                                     |
| opC3Max          | C3上限           | int      |  O   |                                                     |
| opC3Min          | C3下限           | int      |  O   |                                                     |
| opC4Std          | C4標準           | int      |  O   |                                                     |
| opC4Max          | C4上限           | int      |  O   |                                                     |
| opC4Min          | C4下限           | int      |  O   |                                                     |
| opC5Std          | C5標準           | int      |  O   |                                                     |
| opC5Max          | C5上限           | int      |  O   |                                                     |
| opC5Min          | C5下限           | int      |  O   |                                                     |
| opC6Std          | C6標準           | int      |  O   |                                                     |
| opC6Max          | C6上限           | int      |  O   |                                                     |
| opC6Min          | C6下限           | int      |  O   |                                                     |
| opC7Std          | C7標準           | int      |  O   |                                                     |
| opC7Max          | C7上限           | int      |  O   |                                                     |
| opC7Min          | C7下限           | int      |  O   |                                                     |
| opC8Std          | C8標準           | int      |  O   |                                                     |
| opC8Max          | C8上限           | int      |  O   |                                                     |
| opC8Min          | C8下限           | int      |  O   |                                                     |
| opC9Std          | C9標準           | int      |  O   |                                                     |
| opC9Max          | C9上限           | int      |  O   |                                                     |
| opC9Min          | C9下限           | int      |  O   |                                                     |
| opCAStd          | C10標準          | int      |  O   |                                                     |
| opCAMax          | C10上限          | int      |  O   |                                                     |
| opCAMin          | C10下限          | int      |  O   |                                                     |
| opCBStd          | C11標準          | int      |  O   |                                                     |
| opCBMax          | C11上限          | int      |  O   |                                                     |
| opCBMin          | C11下限          | int      |  O   |                                                     |
| opCCStd          | C12標準          | int      |  O   |                                                     |
| opCCMax          | C12上限          | int      |  O   |                                                     |
| opCCMin          | C12下限          | int      |  O   |                                                     |
| opScrewRpmStd    | 螺桿轉速         | int      |  O   |                                                     |
| opScrewRpmMax    | 螺桿轉速上限     | int      |  O   |                                                     |
| opScrewRpmMin    | 螺桿轉速下限     | int      |  O   |                                                     |
| opFeederRpmStd   | 餵料機轉速       | int      |  O   |                                                     |
| opFeederRpmMax   | 餵料機轉速下限   | int      |  O   |                                                     |
| opFeederRpmMin   | 餵料機轉速下限   | int      |  O   |                                                     |
| opWrenchRate     | 扭力             | int      |  O   |                                                     |
| opVacuumIndex    | 真空指數         | int      |  O   |                                                     |
| opIronFQty       | 磁力架鐵屑F1     | string   |  O   |                                                     |
| opIronItemQty    | 磁力架鐵屑F1     | string   |  O   |                                                     |
| opBTempStd       | 二槽水溫標準值   | int      |  O   |                                                     |
| opBTempMax       | 二槽水溫上限     | int      |  O   |                                                     |
| opBTempMin       | 二槽水溫下限     | int      |  O   |                                                     |
| opSTempStd       | 一槽水溫標準值   | int      |  O   |                                                     |
| opSTempMax       | 一槽水溫上限     | int      |  O   |                                                     |
| opSTempMin       | 一槽水溫下限     | int      |  O   |                                                     |
| opETempStd       | 三槽水溫標準值   | int      |  O   |                                                     |
| opETempMax       | 三槽水溫上限     | int      |  O   |                                                     |
| opETempMin       | 三槽水溫下限     | int      |  O   |                                                     |
| opScreenFreq     | 網目更換         | int      |  O   |                                                     |
| opRecordFreq     | 記錄頻率         | int      |  O   |                                                     |
| opMfgNotice      | 作業注意事項     | string   |  O   |                                                     |
| opRemark         | 備註             | string   |  O   |                                                     |
| opNonstopFlag    | 驗色不停車       | string   |  O   |                                                     |


#### Request 範例

```json
{
  "opSocCUid":"44CEDF0C-BE87-4CB3-B1FC-DDF98285097B",
  "opEquipId":32,
  "opItemNo":"110UXXXDPC110XXXX25F21306XX1BXA15681",
  "opStatus":"N",
  "opMfgQty":null,
  "opMoldScreenSize":"50/50",
  "opSoakLength":null,
  "opScrewType":"少2",
  "opMoldHole":5,
  "opMoldApeture":3.5,
  "opCutterType":"S",
  "opScreenApeture":5,
  "opSocDesc":"PC-110U",
  "opProdDesc":"PC",
  "opC1Std":180,
  "opC1Max":190,
  "opC1Min":170,
  "opC2Std":265,
  "opC2Max":275,
  "opC2Min":255,
  "opC3Std":270,
  "opC3Max":280,
  "opC3Min":260,
  "opC4Std":275,
  "opC4Max":285,
  "opC4Min":265,
  "opC5Std":280,
  "opC5Max":290,
  "opC5Min":270,
  "opC6Std":280,
  "opC6Max":290,
  "opC6Min":270,
  "opC7Std":null,
  "opC7Max":null,
  "opC7Min":null,
  "opC8Std":null,
  "opC8Max":null,
  "opC8Min":null,
  "opC9Std":null,
  "opC9Max":null,
  "opC9Min":null,
  "opCAStd":null,
  "opCAMax":null,
  "opCAMin":null,
  "opCBStd":null,
  "opCBMax":null,
  "opCBMin":null,
  "opCCStd":null,
  "opCCMax":null,
  "opCCMin":null,
  "opScrewRpmStd":600,
  "opScrewRpmMax":900,
  "opScrewRpmMin":300,
  "opFeederRpmStd":null,
  "opFeederRpmMax":null,
  "opFeederRpmMin":null,
  "opWrenchRate":null,
  "opVacuumIndex":null,
  "opIronFQty":null,
  "opIronItemQty":null,
  "opBTempStd":null,
  "opBTempMax":null,
  "opBTempMin":null,
  "opSTempStd":50,
  "opSTempMax":60,
  "opSTempMin":40,
  "opETempStd":null,
  "opETempMax":null,
  "opETempMin":null,
  "opScreenFreq":12,
  "opRecordFreq":2,
  "opMfgNotice":null,
  "opRemark":"首次生產",
  "nonstopFlag":"N"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 取 { 登入者ID } SYS_USER.USER_ID
* 當傳入的opSocCUid(ID)為null時，新增一筆資料至 SOC_C
  * SOC_C_UID(新ID)：取隨機 UUID
  * SOC_C_VER(新版號)：參考SQL-1，取相同 EQUIP_ID 和 ITEM_NO 的 SOC_C_VER 的最大值+1之後作為新的版號
  * 參考 SQL-2 新增資料至 SOC_C
* 有傳入opSocCUid時，參考SQL-3 更新資料
  * 注意更新時，並不會更新STATUS(狀態)欄位
  * 更新後，執行 SQL-LOG 紀錄更新資訊，LOG欄位填入"新增"或"修改"
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200

# Request 後端邏輯說明

SQL-1:

```sql
-- SocCQueryDao.getMaxSocVer
SELECT ISNULL(MAX(SOC_C_VER), 0) + 1 NEW_SOC_VER
FROM SOC_C
WHERE ITEM_NO = { opItemNo }
AND EQUIP_ID = { opEquipId }
```

SQL-2:

```sql
-- SocCQueryDao.insertSocC
INSERT INTO SOC_C(SOC_C_UID, EQUIP_ID, ITEM_NO, SOC_C_VER, STATUS, MFG_QTY, MOLD_SCREEN_SIZE, SOAK_LENGTH,
  SCREW_TYPE, MOLD_HOLE, MOLD_APETURE, CUTTER_TYPE, SCREEN_APETURE, SOC_DESC, PROD_DESC,
  C_1_STD, C_1_MAX, C_1_MIN, C_2_STD, C_2_MAX, C_2_MIN, C_3_STD, C_3_MAX, C_3_MIN,
  C_4_STD, C_4_MAX, C_4_MIN, C_5_STD, C_5_MAX, C_5_MIN, C_6_STD, C_6_MAX, C_6_MIN,
  C_7_STD, C_7_MAX, C_7_MIN, C_8_STD, C_8_MAX, C_8_MIN, C_9_STD, C_9_MAX, C_9_MIN,
  C_A_STD, C_A_MAX, C_A_MIN, C_B_STD, C_B_MAX, C_B_MIN, C_C_STD, C_C_MAX, C_C_MIN,
  SCREW_RPM_STD, SCREW_RPM_MAX, SCREW_RPM_MIN, WRENCH_RATE, VACUUM_INDEX,
  FEEDER_RPM_STD, FEEDER_RPM_MAX, FEEDER_RPM_MIN,
  IRON_F_QTY, IRON_ITEM_QTY, B_TEMP_STD, B_TEMP_MAX, B_TEMP_MIN, S_TEMP_STD, S_TEMP_MAX, S_TEMP_MIN,
  E_TEMP_STD, E_TEMP_MAX, E_TEMP_MIN, NONSTOP_FLAG,
  SCREEN_FREQ, RECORD_FREQ, MFG_NOTICE, REMARK, CDT, CREATE_BY, PUB_DATE, PUBLIC_BY, UDT, UPDATE_BY)
VALUES({ 新ID }, { opEquipId }, { opItemNo }, { 新版號 },  { opStatus }, { opMfgQty }, { opMoldScreenSize }, { opSoakLength },
  { opScrewType }, { opMoldHole }, { opMoldApeture }, { opCutterType }, { opScreenApeture }, { opSocDesc }, { opProdDesc },
  { opC1Std }, { opC1Max }, { opC1Min }, { opC2Std }, { opC2Max }, { opC2Min }, { opC3Std }, { opC3Max }, { opC3Min },
  { opC4Std }, { opC4Max }, { opC4Min }, { opC5Std }, { opC5Max }, { opC5Min }, { opC6Std }, { opC6Max }, { opC6Min },
  { opC7Std }, { opC7Max }, { opC7Min }, { opC8Std }, { opC8Max }, { opC8Min }, { opC9Std }, { opC9Max }, { opC9Min },
  { opCAStd }, { opCAMax }, { opCAMin }, { opCBStd }, { opCBMax }, { opCBMin }, { opCCStd }, { opCCMax }, { opCCMin },
  { opScrewRpmStd }, { opScrewRpmMax }, { opScrewRpmMin }, { opWrenchRate }, { opVacuumIndex }
  { opFeederRpmStd }, { opFeederRpmMax }, { opFeederRpmMin },
  { opIronFQty }, { opIronItemQty }, { opBTempStd }, { opBTempMax }, { opBTempMin }, { opSTempStd }, { opSTempMax }, { opSTempMin }
  { opETempStd }, { opETempMax }, { opETempMin }, { nonstopFlag },
  { opScreenFreq }, { opRecordFreq }, { opMfgNotice }, { opRemark }, GETDATE(), { 登入者ID }, null, null, GETDATE(), { 登入者ID });
```

SQL-3:

```sql
UPDATE SOC_C SET MFG_QTY = { opMfgQty }, MOLD_SCREEN_SIZE = { opMoldScreenSize }, SOAK_LENGTH = { opSoakLength },
  SCREW_TYPE = { opScrewType }, MOLD_HOLE = { opMoldHole }, MOLD_APETURE = { opMoldApeture }, CUTTER_TYPE = { opCutterType }, SCREEN_APETURE = { opScreenApeture },
  SOC_DESC = { opSocDesc }, PROD_DESC = { opProdDesc },
  C_1_STD = { opC1Std }, C_1_MAX = { opC1Max }, C_1_MIN = { opC1Min }, C_2_STD = { opC2Std }, C_2_MAX = { opC2Max }, C_2_MIN = { opC2Min },
  C_3_STD = { opC3Std }, C_3_MAX = { opC3Max }, C_3_MIN = { opC3Min },
  C_4_STD = { opC4Std }, C_4_MAX = { opC4Max }, C_4_MIN = { opC4Min }, C_5_STD = { opC5Std }, C_5_MAX = { opC5Max }, C_5_MIN = { opC5Min },
  C_6_STD = { opC6Std }, C_6_MAX = { opC6Max }, C_6_MIN = { opC6Min },
  C_7_STD = { opC7Std }, C_7_MAX = { opC7Max }, C_7_MIN = { opC7Min }, C_8_STD = { opC8Std }, C_8_MAX = { opC8Max }, C_8_MIN = { opC8Min },
  C_9_STD = { opC9Std }, C_9_MAX = { opC9Max }, C_9_MIN = { opC9Min },
  C_A_STD = { opCAStd }, C_A_MAX = { opCAMax }, C_A_MIN = { opCAMin }, C_B_STD = { opCBStd }, C_B_MAX = { opCBMax }, C_B_MIN = { opCBMin },
  C_C_STD = { opCCStd }, C_C_MAX = { opCCMax }, C_C_MIN = { opCCMin },
  SCREW_RPM_STD = { opScrewRpmStd }, SCREW_RPM_MAX = { opScrewRpmMax }, SCREW_RPM_MIN = { opScrewRpmMin }, WRENCH_RATE = { opWrenchRate }, VACUUM_INDEX = { opVacuumIndex },
  FEEDER_RPM_STD = { opFeederRpmStd }, FEEDER_RPM_MAX = { opFeederRpmMax }, FEEDER_RPM_MIN = { opFeederRpmMin },
  IRON_F_QTY = { opIronFQty }, IRON_ITEM_QTY = { opIronItemQty }, B_TEMP_STD = { opBTempStd }, B_TEMP_MAX = { opBTempMax }, B_TEMP_MIN = { opBTempMin },
  S_TEMP_STD = { opSTempStd }, S_TEMP_MAX = { opSTempMax }, S_TEMP_MIN = { opSTempMin },
  E_TEMP_STD = { opETempStd }, E_TEMP_MAX = { opETempMax }, E_TEMP_MIN = { opETempMin }, NONSTOP_FLAG = { nonstopFlag },
  MFG_NOTICE = { opMfgNotice }, SCREEN_FREQ = { opScreenFreq }, RECORD_FREQ = { opRecordFreq },REMARK = { opRemark }, UDT=GETDATE(), UPDATE_BY = { 登入者ID }
  WHERE SOC_C_UID = { opSocCUid }
```
SQL-LOG:
```sql
INSERT INTO SYS_MODI_LOG (CATEGORY, DATAKEY, USERID, USER_NAME, LOG) VALUES ('SOC_C', { opSocCUid }, {登入者ID}, {登入者名稱}, '新增(修改)');
```

# Response 欄位

| 欄位             | 名稱           | 資料型別 | 說明               |
|------------------|---------------|----------|-------------------|
| opSocCUid        | ID            | string   |                   |

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":{
      "opSocCUid": "44CEDF0C-BE87-4CB3-B1FC-DDF98285097B"
    }
  }
}
```