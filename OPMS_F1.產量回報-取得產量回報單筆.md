# 產量回報-取得單筆回報
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

產量回報-取得單筆回報

## 修改歷程

| 修改時間 | 內容                    | 修改者 |
|----------|-----------------------|--------|
| 20230921 | 新增規格                | Ellen  |
| 20231129 | 志誠提需求需紀錄 PS_UID | Nick   |


## 來源URL及資料格式

| 項目   | 說明         |
| ------ | ------------ |
| URL    | /psr/get_one |
| method | post         |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位     | 名稱        | 資料型別 | 必填  | 資料儲存 & 說明 |
| -------- | ----------- | :------: | :---: | --------------- |
| opPsrUid | 產量回報UID |  string  |   O   |                 |
| opPsUid  | 生產排程UID |  string  |   O   |                 |

#### Request 範例

```json
{
  "opPsrUid": "713986FC-AA42-4283-B476-35D0F7587A48"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 若{opPsrUid}有值，參考 SQL-1取得產量回報資料，如果不存在 return 400 NOTFOUND
* 若{opPsUid}有值，參考 SQL-2取得生產排程資料，如果不存在 return 400 NOTFOUND
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1: 取得產量回報
```sql
SELECT
  R.PSR_UID,
  R.WO_UID,
  W.WO_NO,
  W.CTN_WEIGHT,
  I.ITEM_NO,
  R.EQUIP_ID,
  CONVERT(varchar(10), R.REP_MFG_DATE, 111) AS REP_MFG_DATE, 
  R.REP_SHIFT_CODE,
  R.SHIFT_MFG_QTY,
  R.SHIFT_PRE_QTY,
  R.REP_MIN_MFG_QTY,
  R.REP_SHIFT_HOURS,
  R.REP_TIME_LINE,
  R.REP_CTN_DESC,
  R.WRN_FLAG,
  R.WRN_REMARK,
  R.PS_UID
FROM
  PS_R R
  LEFT JOIN WO_H W ON R.WO_UID = W.WO_UID
  LEFT JOIN BOM_MTM B ON W.BOM_UID = B.BOM_UID
  LEFT JOIN ITEM_H I  ON	B.ITEM_NO = I.ITEM_NO
WHERE
  R.PSR_UID = {opPsrUid}
```

SQL-2: 取得生產排程
```sql
SELECT
	PS_UID,
  WO_UID,
	WO_NO,
  CTN_WEIGHT,
  ITEM_NO,
	EQUIP_ID,
	MFG_DATE,
	SHIFT_CODE,
	SHIFT_HOURS,
	SHIFT_MFG_QTY
FROM
	PS_SCHEDULE_V
WHERE
	PS_UID = {opPsUid}
```

# Response 欄位
| 欄位            | 名稱        | 資料型別 | 來源資料 & 說明                                                   |
| --------------- | ----------- | -------- | ----------------------------------------------------------------- |
| opPsrUid        | 產量回報UID | string   | PSR_UID(SQL-1) 或 `null`                                          |
| opPsUid         | 生產排程UID | string   | PS_UID(SQL-2) 或 PS_UID(SQL-1)                                    |
| opWoUid         | 工單ID      | string   | WO_UID(SQL-1) 或 WO_UID(SQL-2)                                    |
| opWoNo          | 工單號碼    | string   | WO_NO(SQL-1) 或 WO_NO(SQL-2)                                      |
| opItemNo        | 料號        | string   | ITEM_NO(SQL-1) 或 ITEM_NO(SQL-2)                                  |
| opEquipId       | 機台        | string   | EQUIP_ID(SQL-1) 或 EQUIP_ID(SQL-2)                                |
| opRepMfgDate    | 日期        | string   | REP_MFG_DATE(SQL-1) 或 MFG_DATE(SQL-2)                            |
| opRepShiftCode  | 班別代碼    | string   | REP_SHIFT_CODE(SQL-1) 或 SHIFT_CODE(SQL-2)  A: 早 / B: 中 / C: 晚 |
| opRepShiftHours | 時數        | string   | REP_SHIFT_HOURS(SQL-1) 或 SHIFT_HOURS(SQL-2)                      |
| opRepMinMfgQty  | 吐出量      | string   | REP_MIN_MFG_QTY(SQL-1) 或 `null`                                  |
| opRepTimeLine   | 期間        | string   | REP_TIME_LINE(SQL-1) 或 `null`                                    |
| opRepCtnDesc    | 桶次        | string   | REP_CTN_DESC(SQL-1) 或 `null`                                     |
| opCtnWeight     | 桶重        | string   | CTN_WEIGHT(SQL-1) 或 CTN_WEIGHT(SQL-2)                            |
| opShiftMfgQty   | 實際產量    | string   | SHIFT_MFG_QTY(SQL-1) 或 SHIFT_MFG_QTY(SQL-2)                      |
| opShiftPreQty   | 預染數量    | string   | SHIFT_PRE_QTY(SQL-1) 或 `null`                                    |
| opWrnRemark     | 異常回報    | string   | WRN_REMARK(SQL-1) 或 `null`                                       |
| opWrnFlag       | 是否為異常  | string   | WRN_FLAG(SQL-1) 或 "N"  Y/N  Y: 是 / N: 否                        |


#### Response 範例

```json
{
    "msgCode": null,
    "result": {
      "content":{
        "opPsrUid": "713986FC-AA42-4283-B476-35D0F7587A48",
        "opPsUid": "",
        "opWoUid": "EDED1106-8064-4922-BC40-FD8A207682AD",
        "opItemNo": "757HXXXXX1FCA20467",
        "opEquipId": "45",
        "opRepMfgDate": "2022/12/04",
        "opRepShiftCode": "B",
        "opRepShiftHours": "2.5",
        "opRepMinMfgQty": "5.85",
        "opRepTimeLine": "16:00~18:30",
        "opRepCtnDesc": "1~4",
        "opCtnWeight": "1047.6",
        "opShiftMfgQty": "900",
        "opShiftPreQty": "0",
        "opWrnRemark": "",
        "opWrnFlag": "N"
      }
    }
}
```
 