# 產能對照-取得產能對照列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

產能對照列表

## 修改歷程

| 修改時間 | 內容                            | 修改者 |
| -------- | ------------------------------- | ------ |
| 20230828 | 新增規格                        | Ellen  |
| 20231011 | 料號使用模糊查詢                | Nick   |
| 20231011 | JOIN PRODUNIT表，保持跟舊系統一致 | Ellen  |


## 來源URL及資料格式

| 項目   | 說明       |
| ------ | ---------- |
| URL    | /iepc/list |
| method | post       |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             | 名稱     | 資料型別 | 必填 | 來源資料 & 說明                                                         |
|------------------|--------|----------|:----:|-------------------------------------------------------------------------|
| opOrganizationId | 工廠別   | string   |  O   | 預設 `null` ， null:不指定 / 5000:奇菱 / 6000:菱翔                       |
| opProdunitNo     | 生產單位 | string   |  O   | 預設 `null`                                                             |
| opEquipId        | 機台代號 | string   |  O   | 預設 `null`                                                             |
| opItemNo         | 料號     | string   |  O   | 預設 `null`                                                             |
| opBDate          | 開始時間 | string   |  O   | yyyy/MM/dd                                                              |
| opEDate          | 結束時間 | string   |  O   | yyyy/MM/dd                                                              |
| opPage           | 第幾頁   | integer  |  O   | 預設`1`，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推 |
| opPageSize       | 分頁筆數 | integer  |  O   | 預設 `10`                                                               |
| opOrder          | 排序方式 | string   |  O   | "ASC":升冪排序 / "DESC":降冪排序                                        |



#### Request 範例

```json
{
  "opProdunitNo": "A01",
  "opEquipId": "39",
  "opItemNo": "LDCP421X1XBXXXXXXX",
  "opBDate": "2014/01/01",
  "opEDate": "2014/01/20",
  "opPage": 1,
  "opPageSize": 10,
  "opOrder": "DESC"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 使用前端給的條件，參考 SQL-1 自組【搜尋條件】，如為 null 代表不需組條件
* {offset} 為起始列印 row ，第一頁為 0，第二頁後為 0 + (opPageSize*(opPage-1))
* 參考 SQL-1 查回結果 result
* Response 結果需含分頁資訊，無資料 pageInfo 裡面項目皆給 0，頁數與分頁筆數前端如有提供，依前端判斷給的資料計算相關數值及拆分頁面
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1: 
```sql
SELECT
  A.ORGANIZATION_ID,
  A.PRODUNIT_NO,
  A.EQUIP_NAME,
  B.IEPC_UID,
  B.EQUIP_ID,
  CONVERT (VARCHAR (10), B.MS_DATE, 111) AS MS_DATE,
  B.ITEM_NO,
  B.RPM,
  B.MFG_QTY,
  B.NET_TIME,
  B.SHIFT_STD_QTY,
  B.ACTIVE_FLAG,
  B.STATUS,
  B.BDP,
  B.CURR,
  B.HOUR_STD_QTY,
  B.PEOPLE_STD_QTY,
FROM
  IE_PC B
  INNER JOIN EQUIP_H A ON B.EQUIP_ID= A.EQUIP_ID
  INNER JOIN PRODUNIT P ON A.PRODUNIT_NO=P.PRODUNIT_NO AND A.ORGANIZATION_ID=P.ORGANIZATION_ID 
WHERE
  B.ACTIVE_FLAG != 'N'
  --如{opProdunitNo}非 null 則加入以下SQL
  AND A.PRODUNIT_NO = {opProdunitNo}
  --如{opBDate}非 null 則加入以下SQL
  AND B.MS_DATE>= CONVERT (DATETIME, {opBDate}) 
  --如{opEDate}非 null 則加入以下SQL
  AND B.MS_DATE<= CONVERT (DATETIME, {opEDate}) 
  --如{opEquipId}非 null 則加入以下SQL
  AND B.EQUIP_ID = {opEquipId}
  --如{opItemNo}非 null 則加入以下SQL
  AND B.ITEM_NO like '%{opItemNo}%'
ORDER BY 
  --如{opOrder}為 null,預設排序
  A.ORGANIZATION_ID,A.EQUIP_NAME,B.MS_DATE DESC
  --如{opOrder}等於'ASC' 則加入以下SQL
  B.MS_DATE ASC,A.ORGANIZATION_ID,A.EQUIP_NAME
  --如{opOrder}等於'DESC' 則加入以下SQL
  B.MS_DATE DESC,A.ORGANIZATION_ID,A.EQUIP_NAME
```


# Response 欄位
| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
|----------|--------|----------|-----------------|
| pageInfo | 分頁資訊 | object   |                 |
| content  | 附帶訊息 | array    |                 |

#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明 |
|------------|------------|----------|-----------------|
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |


#### 【content】array
| 欄位         | 名稱        | 資料型別 | 來源資料 & 說明                                                     |
|--------------|-----------|----------|---------------------------------------------------------------------|
| opIepcUid    | 產能資料UID | string   | IEPC_UID                                                            |
| opStatus     | 狀態        | string   | STATUS， Y/N  Y: 非概估 / N: 概估 ，若{STATUS}等於null或空字串轉為`Y` |
| opMsDate     | 量測日期    | string   | MS_DATE                                                             |
| opEquipName  | 機台名稱    | string   | EQUIP_NAME                                                          |
| opItemNo     | 料號        | string   | ITEM_NO                                                             |
| opHourStdQty | 標準產能    | string   | SHIFT_STD_QTY                                                       |


#### Response 範例

```json
{
    "msgCode": null,
    "result": {
      "pageInfo": {
        "pageNumber": 1, 
        "pageSize": 10,
        "pages": 1,      
        "total": 1     
      },
      "content":[
        {
            "opIepcUid": "5000-5E1E35DD-33F3-4A16-9E79-B621E420D853",
            "opStatus": "Y",
            "opMsDate": "2014/02/26",
            "opEquipName": "染色少3",
            "opItemNo": "LDCP421X1XBXXXXXXX",
            "opHourStdQty": "28.80",
        }
      ]
    }
}
```
 