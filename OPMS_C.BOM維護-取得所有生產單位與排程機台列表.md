# OPMS_C.BOM維護-取得有機台的所有生產單位列表(by工廠別)
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得有機台的所有生產單位列表(by工廠別)

## 修改歷程

| 修改時間 | 內容                                | 修改者 |
|----------|-----------------------------------|--------|
| 20230614 | 新增規格                            | Nick   |
| 20230806 | 調整工廠別，改為非必填，null 為不指定工廠別 | Nick   |

## 來源URL及資料格式

| 項目   | 說明                                |
|--------|-------------------------------------|
| URL    | /bom/get_all_ps_produnit_with_equip |
| method | post                                |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，(非必填節點不需存在)  
| 欄位             | 名稱   | 資料型別 | 必填 | 來源資料 & 說明                                 |
|------------------|------|----------|:----:|-------------------------------------------|
| opOrganizationId | 工廠別 | string   |  O   | 預設 `null` ， null:不指定 5000:奇菱 / 6000:菱翔 |
#### Request 範例

```json
{
  "opOrganizationId": null
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 參考 SQL-1 查回結果 result
* opEquipList 無資料給 null，ex: "opEquipList":null
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容


# Request 後端邏輯說明

SQL-1:
```sql
SELECT P.PRODUNIT_NO,P.PRODUNIT_NAME,E.EQUIP_ID,E.EQUIP_NAME,E.PROD_TYPE,E.CQ_FLAG,E.NP
FROM PRODUNIT P
INNER JOIN EQUIP_H E ON P.ORGANIZATION_ID=E.ORGANIZATION_ID AND P.PRODUNIT_NO=E.PRODUNIT_NO
WHERE PS_FLAG = 'Y'

--如果 前端條件非 NULL，則加入下一行 SQL
AND  P.ORGANIZATION_ID={opOrganizationId}  --工廠別

ORDER BY P.PRODUNIT_NO,M_EQUIP_ID
```


# Response 欄位

#### 【content】array
| 欄位           | 名稱         | 資料型別 | 來源資料 & 說明 |
|----------------|------------|----------|-----------------|
| opProdunitNo   | 生產單位代號 | string   | P.PRODUNIT_NO   |
| opProdunitName | 生產單位名稱 | string   | P.PRODUNIT_NAME |
| opEquipList    | 機台清單     | array    |                 |


#### 【opEquipList】array
| 欄位        | 名稱       | 資料型別 | 來源資料 & 說明 |
|-------------|----------|----------|-----------------|
| opEquipId   | 機台代號   | string   | EQUIP_ID        |
| opEquipName | 機台名稱   | string   | EQUIP_NAME      |
| opProdType  | 生產型態   | string   | PROD_TYPE       |
| opCqFlag    | 體積或計量 | string   | CQ_FLAG         |
| opNp        | 機台人數   | string   | NP              |

#### Response 範例

```json
{
   "msgCode":null,
   "result":{
      "content":[
         {
            "opProdunitNo":"001",
            "opProdunitName":"憲成",
            "opEquipList":[
               {
                  "opEquipId":"601",
                  "opEquipName":"憲成#1",
                  "opProdType":null,
                  "opCqFlag":"C",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"602",
                  "opEquipName":"憲成#2",
                  "opProdType":null,
                  "opCqFlag":"C",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"603",
                  "opEquipName":"憲成#3",
                  "opProdType":null,
                  "opCqFlag":"C",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"604",
                  "opEquipName":"憲成#4",
                  "opProdType":null,
                  "opCqFlag":"C",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"605",
                  "opEquipName":"憲成#5",
                  "opProdType":null,
                  "opCqFlag":"C",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"606",
                  "opEquipName":"憲成#6",
                  "opProdType":null,
                  "opCqFlag":"C",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"607",
                  "opEquipName":"憲成#7",
                  "opProdType":null,
                  "opCqFlag":"C",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"608",
                  "opEquipName":"憲成#8",
                  "opProdType":null,
                  "opCqFlag":"C",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"609",
                  "opEquipName":"憲成#9",
                  "opProdType":null,
                  "opCqFlag":"C",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"610",
                  "opEquipName":"憲成#10",
                  "opProdType":null,
                  "opCqFlag":"C",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"653",
                  "opEquipName":"憲成#11",
                  "opProdType":null,
                  "opCqFlag":"C",
                  "opNp":"1.0"
               }
            ]
         },
         {
            "opProdunitNo":"A01",
            "opProdunitName":"樣品課",
            "opEquipList":[
               {
                  "opEquipId":"61",
                  "opEquipName":"實驗#1",
                  "opProdType":"C",
                  "opCqFlag":"C",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"62",
                  "opEquipName":"實驗#2",
                  "opProdType":"C",
                  "opCqFlag":"C",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"63",
                  "opEquipName":"實驗#3",
                  "opProdType":"C",
                  "opCqFlag":"C",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"65",
                  "opEquipName":"實驗#4",
                  "opProdType":"C",
                  "opCqFlag":"C",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"44",
                  "opEquipName":"染色少N",
                  "opProdType":"C",
                  "opCqFlag":"C",
                  "opNp":"0.5"
               },
               {
                  "opEquipId":"69",
                  "opEquipName":"實驗#6",
                  "opProdType":"C",
                  "opCqFlag":"C",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"70",
                  "opEquipName":"實驗#Z",
                  "opProdType":"C",
                  "opCqFlag":"C",
                  "opNp":"1.0"
               }
            ]
         },
         {
            "opProdunitNo":"P1",
            "opProdunitName":"複材課",
            "opEquipList":[
               {
                  "opEquipId":"46",
                  "opEquipName":"複材#6",
                  "opProdType":"M",
                  "opCqFlag":"Q",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"11",
                  "opEquipName":"複材#1",
                  "opProdType":"M",
                  "opCqFlag":"C",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"12",
                  "opEquipName":"複材#2",
                  "opProdType":"M",
                  "opCqFlag":"Q",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"14",
                  "opEquipName":"複材#3",
                  "opProdType":"M",
                  "opCqFlag":"Q",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"15",
                  "opEquipName":"複材#4",
                  "opProdType":"M",
                  "opCqFlag":"Q",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"16",
                  "opEquipName":"複材#5",
                  "opProdType":"M",
                  "opCqFlag":"Q",
                  "opNp":"1.0"
               },
               {
                  "opEquipId":"111",
                  "opEquipName":"複材#11(線外)",
                  "opProdType":"M",
                  "opCqFlag":null,
                  "opNp":"1.0"
               }
            ]
         }
      ]
   }
}
```

