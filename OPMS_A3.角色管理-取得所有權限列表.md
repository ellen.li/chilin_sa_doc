# 角色管理-取得所有權限清單
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得所有權限清單

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230405 | 新增規格 | Nick   |
| 20230522 | 調整 ORDERBY 條件 |Nick|

## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    |  /function/list_perms |
| method | get             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
N/A
#### Request 範例
N/A

# Request 後端流程說明

* 參考 SQL-1 查回結果組出不重複的 FCODE，如 PCODE 不是 null 需將資料 PCODE,PNAME 組成 opPermsList 陣列
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
SELECT SF.CODE as FCODE,
       SF.CNAME as FNAME,
	   PARENT,
       SFP.PERM_CODE as PCODE,
       SFP.CNAME as PNAME 
	FROM SYS_FUNCTION_V3 SF  
    LEFT JOIN SYS_FUNCTION_PERMS SFP
	  ON SF.CODE = SFP.CODE
	WHERE SF.VISIBLE =1
	ORDER BY SF.SORT_ID,SF.UDT DESC,SFP.SORT_ID
```

# Response 欄位
#### 【content】array
| 欄位        | 名稱         | 資料型別 | 來源資料 & 說明                |
|-------------|------------|----------|--------------------------------|
| opFCode     | 功能代號     | string   | FCODE                          |
| opFName     | 功能名稱     | string   | FNAME                          |
| opParent    | 父層功能代號 | string   | PARENT                         |
| opPermsList | 權限清單     | array    | 預設 `null` 無清單代表不分權限 |

【opPermsList array object】
| 欄位    | 名稱       | 資料型別 | 來源資料 & 說明 |
|---------|----------|----------|-----------------|
| opPCode | 子權限代號 | string   | PCODE           |
| opPName | 權限名稱   | string   | PNAME           |


#### Response 範例

```json
{
   "msgCode":"",
   "result":{
      "content":[
         {
            "opFCode":"100",
            "opFName":"BOM維護",
            "opParent":"0",
            "opPermsList":[
               { "opPCode":"QUERY","opPName":"查詢"},
               { "opPCode":"EDIT", "opPName":"維護"}
            ]
         },
         {
            "opFCode":"200",
            "opFName":"工單維護",
            "opParent":"0",
            "opPermsList":[
               { "opPCode":"QUERY","opPName":"查詢"},
               { "opPCode":"EDIT", "opPName":"維護"}
            ]
         },
         {
            "opFCode":"300",
            "opFName":"色號維護",
            "opParent":"0",
            "opPermsList":[
               { "opPCode":"QUERY","opPName":"查詢"},
               { "opPCode":"EDIT", "opPName":"維護"}
            ]
         },
         {
            "opFCode":"400",
            "opFName":"生產排程",
            "opParent":"0",
            "opPermsList":null
         },
         {
            "opFCode":"411",
            "opFName":"已訂未交表",
            "opParent":"400",
             "opPermsList":null
         },
         {
            "opFCode":"404",
            "opFName":"生產排程",
            "opParent":"400",
             "opPermsList":null
         },
         {
            "opFCode":"407",
            "opFName":"產能對照表管理",
            "opParent":"400",
             "opPermsList":null
         },
         {
            "opFCode":"493",
            "opFName":"生產排程查詢",
            "opParent":"400",
            "opPermsList":null
         }
      ]
   }
}
```
