# BASE.共用基礎資料-取得OPMS參數列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**  
**參考共用參數列表對照:[OPMS_REF_共用參數列表對照.md](./OPMS_REF_共用參數列表對照.md)**  

## 功能說明:

取得OPMS參數列表

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230501 | 新增規格 | Nick   |

## 來源URL及資料格式

| 項目   | 說明                 |
|--------|----------------------|
| URL    | /comm/get_param_list |
| method | get                  |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。  
| 欄位          | 名稱         | 資料型別     | 必填 | 來源資料 & 說明 |
|---------------|------------|--------------|:----:|-----------------|
| opParamIdList | 查詢參數名稱 | array string |  M   |                 |

#### 【opParamIdList】array
| 欄位      | 名稱     | 資料型別 | 必填 | 來源資料 & 說明 |
|-----------|--------|----------|:----:|-----------------|
| opType    | 參數分類 | string   |  M   |                 |
| opParamId | 參數代號 | string   |  M   |                 |


#### Request 範例
```json
{
    opParamIdList:[
            {"opType":"BOM","opParamId":"MIXDESC2"},
            {"opType":"BOM","opParamId":"MIXDESC2_M"}
        ]
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* opParamIdList 條件拆解為 指定條件 SQL
* 參考 SQL-1 查回結果 result
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
SELECT TYPE,PARAM_ID,OPT_VALUE VALUE,OPT_DISPLAY TEXT FROM OPMS_PARAMS 
WHERE 1=1
--指定條件
AND (
       (TYPE={opType[0]} AND PARAM_ID = {opParamId[0]})
    OR (TYPE={opType[1]} AND PARAM_ID = {opParamId[1]})
    OR .....etc
)

ORDER BY TYPE,PARAM_ID,SORT_ID
```


# Response 欄位
#### 【content】array
| 欄位      | 名稱     | 資料型別 | 來源資料 & 說明 |
|-----------|--------|---------:|-----------------|
| opType    | 參數分類 |   string | TYPE            |
| opParamId | 參數代號 |   string | PARAM_ID        |
| opValue   | 內存值   |   string | VALUE           |
| opText    | 文字外顯 |   string | TEXT            |


#### Response 範例

```json
{
    "msgCode": null,
    "result": {
       "content": [
        {"opType":"BOM","opParamId":"MIXDESC2","opValue":"A1","opText":"3/5原料+色粉(色母)+2/5原料攪拌25mins，即完成攪拌"},
        {"opType":"BOM","opParamId":"MIXDESC2","opValue":"A2","opText":"3/5原料+色粉(色母)+助劑+2/5原料攪拌25mins，即完成攪拌"},
        {"opType":"BOM","opParamId":"MIXDESC2","opValue":"B","opText":"(a)3/5原料+N4+1/5原料攪拌5 mins<br/>&emsp;&ensp;(b)完成步驟(a)加入色粉+1/5原料攪拌25 mins"},
        {"opType":"BOM","opParamId":"MIXDESC2","opValue":"C","opText":"(a)3/5原料+N4+1/5原料攪拌5 mins<br/>&emsp;&ensp;(b)完成步驟(a)加入色粉、助劑+1/5原料攪拌25 mins"},
        {"opType":"BOM","opParamId":"MIXDESC2","opValue":"D","opText":"(a)3/5原料+N4+1/5原料攪拌5 mins<br/>&emsp;&ensp;(b)完成步驟(a)加入色粉+1/5原料攪拌25 mins<br/>&emsp;&ensp;(c) 完成步驟(b)再加入助劑攪拌3 mins"},
        {"opType":"BOM","opParamId":"MIXDESC2","opValue":"E","opText":"(a)3/5原料+N4+1/5原料攪拌5 mins<br/>&emsp;&ensp;(b)完成步驟(a)加入色粉+1/5原料攪拌25 mins<br/>&emsp;&ensp;(c) 完成步驟(b)再加入玻纖並攪拌22圈"},
        {"opType":"BOM","opParamId":"MIXDESC2","opValue":"F","opText":"(a)3/5原料+N4+1/5原料攪拌5 mins<br/>&emsp;&ensp;(b)完成步驟(a)加入色粉+1/5原料攪拌25 mins<br/>&emsp;&ensp;(c) 完成步驟(b)再加入助劑攪拌3 mins<br/>&emsp;&ensp;(d)完成步驟(c)再加入玻纖並攪拌22圈"},
        {"opType":"BOM","opParamId":"MIXDESC2","opValue":"G","opText":"(a)3/5原料+[N-6]+1/5原料攪拌5 mins<br/>&emsp;&ensp;(b)加入色粉(或色粉+助劑)+ 1/5原料攪拌25 mins"},
        {"opType":"BOM","opParamId":"MIXDESC2","opValue":"H","opText":"(a)3/5原料+[N-6]+1/5原料攪拌5 mins<br/>&emsp;&ensp;(b)加入色粉+1/5原料攪拌25 mins<br/>&emsp;&ensp;(c) 再加入助劑攪拌3 mins"},
        {"opType":"BOM","opParamId":"MIXDESC2","opValue":"I","opText":"(a)3/5原料+[A-NC50]+1/5原料攪拌5 mins<br/>&emsp;&ensp;(b)加入色粉(或色粉+助劑)+ 1/5原料攪拌25 mins<br/>"},
        {"opType":"BOM","opParamId":"MIXDESC2","opValue":"J","opText":"(a)3/5原料+[A-NC50]+1/5原料攪拌5 mins<br/>&emsp;&ensp;(b)加入色粉+1/5原料攪拌25 mins<br/>&emsp;&ensp;(c)再加入助劑攪拌3 mins"},
        {"opType":"BOM","opParamId":"MIXDESC2","opValue":"K","opText":"原料+色粉攪拌25sec ×2次後，卸料並作卡粉清理，,重複此作業3次"},
        {"opType":"BOM","opParamId":"MIXDESC2","opValue":"L","opText":"2/3原料+N4+色粉+1/3原料,攪拌25分"},
        {"opType":"BOM","opParamId":"MIXDESC2","opValue":"M","opText":"(a)3/5原料+NC-100+1/5原料攪拌5 mins<br/>&emsp;&ensp;(b)完成步驟(a)加入色粉+1/5原料攪拌25 mins"},
        {"opType":"BOM","opParamId":"MIXDESC2","opValue":"N","opText":"(a)3/5原料+NC-100+1/5原料攪拌5 mins<br/>&emsp;&ensp;(b)完成步驟(a)加入色粉、助劑+1/5原料攪拌25 mins"},
        {"opType":"BOM","opParamId":"MIXDESC2_M","opValue":"M3","opText":"原料+色粉+助劑用高速混合機混合二次，即完成攪拌"},
        {"opType":"BOM","opParamId":"MIXDESC2_M","opValue":"M4","opText":"(a)3/5原料+N4+1/5原料混合5mins<br/>&emsp;&ensp;(b)完成步驟(a)，加入色粉(珠光色粉除外)+1/5原料混合25mins<br/>&emsp;&ensp;(c) 完成步驟(b)，加入珠光、銀粉混合3mins"},
        {"opType":"BOM","opParamId":"MIXDESC2_M","opValue":"M5","opText":"(a)3/5原料+NC-100+1/5原料攪拌5 mins<br/>&emsp;&ensp;(b) 完成步驟(a)，加入色粉(珠光色粉除外)+1/5原料混合25mins<br/>&emsp;&ensp;(c) 完成步驟(b)，加入珠光、銀粉+助劑混合3mins"},
        {"opType":"BOM","opParamId":"MIXDESC2_M","opValue":"M6","opText":"3/5原料+NC-100+1/5原料混合5min，完成後再投入微量色粉+1/5原料混合25min"},
        {"opType":"BOM","opParamId":"MIXDESC2_M","opValue":"M7","opText":"多點 PC"},
        {"opType":"BOM","opParamId":"MIXDESC2_M","opValue":"M8","opText":"多點 ABS 或 AS"},
        {"opType":"BOM","opParamId":"MIXDESC2_M","opValue":"M9","opText":"多點回收料"},
        {"opType":"BOM","opParamId":"MIXDESC2_M","opValue":"M10","opText":"液態注射器"}
      ]
    }
}
```


