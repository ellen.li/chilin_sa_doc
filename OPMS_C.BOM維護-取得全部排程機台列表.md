# OPMS_C.BOM維護-取得全部排程機台列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得全部排程機台列表

## 修改歷程

| 修改時間 | 內容         | 修改者 |
| -------- | ------------ | ------ |
| 20230526 | 新增規格     | Nick   |
| 20230725 | 增加查詢參數 | Ellen  |

## 來源URL及資料格式

| 項目   | 說明                           |
| ------ | ------------------------------ |
| URL    | /bom/get_all_ps_equipment_list |
| method | post                           |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。  
| 欄位             | 名稱         | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明                                    |
| ---------------- | ------------ | -------- | :----------------------: | -------------------------------------------------- |
| opOrganizationId | 工廠別       | string   |            O             | 預設 `null` ， null:不指定 / 5000:奇菱 / 6000:菱翔 |
| opProdunitNo     | 生產單位代碼 | string   |            O             | 預設 `null`                                        |
| opProdType       | 工廠別       | string   |            O             | 預設 `null`                                        |

#### Request 範例

```json
{
  "opOrganizationId": null,
  "opProdunitNo": null,
  "opProdType": null,
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 前端條件如為 null 表全部，對應的指定條件不組，否則按照前端條件指定。
* 參考 SQL-1 查回結果 result
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
SELECT E.EQUIP_ID,E.EQUIP_NAME,E.CQ_FLAG,E.NP,E.BIAXIS_FLAG,P.PROD_TYPE
FROM EQUIP_H E
INNER JOIN PRODUNIT P ON E.ORGANIZATION_ID=P.ORGANIZATION_ID AND E.PRODUNIT_NO=P.PRODUNIT_NO
WHERE E.PS_FLAG='Y' 
  --搜尋條件，請依前端變數是否為 null 決定是否要組

  --如{opOrganizationId}非 null 則加入以下SQL
  AND E.ORGANIZATION_ID={opOrganizationId}
  --如{opProdunitNo}非 null 則加入以下SQL
  AND E.PRODUNIT_NO={opProdunitNo}
  --如{opProdType}非 null 則加入以下SQL
  AND E.PROD_TYPE={opProdType}
ORDER BY E.EQUIP_ID
```


# Response 欄位
#### 【content】array
| 欄位         | 名稱       | 資料型別 | 來源資料 & 說明 |
| ------------ | ---------- | -------- | --------------- |
| opEquipId    | 機台代號   | string   | E.EQUIP_ID      |
| opEquipName  | 機台名稱   | string   | E.EQUIP_NAME    |
| opCqFlag     | 體積或計量 | string   | E.CQ_FLAG       |
| opNp         | 機台人數   | string   | E.NP            |
| opBiaxisFlag | 雙軸       | string   | E.BIAXIS_FLAG   |
| opProdType   | 生產型態   | string   | P.PROD_TYPE     |

#### Response 範例

```json
{
    "msgCode": null,
    "result": {
       "content": [
            {"opEquipId":"601","opEquipName":"憲成#1","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":null,"opProdType":null},
            {"opEquipId":"602","opEquipName":"憲成#2","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":null,"opProdType":null},
            {"opEquipId":"603","opEquipName":"憲成#3","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":null,"opProdType":null},
            {"opEquipId":"604","opEquipName":"憲成#4","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":null,"opProdType":null},
            {"opEquipId":"605","opEquipName":"憲成#5","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":null,"opProdType":null},
            {"opEquipId":"606","opEquipName":"憲成#6","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":null,"opProdType":null},
            {"opEquipId":"607","opEquipName":"憲成#7","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":null,"opProdType":null},
            {"opEquipId":"608","opEquipName":"憲成#8","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":null,"opProdType":null},
            {"opEquipId":"609","opEquipName":"憲成#9","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":null,"opProdType":null},
            {"opEquipId":"610","opEquipName":"憲成#10","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":null,"opProdType":null},
            {"opEquipId":"653","opEquipName":"憲成#11","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":null,"opProdType":null},
            {"opEquipId":"61","opEquipName":"實驗#1","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":null,"opProdType":"C"},
            {"opEquipId":"62","opEquipName":"實驗#2","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":null,"opProdType":"C"},
            {"opEquipId":"63","opEquipName":"實驗#3","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":null,"opProdType":"C"},
            {"opEquipId":"65","opEquipName":"實驗#4","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":null,"opProdType":"C"},
            {"opEquipId":"44","opEquipName":"染色少N","opCqFlag":"C","opNp":"0.5","opBiaxisFlag":null,"opProdType":"C"},
            {"opEquipId":"69","opEquipName":"實驗#6","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":null,"opProdType":"C"},
            {"opEquipId":"70","opEquipName":"實驗#Z","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":null,"opProdType":"C"},
            {"opEquipId":"17","opEquipName":"染色少1","opCqFlag":"C","opNp":"0.5","opBiaxisFlag":null,"opProdType":"C"},
            {"opEquipId":"32","opEquipName":"染色少2","opCqFlag":"C","opNp":"0.5","opBiaxisFlag":null,"opProdType":"C"},
            {"opEquipId":"33","opEquipName":"染色少3","opCqFlag":"C","opNp":"0.5","opBiaxisFlag":null,"opProdType":"C"},
            {"opEquipId":"34","opEquipName":"染色少4","opCqFlag":"C","opNp":"0.5","opBiaxisFlag":null,"opProdType":"C"},
            {"opEquipId":"35","opEquipName":"染色少5","opCqFlag":"C","opNp":"0.5","opBiaxisFlag":"Y","opProdType":"C"},
            {"opEquipId":"36","opEquipName":"染色少6","opCqFlag":"C","opNp":"0.5","opBiaxisFlag":null,"opProdType":"C"},
            {"opEquipId":"37","opEquipName":"染色少7","opCqFlag":"C","opNp":"0.5","opBiaxisFlag":null,"opProdType":"C"},
            {"opEquipId":"38","opEquipName":"染色少8","opCqFlag":"C","opNp":"0.5","opBiaxisFlag":null,"opProdType":"C"},
            {"opEquipId":"40","opEquipName":"染色少10","opCqFlag":"C","opNp":"0.5","opBiaxisFlag":"Y","opProdType":"C"},
            {"opEquipId":"46","opEquipName":"複材#6","opCqFlag":"Q","opNp":"1.0","opBiaxisFlag":"Y","opProdType":"M"},
            {"opEquipId":"11","opEquipName":"複材#1","opCqFlag":"C","opNp":"1.0","opBiaxisFlag":"Y","opProdType":"M"},
            {"opEquipId":"12","opEquipName":"複材#2","opCqFlag":"Q","opNp":"1.0","opBiaxisFlag":"Y","opProdType":"M"},
            {"opEquipId":"14","opEquipName":"複材#3","opCqFlag":"Q","opNp":"1.0","opBiaxisFlag":"Y","opProdType":"M"},
            {"opEquipId":"15","opEquipName":"複材#4","opCqFlag":"Q","opNp":"1.0","opBiaxisFlag":"Y","opProdType":"M"},
            {"opEquipId":"16","opEquipName":"複材#5","opCqFlag":"Q","opNp":"1.0","opBiaxisFlag":"Y","opProdType":"M"},
            {"opEquipId":"111","opEquipName":"複材#11(線外)","opCqFlag":null,"opNp":"1.0","opBiaxisFlag":"Y","opProdType":"M"},
            {"opEquipId":"121","opEquipName":"新材料課#1","opCqFlag":"Q","opNp":"1.0","opBiaxisFlag":"Y","opProdType":"M"},
            {"opEquipId":"122","opEquipName":"新材料課#2","opCqFlag":"Q","opNp":"1.0","opBiaxisFlag":"Y","opProdType":"M"},
            {"opEquipId":"123","opEquipName":"新材料課#3","opCqFlag":"Q","opNp":"1.0","opBiaxisFlag":"Y","opProdType":"M"},
            {"opEquipId":"124","opEquipName":"新材料課#4","opCqFlag":"Q","opNp":"1.0","opBiaxisFlag":"Y","opProdType":"M"}
      ]
    }
}
```


