# 複材-PDA-匯出pdf
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容                  | 修改者 |
| -------- | --------------------- | ------ |
| 20230906 | 新增規格              | 黃東俞 |
| 20231012 | 合併預覽和空白PDA邏輯 | Ellen  |

## 來源URL及資料格式

| 項目   | 說明           |
| ------ | -------------- |
| URL    | /pda/pda_m_pdf |
| method | post           |


## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             | 名稱   | 資料型別 | 必填  | 資料儲存 & 說明 |
| ---------------- | ------ | -------- | :---: | --------------- |
| opPdaUid         | PDA ID | string   |   O   | 列印PDA時需有值 |
| opSocUid         | SOC ID | string   |   O   | 預覽PDA時需有值 |
| opOrganizationId | 工廠別 | string   |   O   | 空白PDA時需有值 |


#### Request 範例

```json
{
  "opPdaUid": "2B282E8F-D4C3-4F8B-8A80-A4F76E7F45B5"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 若 opPdaUid有值為列印PDA
  * 將opPdaUid帶入SQL-1取得[PDA資料]
  * 將[PDA資料的]WO_H.WO_UID(工單ID)再帶入SQL-2取[工單資料]
    * 取得工單之後，再用[工單資料]的WO_H.WO_NO(工單號碼)呼叫SAP-1取回傳資料
    * 取T_ORDER的第一筆資料
      * JCoTable T_ORDER=ret.getTableParameterList().getTable("T_ORDER");
      * T_ORDER.setRow(0);
    * 若有取到資料時，將資料取代SQL-2對應的欄位值
  * [工廠別]=WO_H.ORGANIZATION_ID(SQL-2)
  * 取[SOC資料]：將[PDA資料]的SOC_M_UID(SOC ID)帶入SQL-3取[SOC資料]  (和[OPMS_F3.複材-SOC管理-PDA](./OPMS_F3.複材-SOC管理-PDA.md)相同SQL)
  * 填入JASPER-1相關資料
    * 取pres欄位:
      * 將[SOC資料]直接依SQL欄位名稱填入pres
      * today： 取系統時間 yyyy/mm/dd hh/mm/ss
      * title
        * [機台名稱]：取EQUIP_H.EQUIP_NAME(SQL-1)並去除其中的"複材"文字
        * [狀態]：SOC_M.STATUS(SQL-3)="W"時，狀態="(簽核中)",否則為空字串""
        * [製造日期]=PDA_C.MFG_DATE(SQL-1)，若PDA_C.MFG_DATE為空時，則改為"　　年　月　日"
        * [班別]：若未取到CMC_SO_TYPE(SAP-1)時，則為空字串，若CMC_SO_TYPE為"A"，則[班別]="早"。若CMC_SO_TYPE為"B"，[班別]="中"，CMC_SO_TYPE為"C"，[班別]="晚"
        * title = EQUIP_H.EQUIP_NAME(SQL-1) + 狀態 + "　機台:" + [機台名稱] + "　日期:" + [製造日期] +  "　班別:" + [班別]
      * PATTERN_NO：取ITEM_H.PATTERN_NO(SQL-2)，若為null時改填入空字串""
      * WO_NO：取WO_H.WO_NO(SQL-2)
      * SALES_QTY
        * [銷售數量]：取WO_H.SALES_QTY(SQL-2 OR SAP-1)，若為null時轉"N/A "
        * SALES_QTY = [銷售數量] + WO_H.UNIT
      * BATCH_QTY：BATCH_QTY(SQL-2) + WO_H.UNIT
      * ISO_PROD_OPT
        * 若[工廠別]="5000"且 (SOC_M.EQUIP_ID(SQL-1)="45"或"19"時才需要填，否則為null
        * 參考SQL-4取得資料填入
        * SQL-4沒取到資料時，則填入"□一般級  □醫療級"
      * MIX_DESC
        * MIX_DESC(攪拌步驟)預設為空字串
        * 參考SQL-7，帶入WO_H.WO_UID(SQL-2)和SOC_M.EQUIP_ID(SQL-1)，若有取到資料時，再進行以下處理
        * [每Batch用量]=BATCH_QTY(SQL-2)
        * [差異數]：預設0
        * [剩餘數量]：預設BATCH_QTY(SQL-2)
        * [是否顯示整包數]：若PRODUNIT_NO(SQL-2 OR SAP-1)為"211040"則不顯示
        * 取BOM_MIX.MIX_DESC(SQL-7)內所有包含分母為5的分數字串(可能有多個)，若有取到時則重新組合MIX_DESC字串
          * 如果此分數不是最後一個
            * [比例]：將分數字串轉為小數，例如"3/5"轉為0.6
            * [公斤數]=[每Batch用量]x[比例]+[差異數]
            * [整包數]=[公斤數]/25 無條件捨去小數
            * [差異數]=[差異數]+[公斤數]-([整包數]x25)
            * [數量文字]=[整包數]x25 + "KG("+[整包數]+"包)"
            * 將BOM_MIX.MIX_DESC原本此分數字串的部位以[數量文字]取代
            * 計算[剩餘數量]=[剩餘數量]-([整包數]x25)
          * 如果是最後一個
              * [公斤數]=[剩餘數量]
              * [整包數]=[公斤數]/25 無條件捨去小數
              * [差異數]=[公斤數]-([整包數]x25)
              * [數量文字]= [剩餘數量] + "KG(" + [整包數] + "包)"
                * 若[差異數]>0則 則在"包"字和")"間插入 [差異數] + "KG"
              * 將取代BOM_MIX.MIX_DESC原本此分數字串的部位以產生的[數量文字]取代
        * 若取代分數後的MIX_DESC中包含"N4"字串
          * [N4]：先取WO_H.CUS_N_QUANTITY_PER(SQL-2)，若為null，改取WO_H.STD_N_QUANTITY_PER(SQL-2)
          * 有取到值時進行以下處理
          * [N4數字1] = [N4]x[每Batch用量]x0.827 取到個位數
          * [N4數字2] = [N4數字1]x0.9 取到個位數
          * [N4數字3] = [N4數字1]x1.1 取到個位數
          * [N4字串] = "N4[" + [N4數字1] + " g(" + [N4數字2] + "g~" + [N4數字3] + "g)]"
          * 將MIX_DESC中的"N4"用[N4字串]取代
        * 將MIX_DESC內的換行符號"\r\n"和"\n"用空白" "取代
      * 下藥量相關欄位
        * 參考SQL-5，帶入WO_H.WO_UID(SQL-2)和SOC_M.EQUIP_ID(SQL-1) 若有取得資料時，重新計算下欄位值
          * F_1_MTR_RATE ~ F_6_MTR_RATE
            * 取BOM_FED.F_1_RATE(F1下料比例) ~ BOM_FED.F_6_RATE(F6下料比例) 欄位，取兩位小數並加上"%"
            * 若欄位空白或null時，則填"0%"
          * BDP_EXTRA_RATE：BOM_FED.BDP_RATE取兩位小數並加上"%"，若為null或空白時填"0%"
          * F_1_STD, F_1_MAX, F_1_MIN
            * [餵料機下料量] = SOC_M.FEEDER_QTY(SQL-3)
            * F_1_STD = [餵料機下料量] x BOM_FED.F_1_RATE(SQL-5) / 100
            * F_1_MAX = [餵料機下料量] x BOM_FED.F_1_RATE(SQL-5) x 1.01 / 100
            * F_1_MIN = [餵料機下料量] x BOM_FED.F_1_RATE(SQL-5) x 0.99 / 100
            * 以上三個數字大於等於100時取一位小數，小於10時取三位小數，其餘則取兩位小數並轉為字串
          * F_2_STD, F_2_MAX, F_2_MIN ~ F_6_STD, F_6_MAX, F_6_MIN
            * 計算處理方式同 F_1_STD, F_1_MAX, ，只是F_1_RATE改用F_2_RATE ~ F_6_RATE
          * BDP_STD, BDP_MAX, BDP_MIN
            * [工廠別]="5000"時
              * BDP_STD = [餵料機下料量] x BOM_FED.BDP_RATE(SQL-5) / 60 / 100
              * BDP_MAX = [餵料機下料量] x BOM_FED.BDP_RATE(SQL-5) x 1.03 / 60 / 100
              * BDP_MIN = [餵料機下料量] x BOM_FED.BDP_RATE(SQL-5) x 0.97 / 60 / 100
            * [工廠別]="6000"時
              * BDP_STD = [餵料機下料量] x BOM_FED.BDP_RATE(SQL-5) / 100
              * BDP_MAX = [餵料機下料量] x BOM_FED.BDP_RATE(SQL-5) x 1.03 / 100
              * BDP_MIN = [餵料機下料量] x BOM_FED.BDP_RATE(SQL-5) x 0.97 / 100
            * 數字大於等於100時取一位小數，小於10時取三位小數，其餘則取兩位小數並轉為字串
        * 若SQL-5沒查到資料時，F_1_STD, F_1_MAX, F_1_MIN ~ F_6_STD, F_6_MAX, F_6_MIN 和 BDP_STD, BDP_MAX, BDP_MIN這些欄位，若為null時，改填入空字串
      * MFG_QTY
        * 參考SQL-6，帶入EQUIP_ID(SQL-3)和ITEM_NO(SQL-3)
        * 有查詢到結果時，將結果轉為BigDecimal填入MFG_QTY
      * STD_LOSS_RATE
        * SOC_M.EQUIP_ID(SQL-1) <= 13時
          * STD_LOSS_RATE = 10 * 100 / RAW_QTY(SQL-2) + 1.5
        * SOC_M.EQUIP_ID(SQL-1) > 13時
          * STD_LOSS_RATE = 30 * 100 / RAW_QTY(SQL-2) + 0.65
        * 四捨五入至小數點兩位轉為字串
      * BDP_UNIT
        * 工廠別為"5000"："Kg/min"
        * 工廠別不為"5000"："Kg/hr"
      * VACUUM_UNIT
        * 工廠別為"5000"："cmHg"
        * 工廠別不為"5000"："mmHg"
      * PIC_FILENAME
        * 當SOC_M.FILE_NAME(SQL-3)有值時，取檔案 "/jasperReport/PDA_M_PIC/"+SOC_M.FILE_NAME 填入
      * ISO_DESC
        * 工廠別為"5000"時設為"保存期限：3年\nCLT-20QG00046\nCLT-40GG00683-08"
        * 工廠別不為"5000"時設為"保存期限：3年 LS-35QAA0078 LS-40QAA0267-01-Ver.03 "
      * LOGO
        * 工廠別為"5000"時取圖檔路徑 "/jasperReport/logo.png"
        * 工廠別不為"5000"時取圖檔路徑 "/jasperReport/logoLSO.png"
      * MFG_NOTICE：工廠別為"6000"時，若SOC_M.MFG_NOTICE(SQL-2)有值，且內容有換行("\r\n"或"\n")，將換行用一格空白(" ")取代
      * FEEDER_QTY：SOC_M.FEEDER_QTY(SQL-3)有值時，取兩位小數並轉為BigDecimal格式
    * jrds明細行欄位(tableList):
      * [工廠別]為"5000"時新增4筆資料，
        * rowNo設為"0"~"3"
        * TIME_S,TIME_E 預設填null
        * PDA_C.SHIFT_CODE(SQL-1)為"A"時
          * TIME_S="08:00","10:00","12:00","14:00"
          * TIME_E="10:00","12:00","14:00","16:00"
          * TIME_0_S ~ TIME_3_S = "08:00","10:00","12:00","14:00"
          * TIME_0_E ~ TIME_3_E = "10:00","12:00","14:00","16:00"
        * PDA_C.SHIFT_CODE(SQL-1)為"B"時
          * TIME_S="16:00","18:00","20:00","22:00"
          * TIME_E="18:00","20:00","22:00","00:00"
          * TIME_0_S ~ TIME_3_S = "16:00","18:00","20:00","22:00"
          * TIME_0_E ~ TIME_3_E = "18:00","20:00","22:00","00:00"
        * PDA_C.SHIFT_CODE(SQL-1)為"C"時，
          * TIME_S="00:00","02:00","04:00","06:00"
          * TIME_E="02:00","04:00","06:00","08:00"
          * TIME_0_S ~ TIME_3_S = "00:00","02:00","04:00","06:00"
          * TIME_0_E ~ TIME_3_E = "02:00","04:00","06:00","08:00"
      * [工廠別]為"6000"新增6筆資料
        * rowNo設為"0"~"5"
        * PDA_C.SHIFT_CODE(SQL-1)為"A"時，
          * TIME_S="08:00","10:00","12:00","14:00","16:00","18:00"
          * TIME_E="10:00","12:00","14:00","16:00","18:00","20:00"
        * PDA_C.SHIFT_CODE(SQL-1)為"B"時
          * TIME_S="20:00","22:00","00:00","02:00","04:00","06:00"
          * TIME_E="22:00","00:00","02:00","04:00","06:00","08:00"
  * 填入JASPER-2相關資料
    * troPars
      * ITEM_NO：同JASPAER-1的ITEM_NO
      * PATTERN_NO：同JASPAER-1的PATTERN_NO
      * woNo：同JASPAER-1的WO_NO
      * LOGO：同JASPAER-1的LOGO
    * jrds
      * 參考SQL-8取得Trouble情報資料
      * woNo = WO_H.WO_NO(SQL-8)
      * equipName = EQUIP_H.EQUIP_NAME(SQL-8)
      * mfgDate = MFG_DATE(SQL-8)
      * dfQty = WO_TRO.DF_QTY(SQL-8) + WO_TRO.UNIT(SQL-8)
      * dfName = WO_TRO.DF_NAME(SQL-8)
      * dfDesc = WO_TRO.DF_DESC(SQl-8)
      * dfSol = WO_TRO.DF_SOL(SQL-8)
      * status
        * 預設為null
        * 若WO_TRO.STATUS(SQL-8)="Y", status="已結案"
        * 若WO_TRO.STATUS(SQL-8)="P", status="處理中"
* 若 opPdaUid為空：
  * opSocUid有值時為預覽PDA，否則為空白PDA，參考JASPER-1設定參數
  * pres
    * 預覽PDA時，參考SQL-9取得資料填入。若為空白PDA則不須撈取資料
    * 加入其他固定參數值
      * title：'複材課　機台:' + EQUIP_NAME + '　日期:　　年　月　日　班別:'
      * PATTERN_NO: 空字串 ''
      * WO_NO: 空字串 ''
      * SALES_QTY: 空字串 ''
      * BATCH_QTY: 空字串 ''
      * MFG_QTY: null
      * MIX_DESC: 空字串 ''
    * F_1_STD, F_1_MAX, F_1_MIN ~ F_6_STD, F_6_MAX, F_6_MIN 和 BDP_STD, BDP_MAX, BDP_MIN 這些欄位，若為null時，改填入空字串
    * 工廠別：預覽PDA時，取SQL-1的ORGANIZATION_ID，否則取request傳入的opOrganizationId
    * BDP_UNIT
      * 工廠別為'5000'：'Kg/min'
      * 工廠別不為'5000'：'Kg/hr'
    * VACUUM_UNIT
      * 工廠別為'5000'：'cmHg'
      * 工廠別不為'5000'：'mmHg'
    * PIC_FILENAME
      * 當SOC_M.FILE_NAME(SQL-3)有值時，取檔案 "/jasperReport/PDA_M_PIC/"+SOC_M.FILE_NAME 填入
    * ISO_DESC
      * 工廠別為'5000'時設為'保存期限：3年\nCLT-20QG00046\nCLT-40GG00683-08'
      * 工廠別不為'5000'時設為'保存期限：3年 LS-35QAA0078 LS-40QAA0267-01-Ver.03 '
    * LOGO
      * 工廠別為'5000'時取圖檔路徑 '/jasperReport/logo.png'
      * 工廠別不為'5000'時取圖檔路徑 '/jasperReport/logoLSO.png'
    * MFG_NOTICE：工廠別為'6000'時，若MFG_NOTICE有值，且內容有換行('\r\n'或'\n')，將換行用一格空白(' ')取代
    * FEEDER_QTY：FEEDER_QTY有值時，取兩位小數並轉為BigDecimal格式
  * jrds:
    * 工廠別為'5000' 時新增4筆資料，rowNo設為'0'~'3'，否則新增6筆資料，rowNo設為'0'~'5'
    * TIME_S 和 TIME_E 皆填入空字串
* jasper檔案位置：
    * 工廠別為"5000"時：/jasperReport/PDA_M.jasper
    * 工廠別不為"5000"時：/jasperReport/PDA_M_LS.jasper
* 有系統錯誤 return 500,"SYSTEM_ERROR"，正常回傳PDF檔

# Request 後端邏輯說明

SQL-1:

```sql
-- PdaQueryTo.getPdaMSingle
SELECT P.PDA_M_UID PDA_UID, S.SOC_M_UID SOC_UID, P.WO_UID, S.SOC_M_VER SOC_VER, S.EQUIP_ID, E.EQUIP_NAME, S.ITEM_NO,
W.WO_NO, P.MFG_DATE ,P.SHIFT_CODE, S.PUB_DATE, U1.USER_NAME PUBLIC_NAME, P.CDT, U2.USER_NAME CREATE_BY_NAME
FROM PDA_M P
INNER JOIN SOC_M S ON P.SOC_M_UID = S.SOC_M_UID
INNER JOIN WO_H W ON P.WO_UID = W.WO_UID
INNER JOIN BOM_MTM B ON W.BOM_UID = B.BOM_UID
INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
LEFT JOIN SYS_USER U1 ON S.PUBLIC_BY = U1.USER_ID
LEFT JOIN SYS_USER U2 ON P.CREATE_BY = U2.USER_ID
WHERE P.PDA_M_UID = {opPdaUid}
```
SQL-2

```sql
--WoQueryDao.getWoHSingleByUid
SELECT H.WO_UID, H.ACTIVE_FLAG, H.BOM_UID, H.EQUIP_ID, H.ORGANIZATION_ID, H.WO_NO, H.WO_SEQ, H.LOT_NO, H.CUST_PO_NO,
CONVERT(VARCHAR(10),H.WO_DATE,111) AS WO_DATE, H.PRODUNIT_NO, H.CUST_NO, H.CUST_NAME,
H.INDIR_CUST, B.CUST_NAME AS BASE_CUST, H.UNIT, H.WEIGHT, B.CMDI_NO, B.CMDI_NAME, B.COLOR, H.[USE], H.SALES_NO,
case H.RAW_QTY when cast(H.RAW_QTY as int) then cast(CONVERT(int,H.RAW_QTY) as varchar(20)) else cast(CONVERT(FLOAT,H.RAW_QTY) as varchar(20)) end RAW_QTY,
case H.BATCH_QTY when cast(H.BATCH_QTY as int) then cast(CONVERT(int,H.BATCH_QTY) as varchar(20)) else cast(CONVERT(FLOAT,H.BATCH_QTY) as varchar(20)) end BATCH_QTY,
H.BATCH_TIMES, case H.REMAIN_QTY when cast(H.REMAIN_QTY as int) then cast(CONVERT(int,H.REMAIN_QTY) as varchar(20)) else cast(CONVERT(FLOAT,H.REMAIN_QTY) as varchar(20)) end REMAIN_QTY,
H.REMAIN_TIMES, H.MODIFY_FLAG,H.PRINT_FLAG, H.REMARK, H.BOM_REMARK, H.ERP_FLAG,
H.WIP_CLASS_CODE, H.ALT_BOM_PART_NO,H.ALT_BOM_DESIGNATOR,H.ALT_ROUTING_PART_NO,H.ALT_ROUTING_DESIGNATOR, H.COMPLETION_SUBINV,
H.COMPLETION_LOCATOR, H.SALES_ORDER_NO, H.SALES_QTY, H.OEM_ORDER_NO, CONVERT(VARCHAR(10),H.ESTIMATED_SHIP_DATE,111) AS ESTIMATED_SHIP_DATE,
I.PATTERN_NO, B.ITEM_NO, B.ITEM_DESC, B.BOM_NO, B.BOM_NAME, B.BOM_DESC, B.[USE], B.BASE_ITEM_NO, B.BASE_BOM_NO, H.STD_N_QUANTITY_PER,
H.CUS_N_QUANTITY_PER, REPLACE(CONVERT(VARCHAR(16),H.CDT,120),'-','/') AS CDT, H.CREATE_BY, REPLACE(CONVERT(VARCHAR(16),H.UDT,120),'-','/') AS UDT, H.UPDATE_BY,
B.MULTIPLE BOM_MULTIPLE, H.CTN_WEIGHT, H.PROD_REMARK, H.SALES_ORDER_SEQ, B.BIAXIS_ONLY, B.DYE_ONLY, H.GRADE_NO, H.PROD_REMARK, H.PS_FLAG, H.WO_TYPE
FROM WO_H H
INNER JOIN BOM_MTM B ON H.BOM_UID = B.BOM_UID
INNER JOIN ITEM_H I ON B.ITEM_NO = I.ITEM_NO
WHERE H.WO_UID = {PDA_M.WO_UID(SQL-1)}
```

SQL-3
```sql
-- SocMQueryDao.getSocMMap
SELECT E.ORGANIZATION_ID, S.EQUIP_ID, E.EQUIP_NAME, S.ITEM_NO, B.BOM_NO, S.SOC_M_UID, S.SOC_M_VER SOC_VER, S.STATUS, S.MFG_QTY, S.MOLD_SCREEN_SIZE, S.SOAK_LENGTH,
  S.SCREW_TYPE, S.MOLD_HOLE, S.MOLD_APETURE, S.CUTTER_TYPE, S.CUT_TYPE, S.SCREEN_APETURE, S.FEEDER_QTY, S.FEEDER_UNIT,S.METAL_CHECK, S.PROD_DESC,
  S.F_1_MTR_DESC, S.F_1_MTR_RATE, S.F_1_MTR_MIX, S.F_1_MIX_TIME, S.F_1_MIX_TIME_2, S.F_1_SCREW,
  S.F_2_MTR_DESC, S.F_2_MTR_RATE, S.F_2_MTR_MIX, S.F_2_MIX_TIME, S.F_2_MIX_TIME_2, S.F_2_SCREW,
  S.F_3_MTR_DESC, S.F_3_MTR_RATE, S.F_3_MTR_MIX, S.F_3_MIX_TIME, S.F_3_MIX_TIME_2, S.F_3_SCREW,
  S.F_4_MTR_DESC, S.F_4_MTR_RATE, S.F_4_MTR_MIX, S.F_4_MIX_TIME, S.F_4_MIX_TIME_2, S.F_4_SCREW,
  S.F_5_MTR_DESC, S.F_5_MTR_RATE, S.F_5_MTR_MIX, S.F_5_MIX_TIME, S.F_5_MIX_TIME_2, S.F_5_SCREW,
  S.F_6_MTR_DESC, S.F_6_MTR_RATE, S.F_6_MTR_MIX, S.F_6_MIX_TIME, S.F_6_MIX_TIME_2, S.F_6_SCREW,
  S.C_1_STD, S.C_1_MAX, S.C_1_MIN, S.C_2_STD, S.C_2_MAX, S.C_2_MIN, S.C_3_STD, S.C_3_MAX, S.C_3_MIN,
  S.C_4_STD, S.C_4_MAX, S.C_4_MIN, S.C_5_STD, S.C_5_MAX, S.C_5_MIN, S.C_6_STD, S.C_6_MAX, S.C_6_MIN,
  S.C_7_STD, S.C_7_MAX, S.C_7_MIN, S.C_8_STD, S.C_8_MAX, S.C_8_MIN, S.C_9_STD, S.C_9_MAX, S.C_9_MIN,
  S.C_A_STD, S.C_A_MAX, S.C_A_MIN, S.C_B_STD, S.C_B_MAX, S.C_B_MIN, S.C_C_STD, S.C_C_MAX, S.C_C_MIN,
  S.C_D_STD, S.C_D_MAX, S.C_D_MIN, S.C_E_STD, S.C_E_MAX, S.C_E_MIN, S.C_F_STD, S.C_F_MAX, S.C_F_MIN,
  S.F_1_STD, S.F_1_MAX, S.F_1_MIN, S.F_2_STD, S.F_2_MAX, S.F_2_MIN, S.F_3_STD, S.F_3_MAX, S.F_3_MIN,
  S.F_4_STD, S.F_4_MAX, S.F_4_MIN, S.F_5_STD, S.F_5_MAX, S.F_5_MIN, S.F_6_STD, S.F_6_MAX, S.F_6_MIN,
  S.GRANULATOR_RPM_STD, S.GRANULATOR_RPM_MAX, S.GRANULATOR_RPM_MIN,
  S.SCREW_RPM_STD, S.SCREW_RPM_MAX, S.SCREW_RPM_MIN,S.SIDE_RPM_STD, S.SIDE_RPM_MAX, S.SIDE_RPM_MIN, S.WRENCH_RATE, S.RESIN_TEMP, S.DIE_PRESSURE,
  S.VACUUM_INDEX_STD,S.VACUUM_INDEX_MAX,S.VACUUM_INDEX_MIN,
  S.BDP_STD, S.BDP_MAX, S.BDP_MIN, S.B_TEMP_STD, S.B_TEMP_MAX, S.B_TEMP_MIN, S.S_TEMP_STD, S.S_TEMP_MAX, S.S_TEMP_MIN, S.E_TEMP_STD, S.E_TEMP_MAX, S.E_TEMP_MIN,
  S.BDP_EXTRA_RATE, S.SCREEN_FREQ, S.RECORD_FREQ, S.MFG_NOTICE, S.FILE_NAME, S.MIX_DESC, S.MFG_DESC,
  REPLACE(CONVERT(VARCHAR(19),S.CDT,120),'-','/') CDT, S.CREATE_BY,  CONVERT(VARCHAR(10),S.PUB_DATE,111) PUB_DATE, S.PUBLIC_BY, REPLACE(CONVERT(VARCHAR(19),S.UDT,120),'-','/') UDT,
  S.UPDATE_BY, U.USER_NAME PUBLIC_NAME
  FROM SOC_M S
  INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
  LEFT JOIN SYS_USER U ON S.PUBLIC_BY = U.USER_ID
  LEFT JOIN BOM_MTM B ON S.BOM_UID = B.BOM_UID
  WHERE S.SOC_M_UID = {SOC_M.SOC_M_UID(SQL-1)}
```

SQL-4

```sql
-- WoQueryDao.getIsoProdOpt
SELECT OPT_DESC FROM WO_SIS WHERE LIST_HEADER_ID = 158 AND WO_UID = {WO_H.WO_UID(SQL-2)}
```

SQL-5

```sql
-- WoQueryDao.getWoFedWithEquip
SELECT TOP 1 F.EQUIP_ID, F.F_1_RATE, F.F_2_RATE, F.F_3_RATE, F.F_4_RATE, F.F_5_RATE, F.F_6_RATE, F.BDP_RATE,
  CASE WHEN F.EQUIP_ID IS NULL THEN 1 ELSE 0 END EQUIP
FROM WO_H W, BOM_FED F
WHERE W.BOM_UID = F.BOM_UID
AND W.WO_UID = {WO_H.WO_UID(SQL-2)}
AND (F.EQUIP_ID IS NULL OR F.EQUIP_ID = {SOC_M.EQUIP_ID(SQL-1)})
ORDER BY EQUIP
```

SQL-6
```sql
-- IepcQueryDao.getMfgQty
SELECT MAX(MFG_QTY) AS QTY
FROM (SELECT TOP 1 IE.MS_DATE,IE.MFG_QTY
  FROM (SELECT TOP 1 IE.MS_DATE,IE.MFG_QTY
    FROM IE_PC IE
    INNER JOIN EQUIP_H E ON IE.EQUIP_ID = E.EQUIP_ID
    WHERE E.PS_EQUIP_ID = {BOM_MTM.EQUIP_ID(SQL-2)} AND IE.ITEM_NO={BOM_MTM.ITEM_NO(SQL-2 OR SAP-1)}
    ORDER BY IE.MS_DATE DESC) a
```

SQL-7
```sql
-- WoQueryDao.getWoMixDescWithEquip
SELECT B.EQUIP_ID,
  B.MIX_DESC, B.MIX_TIME, B.MFG_DESC
FROM WO_H W
INNER JOIN BOM_MIX B ON W.BOM_UID = B.BOM_UID
WHERE W.WO_UID = {WO_H.WO_UID(SQL-2)}
AND (B.EQUIP_ID IS NULL OR B.EQUIP_ID = {SOC_M.EQUIP_ID(SQL-1)})
ORDER BY EQUIP_ID DESC
```

SQL-8
```sql
-- WoQueryDao.getWoTroQueryResultList
SELECT TOP 1000 T.WO_TRO_UID, T.EQUIP_ID, E.EQUIP_NAME, CONVERT(VARCHAR(10),T.CDT,111) MFG_DATE,
    T.DF_QTY, T.DF_NAME, T.DF_DESC, T.DF_SOL, T.UNIT, T.STATUS, T.WO_UID, H.WO_NO,
    E.PRODUNIT_NO, I.PATTERN_NO, B.ITEM_NO, H.CUST_NAME, P.PRODUNIT_NAME,
    CONVERT(VARCHAR(10),T.CDT,111) +' by '+ U.USER_NAME CREATE_BY_NAME, T.CDT
FROM WO_TRO T
INNER JOIN EQUIP_H E ON T.EQUIP_ID = E.EQUIP_ID
LEFT JOIN SYS_USER U ON T.CREATE_BY = U.USER_ID
INNER JOIN WO_H H ON T.WO_UID = H.WO_UID
INNER JOIN PRODUNIT P ON E.PRODUNIT_NO = P.PRODUNIT_NO
INNER JOIN BOM_MTM B ON H.BOM_UID = B.BOM_UID
INNER JOIN ITEM_H I ON B.ITEM_NO = I.ITEM_NO
WHERE H.ORGANIZATION_ID = {工廠別}
AND B.ITEM_NO like {SOC_M.ITEM_NO(SQL-3)}
AND T.STATUS = 'Y'
ORDER BY T.CDT DESC
```

SQL-9:

```sql
-- SocMQueryDao.getSocMMap
SELECT E.ORGANIZATION_ID, S.EQUIP_ID, E.EQUIP_NAME, S.ITEM_NO, B.BOM_NO, S.SOC_M_UID, S.SOC_M_VER SOC_VER, S.STATUS, S.MFG_QTY, S.MOLD_SCREEN_SIZE, S.SOAK_LENGTH,
  S.SCREW_TYPE, S.MOLD_HOLE, S.MOLD_APETURE, S.CUTTER_TYPE, S.CUT_TYPE, S.SCREEN_APETURE, S.FEEDER_QTY, S.FEEDER_UNIT,S.METAL_CHECK, S.PROD_DESC,
  S.F_1_MTR_DESC, S.F_1_MTR_RATE, S.F_1_MTR_MIX, S.F_1_MIX_TIME, S.F_1_MIX_TIME_2, S.F_1_SCREW,
  S.F_2_MTR_DESC, S.F_2_MTR_RATE, S.F_2_MTR_MIX, S.F_2_MIX_TIME, S.F_2_MIX_TIME_2, S.F_2_SCREW,
  S.F_3_MTR_DESC, S.F_3_MTR_RATE, S.F_3_MTR_MIX, S.F_3_MIX_TIME, S.F_3_MIX_TIME_2, S.F_3_SCREW,
  S.F_4_MTR_DESC, S.F_4_MTR_RATE, S.F_4_MTR_MIX, S.F_4_MIX_TIME, S.F_4_MIX_TIME_2, S.F_4_SCREW,
  S.F_5_MTR_DESC, S.F_5_MTR_RATE, S.F_5_MTR_MIX, S.F_5_MIX_TIME, S.F_5_MIX_TIME_2, S.F_5_SCREW,
  S.F_6_MTR_DESC, S.F_6_MTR_RATE, S.F_6_MTR_MIX, S.F_6_MIX_TIME, S.F_6_MIX_TIME_2, S.F_6_SCREW,
  S.C_1_STD, S.C_1_MAX, S.C_1_MIN, S.C_2_STD, S.C_2_MAX, S.C_2_MIN, S.C_3_STD, S.C_3_MAX, S.C_3_MIN,
  S.C_4_STD, S.C_4_MAX, S.C_4_MIN, S.C_5_STD, S.C_5_MAX, S.C_5_MIN, S.C_6_STD, S.C_6_MAX, S.C_6_MIN,
  S.C_7_STD, S.C_7_MAX, S.C_7_MIN, S.C_8_STD, S.C_8_MAX, S.C_8_MIN, S.C_9_STD, S.C_9_MAX, S.C_9_MIN,
  S.C_A_STD, S.C_A_MAX, S.C_A_MIN, S.C_B_STD, S.C_B_MAX, S.C_B_MIN, S.C_C_STD, S.C_C_MAX, S.C_C_MIN,
  S.C_D_STD, S.C_D_MAX, S.C_D_MIN, S.C_E_STD, S.C_E_MAX, S.C_E_MIN, S.C_F_STD, S.C_F_MAX, S.C_F_MIN,
  S.F_1_STD, S.F_1_MAX, S.F_1_MIN, S.F_2_STD, S.F_2_MAX, S.F_2_MIN, S.F_3_STD, S.F_3_MAX, S.F_3_MIN,
  S.F_4_STD, S.F_4_MAX, S.F_4_MIN, S.F_5_STD, S.F_5_MAX, S.F_5_MIN, S.F_6_STD, S.F_6_MAX, S.F_6_MIN,
  S.GRANULATOR_RPM_STD, S.GRANULATOR_RPM_MAX, S.GRANULATOR_RPM_MIN,
  S.SCREW_RPM_STD, S.SCREW_RPM_MAX, S.SCREW_RPM_MIN,S.SIDE_RPM_STD, S.SIDE_RPM_MAX, S.SIDE_RPM_MIN, S.WRENCH_RATE, S.RESIN_TEMP, S.DIE_PRESSURE,
  S.VACUUM_INDEX_STD,S.VACUUM_INDEX_MAX,S.VACUUM_INDEX_MIN,
  S.BDP_STD, S.BDP_MAX, S.BDP_MIN, S.B_TEMP_STD, S.B_TEMP_MAX, S.B_TEMP_MIN, S.S_TEMP_STD, S.S_TEMP_MAX, S.S_TEMP_MIN, S.E_TEMP_STD, S.E_TEMP_MAX, S.E_TEMP_MIN,
  S.BDP_EXTRA_RATE, S.SCREEN_FREQ, S.RECORD_FREQ, S.MFG_NOTICE, S.FILE_NAME, S.MIX_DESC, S.MFG_DESC,
  REPLACE(CONVERT(VARCHAR(19),S.CDT,120),'-','/') CDT, S.CREATE_BY,  CONVERT(VARCHAR(10),S.PUB_DATE,111) PUB_DATE, S.PUBLIC_BY, REPLACE(CONVERT(VARCHAR(19),S.UDT,120),'-','/') UDT,
  S.UPDATE_BY, U.USER_NAME PUBLIC_NAME
  FROM SOC_M S
  INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
  LEFT JOIN SYS_USER U ON S.PUBLIC_BY = U.USER_ID
  LEFT JOIN BOM_MTM B ON S.BOM_UID = B.BOM_UID
  WHERE S.SOC_M_UID = { opSocUid }
```

SAP-1：
可參考舊版 WoQueryRfc.getWoHSingle

**獲得BAPI方法**
|              | 型態   | 參數值                     | 說明 |
| ------------ | ------ | -------------------------- | ---- |
| BAPIFunction | string | Z_PP_OPMS_PRODUCTION_ORDER |      |

**設定輸入參數**
| 參數名稱 | 參數值                      | 型態         | 說明 |
| -------- | --------------------------- | ------------ | ---- |
| I_WERKS  | WO_H.ORGANIZATION_ID(SQL-2) | string       |      |
| I_AUFNR  | WO_NO(SQL-1)                | string       |      |
| I_ERDAT  |                             | JCoStructure |      |

**設定輸入參數-I_ERDAT**
| 參數名稱 | 參數值     | 型態   | 說明 |
| -------- | ---------- | ------ | ---- |
| ERDAT_FR | "00000000" | string |      |
| ERDAT_TO | "99991231" | string |      |

| 取得回傳表格名稱 |
| ---------------- |
| T_ORDER          |

| SAP欄位名稱 | SAP資料型別 | 欄位         | 說明                                                                                         |
| ----------- | :---------: | ------------ | -------------------------------------------------------------------------------------------- |
| MATNR       |   string    | 料號         | 取代 BOM_MTM.ITEM_NO                                                                         |
| COLOR       |   string    | 色相         | 取代 BOM_MTM.COLOR                                                                           |
| STAND       |   string    | 生產單位     | 過濾前綴0再取代 WO_H.PRODUNIT_NO                                                             |
| STAND_T     |   string    | 生產單位名稱 |                                                                                              |
| AUFNR       |   string    | 工單號碼     | 過濾前綴0再取代 WO_H.WO_NO                                                                   |
| ERNAM       |   string    | 業助代號     | 取代 WO_H.SALES_NO                                                                           |
| KUNNR       |   string    | 客戶代號     | 過濾前綴0再取代 WO_H.CUST_NO                                                                 |
| SORT2       |   string    | 客戶名稱     | 取代 WO_H.CUST_NAME                                                                          |
| IHREZ       |   string    | 間接客戶     | 取代 WO_H.INDIR_CUST                                                                         |
| GRADE       |   string    | Grade No     | 取代 WO_H.GRADE_NO                                                                           |
| AUART       |   string    |              |                                                                                              |
| AUART_T     |   string    |              | 取代 WO_H.WIP_CLASS_CODE                                                                     |
| MAKTX       |   string    | 料號摘要     | 取代 BOM_MTM.ITEM_DESC                                                                       |
| CUSTPO      |   string    | 客戶PO單號   | 取代 WO_H.CUST_PO_NO                                                                         |
| VORNR       |   string    |              | 過濾前綴0                                                                                    |
| ARBPL       |   string    |              |                                                                                              |
| GAMNG       |   string    |              | 四捨五入至五位小數並轉為字串                                                                 |
| GMEIN       |   string    |              | 取代 WO_H.UNIT                                                                               |
| STATUS      |   string    |              | 取代 WO_H.ACTIVE_FLAG                                                                        |
| GSTRP       |   string    | 使用日期     | 取代 WO_H.WO_DATE                                                                            |
| GSTRP       |   string    | 使用日期     | 取代 WO_H.WO_DATE  格式：yyyy/MM/dd                                                          |
| ARBPL_T     |   string    | 機台名稱     |                                                                                              |
| SOQTY       |   string    | 銷售數量     | 四捨五入至五位小數並轉為字串 取代 WO_H.SALES_QTY                                             |
| SHIP_DATE   |   string    |              | WO_H.ESTIMATED_SHIP_DATE為空且SHIP_DATE不為空才取代WO_H.ESTIMATED_SHIP_DATE 格式：yyyy/MM/dd |
| LGORT       |   string    |              | 取代 WO_H.COMPLETION_SUBINV                                                                  |
| KDAUF       |   string    |              | 過濾前綴0再取代WO_H.SALES_ORDER_NO                                                           |
| KDPOS       |   string    |              | 過濾前綴0再取代WO_H.SALES_ORDER_SEQ                                                          |
| CMC_SO_TYPE |   string    |              | 四捨五入至五位小數並轉為字串                                                                 |


JASPER-1:
**jasper檔案位置**
|          | 型態   | 參數值           | 說明 |
| -------- | ------ | ---------------- | ---- |
| RealPath | string | 參考後端流程說明 |      |

**jasper fillReport參數**
| 參數名稱 | 名稱 | 型態                | 來源資料 & 說明 |
| -------- | ---- | ------------------- | --------------- |
| pars     |      | Map<String, Object> |                 |
| jrds     |      | JRDataSource        | {tableList}     |


**pars object 參數**
| 參數名稱           | 名稱             | 型態   | 來源資料 & 說明                               |
| ------------------ | ---------------- | ------ | --------------------------------------------- |
| ORGANIZATION_ID    | 工廠別           | string | EQUIP_H.ORGANIZATION_I                        |
| EQUIP_ID           | 機台             | string | SOC_M.EQUIP_ID                                |
| EQUIP_NAME         | 機台名稱         | string | EQUIP_H.EQUIP_NAME                            |
| ITEM_NO            | 料號             | string | SOC_M.ITEM_NO                                 |
| BOM_NO             | 配方BOM          | string | BOM_MTM.BOM_NO                                |
| SOC_M_UID          | ID               | string | SOC_M.SOC_M_UID                               |
| SOC_VER            | 版號             | string | SOC_M.SOC_M_VER                               |
| STATUS             | 狀態             | string | SOC_M.STATUS                                  |
| MOLD_SCREEN_SIZE   | 模頭濾網         | string | SOC_M.MOLD_SCREEN_SIZE                        |
| SOAK_LENGTH        | 大槽膠條浸水長度 | string | SOC_M.SOAK_LENGTH                             |
| SCREW_TYPE         | 螺桿型號         | string | SOC_M.SCREW_TYPE                              |
| MOLD_HOLE          | 模嘴孔數         | string | SOC_M.MOLD_HOLE                               |
| MOLD_APETURE       | 模嘴孔徑         | string | SOC_M.MOLD_APETURE                            |
| CUTTER_TYPE        | 切刀型態         | string | SOC_M.CUTTER_TYPE                             |
| CUT_TYPE           | 造粒方式         | string | SOC_M.CUT_TYPE                                |
| SCREEN_APETURE     | 篩網孔徑         | string | SOC_M.SCREEN_APETURE                          |
| FEEDER_QTY         | 餵料機下料量     | string | SOC_M.FEEDER_QTY                              |
| FEEDER_UNIT        | 下料單位         | string | SOC_M.FEEDER_UNIT                             |
| METAL_CHECK        | 金檢機           | string | SOC_M.METAL_CHECK                             |
| PROD_DESC          | 產品說明         | string | SOC_M.PROD_DESC                               |
| F_1_MTR_DESC       | F1原料說明       | string | SOC_M.F_1_MTR_DESC                            |
| F_1_MTR_RATE       | 失效:F1下料比例  | string | SOC_M.F_1_MTR_RATE                            |
| F_1_MTR_MIX        | F1混合方式       | string | SOC_M.F_1_MTR_MIX                             |
| F_1_MIX_TIME       | F1混合時間       | string | SOC_M.F_1_MIX_TIME                            |
| F_1_MIX_TIME_2     | F1混合時間2      | string | SOC_M.F_1_MIX_TIME_2                          |
| F_1_SCREW          | F1餵料螺桿       | string | SOC_M.F_1_SCREW                               |
| F_2_MTR_DESC       | F2原料說明       | string | SOC_M.F_2_MTR_DESC                            |
| F_2_MTR_RATE       | 失效:F2下料比例  | string | SOC_M.F_2_MTR_RATE                            |
| F_2_MTR_MIX        | F2混合方式       | string | SOC_M.F_2_MTR_MIX                             |
| F_2_MIX_TIME       | F2混合時間       | string | SOC_M.F_2_MIX_TIME                            |
| F_2_MIX_TIME_2     | F2混合時間2      | string | SOC_M.F_2_MIX_TIME_2                          |
| F_2_SCREW          | F2餵料螺桿       | string | SOC_M.F_2_SCREW                               |
| F_3_MTR_DESC       | F3原料說明       | string | SOC_M.F_3_MTR_DESC                            |
| F_3_MTR_RATE       | 失效:F3下料比例  | string | SOC_M.F_3_MTR_RATE                            |
| F_3_MTR_MIX        | F3混合方式       | string | SOC_M.F_3_MTR_MIX                             |
| F_3_MIX_TIME       | F3混合時間       | string | SOC_M.F_3_MIX_TIME                            |
| F_3_MIX_TIME_2     | F3混合時間2      | string | SOC_M.F_3_MIX_TIME_2                          |
| F_3_SCREW          | F3餵料螺桿       | string | SOC_M.F_3_SCREW                               |
| F_4_MTR_DESC       | F4原料說明       | string | SOC_M.F_4_MTR_DESC                            |
| F_4_MTR_RATE       | 失效:F4下料比例  | string | SOC_M.F_4_MTR_RATE                            |
| F_4_MTR_MIX        | F4混合方式       | string | SOC_M.F_4_MTR_MIX                             |
| F_4_MIX_TIME       | F4混合時間       | string | SOC_M.F_4_MIX_TIME                            |
| F_4_MIX_TIME_2     | F4混合時間2      | string | SOC_M.F_4_MIX_TIME_2                          |
| F_4_SCREW          | F4餵料螺桿       | string | SOC_M.F_4_SCREW                               |
| F_5_MTR_DESC       | F5原料說明       | string | SOC_M.F_5_MTR_DESC                            |
| F_5_MTR_RATE       | 失效:F5下料比例  | string | SOC_M.F_5_MTR_RATE                            |
| F_5_MTR_MIX        | F5混合方式       | string | SOC_M.F_5_MTR_MIX                             |
| F_5_MIX_TIME       | F5混合時間       | string | SOC_M.F_5_MIX_TIME                            |
| F_5_MIX_TIME_2     | F5混合時間2      | string | SOC_M.F_5_MIX_TIME_2                          |
| F_5_SCREW          | F5餵料螺桿       | string | SOC_M.F_5_SCREW                               |
| F_6_MTR_DESC       | F6原料說明       | string | SOC_M.F_6_MTR_DESC                            |
| F_6_MTR_RATE       | 失效:F6下料比例  | string | SOC_M.F_6_MTR_RATE                            |
| F_6_MTR_MIX        | F6混合方式       | string | SOC_M.F_6_MTR_MIX                             |
| F_6_MIX_TIME       | F6混合時間       | string | SOC_M.F_6_MIX_TIME                            |
| F_6_MIX_TIME_2     | F6混合時間2      | string | SOC_M.F_6_MIX_TIME_2                          |
| F_6_SCREW          | F6餵料螺桿       | string | SOC_M.F_6_SCREW                               |
| C_1_STD            | C1標準           | string | SOC_M.C_1_STD                                 |
| C_1_MAX            | C1上限           | string | SOC_M.C_1_MAX                                 |
| C_1_MIN            | C1下限           | string | SOC_M.C_1_MIN                                 |
| C_2_MAX            | C2標準           | string | SOC_M.C_2_STD                                 |
| C_2_MAX            | C2上限           | string | SOC_M.C_2_MAX                                 |
| C_2_MIN            | C2下限           | string | SOC_M.C_2_MIN                                 |
| C_3_STD            | C3標準           | string | SOC_M.C_3_STD                                 |
| C_3_MAX            | C3上限           | string | SOC_M.C_3_MAX                                 |
| C_3_MIN            | C3下限           | string | SOC_M.C_3_MIN                                 |
| C_4_STD            | C4標準           | string | SOC_M.C_4_STD                                 |
| C_4_MAX            | C4上限           | string | SOC_M.C_4_MAX                                 |
| C_4_MIN            | C4下限           | string | SOC_M.C_4_MIN                                 |
| C_5_STD            | C5標準           | string | SOC_M.C_5_STD                                 |
| C_5_MAX            | C5上限           | string | SOC_M.C_5_MAX                                 |
| C_5_MIN            | C5下限           | string | SOC_M.C_5_MIN                                 |
| C_6_STD            | C6標準           | string | SOC_M.C_6_STD                                 |
| C_6_MAX            | C6上限           | string | SOC_M.C_6_MAX                                 |
| C_6_MIN            | C6下限           | string | SOC_M.C_6_MIN                                 |
| C_7_STD            | C7標準           | string | SOC_M.C_7_STD                                 |
| C_7_MAX            | C7上限           | string | SOC_M.C_7_MAX                                 |
| C_7_MIN            | C7下限           | string | SOC_M.C_7_MIN                                 |
| C_8_STD            | C8標準           | string | SOC_M.C_8_STD                                 |
| C_8_MAX            | C8上限           | string | SOC_M.C_8_MAX                                 |
| C_8_MIN            | C8下限           | string | SOC_M.C_8_MIN                                 |
| C_9_STD            | C9標準           | string | SOC_M.C_9_STD                                 |
| C_9_MAX            | C9上限           | string | SOC_M.C_9_MAX                                 |
| C_9_MIN            | C9下限           | string | SOC_M.C_9_MIN                                 |
| C_A_STD            | C10標準          | string | SOC_M.C_A_STD                                 |
| C_A_MAX            | C10上限          | string | SOC_M.C_A_MAX                                 |
| C_A_MIN            | C10下限          | string | SOC_M.C_A_MIN                                 |
| C_B_STD            | C11標準          | string | SOC_M.C_B_STD                                 |
| C_B_MAX            | C11上限          | string | SOC_M.C_B_MAX                                 |
| C_B_MIN            | C11下限          | string | SOC_M.C_B_MIN                                 |
| C_C_STD            | C12標準          | string | SOC_M.C_C_STD                                 |
| C_C_MAX            | C12上限          | string | SOC_M.C_C_MAX                                 |
| C_C_MIN            | C12下限          | string | SOC_M.C_C_MIN                                 |
| C_D_STD            | C13標準          | string | SOC_M.C_D_STD                                 |
| C_D_MAX            | C13上限          | string | SOC_M.C_D_MAX                                 |
| C_D_MIN            | C13下限          | string | SOC_M.C_D_MIN                                 |
| C_E_STD            | C14標準          | string | SOC_M.C_E_STD                                 |
| C_E_MAX            | C14上限          | string | SOC_M.C_E_MAX                                 |
| C_E_MIN            | C14下限          | string | SOC_M.C_E_MIN                                 |
| C_F_STD            | C15標準          | string | SOC_M.C_F_STD                                 |
| C_F_MAX            | C15上限          | string | SOC_M.C_F_MAX                                 |
| C_F_MIN            | C15下限          | string | SOC_M.C_F_MIN                                 |
| F_1_STD            | F1標準           | string | SOC_M.F_1_STD                                 |
| F_1_MAX            | F1上限           | string | SOC_M.C_1_MAX                                 |
| F_1_MIN            | F1下限           | string | SOC_M.F_1_MIN                                 |
| F_2_MAX            | F2標準           | string | SOC_M.F_2_STD                                 |
| F_2_MAX            | F2上限           | string | SOC_M.F_2_MAX                                 |
| F_2_MIN            | F2下限           | string | SOC_M.F_2_MIN                                 |
| F_3_STD            | F3標準           | string | SOC_M.F_3_STD                                 |
| F_3_MAX            | F3上限           | string | SOC_M.F_3_MAX                                 |
| F_3_MIN            | F3下限           | string | SOC_M.F_3_MIN                                 |
| F_4_STD            | F4標準           | string | SOC_M.F_4_STD                                 |
| F_4_MAX            | F4上限           | string | SOC_M.F_4_MAX                                 |
| F_4_MIN            | F4下限           | string | SOC_M.F_4_MIN                                 |
| F_5_STD            | F5標準           | string | SOC_M.F_5_STD                                 |
| F_5_MAX            | F5上限           | string | SOC_M.F_5_MAX                                 |
| F_5_MIN            | F5下限           | string | SOC_M.F_5_MIN                                 |
| F_6_STD            | F6標準           | string | SOC_M.F_6_STD                                 |
| F_6_MAX            | F6上限           | string | SOC_M.F_6_MAX                                 |
| F_6_MIN            | F6下限           | string | SOC_M.F_6_MIN                                 |
| GRANULATOR_RPM_STD |                  | string | SOC_M.GRANULATOR_RPM_STD                      |
| GRANULATOR_RPM_MAX |                  | string | SOC_M.GRANULATOR_RPM_MAX                      |
| GRANULATOR_RPM_MIN |                  | string | SOC_M.GRANULATOR_RPM_MIN                      |
| SCREW_RPM_STD      | 螺桿轉速         | string | SOC_M.SCREW_RPM_STD                           |
| SCREW_RPM_MAX      | 螺桿轉速上限     | string | SOC_M.SCREW_RPM_MAX                           |
| SCREW_RPM_MIN      | 螺桿轉速下限     | string | SOC_M.SCREW_RPM_MIN                           |
| SIDE_RPM_STD       | 側餵料機轉速     | string | SOC_M.SIDE_RPM_STD                            |
| SIDE_RPM_MAX       | 側餵料機轉速上限 | string | SOC_M.SIDE_RPM_MAX                            |
| SIDE_RPM_MIN       | 側餵料機轉速下限 | string | SOC_M.SIDE_RPM_MIN                            |
| WRENCH_RATE        | 扭力             | string | SOC_M.WRENCH_RATE                             |
| RESIN_TEMP         |                  | string | SOC_M.RESIN_TEMP                              |
| DIE_PRESSURE       | 模頭壓力         | string | SOC_M.DIE_PRESSURE                            |
| VACUUM_INDEX_STD   | 真空指數         | string | SOC_M.VACUUM_INDEX_STD                        |
| VACUUM_INDEX_MAX   | 真空指數         | string | SOC_M.VACUUM_INDEX_MAX                        |
| VACUUM_INDEX_MIN   | 真空指數         | string | SOC_M.VACUUM_INDEX_MIN                        |
| BDP_STD            | 失效:BDP標準     | string | SOC_M.BDP_STD                                 |
| BDP_MAX            | 失效:BDP上限     | string | SOC_M.BDP_MAX                                 |
| BDP_MIN            | 失效:BDP下限     | string | SOC_M.BDP_MIN                                 |
| B_TEMP_STD         | 大槽水溫標準值   | string | SOC_M.B_TEMP_STD                              |
| B_TEMP_MAX         | 大槽水溫上限     | string | SOC_M.B_TEMP_MAX                              |
| B_TEMP_MIN         | 大槽水溫下限     | string | SOC_M.B_TEMP_MIN                              |
| S_TEMP_STD         | 小槽水溫標準值   | string | SOC_M.S_TEMP_STD                              |
| S_TEMP_MAX         | 小槽水溫上限     | string | SOC_M.S_TEMP_MAX                              |
| S_TEMP_MIN         | 小槽水溫下限     | string | SOC_M.S_TEMP_MIN                              |
| E_TEMP_STD         | 第三段水溫標準值 | string | SOC_M.E_TEMP_STD                              |
| E_TEMP_MAX         | 第三段水溫上限   | string | SOC_M.E_TEMP_MAX                              |
| E_TEMP_MIN         | 第三段水溫下限   | string | SOC_M.E_TEMP_MIN                              |
| BDP_EXTRA_RATE     | 失效:BDP外加量   | string | SOC_M.BDP_EXTRA_RATE                          |
| SCREEN_FREQ        | 網目更換         | string | SOC_M.SCREEN_FREQ                             |
| RECORD_FREQ        | 記錄頻率         | string | SOC_M.RECORD_FREQ                             |
| MFG_NOTICE         | 作業注意事項     | string | SOC_M.MFG_NOTICE                              |
| FILE_NAME          | 圖檔             | string | SOC_M.FILE_NAME                               |
| MFG_DESC           | 生產方式說明     | string | SOC_M.MFG_DESC                                |
| CDT                | 建立日期         | string | SOC_M.CDT yyyy/mm/dd hh:mm:ss                 |
| CREATE_BY          | 建立人員         | string | SOC_M.CREATE_BY                               |
| PUB_DATE           | 發行日期         | string | SOC_M.PUB_DATE  yyyy/mm/dd                    |
| PUBLIC_BY          | 發行人代碼       | string | SOC_M.PUBLIC_BY                               |
| PUBLIC_NAME        | 發行人名稱       | string | SYS_USER.USER_NAME                            |
| UDT                | 更新日期         | string | SOC_M.UDT  yyyy/mm/dd hh:mm:ss                |
| UPDATE_BY          | 更新人員         | string | SOC_M.UPDATE_BY                               |
| title              |                  | string |                                               |
| PATTERN_NO         |                  | string |                                               |
| WO_NO              |                  | string |                                               |
| SALES_QTY          |                  | string |                                               |
| BATCH_QTY          |                  | string |                                               |
| MFG_QTY            |                  | string |                                               |
| MIX_DESC           |                  | string |                                               |
| PIC_FILENAME       |                  | string | 取檔案 '/jasperReport/PDA_M_PIC/' + FILE_NAME |
| BDP_UNIT           |                  | string | Kg/min, Kg/hr                                 |
| VACUUM_UNIT        |                  | string | cmHg, mmHg                                    |
| today              |                  | string | 系統日期 yyyy/mm/dd hh:mm:ss                  |
| ISO_DESC           |                  | string |                                               |
| LOGO               |                  | string |                                               |

**jrds object 參數**
| 參數名稱 | 名稱 | 資料型別 | 資料儲存 & 說明 |
| -------- | ---- | -------- | --------------- |
| rowNo    |      | string   |                 |
| TIME_S   |      | string   |                 |
| TIME_E   |      | string   |                 |

**jrds object 參數**
| 參數名稱 | 名稱 | 資料型別 | 資料儲存 & 說明 |
| -------- | ---- | -------- | --------------- |
| rowNo    |      | string   |                 |
| TIME_S   |      | string   |                 |
| TIME_E   |      | string   |                 |


JASPER-2:
**jasper檔案位置**
|          | 型態   | 參數值                          | 說明 |
| -------- | ------ | ------------------------------- | ---- |
| RealPath | string | /jasperReport/WoPrintTro.jasper |      |

**jasper fillReport參數**
| 參數名稱 | 名稱 | 型態                | 來源資料 & 說明 |
| -------- | ---- | ------------------- | --------------- |
| troPars  |      | Map<String, Object> |                 |
| jrds     |      | JRDataSource        | {troDetail}     |

**troPars object 參數**
| 參數名稱  | 名稱 | 資料型別 | 資料儲存 & 說明 |
| --------- | ---- | -------- | --------------- |
| itemNo    |      | string   |                 |
| patternNo |      | string   |                 |
| woNo      |      | string   |                 |
| LOGO      |      | string   |                 |

**jrds object 參數**
| 參數名稱  | 名稱 | 資料型別 | 資料儲存 & 說明 |
| --------- | ---- | -------- | --------------- |
| woNo      |      | string   |                 |
| equipName |      | string   |                 |
| mfgDate   |      | string   |                 |
| dfQty     |      | string   |                 |
| dfName    |      | string   |                 |
| dfDesc    |      | string   |                 |
| dfSol     |      | string   |                 |
| status    |      | string   |                 |


