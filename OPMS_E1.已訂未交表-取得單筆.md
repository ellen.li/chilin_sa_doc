# 已訂未交表-取得單筆
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

已訂未交表-取得單筆

## 修改歷程

| 修改時間 | 內容               | 修改者 |
| -------- | ------------------ | ------ |
| 20230921 | 新增規格           | Ellen  |
| 20230925 | 增加回傳opBatchQty | Ellen  |


## 來源URL及資料格式

| 項目   | 說明          |
| ------ | ------------- |
| URL    | /wops/get_one |
| method | post          |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位    | 名稱    | 資料型別 | 必填  | 資料儲存 & 說明 |
| ------- | ------- | :------: | :---: | --------------- |
| opWoUid | 工單UID |  string  |   M   |                 |

#### Request 範例

```json
{
  "opWoUid": "2AED5F2A-2974-4C29-88E8-A59B45513DF3"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* {organizationId} 為登入者工廠別ID
* 執行SQL-1取得已訂未交表資料，如果不存在 return 400 NOTFOUND
* 執行 SQL-2 取得排班數
* 執行 SQL-3 取得適用機台，結果以分號`;`連接轉為字串
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1: 取得已訂未交表
```sql
SELECT
  U.PRODUNIT_NAME,
  CONVERT (VARCHAR (10), H.WO_DATE, 111) AS WO_DATE,
  H.WO_SEQ,
  H.WO_NO,
  H.ACTIVE_FLAG,
  H.PS_FLAG,
  H.CUST_NAME,
  I.PATTERN_NO,
  B.ITEM_NO,
  H.GRADE_NO AS MTR_NAME,
  H.CTN_WEIGHT,
  H.SALES_ORDER_NO,
  H.SALES_ORDER_SEQ,
  H.BATCH_QTY,
  H.BATCH_TIMES,
  H.REMAIN_QTY,
  H.SALES_QTY,
  H.RAW_QTY,
  CONVERT (VARCHAR (10), H.DD_SALES, 111) AS DD_SALES,
  CONVERT (VARCHAR (10), H.DD_MFG, 111) AS DD_MFG,
  CONVERT (VARCHAR (10), H.ESTIMATED_SHIP_DATE, 111) AS ESTIMATED_SHIP_DATE,
  H.REMARK,
  H.PS_REMARK,
  H.INV_QTY,
  H.MFG_QTY,
  H.CUST_PO_NO,
  H.PS_EQUIP_ID,
  PSE.EQUIP_NAME PS_EQUIP_NAME,
  E.EQUIP_NAME,
  H.WO_UID,
  H.BOM_UID,
  B.BOM_NO 
FROM
  WO_H H
  INNER JOIN PRODUNIT U ON H.ORGANIZATION_ID= U.ORGANIZATION_ID AND H.PRODUNIT_NO= U.SAP_PRODUNIT
  INNER JOIN BOM_MTM B ON H.BOM_UID= B.BOM_UID
  INNER JOIN ITEM_H I ON B.ITEM_NO= I.ITEM_NO
  LEFT JOIN EQUIP_H E ON H.EQUIP_ID= E.EQUIP_ID
  LEFT JOIN EQUIP_H PSE ON H.PS_EQUIP_ID= PSE.EQUIP_ID 
WHERE
  H.ORGANIZATION_ID = {organizationId}
  AND H.WO_UID = {opWoUid}
```

SQL-2: 取得排班數
```sql
SELECT
  COUNT (*) PS_CNT 
FROM
  WO_H H
  INNER JOIN PS_H P ON H.WO_UID= P.WO_UID 
WHERE
  H.ORGANIZATION_ID = {organizationId}
  AND H.PS_FLAG= 'Y' 
  AND H.WO_UID = {WO_UID(SQL-1)}
```

SQL-3: 取得適用機台
```sql
SELECT
  DISTINCT E.EQUIP_NAME 
FROM
  BOM_EQUIP B
  INNER JOIN EQUIP_H E ON B.EQUIP_ID= E.EQUIP_ID 
WHERE
  B.ACTIVE_FLAG= 'Y' 
  AND B.BOM_UID = {BOM_UID(SQL-1)}
```

# Response 欄位
| 欄位                | 名稱             | 資料型別 | 來源資料 & 說明                             |
| ------------------- | ---------------- | -------- | ------------------------------------------- |
| opPsFlag            | 狀態             | string   | PS_FLAG(SQL-1)，F: 完工 / N: 失效 / Y: 生效 |
| opActiveFlag        | 工單狀態         | string   | ACTIVE_FLAG(SQL-1) 轉換為 CRTD: `已開立未核發` / REL: `已核發未結案` / TECO: `結案` / DLFL: `已上刪除旗標` / 其餘: `-` |
| opPsCnt             | 排班數           | string   | PS_CNT(SQL-2)                               |
| opProdunitName      | 生產單位         | string   | PRODUNIT_NAME(SQL-1)                        |
| opWoDate            | 訂單日           | string   | WO_DATE(SQL-1)                              |
| opCustName          | 客戶名稱         | string   | CUST_NAME(SQL-1)                            |
| opPatternNo         | 色號             | string   | PATTERN_NO(SQL-1)                           |
| opItemNo            | 料號             | string   | ITEM_NO(SQL-1)                              |
| opBomUid            | BOM代碼          | string   | BOM_UID(SQL-1)                              |
| opBomNo             | 替代結構         | string   | BOM_NO(SQL-1)                               |
| opMtrName           | 原料             | string   | MTR_NAME(SQL-1)                             |
| opCtnWeight         | 桶重             | string   | CTN_WEIGHT(SQL-1)                           |
| opSalesOrderNo      | 銷售訂單代號     | string   | SALES_ORDER_NO(SQL-1)                       |
| opSalesOrderSeq     | 銷售訂單明細行號 | string   | SALES_ORDER_SEQ(SQL-1)                      |
| opBatchQty          | 每Batch用量      | string   | BATCH_QTY(SQL-1)                           |
| opBatchTimes        | 桶數             | string   | BATCH_TIMES(SQL-1)                          |
| opRemainQty         | 尾數             | string   | REMAIN_QTY(SQL-1)                           |
| opSalesQty          | 訂單數量         | string   | SALES_QTY(SQL-1)                            |
| opRawQty            | 原料量           | string   | RAW_QTY(SQL-1)                              |
| opWoNo              | 工單號碼         | string   | WO_NO(SQL-1)                                |
| opWoUid             | 工單ID           | string   | WO_UID(SQL-1)                               |
| opDdSales           | 業務交期         | string   | DD_SALES(SQL-1)                             |
| opDdMfg             | 現場交期         | string   | DD_MFG(SQL-1)                               |
| opEstimatedShipDate | 預計出貨日       | string   | ESTIMATED_SHIP_DATE(SQL-1)                  |
| opPsEquipId         | 預排機台代號     | string   | PS_EQUIP_ID(SQL-1)                          |
| opPsEquipName       | 預排機台         | string   | PS_EQUIP_NAME(SQL-1)                        |
| opEquipName         | 機台             | string   | EQUIP_NAME(SQL-1)                          |
| opEquipList         | 適用機台         | string   | EQUIP_NAME(SQL-3)，以分號`;`連接            |
| opRemark            | 工單備註         | string   | REMARK(SQL-1)                               |
| opPsRemark          | 備註             | string   | PS_REMARK(SQL-1)                            |
| opInvQty            | 入庫量           | string   | INV_QTY(SQL-1)                              |
| opMfgQty            | 產量             | string   | MFG_QTY(SQL-1)                              |
| opCustPoNo          | SAP單號          | string   | CUST_PO_NO(SQL-1)                           |


#### Response 範例

```json
{
    "msgCode": null,
    "result": {
      "content":{
        "opPsFlag": "F",
        "opActiveFlag": "已核發未結案",
        "opPsCnt": "2",
        "opProdunitName": "染色課",
        "opWoDate": "2022/12/12",
        "opCustName": "瑞健",
        "opPatternNo": "E92009B4",
        "opItemNo": "LPA747XX1XTAF92613",
        "opBomUid": "33A626E3-A45C-44E7-BE59-77E16D883EBF",
        "opBomNo": "01",
        "opMtrName": "PA-747",
        "opCtnWeight": "1002.6",
        "opSalesOrderNo": "21572541",
        "opSalesOrderSeq": "210",
        "opBatchTimes": "3",
        "opRemainQty": "25",
        "opSalesQty": "3000",
        "opRawQty": "",
        "opWoNo": "51030056",
        "opWoUid": "D65D4B8B-8922-4165-8A3E-489C0B46D7B1",
        "opDdSales": "",
        "opDdMfg": "2023/01/06",
        "opEstimatedShipDate": "",
        "opPsEquipId": "39",
        "opPsEquipName": "染色#5",
        "opEquipName": "染色#5",
        "opEquipList": "染色#1;染色#5;染色#9(原#3)",
        "opRemark":"",
        "opPsRemark": "",
        "opInvQty": "3004.0",
        "opMfgQty": "3000.0",
        "opCustPoNo": "5500040180"
      }
    }
}
```
 