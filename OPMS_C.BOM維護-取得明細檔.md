# BOM維護-取得Bom明細檔
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得Bom明細頁

## 修改歷程

| 修改時間 | 內容                                              | 修改者 |
|----------|-------------------------------------------------|--------|
| 20230525 | 新增規格                                          | Tim    |
| 20230530 | 調整規格                                          | Tim    |
| 20230605 | 調整規格                                          | Tim    |
| 20230615 | 調整規格                                          | Tim    |
| 20230710 | 調整規格                                          | Tim    |
| 20230711 | 調整 opMixTime 範例為字串                         | Nick   |
| 20230714 | 修正OPMS無原料資料，但畫面顯示與SAP筆數不同        | Nick   |
| 20230720 | 調整equipData,mixList,fedList欄位[opStatus]預設值 | Tim    |
| 20230801 | 調整SAP的資料在OPMS找不到新增時，單位用量也需帶入  | Nick   |
| 20230802 | 新增 opBiaxisOnly、opDyeOnly 節點                  | Nick   |
| 20230817 | 調整 狀態 D 的判斷 邏輯                           | Nick   |
| 20231225 | 如果秤料資料庫為 '' 回傳前端給 null               | Nick   |

   
## 來源URL及資料格式

| 項目    | 說明               |
| ------ | ----------------- |
| URL    | /bom/get_bom_detail |
| method | post              |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。  
| 欄位             | 名稱     | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明                                 |
|------------------|--------|----------|:------------------------:|-------------------------------------------------|
| opBomUid         | Bom代碼  | string   |            M             |                                                 |
| opOrganizationId | 工廠別   | string   |            O             | 預設 null ， null:不指定 / 5000:奇菱 / 6000:菱翔 |
| opAction         | 行為模式 | string   |            M             | E:編輯 ， M:色母倍數 ， C:複製                    |
| opMultiple       | 倍數     | integer  |            O             |                                                 |


#### Request 範例

```json
{
  "opBomUid":"XXX15656312-H16548MM",
  "opOrganizationId":"5000",
  "opAction":"E"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 查詢原料資料並取回第一筆 參考 SQL-1
* {mtrList} 查詢原料資料 參考SQL-2
* {pgmList} 查詢色料資料 參考SQL-3
* {produnitList} 依照前端給的{opOrganizationId}查詢資料 參考SQL-8
* {equipList} 依照前端給的{opOrganizationId}查詢資料 參考SQL-9

- 編輯模式{opAction}="E"：
    - {opBomRfcList}依據前端給的條件與SQL-1查詢原料的結果 查詢SAP-1[Z_PP_OPMS_BOM]資料 Request-> I_WERKS = {pOrganizationId}, I_MATNR = {opBomMtmTo}.[ItemNo],I_STLAL = {opBomMtmTo}.[BomNo] 判斷查詢SAP-1資料，如無回傳結果為Null或沒有任何一筆資料則需回覆前端訊息 {opMessage}="SAP BOM表尚未維護，無法比對BOM表內容是否已修改"
    - 如SAP-1回傳的資料{opBomRfcList}有值則進行迴圈並宣告一個變數{isFound}為False，進行交叉比對->後端邏輯說明。
    - 交叉比對完後判斷變數{isFound}是否為Ture
      - 是
        - {opBomRfcList}.[CheckFlag]='Y'
      - 否
        - 判斷欄位[Sortf]是否有值同時值又等於"A"或欄位[PartNo]的值起頭是否為"92-" 如是則將該筆資料新增至原料清單{mtrList}中，如不是則將該筆資料至色料清單{pgmList}中->[DataFlag]="N",[MtrPartNo]/[PgmPartNo]={opBomRfcList}.[PartNo],[Unit]={opBomRfcList}.[Unit],[QuantityPer]={opBomRfcList}.[QuantityPer]
    - 如果SAP-1{opBomRfcList}有回傳資料，則 判斷原料{mtrList}/色料{pgmList}清單是否有資料，如有則依照清單進行迴圈，搜尋每一筆資料的欄位[PartNo]在SAP-1{opBomRfcList}清單資料的欄位[PartNo]是否有相等的值，如找不到任何一筆相等的資料，則將該筆資料的欄位[DataFlag]改成"D"
    - {equipData} 依照前端給的{opBomUid}查詢資料 參考SQL-4 欄位[opStatus]:"M"
    - {mixList} 依照前端給的{opBomUid}查詢資料 參考SQL-5 欄位[opStatus]:"M"
    - {fedList} 依照前端給的{opBomUid}查詢資料 參考SQL-6 欄位[opStatus]:"M" ，判斷{fedList}是否有值，如有值則進入迴圈，用前端給的{opBomUid}與{fedList}清單的欄位[BomFedUid]查詢資料 參考SQL-7 ，如有資料則將資料新增至{fedList}的{fedDList}

- 複製or色母倍數模式：
    - {opCopyFromPartNo} 帶入目前原料資料的{opItemNo}
    - {opFromWeight} 依照原料資料的{opItemNo}查詢資料 參考SQL-10
        - 複製{opAction}="C"：
            - 判斷原料{mtrList}清單是否有值，如有資料則用原料{mtrList}清單進行迴圈，將每一筆的欄位[opDataFlag]="N"
            - 判斷色料{pgmList}清單是否有值，如有資料則用色料{pgmList}清單進行迴圈，將每一筆的欄位[opDataFlag]="N",欄位[opSelfPick]="N",欄位[opFeederSeq]="1"
        - 色母倍數{opAction}="M"：
            - 將前端給的倍數multiple回傳給前端{opMultiple}
            - 判斷原料{mtrList}清單是否有值，如有資料則用原料{mtrList}清單進行迴圈，將每一筆的欄位[opDataFlag]="N"
            - 宣告兩個變數[baseQuantityPerTotal]基本色母所有色料單位用量總和,[tempQuantityPerTotal]N倍色母所有色料單位用量暫存值總和
            - 判斷色料{pgmList}清單是否有值，如有值則依照以下邏輯進行迴圈
                - 第一個迴圈 基本色母所有色料單位用量總和
                    - 假如欄位[Unit]等於"KG"->變數[baseQuantityPerTotal]+=(欄位[QuantityPer]乘1000)
                    - 假如欄位[Unit]等於"G"->變數[baseQuantityPerTotal]+=欄位[QuantityPer]
                - 第二個迴圈 N倍色母所有色料單位用量暫存值總和 宣告一個暫存變數[tempQuantityPer]
                    - 假如變數[baseQuantityPerTotal]+1000等於0->暫存變數[tempQuantityPer]=0
                    - 假如變數[baseQuantityPerTotal]+1000後不等於0->暫存變數[tempQuantityPer]=(欄位[QuantityPer]乘倍數{opMultiple}乘1000)除([baseQuantityPerTotal]+1000)，取5位數並四捨五入
                    - 假如欄位[Unit]等於"KG"->暫存變數[tempQuantityPer]=(暫存變數[tempQuantityPer]乘{opMultiple}乘1000])
                    - 變數[tempQuantityPerTotal]+=暫存變數[tempQuantityPer]
                - 第三個迴圈
                    - 宣告暫存變數[tempQuantityPer]=(欄位[QuantityPer]乘倍數{opMultiple}乘1000)除(變數[baseQuantityPerTotal]+1000)，取5位數並四捨五入
                    - 宣告暫存變數[tempNumber]=1000-變數[tempQuantityPerTotal]
                    - 假如暫存變數[tempNumber]等於0，欄位[opQuantityPer]=0
                    - 假如暫存變數[tempNumber]不等於0，欄位[opQuantityPer]=(暫存變數[tempQuantityPer]乘1000)除暫存變數[tempNumber]，取5位數並四捨五入並SetScale(5, BigDecimal.ROUND_HALF_UP)
                    - 將每一筆的欄位[opDataFlag]="N",欄位[opSelfPick]="N",欄位[opFeederSeq]="1"
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

* 交叉比對 : 判斷原料{mtrList}/色料{pgmList}清單是否有值，如有資料則用原料{mtrList}/色料{pgmList}清單進行迴圈，判斷SAP-1的資料{opBomRfcList}與原料{mtrList}/色料{pgmList}清單的[PartNo值是否相等]，如相等則將變數{isFound}改為Ture繼續[判斷QuantityPer相比不等於0或Unit是否不相等]，如是則將該筆原料/色料資料的欄位[DataFlag]改成"M",欄位[QuantityPer]={opBomRfcList}.[QuantityPer],欄位[Unit]={opBomRfcList}.[Unit]並用原本原料{mtrList}/色料{pgmList}清單的欄位[QuantityPer]去除SAP-1資料{opBomRfcList}的欄位[QuantityPer]並四捨五入ROUND_HALF_DOWN至小數第二位={diffQuantityPer}，然後用{diffQuantityPer}進行判斷，[{diffQuantityPer}.compareTo(0.7)小於等於0或{diffQuantityPer}.compareTo(1.3)大於等於0]，如是則需回傳前端訊息 {opMessage}="{PartNo}用量調整範圍超過30%，請再複核!"

SAP-1：
可參考舊版 rfc_getBomList
程序如有任何異常，紀錄錯誤LOG並回傳NULL
查詢SAP-API回傳結果[T_BOM]參數如下
|              | 型態   | 參數值        | 說明 |
|--------------|--------|---------------|------|
| BAPIFunction | string | Z_PP_OPMS_BOM |      |

| 參數名稱 | 參數值             | 型態   | 說明                                     |
|----------|--------------------|--------|------------------------------------------|
| I_WERKS  | {opOrganizationId} | string |                                          |
| I_MATNR  | {opItemNo}         | string |                                          |
| I_STLAL  | {opBomNo}          | string | 如果{opBomNo}只有一位數，需補足兩位，ex:01 |

| 取得回傳表格名稱 |
| ------------- |
| T_BOM         |

如果回傳資料數量為0則回傳null，否則依下面物件集合回傳
| 物件元素名稱 | 資料型別   | SAP欄位名稱 | SAP資料型別 | 物件元素回傳值                               | 說明                      |
|--------------|------------|-------------|-------------|---------------------------------------------|---------------------------|
| PartNo       | string     | IDNRK       | string      | 同SAP                                        |                           |
| Unit         | string     | MEINS       | string      | 同SAP                                        |                           |
| Sortf        | string     | SORTF       | string      | 同SAP                                        |                           |
| PartName     | string     | MAKTX       | string      | 同SAP                                        |                           |
| QuantityPer  | BigDecimal | MENGE       | string      | SAP["MENGE"]/1000 (結果四捨五入到小數點五位) | 回傳值必須格式化為0.00000 |

SQL-1:

```sql
SELECT B.BOM_UID, B.ORGANIZATION_ID, B.ACTIVE_FLAG, I.PATTERN_NO, B.ITEM_NO, I.ITEM_DESC, I.UNIT, B.BOM_NO, B.BOM_NAME, B.BOM_DESC, B.CUST_NAME, I.CMDI_NO, I.CMDI_NAME, P.COLOR, B.[USE], B.MULTIPLE, B.BASE_ITEM_NO, B.BASE_BOM_NO, B.REMARK, B.ERP_FLAG, B.ASSEMBLY_TYPE, B.BOM_FLAG, REPLACE(CONVERT(VARCHAR(16),B.CDT,120),'-','/') AS CDT, B.CREATE_BY, REPLACE(CONVERT(VARCHAR(16),B.UDT,120),'-','/') AS UDT, B.UPDATE_BY, I.WEIGHT, B.STD_N_QUANTITY_PER, B.CUS_N_QUANTITY_PER, B.BIAXIS_ONLY, B.DYE_ONLY
				 FROM BOM_MTM B
				 INNER JOIN ITEM_H I ON B.ITEM_NO=I.ITEM_NO
				 INNER JOIN PAT_H P ON I.PATTERN_NO=P.PATTERN_NO
				 WHERE B.BOM_UID = {opBomUid}
```

SQL-2:

```sql
SELECT R.MTR_PART_NO,R.QUANTITY_PER,R.UNIT
FROM BOM_MTR R WHERE R.BOM_UID = {opBomUid} 
ORDER BY R.MTR_PART_NO ASC, R.QUANTITY_PER DESC
```

SQL-3:

```sql
SELECT PGM_PART_NO,QUANTITY_PER,UNIT,SITE_PICK,NULLIF(SELF_PICK, '') as SELF_PICK,FEEDER_SEQ 
FROM BOM_PGM WHERE BOM_UID = {opBomUid} 
ORDER BY PGM_PART_NO ASC
```

SQL-4:

```sql
SELECT A.BOM_UID, A.EQUIP_ID, A.ACTIVE_FLAG, A.REMARK, REPLACE(CONVERT(VARCHAR(19),A.UDT,120),'-','/') UDT, A.UPDATE_BY,E.EQUIP_NAME,E.PRODUNIT_NO,P.PRODUNIT_NAME 
    			FROM BOM_EQUIP A
    			INNER JOIN EQUIP_H E ON A.EQUIP_ID=E.EQUIP_ID
    			INNER JOIN PRODUNIT P ON E.ORGANIZATION_ID=P.ORGANIZATION_ID AND E.PRODUNIT_NO=P.PRODUNIT_NO
    			WHERE A.BOM_UID = {opBomUid}
                ORDER BY EQUIP_ID
```

SQL-5:

```sql
SELECT A.BOM_UID, A.BOM_MIX_UID, A.EQUIP_ID, A.MIX_DESC, A.MIX_TIME, A.MFG_DESC, REPLACE(CONVERT(VARCHAR(19),A.UDT,120),'-','/') UDT, A.UPDATE_BY,E.EQUIP_NAME,E.PRODUNIT_NO,P.PRODUNIT_NAME
    				 FROM BOM_MIX A
    				 LEFT JOIN EQUIP_H E ON A.EQUIP_ID=E.EQUIP_ID
    				 LEFT JOIN PRODUNIT P ON E.PRODUNIT_NO=P.PRODUNIT_NO
    				 WHERE A.BOM_UID = {opBomUid} 
                     ORDER BY EQUIP_ID
```

SQL-6:

```sql
SELECT A.BOM_UID, A.BOM_FED_UID, A.EQUIP_ID, A.F_1_RATE, A.F_2_RATE, A.F_3_RATE, A.F_4_RATE, A.F_5_RATE, A.F_6_RATE, A.BDP_RATE, A.PPG_RATE, A.DBE_RATE, REPLACE(CONVERT(VARCHAR(19),A.UDT,120),'-','/') UDT, A.UPDATE_BY, E.EQUIP_NAME,E.PRODUNIT_NO,P.PRODUNIT_NAME 
			       FROM BOM_FED A "
			       LEFT JOIN EQUIP_H E ON A.EQUIP_ID=E.EQUIP_ID
			       LEFT JOIN PRODUNIT P ON E.ORGANIZATION_ID=P.ORGANIZATION_ID AND E.PRODUNIT_NO=P.PRODUNIT_NO 
			       WHERE A.BOM_UID = {opBomUid}
                   ORDER BY EQUIP_ID
```

SQL-7:

```sql
SELECT BOM_UID, BOM_FED_UID, BOM_PART_NO, FEEDER_NO, FEEDER_RATE, REPLACE(CONVERT(VARCHAR(19),UDT,120),'-','/') UDT, UPDATE_BY 
                   FROM BOM_FED_D WHERE BOM_UID = {opBomUid} AND BOM_FED_UID = {bomFedUid} 
                   ORDER BY BOM_PART_NO
```

SQL-8:

```sql
SELECT DISTINCT P.PRODUNIT_NO,P.PRODUNIT_NAME
	    		FROM PRODUNIT P
	    		INNER JOIN EQUIP_H E ON P.ORGANIZATION_ID=E.ORGANIZATION_ID AND P.PRODUNIT_NO=E.PRODUNIT_NO
	    		WHERE P.ORGANIZATION_ID = {opOrganizationId}    
```

SQL-9:

```sql
SELECT E.EQUIP_ID,E.EQUIP_NAME,E.CQ_FLAG,E.NP,E.BIAXIS_FLAG,P.PROD_TYPE
	    			 FROM EQUIP_H E 
	    			 INNER JOIN PRODUNIT P ON E.ORGANIZATION_ID=P.ORGANIZATION_ID AND E.PRODUNIT_NO=P.PRODUNIT_NO
	    			 WHERE E.PS_FLAG='Y' AND E.ORGANIZATION_ID = {opOrganizationId}
                     
                     
```

SQL-10:

```sql
SELECT WEIGHT FROM ITEM_H WHERE ITEM_NO={opItemNo}
```

# Response 欄位
| 欄位              | 名稱                | 資料型別 | 資料儲存 & 說明             |
|-------------------|-------------------|----------|-----------------------------|
| opBomUid          | Bom代碼             | string   | B.BOM_UID(SQL-1)            |
| opOrganizationId  | 工廠別              | string   | B.ORGANIZATION_ID(SQL-1)    |
| opActiveFlag      | 是否有效            | string   | B.ACTIVE_FLAG(SQL-1)        |
| opPatternNo       | 代碼                | string   | I.PATTERN_NO(SQL-1)         |
| opItemNo          | 料號                | string   | B.ITEM_NO(SQL-1)            |
| opBomNo           | 替代結構            | string   | B.BOM_NO(SQL-1)             |
| opBomName         | 替代結構名稱        | string   | B.BOM_NAME(SQL-1)           |
| opBomDesc         | 替代備註            | string   | B.BOM_DESC(SQL-1)           |
| opColor           | 顏色                | string   | P.COLOR(SQL-1)              |
| opCmdiNo          | 品號                | string   | I.CMDI_NO(SQL-1)            |
| opCmdiName        | 品稱                | string   | I.CMDI_NAME(SQL-1)          |
| opCustName        | 客戶名稱            | string   | B.CUST_NAME(SQL-1)          |
| opUse             | 用途                | string   | B.USE(SQL-1)                |
| opMultiple        | 倍數                | string   | B.MULTIPLE(SQL-1)           |
| opBaseItemNo      | 色母基本配方料號    | string   | B.BASE_ITEM_NO(SQL-1)       |
| opBaseBomNo       | 色母基本配方BOM     | string   | B.BASE_BOM_NO(SQL-1)        |
| opRemark          | 備註                | string   | B.REMARK(SQL-1)             |
| opErpFlag         | 是否已拋轉ERP       | string   | B.ERP_FLAG(SQL-1)           |
| opAssemblyType    | 組裝類型            | string   | B.ASSEMBLY_TYPE(SQL-1)      |
| opItemDesc        | 料號摘要            | string   | I.ITEM_DESC(SQL-1)          |
| opUnit            | 計量單位            | string   | I.UNIT(SQL-1)               |
| opWeight          | 重量                | string   | I.WEIGHT(SQL-1)             |
| opBomFlag         | 依此配方            | string   | B.BOM_FLAG(SQL-1)           |
| opStdNQuantityPer | N4單位用量-系統     | string   | B.STD_N_QUANTITY_PER(SQL-1) |
| opCusNQuantityPer | N4單位用量-自行輸入 | string   | B.CUS_N_QUANTITY_PER(SQL-1) |
| opCdt             | 建立日期            | string   | CDT(SQL-1)                  |
| opCreateBy        | 建立人員            | string   | B.CREATE_BY(SQL-1)          |
| opUdt             | 更新日期            | string   | UDT(SQL-1)                  |
| opUpdateBy        | 更新人員            | string   | B.UPDATE_BY(SQL-1)          |
| opCopyFromPartNo  | 複製來源料號        | string   | {opCopyFromPartNo}          |
| opFromWeight      | 來源重量            | string   | WEIGHT(SQL-10)              |
| opBiaxisOnly      | 限雙軸生產          | string   | B.BIAXIS_ONLY(SQL-1)        |
| opDyeOnly         | 只染不押            | string   | B.DYE_ONLY (SQL-1)          |
| mtrList           | 原料清單            | array    |                             |
| pgmList           | 色料清單            | array    |                             |
| equipData         | 設備清單            | array    |                             |
| mixList           | 攪拌方式            | array    |                             |
| fedList           | 下料比例與下料設備  | array    |                             |
| produnitList      | 生產單位選項        | array    |                             |
| equipList         | 機台選項            | array    |                             |
| opMessage         | 訊息                | string   | {opMessage}                 |

#### 【mtrList】array
| 欄位             | 名稱      | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明         |
| --------------- | --------- | ------- | --------------------| --------------------- |
|opPartNo         |料號       | string  |O                     | R.MTR_PART_NO(SQL-2)  |
|opQuantityPer    |單位用量    | string  |O                     | R.QUANTITY_PER(SQL-2)|
|opUnit           |計量單位    | string  |O                     | R.UNIT(SQL-2)        |
|opDataFlag       |資料註記    | string  |O                     | N:新增 M:修改 D:刪除   |

#### 【pgmList】array
| 欄位             | 名稱      | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明         |
| --------------- | --------- | ------- | --------------------| --------------------- |
|opPartNo         |料號       | string  |O                    | PGM_PART_NO(SQL-3)    |
|opQuantityPer    |單位用量    | string  |O                    | QUANTITY_PER(SQL-3)    |
|opUnit           |計量單位    | string  |O                    | UNIT(SQL-3)            |
|opDataFlag       |資料註記    | string  |O                    | N:新增 M:修改 D:刪除     |
|opSitePick       |現場領料    | string  |O                    | SITE_PICK(SQL-3)       |
|opSelfPick       |自秤       | string  |O                    | SELF_PICK(SQL-3)       |
|opFeederSeq      |下料順序    | string  |O                    | FEEDER_SEQ(SQL-3)      |


#### 【equipData】array
| 欄位             | 名稱      | 資料型別  | 必填(非必填節點不需存在) | 來源資料 & 說明          |
| --------------- | --------- | ------- | -------------------- | ---------------------- |
|opEquipId        |機台代碼     | string  |O                     | A.EQUIP_ID(SQL-4)     |
|opEquipName      |機台名稱     | string  |O                     | E.EQUIP_NAME(SQL-4)   |
|opUpdateBy       |更新人員     | string  |O                     | A.UPDATE_BY(SQL-4)    |
|opUdt            |更新日期     | string  |O                     | UDT(SQL-4)            |
|opProdunitNo     |產品單位代碼  | string  |O                     | E.PRODUNIT_NO(SQL-4)  |
|opProdunitName   |產品單位名稱  | string  |O                     | P.PRODUNIT_NAME(SQL-4)|
|opRemark         |備註        | string  |O                     | A.REMARK(SQL-4)       |
|opActiveFlag     |有效註記     | string  |O                     | A.ACTIVE_FLAG(SQL-4)  |
|opStatus         |狀態        | string  |O                     | N:新增 M:修改 D:刪除<br>編輯模式預設:M    |

#### 【mixList】array
| 欄位             | 名稱      | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明           |
| --------------- | --------- | ------- | --------------------| ----------------------- |
|opEquipId        |機台代碼    | string  |O                     | A.EQUIP_ID(SQL-5)      |
|opBomMixUid      |攪拌方式 ID | string  |O                     | A.BOM_MIX_UID(SQL-5)   |
|opMixDesc        |生產方式說明 | string  |O                     | A.MIX_DESC(SQL-5)      |
|opMixTime        |攪拌時間    | string |O                     | A.MIX_TIME(SQL-5)      |
|opMfgDesc        |攪拌步驟    | string  |O                     | A.MFG_DESC(SQL-5)      |
|opEquipName      |機台名稱    | string  |O                     | E.EQUIP_NAME(SQL-5)    |
|opUpdateBy       |更新人員    | string  |O                     | A.UPDATE_BY(SQL-5)     |
|opUdt            |更新日期    | string  |O                     | UDT(SQL-5)             |
|opStatus         |狀態       | string  |O                     | N:新增 M:修改 D:刪除<br>編輯模式預設:M     |
|opProdunitNo     |產品單位代碼 | string  |O                     | E.PRODUNIT_NO(SQL-5)   |
|opProdunitName   |產品單位名稱 | string  |O                     | P.PRODUNIT_NAME(SQL-5) |

#### 【fedList】array
| 欄位             | 名稱            | 資料型別 | 必填(非必填節點不需存在)| 來源資料 & 說明         |
| --------------- | -------------- | ------- | --------------------| --------------------- |
|opEquipId        |機台代碼          | string  |O                    | A.EQUIP_ID(SQL-6)     |
|opBomFedUid      |投料設備與比例ID   | string  |O                    | A.BOM_FED_UID(SQL-6)  |
|opF1Rate         |F1下料比例        | string |O                    | A.F_1_RATE(SQL-6)回傳有小數點之字串     |
|opF2Rate         |F2下料比例        | string |O                    | A.F_2_RATE(SQL-6)回傳有小數點之字串     |
|opF3Rate         |F3下料比例        | string |O                    | A.F_3_RATE(SQL-6)回傳有小數點之字串     |
|opF4Rate         |F4下料比例        | string |O                    | A.F_4_RATE(SQL-6)回傳有小數點之字串     |
|opF5Rate         |F5下料比例        | string |O                    | A.F_5_RATE(SQL-6)回傳有小數點之字串     |
|opF6Rate         |F6下料比例        | string |O                    | A.F_6_RATE(SQL-6)回傳有小數點之字串     |
|opBdpRate        |BDP下料比例       | string |O                    | A.BDP_RATE(SQL-6)回傳有小數點之字串     |
|opPpgRate        |PPG下料比例       | string |O                    | A.PPG_RATE(SQL-6)回傳有小數點之字串     |
|opDbeRate        |DBE下料比例       | string |O                    | A.DBE_RATE(SQL-6)回傳有小數點之字串     |
|opEquipName      |機台名稱          | string  |O                    | E.EQUIP_NAME(SQL-6)   |
|opUpdateBy       |更新人員          | string  |O                    | A.UPDATE_BY(SQL-6)    |
|opUdt            |更新日期          | string  |O                    | UDT(SQL-6)            |
|opStatus         |狀態             | string  |O                    | N:新增 M:修改 D:刪除<br>編輯模式預設:M     |
|opProdunitNo     |生產單位代碼       | string  |O                    | E.PRODUNIT_NO(SQL-6)  |
|opProdunitName   |生產單位名稱       | string  |O                    | P.PRODUNIT_NAME(SQL-6)|
|fedDetailList    |比例與設備明細     | array   |                     |                       |

#### 【fedDetailList】array
| 欄位             | 名稱      | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明         |
| --------------- | --------- | ------- | --------------------| --------------------- |
|opBomPartNo      |替代結構料號 | string  |O                     | BOM_PART_NO(SQL-7)  |
|opFeederNo       |下料設備    | string  |O                     | FEEDER_NO(SQL-7)|
|opFeederRate     |下料比例    | string  |O                     | FEEDER_RATE(SQL-7)        |
|opUpdateBy       |更新人員    | string  |O                     | UPDATE_BY(SQL-7) |
|opUdt            |更新日期    | string  |O                     | UDT(SQL-7)   |

#### 【produnitList】array
| 欄位                | 名稱      | 資料型別  | 必填(非必填節點不需存在) | 來源資料 & 說明        |
| ------------------ | --------  | ------- | -------------------- | -------------------- |
|opProdunitNo        |生產單位代碼 | string  |O                     | P.PRODUNIT_NO(SQL-8)   |
|opProdunitName      |生產單位名稱 | string  |O                     | P.PRODUNIT_NAME(SQL-8) |


#### 【equipList】array
| 欄位         | 名稱     | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明        |
| ----------- | ------- | ------- | -------------------- | -------------------- |
|opEquipId    |機台代碼   | string  |O                     | E.EQUIP_ID(SQL-9)   |
|opEquipName  |機台台稱   | string  |O                     | E.EQUIP_NAME(SQL-9) |
|opCqFlag     |Cq註記    | string  |O                     | E.CQ_FLAG(SQL-9)   |
|opNp         |Np        | string  |O                     | E.NP(SQL-9)   |
|opBiaxisFlag |雙軸註記   | string  |O                     | E.BIAXIS_FLAG(SQL-9)   |
|opProdType   |產品類型   | string  |O                     | P.PROD_TYPE(SQL-9)   |



#### Response 範例

```json
{
    "msgCode": null,
    "result": {
        "opBomUid":null,
        "opOrganizationId":null,
        "opActiveFlag":null,
        "opPatternNo":null,
        "opItemNo":null,
        "opBomNo":null,
        "opBomName":null,
        "opBomDesc":null,
        "opColor":null,
        "opCmdiNo":null,
        "opCmdiName":null,
        "opCustName":null,
        "opUse":null,
        "opMultiple":null,
        "opBaseItemNo":null,
        "opBaseBomNo":null,
        "opRemark":null,
        "opErpFlag":null,
        "opAssemblyType":null,
        "opItemDesc":null,
        "opUnit":null,
        "opWeight":null,
        "opBomFlag":null,
        "opStdNQuantityPer":null,
        "opCusNQuantityPer":null,
        "opCdt":null,
        "opCreateBy":null,
        "opUdt":null,
        "opUpdateBy":null,
        "opBiaxisOnly":"N",
        "opDyeOnly":"N",
        "mtrList":[
            {                
                "opPartNo":"04-105-PC-110UXXXX",
                "opQuantityPer":"1.00000",
                "opUnit":"KG",
                "opDataFlag":null
            },
            {                
                "opPartNo":"04-106-PC-110UXXXX",
                "opQuantityPer":"2.00000",
                "opUnit":"KG",
                "opDataFlag":null
            }
        ],
        "pgmList":[
            {                
                "opPartNo":"04-105-PC-110UXXXX",
                "opQuantityPer":"1.00000",
                "opUnit":"KG",
                "opDataFlag":null,
                "opSitePick":null,
                "opSelfPick":null,
                "opFeederSeq":"1"
            },
            {                
                "opPartNo":"04-105-PC-110UXXXX",
                "opQuantityPer":"1.00000",
                "opUnit":"KG",
                "opDataFlag":null,
                "opSitePick":null,
                "opSelfPick":null,
                "opFeederSeq":"2"
            }
        ],
        "equipAList":[
            {                
                "opEquipId":"511",
                "opEquipName":"良輝#7",
                "opUpdateBy":null,
                "opUdt":null,
                "opProdunitNo":"002",
                "opProdunitName":"良輝",
                "opRemark":"TXT",
                "opActiveFlag":null,
                "opStatus":"N"
            },
            {                
                "opEquipId":"511",
                "opEquipName":"良輝#7",
                "opUpdateBy":null,
                "opUdt":null,
                "opProdunitNo":"002",
                "opProdunitName":"良輝",
                "opRemark":"TXT",
                "opActiveFlag":null,
                "opStatus":"N"
            }
        ],
        "equipBList":[
            {                
                "opEquipId":"511",
                "opEquipName":"良輝#7",
                "opUpdateBy":null,
                "opUdt":null,
                "opProdunitNo":"002",
                "opProdunitName":"良輝",
                "opRemark":"TXT",
                "opActiveFlag":null,
                "opStatus":"N"
            },
            {                
                "opEquipId":"511",
                "opEquipName":"良輝#7",
                "opUpdateBy":null,
                "opUdt":null,
                "opProdunitNo":"002",
                "opProdunitName":"良輝",
                "opRemark":"TXT",
                "opActiveFlag":null,
                "opStatus":"N"
            }
        ],
        "mixList":[
            {                
                "opEquipId":"511",
                "opBomMixUid":"123456",
                "opMixDesc":"TESTB",
                "opMixTime":null,
                "opMfgDesc":"TESTA",
                "opEquipName":"良輝#7",
                "opUpdateBy":null,
                "opUdt":null,
                "opStatus":"N",
                "opProdunitNo":"002",
                "opProdunitName":"良輝"
            },
            {                
                "opEquipId":"512",
                "opBomMixUid":"1234567",
                "opMixDesc":"TESTD",
                "opMixTime":"5",
                "opMfgDesc":"TESTC",
                "opEquipName":"良輝#8",
                "opUpdateBy":null,
                "opUdt":null,
                "opStatus":"N",
                "opProdunitNo":"002",
                "opProdunitName":"良輝"
            }
        ],
        "fedList":[
            {                
                "opEquipId":"511",
                "opBomFedUid":"13256",
                "opF1Rate":100,
                "opF2Rate":0,
                "opF3Rate":0,
                "opF4Rate":0,
                "opF5Rate":0,
                "opF6Rate":0,
                "opBdpRate":0,
                "opPpgRate":0,
                "opDbeRate":0,
                "opEquipName":"良輝#7",
                "opUpdateBy":null,
                "opUdt":null,
                "opStatus":null,
                "opProdunitNo":"002",
                "opProdunitName":"良輝",
                "fedDetailList":[
                   {
                     "opBomPartNo":"04-105-PC-110UXXXX",
                     "opFeederNo":"F1-Mixaco",
                     "opFeederRate":"97.7334"
                   },
                   {
                     "opBomPartNo":"04-105-PC-110UXXXX",
                     "opFeederNo":"F1-Mixaco",
                     "opFeederRate":"97.7334"
                   }
                ]
            },
            {                
                "opEquipId":"511",
                "opBomFedUid":"13256",
                "opF1Rate":100,
                "opF2Rate":0,
                "opF3Rate":0,
                "opF4Rate":0,
                "opF5Rate":0,
                "opF6Rate":0,
                "opBdpRate":0,
                "opPpgRate":0,
                "opDbeRate":0,
                "opEquipName":"良輝#7",
                "opUpdateBy":null,
                "opUdt":null,
                "opStatus":null,
                "opProdunitNo":"002",
                "opProdunitName":"良輝",
                "fedDetailList":[
                   {
                     "opBomPartNo":"04-105-PC-110UXXXX",
                     "opFeederNo":"F1-Mixaco",
                     "opFeederRate":"97.7334"
                   },
                   {
                     "opBomPartNo":"04-105-PC-110UXXXX",
                     "opFeederNo":"F1-Mixaco",
                     "opFeederRate":"97.7334"
                   }
                ]
            }
        ],
        "produnitList":[
            {                
                "opProdunitNo":"002",
                "opProdunitName":"良輝"
            },
            {                
                "opProdunitNo":"002",
                "opProdunitName":"良輝"
            }
        ],
        "equipList":[
            {                
                "opEquipId":"511",
                "opEquipName":"良輝#7",
                "opCqFlag":null,
                "opNp":null,
                "opBiaxisFlag":null,
                "opProdType":null
            },
            {                
                "opEquipId":"511",
                "opEquipName":"良輝#7",
                "opCqFlag":null,
                "opNp":null,
                "opBiaxisFlag":null,
                "opProdType":null
            }
        ],
        "opMessage":"04-105-PC-110UXXXX用量調整範圍超過30%，請再複核!"
    }
}
```

