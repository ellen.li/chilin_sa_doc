# BOM維護-新增修改存檔
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

新增修改存檔使用

## 修改歷程

| 修改時間 | 內容                                                                                             | 修改者 |
|----------|------------------------------------------------------------------------------------------------|--------|
| 20230518 | 新增規格                                                                                         | Nick   |
| 20230608 | 執行 SQL-27判斷值調整                                                                            | Nick   |
| 20230608 | 下料比例改 float                                                                                 | Nick   |
| 20230608 | opBomName 從原程式查看前端並未給值，固定給 null                                                   | Nick   |
| 20230608 | {opColor} 從原程式查看前端並未給值，固定給 null                                                   | Nick   |
| 20230612 | 調整 response 格式                                                                               | Nick   |
| 20230629 | 調整 opBaseBomNo、opMultiple、opBomUid、opBomDesc、opPgmData、opMtrData、opBaseItemNo、opBomNo 為非必填 | Nick   |
| 20230720 | 調整 【opEquipData 裡面的 opRemark】 非必填 ， opBomUid 補充說明                                    | Nick   |
| 20230721 | 陣列全部改為非必填(因為前端只傳有異動的) opPgmData、opEquipData、 opMixData、opFedData<br>opBomData (opStdNQuantityPer、opCusNQuantityPer)、opMtrData(opFeederSeq) 為非必填                              | Nick   |
| 20230731 | opBomData.opRemark、opPgmData.opSitePick 改為非必填                                               |   Nick   |
| 20230731 | opMixData(opProdunitNo、opProdunitName、opEquipId、opEquipName)、opFedData(opProdunitNo、opProdunitName、opEquipId、opEquipName) 改為非必填，因為 null 也是值 Q_Q  |   Nick   |
| 20230803 | 補料號色號在SAP查不到的邏輯，並且美化描述                                                            | Nick|
| 20231116 |　生失效功能作廢，新單據固定給 ACTIVE_FLAG 給 Y ，舊單據不更新 ACTIVE_FLAG 欄位                        | Nick |

## 來源URL及資料格式

| 項目   | 說明      |
| ------ | --------- |
| URL    | /bom/save |
| method | post      |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位        | 名稱               | 資料型別 | 必填 | 資料儲存 & 說明                |
|-------------|------------------|----------|------|--------------------------------|
| opIsNew     | 新增               | string   | M    | Y:新增、複製、色母倍數 N:修改 |
| opBomData   | BOM 主檔           | array    | M    |                                |
| opMtrData   | 原料資料           | array    | O    |                                |
| opPgmData   | 色料資料           | array    | O    |                                |
| opEquipData | 機台適用性         | array    | O    |                                |
| opMixData   | 攪拌方式           | array    | O    |                                |
| opFedData   | 下料比例與下料設備  | array    | O    |                                |


#### 【 opBomData 】array
| 欄位              | 名稱                | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明   |
|-------------------|---------------------|----------|:------------------------:|-------------------|
| opBomUid          | BOM ID              | string   |            O             | 複製、色母倍數、修改 必填 |
| opItemNo          | 料號                | string   |            M             |                   |
| opItemDesc        | 料號摘要            | string   |            M             |                   |
| opPatternNo       | 色號                | string   |            M             |                   |
| opUnit            | 單位                | string   |            M             |                   |
| opWeight          | 重量                | string   |            M             |                   |
| opOrganizationId  | 工廠別              | string   |            M             |                   |
| opBomFlag         | 依此配方            | string   |            M             |                   |
| opBomNo           | 替代結構            | string   |            O             |                   |
| opCustName        | 客戶名稱            | string   |            M             |                   |
| opBomDesc         | 替代說明            | string   |            O             |                   |
| opUse             | 用途                | string   |            M             |                   |
| opRemark          | 備註                | string   |            O             |                   |
| opBiaxisOnly      | 限雙軸生產          | string   |            M             |                   |
| opDyeOnly         | 只染不押            | string   |            M             |                   |
| opBaseItemNo      | 色母基本配方料號    | string   |            O             |                   |
| opBaseBomNo       | 色母基本配方BOM     | string   |            O             |                   |
| opMultiple        | 倍數                | string   |            O             |                   |
| opStdNQuantityPer | N4單位用量-系統     | string   |            O             |                   |
| opCusNQuantityPer | N4單位用量-自行輸入 | string   |            O             |                   |

#### 【 opMtrData 】array
| 欄位          | 名稱     | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明      |
| ------------- | -------- | -------- | :----------------------: | -------------------- |
| opDataFlag    | 資料狀態 | string   |            M             | N:新增 M:修改 D:刪除 |
| opMtrPartNo   | 原料料號 | string   |            M             |                      |
| opQuantityPer | 單位用量 | string   |            M             |                      |
| opUnit        | 單位     | string   |            M             |                      |

#### 【 opPgmData 】array
| 欄位          | 名稱     | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明                          |
|---------------|--------|----------|:------------------------:|------------------------------------------|
| opDataFlag    | 資料狀態 | string   |            M             | N:新增 M:修改 D:刪除                     |
| opPgmPartNo   | 色粉料號 | string   |            M             |                                          |
| opQuantityPer | 單位用量 | string   |            M             |                                          |
| opUnit        | 單位     | string   |            M             |                                          |
| opSitePick    | 現場領料 | string   |            O             | Y/N (規格撰寫時找不到此欄位會有值的情況) |
| opSelfPick    | 自秤     | string   |            M             | Y/N                                      |
| opFeederSeq   | 下料順序 | string   |            O             |                                          |

#### 【 opEquipData 】array
| 欄位           | 名稱         | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明      |
| -------------- | ------------ | -------- | :----------------------: | -------------------- |
| opStatus       | 資料狀態     | string   |            M             | N:新增 M:修改 D:刪除 |
| opProdunitNo   | 生產單位代碼 | string   |            M             |                      |
| opProdunitName | 生產單位名稱 | string   |            M             |                      |
| opEquipId      | 機台         | string   |            M             |                      |
| opEquipName    | 機台名稱     | string   |            M             |                      |
| opActiveFlag   | 是否生效     | string   |            M             | Y/N                  |
| opRemark       | 備註         | string   |            O             |                      |

#### 【 opMixData 】array
| 欄位           | 名稱         | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明           |
| -------------- | ------------ | -------- | :----------------------: | ------------------------- |
| opStatus       | 資料狀態     | string   |            M             | N:新增 M:修改 D:刪除      |
| opBomMixUid    | 攪拌方式 ID  | string   |           M/O            | opStatus 為 M 與 D 時必填 |
| opProdunitNo   | 生產單位代碼 | string   |            O             |                           |
| opProdunitName | 生產單位名稱 | string   |            O             |                           |
| opEquipId      | 機台         | string   |            O             |                           |
| opEquipName    | 機台名稱     | string   |            O             |                           |
| opMixDesc      | 攪拌步驟     | string   |            M             |                           |
| opMixTime      | 攪拌時間     | string  |            M             |                           |
| opMfgDesc      | 生產方式說明 | string   |            M             |                           |

#### 【 opFedData 】array
| 欄位           | 名稱             | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明           |
| -------------- | ---------------- | -------- | :----------------------: | ------------------------- |
| opStatus       | 資料狀態         | string   |            M             | N:新增 M:修改 D:刪除      |
| opBomFedUid    | 投料設備與比例ID | string   |           M/O            | opStatus 為 M 與 D 時必填 |
| opProdunitNo   | 生產單位代碼     | string   |            O             |                           |
| opProdunitName | 生產單位名稱     | string   |            O             |                           |
| opEquipId      | 機台             | string   |           O             |                           |
| opEquipName    | 機台名稱         | string   |           O             |                           |
| opF1Rate       | F1下料比例       | float  |            M             |                           |
| opF2Rate       | F2下料比例       | float  |            M             |                           |
| opF3Rate       | F3下料比例       | float  |            M             |                           |
| opF4Rate       | F4下料比例       | float  |            M             |                           |
| opF5Rate       | F5下料比例       | float  |            M             |                           |
| opF6Rate       | F6下料比例       | float  |            M             |                           |
| opBdpRate      | BDP下料比例      | float  |            M             |                           |
| opPpgRate      | PPG下料比例      | float  |            M             |                           |
| opDbeRate      | DBE下料比例      | float  |            M             |                           |
| opDData        | array            |          |            M             |                           |
 
#### 【 opDData 】array
| 欄位         | 名稱     | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明 |
| ------------ | -------- | -------- | :----------------------: | --------------- |
| opBomPartNo  | 料號     | string   |            M             |                 |
| opFeederNo   | 下料設備 | string   |            M             |                 |
| opFeederRate | 下料比率 | string   |            M             |                 |


<!-- 
以下為參考原程式未刪除不必要欄位前備份
| 欄位        | 名稱               | 資料型別 | 必填                                     | 資料儲存 & 說明 |
| ----------- | ------------------ | -------- | ---------------------------------------- | --------------- |
| opIsNew     | 新增               | string   | 預設`Y`  Y:新增(含複製、色母倍數) N:修改 |                 |
| opBomData   | BOM 主檔           | array    |                                          |                 |
| opMtrData   | 原料資料           | array    |                                          |                 |
| opPgmData   | 色料資料           | array    |                                          |                 |
| opEquipData | 機台適用性         | array    |                                          |                 |
| opFedData   | 下料比例與下料設備 | array    |                                          |                 |
| opMixData   | 攪拌方式           | array    |                                          |                 |


#### 【 opBomData 】array (BomMtmTo)
| 欄位              | 名稱                | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明 |
| ----------------- | ------------------- | -------- | :----------------------: | --------------- |
| opBomUid          | BOM ID              | string   |            M             |                 |
| opItemNo          | 料號                | string   |            M             |                 |
| opItemDesc        | 料號摘要            | string   |            M             |                 |
| opPatternNo       | 色號                | string   |            M             |                 |
| opUnit            | 單位                | string   |            M             |                 |
| opWeight          | 重量                | string   |            M             |                 |
| opOrganizationId  | 工廠別              | string   |            M             |                 |
| opActiveFlag      | 是否生效            | string   |            M             |                 |
| opBomFlag         | 依此配方            | string   |            M             |                 |
| opBomNo           | 替代結構            | string   |            M             |                 |
| opCustName        | 客戶名稱            | string   |            M             |                 |
| opBomDesc         | 替代說明            | string   |            M             |                 |
| opUse             | 用途                | string   |            M             |                 |
| opRemark          | 備註                | string   |            M             |                 |
| opBiaxisOnly      | 限雙軸生產          | string   |            M             |                 |
| opDyeOnly         | 只染不押            | string   |            M             |                 |
| opBaseItemNo      | 色母基本配方料號    | string   |            M             |                 |
| opBaseBomNo       | 色母基本配方BOM     | string   |            M             |                 |
| opMultiple        | 倍數                | string   |            M             |                 |
| opStdNQuantityPer | N4單位用量-系統     | string   |            M             |                 |
| opCusNQuantityPer | N4單位用量-自行輸入 | string   |            M             |                 |
| opAssemblyType    | 型態 MBOM/EBOM      | string   |            M             | 後端未使用到    |

#### 【 opMtrData 】array (BomMtrTo)
| 欄位             | 名稱      | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明      |
| ---------------- | --------- | -------- | :----------------------: | -------------------- |
| opDataFlag       | 資料狀態  | string   |            M             | N:新增 M:修改 D:刪除 |
| opMtrPartNo      | 原料料號  | string   |            M             |                      |
| opQuantityPer    | 單位用量  | string   |            M             |                      |
| opUnit           | 單位      | string   |            M             |                      |
| opQuantityPerLog | 單位用量? | string   |            M             | 後端未使用到         |



#### 【 opPgmData 】array (BomPgmTo)
| 欄位             | 名稱      | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明      |
| ---------------- | --------- | -------- | :----------------------: | -------------------- |
| opDataFlag       | 資料狀態  | string   |            M             | N:新增 M:修改 D:刪除 |
| opPgmPartNo      | 色粉料號  | string   |            M             |                      |
| opQuantityPer    | 單位用量  | string   |            M             |                      |
| opUnit           | 單位      | string   |            M             |                      |
| opSitePick       | 現場領料  | string   |            M             | Y/N                  |
| opSelfPick       | 自秤      | string   |            M             | Y/N                  |
| opFeederSeq      | 下料順序  | string   |            M             |                      |
| opQuantityPerLog | 單位用量? | string   |            M             | 後端未使用到         |


#### 【 opEquipData 】array (BomEquipTo)
| 欄位           | 名稱         | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明      |
| -------------- | ------------ | -------- | :----------------------: | -------------------- |
| opStatus       | 資料狀態     | string   |            M             | N:新增 M:修改 D:刪除 |
| opProdunitNo   | 生產單位代碼 | string   |            M             |                      |
| opProdunitName | 生產單位名稱 | string   |            M             |                      |
| opEquipId      | 機台         | string   |            M             |                      |
| opEquipName    | 機台名稱     | string   |            M             |                      |
| opActiveFlag   | 是否生效     | string   |            M             | Y/N                  |
| opRemark       | 備註         | string   |            M             |                      |


#### 【 opFedData 】array (BomFedTo)
| 欄位           | 名稱         | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明      |
| -------------- | ------------ | -------- | :----------------------: | -------------------- |
| opStatus       | 資料狀態     | string   |            M             | N:新增 M:修改 D:刪除 |
| opProdunitNo   | 生產單位代碼 | string   |            M             |                      |
| opProdunitName | 生產單位名稱 | string   |            M             |                      |
| opEquipId      | 機台         | string   |            M             |                      |
| opEquipName    | 機台名稱     | string   |            M             |                      |
| opF1Rate       | F1下料比例   | integer  |            M             |                      |
| opF2Rate       | F2下料比例   | integer  |            M             |                      |
| opF3Rate       | F3下料比例   | integer  |            M             |                      |
| opF4Rate       | F4下料比例   | integer  |            M             |                      |
| opF5Rate       | F5下料比例   | integer  |            M             |                      |
| opF6Rate       | F6下料比例   | integer  |            M             |                      |
| opBdpRate      | BDP下料比例  | integer  |            M             |                      |
| opPpgRate      | PPG下料比例  | integer  |            M             |                      |
| opDbeRate      | DBE下料比例  | integer  |            M             |                      |
| opDData        | array        |          |            M             |                      |
 
#### 【 opDData 】array
| 欄位         | 名稱     | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明 |
| ------------ | -------- | -------- | :----------------------: | --------------- |
| opBomPartN   | 料號     | string   |            M             |                 |
| opFeederNo   | 下料設備 | string   |            M             |                 |
| opFeederRate | 下料比率 | string   |            M             |                 |


#### 【 opMixData 】array (BomMixTo)
| 欄位           | 名稱         | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明      |
| -------------- | ------------ | -------- | :----------------------: | -------------------- |
| opStatus       | 資料狀態     | string   |            M             | N:新增 M:修改 D:刪除 |
| opProdunitNo   | 生產單位代碼 | string   |            M             |                      |
| opProdunitName | 生產單位名稱 | string   |            M             |                      |
| opEquipId      | 機台         | string   |            M             |                      |
| opEquipName    | 機台名稱     | string   |            M             |                      |
| opMixDesc      | 攪拌步驟     | string   |            M             |                      |
| opMixTime      | 攪拌時間     | string  |            M             |                      |
| opMfgDesc      | 生產方式說明 | string   |            M             |                      |
-->

#### Request 範例

```json
{
   "opIsNew":"N",
   "opBomData":[
      {
         "opBomUid":"1E52FDA9-E248-485C-A48E-A6818C3EA3BD",
         "opItemNo":"110UXXXXX1BXA15681",
         "opItemDesc":"PC-110U無襯,BX 25KG包裝袋A15681C1",
         "opPatternNo":"A15681C1",
         "opUnit":"KG",
         "opWeight":"1.000",
         "opOrganizationId":"5000",
         "opActiveFlag":"Y",
         "opBomFlag":"N",
         "opBomNo":"01",
         "opCustName":"中華塑膠",
         "opBomDesc":"SA#少7#108#110",
         "opUse": null,
         "opRemark": null,
         "opBiaxisOnly":"Y",
         "opDyeOnly":"Y",
         "opBaseItemNo": null,
         "opBaseBomNo": null,
         "opMultiple": null,
         "opAssemblyType":"NA",
         "opStdNQuantityPer":"0.31780",
         "opCusNQuantityPer":"0"
      }
   ],
   "opMtrData":[
      {
         "opDataFlag": null,
         "opMtrPartNo":"04-105-PC-110UXXXX",
         "opQuantityPer":"1.00000",
         "opUnit":"KG",
         "opQuantityPerLog":"1.00000"
      },
      {
         "opDataFlag": null,
         "opMtrPartNo":"92-912-5A787415120",
         "opQuantityPer":"0.04089",
         "opUnit":"PC",
         "opQuantityPerLog":"0.04089"
      }
   ],
   "opPgmData":[
      {
         "opDataFlag": null,
         "opPgmPartNo":"05-201-C-07XXXXXXX",
         "opQuantityPer":"0.00070",
         "opUnit":"G",
         "opSitePick": null,
         "opSelfPick":"N",
         "opQuantityPerLog":"0.00070",
         "opFeederSeq":"1"
      },
      {
         "opDataFlag": null,
         "opPgmPartNo":"05-201-G-17XXXXXXX",
         "opQuantityPer":"0.01000",
         "opUnit":"G",
         "opSitePick": null,
         "opSelfPick":"N",
         "opQuantityPerLog":"0.01000",
         "opFeederSeq":"1"
      },
      {
         "opDataFlag": null,
         "opPgmPartNo":"05-201-W-10XXXXXXX",
         "opQuantityPer":"19.00000",
         "opUnit":"G",
         "opSitePick": null,
         "opSelfPick":"N",
         "opQuantityPerLog":"19.00000",
         "opFeederSeq":"1"
      },
      {
         "opDataFlag": null,
         "opPgmPartNo":"05-201-Y-575XXXXXX",
         "opQuantityPer":"0.05700",
         "opUnit":"G",
         "opSitePick": null,
         "opSelfPick":"N",
         "opQuantityPerLog":"0.05700",
         "opFeederSeq":"1"
      },
      {
         "opDataFlag": null,
         "opPgmPartNo":"05-251-A-F13XXXXXX",
         "opQuantityPer":"0.12400",
         "opUnit":"G",
         "opSitePick": null,
         "opSelfPick":"N",
         "opQuantityPerLog":"0.12400",
         "opFeederSeq":"1"
      },
      {
         "opDataFlag": null,
         "opPgmPartNo":"05-251-A-L091XXXXX",
         "opQuantityPer":"2.00000",
         "opUnit":"G",
         "opSitePick": null,
         "opSelfPick":"N",
         "opQuantityPerLog":"2.00000",
         "opFeederSeq":"1"
      },
      {
         "opDataFlag": null,
         "opPgmPartNo":"05-251-A-NC100AXXX",
         "opQuantityPer":"1.00000",
         "opUnit":"G",
         "opSitePick": null,
         "opSelfPick":"D",
         "opQuantityPerLog":"1.00000",
         "opFeederSeq":"1"
      },
      {
         "opDataFlag": null,
         "opPgmPartNo":"05-251-A-O68XXXXXX",
         "opQuantityPer":"1.00000",
         "opUnit":"G",
         "opSitePick": null,
         "opSelfPick":"N",
         "opQuantityPerLog":"1.00000",
         "opFeederSeq":"1"
      }
   ],
   "opEquipData":[
      {
         "opStatus":"N",
         "opProdunitNo":"001",
         "opProdunitName":"憲成",
         "opEquipId":"602",
         "opEquipName":"憲成#2",
         "opActiveFlag":"Y",
         "opRemark":"aaa"
      },
      {
         "opStatus":"N",
         "opProdunitNo":"002",
         "opProdunitName":"良輝",
         "opEquipId":"612",
         "opEquipName":"良輝#7",
         "opActiveFlag":"N",
         "opRemark":"bbb"
      }
   ],
   "opMixData":[
      {
         "opStatus":"N",
         "opProdunitNo":"008",
         "opProdunitName":"高嘉",
         "opEquipId":"636",
         "opEquipName":"高嘉#3",
         "opMixDesc":"aaaa",
         "opMixTime":"12",
         "opMfgDesc":"以一段式攪拌"
      }
   ],
   "opFedData":[
      {
         "opStatus":"N",
         "opProdunitNo":"002",
         "opProdunitName":"良輝",
         "opEquipId":"611",
         "opEquipName":"良輝#6",
         "opStatusIcon":"icon-plus",
         "opF1Rate":100,
         "opF2Rate":0,
         "opF3Rate":0,
         "opF4Rate":0,
         "opF5Rate":0,
         "opF6Rate":0,
         "opBdpRate":0,
         "opPpgRate":0,
         "opDbeRate":0,
         "opDData":[
            {
               "opBomPartNo":"04-105-PC-110UXXXX",
               "opFeederNo":"F1-Mixaco",
               "opFeederRate":"97.7334"
            },
            {
               "opBomPartNo":"92-912-5A787415120",
               "opFeederNo":"F1-Mixaco",
               "opFeederRate":"3.9963"
            },
            {
               "opBomPartNo":"05-201-C-07XXXXXXX",
               "opFeederNo":"F1-Mixaco",
               "opFeederRate":"0.0001"
            },
            {
               "opBomPartNo":"05-201-G-17XXXXXXX",
               "opFeederNo":"F1-Mixaco",
               "opFeederRate":"0.0010"
            },
            {
               "opBomPartNo":"05-201-W-10XXXXXXX",
               "opFeederNo":"F1-Mixaco",
               "opFeederRate":"1.8569"
            },
            {
               "opBomPartNo":"05-201-Y-575XXXXXX",
               "opFeederNo":"F1-Mixaco",
               "opFeederRate":"0.0056"
            },
            {
               "opBomPartNo":"05-251-A-F13XXXXXX",
               "opFeederNo":"F1-Mixaco",
               "opFeederRate":"0.0121"
            },
            {
               "opBomPartNo":"05-251-A-L091XXXXX",
               "opFeederNo":"F1-Mixaco",
               "opFeederRate":"0.1955"
            },
            {
               "opBomPartNo":"05-251-A-NC100AXXX",
               "opFeederNo":"F1-Mixaco",
               "opFeederRate":"0.0977"
            },
            {
               "opBomPartNo":"05-251-A-O68XXXXXX",
               "opFeederNo":"F1-Mixaco",
               "opFeederRate":"0.0977"
            }
         ]
      }
   ]
}
```
# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* {USER_ID} = 目前登入者使用者ID
* {ORGANIZATION_ID} = 目前登入者工廠別		
* {bomLogUid} = 亂數產生一組全大寫 UUID (ex: 0AD9DFA8-536C-484E-BC15-D9EE457FBAA9)
* 如 {opIsNew} 等於 "Y" 則亂數產生一組全大寫 UUID 覆寫 {opBomUid} 
* 執行 SAP-1 將回傳值設為 rfcList 物件集合
* 如果 色號{opPatternNo} 不是 null 且色號{opPatternNo}不存在色號主檔(PAT_H)內(執行 SQL-1 取得結果做 NOT)，則執行 SQL-2 寫入色號主檔，參數{mtrType} 'NA'，其餘參數參考 JSON opBomData 內對應資料賦予
* 如果 料號{opItemNo} 不存在料號主檔(ITEM_H)內(執行 SQL-3 取得結果做 NOT，{OPMSITEMNO} 等於 {opItemNo} 值)，則執行 SQL-4 寫入料號主檔，參數itemNo} 設為 {opItemNo} 值，{patternNo} 設為 {opPatternNo} 值，{itemDesc} 設為 {opItemDesc} 值，{unit} 設為 {opUnit} 值，{weight}設為 opWeight} 值，如{opWeight}為 null 將{weight}設為0
  反之，如果料號{opItemNo} 存在料號主檔(ITEM_H)內，則執行 SQL-5 更新料號主檔，參數{opWeight}如為 null 需將{opWeight}覆寫為0，其餘參數參考 JSON opBomData 內對應資料賦予 
* 如 {opIsNew} 等於 "Y" ，則執行 SQL-6 新增配色配方主檔(BOM_MTM)，反之則執行 SQL-7 更新配色配方主檔(BOM_MTM)
* 執行 SQL-8 寫入配色配方主檔 LOG (BOM_MTM_LOG)	
* 如 {opMtrData} 陣列內有資料，則擷取每一筆的 {opDataFlag}判斷
  * 如 {opDataFlag}為 "N"
    * 則執行 SQL-9 新增配色配方原料檔(BOM_MTR) ，參數參照 opMtrData 內容
    * 執行 SQL-3 判斷是否存在料號主檔({OPMSITEMNO} 等於 {opMtrPartNo} 值)
      * 如不存在
        * 執行 SQL-4 寫入料號主檔
          * 寫入前參數{itemNo} 設為 {opMtrPartNo} 值，{patternNo} 設為 "NA"，{itemDesc} 設為 "NA"，{unit} 設為 "KG"
          * 檢查 {opMtrPartNo} 是否與 rfcList 物件集合內的每一個{PartNo} 相等，如果相等 {itemDesc} 覆寫 SAP 的 {PartName}值(SAP 值等於 null 或等於空白覆寫為 'NA')，{unit} 覆寫為 SAP 的 {Unit}值 
          * 尋找完全部 rfcList 物件集合後，如果找不到，則 {itemDesc} 設為 "NA"，{unit} 設為 "KG"
          * 最終如果 {unit}為 "KG" 開頭，則 {weight} 覆寫為 "1" ， 如果{unit} 內容等於 "G" ，則 {weight} 值覆寫為 "0.001"， 如果{unit} 為其他值 ，則 {weight} 值覆寫為 "0"
        * 參數設定完成後即可寫入
  * 如 {opDataFlag}為 "M"，則執行 SQL-10 更新配色配方原料檔(BOM_MTR)，參數參照 opMtrData 內容
  * 如 {opDataFlag}為 "D"，則執行 SQL-11 刪除配色配方原料檔(BOM_MTR)，參數參照 opMtrData 內容
  * 執行 SQL-12 寫入配色配方原料 LOG 檔 LOG (BOM_MTR_LOG)	
* 如 {opPgmData} 陣列內有資料，則擷取每一筆的 {opDataFlag}判斷
  * 如 {opDataFlag}為 "N"
    * 則執行 SQL-13 新增配色配方色粉檔(BOM_PGM) ， 參數參照 opPgmData 內容
    * 執行 SQL-3 判斷是否存在料號主檔({OPMSITEMNO} 等於 {opPgmPartNo} 值)
      * 如不存在
        * 執行 SQL-4 寫入料號主檔
          * 寫入前參數{itemNo} 設為 {opPgmPartNo} 值，{patternNo} 設為 "NA"，{itemDesc} 設為 "NA"，{unit} 設為 "KG"
          * 檢查 {opPgmPartNo} 是否與 rfcList 物件集合內的每一個{PartNo} 相等，如果相等 {itemDesc} 覆寫 SAP 的 {PartName}值(SAP 值等於 null 或等於空白覆寫為 'NA')，{unit} 覆寫  為 SAP 的 {Unit}值 
          * 尋找完全部 rfcList 物件集合後，如果找不到，則 {itemDesc} 設為 "NA"，{unit} 設為 "KG"
          * 最終如果 {unit}為 "KG" 開頭，則 {weight} 覆寫為 "1" ， 如果{unit} 內容等於 "G" ，則 {weight} 值覆寫為 "0.001"， 如果{unit} 為其他值 ，則 {weight} 值覆寫為 "0"
        * 參數設定完成後即可寫入
  * 如 {opDataFlag}為 "M"，則執行 SQL-14 更新配色配方色粉檔(BOM_PGM)，參數參照 opPgmData 內容
  * 如 {opDataFlag}為 "D"，則執行 SQL-15 刪除配色配方色粉檔(BOM_PGM)，參數參照 opPgmData 內容
  * 執行 SQL-16 寫入配色配方色粉 LOG 檔 LOG (BOM_PGM_LOG)	
* 如 {opEquipData} 陣列內有資料，則擷取每一筆的 {opStatus}判斷
  * 如 {opStatus}為 "N"，則執行 SQL-17 新增BOM適用機台檔(BOM_EQUIP)，參數參照 opEquipData 內容
  * 如 {opStatus}為 "M"，則執行 SQL-18 更新BOM適用機台檔(BOM_EQUIP)，參數參照 opEquipData 內容
  * 如 {opStatus}為 "D"，則執行 SQL-19 刪除BOM適用機台檔(BOM_EQUIP)，參數參照 opEquipData 內容
* 如 {opMixData} 陣列內有資料，則擷取每一筆的 {opStatus}判斷
  * 如 {opStatus}為 "N"，則執行 SQL-20 新增攪拌方式檔(BOM_MIX)，參數參照 opMixData 內容，{bomMixUid} 使用亂數產生一組全大寫 UUID
  * 如 {opStatus}為 "M"，則執行 SQL-21 更新攪拌方式檔(BOM_MIX)，參數參照 opMixData 內容
  * 如 {opStatus}為 "D"，則執行 SQL-22 刪除攪拌方式檔(BOM_MIX)，參數參照 opMixData 內容
 * 如 {opFedData} 陣列內有資料，則擷取每一筆的 {opStatus}判斷
  * 如 {opStatus}為 "N"，則執行 SQL-23 新增投料設備與比例檔(BOM_FED)，參數參照 opFedData 內容，{bomFedUid} 使用亂數產生一組全大寫 UUID
  * 如 {opStatus}為 "M"，則執行 SQL-24 更新投料設備與比例檔(BOM_FED)，執行 SQL-25 刪除投料設備與明細檔(BOM_FED_D) ，參數參照 opFedData 內容
  * 如 {opStatus}為 "D"，則執行 SQL-25 刪除投料設備與明細檔(BOM_FED_D)，執行 SQL-26 刪除投料設備與比例檔(BOM_FED)，參數參照 opFedData 內容
  * 上面三種狀態碼判斷完後，如果 {opStatus}為 "N" 或 "M"，執行 SQL-27 新增投料設備與明細檔(BOM_FED_D)，參數參照 opFedData 及 opDData 內容
* 上述過程如有執行任何資料庫任何異動，執行 SQL-LOG 紀錄 LOG，{DATAKEY} = {opBomUid}
  有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SAP-1:
可參考舊版 rfc.getBomList
程序如有任何異常，記錄錯誤 LOG 並 回傳 null
|              | 型態   | 參數值        | 說明 |
| ------------ | ------ | ------------- | ---- |
| BAPIFunction | string | Z_PP_OPMS_BOM |      |

| 參數名稱 | 參數值            | 型態   | 說明                                        |
| -------- | ----------------- | ------ | ------------------------------------------- |
| I_WERKS  | {ORGANIZATION_ID} | string |                                             |
| I_MATNR  | {opItemNo}        | string |                                             |
| I_STLAL  | {opBomNo}         | string | 如果{opBomNo}只有一位數，需補足兩位，ex: 01 |

| 取得回傳表格名稱 |
| ---------------- |
| T_BOM            |

 如果回傳資料數量為0則 回傳 null ，否則依下面物件集合回傳

| 物件元素名稱 |  資料型別  | SAP欄位名稱 | SAP資料型別 | 物件元素回傳值                                  | 說明                       |
| ------------ | :--------: | ----------- | :---------: | ----------------------------------------------- | -------------------------- |
| PartNo       |   string   | IDNRK       |   string    | 同SAP                                           |                            |
| Unit         |   string   | MEINS       |   string    | 同SAP                                           |                            |
| Sortf        |   string   | SORTF       |   string    | 同SAP                                           |                            |
| PartName     |   string   | MAKTX       |   string    | 同SAP                                           |                            |
| QuantityPer  | BigDecimal | MENGE       |   string    | SAP["MENGE"] /1000 (結果四捨五入到小數點第五位) | 回傳值必須格式化為 0.00000 |



SQL-1:
```sql
--patDao.checkPatternExist(bomMtmTo.getPatternNo())
SELECT COUNT(*) CNT FROM PAT_H A WHERE PATTERN_NO={opPatternNo}
```
SQL-2:
```sql
--patternModifyDao.insertPattern(patTo, userId);
INSERT INTO PAT_H (PATTERN_NO,CUST_NO,CUST_NAME,COLOR,MTR_TYPE,REMARK,CDT,CREATE_BY,UDT,UPDATE_BY) 
           VALUES({opPatternNo},null,{opCustName},null,{mtrType},null,GETDATE(),{登入者使用者ID},GETDATE(),{登入者使用者ID})";
```

SQL-3:
```sql
--opmsItemDao.checkItemExist(bomMtmTo.getItemNo())
--opmsItemDao.checkItemExist(rTo.getMtrPartNo())
--opmsItemDao.checkItemExist(pTo.getPgmPartNo())

  SELECT COUNT(*) CNT FROM ITEM_H A WHERE ITEM_NO={OPMSITEMNO}

```

SQL-4:
```sql
--itemModifyDao.insertItem(iTo, userId);
--itemModifyDao.insertItem(iTo, userId);
--itemModifyDao.insertItem(iTo, userId);

--注意!!! 如果 {opWeight} 為 null 請務必轉成 0 再填入資料庫
INSERT INTO ITEM_H (ITEM_NO,PATTERN_NO,ITEM_DESC,UNIT,WEIGHT,PACKING_WEIGHT,CMDI_NO,CMDI_NAME,MTR_NAME,CDT,CREATE_BY,UDT,UPDATE_BY) 
                 VALUES({itemNo},{patternNo},{itemDesc},{unit},{weight},null,null,null,null,GETDATE(),{登入者使用者ID},GETDATE(),{登入者使用者ID})
```

SQL-5:
```sql
--itemModifyDao.updateItemPattern(iTo, userId);

--注意!!! 如果 {opWeight} 為 null 請務必轉成 0 再填入資料庫
UPDATE ITEM_H SET PATTERN_NO={PatternNo},ITEM_DESC={opItemDesc},WEIGHT={opWeight},UNIT={opUnit},UDT=GETDATE(),UPDATE_BY={登入者使用者ID} WHERE ITEM_NO={opItemNo}
```

SQL-6:
```sql
--dao.insertBomMtm(organizationId, userId, bomMtmTo);

INSERT INTO BOM_MTM(ORGANIZATION_ID, BOM_UID, ACTIVE_FLAG, ITEM_NO, BOM_NO,
                    BOM_NAME, BOM_DESC,CUST_NAME, [USE], MULTIPLE, 
                    BASE_ITEM_NO, BASE_BOM_NO, REMARK, BOM_FLAG, STD_N_QUANTITY_PER, 
                    CUS_N_QUANTITY_PER, BIAXIS_ONLY,DYE_ONLY, CDT, CREATE_BY, 
                    UDT , UPDATE_BY) 
            VALUES({登入者工廠別}, {opBomUid}, 'Y', {opItemNo}, {opBomNo}, 
                   null, {opBomDesc}, {opCustName}, {opUse}, {opMultiple}, 
                   {opBaseItemNo}, {opBaseBomNo}, {opRemark}, {opBomFlag}, {opStdNQuantityPer}, 
                   {opCusNQuantityPer}, {opBiaxisOnly}, {opDyeOnly}, GETDATE(), {登入者使用者ID},
                   GETDATE(), {登入者使用者ID})

```

SQL-7:
```sql
--dao.updateBomMtm(userId, bomMtmTo);
UPDATE BOM_MTM 
SET BOM_DESC={opBomDesc}, CUST_NAME={opCustName}, [USE]={opUse}, REMARK={opRemark}, 
    BOM_FLAG={opBomFlag}, STD_N_QUANTITY_PER={opStdNQuantityPer}, CUS_N_QUANTITY_PER={opCusNQuantityPer},BIAXIS_ONLY={opBiaxisOnly},DYE_ONLY={opDyeOnly}, 
    UDT=GETDATE(), UPDATE_BY={登入者使用者ID}
 WHERE BOM_UID={opBomUid}
```

SQL-8:
```sql
--logDao.insertBomMtmLog(bomLogUid, organizationId, userId, bomMtmTo);


INSERT INTO BOM_MTM_LOG(BOM_LOG_UID,ORGANIZATION_ID, BOM_UID,  ITEM_NO, 
                        BOM_NO,BOM_NAME, BOM_DESC, CUST_NAME, [USE], 
                        MULTIPLE, BASE_ITEM_NO, BASE_BOM_NO, REMARK, BOM_FLAG, 
                        STD_N_QUANTITY_PER, CUS_N_QUANTITY_PER, BIAXIS_ONLY,DYE_ONLY, UDT, 
                        UPDATE_BY)
                 VALUES({bomLogUid}, {登入者工廠別}, {opBomUid}, {opItemNo}, 
                        {opBomNo}, null, {opBomDesc}, {opCustName}, {opUse}, 
                        {opMultiple}, {opBaseItemNo}, {opBaseBomNo}, {opRemark}, {opBomFlag}, 
                        {opStdNQuantityPer}, {opCusNQuantityPer}, {opBiaxisOnly}, {opDyeOnly}, GETDATE(), 
                        {登入者使用者ID})

```

SQL-9:
```sql
--dao.insertBomMtr(userId, rTo);
INSERT INTO BOM_MTR(BOM_UID, MTR_PART_NO, QUANTITY_PER, UNIT, CDT, CREATE_BY, UDT, UPDATE_BY) 
             VALUES({opBomUid}, {opMtrPartNo}, {opQuantityPer}, {opUnit}, GETDATE(), 
                    {登入者使用者ID}, GETDATE(), {登入者使用者ID})

```

SQL-10:
```sql
--dao.updateBomMtr(userId, rTo);
UPDATE BOM_MTR SET QUANTITY_PER={opQuantityPer}, UNIT={opUnit}, UDT=GETDATE(), UPDATE_BY={登入者使用者ID}
WHERE BOM_UID={opBomUid} AND MTR_PART_NO={opMtrPartNo}

```

SQL-11:
```sql
--dao.deleteBomMtr(finalBomUid, rTo.getMtrPartNo());
DELETE BOM_MTR WHERE BOM_UID={finalBomUid} AND MTR_PART_NO={opMtrPartNo}
```

SQL-12:
```sql
--logDao.insertBomMtrLog(bomLogUid, userId, rTo);
INSERT INTO BOM_MTR_LOG(BOM_LOG_UID, BOM_UID, MTR_PART_NO, QUANTITY_PER, UNIT, UDT, UPDATE_BY) 
                 VALUES({bomLogUid}, {opBomUid}, {opMtrPartNo}, {opQuantityPer}, {opUnit}, GETDATE(), {登入者使用者ID})

```

SQL-13:
```sql
--dao.insertBomPgm(userId, pTo);
INSERT INTO BOM_PGM(BOM_UID, PGM_PART_NO, QUANTITY_PER, UNIT, SITE_PICK, SELF_PICK, FEEDER_SEQ, CDT, CREATE_BY, UDT, UPDATE_BY)  
             VALUES({opBomUid}, {opPgmPartNo}, {opQuantityPer}, {opUnit}, {opSitePick}, {opSelfPick}, {opFeederSeq}, GETDATE(),{登入者使用者ID}, GETDATE(), {登入者使用者ID})
```

SQL-14:
```sql
--dao.updateBomPgm(userId, pTo);
UPDATE BOM_PGM 
SET QUANTITY_PER={opQuantityPer}, UNIT={opUnit}, SITE_PICK={opSitePick}, SELF_PICK={opSelfPick}, FEEDER_SEQ={opFeederSeq}, 
    UDT=GETDATE(), UPDATE_BY={登入者使用者ID}
WHERE BOM_UID={opBomUid} AND PGM_PART_NO={opPgmPartNo}
```
SQL-15:
```sql
--dao.deleteBomPgm(finalBomUid, pTo.getPgmPartNo());

DELETE BOM_PGM WHERE BOM_UID={bomUid} AND PGM_PART_NO={pgmPartNo}    
```

SQL-16:
```sql
--logDao.insertBomPgmLog(bomLogUid, userId, pTo);
INSERT INTO BOM_PGM_LOG(BOM_LOG_UID, BOM_UID, PGM_PART_NO, QUANTITY_PER, UNIT, 
                        SITE_PICK, SELF_PICK, FEEDER_SEQ, UDT, UPDATE_BY) 
                 VALUES({bomLogUid}, {opBomUid}, {opPgmPartNo}, {opQuantityPer}, {opUnit}, 
                         {opSitePick}, {opSelfPick}, {opFeederSeq}, GETDATE(), {登入者使用者ID})
        
```
SQL-17:
```sql
--dao.insertBomEquip(userId, tmpTo);
INSERT INTO BOM_EQUIP(BOM_UID, EQUIP_ID, ACTIVE_FLAG, REMARK, CDT, CREATE_BY, UDT, UPDATE_BY) 
               VALUES({opBomUid}, {opEquipId}, {opActiveFlag}, {opRemark}, GETDATE(), {登入者使用者ID}, GETDATE(), {登入者使用者ID})


```
SQL-18:
```sql
--dao.updateBomEquip(userId, tmpTo);
UPDATE BOM_EQUIP SET REMARK={opRemark}, UDT=GETDATE(), UPDATE_BY={登入者使用者ID}
WHERE BOM_UID={opBomUid} AND EQUIP_ID={opEquipId}

```
SQL-19:
```sql
--dao.deleteBomEquip(finalBomUid, tmpTo.getEquipId());
DELETE BOM_EQUIP WHERE BOM_UID={opBomUid} AND EQUIP_ID={equipId}    
```

SQL-20:
```sql
--dao.insertBomMix(userId, tmpTo);
INSERT INTO BOM_MIX(BOM_UID, BOM_MIX_UID, EQUIP_ID, MIX_DESC, MIX_TIME, MFG_DESC, UDT, UPDATE_BY) 
                   VALUES({opBomUid}, {bomMixUid}, {opEquipId}, {opMixDesc}, {opMixTime}, {opMfgDesc}, GETDATE(), {登入者使用者ID})
```

SQL-21:
```sql
--dao.updateBomMix(userId, tmpTo);
UPDATE BOM_MIX SET MIX_DESC={opMixDesc}, MIX_TIME={opMixTime}, MFG_DESC={opMfgDesc}, UDT=GETDATE(), UPDATE_BY={登入者使用者ID}
WHERE BOM_UID={opBomUid} AND BOM_MIX_UID={opBomMixUid}
```

SQL-22:
```sql
--dao.deleteBomMix(finalBomUid, tmpTo.getBomMixUid());
DELETE BOM_MIX WHERE BOM_UID={bomUid} AND BOM_MIX_UID={opBomMixUid}    

```

SQL-23:
```sql
--dao.insertBomFed(userId, tmpTo);
INSERT INTO BOM_FED(BOM_UID, BOM_FED_UID, EQUIP_ID, F_1_RATE, F_2_RATE, 
                    F_3_RATE, F_4_RATE, F_5_RATE, F_6_RATE,BDP_RATE, 
                    PPG_RATE, DBE_RATE, UDT, UPDATE_BY) 
            VALUES({opBomUid}, {opBomFedUid}, {opEquipId}, {opF1Rate}, {opF2Rate}, 
                    {opF3Rate}, {opF4Rate}, {opF5Rate}, {opF6Rate}, {opBdpRate}, 
                    {opPpgRate}, {opDbeRate}, GETDATE(), {登入者使用者ID})
```

SQL-24:
```sql
--dao.updateBomFed(userId, tmpTo);
UPDATE BOM_FED SET F_1_RATE={opF1Rate}, F_2_RATE={opF2Rate}, F_3_RATE={opF3Rate}, F_4_RATE={opF4Rate}, F_5_RATE={opF5Rate}, F_6_RATE={opF6Rate}, BDP_RATE={opBdpRate}, PPG_RATE={opPpgRate}, DBE_RATE={opDbeRate}, UDT=GETDATE(), UPDATE_BY={登入者使用者ID}
WHERE BOM_UID={opBomUid} AND BOM_FED_UID={opBomFedUid}

```

SQL-25:
```sql
--dao.deleteBomFedD(finalBomUid, tmpTo.getBomFedUid());
--dao.deleteBomFedD(finalBomUid, tmpTo.getBomFedUid())
--dao.deleteBomFedD(finalBomUid, tmpTo.getBomFedUid())

DELETE BOM_FED_D WHERE BOM_UID={opBomUid} AND BOM_FED_UID={bomFedUid}     
```

SQL-26:
```sql
--dao.deleteBomFed(finalBomUid, tmpTo.getBomFedUid());

DELETE BOM_FED WHERE BOM_UID={opBomUid} AND BOM_FED_UID={bomFedUid}     
```

SQL-27:
```sql
--dao.insertBomFedD(userId, tmpDTo);

INSERT INTO BOM_FED_D(BOM_UID, BOM_FED_UID, BOM_PART_NO, FEEDER_NO, FEEDER_RATE, UDT, UPDATE_BY) 
              VALUES({opBomUid}, {opBomFedUid}, {opBomPartNo}, {opFeederNo}, {opFeederRate}, GETDATE(), {登入者使用者ID})
```    

SQL-LOG:
```sql
INSERT INTO  SYS_MODI_LOG (CATEGORY,DATAKEY,USERID,USER_NAME,LOG) VALUES ('BOM',{DATAKEY},{登入者使用者ID},{登入者使用者名稱}, NULL)
```


# Response 欄位
| 欄位     | 名稱   | 資料型別 | 資料儲存 & 說明 |
| -------- | ------ | -------- | --------------- |
| opBomUid | BOM ID | string   |                 |


## Response 範例

```json
{
    "msgCode": null,
    "result": {
       "content":{
          "opBomUid":"1E52FDA9-E248-485C-A48E-A6818C3EA3BD"
      }
    }
}
```
## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考

* BomManageAction.saveBom



