# 工單維護-新增修改存檔
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

工單維護-新增修改存檔

## 修改歷程

| 修改時間   | 內容                                                      | 修改者 |
|------------|---------------------------------------------------------|--------|
| 20230804   | 新增規格                                                  | Ellen  |
| 20230825   | 移除BOM_NO檢查                                            | Ellen  |
| 20230908   | 修正委外檢查LOT邏輯                                       | Nick   |
| 20230908-1 | 同 20230908 SQL-15 也需修正                               | Nick   |
| 20230908   | 修正必填欄位                                              | Ellen  |
| 20230911   | 因應客戶需求 HeaderID=53 自動處理單位"只"(給前端不給"只") | Nick   |



## 來源URL及資料格式

| 項目   | 說明     |
| ------ | -------- |
| URL    | /wo/save |
| method | post     |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。

| 欄位                   | 名稱             | 資料型別     | 必填 | 資料儲存 & 說明                    |
| ---------------------- | ---------------- | ------------ | ---- | ---------------------------------- |
| opIsNew                | 是否為新增       | string       | M    | Y/N  Y: 是 / N: 否                 |
| opBomUid               | uid              | string       | M    |                                    |
| opItemNo               | 料號             | string       | M    |                                    |
| opWoNo                 | 工單號碼         | string       | M    |                                    |
| opWoUid                | 工單ID           | string       | O    | 預設 `null`，{opIsNew}為`N`時必填  |
| opWoSeq                | 序號             | string       | O    | 預設 `null` ，{opIsNew}為`N`時必填 |
| opLotNo                | 流水號           | string       | O    | 預設 `null`                        |
| opPatternNo            | 色號             | string       | O    | 預設 `null`                        |
| opColor                | 顏色             | string       | O    | 預設 `null`                        |
| opItemDesc             | 料號摘要         | string       | O    | 預設 `null`                        |
| opBomNo                | 替代BOM          | string       | O    | 預設 `null`                        |
| opCmdiName             | 品稱             | string       | O    | 預設 `null`                        |
| opBomRemark            | BOM備註          | string       | O    | 預設 `null`                        |
| opWoDate               | 使用日期         | string       | M    |                                    |
| opWipClassCode         | 工單類別         | string       | O    | 預設 `null`                        |
| opWipClassNo           | no               | string       | O    | 預設 `null`                        |
| opSalesOrderNo         | 銷售訂單單號     | string       | O    | 預設 `null`                        |
| opSalesOrderSeq        | 銷售訂單明細行號 | string       | O    | 預設 `null`                        |
| opWoQty                | 工單數量         | string       | O    | 預設 `null`                        |
| opSalesQty             | 銷售數量         | string       | O    | 預設 `null`                        |
| opProdunitNo           | 生產單位代碼     | string       | O    | 預設 `null`                        |
| opProdunitName         | 生產單位名稱     | string       | O    | 預設 `null`                        |
| opSalesNo              | 業助代號         | string       | O    | 預設 `null`                        |
| opUnit                 | 單位             | string       | O    | 預設 `null`                        |
| opEquipName            | 指定機台         | string       | O    | 預設 `null`                        |
| opCustPoNo             | 客戶PO#          | string       | O    | 預設 `null`                        |
| opGradeNo              | Grade No         | string       | O    | 預設 `null`                        |
| opCustNo               | 客戶代號         | string       | O    | 預設 `null`                        |
| opCustName             | 客戶名稱         | string       | O    | 預設 `null`                        |
| opCompletionSubinv     | 完工倉別         | string       | O    | 預設 `null`                        |
| opEstimatedShipDate    | 預計出貨日       | string       | O    | 預設 `null`                        |
| opTotalQuantityPer     | 色料單位總重     | string       | O    | 預設 `null`                        |
| opCusNQuantityPer      | N-4單位用量      | string       | O    | 預設 `null`                        |
| opStdNQuantityPer      | N-4單位用量-系統 | string       | O    | 預設 `null`                        |
| opRawQty               | 投入原料量       | string       | O    | 預設 `null`                        |
| opBatchQty             | 每Batch用量      | string       | O    | 預設 `null`                        |
| opBatchTimes           | Batch用量次數    | string       | O    | 預設 `null`                        |
| opRemainQty            | 尾數量           | string       | O    | 預設 `null`                        |
| opRemainTimes          | 尾數量次數       | string       | O    | 預設 `null`                        |
| opRemark               | 指圖表備註       | string       | O    | 預設 `null`                        |
| opProdRemark           | 生產指示備註     | string       | O    | 預設 `null`                        |
| opActiveFlag           | 狀態             | string       | O    | 預設 `null`                        |
| opIndirCust            | 間接客戶         | string       | O    | 預設 `null`                        |
| opSapOperation         |                  | string       | O    | 預設 `null`                        |
| opSapWc                | 工作中心         | string       | O    | 預設 `null`                        |
| opAltBomPartNo         | 參考用料表料號   | string       | O    | 預設 `null`                        |
| opAltBomDesignator     | 參考用料表BOM    | string       | O    | 預設 `null`                        |
| opAltRoutingPartNo     | 參考製程料號     | string       | O    | 預設 `null`                        |
| opAltRoutingDesignator | 製程替代         | string       | O    | 預設 `null`                        |
| opWeight               | 重量             | string       | O    | 預設 `null`                        |
| opCtnWeight            | 桶重             | string       | O    | 預設 `null`                        |
| opWoType               | 生產方式         | string       | O    | 預設 `null`                        |
| opEquipId              | 設備ID           | string       | O    | 預設 `null`                        |
| opMtrList              | 原料             | array object | O    | 預設 `null`                        |
| opPgmList              | 色料             | array object | O    | 預設 `null`                        |
| opSisList              | 工單-出貨指示    | array object | O    | 預設 `null`                        |
| opWoSisList            | 訂單-出貨指示    | array object | O    | 預設 `null`                        |


### 【opMtrList】array object
| 欄位          | 名稱 | 資料型別 | 必填 | 資料儲存 & 說明 |
| ------------- | ---- | -------- | ---- | --------------- |
| opWoMtrPartNo | 料號 | string   | O    |                 |
| opQuantityPer | 用量 | string   | O    |                 |
| opUnit        | 單位 | string   | O    |                 |

### 【opPgmList】array object
| 欄位           | 名稱      | 資料型別 | 必填 | 資料儲存 & 說明 |
| -------------- | --------- | -------- | ---- | --------------- |
| opSelfPick     | 秤料      | string   | O    |                 |
| opWoPgmPartNo  | 色料      | string   | M    |                 |
| opQuantityPer  | 單位用量  | string   | M    |                 |
| opUnit         | 單位      | string   | M    |                 |
| opPgmBatchQty  | batch用量 | string   | M    |                 |
| opPgmRemainQty | 尾數用量  | string   | M    |                 |
| opFeederSeq    | 投料順序  | string   | O    |                 |
| opSitePick     | 現場領料  | string   | O    |                 |

### 【opSisList】array object
| 欄位            | 名稱           | 資料型別 | 必填 | 資料儲存 & 說明 |
| --------------- | -------------- | -------- | ---- | --------------- |
| opListHeaderId  | 出貨指示項目ID | string   | M    |                 |
| opListLineId    | 出貨指示選項ID | string   | M    |                 |
| opListSeq       | 順序           | string   | O    |                 |
| opSpInsName     | 出貨指示名稱   | string   | M    |                 |
| opCategoryAttri | 類別           | string   | M    |                 |
| opAssDesc       | 指定內容       | string   | O    |                 |
| opOptDesc       | 選項內容       | string   | O    |                 |
| opFlagAssign    | 是否指定內容   | string   | M    |                 |
| opFlagOrderType |                | string   | M    |                 |


### 【opWoSisList】array object
| 欄位            | 名稱           | 資料型別 | 必填 | 資料儲存 & 說明 |
| --------------- | -------------- | -------- | ---- | --------------- |
| opListHeaderId  | 出貨指示項目ID | string   | M    |                 |
| opListLineId    | 出貨指示選項ID | string   | M    |                 |
| opListSeq       | 順序           | string   | O    |                 |
| opSpInsName     | 出貨指示名稱   | string   | M    |                 |
| opCategoryAttri | 類別           | string   | M    |                 |
| opAssDesc       | 指定內容       | string   | O    |                 |
| opOptDesc       | 選項內容       | string   | O    |                 |
| opFlagAssign    | 是否指定內容   | string   | M    |                 |
| opFlagOrderType |                | string   | M    |                 |
| opShowText      | 顯示內容       | string   | M    |                 |


#### Request 範例

```json
{
  "opIsNew": "N",
  "opWoUid": "2B7D3005-A1B7-4967-8F7B-963FCFA037C0",
  "opBomUid": "6F69F837-D36C-4A31-8CAD-CE55666E457C",
  "opWoNo": "52018394",
  "opCustPoNo": "2000156991",
  "opWoDate": "2022/12/01",
  "opActiveFlag": "TECO",
  "opProdunitNo": "211020",
  "opCustNo": "1028028",
  "opCustName": "奇美實業",
  "opIndirCust": "",
  "opSalesNo": "00091118",
  "opUnit": "KG",
  "opWeight": "1.000",
  "opWoQty": "265.055",
  "opRawQty": "250",
  "opBatchQty": "200",
  "opBatchTimes": "1",
  "opRemainQty": "50",
  "opRemainTimes": "1",
  "opWipClassNo": "Z520",
  "opWipClassCode": "複材工單(一般生產)",
  "opSalesOrderNo": "21560091",
  "opSalesOrderSeq": "20",
  "opSalesQty": "2000",
  "opCompletionSubinv": "FZ10",
  "opEstimatedShipDate": "2022/12/15",
  "opStdNQuantityPer": "0.66703",
  "opCusNQuantityPer": "0",
  "opGradeNo": "PC-110U",
  "opCtnWeight": "212.04422",
  "opSapOperation": "10",
  "opSapWc": "M02",
  "opBomRemark": "1.2019.08.15配色修色:C80(0.0072-0.02);T01(0.0036-0.0042)",
  "opProdRemark": "",
  "opPgmList": [
    {
      "opWoPgmPartNo": "05-201-C-80XXXXXXX",
      "opQuantityPer": "0.02",
      "opUnit": "G",
      "opSelfPick": "N",
      "opPgmBatchQty": "4.00000",
      "opPgmRemainQty": "1.00000",
      "opModifyFlag": "Y",
      "opFeederSeq": ""
    }
  ],
  "opSisList": [
    {
      "opListHeaderId": "159",
      "opListLineId": "86",
      "opListSeq": "309",
      "opSpInsName": "標籤LOT",
      "opCategoryAttri": "包裝",
      "opAssDesc": "23T537201Y", 
      "opOptDesc": "委外標籤奇菱LOT為：",
      "opFlagAssign": "Y",
      "opFlagOrderType": "W"
    },
    {
      "opListHeaderId": "53",
      "opListLineId": "84",
      "opListSeq": "310",
      "opSpInsName": "外袋領用量",
      "opCategoryAttri": "包裝",
      "opAssDesc": "10只",
      "opOptDesc": "外袋領用量為: ",
      "opFlagAssign": "Y",
      "opFlagOrderType": "W"
    }
  ],
  "opWoSisList": [
    {
      "opListHeaderId": "79",
      "opListLineId": "1403",
      "opListSeq": "305",
      "opSpInsName": "外袋別",
      "opCategoryAttri": "包裝",
      "opAssDesc": "",
      "opOptDesc": "PC樹脂(PE袋)",
      "opFlagAssign": "N",
      "opFlagOrderType": "L",
      "opShowText": "PC樹脂(PE袋)"
    }
  ]
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* {organizationId} 為登入者工廠別ID
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* 遍歷opSisList，若{opListHeaderId}等於"159"，執行SQL-2，檢查委外工單lot是否有重複，如果存在 [return 400,WO_LOT_DUPLICATE,欄位相關訊息]
* 若{opSapWc}不為空，執行SQL-3，將sap工作中心轉換為OPMS的設備ID，覆寫{opEquipId}
* 若{opIsNew}等於"Y"，執行新增
  * 亂數產生一組全大寫 UUID，覆寫{opWoUid}
  * 執行SQL-4，產生woSeq，覆寫{opWoSeq}
  * 執行SQL-5，產生流水號，覆寫{opLotNo}
  * 執行SQL-6，新增工單
* 若{opIsNew}等於"N"，執行更新
  * 執行SQL-7，更新工單
* 執行SQL-8，刪除舊有原料，遍歷{opMtrList}，執行SQL-9，新增原料
* 執行SQL-10，刪除舊有色料，遍歷{opPgmList}，執行SQL-11，新增色料
* 執行SQL-12，刪除舊有出貨指示
* 若{opWoNo}前兩碼為"58"表示為委外工單，執行SQL-13，刪掉已紀錄的LOT
* 遍歷{opSisList}
  * 如果 {opAssDesc}不為空且opListHeaderId="53" ，則 {opAssDesc} = {opAssDesc} + "只"
  * 執行SQL-14，新增工單出貨指示
  * 若{opListHeaderId}為"159"且{opAssDesc}不為空，執行SQL-15，新增lot給標籤機使用
* 遍歷{opWoSisList}，執行SQL-14，新增訂單出貨指示
* 上述過程如有執行任何資料庫任何異動，執行 SQL-LOG 紀錄 LOG，{DATAKEY} = {opWoUid}
  有系統錯誤 return 500,SYSTEM_ERROR,正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-2: 檢查lot是否有重複
```sql
SELECT LOT_NO FROM PS_L WHERE WO_NO != {opWoNo} AND LOT_NO = {opAssDesc}
```

SQL-3: sap工作中心轉換為OPMS的設備ID
```sql
SELECT EQUIP_ID FROM EQUIP_H WHERE ORGANIZATION_ID = {organizationId} AND SAP_WC = {opSapWC}
```

SQL-4: 產生woSeq
```sql
SELECT 
  ISNULL(MAX(WO_SEQ),0) + 1 AS WO_SEQ 
FROM 
  WO_H
WHERE 
  ORGANIZATION_ID = {organizationId}
  AND WO_DATE = {opWoDate}
```

SQL-5: 產生流水號
```sql
SELECT 
  ISNULL(MAX(LOT_NO),0) + 1 AS LOT_NO
FROM 
  WO_H 
WHERE ORGANIZATION_ID = {organizationId}
  AND WO_DATE >= CONVERT(DATETIME, '{opWoDate年份/01/01}')
  AND WO_DATE <= CONVERT(DATETIME, '{opWoDate年份/12/31}')
```

SQL-6: 新增工單
```sql
INSERT INTO WO_H (
  WO_UID,
  ORGANIZATION_ID,
  BOM_UID,
  EQUIP_ID,
  WO_NO,
  WO_SEQ,
  LOT_NO,
  CUST_PO_NO,
  WO_DATE,
  ACTIVE_FLAG,
  PRODUNIT_NO,
  CUST_NO,
  CUST_NAME,
  INDIR_CUST,
  SAP_OPERATION,
  SAP_WC,
  WIP_CLASS_NO,
  WIP_CLASS_CODE,
  ALT_BOM_PART_NO,
  ALT_BOM_DESIGNATOR,
  ALT_ROUTING_PART_NO,
  ALT_ROUTING_DESIGNATOR,
  COMPLETION_SUBINV,
  SALES_ORDER_NO,
  SALES_ORDER_SEQ,
  SALES_QTY,
  ESTIMATED_SHIP_DATE,
  SALES_NO,
  UNIT,
  WEIGHT,
  WO_QTY,
  RAW_QTY,
  BATCH_QTY,
  BATCH_TIMES,
  REMAIN_QTY,
  REMAIN_TIMES,
  REMARK,
  BOM_REMARK,
  STD_N_QUANTITY_PER,
  CUS_N_QUANTITY_PER,
  GRADE_NO,
  PROD_REMARK,
  CTN_WEIGHT,
  WO_TYPE,
  CDT,
  CREATE_BY,
  UDT,
  UPDATE_BY 
) 
VALUES(
  {opWoUid},
  {organizationId},
  {opBomUid},
  {opEquipId},
  {opWoNo},
  {opWoSeq},
  {opLotNo},
  {opCustPoNo},
  {opWoDate},
  {opActiveFlag},
  {opProdunitNo},
  {opCustNo},
  {opCustName},
  {opIndirCust},
  {opSapOperation},
  {opSapWc},
  {opWipClassNo},
  {opWipClassCode},
  {opAltBomPartNo},
  {opAltBomDesignator},
  {opAltRoutingPartNo},
  {opAltRoutingDesignator},
  {opCompletionSubinv},
  {opSalesOrderNo},
  {opSalesOrderSeq},
  {opSalesQty},
  {opEstimatedShipDate},
  {opSalesNo},
  {opUnit},
  {opWeight},
  {opWoQty},
  {opRawQty},
  {opBatchQty},
  {opBatchTimes},
  {opRemainQty},
  {opRemainTimes},
  {opRemark},
  {opBomRemark},
  {opStdNQuantityPer},
  {opCusNQuantityPer},
  {opGradeNo},
  {opProdRemark},
  {opCtnWeight},
  {opWoType},
  GETDATE(),
  {登入者使用者ID},
  GETDATE(),
  {登入者使用者ID}
)
```

SQL-7: 更新工單
```sql
UPDATE WO_H 
SET EQUIP_ID = {opEquipId},
  CUST_PO_NO = {opCustPoNo},
  CUST_NO = {opCustNo},
  CUST_NAME = {opCustName},
  INDIR_CUST = {opIndirCust},
  SALES_NO = {opSalesNo},
  PRODUNIT_NO = {opProdunitNo},
  REMARK = {opRemark},
  ACTIVE_FLAG = {opActiveFlag},
  WO_QTY = {opWoQty},
  RAW_QTY = {opRawQty},
  BATCH_QTY = {opBatchQty},
  BATCH_TIMES = {opBatchTimes},
  REMAIN_QTY = {opRemainQty},
  REMAIN_TIMES = {opRemainTimes},
  WO_DATE ={opWoDate},
  WIP_CLASS_NO = {opWipClassNo},
  WIP_CLASS_CODE = {opWipClassCode},
  ALT_BOM_PART_NO = {opAltBomPartNo},
  ALT_BOM_DESIGNATOR = {opAltBomDesignator},
  ALT_ROUTING_PART_NO = {opAltRoutingPartNo},
  ALT_ROUTING_DESIGNATOR = {opAltRoutingDesignator},
  COMPLETION_SUBINV = {opCompletionSubinv},
  SALES_ORDER_NO = {opSalesOrderNo},
  SALES_ORDER_SEQ = {opSalesOrderSeq},
  SALES_QTY = {opSalesQty},
  SAP_OPERATION = {opSapOperation},
  ESTIMATED_SHIP_DATE ={opEstimatedShipDate},
  GRADE_NO = {opGradeNo},
  PROD_REMARK = {opProdRemark},
  CTN_WEIGHT = {opCtnWeight},
  WO_TYPE = {opWoType},
  SAP_WC = {opSapWc},
  BOM_UID = {opBomUid},
  BOM_REMARK = {opBomRemark},
  STD_N_QUANTITY_PER = {opStdNQuantityPer},
  CUS_N_QUANTITY_PER = {opCusNQuantityPer},
  UDT = GETDATE(),
  UPDATE_BY = {登入者使用者ID} 
WHERE
  WO_UID = {opWoUid}
```

SQL-8: 刪除舊有原料
```sql
DELETE WO_MTR WHERE WO_UID = {opWoUid}
```

SQL-9: 新增原料
```sql
INSERT INTO WO_MTR(WO_UID, WO_MTR_PART_NO, QUANTITY_PER, UNIT, CDT, CREATE_BY, UDT, UPDATE_BY) 
VALUES({opWoUid}, {opWoMtrPartNo}, {opQuantityPer}, {opUnit}, GETDATE(), {登入者使用者ID}, GETDATE(), {登入者使用者ID})
```

SQL-10: 刪除舊有色料
```sql
DELETE WO_PGM WHERE WO_UID = {opWoUid}
```

SQL-11: 新增色料
```sql
INSERT INTO WO_PGM (
  WO_UID, 
  WO_PGM_PART_NO, 
  QUANTITY_PER, 
  UNIT, 
  SITE_PICK, 
  SELF_PICK, 
  PGM_BATCH_QTY, 
  PGM_REMAIN_QTY, 
  FEEDER_SEQ, 
  CDT, 
  CREATE_BY, 
  UDT, 
  UPDATE_BY 
)
VALUES(
  {opWoUid}, 
  {opWoPgmPartNo}, 
  {opQuantityPer}, 
  {opUnit}, 
  {opSitePick}, 
  {opSelfPick}, 
  {opPgmBatchQty}, 
  {opPgmRemainQty}, 
  {opFeederSeq}, 
  GETDATE(),
  {登入者使用者ID},
  GETDATE(),
  {登入者使用者ID}
)
```

SQL-12: 刪除舊有出貨指示
```sql
DELETE WO_SIS WHERE WO_UID = {opWoUid}
```

SQL-13: 委外工單刪掉已紀錄的LOT
```sql
DELETE FROM PS_L WHERE WO_UID = {opWoUid}
```

SQL-14: 新增出貨指示
```sql
INSERT INTO WO_SIS (
  WO_UID,
  LIST_HEADER_ID,
  CATEGORY_ATTRI,
  SP_INS_NAME,
  LIST_LINE_ID,
  LIST_SEQ,
  FLAG_ASSIGN,
  FLAG_ORDER_TYPE,
  OPT_DESC,
  ASS_DESC,
  CDT,
  CREATE_BY,
  UDT,
  UPDATE_BY 
)
VALUES(
  {opWoUid},
  {opListHeaderId},
  {opCategoryAttri},
  {opSpInsName},
  {opListLineId},
  {opListSeq},
  {opFlagAssign},
  {opFlagOrderType},
  {opOptDesc},
  {opAssDesc},
  GETDATE(),
  {登入者使用者ID},
  GETDATE(),
  {登入者使用者ID}
)
```

SQL-15: 新增lot給標籤機使用
```sql
INSERT INTO PS_L 
SELECT 
  W.WO_UID,
  W.WO_NO,
  E.EQUIP_ID,
  CONVERT (VARCHAR (10), W.WO_DATE, 111),
  NULL,
  {opAssDesc},
  NULL,
  NULL,
  NULL,
  NULL,
  GETDATE (),
  {登入者使用者ID} 
FROM
  WO_H W,
  SYS_PARA_D S,
  EQUIP_H E 
WHERE
  W.COMPLETION_SUBINV = S.PARA_VALUE 
  AND S.PARA_ID = 'LOT_OUTSORCE' 
  AND S.PARA_CODE = E.EQUIP_ID 
  AND W.WO_UID = {opWoUid}
```

SQL-LOG:
```sql
INSERT INTO  SYS_MODI_LOG (CATEGORY,DATAKEY,USERID,USER_NAME,LOG) VALUES ('WO',{DATAKEY},{登入者使用者ID},{登入者使用者名稱}, NULL)
```

# Response 欄位
 | 欄位    | 名稱   | 資料型別 | 資料儲存 & 說明 |
 | ------- | ------ | -------- | --------------- |
 | opWoUid | 工單ID | string   |                 |


## Response 範例

```json
{
    "msgCode": null,
    "result": {
       "content":{
          "opWoUid":"2B7D3005-A1B7-4967-8F7B-963FCFA037C0"
      }
    }
}
```
