# BASE.共用基礎資料-取得生產單位列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得生產單位列表

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230501 | 新增規格 | Nick   |
| 20230923 | 增加參數 | Ellen   |
| 20230925 | 回傳增加opSapProdunit | Ellen |


## 來源URL及資料格式

| 項目   | 說明                 |
| ------ | -------------------- |
| URL    | /comm/list_produnits |
| method | post                 |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。  
| 欄位             | 名稱         | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明                                                   |
| ---------------- | ------------ | -------- | :----------------------: | ----------------------------------------------------------------- |
| opByUser         | 依使用者過濾 | string   |            O             | Y/N  Y: 是 / N: 否，預設`N`                                       |
| opPage           | 第幾頁       | integer  |            O             | 預設`-1`，-1:取得全部 / 1  :第一頁 / 2:第二頁 / 3:第三頁 以此類推 |
| opPageSize       | 分頁筆數     | integer  |            O             | 預設 `10`                                                         |
| opOrganizationId | 工廠別       | string   |            O             | 預設 `null`  null:全部 / 5000:奇菱 / 6000:菱翔                    |

#### Request 範例

```json
{
  "opPage": -1,
  "opPageSize":10
  
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* {opPage} 如為 -1 ，SQL不需組分頁條件；反之{opPage} 不為 -1，{offset} 為起始列印 row ，第一頁為 0，第二頁後為 0 + (opPageSize*(opPage-1))
* {opOrganizationId} 如為 null 表全部，不指定工廠別條件，否則按照前端條件指定。
* 若{opByUser}等於Y 參考 SQL-2 查回結果 result
* 否則 參考 SQL-1 查回結果 result
* Response 結果需含分頁資訊，無資料 pageInfo 裡面項目皆給 0，頁數與分頁筆數前端如有提供，依前端判斷給的資料計算相關數值及拆分頁面，{opPage} 如為 -1，代表一次帶回全部資料，如有資料，{pageNumber}等於{pages}等於 1，{pageSize}等於{total}
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
SELECT PRODUNIT_NO
      ,PRODUNIT_NAME
      ,ORGANIZATION_ID
      ,SAP_PRODUNIT
  FROM PRODUNIT
  WHERE 1=1 
  --指定工廠別條件
  AND ORGANIZATION_ID = {opOrganizationId} 
  ORDER BY PRODUNIT_NO 
  --分頁條件
  OFFSET {offset} ROWS
  FETCH NEXT {opageSize} ROWS ONLY
```

SQL-2: 取得生產單位-依使用者過濾
```sql
SELECT 
  P.PRODUNIT_NO,
  P.PRODUNIT_NAME,
  P.ORGANIZATION_ID,
  P.SAP_PRODUNIT
FROM PRODUNIT P 
INNER JOIN SYS_USER_PRODUNIT U ON P.PRODUNIT_NO=U.PRODUNIT_NO 
WHERE 
  U.USER_ID= {登入者使用者ID}
ORDER BY PRODUNIT_NO 
  --分頁條件
  OFFSET {offset} ROWS
  FETCH NEXT {opageSize} ROWS ONLY
```


# Response 欄位
| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
| -------- | -------- | -------- | --------------- |
| pageInfo | 分頁資訊 | object   |                 |
| content  | 附帶訊息 | array    |                 |
#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明 |
| ---------- | ------------ | -------- | --------------- |
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |

#### 【content】array
| 欄位             | 名稱         | 資料型別 | 來源資料 & 說明 |
| ---------------- | ------------ | -------- | --------------- |
| opProdunitNo     | 生產單位代號 | string   | PRODUNIT_NO     |
| opProdunitName   | 生產單位名稱 | string   | PRODUNIT_NAME   |
| opOrganizationId | 工廠別       | string   | ORGANIZATION_ID |
| opSapProdunit    | SAP生產單位代號 | string  | SAP_PRODUNIT   |

#### Response 範例

```json
{
    "msgCode": null,
    "result": {
        "pageInfo": {
        "pageNumber": 1, 
        "pageSize": 17,
        "pages": 1,      
        "total": 17     
        },
       "content": [
          {"opProdunitNo":"001", "opProdunitName":"憲成","opOrganizationId":"5000","opSapProdunit":"1010368"},
          {"opProdunitNo":"002", "opProdunitName":"良輝","opOrganizationId":"5000","opSapProdunit":"1002206"},
          {"opProdunitNo":"004", "opProdunitName":"敬揚","opOrganizationId":"5000","opSapProdunit":"1012764"},
          {"opProdunitNo":"005", "opProdunitName":"資堡","opOrganizationId":"5000","opSapProdunit":"1010629"},
          {"opProdunitNo":"008", "opProdunitName":"高嘉","opOrganizationId":"5000","opSapProdunit":"1000059"},
          {"opProdunitNo":"028", "opProdunitName":"松旻","opOrganizationId":"5000","opSapProdunit":"1021616"},
          {"opProdunitNo":"999", "opProdunitName":"委外","opOrganizationId":"5000","opSapProdunit":null},
          {"opProdunitNo":"C1N", "opProdunitName":"染色","opOrganizationId":"6000","opSapProdunit":"311010"},
          {"opProdunitNo":"C2N", "opProdunitName":"複材一課","opOrganizationId":"6000","opSapProdunit":"311120"}
      ]
    }
}
```

