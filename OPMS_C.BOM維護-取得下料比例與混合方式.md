# BOM維護-取得下料比例與混合方式
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得下料比例與混合方式

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20231012 | 新增規格 | Ellen  |

## 來源URL及資料格式

| 項目   | 說明                  |
| ------ | --------------------- |
| URL    | /bom/get_bom_fed_rate |
| method | post                  |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位      | 名稱     | 資料型別 | 必填  | 資料儲存 & 說明 |
| --------- | -------- | :------: | :---: | --------------- |
| opBomUid  | BOM ID   |  string  |   M   |                 |
| opEquipId | 機台代號 |  string  |   O   | 預設: `null`    |

#### Request 範例

```json
{
   "opBomUid":"5000-L9300-MBXXXXXXXXXX02"
}
```

# Request 後端流程說明

可參考舊程式 BomManageAction.ajaxBomFedRate
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 執行 SQL-1 取得下料設備與比例
* 執行 SQL-2 取得下料設備與比例明細
* 遍歷 SQL-2取得混合方式：
  * 混合方式為 FEEDER_NO 以 "-"切分後半段
  * 若 FEEDER_NO為F1開頭，混合方式填入{opF1MixType}
  * 若 FEEDER_NO為F2開頭，混合方式填入{opF2MixType}
  * 若 FEEDER_NO為F3開頭，混合方式填入{opF3MixType}
  * 若 FEEDER_NO為F4開頭，混合方式填入{opF4MixType}
  * 若 FEEDER_NO為F5開頭，混合方式填入{opF5MixType}
  * 若 FEEDER_NO為F6開頭，混合方式填入{opF6MixType}
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容


# Request 後端邏輯說明

SQL-1: 取得下料設備與比例
```sql
-- BomQueryDao.getBomFedRate
SELECT
	A.BOM_UID,
	A.BOM_FED_UID,
	A.EQUIP_ID,
	A.F_1_RATE,
	A.F_2_RATE,
	A.F_3_RATE,
	A.F_4_RATE,
	A.F_5_RATE,
	A.F_6_RATE,
	A.BDP_RATE,
	A.PPG_RATE,
	A.DBE_RATE,
	REPLACE(CONVERT (VARCHAR (19), A.UDT, 120), '-', '/') UDT,
	A.UPDATE_BY,
	E.EQUIP_NAME,
	E.PRODUNIT_NO,
	P.PRODUNIT_NAME 
FROM
	BOM_FED A
	LEFT JOIN EQUIP_H E ON A.EQUIP_ID = E.EQUIP_ID
	LEFT JOIN PRODUNIT P ON E.ORGANIZATION_ID = P.ORGANIZATION_ID 
	AND E.PRODUNIT_NO = P.PRODUNIT_NO 
WHERE
	A.BOM_UID = {opBomUid}
	AND (A.EQUIP_ID IS NULL OR A.EQUIP_ID = {opEquipId}) 
ORDER BY
	EQUIP_ID
```

SQL-2: 取得下料設備與比例明細
```sql
-- BomQueryDao.getBomFedMixType
SELECT DISTINCT FEEDER_NO FROM BOM_FED_D WHERE BOM_FED_UID = {BOM_FED_UID(SQL-1)}
```

# Response 欄位
| 欄位           | 名稱              | 資料型別 | 資料儲存 & 說明                 |
| -------------- | ----------------- | -------- | ------------------------------- |
| opBomFedUid    | BOM_FED UID       | string   | BOM_FED_UID(SQL-1)              |
| opBomUid       | BOM ID            | string   | BOM_UID(SQL-1)                  |
| opEquipId      | 機台代號          | string   | EQUIP_ID(SQL-1)                 |
| opEquipName    | 機台名稱          | string   | EQUIP_NAME(SQL-1)               |
| opProdunitName | 生產單位          | string   | PRODUNIT_NAME(SQL-1)            |
| opProdunitNo   | 生產單位代號      | string   | PRODUNIT_NO(SQL-1)              |
| opF1Rate       | F1下料比例        | string   | F_1_RATE(SQL-1)                 |
| opF2Rate       | F2下料比例        | string   | F_2_RATE(SQL-1)                 |
| opF3Rate       | F3下料比例        | string   | F_3_RATE(SQL-1)                 |
| opF4Rate       | F4下料比例        | string   | F_4_RATE(SQL-1)                 |
| opF5Rate       | F5下料比例        | string   | F_5_RATE(SQL-1)                 |
| opF6Rate       | F6下料比例        | string   | F_6_RATE(SQL-1)                 |
| opF1MixType    | F1混合方式        | string   |                                 |
| opF2MixType    | F2混合方式        | string   |                                 |
| opF3MixType    | F3混合方式        | string   |                                 |
| opF4MixType    | F4混合方式        | string   |                                 |
| opF5MixType    | F5混合方式        | string   |                                 |
| opF6MixType    | F6混合方式        | string   |                                 |
| opBDP          | BDP (BDP外加比例) | string   | BDP_RATE (SQL-1)                |
| opPPG          | PPG               | string   | PPG_RATE (SQL-1)                |
| opDBE          | DBE               | string   | DBE_RATE (SQL-1)                |
| opUdt          | 編輯時間          | string   | UDT(SQL-1)  yyyy/MM/dd hh:mm:ss |
| opUpdateBy     |                   | string   | UPDATE_BY(SQL-1)                |


#### Response 範例

```json
{
  "msgCode":"",
  "result":{
    "content": [
      {
        "opBomFedUid": "39EF7D2F-AFED-403C-8553-7083C6920AB1",
        "opBomUid": "5000-L9300-MBXXXXXXXXXX02",
        "opEquipId": "11",
        "opEquipName": "複材#1",
        "opProdunitName": "",
        "opProdunitNo": "P1",
        "opF1Rate": "90.02",
        "opF2Rate": "9.98",
        "opF3Rate": "0.00",
        "opF4Rate": "0.00",
        "opF5Rate": "0.00",
        "opF6Rate": "0.00",
        "opF1MixType": "混合桶",
        "opF2MixType": "Mixaco",
        "opF3MixType": "",
        "opF4MixType": "",
        "opF5MixType": "",
        "opF6MixType": "",
        "opBDP": "0.00",
        "opPPG": "0.00",
        "opDBE": "0.00",
        "opUdt": "2018/09/19 11:09:21",
        "opUpdateBy": "A000067"
      }
    ]
  }
}
```
