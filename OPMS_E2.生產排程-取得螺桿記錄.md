# 生產排程-取得螺桿記錄
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得螺桿記錄


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230815 | 新增規格 | shawn  |

## 來源URL及資料格式

| 項目   | 說明                     |
| ------ | ------------------------ |
| URL    | /ps/equip_screw_query_pre |
| method | post                     |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         | 名稱     | 資料型別 | 必填  | 資料儲存 & 說明 |
| ------------ | -------- | :------: | :---: | --------------- |
| opQueryDateS | 開始日期 |  string  |   O   | yyyy/MM/dd      |
| opQueryDateE | 結束日期 |  string  |   O   | yyyy/MM/dd      |
| opEquipId    | 機台     |  string  |   M   |                 |

#### Request 範例

```json
{
  "opQueryDateS":"2023/01/01",  
  "opQueryDateE":"2023/02/01",    
  "opEquipId":"1"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 依傳入條件查詢sql-1，回傳列表資料
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
SELECT
	R.SCREW_UID,
	S.SCREW_NAME,
	E.EQUIP_NAME,
	R.EQUIP_ID,
	REPLACE(CONVERT(VARCHAR(16), R.START_DT, 120), '-', '/') START_DT,
	REPLACE(CONVERT(VARCHAR(16), R.END_DT, 120), '-', '/') END_DT,
	REPLACE(CONVERT(VARCHAR(16), R.CDT, 120), '-', '/') AS CDT,
	R.CREATE_BY,
	REPLACE(CONVERT(VARCHAR(16), R.UDT, 120), '-', '/') AS UDT,
	R.UPDATE_BY,
	U1.USER_NAME AS CREATE_BY_NAME,
	U2.USER_NAME AS UPDATE_BY_NAME
FROM
	EQUIP_SCREW R
INNER JOIN SCREW_H S ON
	R.SCREW_UID = S.SCREW_UID
INNER JOIN EQUIP_H E ON
	R.EQUIP_ID = E.EQUIP_ID
LEFT JOIN SYS_USER U1 ON
	R.CREATE_BY = U1.USER_ID
LEFT JOIN SYS_USER U2 ON
	R.UPDATE_BY = U2.USER_ID
WHERE
	R.EQUIP_ID ={opEquipId}
  --日期有傳入才加入條件
  [
	  AND (R.END_DT IS NULL	OR R.END_DT >= CONVERT(DATETIME, {opQueryDateS}))
	  AND R.START_DT <= CONVERT(DATETIME,{opQueryDateE})
  ]
ORDER BY
	START_DT
```


# Response 欄位
| 欄位           | 名稱     | 資料型別 | 資料儲存 & 說明       |
| -------------- | -------- | -------- | --------------------- |
| opScrewName    | 螺桿名稱 | string   | SCREW_NAME(sql-1)     |
| opStartDt      | 起始日期 | string   | START_DT(sql-1)       |
| opEndDt        | 截止日期 | string   | END_DT(sql-1)         |
| opUDT          | 更新時間 | string   | UDT(sql-1)            |
| opUpdateByName | 更新者   | string   | UPDATE_BY_NAME(sql-1) |



#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":[
		{
          "opScrewName":"1-120-F-2", 
          "opStartDt":"2019/04/02 16:30",
          "opEndDt":null,
          "opUDT":"2019/08/23 17:52",
          "opUpdateByName":"許登貴"
		},
		{
			...
		}
	]
  }
}
```

## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考
* EquipManageAction.equipScrewQueryPre