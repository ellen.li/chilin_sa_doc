# 使用者權限管理-取得單一使用者資料
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得單一使用者資料


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230405 | 新增規格 | Nick   |
| 20230509 | 新增生產單位 - opProdunitNoList | Nick|
| 20230517 | SYS_USER_ROLE 改 SYS_USER_ROLE_V3 | Nick |
| 20230519 | 應該要濾掉已經刪除的角色 | Nick|

## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    | /user/get_one |
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位    | 名稱 | 資料型別 | 必填 | 資料儲存 & 說明 |
| ------- | ---- | :------: | :--: | --------------- |
| opUserId | 使用者ID |  string  |  M  |                 |

#### Request 範例

```json
{
  "opUserId":"richman"
}
```

# Request 後端流程說明
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 執行  SQL-1 確認該使用者存在，找不到 return 400,"NOTFOUND"
* 執行  SQL-2 取回使用者所屬角色清單，找不到資料給 null，ex: "opRoleIdList":null
* 執行  SQL-3 取回使用者生產單位清單，找不到資料給 null，ex: "opProdunitIdList":null
* 密碼請解碼為明碼再回傳 
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容


# Request 後端邏輯說明

SQL-1:

```sql
SELECT CASE WHEN ISNUll(LDAP_SITE, '') = '' 
           THEN 'NA' 
           ELSE LDAP_SITE 
       END AS LDAP_SITE,
       USER_ID,
       USER_NAME,
       EMAIL,
       ORGANIZATION_ID,
       PASSWORD,
       CASE WHEN ISNUll(ACTIVE, '') = '' 
         THEN '0'
         ELSE ACTIVE 
       END AS ACTIVE ,
    FROM SYS_USER WHERE USER_ID = {opUserId}
```

SQL-2:

```sql
SELECT R.ROLE_ID FROM SYS_ROLE_V3 R 
  INNER JOIN SYS_USER_ROLE_V3 UR 
    ON R.ROLE_ID=UR.ROLE_ID 
    AND R.VISIBLE='1'
  WHERE UR.USER_ID={opUserId}
```

SQL-3:
```sql
SELECT PRODUNIT_NO,ORGANIZATION_ID FROM SYS_USER_PRODUNIT ORDER BY PRODUNIT_NO 
WHERE USER_ID={opUserId}
```


# Response 欄位
| 欄位             | 名稱             | 資料型別      | 資料儲存 & 說明 |
|------------------|------------------|---------------|-----------------|
| opLdapSite       | LDAP ID          | string        |                 |
| opUserId         | 使用者ID         | string        |                 |
| opUserName       | 使用者名稱       | string        |                 |
| opEmail          | email            | string        |                 |
| opOrgId          | 工廠別           | string        |                 |
| opActive         | 啟用             | string        |                 |
| opRoleIdList     | 使用者角色ID清單 | array integer |                 |
| opProdunitNoList | 生產單位代號清單 | array string  |                 |

#### Response 範例

```json
{
  "msgCode":"",
  "result":{
    "content":
      {
        "opLdapSite":"NA",
        "opUserId":"richman",
        "opUserName":"richman",
        "opEmail":"xxx@test.com",
        "opOrgId":"5000",
        "opActive":"1",
        "opRoleIdList": [601,932,101,221],
        "opProdunitNoList":[
          {"opProdunitNo:":"001","opOrganizationId":"5000"},
          {"opProdunitNo:":"002","opOrganizationId":"5000"}
        ]
      }
  }
}
```

