# 複材-SOC管理-失效
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230901 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /soc/soc_m_delete           |
| method | post                        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             | 名稱           | 資料型別 | 必填 | 資料儲存 & 說明     |
|------------------|---------------|----------|:----:|-------------------|
| opSocMUid        | ID            | string  |   M   |                   |

#### Request 範例

```json
{
  "opSocMUid":"B61455CC-52AA-48D2-B6DE-3D2AEC016F5F"
}
```
# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 取 { 登入者ID } SYS_USER.USER_ID
* 參考SQL-1更新資料
* 執行 SQL-LOG 紀錄更新資訊
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200


# Request 後端邏輯說明

SQL-1:

```sql
-- SocMModifyDao.deleteSocM
UPDATE SOC_M SET STATUS = 'D', UDT = GETDATE(), UPDATE_BY = {登入者ID} WHERE SOC_M_UID = {opSocMUid};
```

SQL-LOG:
```sql
INSERT INTO SYS_MODI_LOG (CATEGORY, DATAKEY, USERID, USER_NAME, LOG) VALUES ('SOC_M', {opSocMUid}, {登入者ID}, {登入者名稱}, '失效');
```

# Response 欄位

* 無

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":null
  }
}
```