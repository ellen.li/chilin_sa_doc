# 報表-色粉補正統計表-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)


**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230821 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /report/pgm_correction_list |
| method | post                        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱    |  資料型別 | 必填 | 資料儲存 & 說明  |
|--------------|---------|----------|:----:|-----------------|
| opBDate      | 開始日期 | string   |  O   | yyyy/mm/dd      |
| opEDate      | 結束日期 | string   |  O   | yyyy/mm/dd      |

#### Request 範例

```json
{
  "opBDate": "2019/01/01",
  "opEDate": "2019/01/31"
}
```

# Request 後端流程說明

* 取{ 登入者工廠別 } SYS_USER.ORGANIZATION_ID 欄位
* 參考SQL-1撈取資料
  * opBDate, opEDate 為非必填，有傳入時才輸入條件
* opModifyQty(補正數量)：取兩位小數並轉為千分位格式回傳
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
-- ReportQueryDao.getPgmCorrectionReport
SELECT P.WO_PGM_PART_NO, B.ITEM_NO, H.WO_NO, H.WO_DATE,
  P.MODIFY_QTY * H.BATCH_TIMES + P.MODIFY_UNIT_QTY * H.REMAIN_QTY * H.REMAIN_TIMES AS MODIFY_QTY
  FROM BOM_MTM B
  INNER JOIN WO_H H ON H.BOM_UID = B.BOM_UID
  INNER JOIN WO_PGM P ON H.WO_UID = P.WO_UID
  WHERE P.MODIFY_QTY > 0 AND H.ORGANIZATION_ID = { 登入者工廠別 }
  AND H.WO_DATE >= { opBDate }
  AND H.WO_DATE <= { opEDate }
ORDER BY P.WO_PGM_PART_NO,B.ITEM_NO

```

# Response 欄位
| 欄位           | 名稱         | 資料型別 | 資料儲存 & 說明                         |
|----------------|-------------|---------|----------------------------------------|
| opWoPgmPartNo  | 色料        | string  | WO_PGM.WO_PGM_PART_NO                   |
| opItemNo       | 料號        | string  | BOM_MTM.ITEM_NO                         |
| opWoNo         | 工單號碼     | string  | WO_H.WO_NO                              |
| opWoDate       | 日期        | string  | WO_H.WO_DATE  yyyy/mm/dd                |
| opModifyQty    | 補正數量     | string  | MODIFY_QTY 取兩位小數並轉為千分位格式     |

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content": [
      {
        "opWoPgmPartNo": "04-251-A-L091XXXXX",
        "opItemNo": "540AXXXOL1PFA19331",
        "opWoNo": "52008495",
        "opWoDate": "2019/07/12",
        "opModifyQty": "79.96"
      },
      {
        "opWoPgmPartNo": "05-201-V-07XXXXXXX",
        "opItemNo": "717CXXXXX1BXJ19401",
        "opWoNo": "51021946",
        "opWoDate": "2020/08/06",
        "opModifyQty": "1,023.00"
      },
      {
        "opWoPgmPartNo": "05-201-C-81XXXXXXX",
        "opItemNo": "DPA764BX1XBXC09212",
        "opWoNo": "58026898",
        "opWoDate": "2020/01/02",
        "opModifyQty": "2.51"
      }
    ]
  }
}
```




