# OPMS_D.工單列印-配色作業指圖
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

列印配色作業指圖


## 修改歷程

| 修改時間 | 內容                 | 修改者 |
| -------- | -------------------- | ------ |
| 20230731 | 新增規格             | Ellen  |
| 20230915 | 修正SQL-4            | Ellen  |
| 20230918 | 修正補量列印色粉邏輯 | Ellen  |
| 20231215 | 修正色料總合計算邏輯 | Ellen  |


## 來源URL及資料格式

| 項目   | 說明           |
| ------ | -------------- |
| URL    | /wo/print_wo_a |
| method | post           |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位

M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。

| 欄位          | 名稱                      |   資料型別   | 必填  | 資料儲存 & 說明             |
| ------------- | ------------------------- | :----------: | :---: | --------------------------- |
| opWoUidList   | 工單ID                    | array string |   M   |                             |
| opMultiplePgm | 指定是否使用1.2倍色粉列印 |    string    |   O   | Y/N  Y: 是 / N: 否，預設`Y` |
| opRawQty      | 投入原料量                |    string    |   O   | 預設 `null`                 |
| opBatchQty    | 每Batch用量               |    string    |   O   | 預設 `null`                 |
| opBatchTimes  | Batch用量次數             |    string    |   O   | 預設 `null`                 |
| opRemainQty   | 尾數量                    |    string    |   O   | 預設 `null`                 |
| opRemainTimes | 尾數量次數                |    string    |   O   | 預設 `null`                 |


#### Request 範例
```json
{
  "opWoUidList": [
    "00006D90-B5F1-4A80-9F62-B9989EBAE168", 
    "00008BEB-34CC-482D-B52E-AC3E0498AD4E"
    ],
  "opMultiplePgm": "Y"
}
```

# Request 後端流程說明

請參考舊版 `WebContent\wo\WoPrintA.jsp`
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* 執行 SQL-1，取得需列印的工單，確認傳入工單ID是否已存在，如果不存在 return 400 NOTFOUND
* 遍歷所有需列印的工單，{woUid}為單筆工單ID、{woNo}為單筆工單號碼、{organizationId}為單筆工單工廠別ID
  * 若{opRawQty}不為null，表示為補量列印，覆寫工單 RawQty, BatchQty, BatchTimes, RemainQty, RemainTimes欄位值
  * 執行 SAP-1 取得工單詳細資料，覆寫SQL-1結果各欄位值
  * 是否使用1.2倍色粉判斷，新增變數{useMultiplePgm}，若{opMultiplePgm}為Y {useMultiplePgm}等於true，否則為false
    * 若有色母基本配方料號則強制不做1.2倍色粉: {BASE_ITEM_NO(SQL-1)}不為空或空字串，{useMultiplePgm}等於 false
    * 若是複材則不做1.2倍色粉: {organizationId}等於5000且 {PRODUNIT_NO(SQL-1)}等於"P1"，{useMultiplePgm}等於 false
  * 執行 SQL-2 取得工單原料清單
  * 執行 SQL-3 取得工單色料清單
    * 若{opRawQty}不為null，表示為補量列印，遍歷SQL-3結果，PGM_BATCH_QTY等於`QUANTITY_PER * opBatchQty`取到小數點後三位 、PGM_REMAIN_QTY等於`QUANTITY_PER * opRemainQty`取到小數點後三位
    * 參考 [OPMS_共用_色料計算公式](./OPMS_共用_色料計算公式.md) 計算色料總合
    * `calculatePgm(SQL-3結果, SQL-2結果, BATCH_QTY(SQL-1), BATCH_TIMES(SQL-1), 
			REMAIN_QTY(SQL-1), REMAIN_TIMES(SQL-1), CUS_N_QUANTITY_PER(SQL-1), {useMultiplePgm})`
    * 若計算結果{WoPgmList}不等於`null`
      * 加總色料計算結果{WoPgmList.quantityPer}，暫存為變數{mTotal}
      * 換算色料總合：新增變數{mTotalPgmQtyPerN} 等於 `{mTotal} * 1000 / ((1000 + {mTotal})四捨五入到第5位)`
      * 換算1倍色料總合：新增變數{mTotalPgmQtyPer1} 等於 `1000 * {mTotalPgmQtyPerN} / (1000 * {BOM_MULTIPLE(SQL-1)} - {mTotalPgmQtyPerN})`
  * 執行 SQL-4 取得醫療級或一般級，只取第一筆
  * 執行 SQL-5 取得工單補正記錄
    * 執行 SQL-5-1 取得工單補正記錄色粉
  * 執行 SQL-6 取得工單出貨指示
  * 執行 SQL-7 更新列印指圖
  * 若{opRawQty}不為null，表示為補量列印，執行 SQL-8 新增補量列印紀錄
* 執行JASPER
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SAP-1：
可參考舊版 WoQueryRfc_getWoHSingle
程序如有任何異常，紀錄錯誤LOG並回傳NULL

**獲得BAPI方法**
|              | 型態   | 參數值                     | 說明 |
| ------------ | ------ | -------------------------- | ---- |
| BAPIFunction | string | Z_PP_OPMS_PRODUCTION_ORDER |      |

**設定輸入參數**
| 參數名稱 | 參數值           | 型態         | 說明 |
| -------- | ---------------- | ------------ | ---- |
| I_WERKS  | {organizationId} | string       |      |
| I_AUFNR  | {woNo}           | string       |      |
| I_ERDAT  |                  | JCoStructure |      |

I_ERDAT JCoStructure 參數
| 參數名稱 | 參數值     | 型態   | 說明 |
| -------- | ---------- | ------ | ---- |
| ERDAT_FR | "00000000" | string |      |
| ERDAT_TO | "99991231" | string |      |


| 取得回傳表格名稱 |
| ---------------- |
| T_ORDER          |

| 物件元素名稱      | 資料型別 | SAP欄位名稱 | SAP資料型別 | 物件元素回傳值 | 說明                                                         |
| ----------------- | -------- | ----------- | ----------- | -------------- | ------------------------------------------------------------ |
| WoNo              | string   | AUFNR       | string      | 同SAP          | 工單號碼，過濾前綴0                                          |
| activeFlag        | string   | STATUS      | string      | 同SAP          | 狀態                                                         |
| woDate            | string   | GSTRP       | Date        | 同SAP          | 使用日期，format yyyy/MM/dd                                  |
| color             | string   | COLOR       | string      | 同SAP          | 顏色                                                         |
| patternNo         | string   | COLOR_NO    | string      | 同SAP          | 色號                                                         |
| itemNo            | string   | MATNR       | string      | 同SAP          | 料號                                                         |
| itemDesc          | string   | MAKTX       | string      | 同SAP          | 料號摘要                                                     |
| bomNo             | string   | STLAL       | string      | 同SAP          | BOM                                                          |
| gradeNo           | string   | GRADE       | string      | 同SAP          | 原料                                                         |
| produnitNo        | string   | STAND       | string      | 同SAP          | 生產單位代碼，過濾前綴0，若為空顯示"NA"                      |
| produnitName      | string   | STAND_T     | string      | 同SAP          | 生產單位                                                     |
| salesNo           | string   | ERNAM       | string      | 同SAP          | 業助代號                                                     |
| custNo            | string   | KUNNR       | string      | 同SAP          | 客戶代號，過濾前綴0                                          |
| custName          | string   | SORT2       | string      | 同SAP          | 客戶名稱                                                     |
| indirCust         | string   | IHREZ       | string      | 同SAP          | 間接客戶                                                     |
| custPoNo          | string   | CUSTPO      | string      | 同SAP          | 客戶PO單號                                                   |
| salesQty          | string   | SOQTY       | double      | 同SAP          | 銷售數量，取至小數點後第五位                                 |
| wipClassNo        | string   | AUART       | string      | 同SAP          | 工單類別                                                     |
| wipClassCode      | string   | AUART_T     | string      | 同SAP          | 工單類別Code                                                 |
| sapOperation      | string   | VORNR       | string      | 同SAP          | 過濾前綴0                                                    |
| sapWc             | string   | ARBPL       | string      | 同SAP          | 工作中心                                                     |
| equipName         | string   | ARBPL_T     | string      | 同SAP          | 指定機台                                                     |
| woQty             | string   | GAMNG       | double      | 同SAP          | 工單數量，取至小數點後第五位                                 |
| rawQty            | string   | GAMNG       | double      | 同SAP          | 投入原料量，若H.RAW_QTY(SQL-1)為空才取值，取至小數點後第五位 |
| unit              | string   | GMEIN       | string      | 同SAP          | 單位                                                         |
| estimatedShipDate | string   | SHIP_DATE   | date        | 同SAP          | 預計出貨日                                                   |
| completionSubinv  | string   | LGORT       | string      | 同SAP          | 完工倉別                                                     |
| salesOrderNo      | string   | KDAUF       | string      | 同SAP          | 銷售訂單，過濾前綴0                                          |
| salesOrderSeq     | string   | KDPOS       | string      | 同SAP          | 銷售訂單序號，過濾前綴0                                      |
| cmcSoType         | string   | CMC_SO_TYPE | string      | 同SAP          | 1: 奇美內銷(大黃標籤) /  2: 奇美外銷(外銷標籤) / 3: 其他     |


SQL-1:
```sql
SELECT
  H.WO_UID,
  H.ACTIVE_FLAG,
  H.BOM_UID,
  H.EQUIP_ID,
  H.ORGANIZATION_ID,
  H.WO_NO,
  H.WO_SEQ,
  H.LOT_NO,
  H.CUST_PO_NO,
  CONVERT ( VARCHAR ( 10 ), H.WO_DATE, 111 ) AS WO_DATE,
  H.PRODUNIT_NO,
  H.CUST_NO,
  H.CUST_NAME,
  H.INDIR_CUST,
  B.CUST_NAME AS BASE_CUST,
  H.UNIT,
  H.WEIGHT,
  B.CMDI_NO,
  B.CMDI_NAME,
  B.COLOR,
  H.[ USE ],
  H.SALES_NO,
CASE
  H.RAW_QTY 
  WHEN cast( H.RAW_QTY AS INT ) THEN
  cast( CONVERT ( INT, H.RAW_QTY ) AS VARCHAR ( 20 ) ) ELSE cast( CONVERT ( FLOAT, H.RAW_QTY ) AS VARCHAR ( 20 ) ) 
  END RAW_QTY,
CASE
  H.BATCH_QTY 
  WHEN cast( H.BATCH_QTY AS INT ) THEN
  cast( CONVERT ( INT, H.BATCH_QTY ) AS VARCHAR ( 20 ) ) ELSE cast( CONVERT ( FLOAT, H.BATCH_QTY ) AS VARCHAR ( 20 ) ) 
  END BATCH_QTY,
  H.BATCH_TIMES,
CASE
  H.REMAIN_QTY 
  WHEN cast( H.REMAIN_QTY AS INT ) THEN
  cast( CONVERT ( INT, H.REMAIN_QTY ) AS VARCHAR ( 20 ) ) ELSE cast( CONVERT ( FLOAT, H.REMAIN_QTY ) AS VARCHAR ( 20 ) ) 
  END REMAIN_QTY,
  H.REMAIN_TIMES,
  H.MODIFY_FLAG,
  H.PRINT_FLAG,
  H.REMARK,
  H.BOM_REMARK,
  H.ERP_FLAG,
  H.WIP_CLASS_CODE,
  H.ALT_BOM_PART_NO,
  H.ALT_BOM_DESIGNATOR,
  H.ALT_ROUTING_PART_NO,
  H.ALT_ROUTING_DESIGNATOR,
  H.COMPLETION_SUBINV,
  H.COMPLETION_LOCATOR,
  H.SALES_ORDER_NO,
  H.SALES_QTY,
  H.OEM_ORDER_NO,
  CONVERT ( VARCHAR ( 10 ), H.ESTIMATED_SHIP_DATE, 111 ) AS ESTIMATED_SHIP_DATE,
  I.PATTERN_NO,
  B.ITEM_NO,
  B.ITEM_DESC,
  B.BOM_NO,
  B.BOM_NAME,
  B.BOM_DESC,
  B.[ USE ],
  B.BASE_ITEM_NO,
  B.BASE_BOM_NO,
  H.STD_N_QUANTITY_PER,
  H.CUS_N_QUANTITY_PER,
  REPLACE ( CONVERT ( VARCHAR ( 16 ), H.CDT, 120 ), '-', '/' ) AS CDT,
  H.CREATE_BY,
  REPLACE ( CONVERT ( VARCHAR ( 16 ), H.UDT, 120 ), '-', '/' ) AS UDT,
  H.UPDATE_BY,
  B.MULTIPLE BOM_MULTIPLE,
  H.CTN_WEIGHT,
  H.PROD_REMARK,
  H.SALES_ORDER_SEQ,
  B.BIAXIS_ONLY,
  B.DYE_ONLY,
  H.GRADE_NO,
  H.PROD_REMARK,
  H.PS_FLAG,
  H.WO_TYPE 
FROM
  WO_H H
  INNER JOIN BOM_MTM B ON H.BOM_UID = B.BOM_UID
  INNER JOIN ITEM_H I ON B.ITEM_NO = I.ITEM_NO 
WHERE
  H.WO_UID = {woUid}
```

SQL-2: 取得原料清單
```sql
SELECT
  R.WO_MTR_PART_NO,
CASE
  R.QUANTITY_PER 
  WHEN cast( R.QUANTITY_PER AS INT ) THEN
  cast( CONVERT ( INT, R.QUANTITY_PER ) AS VARCHAR ( 20 ) ) ELSE cast( CONVERT ( FLOAT, R.QUANTITY_PER ) AS VARCHAR ( 20 ) ) 
  END QUANTITY_PER,
  R.UNIT 
FROM
  WO_MTR R 
WHERE
  R.WO_MTR_PART_NO NOT LIKE '92%' 
  AND R.WO_UID = {woUid}
ORDER BY
  R.QUANTITY_PER DESC,
  R.WO_MTR_PART_NO
```

SQL-3: 取得色料清單
```sql
SELECT
	A.WO_PGM_PART_NO,
	A.QUANTITY_PER,
	A.UNIT,
	A.SITE_PICK,
	A.SELF_PICK,
	A.PGM_BATCH_QTY,
	A.PGM_REMAIN_QTY,
	A.MODIFY_QTY,
	A.MODIFY_UNIT_QTY,
	A.FEEDER_SEQ,
	B.BOM_UID AS PGM_BOM_UID 
FROM
	(
SELECT
	H.ORGANIZATION_ID,
	W.WO_PGM_PART_NO,
	W.QUANTITY_PER,
	W.UNIT,
	W.SITE_PICK,
	W.SELF_PICK,
	W.PGM_BATCH_QTY,
	W.PGM_REMAIN_QTY,
	W.MODIFY_QTY,
	W.MODIFY_UNIT_QTY,
	W.FEEDER_SEQ 
FROM
	WO_PGM W
	INNER JOIN WO_H H ON W.WO_UID = H.WO_UID 
WHERE
	W.WO_UID = {woUid}
	) A
	LEFT JOIN ( SELECT ORGANIZATION_ID, ITEM_NO, BOM_UID FROM BOM_MTM WHERE BOM_NO IS NULL AND ACTIVE_FLAG = 'Y' ) B ON A.ORGANIZATION_ID = B.ORGANIZATION_ID 
	AND A.WO_PGM_PART_NO = B.ITEM_NO 
ORDER BY
	WO_PGM_PART_NO
```

SQL-4: 取得醫療級或一般級
```sql
SELECT
	ISNULL((
    SELECT NULLIF(OPT_DESC, '') 
    FROM WO_SIS 
    WHERE 
      LIST_HEADER_ID = 158 
      AND WO_UID = {woUid}
  ), '□一般級  □醫療級') ISO_PROD_OPT
```

SQL-5: 取得補正記錄(只取最新4筆)
```sql
SELECT DISTINCT TOP 4 
  CONVERT ( VARCHAR ( 10 ), H.WO_DATE, 111 ) AS WO_DATE,
  H.WO_UID,
  H.WO_NO,
  H.CUST_NAME,
  H.LOT_NO 
FROM
  WO_H H
  INNER JOIN WO_PGM P ON H.WO_UID = P.WO_UID 
WHERE
  H.BOM_UID = {BOM_UID(SQL-1)}
  AND P.MODIFY_QTY > 0 
ORDER BY
  WO_DATE DESC,
  WO_NO DESC 
```

SQL-5-1: 取得補正記錄色粉
```sql
SELECT
	P.WO_PGM_PART_NO,
	P.QUANTITY_PER,
	P.MODIFY_UNIT_QTY,
	P.MODIFY_QTY 
FROM
	WO_PGM P 
WHERE
	P.WO_UID = {WO_UID(SQL-5)}
ORDER BY
	P.WO_PGM_PART_NO
```

SQL-6: 取得出貨指示
```sql
SELECT 
  S.OPT_DESC+ ISNULL(S.ASS_DESC, '') AS SHOW_TEXT 
FROM
  WO_H W
  INNER JOIN WO_SIS S ON W.WO_UID= S.WO_UID 
  INNER JOIN SYS_SIS_H H ON W.LIST_HEADER_ID = S.LIST_HEADER_ID
WHERE
  W.WO_UID = {woUid}
  AND H.ORGANIZATION_ID = {organizationId}
  AND H.R_OPMS_COLOR = 'Y' 
  AND H.ACTIVE_FLAG = 'Y' 
ORDER BY
  S.LIST_SEQ
```

SQL-7: 更新列印指圖
```sql
UPDATE WO_H SET PRINT_FLAG='Y' WHERE WO_UID = {woUid}
```

SQL-8: 補量列印紀錄
```sql
INSERT INTO WO_PRINTA_REC (WO_UID, RAW_QTY, BATCH_QTY, CDT, CREATE_BY)
VALUES({woUid},{opRawQty},{opBatchQty},GETDATE(),{登入者使用者ID})
```

JASPER:
**jasper檔案位置**

|          | 型態   | 參數值                  | 說明 |
| -------- | ------ | ----------------------- | ---- |
| RealPath | string | /report/WoPrintA.jasper |      |

**jasper fillReport參數**

| 參數名稱 | 名稱 | 型態         | 來源資料 & 說明 |
| -------- | ---- | ------------ | --------------- |
| pars     |      | array object |                 |
| jrds     |      | JRDataSource |                 |

pars object 參數

| 參數名稱          | 名稱           | 型態         | 來源資料 & 說明                                                                                    |
| ----------------- | -------------- | ------------ | -------------------------------------------------------------------------------------------------- |
| info              |                | string       | 參考是否使用1.2倍色粉判斷{useMultiplePgm}，使用為""，否則為`[不做1.2倍色粉判斷]`                   |
| today             | 當下日期       | string       | yyyy/MM/dd hh:mm:ss                                                                                |
| woDate            | 使用日期       | string       | H.WO_DATE(SQL-1)                                                                                   |
| woSeq             | 序號           | string       | H.WO_SEQ(SQL-1)                                                                                    |
| lotNo             | Lot No         | string       | H.LOT_NO(SQL-1)                                                                                    |
| patternNo         | 色號           | string       | I.PATTERN_NO(SQL-1)                                                                                |
| color             | 色相           | string       | B.COLOR(SQL-1)                                                                                     |
| rawQty            | 投入原料量     | string       | H.RAW_QTY(SQL-1) 加 UNIT(SQL-1)                                                                    |
| unit              | 單位           | string       | H.UNIT(SQL-1)                                                                                      |
| woNo              | 工單號碼       | string       | H.WO_NO(SQL-1)                                                                                     |
| custName          | 客戶名稱       | string       | H.CUST_NAME(SQL-1)                                                                                 |
| use               | 用途           | string       | B.USE(SQL-1)                                                                                       |
| itemNo            | 料號           | string       | B.ITEM_NO(SQL-1)                                                                                   |
| bomNo             | Bom No         | string       | B.BOM_NO(SQL-1)                                                                                    |
| cmdiName          | 品稱說明       | string       | B.CMDI_NAME(SQL-1)                                                                                 |
| produnitName      | 生產單位名稱   | string       | produnitName(SAP-1)                                                                                |
| salesNo           | 業助代號       | string       | H.SALES_NO(SQL-1)                                                                                  |
| batchQty          | 每Batch用量    | string       | H.BATCH_QTY(SQL-1)                                                                                 |
| batchTimes        | Batch用量次數  | string       | H.BATCH_TIMES(SQL-1)                                                                               |
| remainQty         | 尾數量         | string       | H.REMAIN_QTY(SQL-1)                                                                                |
| remainTimes       | 尾數量次數     | string       | H.REMAIN_TIMES(SQL-1) ，若H.REMAIN_QTY(SQL-1)等於0 填0                                             |
| userId            | 登入者使用者ID | string       | {登入者使用者ID}                                                                                   |
| woMtrPartNo       | 原料料號       | string       | R.WO_MTR_PART_NO(SQL-2) ，換行符號連接                                                             |
| mtrPercentage     | 原料單位用量   | string       | (R.QUANTITY_PER(SQL-2) + R.UNIT(SQL-2))，換行符號連接                                              |
| ISO_PROD_OPT      | 醫療級或一般級 | string       | ISO_PROD_OPT(SQL-4)，{organizationId}等於"5000"才取值                                              |
| totalPgmBatchQty  | Batch總用量    | string       | pgmTotalBatchQty(色料計算結果)，取到小數第3位                                                      |
| totalPgmRemainQty | 尾數總量       | string       | pgmTotalRemainQty(色料計算結果)，取到小數第3位                                                     |
| totalPgmQty       | 色粉總重       | string       | `pgmTotalQuantityPer(色料計算結果) * H.RAW_QTY(SQL-1) / 1000`，取到小數第3位                       |
| totalQty          | 總重           | string       | `(pgmTotalQuantityPer(色料計算結果) * H.RAW_QTY(SQL-1) / 1000) + H.RAW_QTY(SQL-1)`，取到小數第3位  |
| n4BatchQty        | N4 Batch總用量 | string       | n4BatchQty(色料計算結果)，取到整數                                                                 |
| n4RemainQty       | N4尾數總量     | string       | n4RemainQty(色料計算結果)，取到整數                                                                |
| n4TotalQty        | N4色粉總重     | string       | n4TotalQty(色料計算結果)，取到整數                                                                 |
| remark            | 備註           | string       | `H.BOM_REMARK(SQL-1) + "\r\n" + H.REMARK(SQL-1) + "\r\n" + String.join("\r\n", SHOW_TEXT(SQL-6)) ` |
| SUBREPORT_DIR     |                | string       | `application.getRealPath("/jasperReport/WoPrintA_Correct.jasper")`                                 |
| SubReportParam    |                | array object |                                                                                                    |
| SubReportDS       |                | JRDataSource |                                                                                                    |


SubReportParam object 參數
* 遍歷(SQL-5)結果，{i}為每次遍歷index + 1
  
| 參數名稱   | 名稱        | 型態   | 來源資料 & 說明                          |
| ---------- | ----------- | ------ | ---------------------------------------- |
| wo + {i}   | 日期-流水號 | string | `WO_DATE(SQL-5) + "-" + H.LOT_NO(SQL-5)` |
| cust + {i} | 客戶        | string | H.CUST_NAME(SQL-5)                       |

jrds JRDataSource 參數
* 遍歷色料計算結果{woPgmList}，{woPgm}為單筆色料
* 若{woPgm.quantityPer}, {woPgm.pgmBatchQty}, {woPgm.pgmRemainQty}皆小於0則跳過
* 若為1.2倍色粉，1.2倍色粉字樣放在W-83前面
  
| 參數名稱        | 名稱         | 型態   | 來源資料 & 說明                                                                    |
| --------------- | ------------ | ------ | ---------------------------------------------------------------------------------- |
| woPgmPartNo     | 色料         | string | `"★" + {woPgm.woPgmPartNo}`， 不為空或不等於"N"時加"★" 否則加"　"，節掉色粉尾端之X |
| quantityPer     | 單位用量     | string | `{woPgm.quantityPer}`                                                              |
| pgmBatchQty     | Batch用量    | string | `{woPgm.pgmBatchQty}`                                                              |
| pgmRemainQty    | 尾數用量     | string | `{woPgm.pgmRemainQty}`                                                             |
| pgmQty          | 總領用量     | string | `{woPgm.pgmQty}`                                                                   |
| baseQuantityPer | 色母基本配方 | string | `({woPgm.quantityPer} * (1000 - {mTotalPgmQtyPerN}) /1000) * ({mTotalPgmQtyPer1} + 1000) / (1000 * {BOM_MULTIPLE(SQL-1)})`，四捨五入取到小數第5位，若 BASE_ITEM_NO(SQL-1)為空則為空字串 |

SubReportDS JRDataSource 參數
* 遍歷(SQL-5)結果，{i}為每次遍歷index + 1，執行SQL-5-1，遍歷結果
  
| 參數名稱          | 名稱       | 型態   | 來源資料 & 說明                              |
| ----------------- | ---------- | ------ | -------------------------------------------- |
| woPgmPartNo       | 色粉料號   | string | P.WO_PGM_PART_NO(SQL-5-1)                    |
| quantityPer + {i} | 單位用量   | string | P.QUANTITY_PER(SQL-5-1)，取到小數第5位       |
| modifyPer + {i}   | 補正紀錄量 | string | `"+" + P.MODIFY_QTY(SQL-5-1)`，取到小數第5位 |



# Response

```
Content-Type: application/pdf
Content-Disposition: inline;filename=test.pdf
```

#### Response 範例
