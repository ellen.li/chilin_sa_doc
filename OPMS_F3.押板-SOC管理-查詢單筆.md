# 押板-SOC管理-查詢單筆
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容               | 修改者 |
|----------|------------------|--------|
| 20230831 | 新增規格           | 黃東俞 |
| 20231026 | 調整欄位說明文字    | Nick   |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /soc/soc_b_single           |
| method | post                        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位      | 名稱 | 資料型別 | 必填 | 資料儲存 & 說明     |
|----------|------|----------|:----:|-------------------|
| opSocBUid | ID  | string   |  M   |                   |

#### Request 範例

```json
{
  "opSocBUid": "6A135E88-190D-4C61-861A-969EFFA37DD6"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 參考 SQL-1 取得資料
* 將SQL-1取到的 ITEM_NO 欄位再帶入SQL-2，將取到的資料填入 opItemDesc 並回傳
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
-- SocMQueryDao.getSocMSingle
SELECT E.ORGANIZATION_ID, S.EQUIP_ID, E.EQUIP_NAME, S.ITEM_NO, S.SOC_B_UID, S.SOC_B_VER, S.STATUS,
  S.AC_1_STD, S.AC_1_MAX, S.AC_1_MIN, S.AC_2_STD, S.AC_2_MAX, S.AC_2_MIN, S.AC_3_STD, S.AC_3_MAX, S.AC_3_MIN,
  S.AC_4_STD, S.AC_4_MAX, S.AC_4_MIN, S.AC_5_STD, S.AC_5_MAX, S.AC_5_MIN, S.AC_6_STD, S.AC_6_MAX, S.AC_6_MIN,
  S.AC_7_STD, S.AC_7_MAX, S.AC_7_MIN, S.AC_8_STD, S.AC_8_MAX, S.AC_8_MIN, S.AC_9_STD, S.AC_9_MAX, S.AC_9_MIN,
  S.AC_A_STD, S.AC_A_MAX, S.AC_A_MIN,
  S.A_SCREW_STD, S.A_SCREW_MAX, S.A_SCREW_MIN, S.A_CURR_STD, S.A_CURR_MAX, S.A_CURR_MIN, S.A_GP_STD, S.A_GP_MAX, S.A_GP_MIN,
  S.BC_1_STD, S.BC_1_MAX, S.BC_1_MIN, S.BC_2_STD, S.BC_2_MAX, S.BC_2_MIN, S.BC_3_STD, S.BC_3_MAX, S.BC_3_MIN,
  S.BC_4_STD, S.BC_4_MAX, S.BC_4_MIN, S.BC_5_STD, S.BC_5_MAX, S.BC_5_MIN, S.BC_6_STD, S.BC_6_MAX, S.BC_6_MIN,
  S.BC_7_STD, S.BC_7_MAX, S.BC_7_MIN, S.BC_8_STD, S.BC_8_MAX, S.BC_8_MIN, S.BC_9_STD, S.BC_9_MAX, S.BC_9_MIN,
  S.BC_A_STD, S.BC_A_MAX, S.BC_A_MIN, S.BC_B_STD, S.BC_B_MAX, S.BC_B_MIN, S.BC_C_STD, S.BC_C_MAX, S.BC_C_MIN,
  S.BC_D_STD, S.BC_D_MAX, S.BC_D_MIN, S.BC_E_STD, S.BC_E_MAX, S.BC_E_MIN, S.BC_N_STD, S.BC_N_MAX, S.BC_N_MIN,
  S.B_SCREW_STD, S.B_SCREW_MAX, S.B_SCREW_MIN, S.B_CURR_STD,S.B_CURR_MAX, S.B_CURR_MIN, S.B_GP_STD, S.B_GP_MAX, S.B_GP_MIN,
  S.D_1_STD, S.D_1_MAX, S.D_1_MIN, S.D_2_STD, S.D_2_MAX, S.D_2_MIN, S.D_3_STD, S.D_3_MAX,S.D_3_MIN,
  S.D_4_STD, S.D_4_MAX, S.D_4_MIN, S.D_5_STD, S.D_5_MAX, S.D_5_MIN, S.D_6_STD, S.D_6_MAX,S.D_6_MIN,
  S.D_7_STD, S.D_7_MAX, S.D_7_MIN,
  S.R_1_STD, S.R_1_MAX, S.R_1_MIN, S.R_2_STD, S.R_2_MAX, S.R_2_MIN, S.R_3_STD, S.R_3_MAX, S.R_3_MIN,
  S.R_SPEED_STD, S.R_SPEED_MAX, S.R_SPEED_MIN, S.R_GAP_L,S.R_GAP_R, S.VACUUM_INDEX, S.LAYER_1, S.LAYER_2, S.LAYER_3, S.MODEL_GAP, S.MODEL_WIDTH,
  S.MFG_DESC, S.SOC_DESC, S.REMARK, S.SCREEN_FREQ, S.RECORD_FREQ,
  S.LAYER_1_MAX, S.LAYER_1_MIN, S.LAYER_2_MAX, S.LAYER_2_MIN, S.LAYER_3_MAX, S.LAYER_3_MIN,
  S.VACUUM_INDEX_A_STD, S.VACUUM_INDEX_A_MAX, S.VACUUM_INDEX_A_MIN,S.VACUUM_INDEX_B_STD, S.VACUUM_INDEX_B_MAX, S.VACUUM_INDEX_B_MIN,
  S.SCREEN_LAYER_A, S.SCREEN_LAYER_B, S.SC_BP_A_STD, S.SC_BP_B_STD, S.SC_BP_A_MAX, S.SC_BP_B_MAX, S.SC_BP_A_MIN, S.SC_BP_B_MIN,
  S.MTR_DRY_TEMP_A, S.MTR_DRY_TEMP_B, S.MTR_DRY_TIME_A, S.MTR_DRY_TIME_B, S.MTR_MIX_TIME_A, S.MTR_MIX_TIME_B,
  S.R_U, S.R_M, S.R_D, S.R_TEN_STD, S.R_TEN_MAX, S.R_TEN_MIN,
  R_TEN_SLOPE_STD, R_TEN_SLOPE_MAX, R_TEN_SLOPE_MIN,
  R_TEN_SPEED_STD, R_TEN_SPEED_MAX, R_TEN_SPEED_MIN,
  FEEDP_A_STD, FEEDP_A_MAX, FEEDP_A_MIN, FEEDP_B_STD, FEEDP_B_MAX,FEEDP_B_MIN,
  S.CDT, S.CREATE_BY, S.PUB_DATE, S.PUBLIC_BY, S.UDT, S.UPDATE_BY, U.USER_NAME PUBLIC_NAME
  FROM SOC_B S
  INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
  LEFT JOIN SYS_USER U ON S.PUBLIC_BY = U.USER_ID
  WHERE S.SOC_B_UID = {opSocBUid}
```

SQL-2

```sql
SELECT MAX(ITEM_DESC) AS DESCRIPTION FROM ITEM_H WHERE ITEM_NO = { SOC_B.ITEM_NO(SQL-1) }
```


# Response 欄位
| 欄位              | 名稱                  | 資料型別 | 資料儲存 & 說明                |
|-------------------|-----------------------|----------|--------------------------------|
| opSocBUid         | ID                    | string   | SOC_B.SOC_B_UID                |
| opEquipId         | 機台                  | string   | SOC_B.EQUIP_ID                 |
| opEquipName       | 機台名稱              | string   | EQUIP_H.EQUIP_NAME             |
| opItemNo          | 料號                  | string   | SOC_B.ITEM_NO                  |
| opItemDesc        | 料號摘要              | string   | DESCRIPTION(SQL-2)             |
| opSocBVer         | 版號                  | string   | SOC_B.SOC_B_VER                |
| opStatus          | 狀態                  | string   | SOC_B.STATUS                   |
| opAc1Std          | (A層)A1標準           | string   | SOC_B.AC_1_STD                 |
| opAc1Max          | (A層)A1上限           | string   | SOC_B.AC_1_MAX                 |
| opAc1Min          | (A層)A1下限           | string   | SOC_B.AC_1_MIN                 |
| opAc2Std          | (A層)A2標準           | string   | SOC_B.AC_2_STD                 |
| opAc2Max          | (A層)A2上限           | string   | SOC_B.AC_2_MAX                 |
| opAc2Min          | (A層)A2下限           | string   | SOC_B.AC_2_MIN                 |
| opAc3Std          | (A層)A3標準           | string   | SOC_B.AC_3_STD                 |
| opAc3Max          | (A層)A3上限           | string   | SOC_B.AC_3_MAX                 |
| opAc3Min          | (A層)A3下限           | string   | SOC_B.AC_3_MIN                 |
| opAc4Std          | (A層)A4標準           | string   | SOC_B.AC_4_STD                 |
| opAc4Max          | (A層)A4上限           | string   | SOC_B.AC_4_MAX                 |
| opAc4Min          | (A層)A4下限           | string   | SOC_B.AC_4_MIN                 |
| opAc5Std          | (A層)A5標準           | string   | SOC_B.AC_5_STD                 |
| opAc5Max          | (A層)A5上限           | string   | SOC_B.AC_5_MAX                 |
| opAc5Min          | (A層)A5下限           | string   | SOC_B.AC_5_MIN                 |
| opAc6Std          | (A層)A6標準           | string   | SOC_B.AC_6_STD                 |
| opAc6Max          | (A層)A6上限           | string   | SOC_B.AC_6_MAX                 |
| opAc6Min          | (A層)A6下限           | string   | SOC_B.AC_6_MIN                 |
| opAc7Std          | (A層)A7標準           | string   | SOC_B.AC_7_STD                 |
| opAc7Max          | (A層)A7上限           | string   | SOC_B.AC_7_MAX                 |
| opAc7Min          | (A層)A7下限           | string   | SOC_B.AC_7_MIN                 |
| opAc8Std          | (A層)A8標準           | string   | SOC_B.AC_8_STD                 |
| opAc8Max          | (A層)A8上限           | string   | SOC_B.AC_8_MAX                 |
| opAc8Min          | (A層)A8下限           | string   | SOC_B.AC_8_MIN                 |
| opAc9Std          | (A層)A9標準           | string   | SOC_B.AC_9_STD                 |
| opAc9Max          | (A層)A9上限           | string   | SOC_B.AC_9_MAX                 |
| opAc9Min          | (A層)A9下限           | string   | SOC_B.AC_9_MIN                 |
| opAcAStd          | (A層)A10標準          | string   | SOC_B.AC_A_STD                 |
| opAcAMax          | (A層)A10上限          | string   | SOC_B.AC_A_MAX                 |
| opAcAMin          | (A層)A10下限          | string   | SOC_B.AC_A_MIN                 |
| opAScrewStd       | (A層)螺桿標準         | string   | SOC_B.A_SCREW_STD              |
| opAScrewMax       | (A層)螺桿上限         | string   | SOC_B.A_SCREW_MAX              |
| opAScrewMin       | (A層)螺桿下限         | string   | SOC_B.A_SCREW_MIN              |
| opACurrStd        | (A層)電流標準         | string   | SOC_B.A_CURR_STD               |
| opACurrMax        | (A層)電流上限         | string   | SOC_B.A_CURR_MAX               |
| opACurrMin        | (A層)電流下限         | string   | SOC_B.A_CURR_MIN               |
| opAGpStd          | (A層)GP標準           | string   | SOC_B.A_GP_STD                 |
| opAGpMax          | (A層)GP上限           | string   | SOC_B.A_GP_MAX                 |
| opAGpMin          | (A層)GP下限           | string   | SOC_B.A_GP_MIN                 |
| opBc1Std          | (B層)C1標準           | string   | SOC_B.BC_1_STD                 |
| opBc1Max          | (B層)C1上限           | string   | SOC_B.BC_1_MAX                 |
| opBc1Min          | (B層)C1下限           | string   | SOC_B.BC_1_MIN                 |
| opBc2Std          | (B層)C2標準           | string   | SOC_B.BC_2_STD                 |
| opBc2Max          | (B層)C2上限           | string   | SOC_B.BC_2_MAX                 |
| opBc2Min          | (B層)C2下限           | string   | SOC_B.BC_2_MIN                 |
| opBc3Std          | (B層)C3標準           | string   | SOC_B.BC_3_STD                 |
| opBc3Max          | (B層)C3上限           | string   | SOC_B.BC_3_MAX                 |
| opBc3Min          | (B層)C3下限           | string   | SOC_B.BC_3_MIN                 |
| opBc4Std          | (B層)C4標準           | string   | SOC_B.BC_4_STD                 |
| opBc4Max          | (B層)C4上限           | string   | SOC_B.BC_4_MAX                 |
| opBc4Min          | (B層)C4下限           | string   | SOC_B.BC_4_MIN                 |
| opBc5Std          | (B層)C5標準           | string   | SOC_B.BC_5_STD                 |
| opBc5Max          | (B層)C5上限           | string   | SOC_B.BC_5_MAX                 |
| opBc5Min          | (B層)C5下限           | string   | SOC_B.BC_5_MIN                 |
| opBc6Std          | (B層)C6標準           | string   | SOC_B.BC_6_STD                 |
| opBc6Max          | (B層)C6上限           | string   | SOC_B.BC_6_MAX                 |
| opBc6Min          | (B層)C6下限           | string   | SOC_B.BC_6_MIN                 |
| opBc7Std          | (B層)C7標準           | string   | SOC_B.BC_7_STD                 |
| opBc7Max          | (B層)C7上限           | string   | SOC_B.BC_7_MAX                 |
| opBc7Min          | (B層)C7下限           | string   | SOC_B.BC_7_MIN                 |
| opBc8Std          | (B層)C8標準           | string   | SOC_B.BC_8_STD                 |
| opBc8Max          | (B層)C8上限           | string   | SOC_B.BC_8_MAX                 |
| opBc8Min          | (B層)C8下限           | string   | SOC_B.BC_8_MIN                 |
| opBc9Std          | (B層)C9標準           | string   | SOC_B.BC_9_STD                 |
| opBc9Max          | (B層)C9上限           | string   | SOC_B.BC_9_MAX                 |
| opBc9Min          | (B層)C9下限           | string   | SOC_B.BC_9_MIN                 |
| opBcAStd          | (B層)C10標準          | string   | SOC_B.BC_A_STD                 |
| opBcAMax          | (B層)C10上限          | string   | SOC_B.BC_A_MAX                 |
| opBcAMin          | (B層)C10下限          | string   | SOC_B.BC_A_MIN                 |
| opBcBStd          | (B層)C11標準          | string   | SOC_B.BC_B_STD                 |
| opBcBMax          | (B層)C11上限          | string   | SOC_B.BC_B_MAX                 |
| opBcBMin          | (B層)C11下限          | string   | SOC_B.BC_B_MIN                 |
| opBcCStd          | (B層)C12標準          | string   | SOC_B.BC_C_STD                 |
| opBcCMax          | (B層)C12上限          | string   | SOC_B.BC_C_MAX                 |
| opBcCMin          | (B層)C12下限          | string   | SOC_B.BC_C_MIN                 |
| opBcDStd          | (B層)C13標準          | string   | SOC_B.BC_D_STD                 |
| opBcDMax          | (B層)C13上限          | string   | SOC_B.BC_D_MAX                 |
| opBcDMin          | (B層)C13下限          | string   | SOC_B.BC_D_MIN                 |
| opBcEStd          | (B層)C14標準          | string   | SOC_B.BC_E_STD                 |
| opBcEMax          | (B層)C14上限          | string   | SOC_B.BC_E_MAX                 |
| opBcEMin          | (B層)C14下限          | string   | SOC_B.BC_E_MIN                 |
| opBcNStd          | (B層)脖溫標準         | string   | SOC_B.BC_N_STD                 |
| opBcNMax          | (B層)脖溫上限         | string   | SOC_B.BC_N_MAX                 |
| opBcNMin          | (B層)脖溫下限         | string   | SOC_B.BC_N_MIN                 |
| opBScrewStd       | (B層)螺桿標準         | string   | SOC_B.B_SCREW_STD              |
| opBScrewMax       | (B層)螺桿上限         | string   | SOC_B.B_SCREW_MAX              |
| opBScrewMin       | (B層)螺桿下限         | string   | SOC_B.B_SCREW_MIN              |
| opBCurrStd        | (B層)電流標準         | string   | SOC_B.B_CURR_STD               |
| opBCurrMax        | (B層)電流上限         | string   | SOC_B.B_CURR_MAX               |
| opBCurrMin        | (B層)電流下限         | string   | SOC_B.B_CURR_MIN               |
| opBGpStd          | (B層)GP標準           | string   | SOC_B.B_GP_STD                 |
| opBGpMax          | (B層)GP上限           | string   | SOC_B.B_GP_MAX                 |
| opBGpMin          | (B層)GP下限           | string   | SOC_B.B_GP_MIN                 |
| opD1Std           | (模頭溫度)D1標準      | string   | SOC_B.D_1_STD                  |
| opD1Max           | (模頭溫度)D1上限      | string   | SOC_B.D_1_MAX                  |
| opD1Min           | (模頭溫度)D1下限      | string   | SOC_B.D_1_MIN                  |
| opD2Std           | (模頭溫度)D2標準      | string   | SOC_B.D_2_STD                  |
| opD2Max           | (模頭溫度)D2上限      | string   | SOC_B.D_2_MAX                  |
| opD2Min           | (模頭溫度)D2下限      | string   | SOC_B.D_2_MIN                  |
| opD3Std           | (模頭溫度)D3標準      | string   | SOC_B.D_3_STD                  |
| opD3Max           | (模頭溫度)D3上限      | string   | SOC_B.D_3_MAX                  |
| opD3Min           | (模頭溫度)D3下限      | string   | SOC_B.D_3_MIN                  |
| opD4Std           | (模頭溫度)D4標準      | string   | SOC_B.D_4_STD                  |
| opD4Max           | (模頭溫度)D4上限      | string   | SOC_B.D_4_MAX                  |
| opD4Min           | (模頭溫度)D4下限      | string   | SOC_B.D_4_MIN                  |
| opD5Std           | (模頭溫度)D5標準      | string   | SOC_B.D_5_STD                  |
| opD5Max           | (模頭溫度)D5上限      | string   | SOC_B.D_5_MAX                  |
| opD5Min           | (模頭溫度)D5下限      | string   | SOC_B.D_5_MIN                  |
| opD6Std           | (模頭溫度)D6標準      | string   | SOC_B.D_6_STD                  |
| opD6Max           | (模頭溫度)D6上限      | string   | SOC_B.D_6_MAX                  |
| opD6Min           | (模頭溫度)D6下限      | string   | SOC_B.D_6_MIN                  |
| opD7Std           | (模頭溫度)D7標準      | string   | SOC_B.D_7_STD                  |
| opD7Max           | (模頭溫度)D7上限      | string   | SOC_B.D_7_MAX                  |
| opD7Min           | (模頭溫度)D7下限      | string   | SOC_B.D_7_MIN                  |
| opR1Std           | (滾筒溫度)(上)標準    | string   | SOC_B.R_1_STD                  |
| opR1Max           | (滾筒溫度)(上)上限    | string   | SOC_B.R_1_MAX                  |
| opR1Min           | (滾筒溫度)(上)下限    | string   | SOC_B.R_1_MIN                  |
| opR2Std           | (滾筒溫度)(中)標準    | string   | SOC_B.R_2_STD                  |
| opR2Max           | (滾筒溫度)(中)上限    | string   | SOC_B.R_2_MAX                  |
| opR2Min           | (滾筒溫度)(中)下限    | string   | SOC_B.R_2_MIN                  |
| opR3Std           | (滾筒溫度)(下)標準    | string   | SOC_B.R_3_STD                  |
| opR3Max           | (滾筒溫度)(下)上限    | string   | SOC_B.R_3_MAX                  |
| opR3Min           | (滾筒溫度)(下)下限    | string   | SOC_B.R_3_MIN                  |
| opRSpeedStd       | (滾筒速度)標準        | string   | SOC_B.R_SPEED_STD              |
| opRSpeedMax       | (滾筒速度)上限        | string   | SOC_B.R_SPEED_MAX              |
| opRSpeedMin       | (滾筒速度)下限        | string   | SOC_B.R_SPEED_MIN              |
| opRGapL           | 滾筒間隙(左)          | string   | SOC_B.R_GAP_L                  |
| opRGapR           | 滾筒間隙(右)          | string   | SOC_B.R_GAP_R                  |
| opVacuumIndex     | 真空指數              | string   | SOC_B.VACUUM_INDEX             |
| opLayer1          | (層間比)(上)標準      | string   | SOC_B.LAYER_1                  |
| opLayer2          | (層間比)(中)標準      | string   | SOC_B.LAYER_2                  |
| opLayer3          | (層間比)(下)標準      | string   | SOC_B.LAYER_3                  |
| opModelGap        | 模唇間隙              | string   | SOC_B.MODEL_GAP                |
| opModelWidth      | 模唇出料寬度          | string   | SOC_B.MODEL_WIDTH              |
| opMfgDesc         | 生產方式說明          | string   | SOC_B.MFG_DESC                 |
| opSocDesc         | SOC識別文字           | string   | SOC_B.SOC_DESC                 |
| opRemark          | 備註                  | string   | SOC_B.REMARK                   |
| opScreenFreq      | 網目更換              | string   | SOC_B.SCREEN_FREQ              |
| opRecordFreq      | 記錄頻率              | string   | SOC_B.RECORD_FREQ              |
| opLayer1Max       | (層間比)(上)上限      | string   | SOC_B.LAYER_1_MAX              |
| opLayer1Min       | (層間比)(上)下限      | string   | SOC_B.LAYER_1_MIN              |
| opLayer2Max       | (層間比)(中)上限      | string   | SOC_B.LAYER_2_MAX              |
| opLayer2Min       | (層間比)(中)下限      | string   | SOC_B.LAYER_2_MIN              |
| opLayer3Max       | (層間比)(下)上限      | string   | SOC_B.LAYER_3_MAX              |
| opLayer3Min       | (層間比)(下)下限      | string   | SOC_B.LAYER_3_MIN              |
| opVacuumIndexAStd | (真空指數)A機標準     | string   | SOC_B.VACUUM_INDEX_A_STD       |
| opVacuumIndexAMax | (真空指數)A機上限     | string   | SOC_B.VACUUM_INDEX_A_MAX       |
| opVacuumIndexAMin | (真空指數)A機下限     | string   | SOC_B.VACUUM_INDEX_A_MIN       |
| opVacuumIndexBStd | (真空指數)B機標準     | string   | SOC_B.VACUUM_INDEX_B_STD       |
| opVacuumIndexBMax | (真空指數)B機上限     | string   | SOC_B.VACUUM_INDEX_B_MAX       |
| opVacuumIndexBMin | (真空指數)B機下限     | string   | SOC_B.VACUUM_INDEX_B_MIN       |
| opScreenLayerA    | (網目層別)A層         | string   | SOC_B.SCREEN_LAYER_A           |
| opScreenLayerB    | (網目層別)B層         | string   | SOC_B.SCREEN_LAYER_B           |
| opScBpAStd        | (網前背壓)A機標準     | string   | SOC_B.SC_BP_A_STD              |
| opScBpAMax        | (網前背壓)A機上限     | string   | SOC_B.SC_BP_A_MAX              |
| opScBpAMin        | (網前背壓)A機下限     | string   | SOC_B.SC_BP_A_MIN              |
| opScBpBStd        | (網前背壓)B機標準     | string   | SOC_B.SC_BP_B_STD              |
| opScBpBMax        | (網前背壓)B機上限     | string   | SOC_B.SC_BP_B_MAX              |
| opScBpBMin        | (網前背壓)B機下限     | string   | SOC_B.SC_BP_B_MIN              |
| opMtrDryTempA     | (原料乾燥條件)A層溫度 | string   | SOC_B.MTR_DRY_TEMP_A           |
| opMtrDryTempB     | (原料乾燥條件)B層溫度 | string   | SOC_B.MTR_DRY_TEMP_B           |
| opMtrDryTimeA     | (原料乾燥條件)A層時間 | string   | SOC_B.MTR_DRY_TIME_A           |
| opMtrDryTimeB     | (原料乾燥條件)B層時間 | string   | SOC_B.MTR_DRY_TIME_B           |
| opMtrMixTimeA     | (混合時間)A層         | string   | SOC_B.MTR_MIX_TIME_A           |
| opMtrMixTimeB     | (混合時間)B層         | string   | SOC_B.MTR_MIX_TIME_B           |
| opRU              | (滾輪種類)上          | string   | SOC_B.R_U                      |
| opRM              | (滾輪種類)中          | string   | SOC_B.R_M                      |
| opRD              | (滾輪種類)下          | string   | SOC_B.R_D                      |
| opRTenStd         | (收捲張力)張力標準    | string   | SOC_B.R_TEN_STD                |
| opRTenMax         | (收捲張力)張力上限    | string   | SOC_B.R_TEN_MAX                |
| opRTenMin         | (收捲張力)張力下限    | string   | SOC_B.R_TEN_MIN                |
| opRTenSlopeStd    | (收捲張力)斜率標準    | string   | SOC_B.R_TEN_SLOPE_STD          |
| opRTenSlopeMax    | (收捲張力)斜率上限    | string   | SOC_B.R_TEN_SLOPE_MAX          |
| opRTenSlopeMin    | (收捲張力)斜率下限    | string   | SOC_B.R_TEN_SLOPE_MIN          |
| opRTenSpeedStd    | (收捲張力)速度標準    | string   | SOC_B.R_TEN_SPEED_STD          |
| opRTenSpeedMax    | (收捲張力)速度上限    | string   | SOC_B.R_TEN_SPEED_MAX          |
| opRTenSpeedMin    | (收捲張力)速度下限    | string   | SOC_B.R_TEN_SPEED_MIN          |
| opFeedpAStd       | (進料壓力設定)A機標準 | string   | SOC_B.FEEDP_A_STD              |
| opFeedpAMax       | (進料壓力設定)A機上限 | string   | SOC_B.FEEDP_A_MAX              |
| opFeedpAMin       | (進料壓力設定)A機下限 | string   | SOC_B.FEEDP_A_MIN              |
| opFeedpBStd       | (進料壓力設定)B機標準 | string   | SOC_B.FEEDP_B_STD              |
| opFeedpBMax       | (進料壓力設定)B機上限 | string   | SOC_B.FEEDP_B_MAX              |
| opFeedpBMin       | (進料壓力設定)B機下限 | string   | SOC_B.FEEDP_B_MIN              |
| opCdt             | 建立日期              | string   | SOC_B.CDT yyyy/mm/dd hh:mm:ss  |
| opCreateBy        | 建立人員              | string   | SOC_B.CREATE_BY                |
| opUdt             | 更新日期              | string   | SOC_B.UDT  yyyy/mm/dd hh:mm:ss |
| opUpdateBy        | 更新人員              | string   | SOC_B.UPDATE_BY                |
| opPubDate         | 發行日期              | string   | SOC_B.PUB_DATE  yyyy/mm/dd     |
| opPublicBy        | 發行人代碼            | string   | SOC_B.PUBLIC_BY                |
| opPublicName      | 發行人名稱            | string   | SYS_USER.USER_NAME             |
| opOrganizationId  | 工廠別                | string   | EQUIP_H.ORGANIZATION_ID        |

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":{
      "opSocBUid":"6A135E88-190D-4C61-861A-969EFFA37DD6",
      "opEquipId":"42",
      "opEquipName":"片材#３",
      "opItemNo":"DN01XXXXXXXX025001",
      "opItemDesc":"",
      "opSocMVer":"1",
      "opStatus":"Y",
      "opAc1Std":null,
      "opAc1Max":null,
      "opAc1Min":null,
      "opAc2Std":null,
      "opAc2Max":null,
      "opAc2Min":null,
      "opAc3Std":null,
      "opAc3Max":null,
      "opAc3Min":null,
      "opAc4Std":null,
      "opAc4Max":null,
      "opAc4Min":null,
      "opAc5Std":null,
      "opAc5Max":null,
      "opAc5Min":null,
      "opAc6Std":null,
      "opAc6Max":null,
      "opAc6Min":null,
      "opAc7Std":null,
      "opAc7Max":null,
      "opAc7Min":null,
      "opAc8Std":null,
      "opAc8Max":null,
      "opAc8Min":null,
      "opAc9Std":null,
      "opAc9Max":null,
      "opAc9Min":null,
      "opAcAStd":null,
      "opAcAMax":null,
      "opAcAMin":null,
      "opAScrewStd":null,
      "opAScrewMax":null,
      "opAScrewMin":null,
      "opACurrStd":null,
      "opACurrMax":null,
      "opACurrMin":null,
      "opAGpStd":null,
      "opAGpMax":null,
      "opAGpMin":null,
      "opBc1Std":"200",
      "opBc1Max":"210",
      "opBc1Min":"170",
      "opBc2Std":"200",
      "opBc2Max":"210",
      "opBc2Min":"190",
      "opBc3Std":"200",
      "opBc3Max":"210",
      "opBc3Min":"190",
      "opBc4Std":"200",
      "opBc4Max":"210",
      "opBc4Min":"190",
      "opBc5Std":"200",
      "opBc5Max":"210",
      "opBc5Min":"190",
      "opBc6Std":"200",
      "opBc6Max":"210",
      "opBc6Min":"190",
      "opBc7Std":"200",
      "opBc7Max":"210",
      "opBc7Min":"190",
      "opBc8Std":"200",
      "opBc8Max":"210",
      "opBc8Min":"190",
      "opBc9Std":"200",
      "opBc9Max":"210",
      "opBc9Min":"190",
      "opBcAStd":"200",
      "opBcAMax":"210",
      "opBcAMin":"190",
      "opBcBStd":"190",
      "opBcBMax":"200",
      "opBcBMin":"180",
      "opBcCStd":"190",
      "opBcCMax":"200",
      "opBcCMin":"180",
      "opBcDStd":"190",
      "opBcDMax":"200",
      "opBcDMin":"180",
      "opBcEStd":"190",
      "opBcEMax":"200",
      "opBcEMin":"180",
      "opBcNStd":"195",
      "opBcNMax":"205",
      "opBcNMin":"185",
      "opBScrewStd":"50",
      "opBScrewMax":"70",
      "opBScrewMin":"40",
      "opBCurrStd":"260",
      "opBCurrMax":"280",
      "opBCurrMin":"250",
      "opBGpStd":"25",
      "opBGpMax":"40",
      "opBGpMin":"10",
      "opD1Std":"190",
      "opD1Max":"200",
      "opD1Min":"180",
      "opD2Std":"190",
      "opD2Max":"200",
      "opD2Min":"180",
      "opD3Std":"190",
      "opD3Max":"200",
      "opD3Min":"180",
      "opD4Std":"190",
      "opD4Max":"200",
      "opD4Min":"180",
      "opD5Std":"190",
      "opD5Max":"200",
      "opD5Min":"180",
      "opD6Std":"190",
      "opD6Max":"200",
      "opD6Min":"180",
      "opD7Std":"190",
      "opD7Max":"200",
      "opD7Min":"180",
      "opR1Std":"60",
      "opR1Max":"75",
      "opR1Min":"50",
      "opR2Std":"70",
      "opR2Max":"80",
      "opR2Min":"60",
      "opR3Std":"60",
      "opR3Max":"75",
      "opR3Min":"50",
      "opRSpeedStd":"27.00",
      "opRSpeedMax":null,
      "opRSpeedMin":null,
      "opRGapL":"0.25",
      "opRGapR":"0.25",
      "opVacuumIndex":"-60",
      "opLayer1":"0",
      "opLayer2":"100",
      "opLayer2":"0",
      "opModelGap":"0.34",
      "opModelWidth":"575.00",
      "opMfgDesc":null,
      "opSocDesc":null,
      "opRemark":null,
      "opScreenFreq":null,
      "opRecordFreq":null,
      "opLayer1Max":null,
      "opLayer1Min":null,
      "opLayer2Max":null,
      "opLayer2Min":null,
      "opLayer3Max":null,
      "opLayer3Min":null,
      "opVacuumIndexAStd":"60",
      "opVacuumIndexAMax":null,
      "opVacuumIndexAMin":null,
      "opVacuumIndexBStd":"60",
      "opVacuumIndexBMax":null,
      "opVacuumIndexBMin":null,
      "opScreenLayerA":null,
      "opScreenLayerB":"50/80/50/50",
      "opScBpAStd":null,
      "opScBpAMax":null,
      "opScBpAMin":null,
      "opScBpBStd":"100",
      "opScBpBMax":"100",
      "opScBpBMin":null,
      "opMtrDryTempA":null,
      "opMtrDryTempB":null,
      "opMtrDryTimeA":null,
      "opMtrDryTimeB":null,
      "opMtrMixTimeA":null,
      "opMtrMixTimeB":"30",
      "opRU":"鏡面輪",
      "opRM":"鏡面輪",
      "opRD":"鏡面輪",
      "opRTenStd":"10.0",
      "opRTenMax":"30.0",
      "opRTenMin":"5.0",
      "opRTenSlopeStd":null,
      "opRTenSlopeMax":null,
      "opRTenSlopeMin":null,
      "opRTenSpeedStd":"80",
      "opRTenSpeedMax":"100",
      "opRTenSpeedMin":"50",
      "opFeedpAStd":null,
      "opFeedpAMax":null,
      "opFeedpAMin":null,
      "opFeedpBStd":"30",
      "opFeedpBMax":"50",
      "opFeedpBMin":"20",
      "opCdt":"2019-10-28 09:41:09",
      "opCreateBy":"A000117",
      "opUdt":"2019-10-28 09:41:09",
      "opUpdateBy":"A000117",
      "opPubDate":"2019-10-28 09:42:48",
      "opPublicBy":"A000117",
      "opPublicName":"陳嘉峰",
      "opOrganizationId":"5000"
    }
  }
}
```
