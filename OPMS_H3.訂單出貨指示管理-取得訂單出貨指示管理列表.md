# 業務-取得訂單出貨指示管理列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得訂單出貨指示管理列表

## 修改歷程

| 修改時間 | 內容                            | 修改者 |
|----------|-------------------------------|--------|
| 20230628 | 新增規格                        | Tim    |
| 20230714 | 新增分頁                        | Tim    |
| 20230816 | 調整 TOP 1000(使用OFFSET FETCH) | Nick   |
| 20230921 | 調整預設排序欄位                | Nick   |
| 20231003 | 是否有出貨指示需表頭+表身檢查   | Nick   |

## 來源URL及資料格式

| 項目    | 說明               |
| ------ | ----------------- |
| URL    | /sis/get_sis_so_manage_list |
| method | post              |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。  
| 欄位| 名稱| 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明|
| --                   | --           | --     |:-:|               - |
| opOrganizationId     | 工廠別        |string  |O| 預設 null ， null:不指定 / 5000:奇菱 / 6000:菱翔  |
| opSoNo               | 銷售單號         |string  |O|   |
| opCustName           | 客戶名稱         |string  |O|   |
| opPage               | 第幾頁   | integer  |  O   | 預設`1`，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推 |
| opPageSize           | 分頁筆數 | integer  |  O   | 預設 `10`                                                               |
| opOrder              | 排序方式 | string   |  O   | 預設`DESC`，"ASC":升冪  /"DESC" :降冪                                    |

#### Request 範例

```json
{
  "opOrganizationId":"5000",
  "opSoNo":"12345678",
  "opCustName":"TEST",
  "opPage":1
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 依據前端參數，查詢訂單出貨指示管理列表資料並判斷銷售單號{opSoNo}與客戶名稱{opCustName}是否有值 參考 SQL-1
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
SELECT * FROM(

SELECT A.SO_NO, A.ORGANIZATION_ID, A.CUST_NO, A.CUST_NAME, A.CUST_PO_NO, A.CHANNEL, A.ERP_FLAG, A.SIS_FLAG,REPLACE(CONVERT(VARCHAR(19),A.CDT,120),'-','/') CDT, A.CREATE_BY, REPLACE(CONVERT(VARCHAR(19),A.UDT,120),'-','/') UDT, ISNULL(U.USER_NAME,A.UPDATE_BY) UPDATE_BY
,(SELECT 
    CASE 
        WHEN S_NUM = 1 AND S_AGG = 'Y' 
		THEN 'Y'
        ELSE 'N'
    END AS FLAG 
FROM (
    SELECT 
        COUNT(*) AS S_NUM,
        STRING_AGG(SIS_FLAG, '') AS S_AGG 
    FROM (
        SELECT SIS_FLAG FROM SO_H WHERE SO_NO=A.SO_NO
        UNION 
        SELECT SIS_FLAG FROM SO_D WHERE SO_NO=A.SO_NO
    ) AS S_CHECK
) AS COMBINED) CHECK_SIS_FLAG 
FROM SO_H A LEFT JOIN SYS_USER U ON A.UPDATE_BY=U.USER_ID WHERE A.ORGANIZATION_ID={opOrganizationId} 
AND A.SO_NO LIKE {%opSoNo%} AND (A.CUST_NO LIKE {%opCustName%} OR A.CUST_NAME LIKE {%opCustName%})

       ORDER BY A.UDT {opOrder}
       OFFSET 0 ROWS FETCH NEXT 1000 ROWS ONLY
    ) AS TOP1000T
ORDER BY (SELECT NULL) OFFSET { offset } ROWS FETCH NEXT { opageSize } ROWS ONLY

```

# Response 欄位
| 欄位                | 名稱              | 資料型別     | 資料儲存 & 說明               |
| ------------------ | ---------------- | ----------- | --------------------------- |
|pageInfo            | 分頁資訊 		 | object     |                |
|content             | 內容              | array       |                             |

#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別  | 來源資料 & 說明 |
|------------|--------------|----------|-----------------|
| pageNumber | 目前所在分頁 | int       |                 |
| pageSize   | 分頁筆數大小 | int       |                 |
| pages      | 總分頁數     | int       |                 |
| total      | 總資料筆數   | int       |                 |

#### 【content】array
| 欄位             | 名稱           | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明         |
| --------------- | -------------- | ------- | --------------------| --------------------- |
|opSoNo           |銷售單號         | string  |O                     | A.SO_NO(SQL-1)        |
|opCustNo         |客戶代號         | string  |O                     | A.CUST_NO(SQL-1)      |
|opCustName       |客戶名稱         | string  |O                     | A.CUST_NAME(SQL-1)    |
|opCustPoNo       |客戶PO單號       | string  |O                     | A.CUST_PO_NO(SQL-1)   |
|opChannel        |渠道            | string  |O                     | A.CHANNEL(SQL-1)      |
|opErpFlag        |是否拋轉ERP      | string  |O                     | A.ERP_FLAG(SQL-1)     |
|opSisFlag        |是否出貨指示      | string  |O                     | CHECK_SIS_FLAG(SQL-1)    |
|opUdt            |更新日期         | string  |O                     | UDT(SQL-1)           |
|opUpdateBy       |更新人員         | string  |O                     | UPDATE_BY(SQL-1)     |

#### Response 範例

```json
{
    "msgCode": null,
    "result": {
	    "pageInfo": {
        "pageNumber": 1, 
        "pageSize": 10,
        "pages": 1,      
        "total": 2     
        },
        "content":[
            {                
                "opSoNo":"20515102",
                "opCustNo":"1515612",
                "opCustName":"王哥",
                "opCustPoNo":"1234",
                "opChannel":null,
                "opErpFlag":"Y",
                "opSisFlag":"N",
                "opUdt":"王先生",
                "opUpdateBy":"2022-11-01 11:00:01"
            },
            {                
                "opSoNo":"20515103",
                "opCustNo":"1515612",
                "opCustName":"王哥",
                "opCustPoNo":"1234",
                "opChannel":null,
                "opErpFlag":"N",
                "opSisFlag":"Y",
                "opUdt":"王先生",
                "opUpdateBy":"2022-11-01 11:00:01"
            }
        ]
    }
}
```

