# 角色管理-單一角色刪除

###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  
參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)

針對單一單一角色刪除


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230405 | 新增規格 |    |

## 來源URL及方法

| 項目   | 說明             |
| ------ | ---------------- |
| URL    | /role/delete |
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

## Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位         | 名稱         | 資料型別 |必填| 資料儲存 & 說明          |
| ------------ | ------------ | -------- |---| ------------------------ |
|  opRoleId    | 角色ID      |  integer  |    M      |  |

## Request 範例

```json
{
  "opRoleId":931
}
```

## Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* {DATAKEY} = {opRoleId}
* SQL-1 ~ SQL-3 請使用同一Transaction執行
* 執行  SQL-1 確認該角色存在，找不到 return 400,"NOTFOUND"
* 執行  SQL-2 刪除該角色
* 如果SQL-2 執行成功， 則 {logContent}內容為 "刪除"
* 執行 SQL-3 紀錄LOG
  有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

## Request 後端邏輯說明

SQL-1:
```sql
SELECT * FROM SYS_ROLE_V3 WHERE ROLE_ID={opRoleId}
```
SQL-2:
```sql
UPDATE SYS_ROLE_V3 SET VISIBLE=0 WHERE ROLE_ID={opRoleId},UDT=SYSDATETIME(),UPDATE_BY={登入者使用者ID} 
```
SQL-3:
```sql
INSERT INTO  SYS_MODI_LOG (CATEGORY,DATAKEY,USERID,USER_NAME,LOG) VALUES ('ROLE',{DATAKEY},{登入者使用者ID},{登入者使用者名稱}, {logContent})
```

## Response 欄位

| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
|----------|--------|----------|-----------------|
| opUserId | 使用者ID | string   |                 |


## Response 範例

```json
{
  "msgCode": null, 
  "result":{
    "content": {
       "opRoleId":931
    }
  }
}
```

