# 報表-色粉標籤資料電子檔-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容                      | 修改者 |
|----------|-------------------------|--------|
| 20230808 | 新增規格                  | 黃東俞 |
| 20231027 | 依使用日期新到舊排序      | Ellen  |
| 20231030 | 怡欣想把排序改回舊的      | Ellen  |
| 20231130 | 日期格式調整為 yyyy/mm/dd | Nick   |


## 來源URL及資料格式

| 項目   | 說明                     |
|--------|-------------------------|
| URL    | /report/pgm_label_list  |
| method | post                    |


## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)



# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱    |  資料型別 | 必填 | 資料儲存 & 說明  |
|--------------|---------|----------|:----:|-----------------|
| opBDate      | 開始日期 | String   |  O   | yyyy/mm/dd      |
| opEDate      | 結束日期 | String   |  O   | yyyy/mm/dd      |

```json
{
  "opBDate": "2019/01/01",
  "opEDate": "2019/01/31"
}
```


# Request 後端流程說明

* 取{ 登入者工廠別 } SYS_USER.ORGANIZATION_ID 欄位
* 查詢工單主檔 WO_H，配色配方主檔 BOM_MTM，料號主檔 ITEM_H，參考SQL-1
  * WO_H.BOM_UID = BOM_MTM.BOM_UID
  * BOM_MTM.ITEM_NO = ITEM_H.ITEM_NO
  * 查詢條件
    * WO_H.ORGANIZATION_ID= { 登入者工廠別 }
    * WO_H.WO_DATE >= { opBDate }，沒傳入就不限制
    * WO_H.WO_DATE <= { opEDate }，沒傳入就不限制
  * 排序 WO_H.LOT_NO ASC
* 歷遍以上每筆資料，分別進行以下處理
  * 查詢配色指示作業原料檔 woMtrList，參考SQL-2
    * 條件：WO_MTR.WO_MTR_PART_NO NOT LIKE '92%' AND R.WO_UID = { WO_H.WO_UID }
    * 排序：WO_MTR.QUANTITY_PER 倒序, WO_MTR_PART_NO 順序
    * 取前三筆資料填入原料號和比例欄位
      * opMtrPartNo1~3(原料第一料號 ~ 第三料號)：WO_MTR.WO_MTR_PART_NO
      * opMtrPercentage1~3(比率一 ~ 三)：WO_MTR.QUANTITY_PER乘以100轉為百分比，若WO_MTR.UNIT='G'時再除以1000轉為KG，並轉字串取到小數點後兩位
  * 查詢色料資料 pgmList，參考SQL-3
  * 當登入者工廠別 != '5000'時需展開色粉中間料加入pgmList
    * 歷遍pgmList，當BOM_UID不為null時，帶入SQL-4和SQL-5查詢單位和配色配方色粉檔。
      * 曆遍SQL-5查詢結果bomPgmList，每筆資料都新增一筆資料到pmgList
        * woPgmPartNo = BOM_PGM.PGM_PART_NO
        * 單位用量 pmgList.quantityPer = (SQL-3 QUANTITY_PER) * BOM_PGM.QUANTITY_PER
        * 若中間料用量單位以G計，但其BOM單位(SQL-4)以KG表示，則再除以1000
        * 若中間料用量以KG計，但其BOM單位(SQL-4)以G表示，則再乘以1000
        * 轉為字串並取到小數點後5位
        * unit = BOM_PGM.UNIT(SQL-5)
        * sitePick = BOM_PGM.SITE_PICK(SQL-5)
        * selfPick = BOM_PGM.SELF_PICK(SQL-5)
        * pgmBatchQty(每Batch色粉重) = quantityPer * WO_H.BATCH_QTY 轉為字串並取到小數點後5位
        * pgmRemainQty(每Batch色粉尾數) = quantityPer * WO_H.REMAIN_QTY * WO_H.REMAIN_TIMES 轉為字串並取到小數點後5位
    * 展開所有色粉中間料後，再將已展開的原資料自pgmList移除(BOM_UID不為null的資料)，只留下展開後的內容
  * 使用 [OPMS_共用_色料計算公式](./OPMS_共用_色料計算公式.md) calculatePgm
    * 取參數 n4 = WO_H.CUS_N_QUANTITY_PER(SQL-1)
    * 帶入參數進行計算 pgmList, woMtrList, batchQty(WO_H.BATCH_QTY), batchTimes(WO_H.BATCH_TIMES), remainQty(WO_H.REMAIN_QTY), remainTimes(WO_H.REMAIN_TIMES), n4(如果無值帶入null), true
    * 若計算結果不為null時，取得計算結果寫入回傳欄位
      * opN4BatchQty(N_4數量) = n4BatchQty(N4 Batch總用量) 轉為整數字串
      * opN4RemainQty(N_4尾數) = n4RemainQty(N4尾數總量) 轉為整數字串
      * opPgmBatchQty(每Batch色粉重) = pgmTotalBatchQty(Batch總用量) 轉字串取到小數後兩位
      * opPgmRemainQty(每Batch色粉尾數) = pgmTotalRemainQty(尾數總量) 轉字串取到小數後兩位
      * opPgmWeight(色粉重) = pgmTotalQuantityPer(色粉總重) * WO_H.RAW_QTY / 1000 轉字串取到小數後兩位
      * 當登入者工廠別 = '5000'時，色粉包數計算方式不同，每筆資料要另外進行以下處理
        * 重寫序號 opWoSeq 第一筆從1開始。
        * 若色號 I.PATTERN_NO 為 'N' 開頭，且 B.ITEM_NO 為 '2409'開頭時
          * 重寫LotNo opLotNo = WO_H.LOT_NO + ' [' + WO_H.WO_SEQ + ']'
        * 色號 I.PATTERN_NO 不為 'N' 開頭，或 B.ITEM_NO 不為 '2409'開頭時，一筆資料須拆成多筆
          * 取OPMS_共用_色料計算公式結果：
            * Batch色粉包數labelCount、尾數總量pgmTotalQuantityPer、尾數色粉包數labelRemainCount
            * 將一筆資料拆成 labelCount 筆
              * n = 拆出的筆數序號，從1開始，例如一筆拆成三筆，n = 1~3
              * 重寫 opPatternNo(色號) = I.PATTERN_NO + ' [' + n + '/' + labelCount + ']'，例如：'A22306C6   [2/3]'
              * 重寫 opLotNo = WO_H.LOT_NO + ' [' + WO_H.WO_SEQ + ']'
              * opRemainQty(尾數量),opRemainTimes(尾數量次數), pgmRemainQty(每Batch色粉尾數) 固定給 0
            * 若 WO_H.REMAIN_QTY > 0 且 尾數總量pgmTotalQuantityPer > 0 時，再追加 labelRemainCount 筆資料
              * n = 追加的筆數序號，從1開始
              * 重寫 opPatternNo(色號) = I.PATTERN_NO + ' [尾' + n + '/' + labelRemainCount + ']'，例如：'J16B04C1   [尾1/1]'
              * 重寫 opBatchQty(每Batch用量)，opN4BatchQty(N4_數量)，opPgmBatchQty(每Batch色粉重) 固定給 '0'
              * opBatchTimes(Batch用量次數) 固定給 '-1'
        * 其餘未提到的欄位則照搬原本資料
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

SQL-1:
```sql
-- ReportQueryDao.getPgmLabelReport
SELECT H.WO_UID, H.WO_SEQ, H.LOT_NO, H.WO_DATE, H.WO_NO, B.ITEM_NO, B.CMDI_NO, I.PATTERN_NO,
H.BATCH_QTY, H.BATCH_TIMES, H.REMAIN_QTY, H.REMAIN_TIMES, H.RAW_QTY, H.CUS_N_QUANTITY_PER, H.STD_N_QUANTITY_PER
FROM WO_H H
INNER JOIN BOM_MTM B ON H.BOM_UID = B.BOM_UID
INNER JOIN ITEM_H I ON B.ITEM_NO = I.ITEM_NO
WHERE H.ORGANIZATION_ID= { 登入者工廠別 }
AND H.WO_DATE >= { opBDate }
AND H.WO_DATE <= { opEDate }
ORDER BY H.LOT_NO
```

SQL-2:
```sql
-- WoQueryDao.getWoMtrList
SELECT R.WO_MTR_PART_NO
FROM WO_MTR R
WHERE R.WO_MTR_PART_NO NOT LIKE '92%' AND R.WO_UID = { WO_H.WO_UID }
ORDER BY R.QUANTITY_PER DESC, R.WO_MTR_PART_NO
```

SQL-3
```sql
-- WoQueryDao.getWoPgmListExpand
SELECT A.WO_PGM_PART_NO, A.QUANTITY_PER, A.UNIT, A.SITE_PICK, A.SELF_PICK, A.PGM_BATCH_QTY, A.PGM_REMAIN_QTY, A.MODIFY_QTY, A.MODIFY_UNIT_QTY, A.FEEDER_SEQ, B.BOM_UID AS PGM_BOM_UID
FROM (
  SELECT H.ORGANIZATION_ID, W.WO_PGM_PART_NO, W.QUANTITY_PER, W.UNIT,W.SITE_PICK, W.SELF_PICK, W.PGM_BATCH_QTY, W.PGM_REMAIN_QTY, W.MODIFY_QTY, W.MODIFY_UNIT_QTY, W.FEEDER_SEQ
  FROM WO_PGM W
  INNER JOIN WO_H H ON W.WO_UID = H.WO_UID
  WHERE W.WO_UID = { WO_H.WO_UID } ) A
LEFT JOIN (
  SELECT ORGANIZATION_ID, ITEM_NO,BOM_UID
  FROM BOM_MTM
  WHERE BOM_NO IS NULL AND ACTIVE_FLAG = 'Y') B
ON A.ORGANIZATION_ID = B.ORGANIZATION_ID AND A.WO_PGM_PART_NO = B.ITEM_NO
ORDER BY WO_PGM_PART_NO
```

SQL-4
```sql
-- BomQueryDao.getBomMtmSingle
SELECT I.UNIT
FROM BOM_MTM B
INNER JOIN ITEM_H I ON B.ITEM_NO = I.ITEM_NO
WHERE B.BOM_UID = { PGM_BOM_UID(SQL-3) }
```
SQL-5
```sql
-- BomQueryDao.getBomPgmList
SELECT PGM_PART_NO, QUANTITY_PER, UNIT, SITE_PICK, SELF_PICK, FEEDER_SEQ
FROM BOM_PGM
WHERE BOM_UID = { PGM_BOM_UID(SQL-3) }
ORDER BY PGM_PART_NO
```

# Response 欄位
| 欄位            | 名稱         | 資料型別 | 資料儲存 & 說明                |
|-----------------|--------------|----------|------------------------------|
| opWoDate        | 日期         | string   | WO_H.WO_DATE 格式：yyyy/mm/dd   |
| opWoSeq         | 序號         | string   | WO_H.WO_SEQ                  |
| opLotNo         | Lot No       | string   | WO_H.LOT_NO                  |
| opPatternNo     | 色號         | string   | ITEM_H.PATTERN_NO            |
| opWoNo          | 工單號碼      | string   | WO_H.WO_NO                   |
| opCmdiNo        | 品稱代號      | string   | BOM_MTM.CMDI_NO              |
| opMtrPartNo1    | 原料第一料號  | string   |  WO_MTR.WO_MTR_PART_NO       |
| opBatchQty      | 每Batch用量   | string   | WO_H.BATCH_QTY               |
| opBatchTimes    | Batch用量次數 | string   | WO_H.BATCH_TIMES             |
| opRemainQty     | 尾數量        | string   | WO_H.REMAIN_QTY              |
| opRemainTimes   | 尾數量次數    | string   | WO_H.REMAIN_TIMES            |
| opMtrPercentage1| 比率一        | string   |                              |
| opMtrPartNo2    | 原料第二料號  | string   |  WO_MTR.WO_MTR_PART_NO       |
| opMtrPercentage2| 比率二        | string   |                             |
| opMtrPartNo3    | 原料第三料號  | string   |  WO_MTR.WO_MTR_PART_NO       |
| opMtrPercentage3| 比率三        | string   |                              |
| opN4BatchQty    | N_4數量      | string   |                              |
| opN4RemainQty   | N_4尾數      | string   |                              |
| opPgmBatchQty   | 每Batch色粉重 | string   |                              |
| opPgmRemainQty  | 每Batch色粉尾數| string   |                             |
| opPgmWeight     | 色粉重        | string   |                              |


#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content": [
      {
        "opWoDate": "20230102",
        "opWoSeq": "1",
        "opLotNo": "4  [1]",
        "opPatternNo": "H17934B6   [1/2]",
        "opWoNo": "51030229",
        "opCmdiNo": null,
        "opMtrPartNo1": "04-102-PA-765AXXXX",
        "opBatchQty": "800",
        "opBatchTimes": "15",
        "opRemainQty": "0",
        "opRemainTimes": "0",
        "opMtrPercentage1": "100.00",
        "opMtrPartNo2": null,
        "opMtrPercentage2": null,
        "opMtrPartNo3": null,
        "opMtrPercentage3": null,
        "opN4BatchQty": "480",
        "opN4RemainQty": "0",
        "opPgmBatchQty": "5268.40",
        "opPgmRemainQty": "0",
        "opPgmWeight": "79.03"
      },
      {
        "opWoDate": "20230102",
        "opWoSeq": "2",
        "opLotNo": "4  [1]",
        "opPatternNo": "H17934B6   [2/2]",
        "opWoNo": "51030229",
        "opCmdiNo": null,
        "opMtrPartNo1": "04-102-PA-765AXXXX",
        "opBatchQty": "800",
        "opBatchTimes": "15",
        "opRemainQty": "0",
        "opRemainTimes": "0",
        "opMtrPercentage1": "100.00",
        "opMtrPartNo2": null,
        "opMtrPercentage2": null,
        "opMtrPartNo3": null,
        "opMtrPercentage3": null,
        "opN4BatchQty": "480",
        "opN4RemainQty": "0",
        "opPgmBatchQty": "5268.40",
        "opPgmRemainQty": "0",
        "opPgmWeight": "79.03"
      },
      {
        "opWoDate": "20230102",
        "opWoSeq": "3",
        "opLotNo": "5  [2]",
        "opPatternNo": "B81115M1   [1/2]",
        "opWoNo": "51030228",
        "opCmdiNo": null,
        "opMtrPartNo1": "04-106-CM-205XXXXX",
        "opBatchQty": "1000",
        "opBatchTimes": "1",
        "opRemainQty": "0",
        "opRemainTimes": "0",
        "opMtrPercentage1": "100.00",
        "opMtrPartNo2": null,
        "opMtrPercentage2": null,
        "opMtrPartNo3": null,
        "opMtrPercentage3": null,
        "opN4BatchQty": "0",
        "opN4RemainQty": "0",
        "opPgmBatchQty": "2460.80",
        "opPgmRemainQty": "0",
        "opPgmWeight": "2.46"
      },
      {
        "opWoDate": "20230102",
        "opWoSeq": "3",
        "opLotNo": "5  [2]",
        "opPatternNo": "B81115M1   [2/2]",
        "opWoNo": "51030228",
        "opCmdiNo": null,
        "opMtrPartNo1": "04-106-CM-205XXXXX",
        "opBatchQty": "1000",
        "opBatchTimes": "1",
        "opRemainQty": "0",
        "opRemainTimes": "0",
        "opMtrPercentage1": "100.00",
        "opMtrPartNo2": null,
        "opMtrPercentage2": null,
        "opMtrPartNo3": null,
        "opMtrPercentage3": null,
        "opN4BatchQty": "0",
        "opN4RemainQty": "0",
        "opPgmBatchQty": "2460.80",
        "opPgmRemainQty": "0",
        "opPgmWeight": "2.46"
      }
    ]
  }
}
```




