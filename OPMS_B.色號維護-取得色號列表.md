# 色號維護-取得色號列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得色號列表

## 修改歷程

| 修改時間 | 內容                            | 修改者 |
|----------|-------------------------------|--------|
| 20230424 | 新增規格                        | Nick   |
| 20230531 | 調整 ORDER 欄位及可調整排序方式 | Nick   |
| 20230815 | 調整 TOP 1000(使用OFFSET FETCH) | Nick   |

## 來源URL及資料格式

| 項目   | 說明          |
|--------|---------------|
| URL    | /pattern/list |
| method | post          |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。  
| 欄位        | 名稱             | 資料型別 | 必填 |                             來源資料 & 說明                             |
|-------------|----------------|----------|:----:|:---------------------------------------------------------------------:|
| opPatternNo | 色號             | string   |  O   |                               預設 `null`                               |
| opIsUpload  | 是否上傳色版圖片 | string   |  O   |                               預設 `null`                               |
| opCustName  | 客戶             | string   |  O   |                               預設 `null`                               |
| opColor     | 顏色             | string   |  O   |                               預設 `null`                               |
| opMTRType   | 料性             | string   |  O   |                               預設 `null`                               |
| opBDate     | 開始時間         | string   |  O   |                        預設 `null`    YYYY/MM/DD                        |
| opEDate     | 結束時間         | string   |  O   |                        預設 `null`   YYYY/MM/DD                         |
| opPage      | 第幾頁           | integer  |  O   | 預設`1`，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推 |
| opPageSize  | 分頁筆數         | integer  |  O   |                                預設 `10`                                |
| opOrder     | 排序方式         | string   |  O   |                  預設`DESC`，"ASC":升冪  /"DESC" :降冪                   |

#### Request 範例

```json
{
   "opIsUpload": "N",
  ,"opPage":1
  
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 使用前端給的條件，參考 SQL-1 自組【搜尋條件】(使用 AND 開頭，方便與原WHERE 條件串接)，如為 null 代表不需組條件
* {offset} 為起始列印 row ，第一頁為 0，第二頁後為 0 + (opPageSize*(opPage-1))
* 參考 SQL-1 查回結果 result
* Response 結果需含分頁資訊，無資料 pageInfo 裡面項目皆給 0，頁數與分頁筆數前端如有提供，依前端判斷給的資料計算相關數值及拆分頁面
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
SELECT *
FROM (
      SELECT * FROM(
      SELECT P.PATTERN_NO, 
             P.CUST_NAME, 
      	   P.COLOR,
      	   P.MTR_TYPE,
       　    CASE WHEN count(PU.FNAME) > 0 
               THEN 'Y'
               ELSE 'N' 
             END  AS IS_UPLOAD,
             REPLACE(CONVERT(VARCHAR(19),P.CDT,120),'-','/') CDT,
      	   REPLACE(CONVERT(VARCHAR(19),P.UDT,120),'-','/') UDT
          FROM PAT_H P  
          LEFT JOIN (SELECT PARA_VALUE COLOR,PARA_CODE COLOR_ID 
                     FROM SYS_PARA_D 
                     WHERE PARA_ID='COLOR'
                     ) C 
            ON P.COLOR=C.COLOR
        LEFT JOIN PAT_UPLOAD PU
          ON  P.PATTERN_NO = PU.PATTERN_NO
          GROUP BY P.PATTERN_NO,P.CUST_NAME,P.COLOR,P.MTR_TYPE,P.CDT,P.UDT
      ) ALL_DATA
      WHERE 1=1
          --搜尋條件
          AND PATTERN_NO LIKE %{opPatternNo}%
          AND IS_UPLOAD= {opIsUpload}
          AND COLOR= {opColor} 
          AND MTR_TYPE= {opMTRType}  
          AND CUST_NAME LIKE %{opCustName}%
          AND CDT>=CONVERT(DATETIME,{opBDate})  
          AND CDT<CONVERT(DATETIME,{opEDate})+1 
      ORDER BY CDT {opOrder}
      OFFSET 0 ROWS FETCH NEXT 1000 ROWS ONLY
    ) AS TOP1000T
ORDER BY (SELECT NULL) OFFSET { offset } ROWS FETCH NEXT { opageSize } ROWS ONLY
```


# Response 欄位
| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
|----------|--------|----------|-----------------|
| pageInfo | 分頁資訊 | object   |                 |
| content  | 附帶訊息 | array    |                 |
#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明 |
|------------|------------|----------|-----------------|
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |

#### 【content】array
| 欄位        | 名稱             | 資料型別 | 來源資料 & 說明        |
|-------------|----------------|----------|------------------------|
| opPatternNo | 色號             | string   | PATTERN_NO             |
| opCustName  | 客戶             | string   | CUST_NAME              |
| opIsUpload  | 是否上傳色版圖片 | string   | IS_UPLOAD  (Y/N)       |
| opCDT       | 建立時間         | string   | CDT (YYYY/MM/DD HH:MM) |

#### Response 範例

```json
{
    "msgCode": null,
    "result": {
        "pageInfo": {
        "pageNumber": 1, 
        "pageSize": 10,
        "pages": 1,      
        "total": 3     
        },
       "content": [
       {
         "opPatternNo": "12341B5",
         "opCustName": "NA",
         "opIsUpload": "N",
         "opCDT": "2016/10/12 10:42"
       }, 
       {
         "opPatternNo": "127X100CL1BX",
         "opCustName": "NA",
         "opIsUpload": "N",
         "opCDT": "2015/07/15 09:50"
       }, 
       {
         "opPatternNo": "13705C1",
         "opCustName": "NA",
         "opIsUpload": "N",
         "opCDT": "2015/07/21 10:41"
       }, 
       {
         "opPatternNo": "14606B5",
         "opCustName": "NA",
         "opIsUpload": "N",
         "opCDT": "2015/07/02 18:20"
       }, 
       {
         "opPatternNo": "149056",
         "opCustName": "PLASTRIBUTION",
         "opIsUpload": "N",
         "opCDT": "2015/02/05 14:16"
       }, 
       {
         "opPatternNo": "15855XFG",
         "opCustName": "丞翔(冠捷)",
         "opIsUpload": "N",
         "opCDT": "2015/09/02 14:37"
       }, 
       {
         "opPatternNo": "16803C1",
         "opCustName": "NA",
         "opIsUpload": "N",
         "opCDT": "2016/08/19 13:14"
       }, 
       {
         "opPatternNo": "17A32C6",
         "opCustName": "NA",
         "opIsUpload": "N",
         "opCDT": "2019/10/25 10:56"
       }, 
       {
         "opPatternNo": "18A42L6",
         "opCustName": "NA",
         "opIsUpload": "N",
         "opCDT": "2018/11/30 10:02"
       }, 
       {
         "opPatternNo": "19632XGC",
         "opCustName": "NA",
         "opIsUpload": "N",
         "opCDT": "2019/07/31 15:17"
       }]
    }
}
```

