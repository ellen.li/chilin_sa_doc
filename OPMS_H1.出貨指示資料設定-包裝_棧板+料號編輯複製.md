# 業務-包裝/棧板+料號編輯複製
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

包裝/棧板+料號編輯複製

## 修改歷程

| 修改時間 | 內容                                       | 修改者 |
|----------|------------------------------------------|--------|
| 20230606 | 新增規格                                   | Tim    |
| 20230710 | 調整規格                                   | Tim    |
| 20230711 | 新增 opShowAssDesc 節點、範例修改           | Nick   |
| 20230718 | 因應共用元件結構調整                       | Nick   |
| 20230801 | opConfigUid改為非必填 、opAction 用不到移除 | Nick   |
| 20230801 | 調整邏輯                                   | Nick   |

## 來源URL及資料格式

| 項目   | 說明                           |
|--------|--------------------------------|
| URL    | /sis/get_sis_config_pkg_detail |
| method | post                           |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。  
| 欄位             | 名稱               | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明                                 |
|------------------|------------------|----------|:------------------------:|-------------------------------------------|
| opConfigUid      | 包裝/棧板+料號代碼 | string   |            O             | 新增非必填，複製編輯需給值                       |
| opOrganizationId | 工廠別             | string   |            O             | 預設 null ， null:不指定 / 5000:奇菱 / 6000:菱翔 |

#### Request 範例

```json
{
  "opConfigUid":"XXX15656312-H16548MM",
  "opOrganizationId":"5000"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 依據前端參數，查詢包裝/棧板+料號資料並取回第一筆 參考 SQL-3，無資料，root.* 與 SQL-3 相關節點皆為 null，不包含 opSisList。
* 依據前端參數，查詢出貨指示項目清單 參考 SQL-1，結果為 opSisList 內容，無資料給 opSisList:null ，有資料滾每筆資料，將出貨指示項目ID[{opListHeaderId}] 帶入 SQL-2 查詢出貨指示選項清單 ，將查詢的結果加至出貨指示選項清單{opOptionList}
* 自定義變數 {ShowAssDesc} 預設為 'N'，  前面 SQL-2 結果中的 SHOWASSDESC 欄位有一筆為 'Y'，則 {ShowAssDesc}覆寫為 'Y'
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
--dao.getSisConfigPkgSingle(organizationId,configUid );
SELECT CONVERT(int, L.LIST_HEADER_ID) LIST_HEADER_ID,L.SP_INS_NAME,L.CATEGORY_FLAG CATEGORY_ATTRI,
CONVERT(int, L.LIST_SEQ) LIST_SEQ,S.CONFIG_UID,S.OPT_DESC,S.ASS_DESC,S.FLAG_ASSIGN,CONVERT(int, S.LIST_LINE_ID) LIST_LINE_ID
FROM SYS_SIS_H L LEFT JOIN (SELECT A.CONFIG_UID,A.LIST_HEADER_ID,A.LIST_LINE_ID,A.OPT_DESC,A.ASS_DESC,B.FLAG_ASSIGN FROM SIS_CONFIG A INNER JOIN SYS_SIS_L B 
ON A.LIST_HEADER_ID=B.LIST_HEADER_ID AND A.LIST_LINE_ID=B.LIST_LINE_ID WHERE A.CONFIG_UID={opConfigUid}) S
ON L.LIST_HEADER_ID=S.LIST_HEADER_ID WHERE
L.ORGANIZATION_ID={opOrganizationId} AND L.FLAG_CONFIG='Y' AND L.ACTIVE_FLAG='Y' AND L.MAINTAIN_FLAG='業助' AND L.FLAG_CONFIG_PKG='Y' ORDER BY L.LIST_SEQ

```

SQL-2:
```sql
SELECT 
    CASE WHEN (FLAG_ASSIGN IS NULL OR FLAG_ASSIGN = '') 
      THEN 'N'  
      ELSE FLAG_ASSIGN   
    END AS SHOWASSDESC, 
    cast(ISNULL(FLAG_ASSIGN, 'N') as varchar(20)) + cast(LIST_LINE_ID as varchar(20)) [VALUE],
    cast(ISNULL(LIST_SEQ, '') as varchar(20)) + '-' + OPT_DESC SHOW_TEXT,
    OPT_DESC [TEXT]
FROM SYS_SIS_L
WHERE LIST_HEADER_ID = { opListHeaderId }
ORDER BY LIST_SEQ
```
SQL-3:
```sql
--dao.getSisConfigHeader(configUid);
SELECT S.ORGANIZATION_ID,S.CONFIG_UID,S.CUST_NO,C.CUST_NAME,S.ITEM_NO,S.PKG_TYPE,S.ITEM_RULE,S.ITEM_RULE_VALUE,REPLACE(CONVERT(VARCHAR(19),S.UDT,120),'-','/') AS UDT,S.UPDATE_BY
FROM SIS_CONFIG_H S LEFT JOIN (SELECT DISTINCT CUST_NO,CUST_NAME FROM SO_H) C
ON S.CUST_NO=C.CUST_NO WHERE S.CONFIG_UID={opConfigUid}
```

# Response 欄位
| 欄位             | 名稱               | 資料型別 | 資料儲存 & 說明          |
|------------------|------------------|----------|--------------------------|
| opConfigUid      | 包裝/棧板+料號代碼 | string   | S.CONFIG_UID(SQL-3)      |
| opOrganizationId | 工廠別             | string   | S.ORGANIZATION_ID(SQL-3) |
| opCustNo         | 客戶代號           | string   | S.CUST_NO(SQL-3)         |
| opCustName       | 客戶名稱           | string   | C.CUST_NAME(SQL-3)       |
| opItemNo         | 料號               | string   | S.ITEM_NO(SQL-3)         |
| opPkgType        | 包裝/棧板+料號類型 | string   | S.PKG_TYPE(SQL-3)        |
| opItemRule       | 規則               | string   | S.ITEM_RULE(SQL-3)       |
| opItemRuleValue  | 規則值             | string   | S.ITEM_RULE_VALUE(SQL-3) |
| opUdt            | 更新日期           | string   | UDT(SQL-3)               |
| opUpdateBy       | 更新人員           | string   | UPDATE_BY(SQL-3)         |
| opSisList        | 出貨指定設定值清單 | array    |                          |

#### 【opSisList】array
| 欄位            | 名稱                 | 資料型別 | 來源資料 & 說明        |
|-----------------|--------------------|----------|------------------------|
| opListHeaderId  | 出貨指示項目ID       | string   | LIST_HEADER_ID(SQL-1)  |
| opListLineId    | 出貨指示項目ID       | string   | LIST_LINE_ID(SQL-1)    |
| opConfigUid     | 包裝/棧板+料號代碼   | string   | S.CONFIG_UID(SQL-1)    |
| opListSeq       | 順序                 | string   | LIST_SEQ(SQL-1)        |
| opSpInsName     | 出貨指示名稱         | string   | NL.SP_INS_NAME(SQL-1)  |
| opCategoryAttri | 類別                 | string   | L.CATEGORY_FLAG(SQL-1) |
| opAssDesc       | 指定內容             | string   | S.ASS_DESC(SQL-1)      |
| opOptDesc       | 選項內容             | string   | S.OPT_DESC(SQL-1)      |
| opFlagAssign    | 可否指定內容         | string   | S.FLAG_ASSIGN(SQL-1)   |
| opModifyFlag    | 後端元件對齊用欄位   | string   | 固定為 NULL            |
| opShowAssDesc   | 是否顯示指定內容元件 | string   | {ShowAssDesc}  Y/N     |
| opOptionList    | 出貨指示選項清單     | array    |                        |

#### 【opOptionList】array
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明  |
|------------|------------|----------|------------------|
| opValue    | 選項值       | string   | VALUE(SQL-2)     |
| opText     | 選項名稱     | string   | OPT_DESC(SQL-2)  |
| opShowText | 選項顯示名稱 | string   | SHOW_TEXT(SQL-2) |



#### Response 範例

```json
{
    "msgCode": null,
    "result": {
       "content": 
       {
        "opConfigUid":null,
        "opOrganizationId":null,
        "opCustNo":null,
        "opCustName":null,
        "opItemNo":null,
        "opPkgType":null,
        "opItemRule":null,
        "opItemRuleValue":null,
        "opUdt":null,
        "opUpdateBy":null,
        "opSisList":[
            {                
                "opListHeaderId":"68",
                "opListLineId":null,
                "opConfigUid":null,
                "opListSeq":null,
                "opSpInsName":null,
                "opCategoryAttri":null,
                "opAssDesc":null,
                "opOptDesc":null,
                "opFlagAssign":null,
                "opModifyFlag":null,
                "opShowAssDesc":"N",
                "opOptionList":[
                            {
                                "opShowText":"1-外袋重:25Kg",
                                "opText":"外袋重:25Kg",
                                "opValue":"N482"
                            },
                            {
                                "opShowText":"2-外袋重:20Kg",
                                "opText":"外袋重:20Kg",
                                "opValue":"N483"
                            }
                ]
            },
            {                
                "opListHeaderId":"69",
                "opListLineId":null,
                "opConfigUid":null,
                "opListSeq":null,
                "opSpInsName":null,
                "opCategoryAttri":null,
                "opAssDesc":null,
                "opOptDesc":null,
                "opFlagAssign":null,
                "opModifyFlag":null,
                "opShowAssDesc":"N",
                "opOptionList":[
                        {
                            "opShowText":"1-外袋重:25Kg",
                            "opText":"外袋重:25Kg",
                            "opValue":"N482"
                        },
                        {
                            "opShowText":"2-外袋重:20Kg",
                            "opText":"外袋重:20Kg",
                            "opValue":"N483"
                        }
                ]
            }
			]
		}
    }
}
```

## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考

* SisConfigManageAction.sisConfigPkgEditPre