# 角色管理-取得角色清單(不包含停用)
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得角色清單，可過濾關鍵字(不包含停用)

## 修改歷程

| 修改時間 | 內容                            | 修改者 |
|----------|-------------------------------|--------|
| 20230426 | 新增規格                        | Nick   |
| 20230815 | 調整 TOP 1000(使用OFFSET FETCH) | Nick   |

## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    |  /role/list
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位       | 名稱           | 資料型別 | 必填 | 資料儲存 & 說明                                                         |
|------------|--------------|:--------:|:----:|-----------------------------------------------------------------------|
| opKeyword  | 快速搜尋關鍵字 |  string  |  O   | 預設: `null`                                                            |
| opPage     | 第幾頁         | integer  |  O   | 預設`1`，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推 |
| opPageSize | 分頁筆數       | integer  |  O   | 預設 `10`                                                               |
#### Request 範例

```json
{
  "opKeyword":"nick",
  "opPage":1,
  "opPageSize":10,
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 使用前端給的條件組出 WHERE 條件(使用 AND 開頭，方便與原WHERE 條件串接)，如為 null 代表不需組條件，關鍵字使用模糊搜尋(like 前後使用%)，搜尋 ROLE_ID、ROLE_NAME 欄位，兩欄位使用 or 相加，並用 AND 與前面條件串。
* { 組好的WHERE條件內容 }範例 : AND (ROLE_ID like N'%test%' or ROLE_NAME like N'%test%')
* {offset} 為起始列印 row ，第一頁為 0，第二頁後為 0 + (opPageSize*(opPage-1))
* 參考 SQL-1 查回結果 result
* Response 結果需含分頁資訊，無資料 pageInfo 裡面項目皆給 0，頁數與分頁筆數前端如有提供，依前端判斷給的資料計算相關數值及拆分頁面
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
SELECT *
FROM (
        SELECT ROLE_ID,
            ROLE_NAME
        FROM SYS_ROLE_V3
        WHERE VISIBLE = 1 { 組好的WHERE條件內容 }
        ORDER BY ROLE_ID OFFSET 0 ROWS FETCH NEXT 1000 ROWS ONLY
    ) AS TOP1000T
ORDER BY (SELECT NULL) OFFSET { offset } ROWS FETCH NEXT { opageSize } ROWS ONLY
```

# Response 欄位
| 欄位    | 名稱     | 資料型別 | 說明     |
|---------|--------|----------|----------|
| msgCode | 訊息代碼 | string   |          |
| result  | 回傳資料 | object   | 進階訊息 |


#### 【result】child node
| 欄位     | 名稱     | 資料型別 | 來源資料 & 說明 |
|----------|--------|----------|-----------------|
| pageInfo | 分頁資訊 | object   |                 |
| content  | 附帶訊息 | array    |                 |
#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明 |
|------------|------------|----------|-----------------|
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |

#### 【content】array
| 欄位       | 名稱     | 資料型別 | 來源資料 & 說明 |
|------------|--------|----------|-----------------|
| opRoleId   | 角色ID   | integer  | ROLE_ID         |
| opRoleName | 角色名稱 | string   | ROLE_NAME       |


#### Response 範例

```json
{
  "msgCode":"",
  "result":{
    "pageInfo": {
      "pageNumber": 1, 
      "pageSize": 10,
      "pages": 1,      
      "total": 3     
    },
    "content":[
                { "opRoleId":931, "opRoleName":"工務：案件管理"},
                { "opRoleId":932, "opRoleName":"工務：維修工程師"},
                { "opRoleId":430, "opRoleName":"BOM維護"}
              ]
  }
}
```

