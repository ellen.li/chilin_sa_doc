# 染色-SOC管理-發行
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容                     | 修改者 |
| -------- | ------------------------ | ------ |
| 20230901 | 新增規格                 | 黃東俞 |
| 20231220 | 改由後端呼叫E奇簽發行api | Ellen  |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /soc/soc_c_public           |
| method | post                        |


## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位      | 名稱 | 資料型別 | 必填 | 資料儲存 & 說明     |
|-----------|------|----------|:----:|-------------------|
| opSocCUid | ID  | string   |  M   |                   |

#### Request 範例

```json
{
  "opSocCUid": "44CEDF0C-BE87-4CB3-B1FC-DDF98285097B"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 取{登入者ID}=SYS_USER.USER_ID
* 工廠別="6000"時
  * 發行SOC
    * 執行SQL-1將該筆狀態改為發行"Y"，
    * 執行SQL-LOG-1寫入發行紀錄
  * 將其他資料改為失效
    * 執行SQL-2，查詢其他相同ITEM_NO和EQUIP_ID的SOC_C.SOC_C_UID
    * 執行SQL-3，將這些資料狀態改為失效"D"
    * 執行SQL-LOG-2寫入失效紀錄
* 若工廠別="5000"時，
  * 執行SQL-4 取得ITEM_NO和EQUIP_ID
  * 呼叫[E奇簽發行網址](./OPMS_REF_雜項資料.md#E奇簽網域)
  * 執行SQL-LOG-3，紀錄E奇簽api參數
  * 執行SQL-LOG-4，紀錄E奇簽api回傳
  * 若回傳狀態不等於200  [return 400, ESIGN_PUBLIC_ERROR, 欄位相關訊息]
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
-- SocCModifyDao.publicSocC
UPDATE SOC_C SET STATUS = 'Y', PUB_DATE = GETDATE(), PUBLIC_BY = {登入者ID} WHERE SOC_C_UID = {opSocCUid}
```

SQL-2
```sql
SELECT SOC_C_UID FROM SOC_C
WHERE ITEM_NO = (SELECT ITEM_NO FROM SOC_C WHERE SOC_C_UID = {opSocCUid})
AND EQUIP_ID = (SELECT EQUIP_ID FROM SOC_C WHERE SOC_C_UID = {opSocCUid})
AND STATUS = 'Y' AND SOC_C_UID <> {opSocCUid}
```

SQL-3:

```sql
-- SocCModifyDao.publicSocC
UPDATE SOC_C SET STATUS = 'D', UDT = GETDATE(), UPDATE_BY = {登入者ID}
WHERE SOC_C_UID = {SOC_C.SOC_C_UID(SQL-2)}
```

SQL-4:
```sql
SELECT S.ITEM_NO,E.EQUIP_NAME
FROM SOC_C S
INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
WHERE SOC_C_UID = {opSocCUid}
```

SQL-LOG-1:
```sql
INSERT INTO SYS_MODI_LOG (CATEGORY, DATAKEY, USERID, USER_NAME, LOG) VALUES ('SOC_C', { opSocCUid }, {登入者ID}, {登入者名稱}, '發行');
```

SQL-LOG-2:
```sql
INSERT INTO SYS_MODI_LOG (CATEGORY, DATAKEY, USERID, USER_NAME, LOG) VALUES ('SOC_C', {SOC_C.SOC_C_UID(SQL-2)}, {登入者ID}, {登入者名稱}, '失效');
```

SQL-LOG-3:
```sql
INSERT INTO AOPLOG (URL, REQUEST, RESPONSE, CDT, CREATE_BY, UDT, UPDATE_BY) VALUES ({E奇簽api網址}, {E奇簽api參數}, '', GETDATE(), {登入者使用者ID}, GETDATE(), {登入者使用者ID});
```

SQL-LOG-4:
```sql
UPDATE AOPLOG 
SET RESPONSE={E奇簽api回傳}, UPDATE_BY={登入者使用者ID}
WHERE ID={ID(SQL-LOG-3)}
```


E奇簽發行api:

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | [E奇簽網址](./OPMS_REF_雜項資料.md#E奇簽網域)/{uat\|apis}/agents/eSign/v1/trig/PRO12621701074272046  |
| method | post                        |

| 參數名稱       | 參數值            | 型態   | 說明 |
| -------------- | ----------------- | ------ | ---- |
| itemNo         | ITEM_NO(SQL-4)    | string |      |
| equipName      | EQUIP_NAME(SQL-4) | string |      |
| organizationId | {登入者工廠別ID}  | string |      |
| prodType       | `C`               | string |      |
| socUid         | {opSocCUid}       | string |      |
| userId         | {登入者ID}        | string |      |


# Response 欄位

| 欄位     | 名稱 | 資料型別 | 資料儲存 & 說明     |
| -------- | ---- | -------- | ------------------- |
| opFormNo |      | string   | {formNo(E奇簽回傳)} |
| opUrl    |      | string   | {url(E奇簽回傳)}    |

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":{
      "opFormNo":"23120069", 
      "opUrl":"https://gwtest.chimei.com.tw/uat/esign?page=form&formKey=Tsk000023757102&formId=ART01581701077335513", 
    }
  }
}
```