# 生產排程-取得生產排程
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得生產排程


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230810 | 新增規格 | shawn  |
| 20230913 | 新增預設資料 | shawn  |


## 來源URL及資料格式

| 項目   | 說明                |
| ------ | ------------------- |
| URL    | /ps/ps_schedule_pre |
| method | post                |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             | 名稱     | 資料型別 | 必填  | 資料儲存 & 說明 |
| ---------------- | -------- | :------: | :---: | --------------- |
| opQueryDateS     | 開始日期 |  string  |   M   | yyyy/MM/dd      |
| opQueryDateE     | 結束日期 |  string  |   M   | yyyy/MM/dd      |
| opEquipId        | 機台     |  string  |   M   |                 |

#### Request 範例

```json
{
  "opQueryDateS":"2023/01/01",  
  "opQueryDateE":"2023/02/01",    
  "opEquipId":"1"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 依傳入條件查詢sql-1，回傳列表
* 每個日期至少各有早中晚三筆資料，如日期區間回傳無資料需給預設值，依照傳入日期區間，
  * opPsSeq = "0"
  * opMfgDate = 該日期
  * opEquipId = 傳入的opEquipId
  * opShiftCode = A or B or C
  * opShiftCodeName = 早 or 中 or 晚
  * 其餘資料為null
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
SELECT
	V.PS_UID,
	V.PS_SEQ,
	V.EQUIP_ID,
	V.EQUIP_NAME,
	V.MFG_DATE,
	V.SHIFT_CODE,
  case V.SHIFT_CODE when 'A' then '早'
	when 'B' then '中'
	when 'C' then '晚'
	else null end SHIFT_CODE_NAME,
	V.WO_UID,
	V.WO_NO, 
  ISNULL(WO_NO, STOP_DESC) WO_NO_DESC,
	V.PATTERN_NO,
	V.ITEM_NO,
  CASE WHEN ITEM_NO IS NULL THEN 
		case when V.STOP_DESC is not null then '停車'
		else V.STOP_DESC end
	else ITEM_NO end ITEM_NO_DESC,
  V.STOP_DESC,
	V.MTR_NAME,
	V.CTN_WEIGHT,
	V.DD_MFG,
	V.SHIFT_HOURS,
	V.WO_QTY,
	V.SHIFT_STD_QTY,
	V.SHIFT_PLN_QTY,
	V.SHIFT_MFG_QTY,
	V.REMARK
FROM
	PS_SCHEDULE_V V
WHERE
  V.ORGANIZATION_ID ={登入者工廠別} 
	AND V.EQUIP_ID={opEquipId}
  -- 日期有傳入才加入條件
  [ AND V.MFG_DATE >= {opQueryDateS} AND V.MFG_DATE <= {opQueryDateE} ]	
 ORDER BY V.EQUIP_ID,V.MFG_DATE,V.SHIFT_CODE,V.PS_SEQ,V.WO_NO
```


# Response 欄位
| 欄位            | 名稱         | 資料型別 | 資料儲存 & 說明        |
| --------------- | ------------ | -------- | ---------------------- |
| opPsUid         | uid          | string   | PS_UID(sql-1)          |
| opPsSeq         | 序號         | string   | PS_SEQ(sql-1)          |
| opEquipId       | 機台         | string   | EQUIP_ID(sql-1)        |
| opEquipName     | 機台名稱     | string   | EQUIP_NAME(sql-1)      |
| opMfgDate       | 生產日期     | string   | MFG_DATE(sql-1)        |
| opShiftCode     | 班別代碼     | string   | SHIFT_CODE(sql-1)      |
| opShiftCodeName | 班別名稱     | string   | SHIFT_CODE_NAME(sql-1) |
| opWoUid         | uid          | string   | WO_UID(sql-1)          |
| opWoNo          | 工單號碼     | string   | WO_NO(sql-1)           |
| opWoNoDesc     | 工單/停車原因         | string   | WO_NO_DESC(sql-1)      |
| opPatternNo     | 色號         | string   | PATTERN_NO(sql-1)      |
| opItemNo        | 料號         | string   | ITEM_NO(sql-1)         |
| opItemNoDesc    | 料號列表顯示 | string   | ITEM_NO_DESC(sql-1)    |
| opStopDesc      | 停車原因     | string   | STOP_DESC(sql-1)       |
| opShiftHours    | 時數         | string   | SHIFT_HOURS(sql-1)     |
| opWoQty         | 訂單未做     | string   | WO_QTY(sql-1)          |
| opShiftStdQty   | 標準產量     | string   | SHIFT_STD_QTY(sql-1)   |
| opShiftPlnQty   | 預計產量     | string   | SHIFT_PLN_QTY(sql-1)   |
| opShiftMfgQty   | 實際產量     | string   | SHIFT_MFG_QTY(sql-1)   |
| opRemark        | 排程說明     | string   | REMARK(sql-1)          |
| opCtnWeight     | 桶重         | string   | CTN_WEIGHT(sql-1)      |



#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":[
      {
        "opPsUid":"E727D2E2-9A77-4C82-8AB0-F273737E9D1A",        
        "opPsSeq":"0",        
        "opEquipId":"1",      
        "opEquipName":"染色#1",    
        "opMfgDate":"2022/01/02",      
        "opShiftCode":"A",    
        "opShiftCodeName":"早",
        "opWoUid":"36A4D4A0-DECC-4B4F-9890-4622B0E04FC3",       
        "opWoNo":"51026548",        
        "opWoNoDesc":"51026548",     
        "opPatternNo": "H95429B5",   
        "opItemNo": "757XXXXXX1DFH95429",      
        "opItemNoDesc": "停車",
        "opStopDesc":"計劃性停車",  
        "opShiftHours": "4",  
        "opWoQty":"1400",      
        "opShiftStdQty":"2800",
        "opShiftPlnQty":"1400", 
        "opShiftMfgQty":"1000",  
        "opRemark":null,
        "opCtnWeight":"1008.4"       
      },
      {
        ...
      }
    ]
  }
}
```

## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考
- PsManageAction.ajaxQueryScheduleResult