# OPMS_C.BOM維護-取得有機台的所有生產單位列表(by工廠別)
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得有機台的所有生產單位列表(by工廠別)

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230516 | 新增規格 | Nick   |

## 來源URL及資料格式

| 項目   | 說明                              |
|--------|-----------------------------------|
| URL    | /bom/get_produnit_list_with_equip |
| method | post                              |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，(非必填節點不需存在)  
| 欄位             | 名稱   | 資料型別 | 必填 | 來源資料 & 說明                                   |
|------------------|------|----------|:----:|---------------------------------------------|
| opOrganizationId | 工廠別 | string   |  O   | 預設 `null` ， null:不指定 / 5000:奇菱 / 6000:菱翔 |
#### Request 範例

```json
{
  "opOrganizationId": null
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 前端條件如為 null 表全部，對應的指定條件不組，否則按照前端條件指定。
* 參考 SQL-1 查回結果 result
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容


# Request 後端邏輯說明

SQL-1:
```sql
SELECT DISTINCT P.PRODUNIT_NO,P.PRODUNIT_NAME
FROM PRODUNIT P
INNER JOIN EQUIP_H E ON P.ORGANIZATION_ID=E.ORGANIZATION_ID AND P.PRODUNIT_NO=E.PRODUNIT_NO
WHERE P.ORGANIZATION_ID=?
WHERE 1=1 
--指定條件
AND P.ORGANIZATION_ID={opOrganizationId}  
```


# Response 欄位
| 欄位         | 名稱                 | 資料型別 | 資料儲存 & 說明          |
| ------------ | ------------        | -------- | ------------------------ |

#### 【content】array
| 欄位           | 名稱         | 資料型別 | 來源資料 & 說明 |
|----------------|------------|----------|-----------------|
| opProdunitNo   | 生產單位代號 | string   | P.PRODUNIT_NO   |
| opProdunitName | 生產單位名稱 | string   | P.PRODUNIT_NAME |

#### Response 範例

```json
{
    "msgCode": null,
    "result": {
       "content": [
          {"opProdunitNo":"001", "opProdunitName":"憲成"},
          {"opProdunitNo":"002", "opProdunitName":"良輝"},
          {"opProdunitNo":"004", "opProdunitName":"敬揚"},
          {"opProdunitNo":"005", "opProdunitName":"資堡"},
          {"opProdunitNo":"008", "opProdunitName":"高嘉"},
          {"opProdunitNo":"028", "opProdunitName":"松旻"},
          {"opProdunitNo":"999", "opProdunitName":"委外"},
          {"opProdunitNo":"A01", "opProdunitName":"樣品課"},
          {"opProdunitNo":"A02", "opProdunitName":"配色課"},
          {"opProdunitNo":"A03", "opProdunitName":"研發課"},
          {"opProdunitNo":"A04", "opProdunitName":"色粉室"},
          {"opProdunitNo":"A05", "opProdunitName":"品管課"},
          {"opProdunitNo":"A06", "opProdunitName":"倉管課"},
          {"opProdunitNo":"A07", "opProdunitName":"工務課"},
          {"opProdunitNo":"A08", "opProdunitName":"安衛課"},
          {"opProdunitNo":"C1N", "opProdunitName":"染色"},
          {"opProdunitNo":"C2N", "opProdunitName":"複材一課"}
      ]
    }
}
```

