# 產銷進度-單筆
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

查詢產銷進度


## 修改歷程

| 修改時間  | 內容    | 修改者 |
|----------|---------|--------|
| 20230815 | 新增規格 | 黃東俞 |


## 來源URL及資料格式

| 項目   | 說明       |
|--------|-----------|
| URL    | /so/list  |
| method | post      |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位            | 名稱            | 資料型別 | 必填 | 資料儲存 & 說明                              |
|-----------------|----------------|:--------:|:----:|--------------------------------------------|
| opSalesOrderNo  | 訂單號碼        | string   |   M  |                                            |
| opWoNo          | 工單號碼        | string   |   M  |                                            |

#### Request 範例

```json
{
  "opSalesOrderNo": "0009012398",
  "opWoNo": "52019203"
}
```

# Request 後端流程說明

* 與 [OPMS_H2.產銷進度-列表 /so/list](./OPMS_H2.產銷進度-列表.md) 同一支API，僅傳入的參數不同。