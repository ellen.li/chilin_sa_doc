# 功能管理-系統功能清單修改存檔
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

系統功能清單修改存檔


## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230406 | 新增規格 | Nick   |

## 來源URL及資料格式

| 項目   | 說明                |
|--------|---------------------|
| URL    | /function/list_save |
| method | post                |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
【array content】
| 欄位     | 名稱         | 資料型別 | 必填 | 資料儲存 & 說明 |
|----------|------------|:--------:|:----:|-----------------|
| opFCode  | 功能代號     |  string  |  M   | null 表示新項目 |
| opFName  | 功能名稱     |  string  |  M   |                 |
| opUrl    | 功能URL      |  string  |  O   |                 |
| opParent | 父階功能代號 |  string  |  M   |                 |

#### Request 範例

```json
{
  "data":[
    {"opFCode":"200", "opFName":"測試功能2",  "opUrl":null, "opParent":"0"},
    {"opFCode":"100", "opFName":"測試功能1",  "opUrl":null, "opParent":"0"},
    {"opFCode":"300", "opFName":"測試功能3",  "opUrl":"http://www.google.com", "opParent":"0"},
    {"opFCode":"400", "opFName":"測試功能4",  "opUrl":null, "opParent":"0"},
    {"opFCode":"411", "opFName":"測試功能4-1",  "opUrl":"http://www.google.com", "opParent":"400"},
    {"opFCode":"407", "opFName":"測試功能4-2",  "opUrl":null, "opParent":"400"},
    {"opFCode":"404", "opFName":"測試功能4-3",  "opUrl":null, "opParent":"400"},
    {"opFCode":"493", "opFName":"測試功能4-4",  "opUrl":null, "opParent":"400"}
  ]
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* {DATAKEY} = {opFCode}
* 組出所有 opFCode 清單，並參考SQL-1 組出正確SQL，找出所有被刪除的項目，並執行 SQL-2 變更成隱藏(不做實體刪除)，並參考SQL-4 寫入LOG，{logContent}內容為 "刪除 【{opFName}】"
* {SORT_ID}依照資料在JSON中的先後順序，使用流水編，從1開始，後續+1，
* 若 opParent 如為 "0" 則 {LEVEL} = 1 ，否則{LEVEL} = 2
* 依照前端條件，滾每一筆資料，將有異動的資料回寫，並寫入LOG，該筆僅是變更順序，{logContent}內容為 "變更順序【{opFName}】"，如果還有其他欄位異動，則加寫"編輯【{opFName}】"
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明


SQL-1:
```sql
--找出所有未在清單內的項目
SELECT CODE FROM SYS_FUNCTION_V3 
  WHERE VISIBLE = 1 
    AND CODE NOT IN ('{opFCode[0]}','{opFCode[1]}'.......)
```
SQL-2
```sql
--刪除項目，更新  VISIBLE 為 0
  UPDATE SYS_FUNCTION_V3 SET VISIBLE=0,UDT=SYSDATETIME(),UPDATE_BY={登入者使用者ID} WHERE CODE={opFCode}
```

SQL-3:
```sql
UPDATE SYS_FUNCTION_V3 SET 
                    CNAME={opFName},
                    LEVEL={LEVEL},
                    PARENT={opParent},
                    URL={opUrl},
                    VISIBLE=1,
                    SORT_ID={SORT_ID流水編},
                    UDT=SYSDATETIME(),
                    UPDATE_BY={登入者使用者ID} 
                WHERE CODE={opFCode}
```

SQL-4:
```sql
INSERT INTO  SYS_MODI_LOG (CATEGORY,DATAKEY,USERID,USER_NAME,LOG) VALUES ('FUNC',{DATAKEY},{登入者使用者ID},{登入者使用者名稱}, {logContent})
```


# Response 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位         | 名稱         | 資料型別 | 資料儲存 & 說明          |
| ------------ | ------------ | -------- | ------------------------ |


## Response 範例

```json
{
  "msgCode": null, 
  "result":{
    "content": null
  }
}
```

