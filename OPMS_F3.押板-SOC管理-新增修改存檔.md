# 押板-SOC管理-新增修改存檔
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容             | 修改者 |
| -------- | ---------------- | ------ |
| 20230830 | 新增規格         | 黃東俞 |
| 20231026 | 調整欄位說明文字 | Nick   |
| 20231030 | 狀態預設為N      | Ellen  |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /soc/soc_b_save             |
| method | post                        |


## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位              | 名稱                  | 資料型別 | 必填 | 資料儲存 & 說明                                     |
| ----------------- | --------------------- | -------- | :--: | --------------------------------------------------- |
| opSocBUid         | ID                    | string   |  O   |                                                     |
| opEquipId         | 機台                  | int      |  O   |                                                     |
| opItemNo          | 料號                  | string   |  M   |                                                     |
| opStatus          | 狀態                  | string   |  O   | N:改版中 / W:簽核中 / Y:已發行 / D:已失效  預設:`N` |
| opAc1Std          | (A層)A1標準           | int      |  O   |                                                     |
| opAc1Max          | (A層)A1上限           | int      |  O   |                                                     |
| opAc1Min          | (A層)A1下限           | int      |  O   |                                                     |
| opAc2Std          | (A層)A2標準           | int      |  O   |                                                     |
| opAc2Max          | (A層)A2上限           | int      |  O   |                                                     |
| opAc2Min          | (A層)A2下限           | int      |  O   |                                                     |
| opAc3Std          | (A層)A3標準           | int      |  O   |                                                     |
| opAc3Max          | (A層)A3上限           | int      |  O   |                                                     |
| opAc3Min          | (A層)A3下限           | int      |  O   |                                                     |
| opAc4Std          | (A層)A4標準           | int      |  O   |                                                     |
| opAc4Max          | (A層)A4上限           | int      |  O   |                                                     |
| opAc4Min          | (A層)A4下限           | int      |  O   |                                                     |
| opAc5Std          | (A層)A5標準           | int      |  O   |                                                     |
| opAc5Max          | (A層)A5上限           | int      |  O   |                                                     |
| opAc5Min          | (A層)A5下限           | int      |  O   |                                                     |
| opAc6Std          | (A層)A6標準           | int      |  O   |                                                     |
| opAc6Max          | (A層)A6上限           | int      |  O   |                                                     |
| opAc6Min          | (A層)A6下限           | int      |  O   |                                                     |
| opAc7Std          | (A層)A7標準           | int      |  O   |                                                     |
| opAc7Max          | (A層)A7上限           | int      |  O   |                                                     |
| opAc7Min          | (A層)A7下限           | int      |  O   |                                                     |
| opAc8Std          | (A層)A8標準           | int      |  O   |                                                     |
| opAc8Max          | (A層)A8上限           | int      |  O   |                                                     |
| opAc8Min          | (A層)A8下限           | int      |  O   |                                                     |
| opAc9Std          | (A層)A9標準           | int      |  O   |                                                     |
| opAc9Max          | (A層)A9上限           | int      |  O   |                                                     |
| opAc9Min          | (A層)A9下限           | int      |  O   |                                                     |
| opAcAStd          | (A層)A10標準          | int      |  O   |                                                     |
| opAcAMax          | (A層)A10上限          | int      |  O   |                                                     |
| opAcAMin          | (A層)A10下限          | int      |  O   |                                                     |
| opAScrewStd       | (A層)螺桿標準         | int      |  O   |                                                     |
| opAScrewMax       | (A層)螺桿上限         | int      |  O   |                                                     |
| opAScrewMin       | (A層)螺桿下限         | int      |  O   |                                                     |
| opACurrStd        | (A層)電流標準         | int      |  O   |                                                     |
| opACurrMax        | (A層)電流上限         | int      |  O   |                                                     |
| opACurrMin        | (A層)電流下限         | int      |  O   |                                                     |
| opAGpStd          | (A層)GP標準           | int      |  O   |                                                     |
| opAGpMax          | (A層)GP上限           | int      |  O   |                                                     |
| opAGpMin          | (A層)GP下限           | int      |  O   |                                                     |
| opBc1Std          | (B層)C1標準           | int      |  O   |                                                     |
| opBc1Max          | (B層)C1上限           | int      |  O   |                                                     |
| opBc1Min          | (B層)C1下限           | int      |  O   |                                                     |
| opBc2Std          | (B層)C2標準           | int      |  O   |                                                     |
| opBc2Max          | (B層)C2上限           | int      |  O   |                                                     |
| opBc2Min          | (B層)C2下限           | int      |  O   |                                                     |
| opBc3Std          | (B層)C3標準           | int      |  O   |                                                     |
| opBc3Max          | (B層)C3上限           | int      |  O   |                                                     |
| opBc3Min          | (B層)C3下限           | int      |  O   |                                                     |
| opBc4Std          | (B層)C4標準           | int      |  O   |                                                     |
| opBc4Max          | (B層)C4上限           | int      |  O   |                                                     |
| opBc4Min          | (B層)C4下限           | int      |  O   |                                                     |
| opBc5Std          | (B層)C5標準           | int      |  O   |                                                     |
| opBc5Max          | (B層)C5上限           | int      |  O   |                                                     |
| opBc5Min          | (B層)C5下限           | int      |  O   |                                                     |
| opBc6Std          | (B層)C6標準           | int      |  O   |                                                     |
| opBc6Max          | (B層)C6上限           | int      |  O   |                                                     |
| opBc6Min          | (B層)C6下限           | int      |  O   |                                                     |
| opBc7Std          | (B層)C7標準           | int      |  O   |                                                     |
| opBc7Max          | (B層)C7上限           | int      |  O   |                                                     |
| opBc7Min          | (B層)C7下限           | int      |  O   |                                                     |
| opBc8Std          | (B層)C8標準           | int      |  O   |                                                     |
| opBc8Max          | (B層)C8上限           | int      |  O   |                                                     |
| opBc8Min          | (B層)C8下限           | int      |  O   |                                                     |
| opBc9Std          | (B層)C9標準           | int      |  O   |                                                     |
| opBc9Max          | (B層)C9上限           | int      |  O   |                                                     |
| opBc9Min          | (B層)C9下限           | int      |  O   |                                                     |
| opBcAStd          | (B層)C10標準          | int      |  O   |                                                     |
| opBcAMax          | (B層)C10上限          | int      |  O   |                                                     |
| opBcAMin          | (B層)C10下限          | int      |  O   |                                                     |
| opBcBStd          | (B層)C11標準          | int      |  O   |                                                     |
| opBcBMax          | (B層)C11上限          | int      |  O   |                                                     |
| opBcBMin          | (B層)C11下限          | int      |  O   |                                                     |
| opBcCStd          | (B層)C12標準          | int      |  O   |                                                     |
| opBcCMax          | (B層)C12上限          | int      |  O   |                                                     |
| opBcCMin          | (B層)C12下限          | int      |  O   |                                                     |
| opBcDStd          | (B層)C13標準          | int      |  O   |                                                     |
| opBcDMax          | (B層)C13上限          | int      |  O   |                                                     |
| opBcDMin          | (B層)C13下限          | int      |  O   |                                                     |
| opBcEStd          | (B層)C14標準          | int      |  O   |                                                     |
| opBcEMax          | (B層)C14上限          | int      |  O   |                                                     |
| opBcEMin          | (B層)C14下限          | int      |  O   |                                                     |
| opBcNStd          | (B層)脖溫標準         | int      |  O   |                                                     |
| opBcNMax          | (B層)脖溫上限         | int      |  O   |                                                     |
| opBcNMin          | (B層)脖溫下限         | int      |  O   |                                                     |
| opBScrewStd       | (B層)螺桿標準         | int      |  O   |                                                     |
| opBScrewMax       | (B層)螺桿上限         | int      |  O   |                                                     |
| opBScrewMin       | (B層)螺桿下限         | int      |  O   |                                                     |
| opBCurrStd        | (B層)電流標準         | int      |  O   |                                                     |
| opBCurrMax        | (B層)電流上限         | int      |  O   |                                                     |
| opBCurrMin        | (B層)電流下限         | int      |  O   |                                                     |
| opBGpStd          | (B層)GP標準           | int      |  O   |                                                     |
| opBGpMax          | (B層)GP上限           | int      |  O   |                                                     |
| opBGpMin          | (B層)GP下限           | int      |  O   |                                                     |
| opD1Std           | (模頭溫度)D1標準      | int      |  O   |                                                     |
| opD1Max           | (模頭溫度)D1上限      | int      |  O   |                                                     |
| opD1Min           | (模頭溫度)D1下限      | int      |  O   |                                                     |
| opD2Std           | (模頭溫度)D2標準      | int      |  O   |                                                     |
| opD2Max           | (模頭溫度)D2上限      | int      |  O   |                                                     |
| opD2Min           | (模頭溫度)D2下限      | int      |  O   |                                                     |
| opD3Std           | (模頭溫度)D3標準      | int      |  O   |                                                     |
| opD3Max           | (模頭溫度)D3上限      | int      |  O   |                                                     |
| opD3Min           | (模頭溫度)D3下限      | int      |  O   |                                                     |
| opD4Std           | (模頭溫度)D4標準      | int      |  O   |                                                     |
| opD4Max           | (模頭溫度)D4上限      | int      |  O   |                                                     |
| opD4Min           | (模頭溫度)D4下限      | int      |  O   |                                                     |
| opD5Std           | (模頭溫度)D5標準      | int      |  O   |                                                     |
| opD5Max           | (模頭溫度)D5上限      | int      |  O   |                                                     |
| opD5Min           | (模頭溫度)D5下限      | int      |  O   |                                                     |
| opD6Std           | (模頭溫度)D6標準      | int      |  O   |                                                     |
| opD6Max           | (模頭溫度)D6上限      | int      |  O   |                                                     |
| opD6Min           | (模頭溫度)D6下限      | int      |  O   |                                                     |
| opD7Std           | (模頭溫度)D7標準      | int      |  O   |                                                     |
| opD7Max           | (模頭溫度)D7上限      | int      |  O   |                                                     |
| opD7Min           | (模頭溫度)D7下限      | int      |  O   |                                                     |
| opR1Std           | (滾筒溫度)(上)標準    | int      |  O   |                                                     |
| opR1Max           | (滾筒溫度)(上)上限    | int      |  O   |                                                     |
| opR1Min           | (滾筒溫度)(上)下限    | int      |  O   |                                                     |
| opR2Std           | (滾筒溫度)(中)標準    | int      |  O   |                                                     |
| opR2Max           | (滾筒溫度)(中)上限    | int      |  O   |                                                     |
| opR2Min           | (滾筒溫度)(中)下限    | int      |  O   |                                                     |
| opR3Std           | (滾筒溫度)(下)標準    | int      |  O   |                                                     |
| opR3Max           | (滾筒溫度)(下)上限    | int      |  O   |                                                     |
| opR3Min           | (滾筒溫度)(下)下限    | int      |  O   |                                                     |
| opRSpeedStd       | (滾筒速度)標準        | float    |  O   |                                                     |
| opRSpeedMax       | (滾筒速度)上限        | float    |  O   |                                                     |
| opRSpeedMin       | (滾筒速度)下限        | float    |  O   |                                                     |
| opRGapL           | 滾筒間隙(左)          | float    |  O   |                                                     |
| opRGapR           | 滾筒間隙(右)          | float    |  O   |                                                     |
| opVacuumIndex     | 真空指數              | int      |  O   |                                                     |
| opLayer1          | (層間比)(上)標準      | int      |  O   |                                                     |
| opLayer2          | (層間比)(中)標準      | int      |  O   |                                                     |
| opLayer2          | (層間比)(下)標準      | int      |  O   |                                                     |
| opModelGap        | 模唇間隙              | float    |  O   |                                                     |
| opModelWidth      | 模唇出料寬度          | float    |  O   |                                                     |
| opMfgDesc         | 生產方式說明          | string   |  O   |                                                     |
| opSocDesc         | SOC識別文字           | string   |  O   |                                                     |
| opRemark          | 備註                  | string   |  O   |                                                     |
| opScreenFreq      | 網目更換              | int      |  O   |                                                     |
| opRecordFreq      | 記錄頻率              | int      |  O   |                                                     |
| opLayer1Max       | (層間比)(上)上限      | int      |  O   |                                                     |
| opLayer1Min       | (層間比)(上)下限      | int      |  O   |                                                     |
| opLayer2Max       | (層間比)(中)上限      | int      |  O   |                                                     |
| opLayer2Min       | (層間比)(中)下限      | int      |  O   |                                                     |
| opLayer3Max       | (層間比)(下)上限      | int      |  O   |                                                     |
| opLayer3Min       | (層間比)(下)下限      | int      |  O   |                                                     |
| opVacuumIndexAStd | (真空指數)A機標準     | int      |  O   |                                                     |
| opVacuumIndexAMax | (真空指數)A機上限     | int      |  O   |                                                     |
| opVacuumIndexAMin | (真空指數)A機下限     | int      |  O   |                                                     |
| opVacuumIndexBStd | (真空指數)B機標準     | int      |  O   |                                                     |
| opVacuumIndexBMax | (真空指數)B機上限     | int      |  O   |                                                     |
| opVacuumIndexBMin | (真空指數)B機下限     | int      |  O   |                                                     |
| opScreenLayerA    | (網目層別)A層         | string   |  O   |                                                     |
| opScreenLayerB    | (網目層別)B層         | string   |  O   |                                                     |
| opScBpAStd        | (網前背壓)A機標準     | int      |  O   |                                                     |
| opScBpAMax        | (網前背壓)A機上限     | int      |  O   |                                                     |
| opScBpAMin        | (網前背壓)A機下限     | int      |  O   |                                                     |
| opScBpBStd        | (網前背壓)B機標準     | int      |  O   |                                                     |
| opScBpBMax        | (網前背壓)B機上限     | int      |  O   |                                                     |
| opScBpBMin        | (網前背壓)B機下限     | int      |  O   |                                                     |
| opMtrDryTempA     | (原料乾燥條件)A層溫度 | int      |  O   |                                                     |
| opMtrDryTempB     | (原料乾燥條件)B層溫度 | int      |  O   |                                                     |
| opMtrDryTimeA     | (原料乾燥條件)A層時間 | int      |  O   |                                                     |
| opMtrDryTimeB     | (原料乾燥條件)B層時間 | int      |  O   |                                                     |
| opMtrMixTimeA     | (混合時間)A層         | int      |  O   |                                                     |
| opMtrMixTimeB     | (混合時間)B層         | int      |  O   |                                                     |
| opRU              | (滾輪種類)上          | string   |  O   |                                                     |
| opRM              | (滾輪種類)中          | string   |  O   |                                                     |
| opRD              | (滾輪種類)下          | string   |  O   |                                                     |
| opRTenStd         | (收捲張力)張力標準    | float    |  O   |                                                     |
| opRTenMax         | (收捲張力)張力上限    | float    |  O   |                                                     |
| opRTenMin         | (收捲張力)張力下限    | float    |  O   |                                                     |
| opRTenSlopeStd    | (收捲張力)斜率標準    | int      |  O   |                                                     |
| opRTenSlopeMax    | (收捲張力)斜率上限    | int      |  O   |                                                     |
| opRTenSlopeMin    | (收捲張力)斜率下限    | int      |  O   |                                                     |
| opRTenSpeedStd    | (收捲張力)速度標準    | int      |  O   |                                                     |
| opRTenSpeedMax    | (收捲張力)速度上限    | int      |  O   |                                                     |
| opRTenSpeedMin    | (收捲張力)速度下限    | int      |  O   |                                                     |
| opFeedpAStd       | (進料壓力設定)A機標準 | int      |  O   |                                                     |
| opFeedpAMax       | (進料壓力設定)A機上限 | int      |  O   |                                                     |
| opFeedpAMin       | (進料壓力設定)A機下限 | int      |  O   |                                                     |
| opFeedpBStd       | (進料壓力設定)B機標準 | int      |  O   |                                                     |
| opFeedpBMax       | (進料壓力設定)B機上限 | int      |  O   |                                                     |
| opFeedpBMin       | (進料壓力設定)B機下限 | int      |  O   |                                                     |

#### Request 範例

```json
{
  "opSocBUid":"6A135E88-190D-4C61-861A-969EFFA37DD6",
  "opEquipId":42,
  "opItemNo":"DN01XXXXXXXX025001",
  "opStatus":"Y",
  "opAc1Std":null,
  "opAc1Max":null,
  "opAc1Min":null,
  "opAc2Std":null,
  "opAc2Max":null,
  "opAc2Min":null,
  "opAc3Std":null,
  "opAc3Max":null,
  "opAc3Min":null,
  "opAc4Std":null,
  "opAc4Max":null,
  "opAc4Min":null,
  "opAc5Std":null,
  "opAc5Max":null,
  "opAc5Min":null,
  "opAc6Std":null,
  "opAc6Max":null,
  "opAc6Min":null,
  "opAc7Std":null,
  "opAc7Max":null,
  "opAc7Min":null,
  "opAc8Std":null,
  "opAc8Max":null,
  "opAc8Min":null,
  "opAc9Std":null,
  "opAc9Max":null,
  "opAc9Min":null,
  "opAcAStd":null,
  "opAcAMax":null,
  "opAcAMin":null,
  "opAScrewStd":null,
  "opAScrewMax":null,
  "opAScrewMin":null,
  "opACurrStd":null,
  "opACurrMax":null,
  "opACurrMin":null,
  "opAGpStd":null,
  "opAGpMax":null,
  "opAGpMin":null,
  "opBc1Std":200,
  "opBc1Max":210,
  "opBc1Min":170,
  "opBc2Std":200,
  "opBc2Max":210,
  "opBc2Min":190,
  "opBc3Std":200,
  "opBc3Max":210,
  "opBc3Min":190,
  "opBc4Std":200,
  "opBc4Max":210,
  "opBc4Min":190,
  "opBc5Std":200,
  "opBc5Max":210,
  "opBc5Min":190,
  "opBc6Std":200,
  "opBc6Max":210,
  "opBc6Min":190,
  "opBc7Std":200,
  "opBc7Max":210,
  "opBc7Min":190,
  "opBc8Std":200,
  "opBc8Max":210,
  "opBc8Min":190,
  "opBc9Std":200,
  "opBc9Max":210,
  "opBc9Min":190,
  "opBcAStd":200,
  "opBcAMax":210,
  "opBcAMin":190,
  "opBcBStd":190,
  "opBcBMax":200,
  "opBcBMin":180,
  "opBcCStd":190,
  "opBcCMax":200,
  "opBcCMin":180,
  "opBcDStd":190,
  "opBcDMax":200,
  "opBcDMin":180,
  "opBcEStd":190,
  "opBcEMax":200,
  "opBcEMin":180,
  "opBcNStd":195,
  "opBcNMax":205,
  "opBcNMin":185,
  "opBScrewStd":50,
  "opBScrewMax":70,
  "opBScrewMin":40,
  "opBCurrStd":260,
  "opBCurrMax":280,
  "opBCurrMin":250,
  "opBGpStd":25,
  "opBGpMax":40,
  "opBGpMin":10,
  "opD1Std":190,
  "opD1Max":200,
  "opD1Min":180,
  "opD2Std":190,
  "opD2Max":200,
  "opD2Min":180,
  "opD3Std":190,
  "opD3Max":200,
  "opD3Min":180,
  "opD4Std":190,
  "opD4Max":200,
  "opD4Min":180,
  "opD5Std":190,
  "opD5Max":200,
  "opD5Min":180,
  "opD6Std":190,
  "opD6Max":200,
  "opD6Min":180,
  "opD7Std":190,
  "opD7Max":200,
  "opD7Min":180,
  "opR1Std":60,
  "opR1Max":75,
  "opR1Min":50,
  "opR2Std":70,
  "opR2Max":80,
  "opR2Min":60,
  "opR3Std":60,
  "opR3Max":75,
  "opR3Min":50,
  "opRSpeedStd":27.00,
  "opRSpeedMax":null,
  "opRSpeedMin":null,
  "opRGapL":0.25,
  "opRGapR":0.25,
  "opVacuumIndex":-60,
  "opLayer1":0,
  "opLayer2":100,
  "opLayer3":0,
  "opModelGap":0.34,
  "opModelWidth":575.00,
  "opMfgDesc":null,
  "opSocDesc":null,
  "opRemark":null,
  "opScreenFreq":null,
  "opRecordFreq":null,
  "opLayer1Max":null,
  "opLayer1Min":null,
  "opLayer2Max":null,
  "opLayer2Min":null,
  "opLayer3Max":null,
  "opLayer3Min":null,
  "opVacuumIndexAStd":60,
  "opVacuumIndexAMax":null,
  "opVacuumIndexAMin":null,
  "opVacuumIndexBStd":60,
  "opVacuumIndexBMax":null,
  "opVacuumIndexBMin":null,
  "opScreenLayerA":null,
  "opScreenLayerB":"50/80/50/50",
  "opScBpAStd":null,
  "opScBpAMax":null,
  "opScBpAMin":null,
  "opScBpBStd":100,
  "opScBpBMax":100,
  "opScBpBMin":null,
  "opMtrDryTempA":null,
  "opMtrDryTempB":null,
  "opMtrDryTimeA":null,
  "opMtrDryTimeB":null,
  "opMtrMixTimeA":null,
  "opMtrMixTimeB":30,
  "opRU":"鏡面輪",
  "opRM":"鏡面輪",
  "opRD":"鏡面輪",
  "opRTenStd":10.0,
  "opRTenMax":30.0,
  "opRTenMin":5.0,
  "opRTenSlopeStd":null,
  "opRTenSlopeMax":null,
  "opRTenSlopeMin":null,
  "opRTenSpeedStd":80,
  "opRTenSpeedMax":100,
  "opRTenSpeedMin":50,
  "opFeedpAStd":null,
  "opFeedpAMax":null,
  "opFeedpAMin":null,
  "opFeedpBStd":30,
  "opFeedpBMax":50,
  "opFeedpBMin":20
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 取 { 登入者ID } SYS_USER.USER_ID
* 當傳入的socMUid(ID)為null時，新增一筆資料至 SOC_M
  * SOC_B_UID(新ID)：取隨機 UUID
  * SOC_B_VER(新版號)：參考SQL-1，取相同 EQUIP_ID, ITEM_NO 的 SOC_B_VER 最大值+1之後作為新的版號
  * 參考 SQL-2 新增資料至 SOC_B
* 有傳入opSocUid時，參考SQL-3更新資料
  * 更新後，執行 SQL-LOG 紀錄更新資訊，LOG欄位填入"新增"或"修改"
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200

# Request 後端邏輯說明

SQL-1:

```sql
-- SocBQueryDao.getMaxSocVer
SELECT ISNULL(MAX(SOC_B_VER), 0) + 1 NEW_SOC_VER
FROM SOC_B
WHERE ITEM_NO = { opItemNo }
AND EQUIP_ID = { opEquipId }
```

SQL-2:

```sql
-- SocMModifyDao.insertSocM
INSERT INTO SOC_B(SOC_B_UID, EQUIP_ID, ITEM_NO, SOC_B_VER, STATUS,
   AC_1_STD, AC_1_MAX, AC_1_MIN, AC_2_STD, AC_2_MAX, AC_2_MIN, AC_3_STD, AC_3_MAX, AC_3_MIN, AC_4_STD, AC_4_MAX, AC_4_MIN,
   AC_5_STD, AC_5_MAX, AC_5_MIN, AC_6_STD, AC_6_MAX, AC_6_MIN, AC_7_STD, AC_7_MAX, AC_7_MIN, AC_8_STD, AC_8_MAX, AC_8_MIN,
   AC_9_STD, AC_9_MAX, AC_9_MIN, AC_A_STD, AC_A_MAX, AC_A_MIN,
   A_SCREW_STD,  A_SCREW_MAX,  A_SCREW_MIN,  A_CURR_STD,  A_CURR_MAX,  A_CURR_MIN,  A_GP_STD,  A_GP_MAX,  A_GP_MIN,
   BC_1_STD, BC_1_MAX, BC_1_MIN, BC_2_STD, BC_2_MAX, BC_2_MIN, BC_3_STD, BC_3_MAX, BC_3_MIN, BC_4_STD, BC_4_MAX, BC_4_MIN,
   BC_5_STD, BC_5_MAX, BC_5_MIN, BC_6_STD, BC_6_MAX, BC_6_MIN, BC_7_STD, BC_7_MAX, BC_7_MIN, BC_8_STD, BC_8_MAX, BC_8_MIN,
   BC_9_STD, BC_9_MAX, BC_9_MIN, BC_A_STD, BC_A_MAX, BC_A_MIN, BC_B_STD, BC_B_MAX, BC_B_MIN, BC_C_STD, BC_C_MAX, BC_C_MIN,
   BC_D_STD, BC_D_MAX, BC_D_MIN, BC_E_STD, BC_E_MAX, BC_E_MIN, BC_N_STD, BC_N_MAX, BC_N_MIN,
   B_SCREW_STD,  B_SCREW_MAX,  B_SCREW_MIN,  B_CURR_STD, B_CURR_MAX,  B_CURR_MIN,  B_GP_STD,  B_GP_MAX,  B_GP_MIN,
   D_1_STD, D_1_MAX, D_1_MIN, D_2_STD, D_2_MAX, D_2_MIN, D_3_STD, D_3_MAX, D_3_MIN, D_4_STD, D_4_MAX, D_4_MIN,
   D_5_STD, D_5_MAX, D_5_MIN, D_6_STD, D_6_MAX, D_6_MIN, D_7_STD, D_7_MAX, D_7_MIN,
   R_1_STD, R_1_MAX, R_1_MIN, R_2_STD, R_2_MAX, R_2_MIN, R_3_STD, R_3_MAX, R_3_MIN, R_SPEED_STD, R_SPEED_MAX, R_SPEED_MIN, R_GAP_L, R_GAP_R,
   MFG_DESC, SOC_DESC, REMARK, SCREEN_FREQ,  RECORD_FREQ,
   VACUUM_INDEX, LAYER_1, LAYER_2, LAYER_3, MODEL_GAP, MODEL_WIDTH,
   LAYER_1_MAX, LAYER_1_MIN, LAYER_2_MAX, LAYER_2_MIN, LAYER_3_MAX, LAYER_3_MIN,
   VACUUM_INDEX_A_STD,  VACUUM_INDEX_A_MAX,  VACUUM_INDEX_A_MIN, VACUUM_INDEX_B_STD,  VACUUM_INDEX_B_MAX, ACUUM_INDEX_B_MIN,
   SCREEN_LAYER_A, SCREEN_LAYER_B, SC_BP_A_STD, SC_BP_B_STD, SC_BP_A_MAX, SC_BP_B_MAX, SC_BP_A_MIN, SC_BP_B_MIN,
   MTR_DRY_TEMP_A, MTR_DRY_TEMP_B, MTR_DRY_TIME_A, MTR_DRY_TIME_B, MTR_MIX_TIME_A, MTR_MIX_TIME_B,
   R_U, R_M, R_D, R_TEN_STD, R_TEN_MAX, R_TEN_MIN,
   R_TEN_SLOPE_STD, R_TEN_SLOPE_MAX, R_TEN_SLOPE_MIN, R_TEN_SPEED_STD, R_TEN_SPEED_MAX, R_TEN_SPEED_MIN,
   FEEDP_A_STD, FEEDP_A_MAX, FEEDP_A_MIN, FEEDP_B_STD, FEEDP_B_MAX, FEEDP_B_MIN,
   CDT, CREATE_BY, UDT, UPDATE_BY)
VALUES({新ID}, {opEquipId}, {opItemNo}, {opSocMVer}, {opStatus},
  {opAc1Std}, {opAc1Max}, {opAc1Min}, {opAc2Std}, {opAc2Max}, {opAc2Min}, {opAc3Std}, {opAc3Max}, {opAc3Min}, {opAc4Std}, {opAc4Max}, {opAc4Min},
  {opAc5Std}, {opAc5Max}, {opAc5Min}, {opAc6Std}, {opAc6Max}, {opAc62Min}, {opAc7Std}, {opAc7Max}, {opAc7Min}, {opAc8Std}, {opAc8Max}, {opAc8Min},
  {opAc9Std}, {opAc6Max}, {opAc9Min}, {opAcAStd}, {opAcAMax}, {opAcAMin},
  {opAScrewStd}, {opAScrewMax}, {opAScrewMin}, {opACurrStd}, {opACurrMax}, {opACurrMin}, {opAGpStd}, {opAGpMax}, {opAGpMin},
  {opBc1Std}, {opBc1Max}, {opBc1Min}, {opBc2Std}, {opBc2Max}, {opBc2Min}, {opBc3Std}, {opBc3Max}, {opBc3Min}, {opBc4Std}, {opBc4Max}, {opBc4Min},
  {opBc5Std}, {opBc5Max}, {opBc5Min}, {opBc6Std}, {opBc6Max}, {opBc6Min}, {opBc7Std}, {opBc7Max}, {opBc7Min}, {opBc8Std}, {opBc8Max}, {opBc8Min},
  {opBc9Std}, {opBc9Max}, {opBc9Min}, {opBcAStd}, {opBcAMax}, {opBcAMin}, {opBcBStd}, {opBcBMax}, {opBcBMin}, {opBcCStd}, {opBcCMax}, {opBcCMin},
  {opBcDStd}, {opBcDMax}, {opBcDMin}, {opBcEStd}, {opBcEMax}, {opBcEMin}, {opBcNStd}, {opBcNMax}, {opBcNMin},
  {opBScrewStd}, {opBScrewMax}, {opBScrewMin}, {opBCurrStd}, {opBCurrMax}, {opBCurrMin}, {opBGpStd}, {opBGpMax}, {opBGpMin},
  {opD1Std}, {opD1Max}, {opD1Min}, {opD2Std}, {opD2Max}, {opD2Min}, {opD3Std}, {opD3Max}, {opD3Min}, {opD4Std}, {opD4Max}, {opD4Min},
  {opD5Std}, {opD5Max}, {opD5Min}, {opD6Std}, {opD6Max}, {opD6Min}, {opD7Std}, {opD7Max}, {opD7Min},
  {opR1Std}, {opR1Max}, {opR1Min}, {opR2Std}, {opR2Max}, {opR2Min}, {opR3Std}, {opR3Max}, {opR3Min}, {opRSpeedStd}, {opRSpeedMax}, {opRSpeedMin}, {opRGapL}, {opRGapR},
  {opMfgDesc}, {opSocDesc}, {opRemark}, {opScreenFreq}, {opRecordFreq},
  {opVacuumIndex}, {opLayer1}, {opLayer2}, {opLayer3}, {opModelGap}, {opModelWidth},
  {opLayer1Max}, {opLayer1Min}, {opLayer2Max}, {opLayer2Min}, {opLayer3Max}, {opLayer3Min},
  {opVacuumIndexAStd}, {opVacuumIndexAMax}, {opVacuumIndexAMin}, {opVacuumIndexBStd}, {opVacuumIndexBMax}, {opVacuumIndexBMin},
  {opScreenLayerA}, {opScreenLayerB}, {opScBpAStd}, {opScBpBStd}, {opScBpAMax}, {opScBpBMax}, {opScBpAMin}, {opScBpBMin},
  {opMtrDryTempA}, {opMtrDryTempB}, {opMtrDryTimeA}, {opMtrDryTimeB}, {opMtrMixTimeA}, {opMtrMixTimeB},
  {opRU}, {opRM}, {opRD}, {opRTenStd}, {opRTenMax}, {opRTenMin},
  {opRTenSlopeStd}, {opRTenSlopeMax}, {opRTenSlopeMin}, {opRTenSpeedStd}, {opRTenSpeedMax}, {opRTenSpeedMin},
  {opFeedpAStd}, {opFeedpAMax}, {opFeedpAMin}, {opFeedpBStd}, {opFeedpBMax}, {opFeedpBMin},
  GETDATE(), {登入者ID}, GETDATE(), {登入者ID});
```

SQL-3:

```sql
-- SocBModifyDao.updateSocB
UPDATE SOC_B SET
  AC_1_STD = {opAc1Std}, AC_1_MAX = {opAc1Max}, AC_1_MIN = {opAc1Min}, AC_2_STD = {opAc2Std}, AC_2_MAX = {opAc2Max}, AC_2_MIN = {opAc2Min},
  AC_3_STD = {opAc3Std}, AC_3_MAX = {opAc3Max}, AC_3_MIN = {opAc3Min}, AC_4_STD = {opAc4Std}, AC_4_MAX = {opAc4Max}, AC_4_MIN = {opAc4Min},
  AC_5_STD = {opAc5Std}, AC_5_MAX = {opAc5Max}, AC_5_MIN = {opAc5Min}, AC_6_STD = {opAc6Std}, AC_6_MAX = {opAc6Max}, AC_6_MIN = {opAc6Min},
  AC_7_STD = {opAc7Std}, AC_7_MAX = {opAc7Max}, AC_7_MIN = {opAc7Min}, AC_8_STD = {opAc8Std}, AC_8_MAX = {opAc8Max}, AC_8_MIN = {opAc8Min},
  AC_9_STD = {opAc9Std}, AC_9_MAX = {opAc9Max}, AC_9_MIN = {opAc9Min}, AC_A_STD = {opAcAStd}, AC_A_MAX = {opAcAMax}, AC_A_MIN = {opAcAMin},
  A_SCREW_STD = {opAScrewStd},  A_SCREW_MAX = {opAScrewMax},  A_SCREW_MIN = {opAScrewMin},  A_CURR_STD = {opACurrStd},  A_CURR_MAX = {opACurrMax},  A_CURR_MIN = {opACurrMin},
  A_GP_STD = {opAGpStd},  A_GP_MAX = {opAGpMax},  A_GP_MIN = {opAGpMin},
  BC_1_STD = {opBc1Std}, BC_1_MAX = {opBc1Max}, BC_1_MIN = {opBc1Min}, BC_2_STD = {opBc2Std}, BC_2_MAX = {opBc2Max}, BC_2_MIN = {opBc2Min},
  BC_3_STD = {opBc3Std}, BC_3_MAX = {opBc3Max}, BC_3_MIN = {opBc3Min}, BC_4_STD = {opBc4Std}, BC_4_MAX = {opBc4Max}, BC_4_MIN = {opBc4Min},
  BC_5_STD = {opBc5Std}, BC_5_MAX = {opBc5Max}, BC_5_MIN = {opBc5Min}, BC_6_STD = {opBc6Std}, BC_6_MAX = {opBc6Max}, BC_6_MIN = {opBc6Min},
  BC_7_STD = {opBc7Std}, BC_7_MAX = {opBc7Max}, BC_7_MIN = {opBc7Min}, BC_8_STD = {opBc8Std}, BC_8_MAX = {opBc8Max}, BC_8_MIN = {opBc8Min},
  BC_9_STD = {opBc9Std}, BC_9_MAX = {opBc9Max}, BC_9_MIN = {opBc9Min}, BC_A_STD = {opBcAStd}, BC_A_MAX = {opBcAMax},
  BC_A_MIN = {opBcAMin}, BC_B_STD = {opBcBStd}, BC_B_MAX = {opBcBMax}, BC_B_MIN = {opBcBMin},
  BC_C_STD = {opBcCStd}, BC_C_MAX = {opBcCMax}, BC_C_MIN = {opBcCMin}, BC_D_STD = {opBcDStd}, BC_D_MAX = {opBcDMax}, BC_D_MIN = {opBcDMin},
  BC_E_STD = {opBcEStd}, BC_E_MAX = {opBcEMax}, BC_E_MIN = {opBcEMin}, BC_N_STD = {opBcNStd}, BC_N_MAX = {opBcNMax}, BC_N_MIN = {opBcNMin},
  B_SCREW_STD = {opBScrewStd},  B_SCREW_MAX = {opBScrewMax},  B_SCREW_MIN = {opBScrewMin},  B_CURR_STD = {opBCurrStd},  B_CURR_MAX = {opBCurrMax},  B_CURR_MIN = {opBCurrMin},
  B_GP_STD = {opBGpStd}, B_GP_MAX = {opBGpMax}, B_GP_MIN = {opBGpMin},
  D_1_STD = {opD1Std}, D_1_MAX = {opD1Max}, D_1_MIN = {opD1Min}, D_2_STD = {opD2Std}, D_2_MAX = {opD2Max}, D_2_MIN = {opD2Min},
  D_3_STD = {opD3Std}, D_3_MAX = {opD3Max}, D_3_MIN = {opD3Min}, D_4_STD = {opD4Std}, D_4_MAX = {opD4Max}, D_4_MIN = {opD4Min},
  D_5_STD = {opD5Std}, D_5_MAX = {opD5Max}, D_5_MIN = {opD5Min}, D_6_STD = {opD6Std}, D_6_MAX = {opD6Max}, D_6_MIN = {opD6Min},
  D_7_STD = {opD7Std}, D_7_MAX = {opD7Max}, D_7_MIN = {opD7Min},
  R_1_STD = {opR1Std}, R_1_MAX = {opR1Max}, R_1_MIN = {opR1Min}, R_2_STD = {opR2Std}, R_2_MAX = {opR2Max}, R_2_MIN = {opR2Min},
  R_3_STD = {opR3Std}, R_3_MAX = {opR3Max}, R_3_MIN = {opR3Min},
  R_SPEED_STD = {opRSpeedStd}, R_SPEED_MAX = {opRSpeedMax}, R_SPEED_MIN = {opRSpeedMin}, R_GAP_L = {opRGapL}, R_GAP_R = {opRGapR},
  VACUUM_INDEX = {opVacuumIndex}, LAYER_1 = {opLayer1}, LAYER_2 = {opLayer2}, LAYER_3 = {opLayer3}, MODEL_GAP = {opModelGap}, MODEL_WIDTH = {opModelWidth},
  MFG_DESC = {opMfgDesc}, SOC_DESC = {opSocDesc}, REMARK = {opRemark}, SCREEN_FREQ = {opScreenFreq},  RECORD_FREQ = {opRecordFreq},
  LAYER_1_MAX = {opLayer1Max}, LAYER_1_MIN = {opLayer1Min}, LAYER_2_MAX = {opLayer2Max}, LAYER_2_MIN = {opLayer2Min}, LAYER_3_MAX = {opLayer3Max}, LAYER_3_MIN = {opLayer3Min},
  VACUUM_INDEX_A_STD = {opVacuumIndexAStd}, VACUUM_INDEX_A_MAX = {opVacuumIndexAMax}, VACUUM_INDEX_A_MIN = {opVacuumIndexAMin},
  VACUUM_INDEX_B_STD = {opVacuumIndexBStd}, VACUUM_INDEX_B_MAX = {opVacuumIndexBMax}, VACUUM_INDEX_B_MIN = {opVacuumIndexBMin},
  SCREEN_LAYER_A = {opScreenLayerA}, SCREEN_LAYER_B = {opScreenLayerB}, SC_BP_A_STD = {opScBpAStd}, SC_BP_B_STD = {opScBpBStd}, SC_BP_A_MAX = {opScBpAMax}, SC_BP_B_MAX = {opScBpBMax},
  SC_BP_A_MIN = {opScBpAMin}, SC_BP_B_MIN = {opScBpBMin},
  MTR_DRY_TEMP_A = {opMtrDryTempA}, MTR_DRY_TEMP_B = {opMtrDryTempB}, MTR_DRY_TIME_A = {opMtrDryTimeA}, MTR_DRY_TIME_B = {opMtrDryTimeB},
  MTR_MIX_TIME_A = {opMtrMixTimeA}, MTR_MIX_TIME_B = {opMtrMixTimeB},
  R_U = {opRU}, R_M = {opRM}, R_D = {opRD}, R_TEN_STD = {opRTenStd}, R_TEN_MAX = {opRTenMax}, R_TEN_MIN = {opRTenMin},
  R_TEN_SLOPE_STD = {opRTenSlopeStd}, R_TEN_SLOPE_MAX = {opRTenSlopeMax}, R_TEN_SLOPE_MIN = {opRTenSlopeMin},
  R_TEN_SPEED_STD = {opRTenSpeedStd}, R_TEN_SPEED_MAX = {opRTenSpeedMax}, R_TEN_SPEED_MIN = {opRTenSpeedMin},
  FEEDP_A_STD = {opFeedpAStd}, FEEDP_A_MAX = {opFeedpAMax}, FEEDP_A_MIN = {opFeedpAMin}, FEEDP_B_STD = {opFeedpBStd}, FEEDP_B_MAX = {opFeedpBMax}, FEEDP_B_MIN = {opFeedpBMin},
  UDT = GETDATE(), UPDATE_BY = {登入者ID}
  WHERE SOC_B_UID = {opSocBUid}
```
SQL-LOG:

```sql
INSERT INTO SYS_MODI_LOG (CATEGORY, DATAKEY, USERID, USER_NAME, LOG) VALUES ('SOC_B', { opSocBUid }, {登入者ID}, {登入者名稱}, '新增(修改)');
```

# Response 欄位

* 無

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":{
       "opSocBUid": "6A135E88-190D-4C61-861A-969EFFA37DD6"
  }
}
```