# 客訴-取得案件新增編輯預設資料
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得案件新增編輯預設資料


## 修改歷程

| 修改時間   | 內容                                     | 修改者 |
|------------|----------------------------------------|--------|
| 20230712   | 新增規格                                 | Nick   |
| 20230801   | opCommData、opDocData、opDiagList 內容調整 | Nick   |
| 20230803   | 新增 檔案讀取異常給 null 不跳錯誤        | Nick   |
| 20230809   | 新增 品保報告更新日期、品保報告更新人員   | Nick   |
| 20230809-1 | 新增 附件 上傳者使用者 ID(opCreateBy)    | Nick   |
| 20230809-2 | 新增 案件類別說明(opClaimDesc)           | Nick   |
| 20230811   | 調整規格說明，加強對附件檔名解說          | Nick   |


## 來源URL及資料格式

| 項目   | 說明            |
|--------|-----------------|
| URL    | /cs/cs_edit_pre |
| method | post            |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位    | 名稱       | 資料型別 | 必填 | 資料儲存 & 說明                 |
|---------|----------|:--------:|:----:|------------------------------|
| opIsNew | 是否為新增 |  string  |  M   | `Y`:新增 `N`:編輯/結案/終報回覆 |
| opCsUid | 客訴案件ID |  string  |  O   | opIsNew 為 N 必填               |

<!--
原先有這兩個參數，隱藏不用，因為Y或N 是由使用者ID決定，目前看起來是無意義會被複寫
| opEditReply| 可否進行終報回覆 |string|M||
| opEditClose| 可否進行結案 |string|M||

風險度 csGrade (OK)
不良現象dfA  (API) AjaxGetCsDfList
次次次 dfB dfC dfD (API) AjaxGetCsDfList
抱怨數量(單位) unit (OK)
樣本到廠 sampleDFlag (OK) SAMPLE_D_FLAG
要求換貨 csRtFlag (OK) CSRTFLAG
賠償要求 csRefundFlag (OK) REFUND_FLAG
賠償金額(幣別)  csRefundCurrency (OK)
製造單位(opProdunitList) 生產機台(API)
附件(檔案類別) docFlag (OK)
真因分析(終判) finalDiagFlag (OK) CS_DIAG_FLAG

-->


#### Request 範例
```json
{
  "opCsUid":null,
  "opIsNew":"Y"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 自定義變數(可否編輯申請單)  {editApply} 預設 "N" 
* 自定義變數(可否編輯真因)    {editDiag}  預設 "N" 
* 自定義變數(其餘區塊是否唯讀){readOnly}  預設 "Y" 
* 自定義變數(可否寫品保報告)  {editReport} 預設 "N" 
* 自定義變數(可否進行終報回覆){editReply } 預設 "N" 
* 自定義變數(可否進行結案)    {editClose } 預設 "N" 
* 自定義變數(是否為CS角色)    {editAsCs }  預設 "N" 
* 如為
  * 新增案件(opIsNew=Y)
    * 將 opCsData 內 {opOrganizationId} 複寫為 {登入者工廠別} ，其餘opCsData內節點皆為 null。  
    * {editApply}="Y"
    * {readOnly}="N"
    * {editReply}="N"
    * {editClose}="N"
  * 非新增案件(opIsNew=N)
    * 執行 SQL-1 抓取客訴主檔資料取回 result1 
    * 將 result1 各欄位指定為 【opCsData】、【opCsProgress】 各節點及 opDiagData.opFinalDiagFlag、root.opCsNo、root.opClaimFlag、root.opInsId 內容值 
    * 如 result1.Claim_Flag 為 NULL，則 opClaimText 設為'N/A'，如為 'Y' 設為 '客訴案件'，否則設為 '客戶反應'
    * 如 result1.ACTIVE_DATE 為 NULL，opCsProgress.opRegistration 設為 '-'，否則設為 result1.ACTIVE_DATE(取 yyyy/mm/dd)
    * 如 result1.STATUS 為 '4'，則 opCsProgress.opDispatch 設為 '進行中' ，反之，如 result1.UDT_B 為 NULL，設為 '-'，否則設為 result1.UDT_B(取 yyyy/mm/dd)
    * 如 result1.STATUS 為 '5'，則 opCsProgress.opInitAnalyze 設為 '進行中'，反之，如 result1.UDT_C 為 NULL，設為 '-'，否則設為 result1.UDT_C(取 yyyy/mm/dd)
    * 如 result1.FIRST_REPORT_DATE 非 NULL ，則 opCsProgress.opInitResponse 設為 result1.FIRST_REPORT_DATE(取 yyyy/mm/dd)，反之，如果 result1.FIRST_REPORT_FLAG 為 NULL、''或 'N'，設為 '不需回覆'，否則設為'尚未回覆'
    * 如 result1.STATUS 為 'A'，則 opCsProgress.opSolution 設為 '進行中'，反之，如 result1.UDT_D 為 NULL，設為 '-'，否則設為 result1.UDT_D(取 yyyy/mm/dd)
    * 如 result1.STATUS 為 'B'，則 opCsProgress.opCompilation 設為 '進行中'，反之，如 result1.UDT_E 為 NULL，設為 '-'，否則設為 result1.UDT_E(取 yyyy/mm/dd)
    * 如 result1.STATUS 為 'C'，則 opCsProgress.opReview 設為 '進行中'，反之，如 result1.UDT_F 為 NULL，設為 '-'，否則設為 result1.UDT_F(取 yyyy/mm/dd)
    * 如 result1.STATUS 為 'D'，則 opCsProgress.opReply 設為 '進行中'，反之，如 result1.CS_REPORT_DATE 為 NULL，設為 '-'，否則設為 result1.CS_REPORT_DATE(取 yyyy/mm/dd)
    * 如 result1.STATUS 為 'E'，則 opCsProgress.opClose 設為 '進行中'，反之，如 result1.UDT_Z 為 NULL，設為 '-'，否則設為 result1.UDT_Z(取 yyyy/mm/dd)
    * 如 尚未結案 （result1.STATUS 為"E" 或是 result1.STATUS 不為 "E" 且 result1.UDT_Z 為 null)，則計算方式為 "當下時間" {取系統 CurrentTime} 減去 "立案時間" result1.ACTIVE_DATE，反之，則計算方式為 "結案時間" result1.UDT_Z 減去 "立案時間" result1.ACTIVE_DATE

    * 自定義變數 {ROLE_ID} (CS角色ID) ，如 {登入者工廠別} 為 "5000" ，則指定為 "921"，否則為 "922"   
    * 執行 SQL-2 確認使用者是否為 CS 角色，並依結果覆寫 {editAsCs} 為 "Y"(有找到資料) or "N"(找不到)
    * 如 ( `(result1.SalesUserId ={登入者使用者ID})` || `{editAsCs}='Y'`  )  && `result1.Status < "Z"` ，則 {editApply}="Y"
    * 如  (`result1.Status < "Z"` && ((`result1.ReportUserId !=null` && `result1.ReportUserId = {登入者使用者ID}` ) || (`result1.UunB !=null` && `result1.UunB() = {登者使用者名稱}` )) )，則 {editDiag}="Y"
    * 如 (`result1.Status != 'Z'`)，則 {readOnly}="N" 
    * 如 ((`result1.Status 狀態為 "5" or "A" or "B" or "C"`) && (`result1.ReportUserId = {登入者使用者ID}`) || (`result1.UunB !=null` && `result1.UunB() = {登入者使用名稱}` )))，則 editReport="Y"
	  * 如 (`editReply=null`)，則 editReply="N"，反之，如 (`result1.Status="D"`) && (`result1.SalesUserId ={登入者使用者ID}` || `{editAsCs}='Y'` )， 則 editReply="Y"
	  * 如 (`editClose=null`)，則 editClose="N"，反之，如 (`result1.Status="E"`) && (`result1.ReportUserId = {登入者使用者ID}`  )，則 editClose="Y"
  * 執行 SQL-3 取得附件清單 result3 ，並將各欄位資料指定為 opDocData 內容，
    * opContent 內容如下:
    * {date} 取 result3.CDT 欄位日期 ex.202307
    * {result3.CS_DOC_UID}為實體路徑的【檔案名稱】
    * {opContent} = 讀取檔案 cs.file.root/{date}/{result3.CS_DOC_UID}，轉成 base64 字串，檔案讀取異常給 null 不跳錯誤。
  * 執行 SQL-4 取得客訴處理說明清單，並將各欄位資料指定為 opCommData 內容，如SQL無資料可取得 opCommData 給 null。
  * 執行 SQL-5 取得製造紀錄清單，並將各欄位資料指定為 opLotData 內容，如SQL無資料可取得 opLotData 給 null。
  * 執行 SQL-6 取得真因分析清單 ，並將各欄位資料指定為  opDiagList 內容，如SQL無資料可取得 opDiagList 給 null。
  * 執行 SQL-7 取得真因分類清單 ，並將各欄位資料指定為 opDiagFlagList 內容，如SQL無資料可取得 opDiagFlagList 給 null。
  <!--* 執行 SQL-8 取得生產單位選項 ，並將各欄位資料指定為 opProdunitList  內容，如SQL無資料可取得 opProdunitList 給 null。  (改用API)-->

* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
--CsQueryDao.getCsSingle(csUid)
SELECT 
    C.CS_UID,
    C.PATTERN_NO,
    C.GRADE_NO,
    C.UNIT,
    C.CS_SUBJECT,
    C.CS_NO,
    C.ORGANIZATION_ID,
    C.WF_INS_ID,
    C.CS_ITEM_TYPE,
    C.ACTIVE_FLAG,
    C.STATUS,
    C.CLAIM_FLAG,
    C.CLAIM_DESC,
    C.CS_GRADE,
    C.CUST_NAME,
    C.CUST_NO,
    C.DEST_CUST_NAME,
    CONVERT(VARCHAR(10), C.CS_ISSUE_DATE, 111) AS CS_ISSUE_DATE,
    C.CS_ITEM_NO,
    C.DF_A,
    C.DF_B,
    C.DF_C,
    C.DF_D,
    C.CS_REMARK,
    C.CS_DF_QTY,
    C.CS_USED_QTY,
    C.CS_NON_QTY,
    C.CS_ITEM_LOCATION,
    C.CS_RT_FLAG,
    C.CS_RT_QTY,
    C.CS_REFUND_FLAG,
    C.CS_REFUND_DESC,
    C.CS_REFUND_AMOUNT,
    C.CS_REFUND_CURRENCY,
    C.SALES_ORDER_NO,
    C.SALES_QTY,
    C.CS_SHIP_QTY,
    C.SAMPLE_D_FLAG,
    CONVERT(VARCHAR(10), C.ACTIVE_DATE, 111) AS ACTIVE_DATE,
    CONVERT(VARCHAR(10), C.CS_REPORT_DATE, 111) AS CS_REPORT_DATE,
    C.CS_CLOSE_REPORT,
    C.FINAL_DIAG_FLAG,
    C.FIRST_REPORT_FLAG,
    CONVERT(VARCHAR(10), C.FIRST_REPORT_DATE, 111) AS FIRST_REPORT_DATE,
    CONVERT(VARCHAR(10),C.UDT_E,111) AS UDT_E, --品保更新時間
    C.UUN_E --品保更新人員
FROM CS_H C
WHERE C.CS_UID = { opCsUid }
```

SQL-2:
```sql
--sysUserDao.checkUserInRole(curUser.getUserId(), roleId);
SELECT COUNT(*) AS CNT
FROM SYS_USER_ROLE_V3
WHERE USER_ID = { 登入者使用者ID }
    AND ROLE_ID = { ROLE_ID }

```

SQL-3:
```sql
--dao.getCsDocList(csUid)
SELECT D.CS_UID,
    D.CS_DOC_UID,
    D.DOC_FLAG,
    D.DOC_NAME,
    D.DOC_TYPE,
    REPLACE(CONVERT(VARCHAR(19), D.CDT, 120), '-', '/') CDT,
    D.CREATE_BY,
    D.ACTIVE_FLAG,
    U.USER_NAME CREATE_BY_NAME
FROM CS_DOC D
    LEFT JOIN SYS_USER U ON D.CREATE_BY = U.USER_ID
WHERE D.CS_UID = { opCsUid }
```

SQL-4:
```sql
--dao.getCsCommentList(csUid);
SELECT C.CS_UID,
    C.CS_COMMENT_SEQ,
    C.CS_COMMENT,
    REPLACE(CONVERT(VARCHAR(19), C.UDT, 120), '-', '/') UDT,
    U.USER_NAME 
FROM CS_COMMENT C
    LEFT JOIN SYS_USER U ON C.UPDATE_BY = U.USER_ID
WHERE C.CS_UID = { opCsUid }
```

SQL-5:
```sql
--dao.getCsLotList(csUid);
SELECT L.CS_UID,
    L.CS_LOT_SEQ,
    L.CS_LOT_NO,
    L.PRODUNIT_NO,
    L.EQUIP_ID,
    L.CS_LOT_QTY,
    CONVERT(VARCHAR(10), L.MFG_DATE, 111) AS MFG_DATE,
    REPLACE(CONVERT(VARCHAR(19), L.CDT, 120), '-', '/') CDT,
    L.CREATE_BY,
    U.USER_NAME CREATE_BY_NAME,
    E.EQUIP_NAME,
    P.PRODUNIT_NAME
FROM CS_LOT L
    LEFT JOIN SYS_USER U ON L.CREATE_BY = U.USER_ID
    LEFT JOIN PRODUNIT P ON L.ORGANIZATION_ID = P.ORGANIZATION_ID
    AND L.PRODUNIT_NO = P.PRODUNIT_NO
    LEFT JOIN EQUIP_H E ON L.EQUIP_ID = E.EQUIP_ID
WHERE L.CS_UID = { opCsUid }
```

SQL-6:
```sql
--dao.getCsDiagList(csUid);
SELECT D.CS_UID,
    D.CS_DIAG_SEQ,
    D.DF_CODE,
    D.DIAG_FLAG,
    D.DIAG_REMARK,
    REPLACE(CONVERT(VARCHAR(19), D.UDT, 120), '-', '/') UDT,
    U.USER_NAME UPDATE_BY_NAME
FROM CS_DIAG D
    LEFT JOIN SYS_USER U ON D.UPDATE_BY = U.USER_ID
WHERE D.CS_UID = { opCsUid }
```

SQL-7:
```sql
--dao.getCsDiagFlagList();
SELECT PARA_VALUE DIAG_FLAG,
    PARA_TEXT DIAG_FLAG_DESC
FROM SYS_PARA_D
WHERE PARA_ID = 'CS_DIAG'
ORDER BY PARA_VALUE
```

<!--
(API 取代)
SQL-8:
```sql
--produnitDao.getAllProdunitList();
SELECT PRODUNIT_NO,PRODUNIT_NAME
FROM PRODUNIT
ORDER BY PRODUNIT_NAME
```
-->



# Response 欄位

| 欄位           | 名稱             | 資料型別 | 資料儲存 & 說明                   |
|----------------|----------------|:--------:|:----------------------------------|
| opReadOnly     | 是否唯讀         |  string  | Y/N                               |
| opEditApply    | 是否編輯申請單   |  string  | Y/N                               |
| opEditDiag     | 是否編輯真因     |  string  | Y/N                               |
| opEditReply    | 是否進行終報回覆 |  string  | Y/N                               |
| opEditClose    | 是否進行結案     |  string  | Y/N                               |
| opEditReport   | 是否編輯品保報告 |  string  | Y/N                               |
| opEditAsCs     | 是否為CS 角色    |  string  | Y/N                               |
| opCsNo         | 案號             |  string  | C.CS_NO(SQL-1) (頁面最上面顯示用) |
| opClaimFlag    | 案件類別(cbo)    |  string  | C.CLAIM_FLAG(SQL-1)               |
| opInsId        | WF表單ID         |  string  | C.WF_INS_ID(SQL-1)                |
| opClaimText    | 案件類別文字     |  string  | (頁面最上面顯示用)                |
| opCsProgress   | 單一客訴狀態進程 |  object  | (頁面最上面顯示用)                |
| opCsData       | 客訴內容         |  object  |                                   |
| opLotData      | 製造記錄         |  array   |                                   |
| opCommData     | 客訴處理說明     |  array   |                                   |
| opDocData      | 附件列表         |  array   |                                   |
| opDiagData     | 真因分析與終判   |  object  |                                   |
| opDiagFlagList | 真因選項清單     |  array   |                                   |
| opClaimDesc    | 案件類別說明    |  string  | C.CLAIM_DESC(SQL-1)               |
<!-- | opProdunitList | 機台清單         |  array   |                                   | (改用API)-->

### 【opCsProgress】object
| 欄位               | 名稱     | 資料型別 | 必填 | 資料儲存 & 說明                                                      |
|--------------------|--------|----------|------|------------------------------------------------------------------|
| opRegistration     | 立案     | string   | M    | ""/ACTIVE_DATE(SQL-1)       時間格式: yyyy/mm/dd                     |
| opDispatch         | 指派成員 | string   | M    | "進行中"/"-"/UDT_B(SQL-1)  時間格式: yyyy/mm/dd                      |
| opInitAnalyze      | 初步分析 | string   | M    | "進行中"/"-"/UDT_C(SQL-1)  時間格式: yyyy/mm/dd                      |
| opInitResponse     | 初報回覆 | string   | M    | "不需回覆"/"尚未回覆"/FIRST_REPORT_DATE(SQL-1)  時間格式: yyyy/mm/dd |
| opSolution         | 真因對策 | string   | M    | "進行中"/"-"/UDT_D(SQL-1)  時間格式: yyyy/mm/dd                      |
| opCompilation      | 終報彙整 | string   | M    | "進行中"/"-"/UDT_E(SQL-1)  時間格式: yyyy/mm/dd                      |
| opReview           | 終報審核 | string   | M    | "進行中"/"-"/UDT_F(SQL-1)  時間格式: yyyy/mm/dd                      |
| opReply            | 回覆客戶 | string   | M    | "進行中"/"-"/CS_REPORT_DATE(SQL-1) 時間格式: yyyy/mm/dd              |
| opClose            | 結案     | string   | M    | "進行中"/"-"/UDT_Z(SQL-1) 時間格式: yyyy/mm/dd                       |
| opProcessingPeriod | 處理時間 | integer  | M    | (當下時間 or 結案時間)-立案時間 (單位為天數)                         |


### 【opCsData】object

| 欄位               | 名稱              | 資料型別 | 資料儲存 & 說明                            |
|--------------------|-------------------|:--------:|--------------------------------------------|
| opOrganizationId   | 工廠別            |  string  | 前端用不到填充用  C.ORGANIZATION_ID(SQL-1) |
| opCsUid            | uid               |  string  | C.CS_UID(SQL-1)                            |
| opCsSubject        | 主旨說明          |  string  | C.CS_SUBJECT(SQL-1)                        |
| opCustNo           | 客戶代號          |  string  | C.CUST_NO(SQL-1)                           |
| opCustName         | 客戶名稱          |  string  | C.CUST_NAME(SQL-1)                         |
| opDestCustName     | 終端客戶          |  string  | C.DEST_CUST_NAME(SQL-1)                    |
| opCsGrade          | 風險度(cbo)       |  string  | C.CS_GRADE(SQL-1)                          |
| opPatternNo        | 廠內色號(cbo)     |  string  | C.PATTERN_NO(SQL-1)                        |
| opGradeNo          | 規格/Grade        |  string  | C.GRADE_NO(SQL-1)                          |
| opCsItemType       | 產品別(cbo)       |  string  | C.CS_ITEM_TYPE(SQL-1)                      |
| opDfa              | 不良現象(cbo)     |  string  | C.DF_A(SQL-1)                              |
| opDfb              | 次(cbo)           |  string  | 次(第一個下拉) C.DF_B(SQL-1)               |
| opDfc              | 次(cbo)           |  string  | 次(第二個下拉) C.DF_C(SQL-1)               |
| opDfd              | 次(cbo)           |  string  | 次(第三個下拉) C.DF_D(SQL-1)               |
| opCsDfQty          | 抱怨數量          |  string  | C.CS_DF_QTY(SQL-1)                         |
| opUnit             | 抱怨數量單位(cbo) |  string  | C.UNIT(SQL-1)                              |
| opCsUsedQty        | 已用量            |  string  | C.CS_USED_QTY(SQL-1)                       |
| opCsNonQty         | 未用量            |  string  | C.CS_NON_QTY(SQL-1)                        |
| opSalesOrderNo     | 銷售訂單          |  string  | C.SALES_ORDER_NO(SQL-1)                    |
| opSalesQty         | 訂購量            |  string  | C.SALES_QTY(SQL-1)                         |
| opCsShipQty        | 已出貨量          |  string  | C.CS_SHIP_QTY(SQL-1)                       |
| opCsIssueDate      | 抱怨日            |  string  | C.CS_ISSUE_DATE(SQL-1)     yyyy/mm/dd      |
| opCsItemLocation   | 產品所在地        |  string  | C.CS_ITEM_LOCATION(SQL-1)                  |
| opCsItemNo         | 客戶料號          |  string  | C.CS_ITEM_NO(SQL-1)                        |
| opSampleDFlag      | 樣本到廠(cbo)     |  string  | C.SAMPLE_D_FLAG(SQL-1)                     |
| opCsRemark         | 客訴細節描述      |  string  | C.CS_REMARK(SQL-1)                         |
| opCsRtFlag         | 要求換貨          |  string  | C.CS_RT_FLAG(SQL-1)                        |
| opCsRtQty          | 換貨量            |  string  | C.CS_RT_QTY(SQL-1)                         |
| opCsRefundFlag     | 賠償要求(cbo)     |  string  | C.CS_REFUND_FLAG(SQL-1)                    |
| opFirstReportFlag  | 需要初報          |  string  | C.FIRST_REPORT_FLAG(SQL-1)                 |
| opCsRefundCurrency | 賠償金額(cbo)     |  string  | 幣別   C.CS_REFUND_CURRENCY(SQL-1)         |
| opCsRefundAmount   | 賠償金額          |  string  | 金額   C.CS_REFUND_AMOUNT(SQL-1)           |
| opCsRefundDesc     | 賠償金額          |  string  | 備註   C.CS_REFUND_DESC(SQL-1)             |
| opFirstReportDate  | 初報回覆          |  string  | C.FIRST_REPORT_DATE(SQL-1)                 |
| opCsReportDate     | 終報回覆          |  string  | C.CS_REPORT_DATE(SQL-1)                    |
| opCsCloseReport    | 品保報告          |  string  | C.CS_CLOSE_REPORT(SQL-1)                   |
| opUpdateDate       | 品保報告更新日期  |  string  | UDT_E (SQL-1)                              |
| opUpdatePerson     | 品保報告更新人員  |  string  | UUN_E (SQL-1)                              |


### 【opLotData】array
| 欄位         | 名稱         | 資料型別 | 資料儲存 & 說明      |
|--------------|--------------|----------|----------------------|
| opCsLotNo    | Lot No.      | string   | L.CS_LOT_NO(SQL-5)   |
| opCsLotSeq   | id           | string   | L.CS_LOT_SEQ(SQL-5)  |
| opProdUnitNo | 生產單位代碼(cbo) | string   | L.PRODUNIT_NO(SQL-5) |
| opEquipId    | 生產機台(cbo)     | string   | L.EQUIP_ID(SQL-5)    |
| opCsLotQty   | 生產數量     | string   | L.CS_LOT_QTY(SQL-5)  |
| opMfgDate    | 生產日期     | string   | MFG_DATE(SQL-5)      |
| opStatus     | 狀態         | string   | 固定 NULL            |

### 【opCommData】array 
| 欄位                | 名稱         | 資料型別 | 資料儲存 & 說明                            |
|---------------------|------------|----------|--------------------------------------------|
| opCsComment         | 客訴處理說明 | string   | C.CS_COMMENT(SQL-4)                        |
| opCsCommentSeq      | 序號         | string   | C.CS_COMMENT_SEQ(SQL-4)                    |
| opCsCommentUserName | 人員         | string   | U.USER_NAME(SQL-4)                         |
| opCsCommentDate     | 日期         | string   | UDT(SQL-4)，預設 `null`，YYYY/MM/DD hh:mm:ss |

### 【opDocData】array 
| 欄位           | 名稱            | 資料型別 | 資料儲存 & 說明                            |
|----------------|-----------------|----------|--------------------------------------------|
| opCsDocUid     | uid             | string   | D.CS_DOC_UID(SQL-3)                        |
| opDocFlag      | 檔案類別        | string   | D.DOC_FLAG (SQL-3)                         |
| opDocName      | 檔案名稱        | string   | D.DOC_NAME (SQL-3)                         |
| opDocType      | 檔案類型        | string   | D.DOC_TYPE (SQL-3)                         |
| opActiveFlag   | 是否生效        | string   | D.ACTIVE_FLAG (SQL-3)                      |
| opStatus       | 狀態            | string   | 固定 null                                  |
| opContent      | 檔案內容        | string   | base64 encoded string                      |
| opCreateByName | 上傳者/人員     | string   | CREATE_BY_NAME(SQL-3)                      |
| opCDT          | 上傳日期/日期   | string   | CDT(SQL-3)，預設 `null`，YYYY/MM/DD hh:mm:ss |
| opCreateBy     | 上傳者使用者 ID | string   | CREATE_BY                                  |

### 【opDiagData】object
| 欄位            | 名稱      | 資料型別 | 資料儲存 & 說明          |
|-----------------|---------|----------|--------------------------|
| opFinalDiagFlag | 終判(cbo) | string   | C.FINAL_DIAG_FLAG(SQL-1) |
| opDiagList      | 列表資料  | array    |                          |

### 【opDiagList】array 
| 欄位           | 名稱          | 資料型別 | 資料儲存 & 說明                            |
|----------------|---------------|----------|--------------------------------------------|
| opCsDiagSeq    | id            | string   | D.CS_DIAG_SEQ(SQL-6)                       |
| opDfCode       | 不良現象(cbo) | string   | D.DF_CODE(SQL-6)                           |
| opDiagFlag     | 真因分類(cbo) | string   | D.DIAG_FLAG(SQL-6)                         |
| opDiagRemark   | 真因描述      | string   | D.DIAG_REMARK(SQL-6)                       |
| opStatus       | 狀態          | string   | 固定 NULL                                  |
| opUpdateByName | 更新人員      | string   | UPDATE_BY_NAME(SQL-6)                      |
| opUDT          | 日期          | string   | UDT(SQL-6)，預設 `null`，YYYY/MM/DD hh:mm:ss |

<!--
(改用API)
#### 【opProdunitList】array //SQL
| 欄位           | 名稱         | 資料型別 | 來源資料 & 說明      |
|----------------|------------|----------|----------------------|
| opProdunitNo   | 生產單位代碼 | string   | PRODUNIT_NO(SQL-8)   |
| opProdunitName | 生產單位名稱 | string   | PRODUNIT_NAME(SQL-8) |
-->

### 【 opDiagFlagList 】array
| 欄位           | 名稱         | 資料型別 | 資料儲存 & 說明       |
|----------------|------------|----------|-----------------------|
| opDiagFlag     | 真因分類     | string   | DIAG_FLAG(SQL-7)      |
| opDiagFlagDesc | 真因分類說明 | string   | DIAG_FLAG_DESC(SQL-7) |


#### Response 範例
``` json
{
   "opReadOnly":"N",
   "opEditApply":"Y",
   "opEditDiag":"Y",
   "opEditReply":"Y",
   "opEditClose":"Y",
   "opEditReport":"Y",
   "opEditAsCs":"N",
   "opCsNo":"C2209003",
   "opClaimFlag":"Y",
   "opClaimText":"客戶案件",
   "opCsProgress":"",
   "opCsData":{
      "opOrganizationId":"5000",
      "opCsUid":"183AAEA7-6598-415B-8D52-3E8A6D710CE2",
      "opCsSubject":"J20609C6長支客訴",
      "opCustNo":"C2209003",
      "opCustName":"奇美實業",
      "opDestCustName":"亞旭",
      "opCsGrade":"2",
      "opPatternNo":"J20609C6",
      "opGradeNo":"PC-6700",
      "opCsItemType":"粒材",
      "opDfa":"A07",
      "opDfb":null,
      "opDfc":null,
      "opDfd":null,
      "opCsDfQty":null,
      "opUnit":null,
      "opCsUsedQty":null,
      "opCsNonQty":null,
      "opSalesOrderNo":"3000 KG",
      "opSalesQty":null,
      "opCsShipQty":null,
      "opCsIssueDate":"2023/07/18",
      "opCsItemLocation":null,
      "opCsItemNo":"PC-6700",
      "opSampleDFlag":null,
      "opCsRemark":"客戶反應PC-6700 J20609C6有長枝(22T028251) 暫將長支去除後繼續使用物料~客戶觀感不佳~請防止再發!",
      "opCsRtFlag":null,
      "opCsRtQty":null,
      "opCsRefundFlag":null,
      "opFirstReportFlag":null,
      "opCsRefundCurrency":null,
      "opCsRefundAmount":null,
      "opCsRefundDesc":null,
      "opFirstReportDate":"2022/09/26",
      "opCsReportDate":"2023/01/09",
      "opCsCloseReport":null
   },
   "opCsProgress":{
      "opRegistration":"2015/03/03",
      "opDispatch":"2015/03/04",
      "opInitAnalyze":"2015/03/05",
      "opInitResponse":"2015/03/06",
      "opSolution":"2015/03/12",
      "opCompilation":"2015/03/18",
      "opReview":"2015/03/19",
      "opReply":"2015/03/18",
      "opClose":"進行中",
      "opProcessingPeriod":19
   },
   "opLotData":[
      {
         "opCsLotNo":"22T028251",
         "opCsLotSeq":"1",
         "opProdUnitNo":"P3",
         "opEquipId":"2",
         "opCsLotQty":"3014",
         "opMfgDate":"2022/08/25",
         "opStatus":"M"
      }
   ],
   "opCommData":[
      {
         "opCsComment":"新增客訴",
         "opCsCommentSeq":"1",
         "opCsCommentUserName":"周鉦斌",
         "opUDT":"2015/03/13 17:03:01"
      }
   ],
   "opDocData":[
      {
         "opCsDocUid":null,
         "opDocFlag":"終報",
         "opDocName":"PC-6700 J20609C6 長枝客訴報告(終報).pptx",
         "opDocType":"application/vnd.openxmlformats-officedocument.presentationml.presentation",
         "opActiveFlag":"Y",
         "opStatus":"N",
         "opContent":"base64 encoded string",
         "opCreateByName":"周鉦斌",
         "opCDT":"2015/03/18 10:12:41",
         "opCreateBy": "tpidev2"
      }
   ],
   "opDiagData":{
      "opFinalDiagFlag":"L",
      "opDiagList":[
         {
            "opCsDiagSeq":"1",
            "opDfCode":"A07",
            "opDiagFlag":"A01",
            "opDiagRemark":"人員未依抽包驗色SOP，人員未依首件取樣SOP",
            "opStatus":"M",
            "opUpdateByName":"周鉦斌",
            "opUDT":"2015/03/13 09:48:08"
         }
      ]
   },
   "opDiagFlagList":[
      {
         "opDiagFlag":"A01",
         "opDiagFlagDesc":"人-未遵守SOP生產"
      },
      {
         "opDiagFlag":"A02",
         "opDiagFlagDesc":"人-訓練不足"
      },
      {
         "opDiagFlag":"A03",
         "opDiagFlagDesc":"人-抽樣不確實"
      }
   ]
}
```

## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考

* CsManageAction.csEditPre