# SOC-寄SOC發行通知
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容                                             | 修改者 |
|----------|------------------------------------------------|--------|
| 20231108 | 新增規格                                         | Ellen  |
| 20231128 | 副本收件者，使用者信箱如果是空白或null，忽略使用者 | Nick   |



## 來源URL及資料格式

| 項目   | 說明                    |
| ------ | ----------------------- |
| URL    | /soc/send_public_notice |
| method | post                    |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位       | 名稱              | 資料型別 | 必填 | 資料儲存 & 說明                 |
| ---------- | ----------------- | :------: | :--: | ------------------------------- |
| opSocUid   | soc UID           |  string  |  M   |                                 |
| opProdType | 產品型態          |  string  |  M   | C:染色 / M:複材                 |
| opRoleId   | email通知角色代號 |  string  |  O   | 904:奇菱 / 914:菱翔  預設:`904` |



#### Request 範例

```json
{
  "opSocUid": "9745F4FAFDEAEC9CE050007F01002131",
  "opProdType": "C",
  "opRoleId": "904"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 依opProdType取得SOC資訊
  * 若opProdType等於C，執行SQL-1，取得染色SOC資訊，變數{socType}等於"染色SOC"
  * 若opProdType等於M，執行SQL-2，取得複材SOC資訊，變數{socType}等於"複材SOC"
  * 若opProdType不等於C或M [return 400,VALIDATION_ERROR]
* 執行SQL-3，取得通知email
* 取Eamil模板檔 SOC-NOTICE.ftl 參考下方email模板檔輸入參數，將資料帶入並發信。
* 發信：
    * 收件者：SQL-3 的 EMAIL
    * 副本收件者：取登入者的 SYS_USER.EMAIL(信箱是空白或 null，則略過該使用者)
    * 信件標題：[OPMS]{socType}發行通知:{ITEM_NO(SQL-1)}-{EQUIP_NAME(SQL-2)} by {PUBLIC_NAME(SQL-1)}
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200。

# Request 後端邏輯說明

SQL-1: 取得染色SOC資訊
```sql
SELECT
	E.EQUIP_NAME,
	S.ITEM_NO,
	U.USER_NAME PUBLIC_NAME 
FROM
	SOC_C S
	INNER JOIN EQUIP_H E ON S.EQUIP_ID= E.EQUIP_ID
	LEFT JOIN SYS_USER U ON S.PUBLIC_BY= U.USER_ID 
WHERE
	S.SOC_C_UID = {opSocUid}
```

SQL-1: 取得複材SOC資訊
```sql
SELECT
	E.EQUIP_NAME,
	S.ITEM_NO,
	U.USER_NAME PUBLIC_NAME 
FROM
	SOC_M S
	INNER JOIN EQUIP_H E ON S.EQUIP_ID= E.EQUIP_ID
	LEFT JOIN SYS_USER U ON S.PUBLIC_BY= U.USER_ID 
WHERE
	S.SOC_M_UID = {opSocUid}
```

SQL-2: 取得通知email
```sql
SELECT DISTINCT
  U.EMAIL 
FROM
  SYS_USER U
  LEFT JOIN SYS_USER_ROLE_V3 R ON U.USER_ID = R.USER_ID 
WHERE
  R.ROLE_ID = {opRoleId}
```

**Email模板檔 SOC-NOTICE.ftl 輸入參數**
|            | 型態   | 參數值             | 說明 |
| ---------- | ------ | ------------------ | ---- |
| equipName  | string | EQUIP_NAME(SQL-1)  |      |
| itemNo     | string | ITEM_NO(SQL-1)     |      |
| publicName | string | PUBLIC_NAME(SQL-1) |      |


# Response 欄位

* 無

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":null
  }
}
```