# 訂單出貨指示管理-取得出貨指示表頭預設值
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得出貨指示表頭預設值


## 修改歷程

| 修改時間 | 內容                                                                       | 修改者 |
|----------|--------------------------------------------------------------------------|--------|
| 20230626 | 新增規格                                                                   | Nick   |
| 20230707 | 調整 opOptionList　無資料時預設值                                           | Nick   |
| 20230710 | 調整 Modifyflag 對應客戶需求                                               | Nick   |
| 20230711 | 調整 response 範例節點名稱，部分無op開頭                                    | Nick   |
| 20230711 | 新增 opShowAssDesc 節點 之前SQL-3 遺漏未執行補上                           | Nick   |
| 20230720 | 補上 opShowAssDesc 後端流程說明                                            | Nick   |
| 20231225 | 2.0 出貨指示設定儲存會有空白產出，預設值搜尋調整寫法                        | Nick   |
| 20240102 | 一開始無表頭&無表身，表頭會取預設值，但是表身先存再存表頭，會導致誤判表頭有值 | Nick   |

## 來源URL及資料格式

| 項目   | 說明               |
|--------|--------------------|
| URL    | /sis/so_h_edit_pre |
| method | post               |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             | 名稱     | 資料型別 | 必填 | 資料儲存 & 說明 |
|------------------|--------|:--------:|:----:|-----------------|
| opOrganizationId | 工廠別   |  string  |  M   |                 |
| opSoNo           | 銷售單號 |  string  |  M   |                 |
| opCustNo         | 客戶代號 |  string  |  M   |                 |



#### Request 範例
```json
{
  "opOrganizationId":"5000",
  "opSoNo":"20765557",
  "opCustNo":"1032995"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 執行  SQL-1 確認銷售訂單是否有設定過表頭訂單出貨指示
  * 如果無
    * 則執行 SQL-2 與 SQL-3 取得訂單出貨指示項目清單 [configList]
    * 自定義變數 {ShowAssDesc} 預設為 'N'，SQL-3 結果中的 SHOWASSDESC 欄位有一筆為 'Y'，則configList的 {ShowAssDesc}覆寫為 'Y'
      * 如果 configList 非 null 執行以下
        * 取出 configList 清單每筆資料定義為 [to]
          * 依照每筆 to 內的 ListHeaderId 帶入 SQL-4 取得訂單出貨指示項目設定過的預設值清單 [defaultList]
            * 如果 defaultList 非 null，執行以下
              * [dTo]取 defaultList 第一筆
              * 如果 dTo.FlagDisplay 不等於 NULL 且 dTo.FlagDisplay 等於 "N"，則清除 defaultList 清單(備註:若已指定"不給指示(N)", 表示要留空白待業助於訂單出貨指示設定) 
            * 如果 defaultList 為 null 執行以下
              *  defaultList 覆寫為 SQL-5 回傳的內容  
            * 如果 defaultList 非 null 且 defaultList.size 大於 0 執行以下
              * [dTo]取 defaultList 第一筆
              * to.ConfigUid = dTo.ConfigUid
              * to.OptDesc = dTo.OptDesc
              * to.AssDesc = dTo.AssDesc
              * to.FlagAssign = dTo.FlagAssign
              * to.ListLineId = dTo.ListLineId
      * 將  configList 組成 opSisList 內容，如 SQL 取不到 opSisList 內相關對應節點值，直接給 null
  * 如果有
    * 則執行 SQL-6 取出原有訂單出貨指示屬於業助維護的單頭資料組出 JSON opSisList 內容且每一筆 result中的 {ListHeaderId} 帶入 SQL-7 取得 opOptionList (找不到 opOptionList: nul)
    * 自定義變數 {ShowAssDesc} 預設為 'N'， 前面 SQL-7 結果中的 SHOWASSDESC 欄位有一筆為 'Y'，則父節點的 {ShowAssDesc}覆寫為 'Y'
* 執行 SQL-8 組出 JSON opSoHTo 內容
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
--dao.checkSisHExist(soNo)
SELECT COUNT(*) CNT
FROM SO_SIS A
WHERE SO_NO = { opSoNo } AND SO_SEQ IS NULL
```

SQL-2:

```sql
--dao.getSisHeaderList(organizationId,"H" );
--取得要設定的出貨指示項目List 以及其選項
SELECT L.LIST_HEADER_ID,
    L.SP_INS_NAME,
    L.CATEGORY_FLAG CATEGORY_ATTRI,
    CASE L.LIST_HEADER_ID
    WHEN 60 THEN 'N'
    ELSE 
      L.MODIFY_FLAG
    END MODIFY_FLAG,
	CASE L.LIST_HEADER_ID
	WHEN 46 THEN 115
	WHEN 48 THEN 105
	ELSE L.LIST_SEQ
	END LIST_SEQ_NEW
FROM SYS_SIS_H L
WHERE L.ORGANIZATION_ID = {opOrganizationId}
    AND L.ACTIVE_FLAG = 'Y'
    AND L.FLAG_ORDER_TYPE = 'H'
ORDER BY LIST_SEQ_NEW
```

SQL-3
```sql
--取得選項
SELECT 
    CASE WHEN (FLAG_ASSIGN IS NULL OR FLAG_ASSIGN = '') 
      THEN 'N'  
      ELSE FLAG_ASSIGN   
    END AS SHOWASSDESC, 
    cast(ISNULL(FLAG_ASSIGN, 'N') as varchar(20)) + cast(LIST_LINE_ID as varchar(20)) [VALUE],
    cast(ISNULL(LIST_SEQ, '') as varchar(20)) + '-' + OPT_DESC SHOW_TEXT,
    OPT_DESC [TEXT]
FROM SYS_SIS_L
WHERE LIST_HEADER_ID = ?
ORDER BY LIST_SEQ
```

SQL-4:

```sql
--dao.getSisDefaultH(organizationId, custNo, to.getListHeaderId(), soNo)
--取得訂單出貨指示項目的預設值
	
	
SELECT CASE
        WHEN NULLIF(H.CUST_NO,'') IS NOT NULL
        AND NULLIF(H.ITEM_NO,'') IS NOT NULL THEN 1
        WHEN NULLIF(H.ITEM_NO,'') IS NULL
        AND NULLIF(H.CUST_NO,'') IS NOT NULL THEN 2
        ELSE 3
    END AS ORDER_COL,
    A.CONFIG_UID,
    A.LIST_HEADER_ID,
    A.LIST_LINE_ID,
    A.OPT_DESC,
    A.ASS_DESC,
    B.FLAG_ASSIGN,
    B.FLAG_DISPLAY
FROM SIS_CONFIG_H H
    INNER JOIN SIS_CONFIG A ON A.CONFIG_UID = H.CONFIG_UID
    INNER JOIN SYS_SIS_L B ON A.LIST_HEADER_ID = B.LIST_HEADER_ID
    AND A.LIST_LINE_ID = B.LIST_LINE_ID
WHERE H.ORGANIZATION_ID = {opOrganizationId}
    AND A.LIST_HEADER_ID = {ListHeaderId}
    AND (
        NULLIF(H.ITEM_NO,'') IS NULL
        OR H.ITEM_NO IN (
            SELECT ITEM_NO
            FROM SO_D
            WHERE SO_NO = {opSoNo}
        )
    )
    AND NULLIF(H.CUST_NO,'') IS NOT NULL
    AND H.CUST_NO = {opCustNo}
ORDER BY ORDER_COL
```


SQL-5:

```sql
--dao.getSisSysDefault(organizationId, to.getListHeaderId())

SELECT A.LIST_HEADER_ID,
    B.LIST_LINE_ID,
    B.OPT_DESC,
    B.FLAG_ASSIGN
FROM SYS_SIS_H A
    INNER JOIN SYS_SIS_L B ON A.LIST_HEADER_ID = B.LIST_HEADER_ID
WHERE A.ORGANIZATION_ID = {opOrganizationId}
    AND A.LIST_HEADER_ID = {ListHeaderId}
    AND B.FLAG_DEFAULT = 'Y'
				
```



SQL-6:

```sql
--dao.getSisSoForEdit(organizationId, soNo, "")
--訂單之出貨指定設定值(所有清單+設定值+選項內容)
SELECT L.LIST_HEADER_ID,
    L.SP_INS_NAME,
    L.CATEGORY_FLAG CATEGORY_ATTRI,
    CASE L.LIST_HEADER_ID
    WHEN 60 THEN 'N'
    ELSE 
      L.MODIFY_FLAG
    END MODIFY_FLAG,
	CASE L.LIST_HEADER_ID
	WHEN 46 THEN 115
	WHEN 48 THEN 105
	ELSE L.LIST_SEQ
	END LIST_SEQ_NEW,
    S.CONFIG_UID,
    S.OPT_DESC,
    S.ASS_DESC,
    S.FLAG_ASSIGN,
    S.LIST_LINE_ID
FROM SYS_SIS_H L
    LEFT JOIN (
        SELECT A.SO_NO,
            A.SO_SEQ,
            A.SIS_UID,
            A.CONFIG_UID,
            A.LIST_HEADER_ID,
            A.LIST_LINE_ID,
            A.OPT_DESC,
            A.ASS_DESC,
            B.FLAG_ASSIGN
        FROM SO_SIS A
            INNER JOIN SYS_SIS_L B ON A.LIST_HEADER_ID = B.LIST_HEADER_ID
            AND A.LIST_LINE_ID = B.LIST_LINE_ID
        WHERE A.SO_NO = { opSoNo }
            AND A.SO_SEQ IS NULL
    ) S ON L.LIST_HEADER_ID = S.LIST_HEADER_ID
WHERE L.ORGANIZATION_ID = { opOrganizationId }
    AND L.ACTIVE_FLAG = 'Y'
    AND L.MAINTAIN_FLAG = '業助'
    AND L.FLAG_ORDER_TYPE = 'H'
ORDER BY LIST_SEQ_NEW
```

SQL-7:
```sql
SELECT 
    CASE WHEN (FLAG_ASSIGN IS NULL OR FLAG_ASSIGN = '') 
      THEN 'N'  
      ELSE FLAG_ASSIGN   
    END AS SHOWASSDESC, 
    cast(ISNULL(FLAG_ASSIGN, 'N') as varchar(20)) + cast(LIST_LINE_ID as varchar(20)) [VALUE],
    cast(ISNULL(LIST_SEQ, '') as varchar(20)) + '-' + OPT_DESC SHOW_TEXT,
    OPT_DESC [TEXT]
FROM SYS_SIS_L
WHERE LIST_HEADER_ID = { ListHeaderId }
ORDER BY LIST_SEQ
```

SQL-8:
```sql
SELECT A.SO_NO, A.CUST_NO, A.CUST_NAME FROM SO_H A WHERE A.SO_NO={ opSoNo }
```




# Response 欄位
【 content 】
| 欄位      | 名稱 | 資料型別 | 資料儲存 & 說明 |
|-----------|------|----------|-----------------|
| opSoHTo   |      | object   |                 |
| opSisList |      | array    |                 |

【 opSoHTo object 】
 | 欄位       | 名稱     | 資料型別 | 資料儲存 & 說明     |
 |------------|--------|----------|---------------------|
 | opSoNo     | 銷售單號 | string   | A.SO_NO  (SQL-8)    |
 | opCustNo   | 客戶代號 | string   | A.CUST_NO  (SQL-8)  |
 | opCustName | 客戶名稱 | string   | A.CUST_NAME (SQL-8) |

【 opSisList array 】
| 欄位            | 名稱                 | 資料型別 | 資料儲存 & 說明                                                     |
|-----------------|----------------------|----------|---------------------------------------------------------------------|
| opAssDesc       | 指定內容             | string   | configList.AssDesc(無設定過訂單出貨指示) / S.ASS_DESC  (SQL-6)      |
| opCategoryAttri | 類別                 | string   | CATEGORY_ATTRI  (SQL-2 or SQL-6)                                    |
| opConfigUid     | Config UID           | string   | configList.ConfigUid(無設定過訂單出貨指示) /S.CONFIG_UID  (SQL-6)   |
| opFlagAssign    | 可否指定內容         | string   | configList.FlagAssign(無設定過訂單出貨指示) /S.FLAG_ASSIGN  (SQL-6) |
| opListHeaderId  | 出貨指示項目ID       | string   | L.LIST_HEADER_ID (SQL-2 or SQL-6)                                   |
| opListLineId    | 出貨指示選項ID       | string   | configList.ListLineId(無設定過訂單出貨指示) /S.LIST_LINE_ID (SQL-6) |
| opListSeq       | 順序                 | string   | L.LIST_SEQ   (SQL-2 or SQL-6)                                       |
| opModifyFlag    | 編輯(是否必填)       | string   | L.MODIFY_FLAG (SQL-2 or SQL-6)                                      |
| opOptDesc       | 選項內容             | string   | configList.OptDesc(無設定過訂單出貨指示) / S.OPT_DESC  (SQL-6)      |
| opSpInsName     | 出貨指示名稱         | string   | L.SP_INS_NAME (SQL-2 or SQL-6)                                      |
| opShowAssDesc   | 是否顯示指定內容元件 | string   | {ShowAssDesc}  Y/N                                                  |
| opOptionList    | 選項清單             | array    |                                                                     |

【 opOptionList array】
| 欄位       | 名稱                        | 資料型別 | 資料儲存 & 說明   |
|------------|---------------------------|----------|-------------------|
| opShowText | 順序_選項內容               | string   | SHOW_TEXT (SQL-7) |
| opText     | 選項內容                    | string   | TEXT (SQL-7)      |
| opValue    | 是否指定內容_出貨指示選項ID | string   | VALUE (SQL-7)     |


#### Response 範例



```json
{
   "msgCode":null,
   "result":{
      "content":{
         "opSoHTo":{
            "opSoNo":"20765557",
            "opCustNo":"1032995",
            "opCustName":"翊驊"
         },
         "opSisList":[
            {
               "opAssDesc":null,
               "opCategoryAttri":"生產",
               "opConfigUid":null,
               "opFlagAssign":null,
               "opListHeaderId":"46",
               "opListLineId":null,
               "opListSeq":"105",
               "opModifyFlag":"N",
               "opOptDesc":null,
               "opShowAssDesc":"N",
               "opOptionList":[
                  {
                     "opShowText":"1-一般訂單",
                     "opText":"一般訂單",
                     "opValue":"N70"
                  },
                  {
                     "opShowText":"2-急單",
                     "opText":"急單",
                     "opValue":"N71"
                  }
               ],
               "opSpInsName":"訂單緊急程度"
            },
            {
               "opAssDesc":null,
               "opCategoryAttri":"生產",
               "opConfigUid":null,
               "opFlagAssign":"N",
               "opListHeaderId":"48",
               "opListLineId":"75",
               "opListSeq":"115",
               "opModifyFlag":"Y",
               "opOptDesc":"原料無指定Lot",
               "opShowAssDesc":"Y",
               "opOptionList":[
                  {
                     "opShowText":"1-原料無指定Lot",
                     "opText":"原料無指定Lot",
                     "opValue":"N75"
                  },
                  {
                     "opShowText":"2-原料使用同一Lot生產",
                     "opText":"原料使用同一Lot生產",
                     "opValue":"N76"
                  },
                  {
                     "opShowText":"3-原料使用客戶指定Lot:",
                     "opText":"原料使用客戶指定Lot:",
                     "opValue":"Y77"
                  }
               ],
               "opSpInsName":"原料Lot需求"
            },
            {
               "opAssDesc":null,
               "opCategoryAttri":"包裝",
               "opConfigUid":null,
               "opFlagAssign":"N",
               "opListHeaderId":"59",
               "opListLineId":"97",
               "opListSeq":"325",
               "opModifyFlag":"Y",
               "opOptDesc":"標籤:一般格式",
               "opShowAssDesc":"N",
               "opOptionList":[
                  {
                     "opShowText":"1-標籤:一般格式",
                     "opText":"標籤:一般格式",
                     "opValue":"N97"
                  },
                  {
                     "opShowText":"2-標籤:含RoHS字樣",
                     "opText":"標籤:含RoHS字樣",
                     "opValue":"N98"
                  },
                  {
                     "opShowText":"3-標籤:客戶格式",
                     "opText":"標籤:客戶格式",
                     "opValue":"N99"
                  },
                  {
                     "opShowText":"4-標籤:不貼",
                     "opText":"標籤:不貼",
                     "opValue":"N100"
                  },
                  {
                     "opShowText":"5-標籤:框料專用",
                     "opText":"標籤:框料專用",
                     "opValue":"N1077"
                  },
                  {
                     "opShowText":"6-標籤:含GP字樣",
                     "opText":"標籤:含GP字樣",
                     "opValue":"N1214"
                  },
                  {
                     "opShowText":"7-標籤:太空包格式",
                     "opText":"標籤:太空包格式",
                     "opValue":"N1302"
                  },
                  {
                     "opShowText":"8-標籤:依明細行備註指示",
                     "opText":"標籤:依明細行備註指示",
                     "opValue":"N1310"
                  },
                  {
                     "opShowText":"9-標籤:醫療級專用(粉紅底)",
                     "opText":"標籤:醫療級專用(粉紅底)",
                     "opValue":"N1333"
                  }
               ],
               "opSpInsName":"標籤種類"
            },
            {
               "opAssDesc":null,
               "opCategoryAttri":"包裝",
               "opConfigUid":null,
               "opFlagAssign":"N",
               "opListHeaderId":"60",
               "opListLineId":"101",
               "opListSeq":"330",
               "opModifyFlag":"Y",
               "opOptDesc":"標籤顏色:依內規",
               "opShowAssDesc":"Y",
               "opOptionList":[
                  {
                     "opShowText":"1-標籤顏色:依內規",
                     "opText":"標籤顏色:依內規",
                     "opValue":"N101"
                  },
                  {
                     "opShowText":"2-指定標籤顏色為:",
                     "opText":"指定標籤顏色為:",
                     "opValue":"Y102"
                  },
                  {
                     "opShowText":"3-不給指示",
                     "opText":"不給指示",
                     "opValue":"N1072"
                  },
                  {
                     "opShowText":"4-依明細行備註指示",
                     "opText":"依明細行備註指示",
                     "opValue":"N1274"
                  }
               ],
               "opSpInsName":"標籤顏色"
            },
            {
               "opAssDesc":null,
               "opCategoryAttri":"包裝",
               "opConfigUid":null,
               "opFlagAssign":"N",
               "opListHeaderId":"110",
               "opListLineId":"1046",
               "opListSeq":"340",
               "opModifyFlag":"Y",
               "opOptDesc":"Lot No.每工單跳號",
               "opShowAssDesc":"N",
               "opOptionList":[
                  {
                     "opShowText":"1-Lot No.每10噸跳號",
                     "opText":"Lot No.每10噸跳號",
                     "opValue":"N1043"
                  },
                  {
                     "opShowText":"2-Lot No.每5噸跳號",
                     "opText":"Lot No.每5噸跳號",
                     "opValue":"N1044"
                  },
                  {
                     "opShowText":"3-Lot No.每10板跳號",
                     "opText":"Lot No.每10板跳號",
                     "opValue":"N1045"
                  },
                  {
                     "opShowText":"4-Lot No.每工單跳號",
                     "opText":"Lot No.每工單跳號",
                     "opValue":"N1046"
                  },
                  {
                     "opShowText":"5-Lot No.每12板跳號",
                     "opText":"Lot No.每12板跳號",
                     "opValue":"N1051"
                  },
                  {
                     "opShowText":"6-Lot No.每2.1噸跳號",
                     "opText":"Lot No.每2.1噸跳號",
                     "opValue":"N1076"
                  },
                  {
                     "opShowText":"7-Lot No.每16噸跳號",
                     "opText":"Lot No.每16噸跳號",
                     "opValue":"N1079"
                  },
                  {
                     "opShowText":"8-Lot No.每2.4噸跳號",
                     "opText":"Lot No.每2.4噸跳號",
                     "opValue":"N1209"
                  },
                  {
                     "opShowText":"9-Lot No.每35噸跳號",
                     "opText":"Lot No.每35噸跳號",
                     "opValue":"N1296"
                  },
                  {
                     "opShowText":"10-Lot No.依明細行備註指示",
                     "opText":"Lot No.依明細行備註指示",
                     "opValue":"N1315"
                  },
                  {
                     "opShowText":"11-Lot No.每20噸跳號",
                     "opText":"Lot No.每20噸跳號",
                     "opValue":"N1361"
                  },
                  {
                     "opShowText":"12-Lot No.每24板跳號",
                     "opText":"Lot No.每24板跳號",
                     "opValue":"N1362"
                  },
                  {
                     "opShowText":"13-Lot No.每9噸跳號",
                     "opText":"Lot No.每9噸跳號",
                     "opValue":"N1374"
                  },
                  {
                     "opShowText":"14-Lot No.每15噸跳號",
                     "opText":"Lot No.每15噸跳號",
                     "opValue":"N1375"
                  },
                  {
                     "opShowText":"15-Lot No.一天一LOT NO",
                     "opText":"Lot No.一天一LOT NO",
                     "opValue":"N1391"
                  }
               ],
               "opSpInsName":"Lot No.跳號頻率"
            },
            {
               "opAssDesc":null,
               "opCategoryAttri":"包裝",
               "opConfigUid":null,
               "opFlagAssign":"N",
               "opListHeaderId":"88",
               "opListLineId":"522",
               "opListSeq":"350",
               "opModifyFlag":"N",
               "opOptDesc":"棧板每層5包",
               "opShowAssDesc":"Y",
               "opOptionList":[
                  {
                     "opShowText":"1-棧板每層4包",
                     "opText":"棧板每層4包",
                     "opValue":"N521"
                  },
                  {
                     "opShowText":"2-棧板每層5包",
                     "opText":"棧板每層5包",
                     "opValue":"N522"
                  },
                  {
                     "opShowText":"3-指定棧板堆疊方式:",
                     "opText":"指定棧板堆疊方式:",
                     "opValue":"Y523"
                  },
                  {
                     "opShowText":"4-不給指示",
                     "opText":"不給指示",
                     "opValue":"N1060"
                  }
               ],
               "opSpInsName":"棧板特殊堆疊方式"
            },
            {
               "opAssDesc":null,
               "opCategoryAttri":"包裝",
               "opConfigUid":null,
               "opFlagAssign":null,
               "opListHeaderId":"93",
               "opListLineId":null,
               "opListSeq":"355",
               "opModifyFlag":"N",
               "opOptDesc":null,
               "opShowAssDesc":"N",
               "opOptionList":[
                  {
                     "opShowText":"1-上下舖完整硬紙板",
                     "opText":"上下舖完整硬紙板",
                     "opValue":"N1016"
                  },
                  {
                     "opShowText":"2-不給指示",
                     "opText":"不給指示",
                     "opValue":"N1061"
                  }
               ],
               "opSpInsName":"紙板特殊要求"
            },
            {
               "opAssDesc":null,
               "opCategoryAttri":"檢驗",
               "opConfigUid":null,
               "opFlagAssign":"N",
               "opListHeaderId":"68",
               "opListLineId":"121",
               "opListSeq":"620",
               "opModifyFlag":"Y",
               "opOptDesc":"每Lot附COA出貨報告",
               "opShowAssDesc":"N",
               "opOptionList":[
                  {
                     "opShowText":"1-不需附COA",
                     "opText":"不需附COA",
                     "opValue":"N120"
                  },
                  {
                     "opShowText":"2-每Lot附COA出貨報告",
                     "opText":"每Lot附COA出貨報告",
                     "opValue":"N121"
                  }
               ],
               "opSpInsName":"COA"
            },
            {
               "opAssDesc":null,
               "opCategoryAttri":"檢驗",
               "opConfigUid":null,
               "opFlagAssign":"N",
               "opListHeaderId":"69",
               "opListLineId":"122",
               "opListSeq":"625",
               "opModifyFlag":"N",
               "opOptDesc":"COA使用廠內格式",
               "opShowAssDesc":"Y",
               "opOptionList":[
                  {
                     "opShowText":"1-COA使用廠內格式",
                     "opText":"COA使用廠內格式",
                     "opValue":"N122"
                  },
                  {
                     "opShowText":"2-指定COA格式:",
                     "opText":"指定COA格式:",
                     "opValue":"Y123"
                  },
                  {
                     "opShowText":"3-不給指示",
                     "opText":"不給指示",
                     "opValue":"N1056"
                  }
               ],
               "opSpInsName":"COA格式"
            },
            {
               "opAssDesc":null,
               "opCategoryAttri":"包裝",
               "opConfigUid":null,
               "opFlagAssign":null,
               "opListHeaderId":"103",
               "opListLineId":null,
               "opListSeq":"910",
               "opModifyFlag":"N",
               "opOptDesc":null,
               "opShowAssDesc":"Y",
               "opOptionList":[
                  {
                     "opShowText":"1-訂單備註:",
                     "opText":"訂單備註:",
                     "opValue":"Y1027"
                  },
                  {
                     "opShowText":"2-不給指示",
                     "opText":"不給指示",
                     "opValue":"N1062"
                  }
               ],
               "opSpInsName":"訂單備註"
            }
         ]
      }
   }
}
```

## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考

* SisConfigManageAction.sisConfigEditPre