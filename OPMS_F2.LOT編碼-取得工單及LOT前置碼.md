# LOT編碼-取得工單及LOT前置碼
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得工單及LOT前置碼

## 修改歷程

| 修改時間 | 內容       | 修改者 |
| -------- | ---------- | ------ |
| 20230830 | 新增規格   | Ellen  |
| 20231016 | 增加分頁   | Ellen  |
| 20231024 | 補色號搜尋 | Ellen  |


## 來源URL及資料格式

| 項目   | 說明         |
| ------ | ------------ |
| URL    | /lot/wo_list |
| method | post         |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位       | 名稱               | 資料型別 | 必填 | 資料儲存 & 說明                                                          |
| ---------- | ------------------ | :------: | :--: | ------------------------------------------------------------------------ |
| opMfgDate  | 日期               |  string  |  M   | yyyy/MM/dd                                                               |
| opEquipId  | 機台               |  string  |  M   |                                                                          |
| opWoNo     | 工單號碼或完整色號 |  string  |  M   | 工單號碼模糊搜尋                                                                 |
| opPage     | 第幾頁             | integer  |  O   | 預設`1`，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推 |
| opPageSize | 分頁筆數           | integer  |  O   | 預設 `10`                                                                |

#### Request 範例

```json
{
  "opMfgDate":"2022/12/04",    
  "opEquipId":"1",
  "opWoNo": "51009801",
  "opPage":1,
  "opPageSize":10,
}
```

# Request 後端流程說明

可參考舊版 PsManageAction.ajaxQueryWoLList
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 取得預設LOT，判斷{opMfgDate}
  * 大於2021年1月1日 使用新編碼規則：年2碼＋產線3碼＋月數字1~C 1碼＋日2碼，執行 SQL-1 
  * 否則使用舊規則：年1碼＋產線3碼＋月1碼＋日2碼，執行 SQL-2
* 執行 SQL-3 取得工單列表
* {offset} 為起始列印 row ，第一頁為 0，第二頁後為 0 + (opPageSize*(opPage-1))
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1: 新編碼規則：取得預設LOT
```sql
SELECT
  {opMfgDate年份末兩碼} + E.LOT_CODE +
  CASE
    D.PARA_VALUE 
    WHEN 10 THEN 'A' 
    WHEN 11 THEN 'B' 
    WHEN 12 THEN 'C' 
    ELSE CONVERT ( VARCHAR, CAST( D.PARA_VALUE AS FLOAT ) ) END + {opMfgDate日期} 
  AS LOT_PRE 
FROM
  EQUIP_H E
  CROSS JOIN SYS_PARA_D D 
WHERE
  D.PARA_ID = 'LOT_MON' 
  AND D.PARA_VALUE = {opMfgDate月份}
  AND E.EQUIP_ID = {opEquipId}
```

SQL-2: 舊編碼規則：取得預設LOT
```sql
SELECT
  SUBSTRING( PS.MFG_DATE, 4, 1 ) + E.LOT_CODE + D.PARA_CODE + SUBSTRING( PS.MFG_DATE, 9, 2 ) AS LOT_PRE
FROM
  (SELECT {opMfgDate} AS MFG_DATE) AS PS
  INNER JOIN SYS_PARA_D D ON SUBSTRING( PS.MFG_DATE, 6, 2 ) = D.PARA_VALUE
  CROSS JOIN EQUIP_H E
WHERE
  D.PARA_ID = 'LOT_MON' 
  AND E.EQUIP_ID = {opEquipId}
```

SQL-3: 取得工單列表
```sql
SELECT
  H.WO_UID,
  H.CUST_NAME,
  B.PATTERN_NO,
  B.ITEM_NO,
  H.WO_NO,
  H.WO_QTY,
  REPLACE ( S1.OPT_DESC, 'Lot No.', '' ) LOT_OPT,
  S2.OPT_DESC PKG_OPT,
  REPLACE ( S3.OPT_DESC, '外袋Lot No.:', '' ) PKG_LOT_OPT,
  CASE 
    WHEN S3.OPT_DESC LIKE '%奇美%' THEN 'Y'
    WHEN S3.OPT_DESC LIKE '%原料%' THEN 'Y'
    ELSE 'N' END AS USE_MTR_FLAG
FROM
  WO_H H
  INNER JOIN BOM_MTM B ON H.BOM_UID = B.BOM_UID
    -- 標籤LOT跳號規則
  LEFT JOIN ( SELECT WO_UID, OPT_DESC FROM WO_SIS WHERE LIST_HEADER_ID = 110 ) S1 ON H.WO_UID = S1.WO_UID
    -- 棧板規則
  LEFT JOIN ( SELECT WO_UID, OPT_DESC FROM WO_SIS WHERE LIST_HEADER_ID = 92 ) S2 ON H.WO_UID = S2.WO_UID
    -- 外袋LOT規則
  LEFT JOIN ( SELECT WO_UID, OPT_DESC FROM WO_SIS WHERE LIST_HEADER_ID = 90 ) S3 ON H.WO_UID = S3.WO_UID
WHERE
  H.WO_NO LIKE '%{opWoNo}%'
  OR B.PATTERN_NO = {opWoNo}
ORDER BY WO_UID ASC
OFFSET {offset} ROWS
FETCH NEXT {opageSize} ROWS ONLY
```

# Response 欄位
| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
| -------- | -------- | -------- | --------------- |
| pageInfo | 分頁資訊 | object   |                 |
| content  | 附帶訊息 | array    |                 |

#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明 |
| ---------- | ------------ | -------- | --------------- |
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |

#### 【content】
| 欄位         | 名稱            | 資料型別 | 來源資料 & 說明     |
| ------------ | --------------- | -------- | ------------------- |
| opWoUid      | 工單ID          | string   | WO_UID(SQL-3)       |
| opCustName   | 客戶名稱        | string   | CUST_NAME(SQL-3)    |
| opPatternNo  | 色號            | string   | PATTERN_NO(SQL-3)   |
| opItemNo     | 料號            | string   | ITEM_NO(SQL-3)      |
| opWoNo       | 工單編號        | string   | WO_NO(SQL-3)        |
| opWoQty      | 工單數量        | string   | WO_QTY(SQL-3)       |
| opLotOpt     | 標籤LOT跳號規則 | string   | LOT_OPT(SQL-3)      |
| opPkgLotOpt  | 紙袋LOT規則     | string   | PKG_LOT_OPT(SQL-3)  |
| opPkgOpt     | 棧板規則        | string   | PKG_OPT(SQL-3)      |
| opUseMtrFlag |                 | string   | USE_MTR_FLAG(SQL-3) |
| opLotPre     | 預設LOT         | string   | LOT_PRE(SQL-1)      |
| opEquipId    | 機台            | string   | {opEquipId}         |


#### Response 範例

```json
{
    "msgCode": null,
    "result": {
        "pageInfo": {
        "pageNumber": 1, 
        "pageSize": 10,
        "pages": 1,      
        "total": 1     
      },
      "content":[
        {
          "opWoUid": "0DA30E93-58A0-4171-A3AB-C047413A7602",
          "opCustName": "丞翔國際",
          "opPatternNo": "J15919GGA",
          "opItemNo": "LGA1535X1XSIJ15919",
          "opWoNo": "51009801",
          "opWoQty": "50197",
          "opLotOpt": "每10噸跳號",
          "opPkgLotOpt": "編奇菱Lot No",
          "opPkgOpt": "1000打包,自購歐高熱棧板+回收紙板",
          "opUseMtrFlag": "N",
          "opLotPre": "22T03C04",
          "opEquipId": "1",
        }
      ]
    }
}
```