# 複材-SOC管理-新增修改存檔
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容                           | 修改者 |
| -------- | ------------------------------ | ------ |
| 20230830 | 新增規格                       | 黃東俞 |
| 20231016 | 移除多餘欄位                   | Ellen  |
| 20231026 | 複材押出機溫度條件遺漏 C13~C15 | Nick   |
| 20231030 | 狀態預設為N                    | Ellen  |

## 來源URL及資料格式

| 項目   | 說明            |
| ------ | --------------- |
| URL    | /soc/soc_m_save |
| method | post            |


## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位               | 名稱                | 資料型別 | 必填 | 資料儲存 & 說明                                     |
| ------------------ | ------------------- | -------- | :--: | --------------------------------------------------- |
| opSocMUid          | ID                  | string   |  O   |                                                     |
| opEquipId          | 機台                | int      |  M   |                                                     |
| opItemNo           | 料號                | string   |  M   |                                                     |
| opBomUid           | BOM ID              | string   |  O   |                                                     |
| opStatus           | 狀態                | string   |  O   | N:改版中 / W:簽核中 / Y:已發行 / D:已失效  預設:`N` |
| opMfgQty           | 每分鐘產出量        | float    |  O   |                                                     |
| opMoldScreenSize   | 模頭濾網            | string   |  O   |                                                     |
| opScrewType        | 螺桿型號            | string   |  O   |                                                     |
| opMoldHole         | 模頭孔數            | int      |  O   |                                                     |
| opMoldApeture      | 模頭孔徑            | float    |  O   |                                                     |
| opCutterType       | 切刀型態            | string   |  O   |                                                     |
| opCutType          | 造粒方式            | string   |  O   |                                                     |
| opScreenApeture    | 篩網孔徑            | int      |  O   |                                                     |
| opFeederQty        | 餵料機下料量        | float    |  O   |                                                     |
| opFeederUnit       | 下料單位            | string   |  O   |                                                     |
| opProdDesc         | 產品說明            | string   |  O   |                                                     |
| opF1MtrDesc        | F1餵料機說明        | string   |  O   |                                                     |
| opF1MtrRate        | F1下料比例          | string   |  O   |                                                     |
| opF1MtrMix         | F1混合方式          | string   |  O   |                                                     |
| opF1Screw          | F1餵料螺桿          | string   |  O   |                                                     |
| opF2MtrDesc        | F2餵料機說明        | string   |  O   |                                                     |
| opF2MtrRate        | F2下料比例          | string   |  O   |                                                     |
| opF2MtrMix         | F2混合方式          | string   |  O   |                                                     |
| opF2Screw          | F2餵料螺桿          | string   |  O   |                                                     |
| opF3MtrDesc        | F3餵料機說明        | string   |  O   |                                                     |
| opF3MtrRate        | F3下料比例          | string   |  O   |                                                     |
| opF3MtrMix         | F3混合方式          | string   |  O   |                                                     |
| opF3Screw          | F3餵料螺桿          | string   |  O   |                                                     |
| opF4MtrDesc        | F4餵料機說明        | string   |  O   |                                                     |
| opF4MtrRate        | F4下料比例          | string   |  O   |                                                     |
| opF4MtrMix         | F4混合方式          | string   |  O   |                                                     |
| opF4Screw          | F4餵料螺桿          | string   |  O   |                                                     |
| opF5MtrDesc        | F5餵料機說明        | string   |  O   |                                                     |
| opF5MtrRate        | F5下料比例          | string   |  O   |                                                     |
| opF5MtrMix         | F5混合方式          | string   |  O   |                                                     |
| opF5Screw          | F5餵料螺桿          | string   |  O   |                                                     |
| opF6MtrDesc        | F6餵料機說明        | string   |  O   |                                                     |
| opF6MtrRate        | F6下料比例          | string   |  O   |                                                     |
| opF6MtrMix         | F6混合方式          | string   |  O   |                                                     |
| opF6Screw          | F6餵料螺桿          | string   |  O   |                                                     |
| opC1Std            | (押出機溫度)C1標準  | int      |  O   |                                                     |
| opC1Max            | (押出機溫度)C1上限  | int      |  O   |                                                     |
| opC1Min            | (押出機溫度)C1下限  | int      |  O   |                                                     |
| opC2Std            | (押出機溫度)C2標準  | int      |  O   |                                                     |
| opC2Max            | (押出機溫度)C2上限  | int      |  O   |                                                     |
| opC2Min            | (押出機溫度)C2下限  | int      |  O   |                                                     |
| opC3Std            | (押出機溫度)C3標準  | int      |  O   |                                                     |
| opC3Max            | (押出機溫度)C3上限  | int      |  O   |                                                     |
| opC3Min            | (押出機溫度)C3下限  | int      |  O   |                                                     |
| opC4Std            | (押出機溫度)C4標準  | int      |  O   |                                                     |
| opC4Max            | (押出機溫度)C4上限  | int      |  O   |                                                     |
| opC4Min            | (押出機溫度)C4下限  | int      |  O   |                                                     |
| opC5Std            | (押出機溫度)C5標準  | int      |  O   |                                                     |
| opC5Max            | (押出機溫度)C5上限  | int      |  O   |                                                     |
| opC5Min            | (押出機溫度)C5下限  | int      |  O   |                                                     |
| opC6Std            | (押出機溫度)C6標準  | int      |  O   |                                                     |
| opC6Max            | (押出機溫度)C6上限  | int      |  O   |                                                     |
| opC6Min            | (押出機溫度)C6下限  | int      |  O   |                                                     |
| opC7Std            | (押出機溫度)C7標準  | int      |  O   |                                                     |
| opC7Max            | (押出機溫度)C7上限  | int      |  O   |                                                     |
| opC7Min            | (押出機溫度)C7下限  | int      |  O   |                                                     |
| opC8Std            | (押出機溫度)C8標準  | int      |  O   |                                                     |
| opC8Max            | (押出機溫度)C8上限  | int      |  O   |                                                     |
| opC8Min            | (押出機溫度)C8下限  | int      |  O   |                                                     |
| opC9Std            | (押出機溫度)C9標準  | int      |  O   |                                                     |
| opC9Max            | (押出機溫度)C9上限  | int      |  O   |                                                     |
| opC9Min            | (押出機溫度)C9下限  | int      |  O   |                                                     |
| opCAStd            | (押出機溫度)C10標準 | int      |  O   |                                                     |
| opCAMax            | (押出機溫度)C10上限 | int      |  O   |                                                     |
| opCAMin            | (押出機溫度)C10下限 | int      |  O   |                                                     |
| opCBStd            | (押出機溫度)C11標準 | int      |  O   |                                                     |
| opCBMax            | (押出機溫度)C11上限 | int      |  O   |                                                     |
| opCBMin            | (押出機溫度)C11下限 | int      |  O   |                                                     |
| opCCStd            | (押出機溫度)C12標準 | int      |  O   |                                                     |
| opCCMax            | (押出機溫度)C12上限 | int      |  O   |                                                     |
| opCCMin            | (押出機溫度)C12下限 | int      |  O   |                                                     |
| opCDStd            | (押出機溫度)C13標準 | int      |  O   |                                                     |
| opCDMax            | (押出機溫度)C13上限 | int      |  O   |                                                     |
| opCDMin            | (押出機溫度)C13下限 | int      |  O   |                                                     |
| opCEStd            | (押出機溫度)C14標準 | int      |  O   |                                                     |
| opCEMax            | (押出機溫度)C14上限 | int      |  O   |                                                     |
| opCEMin            | (押出機溫度)C14下限 | int      |  O   |                                                     |
| opCFStd            | (押出機溫度)C15標準 | int      |  O   |                                                     |
| opCFMax            | (押出機溫度)C15上限 | int      |  O   |                                                     |
| opCFMin            | (押出機溫度)C15下限 | int      |  O   |                                                     |
| opF1Std            | F1標準              | float    |  O   |                                                     |
| opF1Max            | F1上限              | float    |  O   |                                                     |
| opF1Min            | F1下限              | float    |  O   |                                                     |
| opF2Std            | F2標準              | float    |  O   |                                                     |
| opF2Max            | F2上限              | float    |  O   |                                                     |
| opF2Min            | F2下限              | float    |  O   |                                                     |
| opF3Std            | F3標準              | float    |  O   |                                                     |
| opF3Max            | F3上限              | float    |  O   |                                                     |
| opF3Min            | F3下限              | float    |  O   |                                                     |
| opF4Std            | F4標準              | float    |  O   |                                                     |
| opF4Max            | F4上限              | float    |  O   |                                                     |
| opF4Min            | F4下限              | float    |  O   |                                                     |
| opF5Std            | F5標準              | float    |  O   |                                                     |
| opF5Max            | F5上限              | float    |  O   |                                                     |
| opF5Min            | F5下限              | float    |  O   |                                                     |
| opF6Std            | F6標準              | float    |  O   |                                                     |
| opF6Max            | F6上限              | float    |  O   |                                                     |
| opF6Min            | F6下限              | float    |  O   |                                                     |
| opScrewRpmStd      | 螺桿轉速標準        | int      |  O   |                                                     |
| opScrewRpmMax      | 螺桿轉速上限        | int      |  O   |                                                     |
| opScrewRpmMin      | 螺桿轉速下限        | int      |  O   |                                                     |
| opSideRpmStd       | 側餵料機轉速標準    | int      |  O   |                                                     |
| opSideRpmMax       | 側餵料機轉速上限    | int      |  O   |                                                     |
| opSideRpmMin       | 側餵料機轉速下限    | int      |  O   |                                                     |
| opGranulatorRpmStd | 切粒機標準          | float    |  O   |                                                     |
| opGranulatorRpmMax | 切粒機上限          | float    |  O   |                                                     |
| opGranulatorRpmMin | 切粒機下限          | float    |  O   |                                                     |
| opWrenchRate       | 扭力                | int      |  O   |                                                     |
| opResinTemp        | 樹脂溫度            | int      |  O   |                                                     |
| opDiePressure      | 模頭壓力            | int      |  O   |                                                     |
| opVacuumIndexStd   | 真空指數標準        | int      |  O   |                                                     |
| opVacuumIndexMax   | 真空指數上限        | int      |  O   |                                                     |
| opVacuumIndexMin   | 真空指數下限        | int      |  O   |                                                     |
| opBdpStd           | BDP標準             | float    |  O   |                                                     |
| opBdpMax           | BDP上限             | float    |  O   |                                                     |
| opBdpMin           | BDP下限             | float    |  O   |                                                     |
| opBTempStd         | 大槽水溫標準值      | int      |  O   |                                                     |
| opBTempMax         | 大槽水溫上限        | int      |  O   |                                                     |
| opBTempMin         | 大槽水溫下限        | int      |  O   |                                                     |
| opSTempStd         | 小槽水溫標準值      | int      |  O   |                                                     |
| opSTempMax         | 小槽水溫上限        | int      |  O   |                                                     |
| opSTempMin         | 小槽水溫下限        | int      |  O   |                                                     |
| opETempStd         | 第三段水溫標準值    | int      |  O   |                                                     |
| opETempMax         | 第三段水溫上限      | int      |  O   |                                                     |
| opETempMin         | 第三段水溫下限      | int      |  O   |                                                     |
| opBdpExtraRate     | BDP外加比例         | flaot    |  O   |                                                     |
| opScreenFreq       | 網目更換            | string   |  O   |                                                     |
| opRecordFreq       | 記錄間隔            | int      |  O   |                                                     |
| opRemark           | 備註                | string   |  O   |                                                     |
| opMfgNotice        | 作業注意事項        | string   |  O   |                                                     |
| opFileName         | 圖面                | string   |  O   |                                                     |
| opMfgDesc          | 生產方式說明        | string   |  O   |                                                     |
| opMetalCheck       | 金檢                | string   |  O   |                                                     |


#### Request 範例

```json
{
  "opSocMUid":"5BBB6876-5805-4F50-B07A-CB3F6EFEEC2A",
  "opEquipId":45,
  "opItemNo":"06-199-PC110XXXXXX",
  "opBomUid":null,
  "opStatus":"Y",
  "opMoldScreenSize":"50/50mesh",
  "opScrewType":"PC-365-1",
  "opMoldHole":17,
  "opMoldApeture":4.5,
  "opCutterType":"S/W",
  "opCutType":"牽條",
  "opScreenApeture":5,
  "opFeederQty":300.00,
  "opFeederUnit":"Kg/hr",
  "opProdDesc":"PC",
  "opF1MtrDesc":"主原料+副料",
  "opF1MtrRate":"100%",
  "opF1MtrMix":null,
  "opF1Screw":null,
  "opF2MtrDesc":null,
  "opF2MtrRate":null,
  "opF2MtrMix":null,
  "opF2Screw":null,
  "opF3MtrDesc":"-",
  "opF3MtrRate":"-",
  "opF3MtrMix":"-",
  "opF3Screw":"-",
  "opF4MtrDesc":null,
  "opF4MtrRate":null,
  "opF4MtrMix":null,
  "opF4Screw":null,
  "opF5MtrDesc":null,
  "opF5MtrRate":null,
  "opF5MtrMix":null,
  "opF5Screw":null,
  "opF6MtrDesc":null,
  "opF6MtrRate":null,
  "opF6MtrMix":null,
  "opF6Screw":null,
  "opC1Std":180,
  "opC1Max":190,
  "opC1Min":170,
  "opC2Std":195,
  "opC2Max":205,
  "opC2Min":185,
  "opC3Std":200,
  "opC3Max":210,
  "opC3Min":190,
  "opC4Std":215,
  "opC4Max":225,
  "opC4Min":205,
  "opC5Std":220,
  "opC5Max":230,
  "opC5Min":210,
  "opC6Std":215,
  "opC6Max":225,
  "opC6Min":205,
  "opC7Std":220,
  "opC7Max":230,
  "opC7Min":210,
  "opC8Std":215,
  "opC8Max":225,
  "opC8Min":205,
  "opC9Std":210,
  "opC9Max":220,
  "opC9Min":200,
  "opCAStd":220,
  "opCAMax":230,
  "opCAMin":210,
  "opCBStd":225,
  "opCBMax":235,
  "opCBMin":215,
  "opCCStd":null,
  "opCCMax":null,
  "opCCMin":null,
  "opCDStd":null,
  "opCDMax":null,
  "opCDMin":null,
  "opCEStd":null,
  "opCEMax":null,
  "opCEMin":null,
  "opCFStd":null,
  "opCFMax":null,
  "opCFMin":null,
  "opF1Std":300.00,
  "opF1Max":350.00,
  "opF1Min":200.00,
  "opF2Std":0.00,
  "opF2Max":0.00,
  "opF2Min":0.00,
  "opF3Std":null,
  "opF3Max":0.00,
  "opF3Min":0.00,
  "opF4Std":null,
  "opF4Max":null,
  "opF4Min":null,
  "opF5Std":null,
  "opF5Max":null,
  "opF5Min":null,
  "opF6Std":null,
  "opF6Max":null,
  "opF6Min":null,
  "opScrewRpmStd":300,
  "opScrewRpmMax":350,
  "opScrewRpmMin":200,
  "opSideRpmStd":null,
  "opSideRpmMax":null,
  "opSideRpmMin":null,
  "opGranulatorRpmStd":null,
  "opGranulatorRpmMax":null,
  "opGranulatorRpmMin":null,
  "opWrenchRate":200,
  "opResinTemp":null,
  "opDiePressure":null,
  "opVacuumIndexStd":-50,
  "opVacuumIndexMax":-55,
  "opVacuumIndexMin":-45,
  "opBdpStd":null,
  "opBdpMax":0.000,
  "opBdpMin":0.000,
  "opBTempStd":50,
  "opBTempMax":60,
  "opBTempMin":40,
  "opSTempStd":70,
  "opSTempMax":80,
  "opSTempMin":60,
  "opETempStd":null,
  "opETempMax":null,
  "opETempMin":null,
  "opBdpExtraRate":null,
  "opScreenFreq":"每24小時",
  "opRecordFreq":2,
  "opRemark":null,
  "opMfgNotice":"a.此為粉碎料大小不一(有長條狀),3F下料須以濾網過濾及放磁力架 b.因粉碎料較澎鬆,F1本為40Kg補料會溢出,因此改為23Kg補料,生產完工F1請調回40Kg",
  "opFileName":"CLT_50MM_1.jpg",
  "opMfgDesc":"單管下料(#1)",
  "opMetalCheck":"-",
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 取 { 登入者ID } SYS_USER.USER_ID
* 當傳入的socMUid(ID)為null時，新增一筆資料至 SOC_M
  * SOC_M_UID(新ID)：取隨機 UUID
  * SOC_M_VER(新版號)：參考SQL-1，取相同 EQUIP_ID,ITEM_NO,BOM_UID 的 SOC_M_VER 最大值+1之後作為新的版號
  * 參考 SQL-2 新增資料至 SOC_M
* 有傳入opSocUid時，參考SQL-3更新資料
  * 注意更新時，並不會更新STATUS(狀態)欄位
  * 更新後，執行 SQL-LOG 紀錄更新資訊，LOG欄位填入"新增"或"修改"
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200

# Request 後端邏輯說明

SQL-1:

```sql
-- SocMQueryDao.getMaxSocVer
SELECT ISNULL(MAX(SOC_M_VER), 0) + 1 NEW_SOC_VER
FROM SOC_M
WHERE ITEM_NO = { opItemNo }
AND EQUIP_ID = { opEquipId }
AND BOM_UID = { opBomUid }
```

SQL-2:

```sql
-- SocMModifyDao.insertSocM
INSERT INTO SOC_M(
  SOC_M_UID, EQUIP_ID, BOM_UID, ITEM_NO, SOC_M_VER, STATUS, MOLD_SCREEN_SIZE,
  SCREW_TYPE, MOLD_HOLE, MOLD_APETURE, CUTTER_TYPE, CUT_TYPE, SCREEN_APETURE, FEEDER_QTY, FEEDER_UNIT, METAL_CHECK, PROD_DESC,
  F_1_MTR_DESC, F_1_MTR_RATE, F_1_MTR_MIX, F_1_SCREW,
  F_2_MTR_DESC, F_2_MTR_RATE, F_2_MTR_MIX, F_2_SCREW,
  F_3_MTR_DESC, F_3_MTR_RATE, F_3_MTR_MIX, F_3_SCREW,
  F_4_MTR_DESC, F_4_MTR_RATE, F_4_MTR_MIX, F_4_SCREW,
  F_5_MTR_DESC, F_5_MTR_RATE, F_5_MTR_MIX, F_5_SCREW,
  F_6_MTR_DESC, F_6_MTR_RATE, F_6_MTR_MIX, F_6_SCREW,
  C_1_STD, C_1_MAX, C_1_MIN, C_2_STD, C_2_MAX, C_2_MIN, C_3_STD, C_3_MAX, C_3_MIN,
  C_4_STD, C_4_MAX, C_4_MIN, C_5_STD, C_5_MAX, C_5_MIN, C_6_STD, C_6_MAX, C_6_MIN,
  C_7_STD, C_7_MAX, C_7_MIN, C_8_STD, C_8_MAX, C_8_MIN, C_9_STD, C_9_MAX, C_9_MIN,
  C_A_STD, C_A_MAX, C_A_MIN, C_B_STD, C_B_MAX, C_B_MIN, C_C_STD, C_C_MAX, C_C_MIN,
  C_D_STD, C_D_MAX, C_D_MIN, C_E_STD, C_E_MAX, C_E_MIN, C_F_STD, C_F_MAX, C_F_MIN,
  F_1_STD, F_1_MAX, F_1_MIN, F_2_STD, F_2_MAX, F_2_MIN, F_3_STD, F_3_MAX, F_3_MIN,
  F_4_STD, F_4_MAX, F_4_MIN, F_5_STD, F_5_MAX, F_5_MIN, F_6_STD, F_6_MAX, F_6_MIN,
  GRANULATOR_RPM_STD, GRANULATOR_RPM_MAX, GRANULATOR_RPM_MIN,
  SCREW_RPM_STD, SCREW_RPM_MAX, SCREW_RPM_MIN, SIDE_RPM_STD, SIDE_RPM_MAX, SIDE_RPM_MIN,
  WRENCH_RATE, RESIN_TEMP, DIE_PRESSURE, VACUUM_INDEX_STD, VACUUM_INDEX_MAX, VACUUM_INDEX_MIN,
  BDP_STD, BDP_MAX, BDP_MIN, B_TEMP_STD, B_TEMP_MAX, B_TEMP_MIN,
  S_TEMP_STD, S_TEMP_MAX, S_TEMP_MIN, E_TEMP_STD, E_TEMP_MAX, E_TEMP_MIN,
  BDP_EXTRA_RATE, SCREEN_FREQ, RECORD_FREQ, MFG_NOTICE, REMARK, FILE_NAME, MFG_DESC,
  CDT, CREATE_BY, PUB_DATE, PUBLIC_BY, UDT, UPDATE_BY)
VALUES({ 新ID }, { opEquipId }, { opBomUid }, { opItemNo }, { 新版號 }, { opStatus }, { opMoldScreenSize },
  { opScrewType }, { opMoldHole }, { opMoldApeture }, { opCutterType }, { opCutType }, { opScreenApeture }, { opFeederQty }, { opFeederUnit }, { opMetalCheck }, { opProdDesc },
  { opF1MtrDesc }, { opF1MtrRate }, { opF1MtrMix }, { opF1Screw },
  { opF2MtrDesc }, { opF2MtrRate }, { opF2MtrMix }, { opF2Screw },
  { opF3MtrDesc }, { opF3MtrRate }, { opF3MtrMix }, { opF3Screw },
  { opF4MtrDesc }, { opF4MtrRate }, { opF4MtrMix }, { opF4Screw },
  { opF5MtrDesc }, { opF5MtrRate }, { opF5MtrMix }, { opF5Screw },
  { opF6MtrDesc }, { opF6MtrRate }, { opF6MtrMix }, { opF6Screw },
  { opC1Std }, { opC1Max }, { opC1Min }, { opC2Std }, { opC2Max }, { opC2Min }, { opC3Std }, { opC3Max }, { opC3Min },
  { opC4Std }, { opC4Max }, { opC4Min }, { opC5Std }, { opC5Max }, { opC5Min }, { opC6Std }, { opC6Max }, { opC6Min },
  { opC7Std }, { opC7Max }, { opC7Min }, { opC8Std }, { opC8Max }, { opC8Min }, { opC9Std }, { opC9Max }, { opC9Min },
  { opCAStd }, { opCAMax }, { opCAMin }, { opCBStd }, { opCBMax }, { opCBMin }, { opCCStd }, { opCCMax }, { opCCMin },
  { opCDStd }, { opCDMax }, { opCDMin }, { opCEStd }, { opCEMax }, { opCEMin }, { opCFStd }, { opCFMax }, { opCFMin },
  { opF1Std }, { opF1Max }, { opF1Min }, { opF2Std }, { opF2Max }, { opF2Min }, { opF3Std }, { opF3Max }, { opF3Min },
  { opF4Std }, { opF4Max }, { opF4Min }, { opF5Std }, { opF5Max }, { opF5Min }, { opF6Std }, { opF6Max }, { opF6Min },
  { opGranulatorRpmStd }, { opGranulatorRpmMax }, { opGranulatorRpmMin },
  { opScrewRpmStd }, { opScrewRpmMax }, { opScrewRpmMin }, { opSideRpmStd }, { opSideRpmMax }, { opSideRpmMin },
  { opWrenchRate }, { opResinTemp }, { opDiePressure }, { opVacuumIndexStd }, { opVacuumIndexMax }, { opVacuumIndexMin },
  { opBdpStd }, { opBdpMax }, { opBdpMin }, { opBTempStd }, { opBTempMax }, { opBTempMin },
  { opSTempStd }, { opSTempMax }, { opSTempMin }, { opETempStd }, { opETempMax }, { opETempMin },
  { opBdpExtraRate }, { opScreenFreq }, { opRecordFreq }, { opMfgNotice }, { opRemark }, { opFileName }, { opMfgDesc }
  GETDATE(), { 登入者ID }, null ,null, GETDATE(), { 登入者ID });
```

SQL-3:

```sql
-- SocMModifyDao.updateSocM
UPDATE SOC_M
  SET MOLD_SCREEN_SIZE = { opMoldScreenSize },
  SCREW_TYPE = { opScrewType }, MOLD_HOLE = { opMoldHole },  MOLD_APETURE = { opMoldApeture }, CUTTER_TYPE = { opCutterType }, CUT_TYPE = { opCutType },
  SCREEN_APETURE = { opScreenApeture }, FEEDER_QTY = { opFeederQty }, FEEDER_UNIT = { opFeederUnit }, METAL_CHECK = { opMetalCheck }, PROD_DESC = { opProdDesc },
  F_1_MTR_DESC = { opF1MtrDesc }, F_1_MTR_RATE = { opF1MtrRate }, F_1_MTR_MIX = { opF1MtrMix }, F_1_SCREW = { opF1Screw },
  F_2_MTR_DESC = { opF2MtrDesc }, F_2_MTR_RATE = { opF2MtrRate }, F_2_MTR_MIX = { opF2MtrMix }, F_2_SCREW = { opF2Screw },
  F_3_MTR_DESC = { opF3MtrDesc }, F_3_MTR_RATE = { opF3MtrRate }, F_3_MTR_MIX = { opF3MtrMix }, F_3_SCREW = { opF3Screw },
  F_4_MTR_DESC = { opF4MtrDesc }, F_4_MTR_RATE = { opF4MtrRate }, F_4_MTR_MIX = { opF4MtrMix }, F_4_SCREW = { opF4Screw },
  F_5_MTR_DESC = { opF5MtrDesc }, F_5_MTR_RATE = { opF5MtrRate }, F_5_MTR_MIX = { opF5MtrMix }, F_5_SCREW = { opF5Screw },
  F_6_MTR_DESC = { opF6MtrDesc }, F_6_MTR_RATE = { opF6MtrRate }, F_6_MTR_MIX = { opF6MtrMix }, F_6_SCREW = { opF6Screw },
  C_1_STD = { opC1Std }, C_1_MAX = { opC1Max }, C_1_MIN = { opC1Min }, C_2_STD = { opC2Std }, C_2_MAX = { opC2Max }, C_2_MIN = { opC2Min },
  C_3_STD = { opC3Std }, C_3_MAX = { opC3Max }, C_3_MIN = { opC3Min },
  C_4_STD = { opC4Std }, C_4_MAX = { opC4Max }, C_4_MIN = { opC4Min }, C_5_STD = { opC5Std }, C_5_MAX = { opC5Max }, C_5_MIN = { opC5Min },
  C_6_STD = { opC6Std }, C_6_MAX = { opC6Max }, C_6_MIN = { opC6Min },
  C_7_STD = { opC7Std }, C_7_MAX = { opC7Max }, C_7_MIN = { opC7Min }, C_8_STD = { opC8Std }, C_8_MAX = { opC8Max }, C_8_MIN = { opC8Min },
  C_9_STD = { opC9Std }, C_9_MAX = { opC9Max }, C_9_MIN = { opC9Min },
  C_A_STD = { opCAStd }, C_A_MAX = { opCAMax }, C_A_MIN = { opCAMin }, C_B_STD = { opCBStd }, C_B_MAX = { opCBMax }, C_B_MIN = { opCBMin },
  C_C_STD = { opCCStd }, C_C_MAX = { opCCMax }, C_C_MIN = { opCCMin }, C_D_STD = { opCDStd }, C_D_MAX = { opCDMax }, C_D_MIN = { opCDMin },
  C_E_STD = { opCEStd }, C_E_MAX = { opCEMax }, C_E_MIN = { opCEMin }, C_F_STD = { opCFStd }, C_F_MAX = { opCFMax }, C_F_MIN = { opCFMin },
  F_1_STD = { opF1Std }, F_1_MAX = { opF1Max }, F_1_MIN = { opF1Min }, F_2_STD = { opF2Std }, F_2_MAX = { opF2Max }, F_2_MIN = { opF2Min },
  F_3_STD = { opF3Std }, F_3_MAX = { opF3Max }, F_3_MIN = { opF3Min },
  F_4_STD = { opF4Std }, F_4_MAX = { opF4Max }, F_4_MIN = { opF4Min }, F_5_STD = { opF5Std }, F_5_MAX = { opF5Max }, F_5_MIN = { opF5Min },
  F_6_STD = { opF6Std }, F_6_MAX = { opF6Max }, F_6_MIN = { opF6Min },
  GRANULATOR_RPM_STD = { opGranulatorRpmStd }, GRANULATOR_RPM_MAX = { opGranulatorRpmMax }, GRANULATOR_RPM_MIN = { opGranulatorRpmMin },
  SCREW_RPM_STD = { opScrewRpmStd }, SCREW_RPM_MAX = { opScrewRpmMax }, SCREW_RPM_MIN = { opScrewRpmMin },SIDE_RPM_STD = { opSideRpmStd },
  SIDE_RPM_MAX = { opSideRpmMax }, SIDE_RPM_MIN = { opSideRpmMin }, WRENCH_RATE = { opWrenchRate }, RESIN_TEMP = { opResinTemp }, DIE_PRESSURE = { opDiePressure },
  VACUUM_INDEX_STD = { opVacuumIndexStd }, VACUUM_INDEX_MAX = { opVacuumIndexMax }, VACUUM_INDEX_MIN = { opVacuumIndexMin },
  BDP_STD = { opBdpStd }, BDP_MAX = { opBdpMax }, BDP_MIN = { opBdpMin }, B_TEMP_STD = { opBTempStd }, B_TEMP_MAX = { opBTempMax }, B_TEMP_MIN = { opBTempMin },
  S_TEMP_STD = { opSTempStd }, S_TEMP_MAX = { opSTempMax }, S_TEMP_MIN = { opSTempMin }, E_TEMP_STD = { opETempStd }, E_TEMP_MAX = { opETempMax }, E_TEMP_MIN = { opETempMin },
  BDP_EXTRA_RATE = { opBdpExtraRate }, SCREEN_FREQ = { opScreenFreq }, RECORD_FREQ = { opRecordFreq },
  MFG_NOTICE = { opMfgNotice }, REMARK = { opRemark }, FILE_NAME = { opFileName }, MFG_DESC = { opMfgDesc }, UDT = GETDATE(), UPDATE_BY = { 登入者ID }
WHERE SOC_M_UID = { opSocMUid };
```

SQL-LOG:
```sql
INSERT INTO SYS_MODI_LOG (CATEGORY, DATAKEY, USERID, USER_NAME, LOG) VALUES ('SOC_M', { opSocMUid }, {登入者ID}, {登入者名稱}, '新增(修改)');
```

# Response 欄位

* 無

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content": {
      "opSocMUid":"5BBB6876-5805-4F50-B07A-CB3F6EFEEC2A",
  }
}
```