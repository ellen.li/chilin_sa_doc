# 工單維護-匯出工單excel
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230809 | 新增規格 | Ellen  |

## 來源URL及資料格式

| 項目   | 說明           |
| ------ | -------------- |
| URL    | /wo/list_excel |
| method | post           |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。  
| 欄位              | 名稱     | 資料型別 | 必填  | 來源資料 & 說明             |
| ----------------- | -------- | -------- | :---: | --------------------------- |
| 原列表REQUEST參數 | ...      | ...      |  ...  | ...                         |
| opReportType      | 報表類型 | int      |   M   | 1:工單 / 2:溢料 |


#### Request 範例

```json
{
  "opPatternNo":null,
  "opItemNo":null,
  "opWoNo":null,
  "opActiveFlag":"CRTD",
  "opBDate":null,
  "opEDate":null,
  "opOrderField": "GSTRP",
  "opOrder": "ASC",
  "opReportType": 2
}
```

# Request 後端流程說明

* 參考[OPMS_D.工單維護-取得工單列表 /wo/list](./OPMS_D.工單維護-取得工單列表.md) 以同樣方式取得資料，不需分頁。
* 若{opReportType}等於2，計算溢料量，符合下列條件才計算，不符合的不顯示
  * `{STATUS(SAP-1)}` 不等於 `DLFL`
  * `{MATNR(SAP-1)}` 開頭不為L
  * `{STAND_T(SAP-1)}` 存在`染色課/複材課/樣品課/憲成壓克粒/@ 憲成壓克粒/良輝塑膠/敬揚實業/@ 敬揚實業/@ 松旻/松旻/資堡`
  * 溢料量大於1： `{SOQTY(SAP-1)} - {H.RAW_QTY(SQL-1)}`
* 參考[OPMS_共用_excel匯出](./OPMS_共用_excel匯出.md) 將資料製成Excel檔案回傳。
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

參考[OPMS_D.工單維護-取得工單列表 /wo/list](./OPMS_D.工單維護-取得工單列表.md)

# Excel 欄位

| Eexcel欄位名稱 | 資料型別 | 資料來源 | 資料欄位和說明                        |
| -------------- | :------: | -------- | ------------------------------------- |
| 使用日期       |  string  | SAP-1    | GSTRP (YYYY/MM/DD)                    |
| 工單號碼       |  string  | SAP-1    | AUFNR                                 |
| 色號           |  string  | SQL-1    | I.PATTERN_NO                          |
| 顏色           |  string  | SAP-1    | COLOR                                 |
| 料號           |  string  | SAP-1    | MATNR                                 |
| 替代BOM        |  string  | SAP-1    | STLAL                                 |
| 原料           |  string  | SAP-1    | GRADE                                 |
| 生產單位       |  string  | SAP-1    | STAND_T                               |
| 委外標籤LOT    |  string  | SQL-1    | S.ASS_DESC                            |
| 銷售數量       |  string  | SAP-1    | SOQTY                                 |
| 工單數量       |  string  | SAP-1    | GAMNG                                 |
| 投入原料量     |  string  | SQL-1    | H.RAW_QTY                             |
| 客戶           |  string  | SAP-1    | SORT2                                 |
| 間接客戶       |  string  | SAP-1    | IHREZ                                 |
| 客戶PO單號     |  string  | SAP-1    | CUSTPO                                |
| 客戶料號       |  string  | SQL-1    | O.CUST_ITEM_NO                        |
| 溢料           |  string  |          | `{SOQTY(SAP-1)} - {H.RAW_QTY(SQL-1)}` |


