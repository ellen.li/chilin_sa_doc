# 產量回報-新增修改存檔
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

產量回報-新增修改存檔

## 修改歷程

| 修改時間 | 內容                    | 修改者 |
|----------|-----------------------|--------|
| 20230829 | 新增規格                | Ellen  |
| 20231129 | 志誠提需求需紀錄 PS_UID | Nick   |


## 來源URL及資料格式

| 項目   | 說明      |
| ------ | --------- |
| URL    | /psr/save |
| method | post      |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。

| 欄位            | 名稱        | 資料型別 | 必填 | 資料儲存 & 說明             |
| --------------- | ----------- | -------- | ---- | --------------------------- |
| opPsrUid        | 產量回報UID | string   | O    | 有值時為編輯                |
| opPsUid         | 生管排程UID | string   | O    |                             |
| opWoUid         | 工單ID      | string   | O    | 未填時為停車                |
| opEquipId       | 機台        | string   | M    |                             |
| opRepMfgDate    | 日期        | string   | M    |                             |
| opRepShiftCode  | 班別代碼    | string   | M    | A: 早 / B: 中 / C: 晚       |
| opRepShiftHours | 時數        | string   | O    |                             |
| opRepMinMfgQty  | 吐出量      | string   | O    |                             |
| opRepTimeLine   | 期間        | string   | O    |                             |
| opRepCtnDesc    | 桶次        | string   | O    |                             |
| opShiftMfgQty   | 實際產量    | string   | M    |                             |
| opShiftPreQty   | 預染數量    | string   | O    |                             |
| opWrnRemark     | 異常回報    | string   | O    |                             |
| opWrnFlag       | 是否為異常  | string   | O    | Y/N  Y: 是 / N: 否，預設`N` |


#### Request 範例
編輯
```json
{
  "opPsrUid": "713986FC-AA42-4283-B476-35D0F7587A48",
  "opWoUid": "EDED1106-8064-4922-BC40-FD8A207682AD",
  "opEquipId": "45",
  "opRepMfgDate": "2022/12/04",
  "opRepShiftCode": "B",
  "opRepShiftHours": "2.5",
  "opRepMinMfgQty": "5.85",
  "opRepTimeLine": "16:00~18:30",
  "opRepCtnDesc": "1~4",
  "opShiftMfgQty": "900",
  "opShiftPreQty": "0",
  "opWrnRemark": "",
  "opWrnFlag": "N"
}
```

停車
```json
{
  "opEquipId": "45",
  "opRepMfgDate": "2022/12/04",
  "opRepShiftCode": "B",
  "opRepShiftHours": "2.5",
  "opRepMinMfgQty": "0",
  "opRepTimeLine": "16:00~18:30",
  "opShiftMfgQty": "0",
  "opShiftPreQty": "0",
  "opWrnRemark": "",
  "opWrnFlag": "N"
}
```

# Request 後端流程說明

可參考舊版 PsManageAction.saveMfgQty
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* 若{opPsrUid}為空，執行SQL-1 新增產量回報
* 若{opPsrUid}不為空，執行SQL-2 更新產量回報
* 若{opWoUid}不為空，執行SQL-3 更新工單生產回報量
* 執行SQL-4 新增產量回報LOG
* 上述過程如有執行任何資料庫任何異動，執行 SQL-LOG 紀錄 LOG，{DATAKEY} = {opPsrUid}
  有系統錯誤 return 500,SYSTEM_ERROR,正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1: 新增產量回報
```sql
INSERT INTO PS_R (
  PSR_UID,
  WO_UID,
  EQUIP_ID,
  REP_MFG_DATE,
  REP_SHIFT_CODE,
  SHIFT_MFG_QTY,
  SHIFT_PRE_QTY,
  REP_MIN_MFG_QTY,
  REP_SHIFT_HOURS,
  REP_TIME_LINE,
  REP_CTN_DESC,
  WRN_FLAG,
  WRN_REMARK,
  CDT,
  CREATE_BY,
  UDT,
  UPDATE_BY,
  PS_UID 
)
VALUES
(
  {opPsrUid},
  {opWoUid},
  {opEquipId},
  CONVERT (DATETIME, {opRepMfgDate}),
  {opRepShiftCode},
  {opShiftMfgQty},
  {opShiftPreQty},
  {opRepMinMfgQty},
  {opRepShiftHours},
  {opRepTimeLine},
  {opRepCtnDesc},
  {opWrnFlag},
  {opWrnRemark},
  GETDATE(),
  {登入者使用者ID},
  GETDATE(),
  {登入者使用者ID},
  {opPsUid}
)
```

SQL-2: 更新產量回報
```sql
UPDATE PS_R 
SET REP_SHIFT_CODE = {opRepShiftCode},
  SHIFT_MFG_QTY = {opShiftMfgQty},
  SHIFT_PRE_QTY = {opShiftPreQty},
  REP_MIN_MFG_QTY = {opRepMinMfgQty},
  REP_SHIFT_HOURS = {opRepShiftHours},
  REP_TIME_LINE = {opRepTimeLine},
  REP_CTN_DESC = {opRepCtnDesc},
  WRN_FLAG = {opWrnFlag},
  WRN_REMARK = {opWrnRemark},
  UDT = GETDATE(),
  UPDATE_BY = {登入者使用者ID},
  PS_UID = {opPsUid} 
WHERE
  PSR_UID = {opPsrUid}
```

SQL-3: 更新工單生產回報量
```sql
UPDATE WO_H 
SET 
  MFG_QTY = (
     SELECT ISNULL( SUM( R.SHIFT_MFG_QTY ), 0 ) FROM PS_R R WHERE R.WO_UID = WO_H.WO_UID 
  ) 
WHERE
	WO_UID = {opWoUid}
```

SQL-4: 新增產量回報LOG
```sql
INSERT INTO PS_R_LOG (
  PSR_LOG_UID,
  PSR_UID,
  WO_UID,
  EQUIP_ID,
  REP_MFG_DATE,
  REP_SHIFT_CODE,
  SHIFT_MFG_QTY,
  SHIFT_PRE_QTY,
  REP_MIN_MFG_QTY,
  REP_SHIFT_HOURS,
  REP_TIME_LINE,
  REP_CTN_DESC,
  WRN_FLAG,
  WRN_REMARK,
  CDT,
  CREATE_BY,
  PS_UID 
)
VALUES
  ( 
  {產生隨機UUID},
  {opPsrUid},
  {opWoUid},
  {opEquipId},
  CONVERT(DATETIME,{}),
  {opRepShiftCode},
  {opShiftMfgQty},
  {opShiftPreQty},
  {opRepMinMfgQty},
  {opRepShiftHours},
  {opRepTimeLine},
  {opRepCtnDesc},
  {opWrnFlag},
  {opWrnRemark},
  GETDATE(),
  {登入者使用者ID},
  {opPsUid}
)
```

SQL-LOG:
```sql
INSERT INTO  SYS_MODI_LOG (CATEGORY,DATAKEY,USERID,USER_NAME,LOG) VALUES ('PS_R',{DATAKEY},{登入者使用者ID},{登入者使用者名稱}, NULL)
```

# Response 欄位
 | 欄位     | 名稱        | 資料型別 | 資料儲存 & 說明 |
 | -------- | ----------- | -------- | --------------- |
 | opPsrUid | 產量回報UID | string   |                 |


## Response 範例

```json
{
    "msgCode": null,
    "result": {
       "content":{
          "opPsrUid":"713986FC-AA42-4283-B476-35D0F7587A48"
      }
    }
}
```
