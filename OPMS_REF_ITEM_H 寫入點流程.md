

| funtction         | 觸發點         | 影響料號主檔(ITEM_H)欄位                                                           |
|-------------------|----------------|------------------------------------------------------------------------------------|
| insertItem        | saveBom、saveSo | ITEM_NO(料號),PATTERN_NO(色號),ITEM_DESC(料號摘要),UNIT(單位),WEIGHT(重量),PACKING_WEIGHT,CMDI_NO,CMDI_NAME,MTR_NAME |
| updateItem        | saveSo         | PATTERN_NO(色號),ITEM_DESC(料號摘要),MTR_NAME                                                      |
| updateItemPattern | saveBom        | PATTERN_NO(色號),ITEM_DESC(料號摘要),WEIGHT(重量),UNIT(單位)位)                                                   |


* BOM 存檔:
  * 如果料號(opItemNo)不存在料號主檔，寫入料號主檔(insertItem)，反之則更新料號主檔(updateItemPattern)
  * 原料、色料寫入時料號(opMtrPartNo、opPgmPartNo) 不存在料號主檔，寫入料號主檔(insertItem)(會從 SAP 抓料號摘要跟單位來更新及依單位來預設重量(G、0.001,KG、1)、色號 NA 。(備註:存在不更新)
* 出貨指示管理-存檔:
  * 標籤及出貨指示（表身）每一個項目，訂單編號 + 項次如不存在【訂單明細表(SO_D)】，則確認表身料號是否有在【料號主檔(ITEM_H)】，沒有則寫入(insertItem)料號主檔(資料來自SAP，UNIT強制KG，單位1)，有則更新(updateItem)料號主檔
  
* 料號摘要必須為必填，因為 ITEM_H 會新增到

註記:
BOM　patternNo 無檢核 資料庫   可 null  (你們色號一定會輸入嗎? 資料庫看起來一定會，以前沒檔，但是  ITEM_H 裡面也沒有 null 的)
BOM　itemDesc  無檢核 資料庫   可 null 但  ITEM_H 不可 null
BOM　unit 　　 單位   資料庫可  null
BOM　weight 　 重量   資料庫可  null


```mermaid
graph TD
    A[BOM存檔] -->B{"料號(opItemNo)<br>存在料號主檔?"}
    B -->|存在| D["更新【料號主檔】(updateItemPattern)"]
    B -->|不存在| C["寫入【料號主檔】(insertItem)"]
    A --> E{"原料、色料<br>存在料號主檔?"}
    E -->|不存在| C["寫入【料號主檔】(insertItem)"]
    A1["標籤及出貨指示管理存檔"] --> B1{"訂單編號 + 項次<br>存在【訂單明細表(SO_D)】?"}
    B1 -->|不存在| C1{"表身料號<br>存在料號主檔?"}
    B1 -->|存在| D1["其他流程"]
    C1 -->|不存在| C["寫入【料號主檔】(insertItem)"]
    C1 -->|存在| F["更新【料號主檔】(updateItem)"]
```

---

```mermaid
graph TD
    A[BOM存檔] -->B{"料號(opItemNo)<br>存在料號主檔?"}
    B -->|存在| D["更新【料號主檔】(updateItemPattern)"]
    B -->|不存在| C["寫入【料號主檔】(insertItem)"]
    A --> E{"原料、色料<br>存在料號主檔?"}
    E -->|不存在| C["寫入【料號主檔】(insertItem)"]
```
---
```mermaid
graph TD
    A1["標籤及出貨指示管理存檔"] --> B1{"訂單編號 + 項次<br>存在【訂單明細表(SO_D)】?"}
    B1 -->|不存在| C1{"表身料號<br>存在料號主檔?"}
    B1 -->|存在| D1["其他流程"]
    C1 -->|不存在| E1["寫入【料號主檔】(insertItem)"]
    C1 -->|存在| F1["更新【料號主檔】(updateItem)"]
```