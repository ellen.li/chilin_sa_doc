# OPMS_D.工單列印-生產指示書
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

列印生產指示書


## 修改歷程

| 修改時間  |  內容            | 修改者  |
| -------- | --------------- | ------ |
| 20230809 | 新增規格          | Sam    |
| 20230810 | 補充說明後端邏輯   | Sam    |
| 20230811 | 補充說明後端邏輯   | Sam    |

## 來源URL及資料格式

| 項目    | 說明           |
| ------ | -------------- |
| URL    | /wo/print_wo_d |
| method | post           |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位

M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。

| 欄位           | 名稱                       |   資料型別    | 必填  | 資料儲存 & 說明               |
| ------------- | ------------------------- | :----------: | :---: | --------------------------- |
| opWoUidList   | 工單 UID                   | array string |   M   |                            |



#### Request 範例
```json
{
  "opWoUidList": [
    "00006D90-B5F1-4A80-9F62-B9989EBAE168", 
    "00008BEB-34CC-482D-B52E-AC3E0498AD4E"
    ]
}
```

# Request 後端流程說明

請參考舊版 `WebContent\wo\WoPrintD.jsp`
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性

* {organizationId} 為 登入者工廠別 ID
* 遍歷所有需列印的工單，{woUid} 為單筆工單 ID，{woNo}為單筆工單號碼
<!-- * 執行 SAP-1 取得工單詳細資料，覆寫SQL-1結果各欄位值 -->
  * 對於每一張工單
    * 執行 SQL-1，取得需列印的工單，確認傳入工單ID是否已存在，如果不存在 return 400 NOTFOUND
    * 執行 SQL-2 取得工單原料清單 mtrList

    * 判斷是否為委外廠 isOutsource 預設為 false
      如果工單類別 (wipClassNo) 開頭為 "Z58" 或 "Z59" 字樣，則為委外廠，設定 isOutsource=true

    * 重算原料總重 (用來計算用料需求中,各料號使用之%)
      預設原料總重量 totalMtrWeight 為 0
      若原料清單 mtrList 不為 null，則對每個原料中 
        - 若 {WO_MTR_PART_NO} 為 "92" 開頭表示其為包材，故跳過，不列入原料總務重計算
        - 若 {UNIT} 為 "G"，表示單位為公克，則將每一個原料的 {QUANTITY_PER} 的值除以 1000 後取小數點後5位，四捨五入
          其餘則直接將每一個原料的 {QUANTITY_PER} 累加於 原料總重量 totalMtrWeight 

     * 執行 SQL-3 取得工單色料清單 取得色料清單 getWoPgmListExpand，參考 [OPMS_共用_N4計算公式](./OPMS_共用_N4計算公式.md) 計算色料總合
    
    * batchQty = 工單的 {H.BATCH_QTY} (SQL-1)
      batchTimes = 工單的 {H.BATCH_TIMES} (SQL-1)
      remainQty = 工單的 {H.REMAIN_QTY} (SQL-1)
      remainTimes = 工單的 {H.REMAIN_TIMES} (SQL-1)
      如果 remainTimes < 0 且 remainQty > 0，則 remainQty = 0
      
    * pars 塞入工單資料 

    * 計算理論損失率
      若工單 {PRODUNIT_NO} 不為 null 
        
        若工單 {PRODUNIT_NO} 等於 "P3"，則為染色課
          - 染色課-理論損失率計算方式
              dLoss = (25*100/工單的{RAW_QTY})，四捨五入後取到小數點第三位 + 0.5
        
        若工單 {PRODUNIT_NO} 等於 "P1" 且 {EQUIP_ID} 不為 null，則為複材 
          - 複材-理論損失率計算方式
              宣告變數 init，CONSTANT
              若 {EQUIP_ID} <= 13，則 init = 10，CONSTANT = 1.5
              否則 init = 30，CONSTANT = 0.65
              dLoss = (init*100/工單的{RAW_QTY})，四捨五入後取到小數點第三位 + CONSTANT
      
        - 設定 損失率 stdLossRate = dLoss 取小數點後三位
    
    * ERP 工單資料
      <!-- - 以 requirementList List  -->
      - 執行 (SAP-1) 取得 ERP 工單資料 <!-- SapWoTo entitiesTo=rfc.getWipEntities(toH.getOrganizationId(), toH.getWoNo(),requirementList); -->
      - 若 ERP 工單資料不為 null，則
        
        - 預設 備註 prodRemark 為空字串 ""
        
        - 設定 出貨指示 
          - 出貨指定需顯示於生產指示書之項目 執行(SQL-10) assign 給 sisConfigList 變數
          - 工單出貨指示 (SQL-11) assign 給 sisList 變數
          - 遍歷 sisConfigList ...
            - 取出 sisConfigList 中每一個項目 sisConfigTo，並且宣告 showText 為空字串
            - 遍歷 sisList ...
             
              - 若 sisList 內的每一個出貨指示 sisTo {LIST_HEADER_ID} = sisConfigTo {LIST_HEADER_ID}，則
                
                - 宣告 sText 為 sisTo 的 {SHOW_TEXT} 值
                - 若 sText 不為空字串也不為"不給指示" ，則 showText = sisTo {CATEGORY_ATTRI} + "***" + sText
                - 跳出迴圈 break
              
              - 若 showText 不為空字串
                - 若 prodRemark 為空字串，則回傳空字串，否則回傳 "\r\n" 
                - prodRemark = 上述 prodRemark + showText
          
        - LS-加註客戶料號、料號摘要
          - 若此工單的 {ORGANIZATION_ID} = "6000" 且 此工單的 {SALES_ORDER_NO} 不為null也不為空字串 且 此工單的 {SALES_ORDER_SEQ} 不為null也不為空字串
            
            - 以 {SALES_ORDER_NO} 與 {SALES_ORDER_SEQ} 做為參數執行 SQL-4 取得 SoD，並且 assign sodto 變數 

            - 從此一 SoD 取得 PatternNo 
              若 SoD 的 PatternNo 不為 null 值，不為 空字串 ""，也不為 "null" 字串，則回傳該 PatternNo ，否則回傳字串 "空白"
              若備註 prodRemark 為空字串 "" ，則回傳 ""，否則回傳 "\r\n" 
              並將新備註值 prodRemark = 上述 prodRemark + "生產***色號：" + SoD 的 PatternNo

            - 從此一 SoD 取得 CustItemNo 
              若 SoD 的 CustItemNo 不為null值，不為空字串 ""，也不為"null"字串，則回傳該 CustItemNo ，否則回傳字串 "空白"
              若備註 prodRemark 為空字串 "" ，則回傳 ""，否則回傳 "\r\n" 
              並將新備註值 prodRemark = 上述 prodRemark + "生產***客戶料號：" + SoD 的 CustItemNo

            - 從此一 SoD 取得 MtrName 
              若 SoD 的 MtrName 不為null值，不為空字串 ""，也不為"null"字串，則回傳該 MtrName ，否則回傳字串 "空白"
              若備註 prodRemark 為空字串 "" ，則回傳 ""，否則回傳 "\r\n" 
              並將新備註值 prodRemark = 上述 prodRemark + "生產***原料：" + SoD 的 MtrName
            
            - 從此一 SoD 取得 ItemDesc 
              若 SoD 的 ItemDesc 不為null值，不為空字串 ""，也不為"null"字串，則回傳該 ItemDesc ，否則回傳字串 "空白"            
            - pars 加上 "itemDesc","料號摘要:" + ItemDesc

        * 生產指示備註
          - 若該工單 {ORGANIZATION_ID} = "6000"，則
            若備註 prodRemark 為空字串 "" ，則回傳 ""，否則回傳 "\r\n" 
            將新備註值 prodRemark = 上述 prodRemark + "檢驗***備註 : "
          
          - 若備註 prodRemark 不為空值，則
            若備註 prodRemark 為空字串 "" ，則回傳 ""，否則回傳 "\r\n" 
            將新備註值 prodRemark = 上述 prodRemark + "檢驗***" + 上述 prodRemark 中 置換所有的 "\r\n" 為 "\n" 以及 置換所有的 "\n" 為 "\r\n"
          
          - 將 prodRemark 以 "\r\n" 切割之後，塞入 String 陣列變數 aryProdRemark 中 (aryProdRemark 即為 prodRemark 以 "\r\n"切割後的陣列結果)
          
          - 若此一工單為奇菱 {ORGANIZATION_ID} = "5000"
              - 遍歷整個 aryProdRemark 
                - 宣告 Map<String,String> m 
                - 以 string s1 接收 aryProdRemark 中 index i  的內容 
                - 若 s1 字串中，"***" 的 index > 0，則 取自 (\*\*\* 的index+3) 的子字串字串為新的 s1 字串內容
                - Map m 放入參數 ("sis1", s1)
                - 遞增 i 

                - 若 i < aryProdRemark 陣列長度，則
                  - 以 string s2 接收 aryProdRemark 中 index i  的內容 
                  - 若 s2 字串中，"***" 的 index > 0，則 取自 (\*\*\* 的index+3) 的子字串字串為新的 s2 字串內容
                  - Map m 放入參數 ("sis2", s2)
                - 遞增 i 

                - 若 i < aryProdRemark 陣列長度，則
                  - 以 string s3 接收 aryProdRemark 中 index i  的內容 
                  - 若 s3 字串中，"***" 的 index > 0，則 取自 (\*\*\* 的index+3) 的子字串字串為新的 s3 字串內容
                  - Map m 放入參數 ("sis3", s3)
                
                - 將 Map m 加入 subList 中

          - 若為菱翔 {ORGANIZATION_ID} = "6000"
              - 寫入區塊標題
              - 宣告 Map<String, String> mTitle
              - 寫入 包裝 標籤 檢驗 等區塊標題
                - Map mTitle 放入參數 ("sis1", "包裝：")
                - Map mTitle 放入參數 ("sis2", "標籤：")
                - Map mTitle 放入參數 ("sis3", "檢驗：")
                - 將 Map mTitle 加入 subList 中

              - 依包裝、標籤(生產)、檢驗做分類
                - 宣告 List<String> s1, s2, s3
                - 遍歷 aryProdRemark 
                  - 以 string sis 接收 aryProdRemark 中 index i 的內容 
                  - 若 sis 字串開頭為 "包裝"，則 取 aryProdRemark 中 index i 的內容，取其中自 (\*\*\* 字串的 index )+3 的子字串，並且加入至 s1 String List 當中 [ 原程式 : s1.add(sis.substring(aryProdRemark[i].indexOf("***")+3)) ]
                  - 若 sis 字串開頭為 "生產"，則 取 aryProdRemark 中 index i 的內容，取其中自 (\*\*\* 字串的 index )+3 的子字串，並且加入至 s2 String List 當中 [ 原程式 : s2.add(sis.substring(aryProdRemark[i].indexOf("***")+3)) ]
                  - 若 sis 字串開頭為 "檢驗"，則 取 aryProdRemark 中 index i 的內容，取其中自 (\*\*\* 字串的 index )+3 的子字串，並且加入至 s3 String List 當中 [ 原程式 : s3.add(sis.substring(aryProdRemark[i].indexOf("***")+3)) ]

              - 寫入指示 List
                - 遍歷 aryProdRemark 
                  - 宣告 Map<String, String> m 為HashMap
                  - 

        * 出貨指示備註印於包裝指示書 (參考舊程式 line 319-328)

        * 2021-11-01 以後，停止列印trouble情報 

        * 若工單 {ORGANIZATION_ID} = "6000"
          * LS顯示工單數量 
          - pars 塞入參數 woQty 取 entitiesTo {START_QTY} + 工單的 {UNIT}
          - pars 塞入參數 woQtyTitle "工單數量"

          否則 {ORGANIZATION_ID} = "5000"
          - 取工單 {SALES_QTY} 若為null值，則回傳 "N/A"，否則回傳 {SALES_QTY}
          - pars 塞入參數 woQty 取工單 {SALES_QTY} + 工單的 {UNIT}
          - pars 塞入參數 woQtyTitle "訂單數量"

          - 無論 {ORGANIZATION_ID} 為 "5000" 或 "6000"
            - pars 塞入參數 rawQty 工單 {RAW_QTY}
            - 若 {CUST_PO_NO} 為 null，則回傳空字串，否則回傳 {CUST_PO_NO}
            - pars 塞入參數 custPoNo 工單 {CUST_PO_NO} 
            - 若 {ESTIMATED_SHIP_DATE} 為 null，則回傳空字串，否則回傳 {ESTIMATED_SHIP_DATE}
            - pars 塞入參數 shipDate 工單 {ESTIMATED_SHIP_DATE}

        * 設定原料投入量 wipQty = {RAW_QTY} 
          - 初始化以下變數為 0
            - batchQtyA remainQtyA totalQtyA
            - batchQtyB remainQtyB totalQtyB
            - batchQtyC remainQtyC totalQtyC
            - batchSum remainSum totalSum

        * (code line 386 - 629)

        * pars 塞入以下參數，並且精度為小數點後三位
          - batchQtyA remainQtyA totalQtyA
          - batchQtyB remainQtyB totalQtyB
          - batchQtyC remainQtyC totalQtyC
          - batchSum  remainSum  totalSum

        * N4
          - n4Per = 工單 {CUS_N_QUANTITY_PER}
                
        * 建BOM單位
          - {BomUid} 當參數帶入 (SQL-12) 找出其 UpdateBy {USER_ID}
          - 以上述 {USER_ID} 當參數帶入 (SQL-12-1) 找出其 {DEPT_NAME}
          - bomDep = {DEPT_NAME}                    
          - pars 塞入以下參數 bomDep bomDep 

        * 展開色粉中間料
          - 若工單的 {ORGANIZATION_ID} = "5000"，則
            - 取得色料清單 pgmList 以工單 {WoUid} 為參數帶入，執行 (SQL-3)
            - 若 pgmList 不為 null，則
              - kSize 為 pgmList size 大小
              - 遍歷所有 pgmList 
                - 取出每個 pgmList 的色料
                - pgmQuantityPer = 每個色料的 {QuantityPer}
                - 若每個色料的 {PgmBomUid} 不為 null，則 
                  - 有子階，N4重新計算 n4Per 設為空字串
                  - 取得原料，執行 (SQL-12) getBomMtmSingle(to.getPgmBomUid())
                  - bomPgmList getBomPgmList(to.getPgmBomUid())
                  - 若 bomPgmList 不為 null，則
                    - 遍歷 bomPgmList 
                      - 取得每個 bomPgmList 中的色料
                      - 設定 bomPgmQuantityPer 
                      - 設定 newQuantityPer = pgmQuantityPer * bomPgmQuantityPer
                      - 若中間料用量以G計，但其BOM以KG表示，則單位用量/1000
                      - 若中間料用量以KG計，但其BOM以G表示，則單位用量*1000
                      - 加入新的色料進 pgmList
                  - 若 bomPgmList 為 null，則
                    - 若該中間料沒有子階資料，則清除其BomUid==>待會會用來作為刪除該中間料之flag
              - 已展開中間料之色料，刪除原中間料 (loop)

        * 參考舊程式 calculatePgm (待)
          - 設定參數 
            - compClass "D"
            - subinv ""
            - locator ""
            - compItem "N-4"
            - quantityPer ""
            - batchQty 
            - batchTimes
            - remainQty
            - remainTimes
            - totalQty
            - compItemUom "G"

        * 是否為委外廠
          - 若不為委外廠 (!isOutsource)，則
            - 檢查是否有下料設備
            - 帶入 {WoUid} 以及工單的 {EquipId} 並執行 (SQL-14) getWoFedWithEquip(woUid,toH.getEquipId())
            - 若 fedTo 不是 null，則
              - tempList 加入 openListA openListCD
              - 清除 openListA
              - feederRate : F1~F6 rate % 以及 BDP rate%
              - 以 {WoUid} {EquipId} 執行 (SQL-17) getWoFedDWithEquip(woUid, fedTo.getEquipId())
              - 遍歷所有的 tempList
                - 尋找下料設備
                - 如果有找到 且 下料設備有2筆以上才需要拆分
            - 工單備註
              - {CUST_NAME}  若為null，則回傳空字串，否則回傳 {CUST_NAME}  (SQL-1)
              - {INDIR_CUST} 若為null，則回傳空字串，否則回傳 {INDIR_CUST} (SQL-1)
              - {SALES_NO}   若為null，則回傳空字串，否則回傳 {SALES_NO}   (SQL-1)
              - {CUST_NO}    若為null，則回傳空字串，否則回傳 {CUST_NO}    (SQL-1)
              - strRemark = {CUST_NAME} + "-" + {INDIR_CUST} + ";" + {SALES_NO} + ";" + {CUST_NO}
              - pars 塞入參數 woRemark strRemark
              - pars 塞入參數 isQOPattern "N"
              - pars 塞入參數 isOutsource "N"
          - 若為委外廠 (isOutsource)，則
            - 客戶欄位只能顯示代號
              - pars 塞入參數 custName {H.CUST_NO} (SQL-1) <!--客戶欄位只能顯示代號 -->
            - 工單備註
              - {SALES_NO} 若為null，則回傳空字串，否則回傳 {SALES_NO} (SQL-1)
              - {CUST_NO}  若為null，則回傳空字串，否則回傳 {CUST_NO}  (SQL-1)  //不顯示客戶名稱 
              - strRemark = {SALES_NO} + ";" + {CUST_NO}
            - pars 塞入參數 woRemark strRemark
            - pars 塞入參數 isOutsource "Y"		
            - 加嚴檢驗
              - 以工單的 {PATTERN_NO} 做為參數，執行 (SQL-13) (查詢色號是否是加嚴檢驗色號 checkIsQOPattern(String patternNo))
              - 若工單的 {PATTERN_NO} 與 (SQL-13) 回傳的值相同，則為 true，否則為 false
              - 若回傳 true，則 pars 塞入參數 isQOPattern "Y"，否則 塞入參數 isQOPattern "N"

        * 攪拌方式 
          - 以 {WoUid} 以及工單的 {Equip_Id} 為參數帶入，執行 (SQL-16) getWoMixDescWithEquip(woUid, toH.getEquipId())
          - 以 mixDesc 預設為空字串，若上述回傳不為 null，則 mixDesc = (SQL-16) 回傳的 {MIX_DESC}
          - 若 mixDesc 為 null 值 或 空字串 或 "null" 字串，則執行 (SQL-18)，則 mixDesc 為 (SQL-18) 回傳 {MIX_DESC}
          - 若 mixDesc 不為 null 值 或 不為空字串 或 不為 "null" 字串
            - 初始 strOrder = ""，iMax = 0 
            - 取得 比例 公斤數 整包數 以及 差異數=差異數+公斤數-整包之公斤數
            - 最後一筆要直接給餘數
            - 計算 N4
            - 將 mixDesc 中所有 "\r\n" "\n" 置換成 " "
            - pars 塞入參數 mixDesc "投料方式:" + mixDesc

        * SOC 
          - 執行 SQL-7 查詢工單已建SOC機台 ( 參考原程式 getWoSoc(woUid) )
            取得每個機台的 {EQUIP_NAME}，並用字串 socEequipName 將所有 {EQUIP_NAME} 串接起來
            若 socEequipName 為空字串，則回傳 "無"，否則回傳 socEequipName 內容
          - pars 塞入參數 socRemark "SOC已建機台:" + (socEequipName)

        * 攪拌方式 
          - 執行 SQL-8 查詢工單BOM是否設定攪拌方式 ( 參考原程式 getWoMixEquip(woUid) ) 
            取得每個機台的 {EQUIP_NAME}，並用字串 socEequipName 將所有 {EQUIP_NAME} 串接起來
            若 {EQUIP_NAME} 為 null，則回傳 "不指定"，否則回傳 {EQUIP_NAME}
          - mixEquipName 將上述 {EQUIP_NAME} 以 "，" 串接起來
            若 mixEquipName 為空字串，則回傳 "無"，否則回傳 mixEquipName 內容
          - pars 塞入參數 mixRemark "攪拌方式已建機台:" + (mixEquipName)
          - 查詢工單適用生產機台 getWoEquip
            - equipY = 執行 SQL-8-1 帶入參數 {woUid} "Y"
            - equipN = 執行 SQL-8-1 帶入參數 {woUid} "N"
            - equipRemark 預設為空字串
              - 工單 {BIAXIS_ONLY} 為 null 或是 "N"，則回傳空字串，否則回傳 "★限雙軸生產　"
              - 工單 {DYE_ONLY} 為 null 或是 "N"，則回傳空字串，否則回傳 "★只染不押　"
              - "BOM適用機台:"
              - equipY 若為空字串，則回傳 "無"，否則回傳 equipY 值
              - ";不適用機台:" 
              - equipN 若為空字串，則回傳 "無"，否則回傳 equipN 值
              - equipRemark 將上述字串依序串接起來
            - pars 塞入參數 equipRemark equipRemark
    
* 執行JASPER
* 更新 WO_H 的 PRINT_FLAG_D (參考SQL-9)

* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SAP-1：
可參考舊版 WoQueryRfc_getWoHSingle
程序如有任何異常，紀錄錯誤LOG並回傳NULL

**獲得BAPI方法**
|              | 型態    | 參數值                      | 說明  |
| ------------ | ------ | -------------------------- | ---- |
| BAPIFunction | string | Z_PP_OPMS_PRODUCTION_ORDER |      |

**設定輸入參數**
| 參數名稱  | 參數值            | 型態          | 說明  |
| -------- | ---------------- | ------------ | ---- |
| I_WERKS  | {organizationId} | string       |      |
| I_AUFNR  | {woNo}           | string       |      |
| I_ERDAT  |                  | JCoStructure |      |

I_ERDAT JCoStructure 參數
| 參數名稱   | 參數值      | 型態   | 說明 |
| -------- | ---------- | ------ | ---- |
| ERDAT_FR | "00000000" | string |      |
| ERDAT_TO | "99991231" | string |      |


| 取得回傳表格名稱    |
| ---------------- |
| T_ORDER          |

| 物件元素名稱        | 資料型別   | SAP欄位名稱  | SAP資料型別  | 物件元素回傳值   | 說明                                                         |
| ----------------- | -------- | ----------- | ----------- | -------------- | ------------------------------------------------------------ |
| WoNo              | string   | AUFNR       | string      | 同SAP          | 工單號碼，過濾前綴0                                          |
| activeFlag        | string   | STATUS      | string      | 同SAP          | 狀態                                                         |
| woDate            | string   | GSTRP       | Date        | 同SAP          | 使用日期，format yyyy/MM/dd                                  |
| color             | string   | COLOR       | string      | 同SAP          | 顏色                                                         |
| patternNo         | string   | COLOR_NO    | string      | 同SAP          | 色號                                                         |
| itemNo            | string   | MATNR       | string      | 同SAP          | 料號                                                         |
| itemDesc          | string   | MAKTX       | string      | 同SAP          | 料號摘要                                                     |
| bomNo             | string   | STLAL       | string      | 同SAP          | BOM                                                          |
| gradeNo           | string   | GRADE       | string      | 同SAP          | 原料                                                         |
| produnitNo        | string   | STAND       | string      | 同SAP          | 生產單位代碼，過濾前綴0，若為空顯示"NA"                      |
| produnitName      | string   | STAND_T     | string      | 同SAP          | 生產單位                                                     |
| salesNo           | string   | ERNAM       | string      | 同SAP          | 業助代號                                                     |
| custNo            | string   | KUNNR       | string      | 同SAP          | 客戶代號，過濾前綴0                                          |
| custName          | string   | SORT2       | string      | 同SAP          | 客戶名稱                                                     |
| indirCust         | string   | IHREZ       | string      | 同SAP          | 間接客戶                                                     |
| custPoNo          | string   | CUSTPO      | string      | 同SAP          | 客戶PO單號                                                   |
| salesQty          | string   | SOQTY       | double      | 同SAP          | 銷售數量，取至小數點後第五位                                 |
| wipClassNo        | string   | AUART       | string      | 同SAP          | 工單類別                                                     |
| wipClassCode      | string   | AUART_T     | string      | 同SAP          | 工單類別Code                                                 |
| sapOperation      | string   | VORNR       | string      | 同SAP          | 過濾前綴0                                                    |
| sapWc             | string   | ARBPL       | string      | 同SAP          | 工作中心                                                     |
| equipName         | string   | ARBPL_T     | string      | 同SAP          | 指定機台                                                     |
| woQty             | string   | GAMNG       | double      | 同SAP          | 工單數量，取至小數點後第五位                                 |
| rawQty            | string   | GAMNG       | double      | 同SAP          | 投入原料量，若H.RAW_QTY(SQL-1)為空才取值，取至小數點後第五位 |
| unit              | string   | GMEIN       | string      | 同SAP          | 單位                                                         |
| estimatedShipDate | string   | SHIP_DATE   | date        | 同SAP          | 預計出貨日                                                   |
| completionSubinv  | string   | LGORT       | string      | 同SAP          | 完工倉別                                                     |
| salesOrderNo      | string   | KDAUF       | string      | 同SAP          | 銷售訂單，過濾前綴0                                          |
| salesOrderSeq     | string   | KDPOS       | string      | 同SAP          | 銷售訂單序號，過濾前綴0                                      |
| cmcSoType         | string   | CMC_SO_TYPE | string      | 同SAP          | 1: 奇美內銷(大黃標籤) /  2: 奇美外銷(外銷標籤) / 3: 其他     |


| 取得回傳表格名稱 |
| ---------------- |
| T_COMP           |

| 物件元素名稱   | 資料型別   | SAP欄位名稱  | SAP資料型別  | 物件元素回傳值   | 說明  |
| ------------ | -------- | ----------- | ----------- | -------------- | ---- |
| compItem     | string   | IDNRK       | string      | 同SAP          |      |
| compItemUom  | string   | MEINS       | string      | 同SAP          |      |
| requiredQty  | string   | BDMNG       | string      | 同SAP          |      |
| issuedQty    | string   | ENMNG       | string      | 同SAP          |      |
| subinv       | string   | LGORT       | string      | 同SAP          |      |
| locator      | string   | CHARG       | string      | 同SAP          |      |

SQL-1:
```sql
-- getWoHSingleByUid / queryWoHSingle
SELECT
  H.WO_UID,
  H.ACTIVE_FLAG,
  H.BOM_UID,
  H.EQUIP_ID,
  H.ORGANIZATION_ID,
  H.WO_NO,
  H.WO_SEQ,
  H.LOT_NO,
  H.CUST_PO_NO,
  CONVERT ( VARCHAR ( 10 ), H.WO_DATE, 111 ) AS WO_DATE,
  H.PRODUNIT_NO,
  H.CUST_NO,
  H.CUST_NAME,
  H.INDIR_CUST,
  B.CUST_NAME AS BASE_CUST,
  H.UNIT,
  H.WEIGHT,
  B.CMDI_NO,
  B.CMDI_NAME,
  B.COLOR,
  H.[ USE ],
  H.SALES_NO,
CASE
  H.RAW_QTY 
  WHEN cast( H.RAW_QTY AS INT ) THEN
  cast( CONVERT ( INT, H.RAW_QTY ) AS VARCHAR ( 20 ) ) ELSE cast( CONVERT ( FLOAT, H.RAW_QTY ) AS VARCHAR ( 20 ) ) 
  END RAW_QTY,
CASE
  H.BATCH_QTY 
  WHEN cast( H.BATCH_QTY AS INT ) THEN
  cast( CONVERT ( INT, H.BATCH_QTY ) AS VARCHAR ( 20 ) ) ELSE cast( CONVERT ( FLOAT, H.BATCH_QTY ) AS VARCHAR ( 20 ) ) 
  END BATCH_QTY,
  H.BATCH_TIMES,
CASE
  H.REMAIN_QTY 
  WHEN cast( H.REMAIN_QTY AS INT ) THEN
  cast( CONVERT ( INT, H.REMAIN_QTY ) AS VARCHAR ( 20 ) ) ELSE cast( CONVERT ( FLOAT, H.REMAIN_QTY ) AS VARCHAR ( 20 ) ) 
  END REMAIN_QTY,
  H.REMAIN_TIMES,
  H.MODIFY_FLAG,
  H.PRINT_FLAG,
  H.REMARK,
  H.BOM_REMARK,
  H.ERP_FLAG,
  H.WIP_CLASS_CODE,
  H.ALT_BOM_PART_NO,
  H.ALT_BOM_DESIGNATOR,
  H.ALT_ROUTING_PART_NO,
  H.ALT_ROUTING_DESIGNATOR,
  H.COMPLETION_SUBINV,
  H.COMPLETION_LOCATOR,
  H.SALES_ORDER_NO,
  H.SALES_QTY,
  H.OEM_ORDER_NO,
  CONVERT ( VARCHAR ( 10 ), H.ESTIMATED_SHIP_DATE, 111 ) AS ESTIMATED_SHIP_DATE,
  I.PATTERN_NO,
  B.ITEM_NO,
  B.ITEM_DESC,
  B.BOM_NO,
  B.BOM_NAME,
  B.BOM_DESC,
  B.[ USE ],
  B.BASE_ITEM_NO,
  B.BASE_BOM_NO,
  H.STD_N_QUANTITY_PER,
  H.CUS_N_QUANTITY_PER,
  REPLACE ( CONVERT ( VARCHAR ( 16 ), H.CDT, 120 ), '-', '/' ) AS CDT,
  H.CREATE_BY,
  REPLACE ( CONVERT ( VARCHAR ( 16 ), H.UDT, 120 ), '-', '/' ) AS UDT,
  H.UPDATE_BY,
  B.MULTIPLE BOM_MULTIPLE,
  H.CTN_WEIGHT,
  H.PROD_REMARK,
  H.SALES_ORDER_SEQ,
  B.BIAXIS_ONLY,
  B.DYE_ONLY,
  H.GRADE_NO,
  H.PROD_REMARK,
  H.PS_FLAG,
  H.WO_TYPE 
FROM
  WO_H H
  INNER JOIN BOM_MTM B ON H.BOM_UID = B.BOM_UID
  INNER JOIN ITEM_H I ON B.ITEM_NO = I.ITEM_NO 
WHERE
  H.WO_UID = {woUid}
```

SQL-2: 取得原料清單
```sql
-- getWoMtrList
SELECT
  R.WO_MTR_PART_NO,
CASE
  R.QUANTITY_PER 
  WHEN cast( R.QUANTITY_PER AS INT ) THEN
  cast( CONVERT ( INT, R.QUANTITY_PER ) AS VARCHAR ( 20 ) ) ELSE cast( CONVERT ( FLOAT, R.QUANTITY_PER ) AS VARCHAR ( 20 ) ) 
  END QUANTITY_PER,
  R.UNIT 
FROM
  WO_MTR R 
WHERE
  R.WO_MTR_PART_NO NOT LIKE '92%' 
  AND R.WO_UID = {woUid}
ORDER BY
  R.QUANTITY_PER DESC,
  R.WO_MTR_PART_NO
```


SQL-3: 取得色料清單 getWoPgmListExpand
```sql
-- getWoPgmListExpand
SELECT
	A.WO_PGM_PART_NO,
	A.QUANTITY_PER,
	A.UNIT,
	A.SITE_PICK,
	A.SELF_PICK,
	A.PGM_BATCH_QTY,
	A.PGM_REMAIN_QTY,
	A.MODIFY_QTY,
	A.MODIFY_UNIT_QTY,
	A.FEEDER_SEQ,
	B.BOM_UID AS PGM_BOM_UID 
FROM (
    SELECT
      H.ORGANIZATION_ID,
      W.WO_PGM_PART_NO,
      W.QUANTITY_PER,
      W.UNIT,
      W.SITE_PICK,
      W.SELF_PICK,
      W.PGM_BATCH_QTY,
      W.PGM_REMAIN_QTY,
      W.MODIFY_QTY,
      W.MODIFY_UNIT_QTY,
      W.FEEDER_SEQ 
    FROM
      WO_PGM W
    INNER JOIN WO_H H 
      ON W.WO_UID = H.WO_UID 
    WHERE
      W.WO_UID = {woUid} 
  ) A
LEFT JOIN ( 
    SELECT 
      ORGANIZATION_ID, 
      ITEM_NO, 
      BOM_UID 
    FROM BOM_MTM 
    WHERE BOM_NO IS NULL AND ACTIVE_FLAG = 'Y' 
  ) B 
  ON (A.ORGANIZATION_ID = B.ORGANIZATION_ID AND A.WO_PGM_PART_NO = B.ITEM_NO) 
ORDER BY
	WO_PGM_PART_NO
```

SQL-4: 取得 SoD
```sql
SELECT 
  D.SO_NO, D.SO_SEQ, D.STATUS, D.ITEM_NO, D.ITEM_DESC, 
  D.CUST_ITEM_NO, D.QTY, D.QTY, D.PATTERN_NO, D.COLOR, 
  D.MTR_NAME, D.DEEP, D.WIDTH, D.LENGTH, D.SPEC, D.LBL_PATTERN_NO, 
  D.LBL_COLOR, D.LBL_MTR_NAME, D.LBL_DEEP, D.LBL_WIDTH, D.LBL_LENGTH, 
  D.LBL_SPEC, D.DD_CUSINV, D.ERP_FLAG, D.SIS_FLAG, 
  REPLACE(CONVERT(VARCHAR(19),D.CDT,120),'-','/') CDT, D.CREATE_BY, REPLACE(CONVERT(VARCHAR(19),D.UDT,120),'-','/') UDT, D.UPDATE_BY 
FROM SO_D D 
WHERE D.SO_NO={SALES_ORDER_NO} AND D.SO_SEQ={SALES_ORDER_SEQ}
```



SQL-6: 取得出貨指示
```sql
SELECT 
  S.OPT_DESC+ ISNULL(S.ASS_DESC, '') AS SHOW_TEXT 
FROM
  WO_H W
  INNER JOIN WO_SIS S ON W.WO_UID= S.WO_UID 
  INNER JOIN SYS_SIS_H H ON W.LIST_HEADER_ID = S.LIST_HEADER_ID
WHERE
  W.WO_UID = {woUid}
  AND H.ORGANIZATION_ID = {organizationId}
  AND H.R_OPMS_COLOR = 'Y' 
  AND H.ACTIVE_FLAG = 'Y' 
ORDER BY
  S.LIST_SEQ
```


SQL-7 : 查詢工單已建SOC機台
```sql 
-- 查詢工單已建SOC機台 getWoSoc(woUid)
SELECT E.EQUIP_ID,E.EQUIP_NAME 
FROM WO_H W 
INNER JOIN BOM_MTM B 
  ON W.BOM_UID=B.BOM_UID 
INNER JOIN SOC_C C 
  ON B.ITEM_NO=C.ITEM_NO 
INNER JOIN EQUIP_H E 
  ON B.ORGANIZATION_ID=E.ORGANIZATION_ID AND C.EQUIP_ID=E.EQUIP_ID 
WHERE W.WO_UID={woUid} AND C.STATUS='Y' 
UNION 
SELECT E.EQUIP_ID,E.EQUIP_NAME 
FROM WO_H W 
INNER JOIN BOM_MTM B 
  ON W.BOM_UID=B.BOM_UID 
INNER JOIN SOC_M C 
  ON B.ITEM_NO=C.ITEM_NO 
INNER JOIN EQUIP_H E 
  ON B.ORGANIZATION_ID=E.ORGANIZATION_ID AND C.EQUIP_ID=E.EQUIP_ID
WHERE W.WO_UID={woUid} AND C.STATUS='Y' 
ORDER BY EQUIP_ID

```

SQL-8 : 查詢工單BOM是否設定攪拌方式 getWoMixEquip(woUid)
```sql
-- 查詢工單BOM是否設定攪拌方式 getWoMixEquip(woUid)
SELECT E.EQUIP_ID, E.EQUIP_NAME 
FROM WO_H W 
INNER JOIN BOM_MIX B 
  ON W.BOM_UID=B.BOM_UID 
LEFT JOIN EQUIP_H E 
  ON B.EQUIP_ID=E.EQUIP_ID 
WHERE W.WO_UID={woUid}
ORDER BY E.EQUIP_ID
```

SQL-8-1. 查詢工單適用生產機台 getWoEquip(WoUid)
```sql 
-- 查詢工單適用生產機台 getWoEquip(WoUid)
SELECT E.EQUIP_ID, E.EQUIP_NAME 
FROM WO_H W INNER JOIN BOM_EQUIP B 
  ON W.BOM_UID=B.BOM_UID 
INNER JOIN EQUIP_H E 
  ON B.EQUIP_ID=E.EQUIP_ID AND W.ORGANIZATION_ID=E.ORGANIZATION_ID 
WHERE W.WO_UID={woUid} 
AND B.ACTIVE_FLAG={activeFlag} --"Y"/"N"
ORDER BY E.EQUIP_ID
```

SQL-9 : 更新 WO_H 的 PRINT_FLAG_D 
```sql 
UPDATE WO_H 
  SET PRINT_FLAG_D='Y' 
WHERE WO_UID={woUid}
```

SQL-10 : 出貨指定需顯示於生產指示書之項目 getSisListToPdm(工單 OrganizationId)
```sql
-- 出貨指定需顯示於生產指示書之項目 getSisListToPdm(工單 OrganizationId)
SELECT 
  H.LIST_HEADER_ID, 
  H.SP_INS_NAME, 
  H.LIST_SEQ, 
  H.SP_INS_NAME, 
  H.CATEGORY_FLAG CATEGORY_ATTRI, 
  H.FLAG_ORDER_TYPE 
FROM SYS_SIS_H H 
WHERE H.ORGANIZATION_ID={OrganizationId}
AND H.R_OPMS_PDM='Y' 
AND H.ACTIVE_FLAG='Y' 
ORDER BY H.LIST_SEQ
```

SQL-11 : 工單出貨指示 getWoSisList(WoUid)
```sql
-- 工單出貨指示 getWoSisList(WoUid)
SELECT 
  CONVERT(int, S.LIST_HEADER_ID) LIST_HEADER_ID,
  S.CATEGORY_ATTRI,
  S.SP_INS_NAME,
  CONVERT(int, S.LIST_SEQ) LIST_SEQ,
  CONVERT(int, S.LIST_LINE_ID) LIST_LINE_ID,
  S.OPT_DESC,S.ASS_DESC,
  S.FLAG_ASSIGN,
  S.FLAG_ORDER_TYPE, 
  S.OPT_DESC+ISNULL(S.ASS_DESC,'') AS SHOW_TEXT 
FROM WO_H W 
INNER JOIN WO_SIS S 
  ON W.WO_UID=S.WO_UID 
WHERE W.WO_UID={WoUid} ORDER BY S.LIST_SEQ
```

SQL-12 : 
```sql
-- getBomMtmSingle(BomUid)
SELECT 
  B.BOM_UID, B.ORGANIZATION_ID, B.ACTIVE_FLAG, I.PATTERN_NO, B.ITEM_NO, I.ITEM_DESC, I.UNIT, B.BOM_NO, B.BOM_NAME, B.BOM_DESC, 
  B.CUST_NAME, 
  I.CMDI_NO, I.CMDI_NAME, P.COLOR, 
  B.[USE], B.MULTIPLE, B.BASE_ITEM_NO, B.BASE_BOM_NO, 
  B.REMARK, B.ERP_FLAG, B.ASSEMBLY_TYPE, B.BOM_FLAG, 
  REPLACE(CONVERT(VARCHAR(16),B.CDT,120),'-','/') AS CDT, B.CREATE_BY, 
  REPLACE(CONVERT(VARCHAR(16),B.UDT,120),'-','/') AS UDT, B.UPDATE_BY, 
  I.WEIGHT, B.STD_N_QUANTITY_PER, 
  B.CUS_N_QUANTITY_PER, 
  B.BIAXIS_ONLY, B.DYE_ONLY
FROM BOM_MTM B 
INNER JOIN ITEM_H I ON B.ITEM_NO=I.ITEM_NO 
INNER JOIN PAT_H P ON I.PATTERN_NO=P.PATTERN_NO 
WHERE B.BOM_UID={BomUid}
```

SQL-12-1: 
```sql
--getSysUserTo(String userId)
SELECT U.*, D.DEPT_NAME 
FROM SYS_USER U 
INNER JOIN SYS_DEPARTMENT D 
  ON U.DEPT_ID=D.DEPT_ID 
WHERE U.USER_ID={使用者ID}
```


SQL-13 : 查詢色號是否是加嚴檢驗色號 checkIsQOPattern(String patternNo)
```sql
--查詢色號是否是加嚴檢驗色號 checkIsQOPattern(String patternNo)
SELECT MAX(PARA_VALUE) AS PATTERN_NO 
FROM SYS_PARA_D 
WHERE PARA_ID='Q_O_PATTERN' 
AND PARA_CODE='PATTERN_NO' 
AND PARA_VALUE={PatternNo}
```

SQL-14 : getWoFedWithEquip(woUid,toH.getEquipId())
```sql
-- getWoFedWithEquip(woUid,toH.getEquipId())
SELECT 
	F.EQUIP_ID, F.F_1_RATE, F.F_2_RATE, F.F_3_RATE, F.F_4_RATE, F.F_5_RATE, F.F_6_RATE, F.BDP_RATE, 
	CASE WHEN F.EQUIP_ID IS NULL THEN 1 ELSE 0 END EQUIP 
FROM WO_H W,BOM_FED F 
WHERE W.BOM_UID=F.BOM_UID 
AND W.WO_UID={WoUid}
AND (F.EQUIP_ID IS NULL OR F.EQUIP_ID={EquipId}) 
ORDER BY EQUIP 
```

SQL-15 : 
```sql 
-- getBomPgmList
SELECT PGM_PART_NO,QUANTITY_PER,UNIT,SITE_PICK,SELF_PICK,FEEDER_SEQ 
FROM BOM_PGM 
WHERE BOM_UID={BomUid} 
ORDER BY PGM_PART_NO
```

SQL-16 : getWoMixDescWithEquip(String woUid, String equipId)
```sql
-- getWoMixDescWithEquip(String woUid, String equipId)
SELECT B.EQUIP_ID, B.MIX_DESC, B.MIX_TIME, B.MFG_DESC 
FROM WO_H W 
INNER JOIN BOM_MIX B 
  ON W.BOM_UID=B.BOM_UID 
WHERE W.WO_UID={WoUid} AND (B.EQUIP_ID IS NULL OR B.EQUIP_ID={EquipId}) 
ORDER BY EQUIP_ID DESC
```

SQL-17 : getWoFedDWithEquip(String woUid, String equipId)
```sql
-- getWoFedDWithEquip(String woUid, String equipId)
SELECT D.FEEDER_NO, D.BOM_PART_NO, D.FEEDER_RATE 
FROM WO_H W 
INNER JOIN BOM_FED F 
  ON W.BOM_UID=F.BOM_UID 
INNER JOIN BOM_FED_D D 
  ON F.BOM_FED_UID=D.BOM_FED_UID 
WHERE W.WO_UID={WoUid}

-- 若 {equipId} 不為 null 且 不為空字串，加上以下SQL語句
AND F.EQUIP_ID={EquipId}
-- 若 {equipId} 為 null，加上以下SQL語句
AND F.EQUIP_ID IS NULL

```

SQL-18 : getWoMixDescWithNoEquip(String woUid)
```sql
-- 取得不指定機台之攪拌方式(提供給委外廠商使用) getWoMixDescWithNoEquip(String woUid)
SELECT MAX(B.MIX_DESC) MIX_DESC 
FROM WO_H W 
INNER JOIN BOM_MIX B 
  ON W.BOM_UID=B.BOM_UID 
WHERE W.WO_UID={WoUid} AND B.EQUIP_ID IS NULL
```


JASPER:
**jasper檔案位置**

|                      | 型態    | 參數值                           | 說明 |
| -------------------- | ------ | ------------------------------- | ---- |
| reportFile1 RealPath | string | /jasperReport/{rFile}           | {ORGANIZATION_ID} = "5000"，則 檔案 rFile 選擇取 WoPrintD_CLT.jasper，否則取 WoPrintD.jasper     |
| reportFile2 RealPath | string | /jasperReport/WoPrintTro.jasper |      |

**jasperPrintList參數** array object
| 參數名稱 | 名稱 | 型態          | 來源資料 & 說明                                 |
|--------|------|--------------|-----------------------------------------------|
| jp     | jp   | JasperPrint  | JasperFillManager.fillReport(jr,pars,jrds)    |
| jp2    | jp2  | JasperPrint  | JasperFillManager.fillReport(jr2,pars2,jrds2) |

**jasper fillReport參數**
jp 

| 參數名稱   | 名稱 | 型態          | 來源資料 & 說明                         |
| -------- | ---- | ------------ | ------------------------------------- |
| jr       |      | JasperReport |  loadObject(reportFile1)              |
| pars     |      | array object |                                       |
| jrds     |      | JRDataSource |  JRMapCollectionDataSource(openListA) |

jp2

| 參數名稱   | 名稱  | 型態          | 來源資料 & 說明                         |
| --------- | ---- | ------------ | ------------------------------------- |
| jr2       |      | JasperReport |  loadObject(reportFile2)              |
| pars2     |      | array object |                                       |
| jrds2     |      | JRDataSource |  JRMapCollectionDataSource(troDetail) |

pars object 參數 

| 參數名稱           | 名稱           | 型態         | 來源資料 & 說明                                      |
| ----------------- | -------------- | ------------ | ----------------------------------------------- |
| today             | 當下日期       | string       | yyyy/MM/dd hh:mm:ss                               |
| woDate            | 使用日期       | string       | H.WO_DATE(SQL-1)                                  |
| woSeq             | 序號           | string       | H.WO_SEQ(SQL-1)                                   |
| lotNo             | Lot No         | string       | H.LOT_NO(SQL-1)                                  |
| patternNo         | 色號           | string       | I.PATTERN_NO(SQL-1)                               |
| color             | 色相           | string       | B.COLOR(SQL-1)                                    |
| unit              | 單位           | string       | H.UNIT(SQL-1)                                     |
| woNo              | 工單號碼       | string       | H.WO_NO(SQL-1)                                    |
| custName          | 客戶名稱       | string       | 若 H.INDIR_CUST 不存在則 H.CUST_NAME(SQL-1)         |
| use               | 用途           | string       | B.USE(SQL-1)                                      |
| itemNo            | 料號           | string       | B.ITEM_NO(SQL-1)                                   |
| bomNo             | Bom No         | string       | B.BOM_NO(SQL-1)                                  |            
| produnitName      | 生產單位名稱    | string       | produnitName(SAP-1)                                |
| salesNo           | 業助代號       | string       | H.SALES_NO(SQL-1)                                  |
| batchQty          | 每Batch用量    | string       | H.BATCH_QTY(SQL-1)                                |
| batchTimes        | Batch用量次數  | string       | H.BATCH_TIMES(SQL-1)                               |
| remainQty         | 尾數量         | string       | H.REMAIN_QTY(SQL-1)                                |
| remainTimes       | 尾數量次數     | string       | 若 H.REMAIN_QTY 為0，則 H.REMAIN_TIMES也為0，其餘回傳H.REMAIN_TIMES(SQL-1)   |
| cmcSoType         |  cmcSoType    | string      | CMC_SO_TYPE  1: 奇美內銷(大黃標籤) /  2: 奇美外銷(外銷標籤) / 3: 其他  | 
| userId            | 登入者使用者ID | string       | {登入者使用者ID}                                       |
| stdLossRate       | 理論損失率     | string      | 經過計算得出                                           |
| itemDesc          | 料號摘要       | string      | 若為null值，空字串或是"null"字串，則回傳 "空白" (SQL-?)   |
| woQty             | woQty         | integer     | 若{ORGANIZATION_ID}="6000"，為LS顯示工單數量，若 {SALES_QTY} 為 null，則回傳 "N/A"，否則回傳 {SALES_QTY} + {UNIT}   |
| woQtyTitle        | 工單數量/訂單數量  | string      | 若{ORGANIZATION_ID}="6000"，則顯示工單數量，否則顯示訂單數量  |
| rawQty            | 投入原料量       | string       | H.RAW_QTY (SQL-1)                                  |
| custPoNo          | 客戶PO單        | string       | H.CUST_PO_NO (SQL-1)                                |
| shipDate          | 預計出貨        | string       | ESTIMATED_SHIP_DATE (SQL-1)  |
| batchQtyA         |                | Double      | 後端計算後，取小數點後三位  |
| batchQtyB         |                | Double      | 後端計算後，取小數點後三位  |
| batchQtyC         |                | Double      | 後端計算後，取小數點後三位  |
| remainQtyA        |                | Double      | 後端計算後，取小數點後三位  |
| remainQtyB        |                | Double      | 後端計算後，取小數點後三位  |
| remainQtyC        |                | Double      | 後端計算後，取小數點後三位  |
| totalQtyA         |                | Double      | 後端計算後，取小數點後三位  |
| totalQtyB         |                | Double      | 後端計算後，取小數點後三位  |
| totalQtyC         |                | Double      | 後端計算後，取小數點後三位  |
| batchSum          |                | Double      | 後端計算後，取小數點後三位  |
| remainSum         |                | Double      | 後端計算後，取小數點後三位  |
| totalSum          |                 | Double      | 後端計算後，取小數點後三位  |
| bomDep            | 建BOM單位        | string      | getSysUserTo (SQL-?)  |
| feederRate        | feederRate      | Double      |                       |
| woRemark          | 工單備註         | string      | strRemark = {CUST_NAME} + {INDIR_CUST} + {SALES_NO} + {CUST_NO} (SQL-1)      |
| isQOPattern       | 加嚴檢驗         | string      | "Y"/"N"      |
| isOutsource       | 委外廠           | string      | "Y"/"N"      |
| socRemark         | SOC已建機台備註   | string      | getWoSoc(woUid) (SQL-?)      |
| mixRemark         | 攪拌方式已建機台   | string      | getWoMixEquip(woUid) (SQL-?)     |
| equipRemark       | equipRemark     | string      | 跟後端邏輯描述      |
| SUBREPORT_DIR     | 出貨指示明細      | string      | `application.getRealPath("/jasperReport/WoPrintD_Sis.jasper"`      |
| SubReportParam      | subPars       | array object      |       |
| subReportTroParam   | subPars       | array object      |       |
| SubReportDS         | jrdsSis       | JRDataSource      |       |


SubReportParam object 參數

| 參數名稱     | 名稱        | 型態    | 來源資料 & 說明                            |
| ---------- | ----------- | ------ | ---------------------------------------- |



jrds JRDataSource 參數

| 參數名稱         | 名稱             | 型態     | 來源資料 & 說明                     |
| --------------- | --------------- | ------- | --------------------------------- |
| compClass       |  class A        | string  | "A"                               |
| feederNo        |  feederNo       | string  |                                   |
| feederRate      |  feederRate     | Double  | 取小數點後三位                      |
| quantityPer     |  quantityPer    | Double  | 取小數點後五位                      |
| batchQty        |  batchQty       | Double  | 取小數點後三位                      |
| remainQty       |  remainQty      | Double  | 取小數點後三位                      |
| totalQty        |  totalQty       | Double  | 取小數點後三位                      |


<!-- 
jrdsSis JRDataSource 參數

| 參數名稱          | 名稱       | 型態   | 來源資料 & 說明                              |
| ----------------- | ---------- | ------ | -------------------------------------------- |
| woPgmPartNo       | 色粉料號   | string | P.WO_PGM_PART_NO(SQL-5-1)                    |
| quantityPer + {i} | 單位用量   | string | P.QUANTITY_PER(SQL-5-1)，取到小數第5位       |
| modifyPer + {i}   | 補正紀錄量 | string | `"+" + P.MODIFY_QTY(SQL-5-1)`，取到小數第5位 |  
-->



# Response

Content-Type: application/pdf
Content-Disposition: inline;filename=WoPrintD.pdf

#### Response 範例