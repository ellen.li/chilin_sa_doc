# 訂單出貨指示管理-查詢銷售單號
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

查詢SAP銷售單號

## 修改歷程

| 修改時間 | 內容                                              | 修改者 |
|----------|-------------------------------------------------|--------|
| 20230616 | 新增規格                                          | shawn  |
| 20230703 | 調整規格，新增欄位opIsSap, opIsOpms                | shawn  |
| 20230710 | 新增欄位opHFlag, opSisFlag                        | shawn  |
| 20230719 | 移除欄位opDataFlag                                | shawn  |
| 20230829 | 新增回傳是否拋轉                                  | shawn  |
| 20230907 | 調整新增/編輯的處理邏輯                           | shawn  |
| 20230920 | 調整比對邏輯                                      | Nick   |
| 20231122 | 調整-編輯時，不該與 SAP 連動，要更新 SAP 值需用新增 | Nick   |

## 來源URL及資料格式

| 項目   | 說明          |
|--------|---------------|
| URL    | /sis/query_so |
| method | post          |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位   | 名稱     | 資料型別 | 必填 | 資料儲存 & 說明         |
|--------|----------|:--------:|:----:|-------------------------|
| opSoNo | 銷售單號 |  string  |  M   |  |
| opIsNew | 是否新增 |  string  |  M   | Y:新增/N:編輯  |


#### Request 範例

```json
{
  "opSoNo":"21094555"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 查詢sql-1，判斷是否存在，{opIsOpms} = 'Y' or 'N'
* 查詢sql-2，如SO_SEQ有null的存在代表有設置表頭，{opHFlag}='Y'
* 查詢SAP-1資料
  * 判斷T_ITEM回傳數量是否大於0，{opIsSap}='Y' or 'N'
* 如為新增：
  opOrderList的opDataFlag設為N，回傳sap的資料
* 如為編輯：
  * 查詢sql-3設為{opOrderList}
  * 回傳sql-1的資料
  
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容
  
# Request 後端邏輯說明
SAP-1：
可參考舊版 SoQueryRfc_getSo
程序如有任何異常，紀錄錯誤LOG並回傳NULL
查詢SAP-API回傳結果參數如下
|              | 型態   | 參數值       | 說明 |
|--------------|--------|--------------|------|
| BAPIFunction | string | ZSD_OPMS_001 |      |

| 參數名稱 | 參數值   | 型態   | 說明 |
|----------|----------|--------|------|
| I_VBELN  | {opSoNo} | string |      |


| exportParameterList |
|           | 型態   | 參數值   | 說明 |
|-----------|--------|----------|------|
| Structure | string | E_HENDER |      |

| 物件元素名稱 | 資料型別 | SAP欄位名稱 | SAP資料型別 | 物件元素回傳值 | 說明                  |
|--------------|----------|-------------|-------------|---------------|-----------------------|
| custNo       | string   | KUNNR       | string      | 同SAP          | E_HENDER.get("KUNNR") |
| custName     | string   | SORT2       | string      | 同SAP          | E_HENDER.get("SORT2") |
| custPoNo     | string   | BSTNK       | string      | 同SAP          | E_HENDER.get("BSTNK") |
| channel     | string   | VTWEG       | string      | 同SAP          | E_HENDER.get("VTWEG") |

| 取得回傳表格名稱 |
|------------------|
| T_ITEM           |

如果回傳資料數量為0則{opIsSap}='N'，否則依下面物件集合回傳且{opIsSap}='Y'
| 物件元素名稱 | 資料型別 | SAP欄位名稱 | SAP資料型別 | 物件元素回傳值 | 說明 |
|--------------|----------|-------------|-------------|---------------|------|
| soSeq        | string   | POSNR       | string      | 同SAP          |      |
| itemNo       | string   | MATNR       | string      | 同SAP          |      |
| itemDesc     | string   | ARKTX       | string      | 同SAP          |    |
| custItemNo          | string   | KDMAT      | string  | 同SAP          |  如不為空值且長度大於4，{custItemNo}.substring(3)    |
| qty          | string   | KWMENG      | BigDecimal  | 同SAP          |      |
| color        | string   | COLOR       | string      | 同SAP          |      |
| mtrName        | string   | MATERIAL       | string      | 同SAP          |    |
| patternNo        | string   | COLORNO       | string      | 同SAP          |      |
| deep        | string   | DEEP       | string      | 同SAP          |      |
| width        | string   | WIDTH       | string      | 同SAP          |      |
| length        | string   | LENGTH       | string      | 同SAP          |      |
| spec        | string   | SPEC       | string      | 同SAP          |      |


sql-1
```sql
SELECT A.SO_NO, A.ORGANIZATION_ID, A.CUST_NO, 
A.CUST_NAME, A.CUST_PO_NO, A.CHANNEL,
A.ERP_FLAG, A.SIS_FLAG,
REPLACE(CONVERT(VARCHAR(19),A.CDT,120),'-','/') CDT, 
A.CREATE_BY,
REPLACE(CONVERT(VARCHAR(19),A.UDT,120),'-','/') UDT,
A.UPDATE_BY 
FROM SO_H A WHERE A.SO_NO={opSoNo}
```

sql-2
```sql
SELECT SO_SEQ FROM SO_SIS WHERE SO_NO={opSoNo}
```

sql-3
```sql
SELECT
	D.SO_NO,
	D.SO_SEQ,
	D.STATUS,
	D.ITEM_NO,
	D.ITEM_DESC,
	D.CUST_ITEM_NO,
	D.QTY,
	D.PATTERN_NO,
	D.COLOR,
	D.MTR_NAME,
	D.DEEP,
	D.WIDTH,
	D.LENGTH,
	D.SPEC,	
	D.ERP_FLAG,
	D.SIS_FLAG,
	REPLACE(CONVERT(VARCHAR(19), D.CDT, 120), '-', '/') CDT,
	D.CREATE_BY,
	REPLACE(CONVERT(VARCHAR(19), D.UDT, 120), '-', '/') UDT,
	D.UPDATE_BY
FROM
	SO_D D
WHERE
	D.SO_NO ={opSoNo}
```

# Response 欄位
| 欄位       | 名稱     | 資料型別 | 資料儲存 & 說明 |
|------------|--------|----------|-----------------|
| opSoNo       | 銷售單號         | string   | opSoNo(sap-1)/SO_NO(sql-1)           |
| opCustNo   | 客戶代號 | string   | custNo(sap-1)/CUST_NO(sql-1)          |
| opCustName | 客戶簡稱 | string   | custName(sap-1)/CUST_NAME(sql-1)        |
| opCustPoNo | 客戶po   | string   | custPoNo(sap-1)/CUST_PO_NO(sql-1)        |
| opChannel | channel   | string   | channel(sap-1)/CHANNEL(sql-1)   |
| opIsSap | 是否存在sap   | string   | Y/N   |
| opIsOpms | 是否存在opms   | string   | Y/N   |
| opHFlag | 是否設定表頭   | string   | Y/N   |
| opErpFlag | 是否拋轉ERP   | string   | ERP_FLAG(SQL-1)   |
| opOrderList  | 訂單資料 | array    |                 |

### 【opOrderList】array
| 欄位       | 名稱     | 資料型別 | 資料儲存 & 說明 |
|------------|--------|----------|-----------------|
| opSoSeq    | 項次     | string   | soSeq(sap-1)/SO_SEQ(sql-3)           |
| opItemNo   | 料號     | string   | itemNo(sap-1)/ITEM_NO(sql-3)          |
| opItemDesc | 料號摘要 | string   | itemDesc(sap-1)/ITEM_DESC(sql-3)        |
| opCustItemNo      | 客戶料號     | string   | custItemNo(sap-1)/CUST_ITEM_NO(sql-3)             |
| opQty      | 數量     | string   | qty(sap-1)/QTY(sql-3)             |
| opColor      | 色相     | string   | color(sap-1)/COLOR(sql-3)             |
| opMtrName     | 原料     | string   | mtrName(sap-1)/MTR_NAME(sql-3)             |
| opPatternNo     | 色號     | string   | patternNo(sap-1)/PATTERN_NO(sql-3)             |
| opDeep      | 厚度     | string   | deep(sap-1)/DEEP(sql-3)             |
| opWidth    | 寬度     | string   | width(sap-1)/WIDTH(sql-3)             |
| opLength     | 長度     | string   | length(sap-1)/LENGTH(sql-3)             |
| opSpec     | 規格    | string   | spec(sap-1)/ SPEC(sql-3)            |
| opSisFlag     | 提示    | string   | SIS_FLAG(sql-3)             |
| opDataFlag     | 狀態   | string   | N/D/M/null         |
#### Response 範例

```json
{
  "msgCode": null,
  "result":{
    "content":{
        "opSoNo":"20715556",
        "opCustNo":"277723",
        "opCustName":"xx",
        "opCustPoNo":"OB123456",
        "opChannel":"20",
        "opIsSap":"Y",
        "opIsOpms":"Y",
        "opHFlag":"Y",
        "opErpFlag":"Y",
        "opOrderList":[
          {
            "opSoSeq":"1",
            "opItemNo":"",  
            "opItemDesc":"",
            "opCustItemNo":"",
            "opQty":"100",
            "opColor":"",
            "opMtrName":"",
            "opPatternNo":"",
            "opDeep":"",     
            "opWidth":"",    
            "opLength":"",   
            "opSpec":"",
            "opSisFlag":"Y",
            "opDataFlag":"N"
          }
        ]
    }
  }
}
```

