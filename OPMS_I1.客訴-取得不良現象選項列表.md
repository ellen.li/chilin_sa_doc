# 客訴-取得不良現象選項列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得不良現象選項列表

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230725 | 新增規格 | Ellen  |

## 來源URL及資料格式

| 項目   | 說明                |
| ------ | ------------------- |
| URL    | /cs/get_param_cs_df |
| method | get                 |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。  
| 欄位        | 名稱   | 資料型別 | 必填  | 來源資料 & 說明       |
| ----------- | ------ | -------- | :---: | --------------------- |
| opParaValue | 選項值 | string   |   O   | 預設 `null`，模糊搜尋 |

#### Request 範例
```json
{
  "opParaValue":"A2"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL-1 取得不良現象資料
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1 取得不良現象選項
```sql
SELECT PARA_VALUE, PARA_TEXT FROM SYS_PARA_D WHERE PARA_ID='CS_DF' 
WHERE 1=1
    --搜尋條件
    AND PARA_VALUE LIKE {opParaValue}%
ORDER BY PARA_VALUE ASC
```

# Response 欄位
| 欄位    | 名稱         | 資料型別 | 資料儲存 & 說明 |
| ------- | ------------ | -------- | --------------- |
| content | 不良現象選項 | array    |                 |

content
| 欄位    | 名稱     | 資料型別 | 來源資料 & 說明 |
| ------- | -------- | -------- | --------------- |
| opValue | 選項值   | string   | PARA_VALUE      |
| opText  | 選項敘述 | string   | PARA_TEXT       |

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":[
        {
          "opValue": "A20",
          "opText": "表面阻抗(SR)異常"
        },
        {
          "opValue": "A21",
          "opText": "穿透率異常"
        }
      ]
  }
}
```