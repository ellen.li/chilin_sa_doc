# BOM維護-取得單一BOM資料
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得單一bom資料

## 修改歷程

| 修改時間 | 內容                                             | 修改者 |
|----------|------------------------------------------------|--------|
| 20230502 | 新增規格                                         | Nick   |
| 20230711 | opBomMixList 裡面的 opEquipName 誤值為 opEquipId | Nick   |
| 20230731 | 調整 SQL-2, SQL-3 排序                           | Sam    |
| 20230817 | 新增 替代結構、替代說明                           | Nick   |
| 20230831 | 回傳增加色母基本配方                              | Ellen  |


## 來源URL及資料格式

| 項目   | 說明         |
|--------|--------------|
| URL    | /bom/get_one |
| method | post         |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位     | 名稱    | 資料型別 | 必填 | 資料儲存 & 說明 |
|----------|-------|:--------:|:----:|-----------------|
| opBomUid | BOM代碼 |  string  |  M   |                 |

#### Request 範例

```json
{
   "opBomUid": "5000-717CXXXUV1QFA9530902"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 執行  SQL-1 確認BOM存在，找不到 return 400,"NOTFOUND"
* 執行  SQL-2~SQL-7 取出相關資料
* 組出 JSON
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容


# Request 後端邏輯說明


SQL-1:
```sql
SELECT B.BOM_UID, B.ORGANIZATION_ID, B.ACTIVE_FLAG, I.PATTERN_NO, B.ITEM_NO, I.ITEM_DESC, I.UNIT, B.BOM_NO, B.BOM_NAME, B.BOM_DESC, B.CUST_NAME, I.CMDI_NO, I.CMDI_NAME, P.COLOR, B.[USE], B.MULTIPLE, B.BASE_ITEM_NO, B.BASE_BOM_NO, B.REMARK, B.ERP_FLAG, B.ASSEMBLY_TYPE, B.BOM_FLAG, REPLACE(CONVERT(VARCHAR(16),B.CDT,120),'-','/') AS CDT, B.CREATE_BY, REPLACE(CONVERT(VARCHAR(16),B.UDT,120),'-','/') AS UDT, B.UPDATE_BY, I.WEIGHT, B.STD_N_QUANTITY_PER, B.CUS_N_QUANTITY_PER, B.BIAXIS_ONLY, B.DYE_ONLY
  ,B.ACTIVE_FLAG
   FROM BOM_MTM B  INNER JOIN ITEM_H I ON B.ITEM_NO=I.ITEM_NO  INNER JOIN PAT_H P ON I.PATTERN_NO=P.PATTERN_NO  WHERE B.BOM_UID={opBomUid}
```

SQL-2:
```sql
SELECT R.MTR_PART_NO,R.QUANTITY_PER,R.UNIT  FROM BOM_MTR R WHERE R.BOM_UID={opBomUid} 
ORDER BY R.MTR_PART_NO ASC, R.QUANTITY_PER DESC
```

SQL-3:
```sql
SELECT PGM_PART_NO,QUANTITY_PER,UNIT,SITE_PICK,SELF_PICK,OPT_DISPLAY,FEEDER_SEQ FROM BOM_PGM LEFT JOIN OPMS_PARAMS OP ON OP.TYPE='BOM' AND OP.PARAM_ID='SELFPICK' AND OP.OPT_VALUE=SELF_PICK  WHERE BOM_UID={opBomUid} 
ORDER BY PGM_PART_NO ASC
```

SQL-4:
```sql
SELECT A.BOM_UID, A.EQUIP_ID, A.ACTIVE_FLAG,OP.OPT_DISPLAY, A.REMARK, REPLACE(CONVERT(VARCHAR(19),A.UDT,120),'-','/') UDT, A.UPDATE_BY,E.EQUIP_NAME,E.PRODUNIT_NO,P.PRODUNIT_NAME  
FROM BOM_EQUIP A  INNER JOIN EQUIP_H E ON A.EQUIP_ID=E.EQUIP_ID  INNER JOIN PRODUNIT P ON E.ORGANIZATION_ID=P.ORGANIZATION_ID AND E.PRODUNIT_NO=P.PRODUNIT_NO 
LEFT JOIN OPMS_PARAMS OP ON OP.TYPE='BOM' AND OP.PARAM_ID='ACTIVE_FLAG' AND OP.OPT_VALUE=A.ACTIVE_FLAG
WHERE A.BOM_UID={opBomUid} AND (A.ACTIVE_FLAG='Y' OR A.ACTIVE_FLAG='N')  ORDER BY EQUIP_ID
```

SQL-5:
```sql
SELECT A.BOM_UID, A.BOM_MIX_UID, A.EQUIP_ID, A.MIX_DESC, A.MIX_TIME, A.MFG_DESC, REPLACE(CONVERT(VARCHAR(19),A.UDT,120),'-','/') UDT, A.UPDATE_BY,E.EQUIP_NAME,E.PRODUNIT_NO,P.PRODUNIT_NAME  FROM BOM_MIX A  LEFT JOIN EQUIP_H E ON A.EQUIP_ID=E.EQUIP_ID  LEFT JOIN PRODUNIT P ON E.PRODUNIT_NO=P.PRODUNIT_NO  WHERE A.BOM_UID={opBomUid} ORDER BY EQUIP_ID 
```

SQL-6:
```sql
SELECT A.BOM_UID, A.BOM_FED_UID, A.EQUIP_ID, A.F_1_RATE, A.F_2_RATE, A.F_3_RATE, A.F_4_RATE, A.F_5_RATE, A.F_6_RATE, A.BDP_RATE, A.PPG_RATE, A.DBE_RATE, REPLACE(CONVERT(VARCHAR(19),A.UDT,120),'-','/') UDT, A.UPDATE_BY,E.EQUIP_NAME,E.PRODUNIT_NO,P.PRODUNIT_NAME  FROM BOM_FED A  LEFT JOIN EQUIP_H E ON A.EQUIP_ID=E.EQUIP_ID  LEFT JOIN PRODUNIT P ON E.ORGANIZATION_ID=P.ORGANIZATION_ID AND E.PRODUNIT_NO=P.PRODUNIT_NO  WHERE A.BOM_UID={opBomUid} ORDER BY EQUIP_ID
```

# Response 欄位
| 欄位           | 名稱                   | 資料型別 | 資料儲存 & 說明             |
|----------------|------------------------|----------|-----------------------------|
| opBomUid       | BOM ID                 | string   | B.BOM_UID(SQL-1)            |
| opItemNo       | 料號                   | string   | B.ITEM_NO(SQL-1)            |
| opPatternNo    | 色號                   | string   | I.PATTERN_NO(SQL-1)         |
| opColor        | 顏色                   | string   | P.COLOR(SQL-1)              |
| opItemDesc     | 料號摘要               | string   | I.ITEM_DESC(SQL-1)          |
| opCustName     | 客戶                   | string   | B.CUST_NAME(SQL-1)          |
| opUnit         | 單位                   | string   | I.UNIT(SQL-1)               |
| opWeight       | 重量(KG)               | string   | I.WEIGHT(SQL-1)             |
| opRemark       | 備註                   | string   | I.REMARK(SQL-1)             |
| opN4Std        | N-4 單位用量(系統)     | string   | B.STD_N_QUANTITY_PER(SQL-1) |
| opN4Cus        | N-4 單位用量(人工)     | string   | B.CUS_N_QUANTITY_PER(SQL-1) |
| opBiaxisOnly   | 限雙軸生產             | string   | B.BIAXIS_ONLY(SQL-1)        |
| opDyeOnly      | 只染不押               | string   | B.DYE_ONLY (SQL-1)          |
| opBomNo        | 替代結構               | string   | B.BOM_NO (SQL-1)                    |
| opBomDesc      | 替代說明               | string   | B.BOM_DESC (SQL-1)                   |
| opMultiple     | 倍數                  | string   | B.MULTIPLE(SQL-1)           |
| opBaseItemNo   | 色母基本配方料號       | string   | B.BASE_ITEM_NO(SQL-1)       |
| opBaseBomNo    | 色母基本配方BOM        | string   | B.BASE_BOM_NO(SQL-1)        |
| opBomMTrList   | 原料清單               | array    |                             |
| opBomPgmList   | 色料清單               | array    |                             |
| opBomEquipList | 機台適用性清單         | array    |                             |
| opBomMixList   | 攪拌方式               | array    |                             |
| opBomFedList   | 下料比例與下料設備清單 | array    |                             |

【 opBomMTrList array object】
| 欄位          | 名稱 | 資料型別 | 來源資料 & 說明       |
|---------------|----|----------|-----------------------|
| opMtrPartNo   | 料號 | string   | R.MTR_PART_NO(SQL-2)  |
| opQuantityPer | 用量 | string   | R.QUANTITY_PER(SQL-2) |
| opUnit        | 單位 | string   | R.UNIT(SQL-2)         |



【 opBomPgmList array object】
| 欄位           | 名稱 | 資料型別 | 來源資料 & 說明     |
|----------------|----|----------|---------------------|
| opFeederSeq    | 順序 | string   | FEEDER_SEQ(SQL-3)   |
| opPgmPartNo    | 料號 | string   | PGM_PART_NO(SQL-3)  |
| opQuantityPer  | 用量 | string   | QUANTITY_PER(SQL-3) |
| opUnit         | 單位 | string   | UNIT(SQL-3)         |
| opSelfPickText | 秤料 | string   | OPT_DISPLAY(SQL-3)  |


【 opBomEquipList array object】
| 欄位             | 名稱     | 資料型別 | 來源資料 & 說明                                                 |
|------------------|--------|----------|-----------------------------------------------------------------|
| opActiveFlagText | 適用性   | string   | OP.OPT_DISPLAY(SQL-4) <br/>                                     |
| opProdUnitName   | 生產單位 | string   | P.PRODUNIT_NAME(SQL-4) <br/> 說明: null or 空白前端代表【不指定】 |
| opEquipName      | 機台名稱 | string   | E.EQUIP_NAME(SQL-4) <br/> 說明: null or 空白前端代表【不指定】    |
| opRemark         | 說明     | string   | A.REMARK(SQL-4)                                                 |

【 opBomMixList array object】
| 欄位           | 名稱       | 資料型別 | 來源資料 & 說明         |
|----------------|----------|----------|-------------------------|
| opProdUnitName | 生產單位   | string   | P.PRODUNIT_NAME (SQL-5) |
| opEquipName    | 機台名稱   | string   | E.EQUIP_NAME (SQL-5)    |
| opMixDesc      | 攪拌方式   | string   | A.MIX_DESC (SQL-5)      |
| opMixTime      | 時間(分鐘) | string   | A.MIX_TIME (SQL-5)      |
| opMfgDesc      | 生產方式   | string   | A.MFG_DESC (SQL-5)      |


【 opBomFedList array object】
| 欄位           | 名稱        | 資料型別 | 來源資料 & 說明         |
|----------------|-------------|----------|-------------------------|
| opBomFedUid    | BOM_FED_UID | string   | A.BOM_FED_UID (SQL-6)   |
| opProdUnitName | 生產單位    | string   | P.PRODUNIT_NAME (SQL-6) |
| opEquipName    | 機台名稱    | string   | E.EQUIP_NAME (SQL-6)    |
| opF1Rate       | F1          | string   | A.F_1_RATE (SQL-6)      |
| opF2Rate       | F2          | string   | A.F_2_RATE (SQL-6)      |
| opF3Rate       | F3          | string   | A.F_3_RATE (SQL-6)      |
| opF4Rate       | F4          | string   | A.F_4_RATE (SQL-6)      |
| opF5Rate       | F5          | string   | A.F_5_RATE (SQL-6)      |
| opF6Rate       | F6          | string   | A.F_6_RATE (SQL-6)      |
| opBDP          | BDP         | string   | A.BDP_RATE (SQL-6)      |
| opPPG          | PPG         | string   | A.PPG_RATE (SQL-6)      |
| opDBE          | DBE         | string   | A.DBE_RATE (SQL-6)      |

#### Response 範例

```json
{
  "msgCode":"",
  "result":{
    "content":
      {
       "opBomUid":"5000-717CXXXUV1QFA9530902",
       "opItemNo":"717CXXXUV1QFA95309",
       "opPatternNo":"A95309B1",
       "opColor":"白",
       "opItemDesc":"PA-717C,QF 1000KG打包/高棧板A95309B1+UV",
       "opCustName":"Plastributio",
       "opUnit":"KG",
       "opWeight":"1.000",
       "opRemark":"2012.3.7配方修改(秉)",
       "opN4Std":"0.66787",
       "opN4Cus":"1.20000",
       "opBiaxisOnly":"N",
       "opDyeOnly":"N",
       "opMultiple": "",
       "opBaseItemNo": "",
       "opBaseBomNo": "",
       "opBomMTrList":[
          {"opMtrPartNo":"04-102-PA-717CXXXX","opQuantityPer":"1.00000","opUnit":"KG"},
          {"opMtrPartNo":"92-912-1078745276X","opQuantityPer":"0.04220","opUnit":"PC"}
       ],
       "opBomPgmList":[
          {"opFeederSeq":"1","opPgmPartNo":"05-201-B-06XXXXXXX","opQuantityPer":"0.06400","opUnit":"G","opSelfPickText":"色粉室"},
          {"opFeederSeq":"1","opPgmPartNo":"05-201-C-81XXXXXXX","opQuantityPer":"0.00830","opUnit":"G","opSelfPickText":"色粉室"},
          {"opFeederSeq":"1","opPgmPartNo":"05-201-W-10XXXXXXX","opQuantityPer":"40.00000","opUnit":"G","opSelfPickText":"色粉室"},
          {"opFeederSeq":"1","opPgmPartNo":"05-251-A-L12XXXXXX","opQuantityPer":"7.00000","opUnit":"G","opSelfPickText":"色粉室"},
          {"opFeederSeq":"1","opPgmPartNo":"05-251-A-U57XXXXXX","opQuantityPer":"2.50000","opUnit":"G","opSelfPickText":"色粉室"},
          {"opFeederSeq":"1","opPgmPartNo":"05-251-A-U58AXXXXX","opQuantityPer":"2.50000","opUnit":"G","opSelfPickText":"色粉室"}
       ],
       "opBomEquipList":[
         {"opActiveFlagText":"適用","opProdUnitName":"染色課","opEquipName":"染色#1","opRemark":null}
         {"opActiveFlagText":"適用","opProdUnitName":"染色課","opEquipName":"染色#6","opRemark":null}
       ],
       "opBomMixList":[
          {"opProdUnitName":null,"opEquipName":null,"opBomMixUid":"7F6A93F3-0C5E-46C1-8A80-B0127CD78E54", "opMixDesc":"(a)3/5原料+N4+1/5原料攪拌5mins  (b)完成步驟(a)加入色粉、助劑+1/5原料攪拌25mins", "opMixTime":"30","opMfgDesc":"以二段式攪拌"}
       ],
       "opBomFedList":[
         {"opProdUnitName":"複材課","opEquipName":"複材#5","opF1Rate":"3.60","opF2Rate":"0.00","opF3Rate":"54.66","opF4Rate":"13.91","opF5Rate":"27.83","opF6Rate":"0.00","opBDP":"14.91","opPPG":"0.00","opDBE":"0.00"}   
       ]
      }
  }
}
```

