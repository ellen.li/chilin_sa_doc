# 報表-色粉標籤資料電子檔-匯出excel
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間  | 內容    | 修改者 |
|----------|----------|--------|
| 20230809 | 新增規格 | 黃東俞 |


## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /report/pgm_label_list_csv  |
| method | post                        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱    |  資料型別 | 必填 | 資料儲存 & 說明  |
|--------------|---------|----------|:----:|-----------------|
| 原列表REQUEST參數 |     |          |      |                 |

# Request 後端流程說明

* 參考 [OPMS_J5.報表-色粉標籤資料電子檔-列表 /report/pgm_label_list](./OPMS_J5.報表-色粉標籤資料電子檔-列表.md) 取得資料
* 取得資料後轉為csv檔匯出，檔名固定為 'MTX01DTL'
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容


# Excel 欄位
| 欄位           | 資料型別 | 資料儲存 & 說明                |
|----------------|---------|-------------------------------|
| 日期           | string   | WO_H.WO_DATE 格式：yyyymmdd   |
| 序號           | string   | WO_H.WO_SEQ                  |
| Lot No         | string   | WO_H.LOT_NO                  |
| 色號           | string   | ITEM_H.PATTERN_NO            |
| 工單號碼        | string   | WO_H.WO_NO                   |
| 品稱代號        | string   | BOM_MTM.CMDI_NO              |
| 原料第一料號    | string   |  WO_MTR.WO_MTR_PART_NO       |
| 每Batch用量     | string   | WO_H.BATCH_QTY               |
| Batch用量次數   | string   | WO_H.BATCH_TIMES             |
| 尾數量          | string   | WO_H.REMAIN_QTY              |
| 尾數量次數       | string   | WO_H.REMAIN_TIMES            |
| 比率一          | string   |                    |
| 原料第二料號     | string   |                    |
| 比率二          | string   |                    |
| 原料第三料號     | string   |                    |
| 比率三          | string   |                    |
| N_4數量         | string   |                    |
| N_4尾數         | string   |                    |
| 每Batch色粉重   | string   |                    |
| 每Batch色粉尾數 | string   |                    |
| 色粉重          | string   |                    |