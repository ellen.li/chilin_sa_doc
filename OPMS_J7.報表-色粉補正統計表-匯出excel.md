# 報表-色粉補正統計表-匯出excel
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230821 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                      |
|--------|--------------------------|
| URL    | /report/pgm_correction_list_excel|
| method | post                     |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱    |  資料型別 | 必填 | 資料儲存 & 說明 |
|--------------|---------|----------|:----:|-----------------|
| 原列表REQUEST參數 |     |          |      |                 |

# Request 後端流程說明

* 參考[OPMS_J7.報表-色粉補正統計表-列表 /report/pgm_monthly_list](./OPMS_J7.報表-色粉補正統計表-列表.md)，用相同方式取得資料。
* 將資料轉為excel表格匯出，不需設定檔名。
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

參考[OPMS_J7.報表-色粉補正統計表-列表 /report/pgm_monthly_list](./OPMS_J7.報表-色粉補正統計表-列表.md)

# Excel 欄位

| 欄位        | 資料型別 | 資料儲存 & 說明                         |
|------------ |---------|----------------------------------------|
| 色料        | string  | WO_PGM.WO_PGM_PART_NO                   |
| 料號        | string  | BOM_MTM.ITEM_NO                         |
| 工單號碼     | string  | WO_H.WO_NO                              |
| 日期        | string  | WO_H.WO_DATE  yyyy/mm/dd                |
| 補正數量     | string  | MODIFY_QTY 取兩位小數並轉為千分位格式     |