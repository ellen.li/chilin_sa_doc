# LOT編碼-取得LOT列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

LOT編碼-取得LOT列表

## 修改歷程

| 修改時間 | 內容                  | 修改者 |
| -------- | --------------------- | ------ |
| 20230830 | 新增規格              | Ellen  |
| 20230927 | 回傳新增機台,製造日期 | Ellen  |


## 來源URL及資料格式

| 項目   | 說明      |
| ------ | --------- |
| URL    | /lot/list |
| method | post      |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。 
| 欄位      | 名稱     | 資料型別 | 必填  | 資料儲存 & 說明 |
| --------- | -------- | :------: | :---: | --------------- |
| opMfgDate | 日期     |  string  |   O   | yyyy/MM/dd      |
| opEquipId | 機台     |  string  |   O   |                 |
| opWoNo    | 工單編號 |  string  |   O   |                 |

#### Request 範例

```json
{
  "opMfgDate":"2023/01/01",    
  "opEquipId":"1"
}
```

# Request 後端流程說明

可參考舊版 PsManageAction.ajaxQueryMfgLot
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 使用前端給的條件，參考 SQL-1 自組【搜尋條件】，如為 null 代表不需組條件
* 參考 SQL-1 查回結果 result
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1: 
```sql
SELECT
  L.*,
  E.EQUIP_NAME,
  B.ITEM_NO,
  W.WO_QTY,
  REPLACE(S1.OPT_DESC, 'Lot No.', '') AS LOT_OPT,
  S2.OPT_DESC AS PKG_OPT,
  REPLACE(S3.OPT_DESC, '外袋Lot No.:', '') PKG_LOT_OPT
FROM
  PS_L L
  INNER JOIN WO_H W ON L.WO_UID = W.WO_UID
  INNER JOIN BOM_MTM B ON W.BOM_UID = B.BOM_UID
  -- 標籤LOT跳號規則
  LEFT JOIN(SELECT WO_UID, OPT_DESC FROM WO_SIS WHERE LIST_HEADER_ID = 110) S1 ON W.WO_UID = S1.WO_UID
  -- 棧板規則
  LEFT JOIN(SELECT WO_UID, OPT_DESC FROM WO_SIS WHERE LIST_HEADER_ID = 92) S2 ON W.WO_UID = S2.WO_UID
  -- 外袋LOT規則
  LEFT JOIN(SELECT WO_UID, OPT_DESC FROM WO_SIS WHERE LIST_HEADER_ID = 90) S3 ON W.WO_UID = S3.WO_UID
  LEFT JOIN EQUIP_H E ON L.EQUIP_ID=E.EQUIP_ID
WHERE 1=1
  --如{opEquipId}非 null 則加入以下SQL
  AND L.EQUIP_ID = {opEquipId}
  --如{opMfgDate}非 null 則加入以下SQL
  AND L.MFG_DATE = {opMfgDate}
  --如{opWoNo}非 null 則加入以下SQL
  AND W.WO_NO = {opWoNo}
ORDER BY
  MFG_DATE DESC, LOT_SEQ
```

# Response 欄位
| 欄位         | 名稱            | 資料型別 | 來源資料 & 說明          |
| ------------ | --------------- | -------- | ------------------------ |
| opWoUid      | 工單ID          | string   | WO_UID(SQL-1)            |
| opItemNo     | 料號            | string   | ITEM_NO(SQL-1)           |
| opWoNo       | 工單號碼        | string   | WO_NO(SQL-1)             |
| opWoQty      | 工單數量        | string   | WO_QTY(SQL-1)            |
| opLotNo      | 標籤LOT         | string   | LOT_NO(SQL-1)            |
| opLotSeq     | 標籤LOT序號     | string   | LOT_SEQ(SQL-1)           |
| opLotNoMtr   | 紙袋LOT         | string   | LOT_NO_MTR(SQL-1)        |
| opLotOpt     | 標籤LOT跳號規則 | string   | LOT_OPT(SQL-1)           |
| opPkgLotOpt  | 紙袋LOT規則     | string   | PKG_LOT_OPT(SQL-1)       |
| opPkgOpt     | 棧板規則        | string   | PKG_OPT(SQL-1)           |
| opUseMtrFlag |                 | string   | USE_MTR_FLAG(SQL-1)  Y/N |
| opEquipId    | 機台代號        | string   | EQUIP_ID(SQL-1)          |
| opEquipName  | 機台名稱        | string   | EQUIP_NAME(SQL-1)        |
| opMfgDate    | 製造日期        | string   | MFG_DATE(SQL-1)          |


#### Response 範例

```json
{
    "msgCode": null,
    "result": {
      "content":[
        {
          "opWoUid": "621DF9BF-042F-42D1-A984-8552ACE177CB",
          "opItemNo": "757XXXXXX1DFH95429",
          "opWoNo": "51029832",
          "opWoQty": "30159.9",
          "opLotNo": "22T01C043",
          "opLotSeq": "1",
          "opLotNoMtr": "22T01C042",
          "opLotOpt": "每工單跳號",
          "opPkgLotOpt": "無須噴印",
          "opPkgOpt": "500KG太空包,熱松木棧板",
          "opUseMtrFlag": "N",
          "opEquipId": "3",
          "opEquipName": "染色#9(原#3)",
          "opMfgDate": "2023/03/30"
        }
      ]
    }
}
```
 