# 出貨指示資料設定-取得客戶與料號出貨指示與選項
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得客戶與料號出貨指示與選項


## 修改歷程

| 修改時間 | 內容                                        | 修改者 |
|----------|-------------------------------------------|--------|
| 20230530 | 新增規格                                    | Nick   |
| 20230707 | 調整 Response 結構與新增回傳客戶等          | Nick   |
| 20230707 | '奇美併庫別說明' 選項過濾掉移除及調整順序等 | Nick   |
| 20230711 | 新增 opShowAssDesc 節點                     | Nick   |
| 20230726 | 調整 opConfigUid 為選填                     | Nick   |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|------------------------------|
| URL    | /sis/get_sis_config_edit_pre |
| method | post                         |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             | 名稱       | 資料型別 | 必填 | 資料儲存 & 說明                           |
|------------------|------------|:--------:|:----:|-------------------------------------------|
| opOrganizationId | 工廠別     |  string  |  M   |                                           |
| opConfigUid      | Config UID |  string  |  O  | 新增給 `null` ，複製/編輯填來源 Config Uid |


#### Request 範例
```json
{
{
  "opOrganizationId":"5000",
  "opConfigUid":null
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 執行 SQL-3 取得相關參數，如果取不到資料，預設相關節點皆給 null，否則依SQL 回傳對應 content 內容
* 執行  SQL-1 取得相關參數，寫入 opSisList 內，並利用 SQL-1 每一筆的 LIST_HEADER_ID 帶入 SQL-2 取得選項清單 opOptionList 資料
* 自定義變數 {ShowAssDesc} 預設為 'N'，  前面 SQL-2 結果中的 SHOWASSDESC 欄位有一筆為 'Y'，則 {ShowAssDesc}覆寫為 'Y'
* List 型態查無資料皆給 null(例如: "opSisList" : null or "opOptionList":null )
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
SELECT CONVERT(int, L.LIST_HEADER_ID) LIST_HEADER_ID,
      CASE L.SP_INS_NAME WHEN '棧板特殊堆疊(表身)' THEN '棧板標籤擺放要求' ELSE L.SP_INS_NAME END SP_INS_NAME,
	   L.CATEGORY_FLAG CATEGORY_ATTRI,
      CONVERT(int, S.LIST_LINE_ID) LIST_LINE_ID,
	   S.CONFIG_UID,
	   S.OPT_DESC,
	   S.ASS_DESC,
	   S.FLAG_ASSIGN,      
		CASE L.LIST_HEADER_ID
		  WHEN 68  THEN 605
		  WHEN 69  THEN 610
		  WHEN 106 THEN 615
		  WHEN 111 THEN 620
		  WHEN 67  THEN 625
		  WHEN 70  THEN 630
		  WHEN 83  THEN 635
		  WHEN 153 THEN 637
		  WHEN 84  THEN 638
		  WHEN 154 THEN 639
		  WHEN 160 THEN 640
		  ELSE CONVERT(int, L.LIST_SEQ)
		END  LIST_SEQ_NEW
FROM SYS_SIS_H L 
LEFT JOIN (
    SELECT A.CONFIG_UID,
	       A.LIST_HEADER_ID,
		   A.LIST_LINE_ID,
		   A.OPT_DESC,
		   A.ASS_DESC,
		   B.FLAG_ASSIGN 
    FROM SIS_CONFIG A INNER JOIN SYS_SIS_L B 
	  ON A.LIST_HEADER_ID=B.LIST_HEADER_ID 
	  AND A.LIST_LINE_ID=B.LIST_LINE_ID 
	  WHERE A.CONFIG_UID='{opConfigUid}'
	) S
ON L.LIST_HEADER_ID=S.LIST_HEADER_ID 
WHERE  L.ORGANIZATION_ID='{opOrganizationId}'
       AND L.FLAG_CONFIG='Y' 
	   AND L.ACTIVE_FLAG='Y' 
	   AND L.MAINTAIN_FLAG='業助' 
      AND L.SP_INS_NAME != '奇美併櫃庫別說明'
ORDER BY LIST_SEQ_NEW
```




SQL-2
```sql
SELECT 
       CASE WHEN (FLAG_ASSIGN IS NULL OR FLAG_ASSIGN = '') 
         THEN 'N'  
         ELSE FLAG_ASSIGN   
       END AS SHOWASSDESC,  
       cast(ISNULL(FLAG_ASSIGN,'N') as varchar(20))+cast(LIST_LINE_ID as varchar(20)) [VALUE],
       cast(ISNULL(LIST_SEQ,'') as varchar(20))+'-'+OPT_DESC SHOW_TEXT,
       OPT_DESC [TEXT] 
FROM SYS_SIS_L 
WHERE LIST_HEADER_ID={LIST_HEADER_ID} 
ORDER BY LIST_SEQ
```

SQL-3
```sql
SELECT 
    S.CONFIG_UID,
    S.CUST_NO,
    C.CUST_NAME,
    S.ITEM_NO
FROM SIS_CONFIG_H S
    LEFT JOIN (
        SELECT DISTINCT CUST_NO,
            CUST_NAME
        FROM SO_H
    ) C ON S.CUST_NO = C.CUST_NO
WHERE S.CONFIG_UID = {opConfigUid}
```

# Response 欄位
【 content 】
| 欄位        | 名稱             | 資料型別 | 資料儲存 & 說明               |
|-------------|------------------|----------|-------------------------------|
| opConfigUid | Config UID       | string   | NULL or S.CONFIG_UID  (SQL-3) |
| opCustNo    | 客戶代號         | string   | NULL or S.CUST_NO  (SQL-3)    |
| opCustName  | 客戶名稱         | string   | NULL or C.CUST_NAME  (SQL-3)  |
| opItemNo    | 料號             | string   | NULL or S.ITEM_NO   (SQL-3)   |
| opSisList   | 出貨指示選項清單 | array    |                               |

【 opSisList array】
| 欄位            | 名稱                 | 資料型別 | 資料儲存 & 說明                               |
|-----------------|----------------------|----------|-----------------------------------------------|
| opAssDesc       | 指定內容             | string   | S.ASS_DESC                                    |
| opCategoryAttri | 類別                 | string   | L.CATEGORY_FLAG                               |
| opConfigUid     | Config UID           | string   | {opconfigUid}                                 |
| opFlagAssign    | 可否指定內容         | string   | S.FLAG_ASSIGN                                 |
| opListHeaderId  | 出貨指示項目ID       | string   | CONVERT(int, L.LIST_HEADER_ID) LIST_HEADER_ID |
| opListLineId    | 出貨指示選項ID       | string   | CONVERT(int, S.LIST_LINE_ID) LIST_LINE_ID     |
| opListSeq       | 順序                 | string   | LIST_SEQ_NEW                                  |
| opOptDesc       | 選項內容             | string   | S.OPT_DESC                                    |
| opSpInsName     | 出貨指示名稱         | string   | L.SP_INS_NAME                                 |
| opModifyFlag    | 後端元件對齊用欄位   | string   | 固定為 NULL                                   |
| opShowAssDesc   | 是否顯示指定內容元件 | string   | {ShowAssDesc}  Y/N                            |
| opOptionList    | 選項清單             | array    |                                               |

【 opOptionList array】
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填
| 欄位       | 名稱                        | 資料型別 | 資料儲存 & 說明                                            |
|------------|---------------------------|----------|------------------------------------------------------------|
| opShowText | 順序_選項內容               | string   | ISNULL(SYS_SIS_L.LIST_SEQ,'') + '-' + SYS_SIS_L.OPT_DESC   |
| opText     | 選項內容                    | string   | SYS_SIS_L.OPT_DESC                                         |
| opValue    | 是否指定內容_出貨指示選項ID | string   | ISNULL(SYS_SIS_L.FLAG_ASSIGN,'N') + SYS_SIS_L.LIST_LINE_ID |


#### Response 範例

```json
{
   "msgCode":null,
   "result":{
      "content":{
         "opConfigUid": null,
         "opCustNo": null,
         "opCustName": null,
         "opItemNo": null,
         "opSisList":[
         {
            "opAssDesc": null,
            "opCategoryAttri":"生產",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"48",
            "opListLineId": null,
            "opListSeq":"115",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"Y",
            "opOptionList":[
               {
                  "opShowText":"1-原料無指定Lot",
                  "opText":"原料無指定Lot",
                  "opValue":"N75"
               },
               {
                  "opShowText":"2-原料使用同一Lot生產",
                  "opText":"原料使用同一Lot生產",
                  "opValue":"N76"
               },
               {
                  "opShowText":"3-原料使用客戶指定Lot:",
                  "opText":"原料使用客戶指定Lot:",
                  "opValue":"Y77"
               }
            ],
            "opSpInsName":"原料Lot需求"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"生產",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"49",
            "opListLineId": null,
            "opListSeq":"120",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"2-數量盡量勿短少",
                  "opText":"數量盡量勿短少",
                  "opValue":"N79"
               },
               {
                  "opShowText":"3-數量勿短少",
                  "opText":"數量勿短少",
                  "opValue":"N80"
               }
            ],
            "opSpInsName":"成品足量指示"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"生產",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"156",
            "opListLineId": null,
            "opListSeq":"150",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"Y",
            "opOptionList":[
               {
                  "opShowText":"1-額外添加要求:產線成品外加L-09(EBA)",
                  "opText":"額外添加要求:產線成品外加L-09(EBA)",
                  "opValue":"N1292"
               },
               {
                  "opShowText":"2-其他添加為:",
                  "opText":"其他添加為:",
                  "opValue":"Y1293"
               }
            ],
            "opSpInsName":"額外添加要求"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"生產",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"158",
            "opListLineId": null,
            "opListSeq":"155",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-一般級",
                  "opText":"一般級",
                  "opValue":"N1326"
               },
               {
                  "opShowText":"2-醫療級",
                  "opText":"醫療級",
                  "opValue":"N1327"
               }
            ],
            "opSpInsName":"產品類別"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"包裝",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"79",
            "opListLineId": null,
            "opListSeq":"305",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"8-壓克力袋",
                  "opText":"壓克力袋",
                  "opValue":"N469"
               },
               {
                  "opShowText":"13-空白袋",
                  "opText":"空白袋",
                  "opValue":"N474"
               },
               {
                  "opShowText":"14-空白袋+PE內襯",
                  "opText":"空白袋+PE內襯",
                  "opValue":"N475"
               },
               {
                  "opShowText":"15-散袋櫃",
                  "opText":"散袋櫃",
                  "opValue":"N476"
               },
               {
                  "opShowText":"17-瑞健專用太空包",
                  "opText":"瑞健專用太空包",
                  "opValue":"N479"
               },
               {
                  "opShowText":"22-Polylac防擴太空包-ABS",
                  "opText":"Polylac防擴太空包-ABS",
                  "opValue":"N1223"
               },
               {
                  "opShowText":"25-WONDERLOY防擴太空包-PC合金",
                  "opText":"WONDERLOY防擴太空包-PC合金",
                  "opValue":"N1265"
               },
               {
                  "opShowText":"26-Polyrex防擴太空包- HIPS",
                  "opText":"Polyrex防擴太空包- HIPS",
                  "opValue":"N1271"
               },
               {
                  "opShowText":"30-ACRYREX防擴太空包-PMMA",
                  "opText":"ACRYREX防擴太空包-PMMA",
                  "opValue":"N1307"
               },
               {
                  "opShowText":"31-WONDERLITE防擴太空包-PC",
                  "opText":"WONDERLITE防擴太空包-PC",
                  "opValue":"N1309"
               },
               {
                  "opShowText":"32-KIBISAN防擴太空包-AS(奇美提供)",
                  "opText":"KIBISAN防擴太空包-AS(奇美提供)",
                  "opValue":"N1319"
               },
               {
                  "opShowText":"34-空白防擴太空包(無內襯、白掛耳)",
                  "opText":"空白防擴太空包(無內襯、白掛耳)",
                  "opValue":"N1322"
               },
               {
                  "opShowText":"35-ACRYSTEX防擴太空包-MS(奇美提供)",
                  "opText":"ACRYSTEX防擴太空包-MS(奇美提供)",
                  "opValue":"N1328"
               },
               {
                  "opShowText":"36-KIBILAC防擴太空包-ASA(奇美提供)",
                  "opText":"KIBILAC防擴太空包-ASA(奇美提供)",
                  "opValue":"N1332"
               },
               {
                  "opShowText":"39-POLYLAC無內襯防擴太空包-MABS(奇美提供)",
                  "opText":"POLYLAC無內襯防擴太空包-MABS(奇美提供)",
                  "opValue":"N1354"
               },
               {
                  "opShowText":"40-瑞健防擴太空包(PE外袋)",
                  "opText":"瑞健防擴太空包(PE外袋)",
                  "opValue":"N1357"
               },
               {
                  "opShowText":"42-ABS(OPP袋)",
                  "opText":"ABS(OPP袋)",
                  "opValue":"N1398"
               },
               {
                  "opShowText":"43-AS(OPP袋)",
                  "opText":"AS(OPP袋)",
                  "opValue":"N1399"
               },
               {
                  "opShowText":"44-ASA(OPP袋)",
                  "opText":"ASA(OPP袋)",
                  "opValue":"N1400"
               },
               {
                  "opShowText":"45-HIPS(OPP袋)",
                  "opText":"HIPS(OPP袋)",
                  "opValue":"N1401"
               },
               {
                  "opShowText":"46-MABS(OPP袋)",
                  "opText":"MABS(OPP袋)",
                  "opValue":"N1402"
               },
               {
                  "opShowText":"47-PC樹脂(PE袋)",
                  "opText":"PC樹脂(PE袋)",
                  "opValue":"N1403"
               },
               {
                  "opShowText":"48-PC合金樹脂袋(PE袋)",
                  "opText":"PC合金樹脂袋(PE袋)",
                  "opValue":"N1405"
               },
               {
                  "opShowText":"49-PC樹脂(OPP袋)/奇菱專用袋",
                  "opText":"PC樹脂(OPP袋)/奇菱專用袋",
                  "opValue":"N1406"
               },
               {
                  "opShowText":"50-PC合金樹脂(OPP袋)/奇菱專用袋",
                  "opText":"PC合金樹脂(OPP袋)/奇菱專用袋",
                  "opValue":"N1404"
               },
               {
                  "opShowText":"51-奇菱合膠(OPP袋)",
                  "opText":"奇菱合膠(OPP袋)",
                  "opValue":"N1407"
               },
               {
                  "opShowText":"52-奇菱合膠(PE袋)",
                  "opText":"奇菱合膠(PE袋)",
                  "opValue":"N1408"
               },
               {
                  "opShowText":"53-奇菱色母(OPP袋)",
                  "opText":"奇菱色母(OPP袋)",
                  "opValue":"N1409"
               },
               {
                  "opShowText":"54-奇菱消費後再生料(OPP袋)",
                  "opText":"奇菱消費後再生料(OPP袋)",
                  "opValue":"N1410"
               },
               {
                  "opShowText":"55-空白(OPP袋)",
                  "opText":"空白(OPP袋)",
                  "opValue":"N1411"
               },
               {
                  "opShowText":"56-空白(PE袋)",
                  "opText":"空白(PE袋)",
                  "opValue":"N1412"
               },
               {
                  "opShowText":"57-空白非防擴太空包(有襯、藍白雙掛耳)",
                  "opText":"空白非防擴太空包(有襯、藍白雙掛耳)",
                  "opValue":"N1423"
               },
               {
                  "opShowText":"58-MS袋(OPP袋 奇美提供)",
                  "opText":"MS袋(OPP袋 奇美提供)",
                  "opValue":"N1426"
               },
               {
                  "opShowText":"59-PCR-ABS(OPP袋 奇美提供)",
                  "opText":"PCR-ABS(OPP袋 奇美提供)",
                  "opValue":"N1432"
               },
               {
                  "opShowText":"60-PCR-ALLOY(OPP袋 奇美提供)",
                  "opText":"PCR-ALLOY(OPP袋 奇美提供)",
                  "opValue":"N1433"
               },
               {
                  "opShowText":"61-PCR-PC(OPP袋 奇美提供)",
                  "opText":"PCR-PC(OPP袋 奇美提供)",
                  "opValue":"N1434"
               },
               {
                  "opShowText":"62-空白非防擴太空包(無內襯、紅+白雙掛耳)",
                  "opText":"空白非防擴太空包(無內襯、紅+白雙掛耳)",
                  "opValue":"N1439"
               },
               {
                  "opShowText":"63-奇菱特用材料(OPP袋)",
                  "opText":"奇菱特用材料(OPP袋)",
                  "opValue":"N1443"
               },
               {
                  "opShowText":"64-空白非防擴太空包(無內襯、白掛耳)",
                  "opText":"空白非防擴太空包(無內襯、白掛耳)",
                  "opValue":"N1456"
               },
               {
                  "opShowText":"65-延用原外袋",
                  "opText":"延用原外袋",
                  "opValue":"N1459"
               }
            ],
            "opSpInsName":"外袋別"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"包裝",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"80",
            "opListLineId": null,
            "opListSeq":"306",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-外袋重:25Kg",
                  "opText":"外袋重:25Kg",
                  "opValue":"N482"
               },
               {
                  "opShowText":"2-外袋重:20Kg",
                  "opText":"外袋重:20Kg",
                  "opValue":"N483"
               },
               {
                  "opShowText":"3-外袋重:300Kg",
                  "opText":"外袋重:300Kg",
                  "opValue":"N484"
               },
               {
                  "opShowText":"4-外袋重:500Kg",
                  "opText":"外袋重:500Kg",
                  "opValue":"N485"
               },
               {
                  "opShowText":"5-外袋重:800Kg",
                  "opText":"外袋重:800Kg",
                  "opValue":"N486"
               },
               {
                  "opShowText":"6-外袋重:900Kg",
                  "opText":"外袋重:900Kg",
                  "opValue":"N1052"
               },
               {
                  "opShowText":"8-外袋重:1000KG",
                  "opText":"外袋重:1000KG",
                  "opValue":"N487"
               },
               {
                  "opShowText":"9-外袋重:20捲(1捲=1KG)",
                  "opText":"外袋重:20捲(1捲=1KG)",
                  "opValue":"N1295"
               },
               {
                  "opShowText":"10-外袋重:700Kg",
                  "opText":"外袋重:700Kg",
                  "opValue":"N1348"
               },
               {
                  "opShowText":"11-外袋重:250KG",
                  "opText":"外袋重:250KG",
                  "opValue":"N1457"
               }
            ],
            "opSpInsName":"外袋重"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"包裝",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"57",
            "opListLineId": null,
            "opListSeq":"315",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"Y",
            "opOptionList":[
               {
                  "opShowText":"1-外袋Grade No.:",
                  "opText":"外袋Grade No.:",
                  "opValue":"N91"
               },
               {
                  "opShowText":"2-外袋Grade No.指定為:",
                  "opText":"外袋Grade No.指定為:",
                  "opValue":"Y92"
               },
               {
                  "opShowText":"3-外袋Grade No.:無須噴印",
                  "opText":"外袋Grade No.:無須噴印",
                  "opValue":"N1066"
               }
            ],
            "opSpInsName":"外袋Grade No.噴印內容"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"包裝",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"90",
            "opListLineId": null,
            "opListSeq":"320",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-外袋Lot No.:依內規",
                  "opText":"外袋Lot No.:依內規",
                  "opValue":"N702"
               },
               {
                  "opShowText":"2-外袋Lot No.:編奇美Lot No",
                  "opText":"外袋Lot No.:編奇美Lot No",
                  "opValue":"N703"
               },
               {
                  "opShowText":"3-外袋Lot No.:編奇菱Lot No",
                  "opText":"外袋Lot No.:編奇菱Lot No",
                  "opValue":"N704"
               },
               {
                  "opShowText":"4-外袋Lot No.:延用原料Lot No",
                  "opText":"外袋Lot No.:延用原料Lot No",
                  "opValue":"N705"
               },
               {
                  "opShowText":"5-外袋Lot No.:無須噴印",
                  "opText":"外袋Lot No.:無須噴印",
                  "opValue":"N1067"
               },
               {
                  "opShowText":"6-外袋Lot No.:依明細行備註指示",
                  "opText":"外袋Lot No.:依明細行備註指示",
                  "opValue":"N1313"
               }
            ],
            "opSpInsName":"外袋LotNo.規則"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"包裝",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"59",
            "opListLineId": null,
            "opListSeq":"325",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-標籤:一般格式",
                  "opText":"標籤:一般格式",
                  "opValue":"N97"
               },
               {
                  "opShowText":"2-標籤:含RoHS字樣",
                  "opText":"標籤:含RoHS字樣",
                  "opValue":"N98"
               },
               {
                  "opShowText":"3-標籤:客戶格式",
                  "opText":"標籤:客戶格式",
                  "opValue":"N99"
               },
               {
                  "opShowText":"4-標籤:不貼",
                  "opText":"標籤:不貼",
                  "opValue":"N100"
               },
               {
                  "opShowText":"5-標籤:框料專用",
                  "opText":"標籤:框料專用",
                  "opValue":"N1077"
               },
               {
                  "opShowText":"6-標籤:含GP字樣",
                  "opText":"標籤:含GP字樣",
                  "opValue":"N1214"
               },
               {
                  "opShowText":"7-標籤:太空包格式",
                  "opText":"標籤:太空包格式",
                  "opValue":"N1302"
               },
               {
                  "opShowText":"8-標籤:依明細行備註指示",
                  "opText":"標籤:依明細行備註指示",
                  "opValue":"N1310"
               },
               {
                  "opShowText":"9-標籤:醫療級專用(粉紅底)",
                  "opText":"標籤:醫療級專用(粉紅底)",
                  "opValue":"N1333"
               }
            ],
            "opSpInsName":"標籤種類"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"包裝",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"60",
            "opListLineId": null,
            "opListSeq":"330",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"Y",
            "opOptionList":[
               {
                  "opShowText":"1-標籤顏色:依內規",
                  "opText":"標籤顏色:依內規",
                  "opValue":"N101"
               },
               {
                  "opShowText":"2-指定標籤顏色為:",
                  "opText":"指定標籤顏色為:",
                  "opValue":"Y102"
               },
               {
                  "opShowText":"3-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1072"
               },
               {
                  "opShowText":"4-依明細行備註指示",
                  "opText":"依明細行備註指示",
                  "opValue":"N1274"
               }
            ],
            "opSpInsName":"標籤顏色"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"包裝",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"109",
            "opListLineId": null,
            "opListSeq":"335",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-標籤Lot No.:不顯示",
                  "opText":"標籤Lot No.:不顯示",
                  "opValue":"N1041"
               },
               {
                  "opShowText":"2-標籤Lot No.:奇菱Lot No.",
                  "opText":"標籤Lot No.:奇菱Lot No.",
                  "opValue":"N1042"
               },
               {
                  "opShowText":"3-標籤Lot No.:每5T合樣",
                  "opText":"標籤Lot No.:每5T合樣",
                  "opValue":"N1068"
               },
               {
                  "opShowText":"4-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1073"
               },
               {
                  "opShowText":"5-標籤Lot No.:每5T合標",
                  "opText":"標籤Lot No.:每5T合標",
                  "opValue":"N1291"
               },
               {
                  "opShowText":"6-標籤Lot No.:沿用原料Lot No",
                  "opText":"標籤Lot No.:沿用原料Lot No",
                  "opValue":"N1311"
               }
            ],
            "opSpInsName":"標籤Lot No.顯示"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"包裝",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"110",
            "opListLineId": null,
            "opListSeq":"340",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-Lot No.每10噸跳號",
                  "opText":"Lot No.每10噸跳號",
                  "opValue":"N1043"
               },
               {
                  "opShowText":"2-Lot No.每5噸跳號",
                  "opText":"Lot No.每5噸跳號",
                  "opValue":"N1044"
               },
               {
                  "opShowText":"3-Lot No.每10板跳號",
                  "opText":"Lot No.每10板跳號",
                  "opValue":"N1045"
               },
               {
                  "opShowText":"4-Lot No.每工單跳號",
                  "opText":"Lot No.每工單跳號",
                  "opValue":"N1046"
               },
               {
                  "opShowText":"5-Lot No.每12板跳號",
                  "opText":"Lot No.每12板跳號",
                  "opValue":"N1051"
               },
               {
                  "opShowText":"6-Lot No.每2.1噸跳號",
                  "opText":"Lot No.每2.1噸跳號",
                  "opValue":"N1076"
               },
               {
                  "opShowText":"7-Lot No.每16噸跳號",
                  "opText":"Lot No.每16噸跳號",
                  "opValue":"N1079"
               },
               {
                  "opShowText":"8-Lot No.每2.4噸跳號",
                  "opText":"Lot No.每2.4噸跳號",
                  "opValue":"N1209"
               },
               {
                  "opShowText":"9-Lot No.每35噸跳號",
                  "opText":"Lot No.每35噸跳號",
                  "opValue":"N1296"
               },
               {
                  "opShowText":"10-Lot No.依明細行備註指示",
                  "opText":"Lot No.依明細行備註指示",
                  "opValue":"N1315"
               },
               {
                  "opShowText":"11-Lot No.每20噸跳號",
                  "opText":"Lot No.每20噸跳號",
                  "opValue":"N1361"
               },
               {
                  "opShowText":"12-Lot No.每24板跳號",
                  "opText":"Lot No.每24板跳號",
                  "opValue":"N1362"
               },
               {
                  "opShowText":"13-Lot No.每9噸跳號",
                  "opText":"Lot No.每9噸跳號",
                  "opValue":"N1374"
               },
               {
                  "opShowText":"14-Lot No.每15噸跳號",
                  "opText":"Lot No.每15噸跳號",
                  "opValue":"N1375"
               },
               {
                  "opShowText":"15-Lot No.一天一LOT NO",
                  "opText":"Lot No.一天一LOT NO",
                  "opValue":"N1391"
               }
            ],
            "opSpInsName":"Lot No.跳號頻率"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"包裝",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"92",
            "opListLineId": null,
            "opListSeq":"345",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-25KG包裝袋 ,奇美塑膠棧板+SHEET",
                  "opText":"25KG包裝袋 ,奇美塑膠棧板+SHEET",
                  "opValue":"N905"
               },
               {
                  "opShowText":"5-1000打包,奇美歐高熱棧板+上下紙板",
                  "opText":"1000打包,奇美歐高熱棧板+上下紙板",
                  "opValue":"N909"
               },
               {
                  "opShowText":"6-1000打包,奇美一般熱棧板+上下紙板",
                  "opText":"1000打包,奇美一般熱棧板+上下紙板",
                  "opValue":"N911"
               },
               {
                  "opShowText":"7-1000瑞健太空包,瑞健綠色棧板+下紙板",
                  "opText":"1000瑞健太空包,瑞健綠色棧板+下紙板",
                  "opValue":"N912"
               },
               {
                  "opShowText":"17-20KG包裝袋,奇美一般棧板+SHEET",
                  "opText":"20KG包裝袋,奇美一般棧板+SHEET",
                  "opValue":"N921"
               },
               {
                  "opShowText":"18-25KG包裝袋,奇美一般棧板+SHEET",
                  "opText":"25KG包裝袋,奇美一般棧板+SHEET",
                  "opValue":"N922"
               },
               {
                  "opShowText":"19-25KG包裝袋,不回收棧板+回收紙板",
                  "opText":"25KG包裝袋,不回收棧板+回收紙板",
                  "opValue":"N923"
               },
               {
                  "opShowText":"20-1000打包,不回收棧板+上下紙板",
                  "opText":"1000打包,不回收棧板+上下紙板",
                  "opValue":"N925"
               },
               {
                  "opShowText":"22-900太空包,松木棧板",
                  "opText":"900太空包,松木棧板",
                  "opValue":"N928"
               },
               {
                  "opShowText":"24-1000太空包(外銷吹櫃),自購鐵棧板",
                  "opText":"1000太空包(外銷吹櫃),自購鐵棧板",
                  "opValue":"N934"
               },
               {
                  "opShowText":"27-25KG包裝袋(45包/板),奇美塑膠棧板(主) SHEET*1上紙板打包,奇美木頭棧板 SHEET*2(替) ",
                  "opText":"25KG包裝袋(45包/板),奇美塑膠棧板(主) SHEET*1上紙板打包,奇美木頭棧板 SHEET*2(替) ",
                  "opValue":"N937"
               },
               {
                  "opShowText":"28-500雙耳太空包,不回收棧板+上下紙板",
                  "opText":"500雙耳太空包,不回收棧板+上下紙板",
                  "opValue":"N1213"
               },
               {
                  "opShowText":"30-25KG包裝袋(50包/板),奇美塑膠棧板(主) SHEET*1上紙板打包,奇美木頭棧板 SHEET*2(替)",
                  "opText":"25KG包裝袋(50包/板),奇美塑膠棧板(主) SHEET*1上紙板打包,奇美木頭棧板 SHEET*2(替)",
                  "opValue":"N1220"
               },
               {
                  "opShowText":"31-1000打包,自購歐高塑膠棧板+上下紙板",
                  "opText":"1000打包,自購歐高塑膠棧板+上下紙板",
                  "opValue":"N1222"
               },
               {
                  "opShowText":"32-800打包,奇美一般熱棧板+上下紙板",
                  "opText":"800打包,奇美一般熱棧板+上下紙板",
                  "opValue":"N938"
               },
               {
                  "opShowText":"68-875打包,奇美歐高熱棧板+上下紙板",
                  "opText":"875打包,奇美歐高熱棧板+上下紙板",
                  "opValue":"N970"
               },
               {
                  "opShowText":"73-875打包,自購歐高熱棧板+上下紙板",
                  "opText":"875打包,自購歐高熱棧板+上下紙板",
                  "opValue":"N975"
               },
               {
                  "opShowText":"74-1000打包,自購一般熱棧板+上下紙板",
                  "opText":"1000打包,自購一般熱棧板+上下紙板",
                  "opValue":"N976"
               },
               {
                  "opShowText":"76-1000打包,自購歐高熱棧板+上下紙板",
                  "opText":"1000打包,自購歐高熱棧板+上下紙板",
                  "opValue":"N979"
               },
               {
                  "opShowText":"100-1000打包,奇美四面叉熱棧板+上下紙板",
                  "opText":"1000打包,奇美四面叉熱棧板+上下紙板",
                  "opValue":"N1001"
               },
               {
                  "opShowText":"103-875打包,自購歐高塑膠棧板+上下紙板",
                  "opText":"875打包,自購歐高塑膠棧板+上下紙板",
                  "opValue":"N1003"
               },
               {
                  "opShowText":"107-800打包,奇美四面叉取熱棧板+上下紙板",
                  "opText":"800打包,奇美四面叉取熱棧板+上下紙板",
                  "opValue":"N1006"
               },
               {
                  "opShowText":"115-包裝袋打包,瑞健專用棧板+上下紙板",
                  "opText":"包裝袋打包,瑞健專用棧板+上下紙板",
                  "opValue":"N1075"
               },
               {
                  "opShowText":"119-900KG太空包,熱高松木棧板",
                  "opText":"900KG太空包,熱高松木棧板",
                  "opValue":"N1226"
               },
               {
                  "opShowText":"120-900太空包(內銷),不回收棧板+下紙板",
                  "opText":"900太空包(內銷),不回收棧板+下紙板",
                  "opValue":"N1228"
               },
               {
                  "opShowText":"125-875打包,自購四面叉取熱棧板+上下紙板",
                  "opText":"875打包,自購四面叉取熱棧板+上下紙板",
                  "opValue":"N1285"
               },
               {
                  "opShowText":"128-700打包,儒億專用歐規雙面叉一般棧板+上下紙板",
                  "opText":"700打包,儒億專用歐規雙面叉一般棧板+上下紙板",
                  "opValue":"N1323"
               },
               {
                  "opShowText":"130-1000打包,自購四面叉取熱棧板+上下紙板",
                  "opText":"1000打包,自購四面叉取熱棧板+上下紙板",
                  "opValue":"N1334"
               },
               {
                  "opShowText":"132-500KG太空包,熱松木棧板",
                  "opText":"500KG太空包,熱松木棧板",
                  "opValue":"N1349"
               },
               {
                  "opShowText":"133-800打包,自購一般熱棧板+紙板",
                  "opText":"800打包,自購一般熱棧板+紙板",
                  "opValue":"N1350"
               },
               {
                  "opShowText":"134-500空白防擴太空包(無內襯、白掛耳) ,奇美色母專用塑膠棧板",
                  "opText":"500空白防擴太空包(無內襯、白掛耳) ,奇美色母專用塑膠棧板",
                  "opValue":"N1351"
               },
               {
                  "opShowText":"136-1000太空包,自購四面叉取熱棧板+SHEET",
                  "opText":"1000太空包,自購四面叉取熱棧板+SHEET",
                  "opValue":"N1353"
               },
               {
                  "opShowText":"139-25KG包裝袋,奇美大億專用棧板+上下紙板",
                  "opText":"25KG包裝袋,奇美大億專用棧板+上下紙板",
                  "opValue":"N1363"
               },
               {
                  "opShowText":"142-900太空包,自購塑膠棧板+紙板",
                  "opText":"900太空包,自購塑膠棧板+紙板",
                  "opValue":"N1368"
               },
               {
                  "opShowText":"143-1000打包,自購塑膠棧板+紙板",
                  "opText":"1000打包,自購塑膠棧板+紙板",
                  "opValue":"N1369"
               },
               {
                  "opShowText":"144-1000太空包,自購塑膠棧板+紙板",
                  "opText":"1000太空包,自購塑膠棧板+紙板",
                  "opValue":"N1372"
               },
               {
                  "opShowText":"145-800打包,奇美歐高熱棧板+上下紙板",
                  "opText":"800打包,奇美歐高熱棧板+上下紙板",
                  "opValue":"N1373"
               },
               {
                  "opShowText":"146-1125打包(45包/板),奇美一般熱棧板+上下紙板",
                  "opText":"1125打包(45包/板),奇美一般熱棧板+上下紙板",
                  "opValue":"N1384"
               },
               {
                  "opShowText":"147-875KG(35*25KG)打包/奇美四面叉熱棧板+上下紙板",
                  "opText":"875KG(35*25KG)打包/奇美四面叉熱棧板+上下紙板",
                  "opValue":"N1385"
               },
               {
                  "opShowText":"149-800包裝袋打包,瑞健專用棧板+上下紙板",
                  "opText":"800包裝袋打包,瑞健專用棧板+上下紙板",
                  "opValue":"N1389"
               },
               {
                  "opShowText":"150-1000打包, 自購黑色網面田字型塑膠棧板+上下紙板",
                  "opText":"1000打包, 自購黑色網面田字型塑膠棧板+上下紙板",
                  "opValue":"N1392"
               },
               {
                  "opShowText":"151-900KG太空包/新型塑膠棧板(雙面網狀)",
                  "opText":"900KG太空包/新型塑膠棧板(雙面網狀)",
                  "opValue":"N1393"
               },
               {
                  "opShowText":"152-1000打包,家登專用棧板+上下紙板",
                  "opText":"1000打包,家登專用棧板+上下紙板",
                  "opValue":"N1397"
               },
               {
                  "opShowText":"153-500太空包,自購塑膠棧板+紙板",
                  "opText":"500太空包,自購塑膠棧板+紙板",
                  "opValue":"N1413"
               },
               {
                  "opShowText":"154-900太空包,塑膠棧板1100*1100*130 mm 田字型",
                  "opText":"900太空包,塑膠棧板1100*1100*130 mm 田字型",
                  "opValue":"N1425"
               },
               {
                  "opShowText":"155-25KG包裝袋【≧10包打包】，塑膠棧板+SHEET+上紙板",
                  "opText":"25KG包裝袋【≧10包打包】，塑膠棧板+SHEET+上紙板",
                  "opValue":"N1427"
               },
               {
                  "opShowText":"156-25KG包裝袋【≧10包打包】，一般棧板+SHEET+上紙板",
                  "opText":"25KG包裝袋【≧10包打包】，一般棧板+SHEET+上紙板",
                  "opValue":"N1428"
               },
               {
                  "opShowText":"157-500打包, 自購黑色網面田字型塑膠棧板+上下紙板",
                  "opText":"500打包, 自購黑色網面田字型塑膠棧板+上下紙板",
                  "opValue":"N1437"
               },
               {
                  "opShowText":"158-500空白非防擴太空包(無內襯、紅+白雙掛耳),奇美色母專用塑膠棧板",
                  "opText":"500空白非防擴太空包(無內襯、紅+白雙掛耳),奇美色母專用塑膠棧板",
                  "opValue":"N1438"
               },
               {
                  "opShowText":"159-25KG包裝袋(45包/板),奇美蘋果綠塑膠棧板+SHEET*1+上紙板打包",
                  "opText":"25KG包裝袋(45包/板),奇美蘋果綠塑膠棧板+SHEET*1+上紙板打包",
                  "opValue":"N1440"
               },
               {
                  "opShowText":"160-25KG包裝袋(50包/板),奇美蘋果綠塑膠棧板+SHEET*1+上紙板打包",
                  "opText":"25KG包裝袋(50包/板),奇美蘋果綠塑膠棧板+SHEET*1+上紙板打包",
                  "opValue":"N1441"
               },
               {
                  "opShowText":"161-300空白非防擴太空包(無內襯、白掛耳) ,奇美色母專用塑膠棧板",
                  "opText":"300空白非防擴太空包(無內襯、白掛耳) ,奇美色母專用塑膠棧板",
                  "opValue":"N1458"
               }
            ],
            "opSpInsName":"棧板與底板類別"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"包裝",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"88",
            "opListLineId": null,
            "opListSeq":"350",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-棧板每層4包",
                  "opText":"棧板每層4包",
                  "opValue":"N521"
               },
               {
                  "opShowText":"2-棧板每層5包",
                  "opText":"棧板每層5包",
                  "opValue":"N522"
               },
               {
                  "opShowText":"3-指定棧板堆疊方式:",
                  "opText":"指定棧板堆疊方式:",
                  "opValue":"Y523"
               },
               {
                  "opShowText":"4-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1060"
               }
            ],
            "opSpInsName":"棧板特殊堆疊方式"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"包裝",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"93",
            "opListLineId": null,
            "opListSeq":"355",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-上下舖完整硬紙板",
                  "opText":"上下舖完整硬紙板",
                  "opValue":"N1016"
               },
               {
                  "opShowText":"2-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1061"
               }
            ],
            "opSpInsName":"紙板特殊要求"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"包裝",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"155",
            "opListLineId": null,
            "opListSeq":"360",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-標籤朝棧板短邊放置",
                  "opText":"標籤朝棧板短邊放置",
                  "opValue":"N1286"
               }
            ],
            "opSpInsName":"棧板特殊堆疊(表身)"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"包裝",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"161",
            "opListLineId": null,
            "opListSeq":"365",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-OPP封口條+PP繩+防偽字體",
                  "opText":"OPP封口條+PP繩+防偽字體",
                  "opValue":"N1445"
               },
               {
                  "opShowText":"2-防偽字體",
                  "opText":"防偽字體",
                  "opValue":"N1446"
               },
               {
                  "opShowText":"3-其他",
                  "opText":"其他",
                  "opValue":"N1447"
               }
            ],
            "opSpInsName":"外袋特殊要求"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"檢驗",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"106",
            "opListLineId": null,
            "opListSeq":"605",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-色板及數據X1",
                  "opText":"色板及數據X1",
                  "opValue":"N1034"
               },
               {
                  "opShowText":"2-色板及數據X2",
                  "opText":"色板及數據X2",
                  "opValue":"N1035"
               },
               {
                  "opShowText":"3-色板及數據X3",
                  "opText":"色板及數據X3",
                  "opValue":"N1036"
               },
               {
                  "opShowText":"4-圓盤及數據X3",
                  "opText":"圓盤及數據X3",
                  "opValue":"N1037"
               },
               {
                  "opShowText":"5-色板及數據份數:",
                  "opText":"色板及數據份數:",
                  "opValue":"Y1038"
               },
               {
                  "opShowText":"6-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1064"
               },
               {
                  "opShowText":"7-每LOT附色板X3及PRESSX1+500g樣粒",
                  "opText":"每LOT附色板X3及PRESSX1+500g樣粒",
                  "opValue":"N1210"
               },
               {
                  "opShowText":"8-每LOT附色板X3+500g樣粒",
                  "opText":"每LOT附色板X3+500g樣粒",
                  "opValue":"N1211"
               },
               {
                  "opShowText":"9-每LOT附色板及數據X3",
                  "opText":"每LOT附色板及數據X3",
                  "opValue":"N1297"
               }
            ],
            "opSpInsName":"客戶留板"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"檢驗",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"111",
            "opListLineId": null,
            "opListSeq":"610",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-附板原料:",
                  "opText":"附板原料:",
                  "opValue":"N1047"
               },
               {
                  "opShowText":"2-附板原料指定:",
                  "opText":"附板原料指定:",
                  "opValue":"Y1048"
               },
               {
                  "opShowText":"3-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1065"
               }
            ],
            "opSpInsName":"附板原料註明"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"檢驗",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"67",
            "opListLineId": null,
            "opListSeq":"615",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-驗色原料:",
                  "opText":"驗色原料:",
                  "opValue":"N118"
               },
               {
                  "opShowText":"2-驗色原料指定為:",
                  "opText":"驗色原料指定為:",
                  "opValue":"Y119"
               },
               {
                  "opShowText":"3-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1055"
               }
            ],
            "opSpInsName":"驗色原料"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"檢驗",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"68",
            "opListLineId": null,
            "opListSeq":"620",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-不需附COA",
                  "opText":"不需附COA",
                  "opValue":"N120"
               },
               {
                  "opShowText":"2-每Lot附COA出貨報告",
                  "opText":"每Lot附COA出貨報告",
                  "opValue":"N121"
               }
            ],
            "opSpInsName":"COA"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"檢驗",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"69",
            "opListLineId": null,
            "opListSeq":"625",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-COA使用廠內格式",
                  "opText":"COA使用廠內格式",
                  "opValue":"N122"
               },
               {
                  "opShowText":"2-指定COA格式:",
                  "opText":"指定COA格式:",
                  "opValue":"Y123"
               },
               {
                  "opShowText":"3-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1056"
               }
            ],
            "opSpInsName":"COA格式"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"檢驗",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"70",
            "opListLineId": null,
            "opListSeq":"630",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-測物性(MI/Tensil/Izod/比重)",
                  "opText":"測物性(MI/Tensil/Izod/比重)",
                  "opValue":"N124"
               },
               {
                  "opShowText":"3-每5噸留粒1KG打色板",
                  "opText":"每5噸留粒1KG打色板",
                  "opValue":"N126"
               },
               {
                  "opShowText":"4-測MI/MVR",
                  "opText":"測MI/MVR",
                  "opValue":"N127"
               },
               {
                  "opShowText":"5-外銷玻纖留粒4Kg測物性",
                  "opText":"外銷玻纖留粒4Kg測物性",
                  "opValue":"N128"
               },
               {
                  "opShowText":"6-其他檢測要求:",
                  "opText":"其他檢測要求:",
                  "opValue":"Y129"
               },
               {
                  "opShowText":"7-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1057"
               },
               {
                  "opShowText":"8-每LOT成品留粒4KG測物性",
                  "opText":"每LOT成品留粒4KG測物性",
                  "opValue":"N1070"
               },
               {
                  "opShowText":"9-每LOT成品留粒2.5KG打板",
                  "opText":"每LOT成品留粒2.5KG打板",
                  "opValue":"N1212"
               },
               {
                  "opShowText":"10-每桶留粒50g測MI(註明色號、lot及流水號)",
                  "opText":"每桶留粒50g測MI(註明色號、lot及流水號)",
                  "opValue":"N1215"
               },
               {
                  "opShowText":"11-測物性(MI/Tensil/Izod/比重/TM/FS/FM)",
                  "opText":"測物性(MI/Tensil/Izod/比重/TM/FS/FM)",
                  "opValue":"N1321"
               },
               {
                  "opShowText":"12-每LOT成品留粒1KG打板",
                  "opText":"每LOT成品留粒1KG打板",
                  "opValue":"N1342"
               },
               {
                  "opShowText":"13-測含水率",
                  "opText":"測含水率",
                  "opValue":"N1448"
               }
            ],
            "opSpInsName":"檢測要求"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"檢驗",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"83",
            "opListLineId": null,
            "opListSeq":"635",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-ABS料-留粒2KG",
                  "opText":"ABS料-留粒2KG",
                  "opValue":"N500"
               },
               {
                  "opShowText":"2-防火料-留粒4KG",
                  "opText":"防火料-留粒4KG",
                  "opValue":"N501"
               },
               {
                  "opShowText":"3-一般料-留粒3KG",
                  "opText":"一般料-留粒3KG",
                  "opValue":"N502"
               },
               {
                  "opShowText":"4-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1058"
               },
               {
                  "opShowText":"5-框料&新產品-留粒4KG",
                  "opText":"框料&新產品-留粒4KG",
                  "opValue":"N1071"
               },
               {
                  "opShowText":"6-一般料-留粒50G(3號游任袋)",
                  "opText":"一般料-留粒50G(3號游任袋)",
                  "opValue":"N1284"
               },
               {
                  "opShowText":"9-一般料-留粒4KG",
                  "opText":"一般料-留粒4KG",
                  "opValue":"N1343"
               },
               {
                  "opShowText":"10-APPLE白色留粒3KG+500g(膠粒黑點)",
                  "opText":"APPLE白色留粒3KG+500g(膠粒黑點)",
                  "opValue":"N1449"
               },
               {
                  "opShowText":"11-一般料-留粒4KG+2包50g(MVR/汙點)",
                  "opText":"一般料-留粒4KG+2包50g(MVR/汙點)",
                  "opValue":"N1450"
               },
               {
                  "opShowText":"12-PC-145K-留粒4KG+2包50g(MVR/汙點)+700g(膠粒汙點儀)",
                  "opText":"PC-145K-留粒4KG+2包50g(MVR/汙點)+700g(膠粒汙點儀)",
                  "opValue":"N1451"
               },
               {
                  "opShowText":"13-F17111C1-留粒4KG+2包50g(MVR/汙點)+50g(分析研究部)",
                  "opText":"F17111C1-留粒4KG+2包50g(MVR/汙點)+50g(分析研究部)",
                  "opValue":"N1452"
               }
            ],
            "opSpInsName":"測物性請選原料別"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"檢驗",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"153",
            "opListLineId": null,
            "opListSeq":"637",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"2-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1273"
               },
               {
                  "opShowText":"7-留樣X1包,1包送CLT(3KG),每噸頭尾各留存500G",
                  "opText":"留樣X1包,1包送CLT(3KG),每噸頭尾各留存500G",
                  "opValue":"N1370"
               },
               {
                  "opShowText":"8-留樣X2包,1包送CLT(3KG), 1包留存(500G)",
                  "opText":"留樣X2包,1包送CLT(3KG), 1包留存(500G)",
                  "opValue":"N1371"
               },
               {
                  "opShowText":"9-留樣X1包,1包送CMC(注意管制上下限為ΔE≦1,每10T一個LOT回CMC進行物性品管)",
                  "opText":"留樣X1包,1包送CMC(注意管制上下限為ΔE≦1,每10T一個LOT回CMC進行物性品管)",
                  "opValue":"N1388"
               },
               {
                  "opShowText":"10-留樣X1包,1包送CLT",
                  "opText":"留樣X1包,1包送CLT",
                  "opValue":"N1390"
               },
               {
                  "opShowText":"11-留樣X3包,1包送CMC(4kg),2包送CMC(50g)",
                  "opText":"留樣X3包,1包送CMC(4kg),2包送CMC(50g)",
                  "opValue":"N1453"
               },
               {
                  "opShowText":"12-留樣X4包,1包送CMC(4kg),2包送CMC(50g),1包送CMC(700g)",
                  "opText":"留樣X4包,1包送CMC(4kg),2包送CMC(50g),1包送CMC(700g)",
                  "opValue":"N1454"
               },
               {
                  "opShowText":"13-留樣X4包,1包送CMC(4kg),3包送CMC(50g)",
                  "opText":"留樣X4包,1包送CMC(4kg),3包送CMC(50g)",
                  "opValue":"N1455"
               }
            ],
            "opSpInsName":"送樣檢驗要求"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"檢驗",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"154",
            "opListLineId": null,
            "opListSeq":"638",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-每Lot留1包4號游任袋樣粒(50g) FOR XRF",
                  "opText":"每Lot留1包4號游任袋樣粒(50g) FOR XRF",
                  "opValue":"N1275"
               },
               {
                  "opShowText":"2-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1276"
               }
            ],
            "opSpInsName":"XRF留樣要求"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"檢驗",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"160",
            "opListLineId": null,
            "opListSeq":"639",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-每Lot留粒50g給品管測FTIR",
                  "opText":"每Lot留粒50g給品管測FTIR",
                  "opValue":"N1414"
               }
            ],
            "opSpInsName":"FTIR留樣要求"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"檢驗",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"84",
            "opListLineId": null,
            "opListSeq":"640",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-留粒-成品",
                  "opText":"留粒-成品",
                  "opValue":"N504"
               },
               {
                  "opShowText":"3-留粒-成品及原料",
                  "opText":"留粒-成品及原料",
                  "opValue":"N506"
               },
               {
                  "opShowText":"4-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1059"
               }
            ],
            "opSpInsName":"測物性請選留粒別"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"檢驗",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"144",
            "opListLineId": null,
            "opListSeq":"645",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-原料需IQC檢驗",
                  "opText":"原料需IQC檢驗",
                  "opValue":"N1216"
               },
               {
                  "opShowText":"2-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1217"
               }
            ],
            "opSpInsName":"進料檢驗要求"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"檢驗",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"157",
            "opListLineId": null,
            "opListSeq":"650",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-＊專案客戶黑點管控一級色號",
                  "opText":"＊專案客戶黑點管控一級色號",
                  "opValue":"N1304"
               },
               {
                  "opShowText":"2-黑點管控範圍為:",
                  "opText":"黑點管控範圍為:",
                  "opValue":"Y1305"
               },
               {
                  "opShowText":"3-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1306"
               }
            ],
            "opSpInsName":"黑點管控要求"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"包裝",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"104",
            "opListLineId": null,
            "opListSeq":"905",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-明細行備註:",
                  "opText":"明細行備註:",
                  "opValue":"Y1029"
               },
               {
                  "opShowText":"2-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1063"
               }
            ],
            "opSpInsName":"明細行備註"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"包裝",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"103",
            "opListLineId": null,
            "opListSeq":"910",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-訂單備註:",
                  "opText":"訂單備註:",
                  "opValue":"Y1027"
               },
               {
                  "opShowText":"2-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1062"
               }
            ],
            "opSpInsName":"訂單備註"
         },
         {
            "opAssDesc": null,
            "opCategoryAttri":"包裝",
            "opConfigUid": null,
            "opFlagAssign": null,
            "opListHeaderId":"113",
            "opListLineId": null,
            "opListSeq":"920",
            "opOptDesc": null,
            "opModifyFlag": null,
            "opShowAssDesc":"N",
            "opOptionList":[
               {
                  "opShowText":"1-奇美庫別:",
                  "opText":"奇美庫別:",
                  "opValue":"Y1053"
               },
               {
                  "opShowText":"2-不給指示",
                  "opText":"不給指示",
                  "opValue":"N1054"
               }
            ],
            "opSpInsName":"奇美併櫃庫別說明"
         }
      ]
   }
   }
}
```

## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考

* SisConfigManageAction.sisConfigEditPre