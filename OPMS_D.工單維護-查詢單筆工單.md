# 工單維護-查詢單筆工單
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

檢視或編輯查詢單筆工單

## 修改歷程

| 修改時間 | 內容                                                      | 修改者 |
| -------- | --------------------------------------------------------- | ------ |
| 20230801 | 新增規格                                                  | shawn  |
| 20230905 | 補回傳指圖&指示書的flag                                   | shawn  |
| 20230911 | 因應客戶需求 HeaderID=53 自動處理單位"只"(給前端不給"只") | Nick   |
| 20230919 | 調整 OPMS 不存在的工單，訂單出貨指示應該還是要取值        | Nick   |
| 20230926 | 查無資料，增加 SHOWASSDESC 邏輯                           | Nick   |
| 20230928 | LOT 規則修正，舊版移轉 Oracle 時 SQL 有問題，志誠提出     | Nick   |
| 20231024 | SQL 語法多一個 END                                        | Nick   |
| 20240104 | 調整原料量轉字串SQL                                       | Ellen  |


## 來源URL及資料格式

| 項目   | 說明            |
| ------ | --------------- |
| URL    | /wo/wo_view_pre |
| method | post            |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             | 名稱     | 資料型別 | 必填  | 資料儲存 & 說明 |
| ---------------- | -------- | :------: | :---: | --------------- |
| opWoNo           | 工單號碼 |  string  |   M   |                 |
| opBomNo          | 替代結構 |  string  |   O   | 編輯時為必填    |
| opItemNo         | 料號     |  string  |   O   | 編輯時為必填    |

#### Request 範例

```json
{
  "opWoNo":"51004637",
  "opBomNo":"765XXXXXX1MFD14102",
  "opItemNo":"02"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 新增變數{to}
* 新增變數{bomUid}
* 查詢sql-2，查詢結果放入{to}
* 查詢無資料：
  * 查詢sql-1，{bomUid}={BOM_UID}，如果 {BOM_UID} 為null 代表OPMS BOM未維護，則 [return 400,WO_NO_OPMS]，離開程式。 
  * 查詢sql-3，將查詢結果放入{to}
  * 抓取sap工單資料，sap-1，設定{to}各欄位值
  * {to}的isNew = "Y"
  * 新增變數{weight}，預設0
  * 判斷`{to}.weight不為空且不為"0","1"`：
    * {to}.unit = "KG"
    * {weight} = {to}.weight
    * 如weight大於0:
      * {to}.stdNQuantityPer = {stdNQuantityPer}/{weight} 四捨五入到小數5位
      * {to}.cusNQuantityPer = {cusNQuantityPer}/{weight} 四捨五入到小數5位
    * 反之
      *  {to}.stdNQuantityPer = "0"
      *  {to}.cusNQuantityPer = "0"
  * {opMtrList}取值： 參考edit-1 編輯頁的取值
  * {opPgmList}取值： 參考edit-1 編輯頁的取值
  * {opSisList}取值：查詢sql-6
      - 如果 {opAssDesc}不為空且opListHeaderId=53 且 {opAssDesc}最後一個字元為"只"，則去除掉最後一個字元"只" 
      - {opOptionList}取值，查詢sql-6-1返回結果
      - 自定義變數 {ShowAssDesc} 預設為 'N'，sql-6-1 結果中的 SHOWASSDESC 欄位有一筆為 'Y'，則opSisList的 {ShowAssDesc}覆寫為 'Y'
      - 如`{LIST_HEADER_ID}=159且{to}.woNo前兩碼為"58"`：
        參考委外工單產生預設LOT處理邏輯
  * {opWoSisList}取值：查詢sql-9   
  * 程式結束
* 有資料：
  * 如`{opBomNo}和{opItemNo}都有值`，代表為編輯：
    * 查詢sql-1，{bomUid}={BOM_UID}，如果 {BOM_UID} 為null 代表OPMS BOM未維護，則 [return 400,WO_NO_OPMS]，離開程式。 
    * 新增變數{to2}，查詢sql-3返回值
    * 以下設值：
      * {to2}.isNew = "N"
      * {to2}.woUid = {to}.woUid
      * {to2}.rawQty = {to}.rawQty
      * {to2}.batchQty = {to}.batchQty
      * {to2}.estimatedShipDate = {to}.estimatedShipDate
      * {to2}.woType = {to}.woType
      * {to2}.remark = {to}.remark
      * {to2}.prodRemark = {to}.prodRemark
      * {to2}.printFlag = {to}.printFlag
      * {to2}.printFlagD = {to}.printFlagD
      * {to} = {to2}
    * 如`{to}.weight不為空且不為"0","1"`：
      * {to}.unit = 'KG'
      * {weight} = {to}.weight
      * 如weight大於0:
        * {to}.stdNQuantityPer = {stdNQuantityPer}/{weight} 四捨五入到小數5位
        * {to}.cusNQuantityPer = {cusNQuantityPer}/{weight} 四捨五入到小數5位
      * 反之
        *  {to}.stdNQuantityPer = "0"
        *  {to}.cusNQuantityPer = "0"
    * {opMtrList}取值：參考edit-1 編輯頁的取值
	* {opPgmList}取值：參考edit-1 編輯頁的取值
	* {opSisList}取值：查詢sql-6
	    * 如 `{ASS_DESC}不為空` 且 `{LIST_HEADER_ID}=53 `且` {ASS_DESC}最後一個字元為"只"`，則{opAssDesc} = {ASS_DESC}去除掉最後一個字元"只"
    	* 如`({LIST_HEADER_ID}=159且{to}.woNo前兩碼為"58")`且`{assDesc}為空`且`{woDate}和{completionSubinv}都不為空`：
        參考委外工單產生預設LOT處理邏輯
    	* {opOptionList}取值，查詢sql-6-1返回結果
    	* 自定義變數 {ShowAssDesc} 預設為 'N'，sql-6-1 結果中的 SHOWASSDESC 欄位有一筆為 'Y'，則opSisList的 {ShowAssDesc}覆寫為 'Y'
	* {opWoSisList}取值：查詢sql-9   
       
  * 反之，代表為檢視做以下：
	* {opMtrList}取值：查詢sql-10 
    * {opPgmList}取值：查詢sql-11
    * 計算n4值：如`{to}.cusNQuantityPer為空`：
      * [計算n4公式](./OPMS_共用_N4計算公式.md)
      * {to}.stdNQuantityPer = n4回傳值
      * {to}.cusNQuantityPer = n4回傳值
    * {opSisList}取值：查詢sql-6
      * {opOptionList}取值，查詢sql-6-1返回結果
      * 自定義變數 {ShowAssDesc} 預設為 'N'，sql-6-1 結果中的 SHOWASSDESC 欄位有一筆為 'Y'，則opSisList的 {ShowAssDesc}覆寫為 'Y'
    * {opWoSisList}取值：查詢sql-12，並排除sql-6查詢出來的LIST_HEADER_ID
      
  * 抓取sap工單資料，sap-1，設定{to}各欄位值  
  
* 計算色料單位總重：
  * {opPgmList}有資料時計算加總：
    * 如{opUnit}='KG'，單位用量{opQuantityPer}要*1000
    * {opQuantityPer}再進行無條件捨去進行加總
  * {opTotalQuantityPer} = 加總值

* edit-1 編輯頁的取值
```
{opMtrList}取值：
	查詢sql-4，如有資料時做以下：
    迴圈執行
    如果 {weight}大於0：
    {quantityPer} = {quantityPer}/{weight} 四捨五入到小數5位
{opPgmList}取值：
    查詢sql-5，如有資料時做以下：
    迴圈執行
    如果{UNIT}等於"KG"
      {opUnit} = 'G'
      {quantityPer} = {quantityPer}*1000
    如果{weight}大於0
      {quantityPer} = {quantityPer}/{weight} 四捨五入到小數5位
```
* 委外工單產生預設LOT處理邏輯
```
* 先判斷是否用新規則，{to}.woDate如在2021/01/01之後為true
* 新增變數{lotPre:string}，查詢sql-7返回結果取第一筆
* 新增變數{LotSn:int}
* 查詢sql-8返回結果取第一筆{sn}
* 判斷{sn}第一字元是否為數字：
* 是：
	* 如果等於"9"，回傳{sn}轉為int+1+55
	* 反之，回傳{sn}轉為int+1
* 否：回傳{sn}第一字元轉成ASCII後+1
	* 新增變數{lotSn:string}
	* 如{LotSn}大於等於10，回傳{lotPre}+(char){LotSn}
	* 反之，回傳{lotPre}+{LotSn}
* 以下設值：
{opFlagAssign}="Y"
{opFlagOrderType}="W"
{opListLineId}="86"
{opOptDesc}="委外標籤LOT為："
{opAssDesc}={lotSn}
```
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容


# Request 後端邏輯說明

SAP-1：
可參考舊版 WoQueryRfc.getWoHSingle
程序如有任何異常，紀錄錯誤LOG並回傳NULL
查詢SAP-API回傳結果參數如下
|              | 型態   | 參數值                     | 說明 |
| ------------ | ------ | -------------------------- | ---- |
| BAPIFunction | string | Z_PP_OPMS_PRODUCTION_ORDER |      |

| 參數名稱 | 參數值             | 型態   | 說明 |
| -------- | ------------------ | ------ | ---- |
| I_WERKS  | {登入者工廠別} | string |      |
| I_AUFNR  | {opWoNo}           | string |      |

| 取得回傳表格名稱 |
| ---------------- |
| T_ORDER          |

| 物件元素名稱      | 資料型別 | SAP欄位名稱 | SAP資料型別 | 物件元素回傳值 | 說明                                                                 |
| ----------------- | -------- | ----------- | ----------- | -------------- | -------------------------------------------------------------------- |
| itemNo            | string   | MATNR       | string      | 同SAP          |                                                                      |
| color             | string   | COLOR       | string      | 同SAP          |                                                                      |
| prodUnitNo        | string   | STAND       | string      | 同SAP          | sap回傳值做replaceFirst("^0+(?!$)", "")，如果為""給'NA'              |
| produnitName      | string   | STAND_T     | string      | 同SAP          |                                                                      |
| woNo              | string   | AUFNR       | string      | 同SAP          | sap回傳值做replaceFirst("^0+(?!$)", "")                              |
| salesNo           | string   | ERNAM       | string      | 同SAP          |                 |
| custNo            | string   | KUNNR       | string      | 同SAP          |                                                                      |
| custName          | string   | SORT2       | string      | 同SAP          |                                                                      |
| indirCust         | string   | IHREZ       | string      | 同SAP          |                                                                      |
| gradeNo           | string   | GRADE       | string      | 同SAP          |                                                                      |
| wipClassNo        | string   | AUART       | string      | 同SAP          |                                                                      |
| wipClassCode      | string   | AUART_T     | string      | 同SAP          |                                                                      |
| itemDesc          | string   | MAKTX       | string      | 同SAP          |                                                                      |
| custPoNo          | string   | CUSTPO      | string      | 同SAP          |                                                                      |
| sapOperation      | string   | VORNR       | string      | 同SAP          | sap回傳值做replaceFirst("^0+(?!$)", "")                              |
| sapWc             | string   | ARBPL       | string      | 同SAP          |                                                                      |
| woQty             | string   | GAMNG       | double      | 同SAP          | sap回傳值四捨五入到小數5位                                           |
| rawQty            | string   |             |             |                | 如{to}.{rawQty}為空，取woQty                                         |
| unit              | string   | GMEIN       | string      | 同SAP          |                                                                      |
| activeFlag        | string   | STATUS      | string      | 同SAP          |                                                                      |
| woDate            | string   | GSTRP       | date        | 同SAP          | sap回傳值不為空則格式化yyyy/MM/dd                                    |
| equipName         | string   | ARBPL_T     | string      | 同SAP          |                                                                      |
| salesQty          | string   | SOQTY       | double      | 同SAP          | sap回傳值四捨五入到小數5位                                           |
| estimatedShipDate | string   | SHIP_DATE   | date        | 同SAP          | 如{to}的{estimatedShipDate}為空，且sap回傳值不為空則格式化yyyy/MM/dd |
| completionSubinv  | string   | LGORT       | string      | 同SAP          |                                                                      |
| salesOrderNo      | string   | KDAUF       | string      | 同SAP          | sap回傳值做replaceFirst("^0+(?!$)", "")                              |
| salesOrderSeq     | string   | KDPOS       | string      | 同SAP          | sap回傳值做replaceFirst("^0+(?!$)", "")                              |
| cmcSoType         | string   | CMC_SO_TYPE | string      | 同SAP          |                                                                      |
| sapOperation      | string   | VORNR       | string      | 同SAP          | sap回傳值做replaceFirst("^0+(?!$)", "")                              |
| sapWc             | string   | ARBPL       | string      | 同SAP          |






sql-1:

```sql
SELECT BOM_UID FROM BOM_MTM 
WHERE ITEM_NO={opItemNo} AND ORGANIZATION_ID={登入者工廠別} AND BOM_NO={opBomNo}

```

sql-2
```sql
SELECT
	H.WO_UID,
	H.ORGANIZATION_ID,
	case
		BATCH_QTY when cast(BATCH_QTY as int) then cast(CONVERT(int,	BATCH_QTY) as varchar(20))
		else cast(CONVERT(FLOAT, BATCH_QTY) as varchar(20))
	end BATCH_QTY,
	H.BATCH_TIMES,
	H.WO_QTY, 
	FORMAT(CONVERT(float, RAW_QTY), '0.##') RAW_QTY,
	case
		H.REMAIN_QTY when cast(H.REMAIN_QTY as int) then cast(CONVERT(int,H.REMAIN_QTY) as varchar(20))
		else cast(CONVERT(FLOAT,H.REMAIN_QTY) as varchar(20))
	end REMAIN_QTY,
	H.REMAIN_TIMES,
	H.REMARK,
	H.PROD_REMARK,
	H.BOM_UID,
	H.BOM_REMARK,
	H.LOT_NO,
	H.WO_SEQ,
	H.WO_TYPE,
	CONVERT(VARCHAR(10),H.ESTIMATED_SHIP_DATE,111) AS ESTIMATED_SHIP_DATE,
	case
		H.CUS_N_QUANTITY_PER when cast(H.CUS_N_QUANTITY_PER as int)
     then cast(CONVERT(int,	H.CUS_N_QUANTITY_PER) as varchar(20))
		else cast(CONVERT(FLOAT,
		H.CUS_N_QUANTITY_PER) as varchar(20))
	end CUS_N_QUANTITY_PER,
	H.STD_N_QUANTITY_PER,
	H.ALT_BOM_PART_NO,
	H.ALT_BOM_DESIGNATOR,
	H.ALT_ROUTING_PART_NO,
	H.ALT_ROUTING_DESIGNATOR,
	'N' AS IS_NEW,
	H.WEIGHT,
	REPLACE(CONVERT(VARCHAR(16), H.CDT, 120), '-', '/') AS CDT,
	H.CREATE_BY,
	REPLACE(CONVERT(VARCHAR(16), H.UDT, 120), '-', '/') AS UDT,
	H.UPDATE_BY,
	B.BOM_NO,
	B.BOM_NAME,
	B.BOM_DESC,
	I.PATTERN_NO,
	H.PRINT_FLAG,H.PRINT_FLAG_D
FROM WO_H H
INNER JOIN BOM_MTM B ON H.BOM_UID = B.BOM_UID
INNER JOIN ITEM_H I  ON	B.ITEM_NO = I.ITEM_NO
WHERE H.WO_NO = {opWoNo}
```
sql-3
```sql
SELECT
	B.BOM_UID,
	0 as RAW_QTY,
	I.WEIGHT,
	I.PATTERN_NO,
	B.ORGANIZATION_ID,
	B.BOM_UID,
	B.ITEM_NO,
	B.BOM_NO,
	B.BOM_NAME,
	B.BOM_DESC,
	B.REMARK AS BOM_REMARK,
	B.STD_N_QUANTITY_PER,
	B.CUS_N_QUANTITY_PER
FROM
	BOM_MTM B
INNER JOIN ITEM_H I ON
	B.ITEM_NO = I.ITEM_NO
WHERE
	B.BOM_UID ={bomUid}

```


sql-4
```sql
SELECT
	R.MTR_PART_NO AS WO_MTR_PART_NO,
	R.QUANTITY_PER,
	R.UNIT
FROM
	BOM_MTR R
WHERE
	R.BOM_UID ={bomUid}
ORDER BY
	R.QUANTITY_PER DESC,
	R.MTR_PART_NO
```
sql-5
```sql
SELECT PGM_PART_NO AS WO_PGM_PART_NO,
QUANTITY_PER,UNIT,
SITE_PICK,
SELF_PICK,
FEEDER_SEQ,
0 AS PGM_BATCH_QTY,
0 AS PGM_REMAIN_QTY
FROM BOM_PGM WHERE BOM_UID={bomUid}
ORDER BY PGM_PART_NO
```
sql-6
```sql
SELECT CAST
	( H.LIST_HEADER_ID AS VARCHAR ( 20 ) ) LIST_HEADER_ID,
	H.SP_INS_NAME,
	CAST ( H.LIST_SEQ AS VARCHAR ( 20 ) ) LIST_SEQ,
	H.SP_INS_NAME,
	H.CATEGORY_FLAG CATEGORY_ATTRI,
	ISNULL(W.FLAG_ORDER_TYPE, H.FLAG_ORDER_TYPE) FLAG_ORDER_TYPE,
	W.LIST_LINE_ID,
	W.OPT_DESC,
	W.ASS_DESC,
	W.FLAG_ASSIGN,
	W.SHOW_TEXT
FROM
	SYS_SIS_H H 
LEFT JOIN (
SELECT			
	S.LIST_HEADER_ID ,
	S.LIST_LINE_ID,
	S.OPT_DESC,
	S.ASS_DESC,
	S.FLAG_ASSIGN,
	S.FLAG_ORDER_TYPE,
	S.OPT_DESC + ISNULL(S.ASS_DESC,	'') AS SHOW_TEXT
FROM
	WO_H W
INNER JOIN WO_SIS S ON W.WO_UID = S.WO_UID
WHERE W.WO_UID ={opWoUid}
) W ON W.LIST_HEADER_ID = H.LIST_HEADER_ID
WHERE
	H.ORGANIZATION_ID={登入者工廠別}
	AND H.FLAG_ORDER_TYPE= 'W' 
	AND H.ACTIVE_FLAG= 'Y' 
	AND LIST_HEADER_ID != 112
ORDER BY
	H.LIST_SEQ
```

sql-6-1
```sql
SELECT 
CASE WHEN (FLAG_ASSIGN IS NULL OR FLAG_ASSIGN = '') 
	THEN 'N'  
	ELSE FLAG_ASSIGN   
END AS SHOWASSDESC, 
ISNULL(FLAG_ASSIGN,'N') + cast(LIST_LINE_ID as varchar(20)) [VALUE],
cast(ISNULL(LIST_SEQ,'') as varchar(20))+'-'+OPT_DESC SHOW_TEXT,
OPT_DESC TEXT
FROM SYS_SIS_L 
WHERE LIST_HEADER_ID={LIST_HEADER_ID}
ORDER BY LIST_SEQ
```

sql-7
```sql
SELECT 
--2021-01-01起新LOT規則
SUBSTRING({to.woDate},3,2)+ E.LOT_CODE +
CASE S2.PARA_VALUE WHEN 10 THEN 'A' WHEN 11 THEN 'B' WHEN 12 THEN 'C' ELSE CONVERT(VARCHAR, CAST(S2.PARA_VALUE as float)) END +
SUBSTRING(?,9,2) AS OUT_LOT_PRE 

--舊規則
SELECT SUBSTRING({to.woDate},4,1)+E.LOT_CODE +S2.PARA_CODE + SUBSTRING(?,9,2)  AS OUT_LOT_PRE 

FROM EQUIP_H E
INNER JOIN SYS_PARA_D S1 ON S1.PARA_CODE=E.EQUIP_ID 
INNER JOIN SYS_PARA_D S2 ON SUBSTRING({to.woDate},6,2)=S2.PARA_VALUE 
WHERE S1.PARA_VALUE={to.completionSubinv}
AND S1.PARA_ID='LOT_OUTSORCE'
```

sql-8
```sql
SELECT ISNULL (MAX(SUBSTRING(LOT_NO, LEN(LOT_NO) ,1)),0) AS SN 
FROM PS_L WHERE LOT_NO LIKE '{lotPre}%'
```

sql-9
```sql
SELECT S.SO_NO,S.SO_SEQ,S.LIST_HEADER_ID,H.FLAG_ORDER_TYPE,
H.CATEGORY_FLAG CATEGORY_ATTRI,H.SP_INS_NAME,H.LIST_SEQ,
S.LIST_LINE_ID,S.OPT_DESC,S.ASS_DESC,L.FLAG_ASSIGN, 
S.OPT_DESC+ISNULL(S.ASS_DESC,'') AS SHOW_TEXT 
FROM SO_SIS S 
INNER JOIN SYS_SIS_H H ON S.LIST_HEADER_ID=H.LIST_HEADER_ID
INNER JOIN SYS_SIS_L L ON S.LIST_HEADER_ID=L.LIST_HEADER_ID AND S.LIST_LINE_ID=L.LIST_LINE_ID
WHERE S.OPT_DESC IS NOT NULL 
AND S.SO_NO={to.salesOrderNo} 
AND (S.SO_SEQ={to.salesOrderSeq} OR S.SO_SEQ IS NULL) 
ORDER BY H.CATEGORY_FLAG,H.LIST_SEQ
```

sql-10
```sql
SELECT
	R.WO_MTR_PART_NO,
	case
		R.QUANTITY_PER when cast(R.QUANTITY_PER as int) then cast(CONVERT(int,
		R.QUANTITY_PER) as varchar(20))
		else cast(CONVERT(FLOAT,
		R.QUANTITY_PER) as varchar(20))
	end QUANTITY_PER,
	R.UNIT
FROM
	WO_MTR R
WHERE
	R.WO_MTR_PART_NO NOT LIKE '92%'
	AND R.WO_UID ={to.woUid}
ORDER BY
	R.QUANTITY_PER DESC,
	R.WO_MTR_PART_NO

```

sql-11
```sql
SELECT
	WO_PGM_PART_NO,
	case
		QUANTITY_PER when cast(QUANTITY_PER as int) then cast(CONVERT(int,
		QUANTITY_PER) as varchar(20))
		else cast(CONVERT(FLOAT,
		QUANTITY_PER) as varchar(20))
	end QUANTITY_PER,
	UNIT,
	SITE_PICK,
	SELF_PICK,
	case
		PGM_BATCH_QTY when cast(PGM_BATCH_QTY as int) then cast(CONVERT(int,
		PGM_BATCH_QTY) as varchar(20))
		else cast(CONVERT(FLOAT,
		PGM_BATCH_QTY) as varchar(20))
	end PGM_BATCH_QTY,
	case
		PGM_REMAIN_QTY when cast(PGM_REMAIN_QTY as int) then cast(CONVERT(int,
		PGM_REMAIN_QTY) as varchar(20))
		else cast(CONVERT(FLOAT,
		PGM_REMAIN_QTY) as varchar(20))
	end PGM_REMAIN_QTY,
	MODIFY_QTY,
	MODIFY_UNIT_QTY,
	FEEDER_SEQ
FROM
	WO_PGM
WHERE
	WO_UID = {to.woUid}
ORDER BY
	WO_PGM_PART_NO
```

sql-12
```sql
SELECT
	S.LIST_HEADER_ID,
	S.CATEGORY_ATTRI,
	S.SP_INS_NAME,
	S.LIST_SEQ,
	S.LIST_LINE_ID,
	S.OPT_DESC,
	S.ASS_DESC,
	S.FLAG_ASSIGN,
	S.FLAG_ORDER_TYPE,
	S.OPT_DESC + ISNULL(S.ASS_DESC,	'') AS SHOW_TEXT
FROM
	WO_H W
INNER JOIN WO_SIS S ON W.WO_UID = S.WO_UID
WHERE
	W.WO_UID ={to.woUid} 
	AND LIST_HEADER_ID NOT IN {sql-6.LIST_HEADER_ID}
ORDER BY
	S.LIST_SEQ
```

# Response 欄位
| 欄位                   | 名稱             | 資料型別 | 資料儲存 & 說明                  |
| ---------------------- | ---------------- | -------- | -------------------------------- |
| opIsNew                | 是否新增         | string   | IS_NEW(sql-2)                    |
| opWoUid                | uid              | string   | WO_UID(sql-2)                    |
| opLotNo                | 流水號           | string   | LOT_NO(sql-2)                    |
| opPatternNo            | 色號             | string   | PATTERN_NO(sql-2)                |
| opItemNo               | 料號             | string   | itemNo(sap-1)                    |
| opColor                | 顏色             | string   | color(sap-1)                     |
| opItemDesc             | 料號摘要         | string   | itemDesc(sap-1)                  |
| opBomUid               | uid              | string   | BOM_UID(sql-2)                   |
| opBomNo                | 替代BOM          | string   | BOM_NO+"/"+BOM_NAME+"/"+BOM_DESC |
| opCmdiName             | 品稱             | string   | 預設null                         |
| opBomRemark            | BOM備註          | string   | BOM_REMARK(sql-2)                |
| opWoDate               | 使用日期         | string   | woDate(sap-1)                    |
| opWoSeq                | 序號             | string   | WO_SEQ(sql-2)                    |
| opWipClassCode         | 工單類別         | string   | wipClassCode(sap-1)              |
| opWipClassNo           | no               | string   | wipClassNo(sap-1)                |
| opWoNo                 | 工單號碼         | string   | woNo.(sap-1)                     |
| opSalesOrderNo         | 銷售訂單單號     | string   | salesOrderNo(sap-1)              |
| opSalesOrderSeq        | 銷售訂單明細行號 | string   | salesOrderSeq(sap-1)             |
| opWoQty                | 工單數量         | string   | woQty(sap-1)                     |
| opSalesQty             | 銷售數量         | string   | salesQty(sap-1)                  |
| opProdunitNo           | 生產單位代碼     | string   | produnitNo(sap-1)                |
| opProdunitName         | 生產單位名稱     | string   | produnitName(sap-1)              |
| opSalesNo              | 業助代號         | string   | salesNo(sap-1)                   |
| opEquipName            | 指定機台         | string   | equipName(sap-1)                 |
| opCustPoNo             | 客戶PO#          | string   | custPoNo(sap-1)                  |
| opGradeNo              | Grade No         | string   | gradeNo(sap-1)                   |
| opCustNo               | 客戶代號         | string   | custNo(sap-1)                    |
| opCustName             | 客戶名稱         | string   | custName(sap-1)                  |
| opCompletionSubinv     | 完工倉別         | string   | completionSubinv(sap-1)          |
| opEstimatedShipDate    | 預計出貨日       | string   | estumatedShipDate(sap-1)         |
| opTotalQuantityPer     | 色料單位總重     | string   | 計算加總值                       |
| opCusNQuantityPer      | N-4單位用量      | string   | N4回傳值                         |
| opStdNQuantityPer      | N-4單位用量-系統 | string   | N4回傳值                         |
| opRawQty               | 投入原料量       | string   | rawQty(sap-1)                    |
| opBatchQty             | 每Batch用量      | string   | BATCH_QTY(sql-2)                 |
| opBatchTimes           | Batch用量次數    | string   | BATCH_TIMES(sql-2)               |
| opRemainQty            | 尾數量           | string   | REMAIN_QTY(sql-2)                |
| opRemainTimes          | 尾數量次數       | string   | REMAIN_TIMES(sql-2)              |
| opRemark               | 指圖表備註       | string   | REMARK(sql-2)                    |
| opProdRemark           | 生產指示備註     | string   | PROD_REMARK(sql-2)               |
| opPrintFlag           | 指圖     | string   | PRINT_FLAG(sql-2)               |
| opPrintFlagD           | 指示書     | string   | PRINT_FLAG_D(sql-2)               |
| opMtrList              | 原料             | array    |                                  |
| opPgmList              | 色料             | array    |                                  |
| opSisList              | 工單-出貨指示    | array    |
| opWoSisList            | 訂單-出貨指示    | array    |                                  |
| opSapOperation         | 儲存用           | string   | sapOperation(sap-1)              |
| opSapWc                | 儲存用           | string   | sapWc(sap-1)                     |  |
| opAltBomPartNo         | 儲存用           | string   | ALT_BOM_PART_NO(sql-2)           |  |
| opWeight               | 儲存用           | string   | WEIGHT(sql-2)                    |
| opActiveFlag           | 儲存用           | string   | activeFlag(sap-1)                |
| opIndirCust            | 儲存用           | string   | indirCust(sap-1)                 |
| opWoType               | 儲存用           | string   | WO_TYPE(sql-2)                   |
| opAltBomDesignator     | 儲存用           | string   | ALT_BOM_DESIGNATOR(sql-2)        |
| opAltRoutingPartNo     | 儲存用           | string   | ALT_ROUTING_PART_NO(sql-2)       |
| opAltRoutingDesignator | 儲存用           | string   | ALT_ROUTING_DESIGNATOR(sql-2)    |



### 【opMtrList】array
| 欄位          | 名稱 | 資料型別 | 資料儲存 & 說明 |
| ------------- | ---- | -------- | --------------- |
| opWoMtrPartNo | 料號 | string   | WO_MTR_PART_NO  |
| opQuantityPer | 用量 | string   | QUANTITY_PER    |
| opUnit        | 單位 | string   | UNIT            |

### 【opPgmList】array
| 欄位           | 名稱      | 資料型別 | 資料儲存 & 說明 |
| -------------- | --------- | -------- | --------------- |
| opSelfPick     | 秤料      | string   | SELF_PICK       |
| opWoPgmPartNo  | 色料      | string   | WO_PGM_PART_NO  |
| opQuantityPer  | 單位用量  | string   | QUANTITY_PER    |
| opUnit         | 單位      | string   | UNIT            |
| opPgmBatchQty  | batch用量 | string   | PGM_BATCH_QTY   |
| opPgmRemainQty | 尾數用量  | string   | PGM_REMAIN_QTY  |
| opFeederSeq    | 投料順序  | string   | FEEDER_SEQ      |
| opSitePick     | 現場領料  | string   | SITE_PICK       |

### 【opSisList】array
| 欄位            | 名稱                 | 資料型別 | 資料儲存 & 說明  |
| --------------- | -------------------- | -------- | ---------------- |
| opListHeaderId  | 出貨指示項目ID       | string   | LIST_HEADER_ID   |
| opListLineId    | 出貨指示選項ID       | string   | LIST_LINE_ID     |
| opListSeq       | 順序                 | string   | LIST_SEQ_NEW     |
| opSpInsName     | 出貨指示名稱         | string   | SP_INS_NAME      |
| opCategoryAttri | 類別                 | string   | CATEGORY_ATTRI   |
| opAssDesc       | 指定內容             | string   | ASS_DESC         |
| opOptDesc       | 選項內容             | string   | OPT_DESC         |
| opFlagAssign    | 是否指定內容         | string   | FLAG_ASSIGN      |
| opFlagOrderType | FlagOrderType        | string   | FLAG_ORDER_TYPE  |
| opShowAssDesc   | 是否顯示指定內容元件 | string   | SHOWASSDESC      |
| opOptionList    |                      | array    | 編輯用的下拉選項 |

### 【opOptionList】array 
| 欄位       | 名稱     | 資料型別 | 資料儲存 & 說明 |
| ---------- | -------- | -------- | --------------- |
| opValue    | 值       | string   | VALUE           |
| opText     | 選項內容 | string   | TEXT            |
| opShowText | 顯示內容 | string   | SHOW_TEXT       |

### 【opWoSisList】array 
| 欄位            | 名稱           | 資料型別 | 資料儲存 & 說明 |
| --------------- | -------------- | -------- | --------------- |
| opListHeaderId  | 出貨指示項目ID | string   | LIST_HEADER_ID  |
| opListLineId    | 出貨指示選項ID | string   | LIST_LINE_ID    |
| opListSeq       | 順序           | string   | LIST_SEQ_NEW    |
| opSpInsName     | 出貨指示名稱   | string   | SP_INS_NAME     |
| opCategoryAttri | 類別           | string   | CATEGORY_ATTRI  |
| opAssDesc       | 指定內容       | string   | ASS_DESC        |
| opOptDesc       | 選項內容       | string   | OPT_DESC        |
| opFlagAssign    | 是否指定內容   | string   | FLAG_ASSIGN     |
| opFlagOrderType | FlagOrderType  | string   | FLAG_ORDER_TYPE |
| opShowText      | 顯示內容       | string   | SHOW_TEXT       |



#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":{
		"opWoUid":"004ED5F2-2623-4DD7-A087-D6E5CC908C12",            
		"opLotNo":"2995",          
		"opPatternNo":"D14102B6",
		"opItemNo":"765XXXXXX1MFD14102",           
		"opColor":"IVORY",            
		"opItemDesc":"(停用)PA-765,MF 900KG太空包/松木棧板 D14102B6",         
		"opBomUid":"F40C6642-0EC1-4E94-8CB3-53A04B1F3292",           
		"opBomNo":"02/ /SA#4",            
		"opCmdiName":null,         
		"opBomRemark":"(鄭)Y-575",        
		"opWoDate":"2016/04/11",           
		"opWoSeq":"44",            
		"opWipClassCode":"染色工單(一般生產)",     
		"opWipClassNo":"Z510",       
		"opWoNo":"51004637",             
		"opSalesOrderNo":"21485505",     
		"opSalesOrderSeq":"160",    
		"opWoQty":"9102.1",            
		"opSalesQty":"9000",         
		"opProdunitNo":"211010",       
		"opProdunitName":"染色課",     
		"opSalesNo":"00070672",          
		"opEquipName":"染色課#4線",        
		"opCustPoNo":"2000055494",         
		"opGradeNo":"PA-765",          
		"opCustNo":"1028028",           
		"opCustName":"奇美實業",         
		"opCompletionSubinv":"FZ10", 
		"opEstimatedShipDate":"2016/04/19",
		"opTotalQuantityPer":"13", 
		"opCusNQuantityPer":"0.7",  
		"opStdNQuantityPer":"0",  
		"opRawQty":"8975",           
		"opBatchQty":"600",         
		"opBatchTimes":"14",       
		"opRemainQty":"575", 
		"opRemainTimes":"1",       
		"opRemark":null,           
		"opProdRemark":null,   
		"opPrintFlag":"Y",
		"opPrintFlagD":"Y",      
		"opMtrList":[{
			"opWoMtrPartNo":"04-102-PA-765XXXXX",
			"opQuantityPer":"1",
			"opUnit":"KG"
		}],          
		"opPgmList":[{
			"opSelfPick":"N",    
			"opWoPgmPartNo":"05-201-C-81XXXXXXX", 
			"opQuantityPer":"0.0154", 
			"opUnit":"G",        
			"opPgmBatchQty":"9.24", 
			"opPgmRemainQty":"8.855",
			"opFeederSeq":"1",   
			"opSitePick":null    
		},...]          
		"opSisList":[
		{
			"opListHeaderId":"159",
			"opListLineId":null,   
			"opListSeq":"309",      
			"opSpInsName":"標籤LOT",    
			"opCategoryAttri":"包裝",
			"opAssDesc":null,      
			"opOptDesc":null,      
			"opFlagAssign":null,   
			"opFlagOrderType":"W",
			"opShowAssDesc":"Y",  
			"opOptionList":[
				{
					"opValue":"Y86",   
					"opText":"委外標籤奇菱LOT為：",    
					"opShowText":"1-委外標籤奇菱LOT為："
				}
			]   
		},
		{
			"opListHeaderId":"53",
			"opListLineId":"84",   
			"opListSeq":"310",      
			"opSpInsName":"外袋領用量",    
			"opCategoryAttri":"包裝",
			"opAssDesc":"10只",      
			"opOptDesc":"外袋領用量為: ",      
			"opFlagAssign":"Y",   
			"opFlagOrderType":"W",
			"opShowAssDesc":"Y",  
			"opOptionList":[
				{
					"opValue":"Y84",   
					"opText":"外袋領用量為: ",    
					"opShowText":"1-外袋領用量為: "
				}
			]  
		}
		],          
		"opWoSisList":[{
			"opListHeaderId":"48", 
			"opListLineId":"75",   
			"opListSeq":"115",      
			"opSpInsName":"原料Lot需求",    
			"opCategoryAttri":"生產",
			"opAssDesc":null,      
			"opOptDesc":"原料無指定Lot",      
			"opFlagAssign":"N",   
			"opFlagOrderType":"H",
			"opShowText":"原料無指定Lot"     
		},...],
		"opSapOperation":null,
		"opSapWc":null,       
		"opAltBomPartNo":null,
		"opWeight":"1",      
		"opActiveFlag":null,  
		"opIndirCust":null,  
		"opWoType":null      		      
	}
  }
}
```		
## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考

* WoManageAction.woEditPre