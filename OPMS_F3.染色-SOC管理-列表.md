# 染色-SOC管理-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)


**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容                                     | 修改者 |
|----------|----------------------------------------|--------|
| 20230828 | 新增規格                                 | 黃東俞 |
| 20231024 | 調整欄位中文名稱( opAMtrDesc、opSocDesc ) | Nick   |
| 20231115 | 新增【更新日期】也可排序                   | Nick   |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /soc/soc_c_list             |
| method | post                        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱        |  資料型別 | 必填 | 資料儲存 & 說明                                                     |
|--------------|-------------|----------|:----:|--------------------------------------------------------------------|
| opItemNo     | 料號        | string   |  O    |                                                                    |
| opProdunitNo | 生產單位    | string   |  O    | 參考下方說明                                                        |
| opEquipId    | 機台        | integer  |  O    | 參考下方說明                                                        |
| opBDate      | 更新日期開始 | string   |  O    | yyyy/mm/dd                                                         |
| opEDate      | 結束日期結束 | string   |  O    | yyyy/mm/dd                                                         |
| opStatus     | 狀態        | string   |  O    | 共用參數 opType='SOC',opParamId='STATUS'                            |
| opPage       | 第幾頁      | integer  |  O    | 預設 1，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推 |
| opPageSize   | 分頁筆數    | integer  |  O    | 預設 10                                                             |
| opOrderField | 排序欄位名稱 | string   |   O   | 預設 "UDT"， 可排序的參數 ， UDT:更新日期 / PUB_DATE:發行日          |
| opOrder      | 排序方式    | string   |  O    | "ASC":升冪排序 / "DESC":降冪排序                                     |

* 前端生產單位選項(opProdunitNo), 由API [OPMS_F3.SOC-生產單位-列表  /soc/produnit_list](OPMS_F3.SOC-生產單位-列表.md) 取得，opOrganizationId 傳入登入者工場別，opProdType 傳入 'C'
* 前端機台選項(opEquipId)，由API [C.BOM維護-取得排程機台列表(by工廠別.生產單位.生產型態) /bom/get_ps_equipment_list](./OPMS_C.BOM維護-取得排程機台列表(by工廠別.生產單位.生產型態).md) 取得，只傳入 opProdunitNo一個欄位(可傳入null查詢全部機台)，來源為上一步由[OPMS_F.製造-生產單位-列表  /soc/produnit_list](OPMS_F3.SOC-生產單位-列表.md) 取得的 opProdunitNo

```json
{
  "opItemNo": null,
  "opProdunitNo": null,
  "opEquipId": null,
  "opBDate": "2019/01/01",
  "opEDate": "2019/12/31",
  "opStatus": null,
  "opPage": 1,
  "opPage": 10
}
```

# Request 後端流程說明

* 取 { 登入者工廠別 } SYS_USER.ORGANIZATION_ID 欄位
* 參考SQL-1，欄位皆為非必填，有傳入的欄位才需加入SQL篩選
* 只最多取前1000筆資料並進行分頁再回傳資料
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

SQL-1:
```sql
-- SocCQueryDao.getSocCResultList
SELECT S.SOC_C_UID SOC_UID, S.SOC_C_VER SOC_VER, S.EQUIP_ID, E.EQUIP_NAME, S.ITEM_NO, S.STATUS, S.REMARK, S.SOC_DESC, S.A_MTR_DESC,
S.PUB_DATE, S.PUBLIC_BY, S.UDT, S.UPDATE_BY, U.USER_NAME PUBLIC_NAME
FROM SOC_C S
INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
LEFT JOIN SYS_USER U ON S.PUBLIC_BY = U.USER_ID
WHERE E.ORGANIZATION_ID = { 登入者工廠別 }
AND S.UDT >= { opBDate }
AND S.UDT <= { opEDate }
AND E.PRODUNIT_NO = { opProdunitNo }
AND S.EQUIP_ID = { opEquipId }
AND S.ITEM_NO like { %opItemNo% }
AND S.STATUS = { opStatus }

-- 如 {opOrderField}="UDT" OR null => S.UDT
-- 如 {opOrderField}="PUB_DATE"    => S.PUB_DATE
ORDER BY {opOrderField} {opOrder}
```

# Response 欄位
| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
| -------- | -------- | -------- | --------------- |
| pageInfo | 分頁資訊 | object   |                 |
| content  | 附帶訊息 | array    |                 |
#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明 |
| ---------- | ------------ | -------- | --------------- |
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |

#### 【content】array
| 欄位        | 名稱         | 資料型別 | 資料儲存 & 說明    |
|-------------|-------------|---------|--------------------|
| opSocUid    | ID          | string  | SOC_C.SOC_C_UID    |
| opStatus    | 狀態        | string  | SOC_C.STATUS       |
| opEquipId   | 機台        | string  | SOC_C.EQUIP_ID     |
| opEquipName | 機台名稱     | string  | EQUIP_H.EQUIP_NAME |
| opAMtrDesc  | MTR DESC    | string  | SOC_C.A_MTR_DESC   |
| opItemNo    | 料號        | string  | SOC_C.ITEM_NO      |
| opSocVer    | 版號        | string  | SOC_C.SOC_C_VER    |
| opSocDesc   | 說明         | string  | SOC_C.SOC_DESC     |
| opPubDate   | 發行日期     | string  | SOC_C.PUB_DATE  yyyy/mm/dd |
| opPublicBy  | 發行人代碼   | string  | SOC_C.PUBLIC_BY    |
| opPublicName| 發行人名稱   | string  | SYS_USER.USER_NAME |
| opUdt       | 更新日期     | string  | SOC_C.UDT  yyyy/mm/dd hh:mm:ss |
| opUpdateBy  | 更新人員     | string  | SOC_C.UPDATE_BY    |
| opRemark    | 備註        | string  | SOC_C.UDT          |

```json
{
  "msgCode":null,
  "result":{
    "pageInfo": {
      "pageNumber": 1,
      "pageSize": 10,
      "pages": 1,
      "total": 3
    },
    "content": [
      {
        "opSocUid":"5000-2013821R00315",
        "opStatus":"D",
        "opEquipId":"2",
        "opEquipName":"染色#2",
        "opAMtrDesc":"原料",
        "opItemNo":"06-198-PC110D01XXX",
        "opSocVer":"4",
        "opSocDesc":"NA",
        "opPubDate":"2013/08/26",
        "opPublicBy":"A000346",
        "opPublicName":"陳進丁",
        "opUdt":"2019/11/14 10:50:36",
        "opUpdateBy":"91163",
        "opRemark":null
      },
      {
        "opSocUid":"9745F4FAFDEAEC9CE050007F01002131",
        "opStatus":"Y",
        "opEquipId":"2",
        "opEquipName":"染色#2",
        "opAMtrDesc":"原料",
        "opItemNo":"06-198-PC110D01XXX",
        "opSocVer":"5",
        "opSocDesc":"NA",
        "opPubDate":"2019/11/14",
        "opPublicBy":"A000346",
        "opPublicName":"陳進丁",
        "opUdt":"2019/11/14 10:43:52",
        "opUpdateBy":"A000346",
        "opRemark":"押出機溫控改水冷,系統大批修改為10段加溫"
      },
      {
        "opSocUid":"5000-DB109C05-E02A-44E5-8D57-865CCA908EA6",
        "opStatus":"D",
        "opEquipId":"2",
        "opEquipName":"染色#2",
        "opAMtrDesc":null,
        "opItemNo":"06-199-IR220001XXX",
        "opSocVer":"4",
        "opSocDesc":"NA",
        "opPubDate":"2014/04/23",
        "opPublicBy":"A000346",
        "opPublicName":"陳進丁",
        "opUdt":"2019/11/14 10:50:36",
        "opUpdateBy":"91163",
        "opRemark":null
      },
    ]
  }
}
```











