# 報表-配色作業日報表-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230818 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                      |
|--------|--------------------------|
| URL    | /report/daily_report_list|
| method | post                     |


## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱    |  資料型別 | 必填 | 資料儲存 & 說明  |
|--------------|---------|----------|:----:|-----------------|
| opBDate      | 開始日期 | string   |  O   | yyyy/mm/dd      |
| opEDate      | 結束日期 | string   |  O   | yyyy/mm/dd      |

#### Request 範例

```json
{
  "opBDate": "2019/01/01",
  "opEDate": "2019/01/31"
}
```

# Request 後端流程說明

* 取{ 登入者工廠別 } SYS_USER.ORGANIZATION_ID 欄位
* 參考SQL-1撈取資料
  * opBDate, opEDate 為非必填，有傳入時才輸入條件
* opRawQty(原料投入量)和opBatchQty(每Batch用量)取小數點兩位並轉為千分位格式。
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
-- ReportQueryDao.getDailyReport
SELECT H.WO_DATE, H.WO_SEQ, H.LOT_NO, I.PATTERN_NO, H.WO_NO, H.CUST_NAME, H.RAW_QTY, H.BATCH_QTY,
    H.BATCH_TIMES, B.CMDI_NO, B.CMDI_NAME, PRODUNIT.PRODUNIT_NAME, H.UNIT,H.REMARK
FROM BOM_MTM B
INNER JOIN WO_H H ON H.BOM_UID = B.BOM_UID
INNER JOIN ITEM_H I ON B.ITEM_NO = I.ITEM_NO
LEFT JOIN PRODUNIT ON H.PRODUNIT_NO = PRODUNIT.SAP_PRODUNIT AND H.ORGANIZATION_ID = PRODUNIT.ORGANIZATION_ID
WHERE H.ORGANIZATION_ID = { 登入者工廠別 }
AND H.WO_DATE >= { opBDate }
AND H.WO_DATE <= { opEDate }
ORDER BY H.WO_DATE,H.WO_SEQ
```


# Response 欄位
| 欄位           | 名稱         | 資料型別 | 資料儲存 & 說明                         |
|----------------|-------------|---------|----------------------------------------|
| opWoDate       | 日期        | string  | WO_H.WO_DATE  yyyy/mm/dd                |
| opWoSeq        | 序號        | string  | WO_H.WO_SEQ                             |
| opLotNo        | Lot No      | string  | WO_H.LOT_NO                             |
| opPatternNo    | 色號        | string  | ITEM_H.PATTERN_NO                       |
| opCustName     | 客戶        | string  | WO_H.CUST_NAME                          |
| opRawQty       | 原料投入量   | string  | WO_H.RAW_QTY  取小數點兩位，轉千分位格式  |
| opWoNo         | 工單號碼    | string  | WO_H.WO_NO                               |
| opBatchQty     | 每Batch用量 | string  | WO_H.BATCH_QTY 取小數點兩位，轉千分位格式  |
| opBatchTimes   | 次數        | string  | WO_H.BATCH_TIMES                         |
| opCmdiNo       | 品稱代號    | string  | BOM_MTM.CMDI_NO                          |
| opProdunitName | 生產單位    | string  | PRODUNIT.PRODUNIT_NAME                   |
| opRemark       | 備註        | string  | WO_H.REMARK                              |


#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content": [
      {
        "opWoDate": "2023/01/01",
        "opWoSeq": "1",
        "opLotNo": "1",
        "opPatternNo": "SH-F05W3",
        "opCustName": "",
        "opRawQty": "2,777.00",
        "opWoNo": "58040137",
        "opBatchQty": "2,777.00",
        "opBatchTimes": "1",
        "opCmdiNo": "",
        "opProdunitName": "良輝",
        "opRemark": ""
      },
      {
        "opWoDate": "2023/01/01",
        "opWoSeq": "2",
        "opLotNo": "2",
        "opPatternNo": "SHFW3",
        "opCustName": "",
        "opRawQty": "3,603.00",
        "opWoNo": "58040138",
        "opBatchQty": "3,603.00",
        "opBatchTimes": "1",
        "opCmdiNo": "",
        "opProdunitName": "良輝",
        "opRemark": ""
      },
      {
        "opWoDate": "2023/01/01",
        "opWoSeq": "3",
        "opLotNo": "3",
        "opPatternNo": "PSA",
        "opCustName": "",
        "opRawQty": "522.00",
        "opWoNo": "58040140",
        "opBatchQty": "522.00",
        "opBatchTimes": "1",
        "opCmdiNo": "",
        "opProdunitName": "良輝",
        "opRemark": ""
      }
    ]
  }
}
```
