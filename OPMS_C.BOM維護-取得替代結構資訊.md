# BOM維護-取得替代結構資訊
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得替代結構資訊

| 修改時間 | 內容                 | 修改者 |
| -------- | -------------------- | ------ |
| 20230525 | 新增規格             | Tim    |
| 20230706 | 調整規格             | Tim    |
| 20230710 | 調整規格             | Tim    |
| 20231026 | 增加opActiveFlag參數 | Ellen  |

## 來源URL及資料格式

| 項目   | 說明               |
| ------ | ------------------ |
| URL    | /bom/get_bomno_lov |
| method | post               |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在  
| 欄位             | 名稱     | 資料型別 | 必填  | 來源資料 & 說明                                  |
| ---------------- | -------- | -------- | :---: | ------------------------------------------------ |
| opItemNo         | 料號     | string   |   M   |                                                  |
| opOrganizationId | 工廠別   | string   |   M   | 預設 null ， null:不指定 / 5000:奇菱 / 6000:菱翔 |
| opAction         | 行為模式 | string   |   M   | N:新增，E:編輯                                   |
| opActiveFlag     | 是否生效 | string   |   O   | Y/N  Y: 是 / N: 否  預設 `null`                  |



#### Request 範例

```json
{
  "opItemNo":"H16548MM",
  "opOrganizationId":"5000",
  "opAction":"N"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 如{opAction}為新增時，查詢所有的替代結構 參考 SQL-2 然後使用前端給的條件 參考 SQL-1 查回曾建檔過的替代結構，然後進行判斷，將未建檔過的替代結構回傳結果
* 如{opAction}為編輯時，查詢曾建檔過的替代結構 參考 SQL-1 並回傳
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
SELECT BOM_UID,BOM_NO,BOM_NAME FROM BOM_MTM 
WHERE 
    ORGANIZATION_ID = @opOrganizationId 
    AND ITEM_NO = {opItemNo}

    --如{opActiveFlag}非 null 則加入以下SQL
    AND ACTIVE_FLAG={opActiveFlag}
ORDER BY BOM_NO
```

SQL-2:

```sql
SELECT OPT_VALUE FROM OPMS_PARAMS 
WHERE TYPE = 'BOM' AND PARAM_ID = 'NOLIST'
```


# Response 欄位
| 欄位    | 名稱 | 資料型別 | 資料儲存 & 說明 |
| ------- | ---- | -------- | --------------- |
| content | 內容 | array    |                 |



#### 【content】array
| 欄位      | 名稱   | 資料型別 | 來源資料 & 說明      |
| --------- | ------ | -------- | -------------------- |
| opBomUid  | BOM ID | string   | BOM_UID / null       |
| opBomNo   | 品號   | string   | BOM_NO / OPT_VALUE   |
| opBomName | 品稱   | string   | BOM_NAME / OPT_VALUE |




#### Response 範例

```json
{
    "msgCode": null,
    "result": {
        "content": [
            {
                "opBomUid": null,
                "opBomNo": "01",
                "opBomName": "Bom01",
            },
            {
                "opBomUid": null,
                "opBomNo": "02",
                "opBomName": "Bom02",
            }
        ]
    }
}
```

