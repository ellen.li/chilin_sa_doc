# 工單維護-排程-未結工單跨月工時拋轉SAP
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 排程功能說明:

每月1號將未結工單跨月工時拋轉SAP

## 排程修改歷程:

| 修改時間 | 內容                | 修改者 |
| -------- | ------------------- | ------ |
| 20230721 | 新增規格            | Ellen  |
| 20231212 | 修正sap回傳參數類型 | Ellen  |

## 排程時間
 
| 幾秒<br/>(0-59) | 幾分<br/>(0-59) | 幾時<br/>(0-23) | 哪一天<br/>(1 - 31) | 幾月<br/>(1 - 12) | 每週哪天 <br/>(0-7，0或7為週日) |
| --------------- | --------------- | --------------- | ------------------- | ----------------- | ------------------------------- |
| 0               | 0               | 3,10            | 1                   | *                 | ?                               |

## 排程邏輯
可參考舊版 ImportWoHoursToSap
* 執行 SQL-1，若{OPT_VALUE}等於Y才需執行排程，否則結束程式
* 參考 SQL-2 查詢未完工單之報工紀錄
* 參考 SAP-1 將工時拋轉SAP
  * 工作中心SAP_WC為"T3N"時，作業1工時X2
  * 工作中心SAP_WC為"T11"、"T11"、"T2N"時只染不押，只有兩個作業時間
* 遍歷SAP 回傳表格，執行 SQL-LOG 紀錄 LOG

SQL-1: 檢查排程開關
```sql
SELECT OPT_VALUE FROM OPMS_PARAMS 
WHERE TYPE = 'CRON' AND PARAM_ID = 'IMPORT_WO_HOURS'
```

SQL-2: 查詢未完工單之報工紀錄
```sql
SELECT
  W.WO_NO,
  R.PSR_UID,
  R.EQUIP_ID,
  E.EQUIP_NAME,
  CONVERT ( VARCHAR ( 10 ), R.REP_MFG_DATE, 111 ) REP_MFG_DATE,
  R.REP_SHIFT_CODE,
  E.SAP_WC,
  W.SAP_OPERATION,
  R.WO_UID,
  R.REP_SHIFT_HOURS,
  R.REP_TIME_LINE,
  R.REP_MIN_MFG_QTY,
  R.SHIFT_MFG_QTY,
  R.SHIFT_PRE_QTY,
  R.REP_CTN_DESC,
  R.WRN_FLAG,
  R.WRN_REMARK 
FROM
  PS_R R
  INNER JOIN WO_H W ON R.WO_UID= W.WO_UID
  INNER JOIN EQUIP_H E ON R.EQUIP_ID= E.EQUIP_ID
  INNER JOIN PRODUNIT U ON E.ORGANIZATION_ID= U.ORGANIZATION_ID 
  AND E.PRODUNIT_NO= U.PRODUNIT_NO 
WHERE
  R.ERP_FLAG IS NULL 
  AND W.ACTIVE_FLAG= 'REL' 
ORDER BY
  W.WO_NO,
  R.REP_MFG_DATE ""
```

SAP-1：
可參考舊版 PsModifyRfc_importHoursToSap
程序如有任何異常，紀錄錯誤LOG

**獲得BAPI方法**
|              | 型態   | 參數值                | 說明 |
| ------------ | ------ | --------------------- | ---- |
| BAPIFunction | string | Z_PP_CL_ORDER_CONFIRM |      |

**設定輸入參數**
| 參數名稱   | 參數值 | 型態     | 說明 |
| ---------- | ------ | -------- | ---- |
| IT_CONFIRM |        | JCoTable |      |

IT_CONFIRM JCoTable 參數
| 參數名稱        | 參數值                  | 型態   | 說明                       |
| --------------- | ----------------------- | ------ | -------------------------- |
| ORDERID         | {SQL-2.WO_NO}           | string | format: "%012d"            |
| OPERATION       | {SQL-2.SAP_OPERATION}   | string | format: "%04d"             |
| POSTG_DATE      | {SQL-2.REP_MFG_DATE}    | date   |                            |
| WORK_CNTR       | {SQL-2.SAP_WC}          | string | SAP_WC不為空時             |
| CONF_ACTI_UNIT1 | "STD"                   | string |                            |
| CONF_ACTIVITY1  | {SQL-2.REP_SHIFT_HOURS} | double | SAP_WC為T3N時，作業1工時X2 |
| CONF_ACTI_UNIT2 | "STD"                   | string | SAP_WC不為T11、M11、T2N時  |
| CONF_ACTIVITY2  | {SQL-2.REP_SHIFT_HOURS} | double | SAP_WC不為T11、M11、T2N時  |
| CONF_ACTI_UNIT3 | "STD"                   | string | SAP_WC不為T11、M11、T2N時  |
| CONF_ACTIVITY3  | {SQL-2.REP_SHIFT_HOURS} | double | SAP_WC不為T11、M11、T2N時  |
| CONF_ACTI_UNIT4 | "STD"                   | string | SAP_WC不為T11、M11、T2N時  |
| CONF_ACTIVITY4  | {SQL-2.REP_SHIFT_HOURS} | double | SAP_WC不為T11、M11、T2N時  |
| CONF_ACTI_UNIT5 | "STD"                   | string | SAP_WC不為T11、M11、T2N時  |
| CONF_ACTIVITY5  | {SQL-2.REP_SHIFT_HOURS} | double | SAP_WC不為T11、M11、T2N時  |
| CONF_ACTI_UNIT6 | "STD"                   | string |                            |
| CONF_ACTIVITY6  | {SQL-2.REP_SHIFT_HOURS} | double |                            |


| 取得回傳表格名稱 |
| ---------------- |
| ET_RETURN        |

| SAP欄位名稱  | SAP資料型別 | 欄位值 | 說明 |
| ------------ | :---------: | ------ | ---- |
| MESSAGE_TYPE |   string    |        |      |
| MESSAGE      |   string    |        |      |
| RUECK        |   string    |        |      |

SQL-LOG:
```sql
UPDATE PS_R 
SET ERP_FLAG='Y',ERP_RTN_TYPE = {SAP-1.MESSAGE_TYPE},ERP_RTN_MSG = {SAP-1.MESSAGE},ERP_CONFIRM_NO = {SAP-1.RUECK} 
WHERE PSR_UID = {SQL-2.PSR_UID}
```
