# 功能管理-取得單一筆系統功能資料
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

功能管理-取得單一筆系統功能資料


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230406 | 新增規格 | Nick   |

## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    | /function/get_one |
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位    | 名稱 | 資料型別 | 必填 | 資料儲存 & 說明 |
| ------- | ---- | :------: | :--: | --------------- |
| opFCode | 功能代號  |  string  |  M  |                 |

#### Request 範例

```json
{
  "opFCode":"100"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 執行  SQL-1 確認該功能代號存在，找不到 return 400,"NOTFOUND"

* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容


# Request 後端邏輯說明

SQL-1:

```sql
SELECT CODE as FCODE,
       CNAME as FNAME,
         URL
	FROM SYS_FUNCTION_V3   
	ORDER BY SORT_ID WHERE CODE = {opFCode}
```


# Response 欄位
| 欄位    | 名稱     | 資料型別 | 資料儲存 & 說明 |
|---------|----------|----------|-----------------|
| opFCode | 功能代號 | string   | FCODE           |
| opFName | 功能名稱 | string   | FNAME           |
| opUrl   | URL      | string   | URL             |

#### Response 範例

```json
{
  "msgCode":"",
  "result":{
    "content":
         {
            "opFCode":"100",
            "opFName":"BOM維護",
            "opUrl": null
         }
  }
}
```

