# BOM維護-取得BOM列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得BOM列表

## 修改歷程

| 修改時間 | 內容                                                                   | 修改者 |
|----------|----------------------------------------------------------------------|--------|
| 20230518 | 新增規格                                                               | Nick   |
| 20230607 | 調整 ORDER 欄位及可調整排序方式                                        | Nick   |
| 20230607 | 增加 opBomUid                                                          | Nick   |
| 20230617 | B.ITEM_DESC -> I.ITEM_DESC ，優化SQL說明                                | Nick   |
| 20230803 | 優化SQL效能                                                            | Nick   |
| 20230804 | 客戶提出效能慢，調整成 TOP 1000 且排序順序規則調整、opOrder 前端不會給了 | Nick   |
| 20230811 | 客戶提出: 建立時間 換成 替代說明                                       | Nick   |


## 來源URL及資料格式

| 項目   | 說明      |
|--------|-----------|
| URL    | /bom/list |
| method | post      |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。  
| 欄位        | 名稱     | 資料型別 | 必填 | 來源資料 & 說明                                                                                  |
|-------------|--------|----------|:----:|------------------------------------------------------------------------------------------------|
| opItemNo    | 料號     | string   |  O   | 預設 `null`                                                                                      |
| opPatternNo | 色號     | string   |  O   | 預設 `null`                                                                                      |
| opItemDesc  | 料號摘要 | string   |  O   | 預設 `null`                                                                                      |
| opRemark    | 備註     | string   |  O   | 預設 `null`                                                                                      |
| opMtrPartNo | 原料     | string   |  O   | 預設 `null`                                                                                      |
| opBDate     | 開始時間 | string   |  O   | 預設 `null`    YYYY/MM/DD                                                                        |
| opEDate     | 結束時間 | string   |  O   | 預設 `null`   YYYY/MM/DD                                                                         |
| opPage      | 第幾頁   | integer  |  O   | 預設`1`，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推                          |
| opPageSize  | 分頁筆數 | integer  |  O   | 預設 `10`                                                                                        |
| opOrder     | 排序方式 | string   |  O   | 預設`DESC`，"ASC":升冪(建立時間)  /"DESC" :降冪(建立時間) / NULL: 按照色號>料號>替代結構 ASC 排序 |

#### Request 範例

```json
{
  "opPatternNo":"H16548MM",
  "opPage":1
  
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 使用前端給的條件，參考 SQL-1 自組【搜尋條件】(使用 AND 開頭，方便與原WHERE 條件串接)，如為 null 代表不需組條件
* {offset} 為起始列印 row ，第一頁為 0，第二頁後為 0 + (opPageSize*(opPage-1))
* 參考 SQL-1 查回結果 result
* Response 結果需含分頁資訊，無資料 pageInfo 裡面項目皆給 0，頁數與分頁筆數前端如有提供，依前端判斷給的資料計算相關數值及拆分頁面
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
SELECT * FROM (
  SELECT * FROM (
    SELECT B.BOM_UID,I.PATTERN_NO,B.ITEM_NO,B.BOM_NO,B.BOM_DESC
    FROM BOM_MTM B 
    INNER JOIN ITEM_H I ON B.ITEM_NO=I.ITEM_NO AND B.ORGANIZATION_ID=5000
    INNER JOIN PAT_H P ON I.PATTERN_NO=P.PATTERN_NO 
    LEFT JOIN BOM_MTR R ON B.BOM_UID=R.BOM_UID 
    WHERE 1=1
    --搜尋條件，請依前端變數是否為 null 決定是走要組
    
    --如{opItemNo}非 null 則加入以下SQL
    AND B.ITEM_NO  like '%{opItemNo}%'          --料號
    
    --如{opPatternNo}非 null 則加入以下SQL
    AND I.PATTERN_NO like '%{opPatternNo}%'     --色號
    
    --如{opItemDesc}非 null 則加入以下SQL
    AND I.ITEM_DESC  like '%{opItemDesc}%'      --料號摘要
    
    --如{opRemark}非 null 則加入以下SQL
    AND B.REMARK    like '%{opRemark}%'         --備註
    
    --如{opMtrPartNo}非 null 則加入以下SQL
    AND R.MTR_PART_NO like '%{opMtrPartNo}%'    --原料
    
    --如{opBDate}非 null 則加入以下SQL
    AND B.CDT>=CONVERT(DATETIME,{opBDate})      --日期起迄
    
    --如{opEDate}非 null 則加入以下SQL
    AND B.CDT<CONVERT(DATETIME,{opEDate})+1     --日期起迄
  ) AS MainT 
  ORDER BY PATTERN_NO,ITEM_NO,BOM_NO
  OFFSET 0 ROWS
  FETCH NEXT 1000 ROWS ONLY
) AS TOP1000T
ORDER BY PATTERN_NO,ITEM_NO,BOM_NO
OFFSET {offset} ROWS
FETCH NEXT {opageSize} ROWS ONLY
```


# Response 欄位
| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
|----------|----------|----------|-----------------|
| pageInfo | 分頁資訊 | object   |                |
| content  | 附帶訊息 | array    |                |
#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別  | 來源資料 & 說明 |
|------------|--------------|----------|-----------------|
| pageNumber | 目前所在分頁 | int       |                 |
| pageSize   | 分頁筆數大小 | int       |                 |
| pages      | 總分頁數     | int       |                 |
| total      | 總資料筆數   | int       |                 |

#### 【content】array
| 欄位        | 名稱     | 資料型別 | 來源資料 & 說明 |
|-------------|--------|----------|-----------------|
| opPatternNo | 色號     | string   | PATTERN_NO      |
| opItemNo    | 料號     | string   | ITEM_NO         |
| opBomNo     | 替代結構 | string   | BOM_NO          |
| opBomDesc   | 替代說明 | string   | BOM_DESC        |
| opBomUid    | BOM代碼  | string   | B.BOM_UID       |

#### Response 範例

```json
{
    "msgCode": null,
    "result": {
        "pageInfo": {
        "pageNumber": 1, 
        "pageSize": 10,
        "pages": 1,      
        "total": 2     
        },
       "content": [
       {
         "opPatternNo": "H16548MM",
         "opItemNo": "L80NXXXMAX25H16548",
         "opBomNo": "01",
         "opBomDesc": "1倍",
         "opBomUid":"00005D59-963F-4F11-9C68-D81909D98AC4"
       }, 
       {
         "opPatternNo": "H16548MM",
         "opItemNo": "L80NXXXMAX25H16548",
         "opBomNo": "02",
         "opBomDesc": "11倍",
         "opBomUid":"9C13AE25-59DD-48D0-9100-FC72388EFF59"
       }]
    }
}
```

