# OPMS_I1.客訴-變更客訴案件等級
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

變更客訴案件等級


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230725 | 新增規格 | Ellen  |

## 來源URL及資料格式

| 項目   | 說明                  |
| ------ | --------------------- |
| URL    | /cs/update_claim_flag |
| method | post                  |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。

| 欄位        | 名稱             | 資料型別 | 必填  | 資料儲存 & 說明               |
| ----------- | ---------------- | :------: | :---: | ----------------------------- |
| opCsUid     | 案件編號         |  string  |   M   |                               |
| opClaimFlag | 是否為新增       |  string  |   M   | Y/N Y: 客訴案件 / N: 客戶抱怨 |
| opClaimDesc | 客戶抱怨判定原因 |  string  |   M   |                               |

#### Request 範例
```json
{
    "opCsUid":"183AAEA7-6598-415B-8D52-3E8A6D710CE2",  
    "opClaimFlag":"N",   
    "opClaimDesc":"抱怨判定原因"
}
```

# Request 後端流程說明
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* 確認客訴案件是否已存在，如果不存在 return 400 NOTFOUND
* 執行 SQL-1 更新客訴案件等級
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容


# Request 後端邏輯說明
SQL-1
```sql
UPDATE CS_H SET CLAIM_FLAG={opClaimFlag}, CLAIM_DESC={opClaimDesc}, 
  UDT=GETDATE(), UPDATE_BY={登入者使用者ID}
WHERE CS_UID={opCsUid}
```

# Response 欄位
| 欄位    | 名稱 | 資料型別 | 資料儲存 & 說明 |
| ------- | ---- | -------- | --------------- |
| opCsUid | uid  | string   |                 |

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":{
      "opCsUid":"183AAEA7-6598-415B-8D52-3E8A6D710CE2"
    }
  }
}
```