# 染色-複材-理想線表單列印-列印備料表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容               | 修改者 |
|----------|------------------|--------|
| 20230912 | 新增規格           | 黃東俞 |
| 20231012 | 移除不需列印的料號 | Nick   |


## 來源URL及資料格式

| 項目   | 說明            |
|--------|-----------------|
| URL    | /wo/print_e_pdf |
| method | post            |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位    | 名稱   | 資料型別 | 必填 | 資料儲存 & 說明 |
|---------|------|----------|:----:|-----------------|
| opWoUid | 工單ID | string   |  M   |                 |

#### Request 範例

```json
{
  "opWoUid": "0A18CF3D-BF78-4920-B43C-C3280EC4DC62"
}
```

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 資料處理流程參考WoPrintE.jsp
* 查詢[工單資料]
  * 參考SQL-1取得資料後
  * 帶入SQL-1取得的ORGANIZATION_ID和WO_NO再呼叫SAP-1取回傳資料
  * 取SAP-1回傳的T_ORDER的第一筆資料，有取到資料時，將資料填入或取代對應的欄位值
* 查詢[工單色粉檔]：參考SQL-2
* 查詢[配色指示作業原料檔]：參考SQL-3
* [每batch用量] = BATCH_QTY(SQL-1)
* [Batch用量次數] = BATCH_TIMES(SQL-1)
* [尾數量] = REMAIN_QTY(SQL-1)
* [尾數量次數] = REMAIN_TIMES(SQL-1)
* [投入原料量] = RAW_QTY(SQL-1)
* [工廠別] = ORGANIZATION_ID(SQL-1)
* [requirementList]
  * 查詢SAP-2，呼叫方式和SAP-1完全相同，帶入SQL-1取得的[工廠別]和WO_NO
  * 檢查回傳的T_ORDER的第一筆資料，有資料時，再改取T_COMP的全部資料填入[requirementList]
  * 整理資料，歷遍[requirementList]，
    * 若compItemUom(單位)為"G"時修改資料
      * requiredQty除以1000，取5位小數轉為字串
      * compItemUom改為"KG"
    * 符合下列任一條件時，將compItem改為空字串""
      * requiredQty為0
      * [工廠別]為"5000"，且compItem包含"A-246"
      * 若[工廠別]為"5000"，且compItem="05-251-A-L09XXXXXX"
      * compItem為"92"開頭或"04-912"開頭
  * 查詢投料設備與比例資料
    * 取[fedTo]， 有取到資料時再進行後續步驟
      * 參考SQL-4，帶入opWoUid和SQL-1取得的EQUIP_ID，並且只取第一筆資料
    * 取[fedList]
      * 參考SQL-5，帶入opWoUid和EQUIP_ID(SQL-4)，注意帶入的EQUIP_ID可能為null
      * 有取到資料時，再進行下列步驟，尋找下料設備
        * [totalFedRate] = 0
        * 建立[tempList]內容直接照抄[requirementList]
        * 歷遍[tempList]
          * [foundCount] = 0
          * 歷遍[fedList]，當[fedList].bomPartNo = [tempList].compItem時進行下列步驟
            * 重新計算[totalFedRate] += [fedList].feederRate
            * 修改該筆[tempList].locator = [fedList].feederNo
            * [foundCount]++
            * 將該筆[tempList]資料加入[requirementList]
          * 若下料設備有2筆以上才需要拆分(歷遍[fedList]結束後，若[foundCount]>1)
            * 重新歷遍[fedList]，當[fedList].bomPartNo = [tempList].compItem時，新增一筆資料加入[requirementList]，資料內容如下
              * compItem = [tempList].compItem
              * compItemUom = [tempList].compItemUom
              * locator = [fedList].feederNo
              * subinv = "Y"
              * requiredQty = [tempList].requiredQty x [fedList].feederRate / [totalFedRate] 四捨五入取五位小數轉字串
    * 秤料單位：
      * 歷遍[requirementList]，取compItem不為空字串""者進行後續步驟
      * [isPgm]預設false
      * 歷遍[工單色粉檔]
        * [工單色粉檔]woPgmPartNo.substring(1) = [requirementList].compItem.substring(1)時
          * isPgm = true
          * [工單色粉檔]selfPick="M"或"D"時，將[requirementList].subinv改為"M"
          * [工單色粉檔]selfPick不為"M"或"D"時，[requirementList].subinv改改為"N"
      * 歷遍[配色指示作業原料檔]
        * [配色指示作業原料檔].mtrPartNo.substring(1) = [requirementList].compItem.substring(1)時
          * 檢查[isPgm]，若為false時，將[requirementList].subinv改為"M"
    * 相同料合併
      * 歷遍[requirementList]，若有compItem相同且不為空的資料，將requiredQty相加，並將另一筆的compItem改為空字串""
    * 零星品
      * 若[工廠別]="6000"或([工廠別]="5000"且wipClassNo不為"Z52"開頭)時才進行一下步
      * 歷遍[requirementList]，取subinv="Y"且compItem不為空也不為 "06","04","05-201","05-251"的資料為零星品
      * [starQty]：加總零星品requiredQty
      * 紀錄[starUom] = 第一筆零星品的compItemUom
      * 紀錄[starItem] = 全部零星品的compItem加在一起用"/"分隔
    * 自有、客供、奇美料號合併
      * 歷遍[requirementList]，compItem字串長度18碼為CLT自有或客供原料料號，12碼為CMC奇美原料料號
      * CLT和CLT：compItem第3碼之後料號都一樣的進行合併
      * CMC和CMC：compItem前10碼相同進行合併
      * CLT和CMC：CLT第11~13碼和CMC前3碼相同時，同級別才合併
        * CMC第10碼為"1"且CLT第4~5碼為"10"
        * CMC第10碼為"9"且CLT第4~5碼為"199"
      * 合併處理
        * compItem二者以字串連接且中間插入換行"\n"
        * requiredQty二者相加
        * locator若為空字串，則填入另一個的locator
        * 將另一個的compItem設為空字串""
  * 排除多餘資料：只取compItem不為空字串"" 且 subinv不為"N" 且 requiredQty不為0的資料，其餘皆排除
  * 色粉整數包
    * [工廠別]="5000"時，才需撈取資料
    * 參考SQL-6取得[色粉整數包資料]，再篩選selfPick不為null也不為"N"的資料
    * 歷遍[色粉整數包資料]
      * 取[整數包重量]：packWeight
      * 取[每批次用量]：pgmBatchQty
      * 取[尾數用量]：pgmRemainQty
      * 取[單位]：unit
      * 當[單位]為"G"時，[每批次用量]和[尾數用量]要除以1000轉為公斤
      * 篩選[每批次用量]>=[整數包重量]的資料，計算後加入一筆新資料到[requirementList]，機算方式如下
        * compItem = [色粉整數包資料].woPgmPartNo
        * compItemUom = "包"
        * requiredQty = [色粉整數包資料].pgmBatchQty
        * packWeight = [整數包重量]
        * packBatch = [每批次用量] / [整數包重量]
        * packRemain = [尾數用量] / [整數包重量]
  * 零星品，放在最多的feeder上
    * 若前面計算零星品的時候有抓到資料才進行處理
    * 歷遍[requirementList]
      * 取[fed]：預設空字串""，若locator有資料則取 locator.split("-")[0]
      * 統計[feederWeight]：分別統計 fed 為 "F1","F2","F3","F4","F5","F6","BDP","" 的requiredQty
    * 新增一筆資料加入[requirementList]
      * compItem = "零星品:" + [starItem]
      * requiredQty = [starQty]
      * compItemUom = [starUom]
      * locator：取[feederWeight]最大的一筆對應的fed填入
  * [reportList]
    * 歷遍[requirementList]
    * 取[fed]：預設空字串""，若locator有資料則取 locator.split("-")[0]
    * 將[fed]為"F1","F2","F3","F4","F5","F6","BDP",""的資料依序加入[reportList]
  * [是否顯示色粉欄]
    * 預設false不顯示
    * [requirementList]若有subinv="N"且requiredQty>0的資料，則改為true顯示
* 移除不需列印的料號 (註:因前置作業多，怕前面移除產生計算問題，所以放到最後面報表物件產生前)
  * 歷遍[reportList]
  * 抓取 compItem 前六碼，如與下面清單相符，該筆從 reportList 中移除不列印
    | 不需列印料號前六碼 |
    |--------------------|
    | 92-912             |
    | 92-926             |
    | 92-027             |
    | 92-914             |
    | 92-925             |
    | 04-011             |
    | 04-401             |
    | 04R401             |
    | 04-914             |
    | 04-027             |
    | 04-925             |
    | 04-015             |

* 計算[jasper頁數]
  * 計算[dPages]
    * 若[工單資料].wipClassNo為"Z52"開頭 [dPages]=[reportList].size()+([是否顯示色粉欄]?2:0);
    * 否則 [dPages] = [reportList].size()+([是否顯示色粉欄]?1:0);
  * [jasper總頁數] = (int)Math.ceil([dPages]/5);

* 每頁jasper可以寫入五筆[reportList]的資料，一筆資料建立一個 pars 和 jrds
  * pars
    * 將[reportList]資料填入
      * [資料序號]：該頁的資料序號，1~5
      * 檢查單位
        * 當compItemUom = "包"時
          * itemNo+資料序號 = compItem
          * totalQty+資料序號 = (packBatch x [Batch用量次數] + packRemain) + 單位(compItemUom)
          * batchQty+資料序號 = (packBatch x packWeight) + "KG"
          * batchPack+資料序號 = packBatch + compItemUom
          * 若 packRemain 不為0時
            * remainQty+資料序號 = packWeight + "KG"
            * remainPack+資料序號 = packRemain + compItemUom
        * 當compItemUom為"零星品"開頭時
          * remark = compItem
          * itemNo+資料序號 = "零星品(如右)"
        * compItemUom不為"零星品"開頭時
          * itemNo+資料序號 = compItem
      * feederNo+資料序號 = locator
      * 若requiredQty不為空
        * [qty] = requiredQty
        * [quantityPer] = qty / [投入原料量] 四捨五入取五位小數
        * totalQty+資料序號 = [qty]取兩位小數 + compItemUom
        * batchQty+資料序號 = [quantityPer] x [每batch用量] 取兩位小數 + compItemUom
        * remainQty+資料序號
          * 當[尾數量]=0時填入"0"
          * [尾數量]不為0時，[quantityPer] x [尾數量] 取兩位小數 + compItemUom
    * [是否顯示色粉欄]為true時填入色粉欄資料
      * [資料序號]：該頁的資料序號，1~5
      * wipClassNo(SAP-1)若為"Z52"開頭(會有兩筆色粉資料)
        * 第一筆色粉資料
          * pgm+資料序號：取檔案路徑 /jasperReport/pgm1.jpg
          * totalQty+資料序號
            * [尾數量]>0時：[每batch用量] + "\*" +[Batch用量次數] + " ; " + [尾數量] + "\*" + [尾數量次數]
            * [尾數量]<=0時：[每batch用量] + "\*" +[Batch用量次數]
          * remainQty+資料序號
            * [尾數量]>0時：" + [尾數量] + "\*" + [尾數量次數]
          * batchQty+資料序號 = [每batch用量] + "\*1"
        * 第二筆色粉資料
          * pgm+資料序號：取檔案路徑 /jasperReport/pgm2.jpg
          * batchPack+資料序號："有補色請打勾記錄"
      * wipClassNo(SAP-1)不為"Z52"開頭
        * itemNo+資料序號："色粉"
        * totalQty+資料序號： [Batch用量次數] + "桶"
        * remainQty+資料序號：空字串""
    * today：系統日期 yyyy/MM/dd hh:mm:ss
    * patternNo：[工單資料].patternNo
    * woNo：[工單資料].woNo
    * pageDesc：目前頁數/[jasper總頁數]
    * ISO_DESC
      * [工廠別]="5000"，"保存期限:1年 CLT-35QAA0022 CLT-40QAA0194-Ver.08"
      * [工廠別]="6000"，"保存期限3年 LS-35QAA0077  LS-40QAA0258-Ver.03"
    * LOGO
      * [工廠別]="5000"，取檔案 /jasperReport/logo.png
      * [工廠別]="6000"，取檔案 /jasperReport/logoLSO.png
    * ISO_PROD_OPT
      * 工廠別="5000"時，查詢SQL-7，若OPT_DESC有值時則填入OPT_DESC
      * 若OPT_DESC無值時，改填 "□一般級  □醫療級"
  * jrds
    * 建立8筆資料 rowNo 分別填入 "0"~"7"

# Request 後端邏輯說明

SQL-1

```sql
--WoQueryDao.getWoHSingleByUid
SELECT H.WO_UID, H.ACTIVE_FLAG, H.BOM_UID, H.EQUIP_ID, H.ORGANIZATION_ID, H.WO_NO, H.WO_SEQ, H.LOT_NO, H.CUST_PO_NO,
CONVERT(VARCHAR(10),H.WO_DATE,111) AS WO_DATE, H.PRODUNIT_NO, H.CUST_NO, H.CUST_NAME,
H.INDIR_CUST, B.CUST_NAME AS BASE_CUST, H.UNIT, H.WEIGHT, B.CMDI_NO, B.CMDI_NAME, B.COLOR, H.[USE], H.SALES_NO,
case H.RAW_QTY when cast(H.RAW_QTY as int) then cast(CONVERT(int,H.RAW_QTY) as varchar(20)) else cast(CONVERT(FLOAT,H.RAW_QTY) as varchar(20)) end RAW_QTY,
case H.BATCH_QTY when cast(H.BATCH_QTY as int) then cast(CONVERT(int,H.BATCH_QTY) as varchar(20)) else cast(CONVERT(FLOAT,H.BATCH_QTY) as varchar(20)) end BATCH_QTY,
H.BATCH_TIMES, case H.REMAIN_QTY when cast(H.REMAIN_QTY as int) then cast(CONVERT(int,H.REMAIN_QTY) as varchar(20)) else cast(CONVERT(FLOAT,H.REMAIN_QTY) as varchar(20)) end REMAIN_QTY,
H.REMAIN_TIMES, H.MODIFY_FLAG,H.PRINT_FLAG, H.REMARK, H.BOM_REMARK, H.ERP_FLAG,
H.WIP_CLASS_CODE, H.ALT_BOM_PART_NO,H.ALT_BOM_DESIGNATOR,H.ALT_ROUTING_PART_NO,H.ALT_ROUTING_DESIGNATOR, H.COMPLETION_SUBINV,
H.COMPLETION_LOCATOR, H.SALES_ORDER_NO, H.SALES_QTY, H.OEM_ORDER_NO, CONVERT(VARCHAR(10),H.ESTIMATED_SHIP_DATE,111) AS ESTIMATED_SHIP_DATE,
I.PATTERN_NO, B.ITEM_NO, B.ITEM_DESC, B.BOM_NO, B.BOM_NAME, B.BOM_DESC, B.[USE], B.BASE_ITEM_NO, B.BASE_BOM_NO, H.STD_N_QUANTITY_PER,
H.CUS_N_QUANTITY_PER, REPLACE(CONVERT(VARCHAR(16),H.CDT,120),'-','/') AS CDT, H.CREATE_BY, REPLACE(CONVERT(VARCHAR(16),H.UDT,120),'-','/') AS UDT, H.UPDATE_BY,
B.MULTIPLE BOM_MULTIPLE, H.CTN_WEIGHT, H.PROD_REMARK, H.SALES_ORDER_SEQ, B.BIAXIS_ONLY, B.DYE_ONLY, H.GRADE_NO, H.PROD_REMARK, H.PS_FLAG, H.WO_TYPE
FROM WO_H H
INNER JOIN BOM_MTM B ON H.BOM_UID = B.BOM_UID
INNER JOIN ITEM_H I ON B.ITEM_NO = I.ITEM_NO
WHERE H.WO_UID = {opWoUid}
```
SQL-2
```sql
SELECT A.WO_PGM_PART_NO, A.QUANTITY_PER, A.UNIT, A.SITE_PICK, A.SELF_PICK, A.PGM_BATCH_QTY, A.PGM_REMAIN_QTY, A.MODIFY_QTY, A.MODIFY_UNIT_QTY, A.FEEDER_SEQ,B.BOM_UID AS PGM_BOM_UID
FROM (SELECT H.ORGANIZATION_ID, W.WO_PGM_PART_NO, W.QUANTITY_PER, W.UNIT, W.SITE_PICK, W.SELF_PICK, W.PGM_BATCH_QTY, W.PGM_REMAIN_QTY, W.MODIFY_QTY, W.MODIFY_UNIT_QTY, W.FEEDER_SEQ
    FROM WO_PGM W
    INNER JOIN WO_H H ON W.WO_UID = H.WO_UID
    WHERE W.WO_UID = {opWoUid}) A
  LEFT JOIN (SELECT ORGANIZATION_ID,ITEM_NO, BOM_UID FROM BOM_MTM WHERE BOM_NO IS NULL AND ACTIVE_FLAG = 'Y') B ON A.ORGANIZATION_ID = B.ORGANIZATION_ID AND A.WO_PGM_PART_NO = B.ITEM_NO
ORDER BY WO_PGM_PART_NO
```

SQL-3
```sql
-- WoQueryDao.getWoMtrList
SELECT R.WO_MTR_PART_NO,
  case R.QUANTITY_PER when cast(R.QUANTITY_PER as int) then cast(CONVERT(int,R.QUANTITY_PER) as varchar(20)) else cast(CONVERT(FLOAT,R.QUANTITY_PER) as varchar(20)) end QUANTITY_PER,
  R.UNIT
FROM WO_MTR R WHERE R.WO_MTR_PART_NO NOT LIKE '92%' AND R.WO_UID = {opWoUid}
ORDER BY R.QUANTITY_PER DESC,R.WO_MTR_PART_NO
```

SQL-4

```sql
-- WoQueryDao.getWoFedWithEquip
SELECT F.EQUIP_ID, F.F_1_RATE, F.F_2_RATE, F.F_3_RATE, F.F_4_RATE, F.F_5_RATE, F.F_6_RATE, F.BDP_RATE,
CASE WHEN F.EQUIP_ID IS NULL THEN 1 ELSE 0 END EQUIP
FROM WO_H W, BOM_FED F
WHERE W.BOM_UID = F.BOM_UID AND W.WO_UID = {opWoUid} AND (F.EQUIP_ID IS NULL OR F.EQUIP_ID = {WO_H.EQUIP_ID(SQL-1)})
ORDER BY EQUIP
```

SQL-5
```sql
-- WoQueryDao.getWoFedDWithEquip
SELECT D.FEEDER_NO,D.BOM_PART_NO,D.FEEDER_RATE
FROM WO_H W
INNER JOIN BOM_FED F ON W.BOM_UID = F.BOM_UID
INNER JOIN BOM_FED_D D ON F.BOM_FED_UID = D.BOM_FED_UID
WHERE W.WO_UID = {opWoUid}
-- 帶入的EQUIP_ID(SQL-4)不為null時
AND F.EQUIP_ID = {BOM_FED.EQUIP_ID(SQL-4)}
-- 帶入的EQUIP_ID(SQL-4)為null時
AND F.EQUIP_ID IS NULL
```

SQL-6
```sql
-- WoQueryDao.getWoPgmListWithPack
SELECT A.WO_PGM_PART_NO, A.PACK_UNIT, A.PACK_WEIGHT, A.QUANTITY_PER, A.UNIT, A.SITE_PICK, A.SELF_PICK, A.PGM_BATCH_QTY, A.PGM_REMAIN_QTY, A.FEEDER_SEQ, B.BOM_UID PGM_BOM_UID
FROM (
  SELECT W.ORGANIZATION_ID, P.WO_PGM_PART_NO, I.UNIT PACK_UNIT, I.PACKING_WEIGHT PACK_WEIGHT, P.QUANTITY_PER, P.UNIT, SITE_PICK, P.SELF_PICK, P.PGM_BATCH_QTY, P.PGM_REMAIN_QTY, P.FEEDER_SEQ
  FROM WO_PGM P
  INNER JOIN WO_H W ON P.WO_UID = W.WO_UID
  LEFT JOIN ITEM_H I ON P.WO_PGM_PART_NO = I.ITEM_NO
  WHERE P.WO_UID = {opWoUid} ) A
LEFT JOIN (SELECT ORGANIZATION_ID, ITEM_NO, BOM_UID
  FROM BOM_MTM
  WHERE BOM_NO IS NULL
  AND ACTIVE_FLAG = 'Y') B ON A.ORGANIZATION_ID = B.ORGANIZATION_ID AND A.WO_PGM_PART_NO = B.ITEM_NO
ORDER BY WO_PGM_PART_NO
```

SQL-7
```sql
--- WoQueryDao.getIsoProdOpt
SELECT OPT_DESC FROM WO_SIS WHERE LIST_HEADER_ID = 158 AND WO_UID = {opWoUid}
```


SAP-1：
參考舊版 WoQueryRfc.getWoHSingle

**獲得BAPI方法**
|              | 型態   | 參數值                     | 說明 |
|--------------|--------|----------------------------|------|
| BAPIFunction | string | Z_PP_OPMS_PRODUCTION_ORDER |      |

**設定輸入參數**
| 參數名稱 | 參數值                      | 型態         | 說明 |
|----------|-----------------------------|--------------|------|
| I_WERKS  | WO_H.ORGANIZATION_ID(SQL-1) | string       |      |
| I_AUFNR  | WO_NO(SQL-1)                | string       |      |
| I_ERDAT  |                             | JCoStructure |      |

**設定輸入參數-I_ERDAT**
| 參數名稱 | 參數值     | 型態   | 說明 |
|----------|------------|--------|------|
| ERDAT_FR | "00000000" | string |      |
| ERDAT_TO | "99991231" | string |      |

| 取得回傳表格名稱 |
|------------------|
| T_ORDER          |

| SAP欄位名稱 | SAP資料型別 | 欄位         | 說明                                                                                        |
|-------------|:-----------:|--------------|---------------------------------------------------------------------------------------------|
| MATNR       |   string    | 料號         | 取代 BOM_MTM.ITEM_NO                                                                        |
| COLOR       |   string    | 色相         | 取代 BOM_MTM.COLOR                                                                          |
| STAND       |   string    | 生產單位     | 過濾前綴0再取代 WO_H.PRODUNIT_NO                                                            |
| STAND_T     |   string    | 生產單位名稱 | produnitName                                                                                |
| AUFNR       |   string    | 工單號碼     | 過濾前綴0再取代 WO_H.WO_NO                                                                  |
| ERNAM       |   string    | 業助代號     | 取代 WO_H.SALES_NO                                                                          |
| KUNNR       |   string    | 客戶代號     | 過濾前綴0再取代 WO_H.CUST_NO                                                                |
| SORT2       |   string    | 客戶名稱     | 取代 WO_H.CUST_NAME                                                                         |
| IHREZ       |   string    | 間接客戶     | 取代 WO_H.INDIR_CUST                                                                        |
| GRADE       |   string    | Grade No     | 取代 WO_H.GRADE_NO                                                                          |
| AUART       |   string    |              | wipClassNo                                                                                  |
| AUART_T     |   string    |              | 取代 WO_H.WIP_CLASS_CODE                                                                    |
| MAKTX       |   string    | 料號摘要     | 取代 BOM_MTM.ITEM_DESC                                                                      |
| CUSTPO      |   string    | 客戶PO單號   | 取代 WO_H.CUST_PO_NO                                                                        |
| VORNR       |   string    |              | 過濾前綴0                                                                                   |
| ARBPL       |   string    |              | sapWc                                                                                       |
| GAMNG       |   string    |              | 四捨五入至五位小數並轉為字串                                                                |
| GMEIN       |   string    |              | 取代 WO_H.UNIT                                                                              |
| STATUS      |   string    |              | 取代 WO_H.ACTIVE_FLAG                                                                       |
| GSTRP       |   string    | 使用日期     | 取代 WO_H.WO_DATE                                                                           |
| GSTRP       |   string    | 使用日期     | 取代 WO_H.WO_DATE  格式：yyyy/MM/dd                                                          |
| ARBPL_T     |   string    | 機台名稱     | equipName                                                                                   |
| SOQTY       |   string    | 銷售數量     | 四捨五入至五位小數並轉為字串 取代 WO_H.SALES_QTY                                            |
| SHIP_DATE   |   string    |              | WO_H.ESTIMATED_SHIP_DATE為空且SHIP_DATE不為空才取代WO_H.ESTIMATED_SHIP_DATE 格式：yyyy/MM/dd |
| LGORT       |   string    |              | 取代 WO_H.COMPLETION_SUBINV                                                                 |
| KDAUF       |   string    |              | 過濾前綴0再取代WO_H.SALES_ORDER_NO                                                          |
| KDPOS       |   string    |              | 過濾前綴0再取代WO_H.SALES_ORDER_SEQ                                                         |
| CMC_SO_TYPE |   string    |              | 四捨五入至五位小數並轉為字串                                                                |

SAP-2
參考舊版 WoQueryRfc.getWipEntities

**獲得BAPI方法**
|              | 型態   | 參數值                     | 說明 |
|--------------|--------|----------------------------|------|
| BAPIFunction | string | Z_PP_OPMS_PRODUCTION_ORDER |      |

**設定輸入參數**
| 參數名稱 | 參數值                      | 型態         | 說明 |
|----------|-----------------------------|--------------|------|
| I_WERKS  | WO_H.ORGANIZATION_ID(SQL-1) | string       |      |
| I_AUFNR  | WO_NO(SQL-1)                | string       |      |
| I_ERDAT  |                             | JCoStructure |      |

**設定輸入參數-I_ERDAT**
| 參數名稱 | 參數值     | 型態   | 說明 |
|----------|------------|--------|------|
| ERDAT_FR | "00000000" | string |      |
| ERDAT_TO | "99991231" | string |      |

| 取得回傳表格名稱 |
|------------------|
| T_COMP           |

| SAP欄位名稱 | SAP資料型別 | 欄位 | 說明        |
|-------------|:-----------:|------|-------------|
| IDNRK       |   string    |      | compItem    |
| MEINS       |   string    |      | compItemUom |
| BDMNG       |   string    |      | requiredQty |
| ENMNG       |   string    |      | issuedQty   |
| LGORT       |   string    |      | subinv      |
| CHARG       |   string    |      | locator     |

JASPER-1:
**jasper檔案位置**
|          | 型態   | 參數值                          | 說明 |
|----------|--------|---------------------------------|------|
| RealPath | string | /jasperReport/WoPrintE_M.jasper |      |

**jasper fillReport參數**
| 參數名稱 | 名稱 | 型態                | 來源資料 & 說明 |
|----------|------|---------------------|-----------------|
| pars     |      | Map<String, Object> |                 |
| jrds     |      | JRDataSource        | {tableList}     |

**pars object 參數**
| 參數名稱            | 名稱 | 型態   | 來源資料 & 說明 |
|---------------------|------|--------|-----------------|
| itemNo+資料序號     |      | string |                 |
| totalQty+資料序號   |      | string |                 |
| batchQty+資料序號   |      | string |                 |
| batchPack+資料序號  |      | string |                 |
| remainQty+資料序號  |      | string |                 |
| remainPack+資料序號 |      | string |                 |
| remark              |      | string |                 |
| pgm+資料序號        |      | string |                 |
| today               |      | string |                 |
| patternNo           |      | string |                 |
| woNo                |      | string |                 |
| pageDesc            |      | string |                 |
| ISO_DESC            |      | string |                 |
| LOGO                |      | string |                 |
| ISO_PROD_OPT        |      | string |                 |


**jrds object 參數**
| 參數名稱 | 名稱 | 資料型別 | 資料儲存 & 說明 |
|----------|------|----------|-----------------|
| rowNo    |      | string   |                 |