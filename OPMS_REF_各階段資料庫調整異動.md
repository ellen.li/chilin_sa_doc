[返回總覽](./OPMS_00.總覽.md)  
待辦

# 各階段資料庫調整異動
## A.系統管理
* 需重建使用者權限對應(因為原先 root 節點變成需包含在權限清單內)(雅姿)
* 使用者原先只有查詢權限或只有維護 需做轉換
* 新增 OPMS_PARAMS
## B.色號維護
  到時候需補 PAT_UPLOAD  順便補  PAT_H 避免上傳有問題，存檔被刪除
## C.BOM維護
  新增 OPMS_PARAMS BOM
## D.工單維護
  新增 OPMS_PARAMS WO
