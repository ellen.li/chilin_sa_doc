# 生產排程-寄Iepc通知
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容                                             | 修改者 |
|----------|------------------------------------------------|--------|
| 20230906 | 新增規格                                         | Shawn  |
| 20231016 | 新版 SYS_USER_ROLE -> SYS_USER_ROLE_V3          | Nick   |
| 20231128 | 副本收件者，使用者信箱如果是空白或null，忽略使用者 | Nick   |



## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    | /ps/send_iepc_notice |
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位      | 名稱     | 資料型別 | 必填  | 資料儲存 & 說明 |
| --------- | -------- | :------: | :---: | --------------- |
| opEquipId | 機台代號 |  string  |   M   |                 |
| opItemNo  | 料號     |  string  |   M   |                 |
| opPatternNo   | 色號  |  string  |   M   |                 |
| opMfgDate   | 生產日期  |  string  |   M   |                 |



#### Request 範例

```json
{
  "opEquipId": "1",
  "opItemNo": "757XXXXXX1DFH95429",
  "opPatternNo": "H95429B5",
  "opMfgDate":"2022/01/02"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 發送email通知：
  * 取得信件內容：
    * 執行SQL-1，取得機台資訊
    * 執行SQL-2，查詢產能對照需求email
    * 取Eamil模板檔 IEPC-NOTICE.ftl 參考下方email模板檔輸入參數，將資料帶入並發信。
  * 發信：
      * 收件者：SQL-2 的 EMAIL
      * 副本收件者：取登入者的 SYS_USER.EMAIL(信箱是空白或 null，則略過該使用者)
      * 信件標題：[OPMS]產能資料新增需求:{opItemNo}
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200。

# Request 後端邏輯說明

SQL-1: 取得機台資訊
```sql
SELECT 
  E.EQUIP_NAME
  P.SOC_NOTICE_ROLE_ID
FROM
  EQUIP_H E
  LEFT JOIN PRODUNIT P ON E.ORGANIZATION_ID = P.ORGANIZATION_ID AND E.PRODUNIT_NO = P.PRODUNIT_NO
WHERE
  E.EQUIP_ID = {opEquipId}
```

SQL-2: 取得產能對照需求email
```sql
SELECT DISTINCT
  U.EMAIL 
FROM
  SYS_USER U
  LEFT JOIN SYS_USER_ROLE_V3 R ON U.USER_ID = R.USER_ID 
WHERE
  R.ROLE_ID = {SOC_NOTICE_ROLE_ID(SQL-1)}
```

**Email模板檔 SOC-NEW-NOTICE.ftl 輸入參數**
|           | 型態   | 參數值            | 說明 |
| --------- | ------ | ----------------- | ---- |
| equipName | string | EQUIP_NAME(SQL-1) |      |
| itemNo    | string | opItemNo          |      |
| patternNo     | string | opPatternNo           |      |
| mfgDate     | string | opMfgDate          |      |



# Response 欄位

* 無

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":null
  }
}
```