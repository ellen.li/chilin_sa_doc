# 報表-配色作業日報表-匯出excel
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230818 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                      |
|--------|--------------------------|
| URL    | /report/daily_report_list_excel|
| method | post                     |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱    |  資料型別 | 必填 | 資料儲存 & 說明 |
|--------------|---------|----------|:----:|-----------------|
| 原列表REQUEST參數 |     |          |      |                 |


# Request 後端流程說明

* 參考[OPMS_J2.報表-配色作業日報表-列表 /report/pgm_monthly_list](./OPMS_J2.報表-配色作業日報表-列表.md)，用相同方式取得資料。
* 將資料轉為excel表格匯出，不需設定檔名。
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

參考[OPMS_J2.報表-配色作業日報表-列表 /report/pgm_monthly_list](./OPMS_J2.報表-配色作業日報表-列表.md)

# Excel 欄位

| 欄位        | 資料型別 | 資料儲存 & 說明                          |
|-------------|---------|----------------------------------------|
| 日期        | string  | WO_H.WO_DATE  yyyy/mm/dd                |
| 序號        | string  | WO_H.WO_SEQ                             |
| Lot No      | string  | WO_H.LOT_NO                             |
| 色號        | string  | ITEM_H.PATTERN_NO                       |
| 客戶        | string  | WO_H.CUST_NAME                          |
| 原料投入量   | string  | WO_H.RAW_QTY  取小數點兩位，轉千分位格式  |
| 工單號碼    | string  | WO_H.WO_NO                               |
| 每Batch用量 | string  | WO_H.BATCH_QTY 取小數點兩位，轉千分位格式  |
| 次數        | string  | WO_H.BATCH_TIMES                         |
| 生產單位    | string  | PRODUNIT.PRODUNIT_NAME                   |
| 備註        | string  | WO_H.REMARK                              |