# LOT編碼-產生LOT編碼
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

LOT編碼-產生LOT編碼

## 修改歷程

| 修改時間 | 內容         | 修改者 |
| -------- | ------------ | ------ |
| 20230830 | 新增規格     | Ellen  |
| 20231016 | 跳號指示只判斷特定數量 | Ellen  |


## 來源URL及資料格式

| 項目   | 說明        |
| ------ | ----------- |
| URL    | /lot/gen_no |
| method | post        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。

| 欄位      | 名稱 | 資料型別 | 必填 | 資料儲存 & 說明 |
| --------- | ---- | -------- | ---- | --------------- |
| opMfgDate | 日期 | string   | M    | yyyy/MM/dd      |
| opEquipId | 機台 | string   | M    |                 |


#### Request 範例

```json
{
  "opMfgDate":"2022/11/03",    
  "opEquipId":"1",
}
```

# Request 後端流程說明

可參考舊版 PsManageAction.genLotNo
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 取得預設LOT，判斷{opMfgDate}
  * 大於2021年1月1日 使用新編碼規則：年2碼＋產線3碼＋月數字1~C 1碼＋日2碼，執行 SQL-1 
  * 否則使用舊規則：年1碼＋產線3碼＋月1碼＋日2碼，執行 SQL-2
* 執行 SQL-3，取得需要編lot的已排程工單，過濾掉WO_NO重複的工單
* 遍歷(SQL-3)結果 組LotNo，新增流水號變數{seq}從1開始
  * 預設一個工單一個LOT
  * 若生產指示幾噸跳號： {LOT_OPT}等於`每{幾噸}噸跳號`，LOT數量為`ceil({工單數量WO_QTY}/({幾噸}*1000))`
  * 若生產指示幾板幾包跳號： {LOT_OPT}等於`Lot No.每{幾板}板跳號`、{PKG_OPT}包含`{幾包}打包`，LOT數量為`ceil({工單數量WO_QTY}/{幾包}/{幾板})`
  * 每張工單最多展3個LOT
  * 工單編號{lotNo}等於 `預設LOT{LOT_PRE(SQL-1)}加上{seq}`，{seq}依次遞增
  * 若{USE_MTR_FLAG}等於N，紙袋LOT{lotNoMtr}等於工單編號{lotNo}，否則為空字串
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

| 幾噸 |
| ---- |
| 2.1  |
| 2.4  |
| 5    |
| 10   |
| 16   |
| 35   |

| 幾板 |
| ---- |
| 10   |
| 12   |

| 幾包 |
| ---- |
| 500  |
| 700  |
| 800  |
| 875  |
| 1000 |

# Request 後端邏輯說明

SQL-1: 新編碼規則：取得預設LOT
```sql
SELECT
  {opMfgDate年份末兩碼} + E.LOT_CODE +
  CASE
    D.PARA_VALUE 
    WHEN 10 THEN 'A' 
    WHEN 11 THEN 'B' 
    WHEN 12 THEN 'C' 
    ELSE CONVERT ( VARCHAR, CAST( D.PARA_VALUE AS FLOAT ) ) END + {opMfgDate日期} 
  AS LOT_PRE 
FROM
  EQUIP_H E
  CROSS JOIN SYS_PARA_D D 
WHERE
  D.PARA_ID = 'LOT_MON' 
  AND D.PARA_VALUE = {opMfgDate月份}
  AND E.EQUIP_ID = {opEquipId}
```

SQL-2: 舊編碼規則：取得預設LOT
```sql
SELECT
  SUBSTRING( PS.MFG_DATE, 4, 1 ) + E.LOT_CODE + D.PARA_CODE + SUBSTRING( PS.MFG_DATE, 9, 2 ) AS LOT_PRE
FROM
  (SELECT {opMfgDate} AS MFG_DATE) AS PS
  INNER JOIN SYS_PARA_D D ON SUBSTRING( PS.MFG_DATE, 6, 2 ) = D.PARA_VALUE
  CROSS JOIN EQUIP_H E
WHERE
  D.PARA_ID = 'LOT_MON' 
  AND E.EQUIP_ID = {opEquipId}
```

SQL-3: 取得要編lot的已排程工單
```sql
SELECT
  PS.MFG_DATE,
  PS.SHIFT_CODE,
  PS.PS_SEQ,
  PS.EQUIP_ID,
  PS.ITEM_NO,
  PS.WO_UID,
  PS.WO_NO,
  H.WO_QTY,
  REPLACE ( S1.OPT_DESC, 'Lot No.', '' ) LOT_OPT,
  S2.OPT_DESC PKG_OPT,
  REPLACE ( S3.OPT_DESC, '外袋Lot No.:', '' ) PKG_LOT_OPT,
  CASE 
    WHEN S3.OPT_DESC LIKE '%奇美%' THEN 'Y'
    WHEN S3.OPT_DESC LIKE '%原料%' THEN 'Y'
    ELSE 'N' END AS USE_MTR_FLAG
FROM
  PS_SCHEDULE_V PS
  INNER JOIN WO_H H ON PS.WO_UID = H.WO_UID
  -- 標籤LOT跳號規則
  LEFT JOIN ( SELECT WO_UID, OPT_DESC FROM WO_SIS WHERE LIST_HEADER_ID = 110 ) S1 ON PS.WO_UID = S1.WO_UID
  -- 棧板規則
  LEFT JOIN ( SELECT WO_UID, OPT_DESC FROM WO_SIS WHERE LIST_HEADER_ID = 92 ) S2 ON PS.WO_UID = S2.WO_UID
  -- 外袋LOT規則
  LEFT JOIN ( SELECT WO_UID, OPT_DESC FROM WO_SIS WHERE LIST_HEADER_ID = 90 ) S3 ON PS.WO_UID = S3.WO_UID 
WHERE
  PS.WO_UID IS NOT NULL 
  AND PS.ORGANIZATION_ID = '5000' 
  AND PS.MFG_DATE = {opMfgDate}
  AND PS.EQUIP_ID = {opEquipId}
ORDER BY
  PS.MFG_DATE, PS.SHIFT_CODE, PS_SEQ
```

# Response 欄位
| 欄位         | 名稱            | 資料型別 | 資料儲存 & 說明          |
| ------------ | --------------- | -------- | ------------------------ |
| opWoUid      | 工單ID          | string   | WO_UID(SQL-3)            |
| opWoNo       | 工單號碼        | string   | WO_NO(SQL-3)             |
| opWoQty      | 工單數量        | string   | WO_QTY(SQL-3)            |
| opMfgDate    | 日期            | string   | {opMfgDate}              |
| opEquipId    | 機台            | string   | {opEquipId}              |
| opItemNo     | 料號            | string   | ITEM_NO(SQL-3)           |
| opLotNo      | 標籤LOT         | string   | {lotNo}                  |
| opLotNoMtr   | 紙袋LOT         | string   | {lotNoMtr}               |
| opLotOpt     | 標籤LOT跳號規則 | string   | LOT_OPT(SQL-3)           |
| opPkgLotOpt  | 紙袋LOT規則     | string   | PKG_LOT_OPT(SQL-3)       |
| opPkgOpt     | 棧板規則        | string   | PKG_OPT(SQL-3)           |
| opUseMtrFlag |                 | string   | USE_MTR_FLAG(SQL-3)  Y/N |


## Response 範例

```json
{
    "msgCode": null,
    "result": {
      "content":[
        {
          "opWoUid": "A84F12D2-012C-45D2-9A2B-CFD1EE76C9BC",
          "opWoNo": "51029576",
          "opWoQty": "6055.9",
          "opMfgDate": "2022/11/03",
          "opEquipId": "1",
          "opItemNo": "LPA747XX1XTAH13903",
          "opLotNo": "22T01B031",
          "opLotNoMtr": "22T01B031",
          "opLotOpt": "每工單跳號",
          "opPkgLotOpt": "編奇菱Lot No",
          "opPkgOpt": "1000瑞健太空包,瑞健綠色棧板+下紙板",
          "opUseMtrFlag": "N",
        }
      ]
    }
}
```
