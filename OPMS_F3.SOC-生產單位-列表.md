# SOC-生產單位-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230828 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /soc/produnit_list          |
| method | post                        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             |  名稱    |  資料型別 | 必填 | 資料儲存 & 說明  |
|------------------|---------|----------|:----:|-----------------|
| opOrganizationId | 工廠別   | string   |  M   | 5000:奇菱 / 6000:菱翔 |
| opProdType       | 產品型態 | string   |  M   | C:單軸 / M:雙軸 / B:壓板 |

#### Request 範例

```json
{
  "opOrganizationId": "5000",
  "opProdType": "C"
}
```
# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 參考SQL-1取得資料並回傳。
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
-- ProdunitDao.getProdunitListByProdType
SELECT P.* FROM PRODUNIT P
WHERE P.ORGANIZATION_ID = { opOrganizationId }
AND P.PRODUNIT_NO IN (SELECT PRODUNIT_NO FROM EQUIP_H WHERE PROD_TYPE = { opProdType })
ORDER BY PRODUNIT_NAME
```

# Response 欄位
| 欄位               | 名稱              | 資料型別 | 資料儲存 & 說明         |
|--------------------|------------------|---------|------------------------------|
| opProdunitNo       | 生產單位代碼      | string  | PRODUNIT.PRODUNIT_NO         |
| opProdunitName     | 生產單位名稱      | string  | PRODUNIT.PRODUNIT_NAME       |
| opCdt              | 建立日期          | string  | PRODUNIT.CDT                 |
| opCreateBy         | 建立人員          | string  | PRODUNIT.CREATE_BY           |
| opIepcNoticeRoleId | 產能需求通知角色ID | string  | PRODUNIT.IEPC_NOTICE_ROLE_ID |
| opSapProdunit      |                  | string  | PRODUNIT.SAP_PRODUNIT        |
| opSocNoticeRoleId  |                  | string  | PRODUNIT.SOC_NOTICE_ROLE_ID  |


#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":[
    	{
    		"opProdunitNo":"P3",
    		"opProdunitName":"染色課",
    		"opCdt":"2015-01-01 17:07:30",
    		"opCreateBy":"A000546",
    		"opIepcNoticeRoleId":"901",
    		"opSapProdunit":"211010",
    		"opSocNoticeRoleId":"511",
    	},
    	{
    		"opProdunitNo":"A01",
    		"opProdunitName":"樣品課",
    		"opCdt":"2015-01-01 17:07:30",
    		"opCreateBy":"A000546",
    		"opIepcNoticeRoleId":"901",
    		"opSapProdunit":"211040",
    		"opSocNoticeRoleId":"511",
    	}
    ]
  }
}
```


