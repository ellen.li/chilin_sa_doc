# 客訴-取得客訴案件進度列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得客訴案件進度列表

## 修改歷程

| 修改時間 | 內容                                                                              | 修改者 |
|----------|---------------------------------------------------------------------------------|--------|
| 20230627 | 新增規格                                                                          | Sam    |
| 20230801 | response 每筆客訴資料 新增 opCsUid 欄位，新增 opOrder 欄位                         | Sam    |
| 20230801 | 新增 opProcessFieldFilter 欄位過濾器，opProgress 處裡進度，opSalesUserName 負責業務 | Sam    |
| 20230801 | 新增 opEditReply 顯示可否終報回覆，opEditClose 顯示可否結案                        | Sam    |
| 20230814 | opOrder 無作用調整                                                                | Nick   |
| 20230816 | 調整 TOP 1000(使用OFFSET FETCH)                                                   | Nick   |
| 20231016 | 新版 SYS_USER_ROLE -> SYS_USER_ROLE_V3 | Nick   |

## 來源URL及資料格式

| 項目    | 說明                     |
| ------ | ----------------------- |
| URL    | /cs/list                |
| method | post                    |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。  
|        欄位         |        名稱      |     資料型別    | 必填(非必填節點不需存在) | 來源資料 & 說明|  
|  ----------------  |  --------------- | ------------- |:-------: | :--------------: |
|  opOrganizationId  |   工廠別          |  string       | O      | 預設 `null`，CLT:5000，LS:6000         |  
|  opCustName        |   客戶名稱        |   string      | O      | 預設 `null`         |  
|  opCsGrade         |   風險           |   string      | O      | 3/2/1，預設 `null`   |  
|  opStatus          |   狀態           |   string      | O      | Y/N，預設 `null`     |  
|  opActiveFlag      |   立案與否        |   string      | O      | Y/N，預設 `null`     |  
|  opClaimFlag       | 是否為客訴抱怨/案件類別 |  string       | O      | Y/N，預設 `null`     |
|  opCsItemType      |   產品分類        |  string       | O      | 預設 `null`         |
|  opSalesUserId     |   業務員          |  string       | O      | 預設 `null`         |
|  opReportUserId    |   責任者          |  string       | O      | 預設 `null`         |
|  opPatternNo       |   色號            |  string       | O      | 預設 `null`         |
|  opCsNo            |   單號/案件編號     |  string       | O      | 預設 `null`         |
|  opOrder           |   排序方式         |  string       | O      | 預設`DESC`，"ASC":升冪 /"DESC":降冪         |
|  opBDate           |   開始時間         |  string       | O      | 預設 `null` YYYY/MM/DD |
|  opEDate           |   結束時間         |  string       | O      | 預設 `null` YYYY/MM/DD |
|  opProcessFieldFilter |   處理進度過濾器         |  object       | O      | 預設 `null`，表示全部，帶入哪個值表示需要取那個值 |
|  opPage            |   第幾頁         |  integer      | O      | 預設`1`，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推|  
|  opPageSize        |   分頁筆數        | integer       |  O     | 預設 `10`           |  


【opProcessFieldFilter array string】
| 欄位            | 名稱         | 資料型別   | 來源資料 & 說明                               |
|----------------|--------------|----------|---------------------------------------------|
| opFilterStatus | 欄位名稱      | array    | 預設為`null`表示全選，其餘將欲搜尋的狀態加入此list中提供後端加入SQL語句內        |


#### Request 範例

```json
{
   "opOrganizationId":"5000",
   "opCustName":null,
   "opCsGrade":null,
   "opStatus":null,
   "opActiveFlag":"Y",
   "opClaimFlag":"Y",
   "opCsItemType":null,
   "opSalesUserId":null,
   "opReportUserId":null,
   "opPatternNo":null,
   "opCsNo":null,
   "opOrder":"DESC",
   "opBDate":"2023/06/03",
   "opEDate":"2023/07/19",
   "opProcessFieldFilter":{
      "opFilterStatus":["A","B","C","D","Z"]
      },
   "opPage":1,
   "opPageSize":10
}
```


# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 使用前端給的條件，參考 SQL-1 自組【搜尋條件】(使用 AND 開頭，方便與原WHERE 條件串接)，如為 null 代表不需組條件
* {offset} 為起始列印 row ，第一頁為 0，第二頁後為 0 + (opPageSize*(opPage-1))
* 參考 SQL-1 查回結果 result1
* 單號/案件編號 {opCsNo}
* 是否為客訴/案件類別 {opClaimFlag} : null或"":"未定義" ; "Y":客訴案件 ; "N":客戶反應
* 主旨 {opCsSubject} : CS_SUBJECT
* 處理進度 {opProgress} : 自定義欄位，需要做額外判斷，C.STATUS 欄位為 String 字串類型
  查詢 OPMS_PARAMS table，P.TYPE = 'CS' AND P.PARAM_ID = 'CSTATUS' AND OPT_VALUE = {C.STATUS}
    
    0.  若 C.STATUS 值 < 'D'但不為 null (即處理進度在 "終報審核完成" 之前)，前端 "操作" 欄位都可以顯示編輯icon
    1.  若 C.STATUS 值為 null ，則回傳 "NA"
    2.  若 C.STATUS 值為 "0" ，則回傳 "立案申請"
    3.  若 C.STATUS 值為 "1" ，則回傳 "立案初審"
    4.  若 C.STATUS 值為 "2" ，則回傳 "立案審核(CS)"
    5.  若 C.STATUS 值為 "3" ，則回傳 "立案審核(處)"
    6.  若 C.STATUS 值為 "4" ，則回傳 "指派成員"
    7.  若 C.STATUS 值為 "5" ，則回傳 "初步分析"
    8.  若 C.STATUS 值為 "A" ，則回傳 "分析真因對策"
    9.  若 C.STATUS 值為 "B" ，則回傳 "終報彙總"
    10. 若 C.STATUS 值為 "C" ，則回傳 "終報審核"
    11. 若 C.STATUS 值為 "D" ，則回傳 "終報審核完成" (前端 "操作" 欄位提示文字為 "終報回覆")
    12. 若 C.STATUS 值為 "E" ，則回傳 "回覆客戶"    (前端 "操作" 欄位提示文字為 "結案")
    13. 若 C.STATUS 值為 "Z" ，則回傳 "結案"
    14. 若 C.STATUS 值為 "X" ，則回傳 "作廢"

* 客戶名稱 {opCustName}
    若是 DEST_CUST_NAME 為 null 或是空字串，則直接回傳 ({C.CUST_NAME})
    若是 DEST_CUST_NAME 不為 null，則客戶名稱需加上 ({C.CUST_NAME}) + ({C.DEST_CUST_NAME})

* 立案時間 {opActiveDate} : ACTIVE_DATE
* 負責業務 {opSalesUserName} : SALES_USER_NAME

* 風險 {opCsGrade} : 3:高 ; 2:中 ; 1:低


* {USER_ID} = 目前登入者使用者ID
* {ROLE_ID} : {CS專用角色代號}
  ROLE_ID 根據 {opOrganizationId} 決定
  若 {opOrganizationId} 為 '5000'，則 roleId 為 '921'
  否則為 roleId = '922' 

* 權限部分 : 參考 SQL-3 
  - (A) 是否為 CS : 如果 SQL-3 的結果 CNT>0 則為 CS

  <!-- 原程式邏輯
    - (B) 可否編輯申請單 
			//可否編輯申請單
			if ((to.getSalesUserId().equals(curUser.getUserId())||isCs) && to.getStatus().compareTo("Z")<0) editApply="Y"; 
	-->

  <!-- 原程式邏輯
    - (C) 可否編輯真因
			if (to.getStatus().compareTo("Z")<0 && ((to.getReportUserId()!=null && to.getReportUserId().equals(curUser.getUserId())) || (to.getUunB()!=null && to.getUunB().equals(curUser.getUserName()))) )
				editDiag="Y";
			if (!to.getStatus().equals("Z")) readOnly="N";
	 -->

  <!-- 原程式邏輯
    - 可否寫品保報告 
			if (("5,A,B,C".indexOf(to.getStatus())>=0) && (to.getReportUserId().equals(curUser.getUserId()) || (to.getUunB()!=null && to.getUunB().equals(curUser.getUserName()))))
					editReport="Y"; 
  -->
	
  - 可否終報回覆 opEditReply
      
      預設 opEditReply = 'N'，如果為 null 也設為 'N'
			若 {STATUS} = 'D' 且 {登入使用者 userId} = {SALES_USER_ID} 或者為 CS (滿足上述條件 A)
      則 opEditReply = 'Y' (表示 可 顯示終報回覆)
      若 opEditReply = 'Y' 且 {登入使用者 userId} = {SALES_USER_ID} 或者為 CS，但 {STATUS} = 'E' 
      則 opEditReply = 'N' (表示 不可 顯示終報回覆)

      <!-- 原程式邏輯
      if (editReply==null) editReply="N";
			else{
				//終報審核完成 且 身份為業務或CS 才能回覆客戶
				if (to.getStatus().equals("D") && (to.getSalesUserId().equals(curUser.getUserId()) || isCs)){
					editReply="Y";
				}
				else if (editReply.equals("Y")){
					request.setAttribute("serverMessage", CsParaDesc.getCsStatusDesc("D")+" 且 身份為業務或CS 才能執行 "+CsParaDesc.getCsStatusDesc("E"));
					editReply="N";
				}
			} -->

	- 可否結案

      預設 opEditClose = 'N'，如果為 null 也設為 'N'
      若 {STATUS} = 'E' 且 {登入使用者 userId} = {REPORT_USER_ID}
      則 opEditClose = 'Y' (表示 可 顯示結案)
      若 opEditClose = 'Y' 且 {登入使用者 userId} = {REPORT_USER_ID} ，但 {STATUS} = 'Z' 
      則 opEditClose = 'N' (表示 不可 顯示結案)

      <!-- 原程式邏輯
      if (editClose==null) editClose="N";
			else{
				//已回覆客戶 且 身份為PM才能結案
				if (to.getStatus().equals("E")&& to.getReportUserId().equals(curUser.getUserId())) editClose="Y";
				else if (editClose.equals("Y")){
					request.setAttribute("serverMessage", CsParaDesc.getCsStatusDesc("E")+" 且 身份為PM才能執行 "+CsParaDesc.getCsStatusDesc("Z"));
					editClose="N";
				} 
      -->

* 組成 JSON 格式
* Response 結果需含分頁資訊，無資料 pageInfo 裡面項目皆給 0，頁數與分頁筆數前端如有提供，依前端判斷給的資料計算相關數值及拆分頁面
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
-- getCsManageList /cs/AjaxQueryCsManageList.action --
SELECT * FROM(

       SELECT 
           C.CS_UID, --opCsUid
           C.CS_NO,  -- 單號/案件編號 v
           C.CS_SUBJECT, -- 主旨 
           C.CS_GRADE, -- 風險 
           -- C.UNIT, 
           C.CUST_NAME, -- 客戶簡稱/客戶名稱 v
           C.DEST_CUST_NAME, -- 終端客戶名稱 v
           -- C.SALES_QTY, 
           -- C.GRADE_NO,
           -- C.PATTERN_NO,
           C.SALES_USER_ID, --業務員 ID (判斷邏輯用)
           C.REPORT_USER_ID, --PM ID (判斷邏輯用)
           U1.USER_NAME SALES_USER_NAME, -- 負責業務 v
           -- U2.USER_NAME REPORT_USER_NAME, 
           -- C.ACTIVE_FLAG, 
           C.STATUS, 
           -- C.ORGANIZATION_ID, 
           -- C.WF_INS_ID, 
           -- CONVERT(VARCHAR(10),C.CDT,111) AS CDT, 
           CONVERT(VARCHAR(10),C.ACTIVE_DATE,111) AS ACTIVE_DATE, -- 立案時間 v
           -- CONVERT(VARCHAR(10),C.UDT_A,111) AS UDT_A, C.UUN_A, 
           -- CONVERT(VARCHAR(10),C.UDT_B,111) AS UDT_B, C.UUN_B, 
           -- CONVERT(VARCHAR(10),C.UDT_C,111) AS UDT_C, C.UUN_C, 
           -- CONVERT(VARCHAR(10),C.UDT_D,111) AS UDT_D, C.UUN_D, 
           -- CONVERT(VARCHAR(10),C.UDT_E,111) AS UDT_E, C.UUN_E, 
           -- CONVERT(VARCHAR(10),C.UDT_F,111) AS UDT_F, C.UUN_F, 
           -- CONVERT(VARCHAR(10),C.UDT_Z,111) AS UDT_Z, C.UUN_Z, 
           -- CONVERT(VARCHAR(10),C.CS_REPORT_DATE, 111) AS CS_REPORT_DATE, C.FINAL_DIAG_FLAG, 
           -- C.FIRST_REPORT_FLAG,CONVERT(VARCHAR(10),C.FIRST_REPORT_DATE,111) AS FIRST_REPORT_DATE, 
           -- C.CS_ITEM_TYPE, C.DF_A,C.CS_REFUND_FLAG, C.CS_REFUND_AMOUNT,C.CS_REFUND_CURRENCY,
           C.CLAIM_FLAG,  -- 案件類別 v
           -- C.CLAIM_DESC, 
           P.OPT_DISPLAY AS PROGRESS -- v 處理進度
       FROM CS_H C 
       LEFT JOIN SYS_USER U1 
       	ON C.SALES_USER_ID = U1.USER_ID -- join 出 負責業務
       LEFT JOIN SYS_USER U2 
       	ON C.REPORT_USER_ID = U2.USER_ID -- join 出 PM 
       LEFT JOIN OPMS_PARAMS P  --查詢 OPMS_PARAMS table
       	ON (P.TYPE = 'CS' AND P.PARAM_ID = 'CSTATUS') AND P.OPT_VALUE = {C.STATUS}
       WHERE 1=1     	
       -- 工廠別(site) : {opOrganizationId} 如果不為 null，則加上這句 SQL
       AND C.ORGANIZATION_ID = {opOrganizationId}
       
       -- 客戶名稱 : {opCustName} 如果不為 null，則加上這句 SQL
       AND C.CUST_NAME like %{opCustName}%
       
       -- 風險 : {opCsGrade} 如果不為 null，則加上這句 SQL
       AND C.CS_GRADE = {opCsGrade} 
       
       -- 狀態 : {opStatus} 如果不為 null 
       -- {opStatus} 如果不為 null 且 為 "Y"，則加上這句 SQL
       AND C.STATUS = 'Z' 
       -- {opStatus} 如果不為 null 且 為 "N"，則加上這句 SQL
       AND C.STATUS <> 'Z' 
       
       -- 立案與否 : {opActiveFlag} 如果不為 null 
       -- {opActiveFlag} 如果不為 null 且 為 "Y"，則加上這句 SQL
       AND C.ACTIVE_FLAG='Y'
       -- {opActiveFlag} 如果不為 null 且 為 "N"，則加上這句 SQL
       AND C.ACTIVE_FLAG='N' 
       		
       -- 是否為客訴/案件類別 : {opClaimFlag} 如果不為 null 且為 "Y" 或 "N"
       -- {opClaimFlag} 如果不為 null 且 為 "Y"，表示為客訴案件，則加上這句 SQL
       AND C.CLAIM_FLAG = 'Y' 
       -- {opClaimFlag} 如果不為 null 且 為 "N"，表示為客戶反應，則加上這句 SQL
       AND C.CLAIM_FLAG = 'N' 
       
       -- 產品分類 : {opCsItemType} 如果不為 null，則加上這句 SQL
       AND C.CS_ITEM_TYPE = {opCsItemType}
       
       -- 業務員 : {opSalesUserId} 如果不為 null，則加上這句 SQL
       AND C.SALES_USER_ID = {opSalesUserId}
       
       -- 責任者 : {opReportUserId} 如果不為 null，則加上這句 SQL
       AND C.REPORT_USER_ID = {opReportUserId} 
       		
       -- 色號 : {opPatternNo} 如果不為 null，則加上這句 SQL
       AND C.PATTERN_NO = {opPatternNo}
       
       -- 單號 : {opCsNo} 如果不為 null，則加上這句 SQL
       AND C.CS_NO like %{opCsNo}%
       
       -- 立案日期 : 
       -- {opBDate} 不為 null
       AND C.CDT >= CONVERT(DATETIME,opBDate)
       -- {opEDate} 不為 null
       AND C.CDT <= CONVERT(DATETIME,opEDate)
       
       -- 若 opFilterStatus 不為 null 且 list 內有值，則將下列語句加入SQL中
       AND C.STATUS IN ({opFilterStatus}) 
       
       --{opOrder} 為 null ，則加入下面語法，預設使用【建立時間】排序
       ORDER BY  CDT DESC 
       
       --{opOrder} 不為 null ，則加入下面語法，使用【立案時間】排序
       ORDER BY  ACTIVE_DATE {opOrder} 
       
       OFFSET 0 ROWS FETCH NEXT 1000 ROWS ONLY

    ) AS TOP1000T
ORDER BY (SELECT NULL) OFFSET { offset } ROWS FETCH NEXT { opageSize } ROWS ONLY
```

SQL-2:

```sql
--取得所有客訴列表中，業務員ID與姓名(排掉重複)，讓前端可供下拉選取
select 
  distinct U1.USER_ID ,
           U1.USER_NAME AS SALES_USER_NAME
FROM CS_H C
LEFT JOIN SYS_USER U1 
  ON (C.SALES_USER_ID=U1.USER_ID) --關聯欄位
```

SQL-3:

```sql
-- 確認是否為 CS
SELECT 
  COUNT(*) AS CNT -- CNT > 0 表示有該角色與user存在 
FROM SYS_USER_ROLE_V3 
WHERE (
  USER_ID={目前登入者使用者ID} 
AND ROLE_ID = decode({opOrganizationId},'5000','921','922') --5000 則 roleId 為 921，其餘為922
)
```

SQL-4:

```sql
-- 由 userid 確認是否為 業務

```

SQL-5:

```sql
-- 由 userid 確認是否為 PM

```

# Response 欄位
| 欄位         | 名稱                 | 資料型別 | 資料儲存 & 說明          |
| ------------ | ------------        | -------- | ------------------------ |
|pageInfo |分頁資訊| object|M||
|content |附帶訊息| array|M| |
#### 【pageInfo】child node
| 欄位| 名稱| 資料型別 | 必填 | 來源資料 & 說明|
| -- | --| -- | - | - |
pageNumber |目前所在分頁|int|M||
pageSize|分頁筆數大小|int|M||
pages|總分頁數|int|M||
|total|總資料筆數|int|M||

#### 【content】array
|      欄位        |       名稱         |    資料型別  | 必填(非必填節點不需存在) |         來源資料 & 說明           |
| --------------- | -------------------| ----------- | :------------------: | ------------------------------ |
|  opCsUid        |    csUid           |  string     |          M           |     CS_UID                    |
|  opCsNo         |   單號/案件編號      |  string     |          M           |     CS_NO                      |
|  opClaimFlag    |   案件類別          |  string     |          M           |     CLAIM_FLAG                 |
|  opCsSubject    |   主旨             |  string     |           M           |     CS_SUBJECT                |
|  opProgress     |   處理進度          |  string     |          M           | PROGRESS 自定義欄位，需要額外判斷  |
|  opCustName     |   客戶名稱          |   string    |          M           |    CUST_NAME                   | 
|  opActiveDate   |   最後更新日        |  string     |          M           |    ACTIVE_DATE ( YYYY/MM/DD )  |
| opSalesUserName |   負責業務          |  string     |          M           |    SALES_USER_NAME             |
| opEditReply     |   可否顯示終報回覆    |  string     |          M           |  'Y':表示可顯示終報回覆/'N':表示不可顯示終報回覆 自定義欄位，根據說明欄中定義填入   |
| opEditClose     |   可否顯示結案       |  string     |          M           |  'Y':表示可顯示結案/'N':表示不可顯示結案 自定義欄位，根據說明欄中定義填入   |

#### Response 範例



```json
{
    "msgCode": null,
    "result": {
        "pageInfo": {
        "pageNumber": 1, 
        "pageSize": 10,
        "pages": 1,      
        "total": 3     
        },
       "content": [
       {
         "opCsUid":"32D2C18B-EDFF-43E3-A7EE-DCC8A7866CFB",
         "opCsNo":"C2303003",
         "opClaimFlag":"客訴案件",
         "opCsSubject":"客戶反映A08840H8重量不足(太空包裝)",
         "opProgress":"指派成員",
         "opCustName":"奇美實業(CALSAK)",
         "opActiveDate":"2023/03/30",
         "opSalesUserName":"蔡O玲",
         "opEditReply":"N",
         "opEditClose":"N"
       },
       {
         "opCsUid":"32D2C18B-EDFF-43E3-A7EE-DCC8A7866CFC",
         "opCsNo": "C2302005",
         "opClaimFlag":"客訴案件",
         "opCsSubject":"經銷反映J-01-C1  PC-110貨品中夾帶一包A21A171C1 PC-115P",
         "opProgress":"終報彙總",
         "opCustName":"奇美實業",
         "opActiveDate":"2023/02/23",
         "opSalesUserName":"蔡O玲",
         "opEditReply":"N",
         "opEditClose":"N"
       },
       {
         "opCsUid":"BB595CB2-889B-494C-98F3-503D5A40940B",
         "opCsNo": "C2204001",
         "opClaimFlag":"未定義",
         "opCsSubject":"異色粒",
         "opProgress":"分析真因對策",
         "opCustName":"隆廷(大億交通)",
         "opActiveDate":"2022/04/07",
         "opSalesUserName":"林O賢",
         "opEditReply":"N",
         "opEditClose":"N"
       },
       {
         "opCsUid":"BB595CB2-889B-494C-98F3-503D5A40940C",
         "opCsNo": "C2107003",
         "opClaimFlag":"客戶反應",
         "opCsSubject":"色差",
         "opProgress":"終報審核完成",
         "opCustName":"富士康",
         "opActiveDate":"2021/07/28",
         "opSalesUserName":"林O賢",
         "opEditReply":"Y",
         "opEditClose":"N"
       },
       {
         "opCsUid":"8F70B47F-A2BF-4981-BCEF-07D16A88B384",
         "opCsNo": "C2103004",
         "opClaimFlag":"客戶反應",
         "opCsSubject":"塑景(綠點)J01-PC-345白霧不良客訴",
         "opProgress":"結案",
         "opCustName":"塑景(HTC)",
         "opActiveDate":"2021/04/01",
         "opSalesUserName":"李O良",
         "opEditReply":"N",
         "opEditClose":"Y"
       },
       {
         "opCsUid":"EE97AAC6-8928-4513-88DE-A3427F9E020A",
         "opCsNo": "C2104001",
         "opClaimFlag":"客訴案件",
         "opCsSubject":"健生(奇美)J956818B8反映異味",
         "opProgress":"結案",
         "opCustName":"奇美實業(健生)",
         "opActiveDate":"2021/04/12",
         "opSalesUserName":"李O良",
         "opEditReply":"N",
         "opEditClose":"Y"
       }
        ]
    }
}
```

