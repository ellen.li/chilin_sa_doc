# 已訂未交表-匯出excel
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**


## 修改歷程

| 修改時間   | 內容                                                               | 修改者 |
| ---------- | ------------------------------------------------------------------ | ------ |
| 20230809   | 新增規格                                                           | Ellen  |
| 20231102   | 葉大哥提多欄位調整為無條件捨去到整數 +  業_交期 改為顯示預計出貨日 | Nick   |
| 20231102-1 | 葉大哥提入庫量也需無條件捨去到整數                                 | Nick   |
| 20240119   | 增加指定機台欄位                                                   | Ellen |

## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    | /wops/list_excel |
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。  
| 欄位              | 名稱 | 資料型別 | 必填  | 來源資料 & 說明 |
| ----------------- | ---- | -------- | :---: | --------------- |
| 原列表REQUEST參數 | ...  | ...      |  ...  | ...             |


#### Request 範例

```json
{
  "opPsStatus": "U",
  "opProdunitNo": "211010",
  "opWoNo": "510",
  "opBDate": "2022/12/10",
  "opEDate": "2022/12/12",
  "opPage": 1,
  "opPageSize": 10,
  "opOrderField": "WO_DATE",
  "opOrder": "DESC"
}
```

# Request 後端流程說明

* 參考[OPMS_E1.已訂未交表-取得已訂未交列表](./OPMS_E1.已訂未交表-取得已訂未交列表.md) 以同樣方式取得資料，不需分頁。
* 參考[OPMS_共用_excel匯出](./OPMS_共用_excel匯出.md) 將資料製成Excel檔案回傳。
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

[OPMS_E1.已訂未交表-取得已訂未交列表](./OPMS_E1.已訂未交表-取得已訂未交列表.md)

# Excel 欄位

| Eexcel欄位名稱 | 資料型別 | 資料來源 | 資料欄位和說明                    |
|--------------|:--------:|----------|-----------------------------------|
| 狀態           |  string  | SQL-1    | PS_FLAG                           |
| 訂單日         |  string  | SQL-1    | WO_DATE(SQL-1)                    |
| 客戶           |  string  | SQL-1    | CUST_NAME                         |
| 色號           |  string  | SQL-1    | PATTERN_NO                        |
| 料號           |  string  | SQL-1    | ITEM_NO                           |
| 原料           |  string  | SQL-1    | MTR_NAME                          |
| 桶重           |  string  | SQL-1    | CTN_WEIGHT(無條件捨去取到整數位)  |
| 桶數           |  string  | SQL-1    | BATCH_TIMES(無條件捨去取到整數位) |
| 尾數           |  string  | SQL-1    | REMAIN_QTY(無條件捨去取到整數位)  |
| 訂單數量       |  string  | SQL-1    | SALES_QTY(無條件捨去取到整數位)   |
| 原料量         |  string  | SQL-1    | RAW_QTY(無條件捨去取到整數位)     |
| 工單           |  string  | SQL-1    | WO_NO                             |
| 業_交期        |  string  | SQL-1    | ESTIMATED_SHIP_DATE               |
| 現_交期        |  string  | SQL-1    | DD_MFG                            |
| 預排機台       |  string  | SQL-1    | PS_EQUIP_NAME                     |
| 適用機台       |  string  | SQL-1    | EQUIP_LIST                        |
| 指定機台       |  string  | SQL-1    | EQUIP_NAME                        |
| 備註           |  string  | SQL-1    | PS_REMARK                         |
| 入庫量         |  string  | SQL-1    | INV_QTY (無條件捨去取到整數位)      |
| SAP單號        |  string  | SQL-1    | CUST_PO_NO                        |
