# 押板-PDA-匯出pdf
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容                  | 修改者 |
| -------- | --------------------- | ------ |
| 20230907 | 新增規格              | 黃東俞 |
| 20231012 | 合併預覽和空白PDA邏輯 | Ellen  |

## 來源URL及資料格式

| 項目   | 說明           |
| ------ | -------------- |
| URL    | /pda/pda_b_pdf |
| method | post           |


## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             | 名稱   | 資料型別 | 必填  | 資料儲存 & 說明 |
| ---------------- | ------ | -------- | :---: | --------------- |
| opPdaUid         | PDA ID | string   |   O   | 列印PDA時需有值 |
| opSocUid         | SOC ID | string   |   O   | 預覽PDA時需有值 |
| opOrganizationId | 工廠別 | string   |   O   | 空白PDA時需有值 |

#### Request 範例

```json
{
  "opPdaUid": "5AF193B8-0F46-4012-BF5F-40A399AFDDC1"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 若 opPdaUid有值為列印PDA
  * 將opPdaUid帶入SQL-1取得[PDA資料]
  * [工廠別]=EQUIP_H.ORGANIZATION_ID(SQL-3)
  * [工單資料]：將[PDA資料的]WO_H.WO_NO和[工廠別]直接帶入SAP-1取得工單資料 (沒有去撈WO_H)
  * [SOC資料]：將[PDA資料]的SOC_B_UID帶入SQL-2  (和[OPMS_F3.押板-SOC管理-PDA](./OPMS_F3.押板-SOC管理-PDA.md)相同SQL)，並且使用FormatUtil將數字欄位去除小數點後多餘的0
  * pres:
    * 將[SOC資料]直接依SQL欄位名稱填入pres
    * today： 取系統時間 yyyy/mm/dd hh/mm/ss
    * title
      * [狀態]：SOC_B.STATUS(SQL-2)="W"時，狀態="(簽核中)",否則為空字串""
      * [製造日期]=PDA_C.MFG_DATE(SQL-1) 格式化為字串(yyyy/MM/dd E)，若PDA_C.MFG_DATE為空時，則改為"　　年　月　日 星期:"
      * title = 狀態 + "　日期:" + [製造日期]
    * SHIFT_CODE
      * 若 PDA_B.SHIFT_CODE(SQL-1)="A"時，填"早"
      * 若 PDA_B.SHIFT_CODE(SQL-1)="B"時，填"中"
      * 若 PDA_B.SHIFT_CODE(SQL-1)="C"時，填"晚"
    * WO_NO：取PDA_B.WO_NO(SQL-1)
    * WO_QTY：若 GAMNG(SAP-1)有值，取 GAMNG(取五位小數) + GMEIN(SAP-1)
    * 料號資料 PATTERN_NO, LENGTH, WIDTH, THICK
      * 若SOC_B.ITEM_NO(SQL-2)有值時才進行以下步驟
      * [訂單明細檔資料]：將KDAUF(SAP-1)和KDPOS(SAP-1)過濾前綴"0"之後，帶入SQL-3查詢    *
      * PATTERN_NO：取COLOR_NO(SAP-1) 若無值時填入空字串
      * LENGTH=SO_D.LENGTH(SQL-3)
      * WIDTH=SO_D.WIDTH(SQL-3)
      * THICK=SO_D.DEEP(SQL-3)
    * ISO_DESC
      * 工廠別為"5000"時設為 "保存期限:3年  CLT-35QAA0020  CLT-40QAA0073-Ver.08"
      * 工廠別不為"5000"時設為 "保存期限： 3年 LS-30QAA0012  LS-40QAA0281-Ver.01"
    * LOGO
      * 工廠別為"5000"時取圖檔路徑 "/jasperReport/logo.png"
      * 工廠別不為"5000"時取圖檔路徑 "/jasperReport/logoLSO.png"
    * VACUUM_UNIT
      * 工廠別為"5000"："cmHg"
      * 工廠別不為"5000"："mmHg"
  * jrds
    * 新增一筆資料 rowNo = "1"，TIME_S和TIME_E皆填入空字串""
* 若 opPdaUid為空：
  * opSocUid有值時為預覽PDA，否則為空白PDA，參考JASPER-1設定參數
    * pres
      * 預覽PDA時，參考SQL-4取得資料填入。若為空白PDA則不須撈取資料
      * 使用FormatUtil將數字欄位去除小數點後多餘的0
      * 加入其他固定參數值
        * title："日期:　　年　月　日　星期:"
        * WO_NO: 空字串""
        * PATTERN_NO：空字串""
      * 工廠別：預覽PDA時，取SQL-1的ORGANIZATION_ID，否則取request傳入的opOrganizationId
      * ISO_DESC
        * 工廠別為"5000"時設為 "保存期限:3年  CLT-35QAA0020  CLT-40QAA0073-Ver.08"
        * 工廠別不為"5000"時設為 "保存期限： 3年 LS-30QAA0012  LS-40QAA0281-Ver.01"
      * LOGO
        * 工廠別為"5000"時取圖檔路徑 "/jasperReport/logo.png"
        * 工廠別不為"5000"時取圖檔路徑 "/jasperReport/logoLSO.png"
      * VACUUM_UNIT
        * 工廠別為"5000"："cmHg"
        * 工廠別不為"5000"："mmHg"
    * jrds:
      * 新增一筆資料 rowNo = "1"，TIME_S和TIME_E皆填入空字串""
* jasper檔案位置：
  * 工廠別為"5000"時：/jasperReport/PDA_B.jasper
  * 工廠別不為"5000"時：/jasperReport/PDA_B_LS.jasper
* 有系統錯誤 return 500,"SYSTEM_ERROR"，正常回傳PDF檔

# Request 後端邏輯說明

SQL-1:

```sql
-- PdaQueryTo.getPdaBSingle
SELECT P.PDA_B_UID PDA_UID, S.SOC_B_UID SOC_UID, S.SOC_B_VER SOC_VER,S.EQUIP_ID, E.EQUIP_NAME, S.ITEM_NO,
P.WO_NO, CONVERT(VARCHAR(10),P.MFG_DATE,111) MFG_DATE, P.SHIFT_CODE,
CONVERT(VARCHAR(10),S.PUB_DATE,111) PUB_DATE, U1.USER_NAME PUBLIC_NAME, REPLACE(CONVERT(VARCHAR(19),P.CDT,120),'-','/') CDT, U2.USER_NAME CREATE_BY_NAME
FROM PDA_B P
INNER JOIN SOC_B S ON P.SOC_B_UID = S.SOC_B_UID
INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
LEFT JOIN SYS_USER U1 ON S.PUBLIC_BY = U1.USER_ID
LEFT JOIN SYS_USER U2 ON P.CREATE_BY = U2.USER_ID
WHERE P.PDA_B_UID = {opPdaUid}
```
SQL-2
```sql
-- SocBQueryDao.getSocBMap
SELECT E.ORGANIZATION_ID, S.EQUIP_ID,  E.EQUIP_NAME,  S.ITEM_NO,  S.SOC_B_UID,  S.SOC_B_VER,  S.STATUS,
  S.AC_1_STD, S.AC_1_MAX, S.AC_1_MIN, S.AC_2_STD, S.AC_2_MAX, S.AC_2_MIN, S.AC_3_STD, S.AC_3_MAX, S.AC_3_MIN,
  S.AC_4_STD, S.AC_4_MAX, S.AC_4_MIN, S.AC_5_STD, S.AC_5_MAX, S.AC_5_MIN, S.AC_6_STD, S.AC_6_MAX, S.AC_6_MIN,
  S.AC_7_STD, S.AC_7_MAX, S.AC_7_MIN, S.AC_8_STD, S.AC_8_MAX, S.AC_8_MIN, S.AC_9_STD, S.AC_9_MAX, S.AC_9_MIN,
  S.AC_A_STD, S.AC_A_MAX, S.AC_A_MIN,
  S.A_SCREW_STD,  S.A_SCREW_MAX,  S.A_SCREW_MIN,  S.A_CURR_STD,  S.A_CURR_MAX,  S.A_CURR_MIN,  S.A_GP_STD,  S.A_GP_MAX,  S.A_GP_MIN,
  S.BC_1_STD, S.BC_1_MAX, S.BC_1_MIN, S.BC_2_STD, S.BC_2_MAX, S.BC_2_MIN, S.BC_3_STD, S.BC_3_MAX, S.BC_3_MIN,
  S.BC_4_STD, S.BC_4_MAX, S.BC_4_MIN, S.BC_5_STD, S.BC_5_MAX, S.BC_5_MIN, S.BC_6_STD, S.BC_6_MAX, S.BC_6_MIN,
  S.BC_7_STD, S.BC_7_MAX, S.BC_7_MIN, S.BC_8_STD, S.BC_8_MAX, S.BC_8_MIN, S.BC_9_STD, S.BC_9_MAX, S.BC_9_MIN,
  S.BC_A_STD, S.BC_A_MAX, S.BC_A_MIN, S.BC_B_STD, S.BC_B_MAX, S.BC_B_MIN, S.BC_C_STD, S.BC_C_MAX, S.BC_C_MIN,
  S.BC_D_STD, S.BC_D_MAX, S.BC_D_MIN, S.BC_E_STD, S.BC_E_MAX, S.BC_E_MIN,
  S.B_SCREW_STD,  S.B_SCREW_MAX,  S.B_SCREW_MIN,  S.B_CURR_STD, S.B_CURR_MAX,  S.B_CURR_MIN,  S.B_GP_STD,  S.B_GP_MAX,  S.B_GP_MIN,
  S.D_1_STD, S.D_1_MAX, S.D_1_MIN, S.D_2_STD, S.D_2_MAX, S.D_2_MIN, S.D_3_STD, S.D_3_MAX, S.D_3_MIN,
  S.D_4_STD, S.D_4_MAX, S.D_4_MIN, S.D_5_STD, S.D_5_MAX, S.D_5_MIN, S.D_6_STD, S.D_6_MAX, S.D_6_MIN,
  S.D_7_STD, S.D_7_MAX, S.D_7_MIN,
  S.R_1_STD, S.R_1_MAX, S.R_1_MIN, S.R_2_STD, S.R_2_MAX, S.R_2_MIN, S.R_3_STD, S.R_3_MAX, S.R_3_MIN,
  S.R_SPEED_STD, S.R_SPEED_MAX, S.R_SPEED_MIN, S.R_GAP_L, S.R_GAP_R, S.VACUUM_INDEX, S.LAYER_1, S.LAYER_2, S.LAYER_3, S.MODEL_GAP, S.MODEL_WIDTH,
  S.MFG_DESC, S.SOC_DESC, S.REMARK, S.SCREEN_FREQ, S.RECORD_FREQ,
  S.LAYER_1_MAX, S.LAYER_1_MIN, S.LAYER_2_MAX, S.LAYER_2_MIN, S.LAYER_3_MAX, S.LAYER_3_MIN,
  S.VACUUM_INDEX_A_STD, S.VACUUM_INDEX_A_MAX, S.VACUUM_INDEX_A_MIN, S.VACUUM_INDEX_B_STD, S.VACUUM_INDEX_B_MAX,
  S.VACUUM_INDEX_B_MIN, S.SCREEN_LAYER_A, S.SCREEN_LAYER_B, S.SC_BP_A_STD, S.SC_BP_B_STD, S.SC_BP_A_MAX, S.SC_BP_B_MAX, S.SC_BP_A_MIN, S.SC_BP_B_MIN,
  S.MTR_DRY_TEMP_A, S.MTR_DRY_TEMP_B, S.MTR_DRY_TIME_A, S.MTR_DRY_TIME_B, S.MTR_MIX_TIME_A, S.MTR_MIX_TIME_B,
  S.R_U, S.R_M, S.R_D, S.R_TEN_STD, S.R_TEN_MAX, S.R_TEN_MIN,
  R_TEN_SLOPE_STD, R_TEN_SLOPE_MAX, R_TEN_SLOPE_MIN, R_TEN_SPEED_STD, R_TEN_SPEED_MAX, R_TEN_SPEED_MIN,
  FEEDP_A_STD, FEEDP_A_MAX, FEEDP_A_MIN, FEEDP_B_STD, FEEDP_B_MAX, FEEDP_B_MIN,
  REPLACE(CONVERT(VARCHAR(19), S.CDT, 120), '-', '/') CDT,
  S.CREATE_BY,
  CONVERT(VARCHAR(10), S.PUB_DATE, 111) PUB_DATE,
  S.PUBLIC_BY,
  REPLACE(CONVERT(VARCHAR(19), S.UDT, 120), '-', '/') UDT, S.UPDATE_BY, U.USER_NAME PUBLIC_NAME
FROM SOC_B S
INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
LEFT JOIN SYS_USER U
ON S.PUBLIC_BY = U.USER_ID
WHERE S.SOC_B_UID = {SOC_UID(SQL-1)}
```

SQL-3
```sql
-- SoQueryDao.getSoD
SELECT D.SO_NO, D.SO_SEQ, D.STATUS, D.ITEM_NO, D.ITEM_DESC, D.CUST_ITEM_NO, D.QTY, D.QTY, D.PATTERN_NO, D.COLOR, D.MTR_NAME, D.DEEP, D.WIDTH, D.LENGTH, D.SPEC, D.LBL_PATTERN_NO, D.LBL_COLOR, D.LBL_MTR_NAME, D.LBL_DEEP, D.LBL_WIDTH, D.LBL_LENGTH, D.LBL_SPEC, D.DD_CUSINV,
  D.ERP_FLAG, D.SIS_FLAG,REPLACE(CONVERT(VARCHAR(19),D.CDT,120),'-','/') CDT, D.CREATE_BY, REPLACE(CONVERT(VARCHAR(19),D.UDT,120),'-','/') UDT, D.UPDATE_BY
FROM SO_D D WHERE D.SO_NO = {KDAUF(SAP-1)}
AND D.SO_SEQ = {KDPOS(SAP-1)}
```

SQL-4:

```sql
-- SocBQueryDao.getSocBMap
SELECT E.ORGANIZATION_ID, S.EQUIP_ID,  E.EQUIP_NAME,  S.ITEM_NO,  S.SOC_B_UID,  S.SOC_B_VER,  S.STATUS,
  S.AC_1_STD, S.AC_1_MAX, S.AC_1_MIN, S.AC_2_STD, S.AC_2_MAX, S.AC_2_MIN, S.AC_3_STD, S.AC_3_MAX, S.AC_3_MIN,
  S.AC_4_STD, S.AC_4_MAX, S.AC_4_MIN, S.AC_5_STD, S.AC_5_MAX, S.AC_5_MIN, S.AC_6_STD, S.AC_6_MAX, S.AC_6_MIN,
  S.AC_7_STD, S.AC_7_MAX, S.AC_7_MIN, S.AC_8_STD, S.AC_8_MAX, S.AC_8_MIN, S.AC_9_STD, S.AC_9_MAX, S.AC_9_MIN,
  S.AC_A_STD, S.AC_A_MAX, S.AC_A_MIN,
  S.A_SCREW_STD,  S.A_SCREW_MAX,  S.A_SCREW_MIN,  S.A_CURR_STD,  S.A_CURR_MAX,  S.A_CURR_MIN,  S.A_GP_STD,  S.A_GP_MAX,  S.A_GP_MIN,
  S.BC_1_STD, S.BC_1_MAX, S.BC_1_MIN, S.BC_2_STD, S.BC_2_MAX, S.BC_2_MIN, S.BC_3_STD, S.BC_3_MAX, S.BC_3_MIN,
  S.BC_4_STD, S.BC_4_MAX, S.BC_4_MIN, S.BC_5_STD, S.BC_5_MAX, S.BC_5_MIN, S.BC_6_STD, S.BC_6_MAX, S.BC_6_MIN,
  S.BC_7_STD, S.BC_7_MAX, S.BC_7_MIN, S.BC_8_STD, S.BC_8_MAX, S.BC_8_MIN, S.BC_9_STD, S.BC_9_MAX, S.BC_9_MIN,
  S.BC_A_STD, S.BC_A_MAX, S.BC_A_MIN, S.BC_B_STD, S.BC_B_MAX, S.BC_B_MIN, S.BC_C_STD, S.BC_C_MAX, S.BC_C_MIN,
  S.BC_D_STD, S.BC_D_MAX, S.BC_D_MIN, S.BC_E_STD, S.BC_E_MAX, S.BC_E_MIN,
  S.B_SCREW_STD,  S.B_SCREW_MAX,  S.B_SCREW_MIN,  S.B_CURR_STD, S.B_CURR_MAX,  S.B_CURR_MIN,  S.B_GP_STD,  S.B_GP_MAX,  S.B_GP_MIN,
  S.D_1_STD, S.D_1_MAX, S.D_1_MIN, S.D_2_STD, S.D_2_MAX, S.D_2_MIN, S.D_3_STD, S.D_3_MAX, S.D_3_MIN,
  S.D_4_STD, S.D_4_MAX, S.D_4_MIN, S.D_5_STD, S.D_5_MAX, S.D_5_MIN, S.D_6_STD, S.D_6_MAX, S.D_6_MIN,
  S.D_7_STD, S.D_7_MAX, S.D_7_MIN,
  S.R_1_STD, S.R_1_MAX, S.R_1_MIN, S.R_2_STD, S.R_2_MAX, S.R_2_MIN, S.R_3_STD, S.R_3_MAX, S.R_3_MIN,
  S.R_SPEED_STD, S.R_SPEED_MAX, S.R_SPEED_MIN, S.R_GAP_L, S.R_GAP_R, S.VACUUM_INDEX, S.LAYER_1, S.LAYER_2, S.LAYER_3, S.MODEL_GAP, S.MODEL_WIDTH,
  S.MFG_DESC, S.SOC_DESC, S.REMARK, S.SCREEN_FREQ, S.RECORD_FREQ,
  S.LAYER_1_MAX, S.LAYER_1_MIN, S.LAYER_2_MAX, S.LAYER_2_MIN, S.LAYER_3_MAX, S.LAYER_3_MIN,
  S.VACUUM_INDEX_A_STD, S.VACUUM_INDEX_A_MAX, S.VACUUM_INDEX_A_MIN, S.VACUUM_INDEX_B_STD, S.VACUUM_INDEX_B_MAX,
  S.VACUUM_INDEX_B_MIN, S.SCREEN_LAYER_A, S.SCREEN_LAYER_B, S.SC_BP_A_STD, S.SC_BP_B_STD, S.SC_BP_A_MAX, S.SC_BP_B_MAX, S.SC_BP_A_MIN, S.SC_BP_B_MIN,
  S.MTR_DRY_TEMP_A, S.MTR_DRY_TEMP_B, S.MTR_DRY_TIME_A, S.MTR_DRY_TIME_B, S.MTR_MIX_TIME_A, S.MTR_MIX_TIME_B,
  S.R_U, S.R_M, S.R_D, S.R_TEN_STD, S.R_TEN_MAX, S.R_TEN_MIN,
  R_TEN_SLOPE_STD, R_TEN_SLOPE_MAX, R_TEN_SLOPE_MIN, R_TEN_SPEED_STD, R_TEN_SPEED_MAX, R_TEN_SPEED_MIN,
  FEEDP_A_STD, FEEDP_A_MAX, FEEDP_A_MIN, FEEDP_B_STD, FEEDP_B_MAX, FEEDP_B_MIN,
  REPLACE(CONVERT(VARCHAR(19), S.CDT, 120), '-', '/') CDT,
  S.CREATE_BY,
  CONVERT(VARCHAR(10), S.PUB_DATE, 111) PUB_DATE,
  S.PUBLIC_BY,
  REPLACE(CONVERT(VARCHAR(19), S.UDT, 120), '-', '/') UDT, S.UPDATE_BY, U.USER_NAME PUBLIC_NAME
FROM SOC_B S
INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
LEFT JOIN SYS_USER U
ON S.PUBLIC_BY = U.USER_ID
WHERE S.SOC_B_UID = {opSocUid}
```

SAP-1：
可參考舊版 WoQueryRfc.getWoHSingle

**獲得BAPI方法**
|              | 型態   | 參數值                     | 說明 |
| ------------ | ------ | -------------------------- | ---- |
| BAPIFunction | string | Z_PP_OPMS_PRODUCTION_ORDER |      |

**設定輸入參數**
| 參數名稱 | 參數值                      | 型態         | 說明 |
| -------- | --------------------------- | ------------ | ---- |
| I_WERKS  | WO_H.ORGANIZATION_ID(SQL-2) | string       |      |
| I_AUFNR  | WO_NO(SQL-1)                | string       |      |
| I_ERDAT  |                             | JCoStructure |      |

**設定輸入參數-I_ERDAT**
| 參數名稱 | 參數值     | 型態   | 說明 |
| -------- | ---------- | ------ | ---- |
| ERDAT_FR | "00000000" | string |      |
| ERDAT_TO | "99991231" | string |      |

| 取得回傳表格名稱 |
| ---------------- |
| T_ORDER          |

| SAP欄位名稱 | SAP資料型別 | 欄位         | 說明                                                                 |
| ----------- | :---------: | ------------ | -------------------------------------------------------------------- |
| MATNR       |   string    | 料號         |                                                                      |
| COLOR       |   string    | 色相         |                                                                      |
| STAND       |   string    | 生產單位     | 過濾前綴0                                                            |
| STAND_T     |   string    | 生產單位名稱 |                                                                      |
| AUFNR       |   string    | 工單號碼     | 過濾前綴0                                                            |
| COLOR_NO    |   string    | 料號         |                                                                      |
| ERNAM       |   string    | 業助代號     |                                                                      |
| KUNNR       |   string    | 客戶代號     | 過濾前綴0                                                            |
| SORT2       |   string    | 客戶名稱     |                                                                      |
| IHREZ       |   string    | 間接客戶     |                                                                      |
| GRADE       |   string    | Grade No     |                                                                      |
| AUART       |   string    |              |                                                                      |
| AUART_T     |   string    |              |                                                                      |
| MAKTX       |   string    | 料號摘要     |                                                                      |
| CUSTPO      |   string    | 客戶PO單號   |                                                                      |
| VORNR       |   string    |              | 過濾前綴0                                                            |
| ARBPL       |   string    |              |                                                                      |
| GAMNG       |   string    |              | 四捨五入至五位小數並轉為字串                                         |
| GMEIN       |   string    | 單位         |                                                                      |
| STATUS      |   string    |              |                                                                      |
| GSTRP       |   string    | 使用日期     |                                                                      |
| GSTRP       |   string    | 使用日期     | 格式：yyyy/MM/dd                                                     |
| ARBPL_T     |   string    | 機台名稱     |                                                                      |
| SOQTY       |   string    | 銷售數量     | 四捨五入至五位小數並轉為字串                                         |
| SHIP_DATE   |   string    |              | WO_H.ESTIMATED_SHIP_DATE為空且SHIP_DATE不為空才取值 格式：yyyy/MM/dd |
| LGORT       |   string    |              |                                                                      |
| KDAUF       |   string    |              | 過濾前綴0                                                            |
| KDPOS       |   string    |              | 過濾前綴0                                                            |
| CMC_SO_TYPE |   string    |              | 四捨五入至五位小數並轉為字串                                         |


JASPER-1:
**jasper檔案位置**
|          | 型態   | 參數值           | 說明 |
| -------- | ------ | ---------------- | ---- |
| RealPath | string | 參考後端流程說明 |      |

**jasper fillReport參數**
| 參數名稱 | 名稱 | 型態                | 來源資料 & 說明 |
| -------- | ---- | ------------------- | --------------- |
| pars     |      | Map<String, Object> |                 |
| jrds     |      | JRDataSource        | {tableList}     |

**pars object 參數**
| 參數名稱           | 名稱             | 型態   | 來源資料 & 說明               |
| ------------------ | ---------------- | ------ | ----------------------------- |
| ORGANIZATION_ID    | 工廠別           | string | SOC_B.ORGANIZATION_ID         |
| EQUIP_ID           | 機台             | string | SOC_B.EQUIP_ID                |
| EQUIP_NAME         | 機台名稱         | string | SOC_B.EQUIP_NAME              |
| ITEM_NO            | 料號             | string | SOC_B.ITEM_NO                 |
| SOC_B_UID          | ID               | string | SOC_B.SOC_B_UID               |
| SOC_B_VER          | 版號             | string | SOC_B.SOC_B_VER               |
| STATUS             | 狀態             | string | SOC_B.STATUS                  |
| AC_1_STD           | A層C1標準        | string | SOC_B.AC_1_STD                |
| AC_1_MAX           | A層C1上限        | string | SOC_B.AC_1_MAX                |
| AC_1_MIN           | A層C1下限        | string | SOC_B.AC_1_MIN                |
| AC_2_STD           | A層C1標準        | string | SOC_B.AC_2_STD                |
| AC_2_MAX           | A層C1上限        | string | SOC_B.AC_2_MAX                |
| AC_2_MIN           | A層C1下限        | string | SOC_B.AC_2_MIN                |
| AC_3_STD           | A層C1標準        | string | SOC_B.AC_3_STD                |
| AC_3_MAX           | A層C1上限        | string | SOC_B.AC_3_MAX                |
| AC_3_MIN           | A層C1下限        | string | SOC_B.AC_3_MIN                |
| AC_4_STD           | A層C1標準        | string | SOC_B.AC_4_STD                |
| AC_4_MAX           | A層C1上限        | string | SOC_B.AC_4_MAX                |
| AC_4_MIN           | A層C1下限        | string | SOC_B.AC_4_MIN                |
| AC_5_STD           | A層C1標準        | string | SOC_B.AC_5_STD                |
| AC_5_MAX           | A層C1上限        | string | SOC_B.AC_5_MAX                |
| AC_5_MIN           | A層C1下限        | string | SOC_B.AC_5_MIN                |
| AC_6_STD           | A層C1標準        | string | SOC_B.AC_6_STD                |
| AC_6_MAX           | A層C1上限        | string | SOC_B.AC_6_MAX                |
| AC_6_MIN           | A層C1下限        | string | SOC_B.AC_6_MIN                |
| AC_7_STD           | A層C1標準        | string | SOC_B.AC_7_STD                |
| AC_7_MAX           | A層C1上限        | string | SOC_B.AC_7_MAX                |
| AC_7_MIN           | A層C1下限        | string | SOC_B.AC_7_MIN                |
| AC_8_STD           | A層C1標準        | string | SOC_B.AC_8_STD                |
| AC_8_MAX           | A層C1上限        | string | SOC_B.AC_8_MAX                |
| AC_8_MIN           | A層C1下限        | string | SOC_B.AC_8_MIN                |
| AC_9_STD           | A層C1標準        | string | SOC_B.AC_9_STD                |
| AC_9_MAX           | A層C1上限        | string | SOC_B.AC_9_MAX                |
| AC_9_MIN           | A層C1下限        | string | SOC_B.AC_9_MIN                |
| AC_A_STD           | A層C1標準        | string | SOC_B.AC_A_STD                |
| AC_A_MAX           | A層C1上限        | string | SOC_B.AC_A_MAX                |
| AC_A_MIN           | A層C1下限        | string | SOC_B.AC_A_MIN                |
| A_SCREW_STD        | A層螺桿轉速標準  | string | SOC_B.A_SCREW_STD             |
| A_SCREW_MAX        | A層螺桿轉速上限  | string | SOC_B.A_SCREW_MAX             |
| A_SCREW_MIN        | A層螺桿轉速下限  | string | SOC_B.A_SCREW_MIN             |
| A_CURR_STD         | A層電流標準      | string | SOC_B.A_CURR_STD              |
| A_CURR_MAX         | A層電流上限      | string | SOC_B.A_CURR_MAX              |
| A_CURR_MIN         | A層電流下限      | string | SOC_B.A_CURR_MIN              |
| A_GP_STD           | A層GP標準        | string | SOC_B.A_GP_STD                |
| A_GP_MAX           | A層GP上限        | string | SOC_B.A_GP_MAX                |
| A_GP_MIN           | A層GP下限        | string | SOC_B.A_GP_MIN                |
| BC_1_STD           | B層C1標準        | string | SOC_B.BC_1_STD                |
| BC_1_MAX           | B層C1上限        | string | SOC_B.BC_1_MAX                |
| BC_1_MIN           | B層C1下限        | string | SOC_B.BC_1_MIN                |
| BC_2_STD           | B層C2標準        | string | SOC_B.BC_2_STD                |
| BC_2_MAX           | B層C2上限        | string | SOC_B.BC_2_MAX                |
| BC_2_MIN           | B層C2下限        | string | SOC_B.BC_2_MIN                |
| BC_3_STD           | B層C3標準        | string | SOC_B.BC_3_STD                |
| BC_3_MAX           | B層C3上限        | string | SOC_B.BC_3_MAX                |
| BC_3_MIN           | B層C3下限        | string | SOC_B.BC_3_MIN                |
| BC_4_STD           | B層C4標準        | string | SOC_B.BC_4_STD                |
| BC_4_MAX           | B層C4上限        | string | SOC_B.BC_4_MAX                |
| BC_4_MIN           | B層C4下限        | string | SOC_B.BC_4_MIN                |
| BC_5_STD           | B層C5標準        | string | SOC_B.BC_5_STD                |
| BC_5_MAX           | B層C5上限        | string | SOC_B.BC_5_MAX                |
| BC_5_MIN           | B層C5下限        | string | SOC_B.BC_5_MIN                |
| BC_6_STD           | B層C6標準        | string | SOC_B.BC_6_STD                |
| BC_6_MAX           | B層C6上限        | string | SOC_B.BC_6_MAX                |
| BC_6_MIN           | B層C6下限        | string | SOC_B.BC_6_MIN                |
| BC_7_STD           | B層C7標準        | string | SOC_B.BC_7_STD                |
| BC_7_MAX           | B層C7上限        | string | SOC_B.BC_7_MAX                |
| BC_7_MIN           | B層C7下限        | string | SOC_B.BC_7_MIN                |
| BC_8_STD           | B層C8標準        | string | SOC_B.BC_8_STD                |
| BC_8_MAX           | B層C8上限        | string | SOC_B.BC_8_MAX                |
| BC_8_MIN           | B層C8下限        | string | SOC_B.BC_8_MIN                |
| BC_9_STD           | B層C9標準        | string | SOC_B.BC_9_STD                |
| BC_9_MAX           | B層C9上限        | string | SOC_B.BC_9_MAX                |
| BC_9_MIN           | B層C9下限        | string | SOC_B.BC_9_MIN                |
| BC_A_STD           | B層C10標準       | string | SOC_B.BC_A_STD                |
| BC_A_MAX           | B層C10上限       | string | SOC_B.BC_A_MAX                |
| BC_A_MIN           | B層C10下限       | string | SOC_B.BC_A_MIN                |
| BC_B_STD           | B層C11標準       | string | SOC_B.BC_B_STD                |
| BC_B_MAX           | B層C11上限       | string | SOC_B.BC_B_MAX                |
| BC_B_MIN           | B層C11下限       | string | SOC_B.BC_B_MIN                |
| BC_C_STD           | B層C12標準       | string | SOC_B.BC_C_STD                |
| BC_C_MAX           | B層C12上限       | string | SOC_B.BC_C_MAX                |
| BC_C_MIN           | B層C12下限       | string | SOC_B.BC_C_MIN                |
| BC_D_STD           | B層C13標準       | string | SOC_B.BC_D_STD                |
| BC_D_MAX           | B層C13上限       | string | SOC_B.BC_D_MAX                |
| BC_D_MIN           | B層C13下限       | string | SOC_B.BC_D_MIN                |
| BC_E_STD           | B層C14標準       | string | SOC_B.BC_E_STD                |
| BC_E_MAX           | B層C14上限       | string | SOC_B.BC_E_MAX                |
| BC_E_MIN           | B層C14下限       | string | SOC_B.BC_E_MIN                |
| B_SCREW_STD        | B層螺桿轉速標準  | string | SOC_B.B_SCREW_STD             |
| B_SCREW_MAX        | B層螺桿轉速上限  | string | SOC_B.B_SCREW_MAX             |
| B_SCREW_MIN        | B層螺桿轉速下限  | string | SOC_B.B_SCREW_MIN             |
| B_CURR_STD         | B層電流標準      | string | SOC_B.B_CURR_STD              |
| B_CURR_MAX         | B層電流上限      | string | SOC_B.B_CURR_MAX              |
| B_CURR_MIN         | B層電流下限      | string | SOC_B.B_CURR_MIN              |
| B_GP_STD           | B層GP標準        | string | SOC_B.B_GP_STD                |
| B_GP_MAX           | B層GP上限        | string | SOC_B.B_GP_MAX                |
| B_GP_MIN           | B層GP下限        | string | SOC_B.B_GP_MIN                |
| D_1_STD            | 模頭溫度D1標準   | string | SOC_B.D_1_STD                 |
| D_1_MAX            | 模頭溫度D1上限   | string | SOC_B.D_1_MAX                 |
| D_1_MIN            | 模頭溫度D1下限   | string | SOC_B.D_1_MIN                 |
| D_2_STD            | 模頭溫度D2標準   | string | SOC_B.D_2_STD                 |
| D_2_MAX            | 模頭溫度D2上限   | string | SOC_B.D_2_MAX                 |
| D_2_MIN            | 模頭溫度D2下限   | string | SOC_B.D_2_MIN                 |
| D_3_STD            | 模頭溫度D3標準   | string | SOC_B.D_3_STD                 |
| D_3_MAX            | 模頭溫度D3上限   | string | SOC_B.D_3_MAX                 |
| D_3_MIN            | 模頭溫度D3下限   | string | SOC_B.D_3_MIN                 |
| D_4_STD            | 模頭溫度D4標準   | string | SOC_B.D_4_STD                 |
| D_4_MAX            | 模頭溫度D4上限   | string | SOC_B.D_4_MAX                 |
| D_4_MIN            | 模頭溫度D4下限   | string | SOC_B.D_4_MIN                 |
| D_5_STD            | 模頭溫度D5標準   | string | SOC_B.D_5_STD                 |
| D_5_MAX            | 模頭溫度D5上限   | string | SOC_B.D_5_MAX                 |
| D_5_MIN            | 模頭溫度D5下限   | string | SOC_B.D_5_MIN                 |
| D_6_STD            | 模頭溫度D6標準   | string | SOC_B.D_6_STD                 |
| D_6_MAX            | 模頭溫度D6上限   | string | SOC_B.D_6_MAX                 |
| D_6_MIN            | 模頭溫度D6下限   | string | SOC_B.D_6_MIN                 |
| D_7_STD            | 模頭溫度D7標準   | string | SOC_B.D_7_STD                 |
| D_7_MAX            | 模頭溫度D7上限   | string | SOC_B.D_7_MAX                 |
| D_7_MIN            | 模頭溫度D7下限   | string | SOC_B.D_7_MIN                 |
| R_1_STD            | 滾筒溫度(上)標準 | string | SOC_B.R_1_STD                 |
| R_1_MAX            | 滾筒溫度(上)上限 | string | SOC_B.R_1_MAX                 |
| R_1_MIN            | 滾筒溫度(上)下限 | string | SOC_B.R_1_MIN                 |
| R_2_STD            | 滾筒溫度(中)標準 | string | SOC_B.R_2_STD                 |
| R_2_MAX            | 滾筒溫度(中)上限 | string | SOC_B.R_2_MAX                 |
| R_2_MIN            | 滾筒溫度(中)下限 | string | SOC_B.R_2_MIN                 |
| R_3_STD            | 滾筒溫度(下)標準 | string | SOC_B.R_3_STD                 |
| R_3_MAX            | 滾筒溫度(下)上限 | string | SOC_B.R_3_MAX                 |
| R_3_MIN            | 滾筒溫度(下)下限 | string | SOC_B.R_3_MIN                 |
| R_SPEED_STD        | 滾筒速度標準     | string | SOC_B.R_SPEED_STD             |
| R_SPEED_MAX        | 滾筒速度上限     | string | SOC_B.R_SPEED_MAX             |
| R_SPEED_MIN        | 滾筒速度下限     | string | SOC_B.R_SPEED_MIN             |
| R_GAP_L            | 滾筒間隙(左)     | string | SOC_B.R_GAP_L                 |
| R_GAP_R            | 滾筒間隙(右)     | string | SOC_B.R_GAP_R                 |
| VACUUM_INDEX       | 真空指數         | string | SOC_B.VACUUM_INDEX            |
| LAYER_1            | 層間比%          | string | SOC_B.LAYER_1                 |
| LAYER_2            | 層間比%          | string | SOC_B.LAYER_2                 |
| LAYER_3            | 層間比%          | string | SOC_B.LAYER_3                 |
| MODEL_GAP          | 模唇間隙         | string | SOC_B.MODEL_GAP               |
| MODEL_WIDTH        | 模唇出料寬度     | string | SOC_B.MODEL_WIDTH             |
| MFG_DESC           | 生產方式說明     | string | SOC_B.MFG_DESC                |
| SOC_DESC           | SOC識別文字      | string | SOC_B.SOC_DESC                |
| REMARK             | 備註             | string | SOC_B.REMARK                  |
| SCREEN_FREQ        | 網目更換         | string | SOC_B.SCREEN_FREQ             |
| RECORD_FREQ        | 紀錄頻率         | string | SOC_B.RECORD_FREQ             |
| LAYER_1_MAX        | 間層比1上限      | string | SOC_B.LAYER_1_MAX             |
| LAYER_1_MIN        | 間層比1下限      | string | SOC_B.LAYER_1_MIN             |
| LAYER_2_MAX        | 間層比2上限      | string | SOC_B.LAYER_2_MAX             |
| LAYER_2_MIN        | 間層比2下限      | string | SOC_B.LAYER_2_MIN             |
| LAYER_3_MAX        | 間層比3上限      | string | SOC_B.LAYER_3_MAX             |
| LAYER_3_MIN        | 間層比3下限      | string | SOC_B.LAYER_3_MIN             |
| VACUUM_INDEX_A_STD | A機真空指數標準  | string | SOC_B.VACUUM_INDEX_A_STD      |
| VACUUM_INDEX_A_MAX | A機真空指數上限  | string | SOC_B.VACUUM_INDEX_A_MAX      |
| VACUUM_INDEX_A_MIN | A機真空指數下限  | string | SOC_B.VACUUM_INDEX_A_MIN      |
| VACUUM_INDEX_B_STD | B機真空指數標準  | string | SOC_B.VACUUM_INDEX_B_STD      |
| VACUUM_INDEX_B_MAX | B機真空指數上限  | string | SOC_B.VACUUM_INDEX_B_MAX      |
| VACUUM_INDEX_B_MIN | B機真空指數下限  | string | SOC_B.VACUUM_INDEX_B_MIN      |
| SCREEN_LAYER_A     | 網目A            | string | SOC_B.SCREEN_LAYER_A          |
| SCREEN_LAYER_B     | 網目B            | string | SOC_B.SCREEN_LAYER_B          |
| SC_BP_A_STD        | 網前背壓A        | string | SOC_B.SC_BP_A_STD             |
| SC_BP_B_STD        | 網前背壓B        | string | SOC_B.SC_BP_B_STD             |
| SC_BP_A_MAX        | 網前背壓A        | string | SOC_B.SC_BP_A_MAX             |
| SC_BP_B_MAX        | 網前背壓B        | string | SOC_B.SC_BP_B_MAX             |
| SC_BP_A_MIN        | 網前背壓A        | string | SOC_B.SC_BP_A_MIN             |
| SC_BP_B_MIN        | 網前背壓B        | string | SOC_B.SC_BP_B_MIN             |
| MTR_DRY_TEMP_A     | 原料乾燥A層溫度  | string | SOC_B.MTR_DRY_TEMP_A          |
| MTR_DRY_TEMP_B     | 原料乾燥B層溫度  | string | SOC_B.MTR_DRY_TEMP_B          |
| MTR_DRY_TIME_A     | 原料乾燥A層時間  | string | SOC_B.MTR_DRY_TIME_A          |
| MTR_DRY_TIME_B     | 原料乾燥B層時間  | string | SOC_B.MTR_DRY_TIME_B          |
| MTR_MIX_TIME_A     | 原料A層混合時間  | string | SOC_B.MTR_MIX_TIME_A          |
| MTR_MIX_TIME_B     | 原料B層混合時間  | string | SOC_B.MTR_MIX_TIME_B          |
| R_U                | 滾輪名稱-上      | string | SOC_B.R_U                     |
| R_M                | 滾輪名稱-中      | string | SOC_B.R_M                     |
| R_D                | 滾輪名稱-下      | string | SOC_B.R_D                     |
| R_TEN_STD          | 收捲張力標準     | string | SOC_B.R_TEN_STD               |
| R_TEN_MAX          | 收捲張力上限     | string | SOC_B.R_TEN_MAX               |
| R_TEN_MIN          | 收捲張力下限     | string | SOC_B.R_TEN_MIN               |
| R_TEN_SLOPE_STD    |                  | string | SOC_B.R_TEN_SLOPE_STD         |
| R_TEN_SLOPE_MAX    |                  | string | SOC_B.R_TEN_SLOPE_MAX         |
| R_TEN_SLOPE_MIN    |                  | string | SOC_B.R_TEN_SLOPE_MIN         |
| R_TEN_SPEED_STD    |                  | string | SOC_B.R_TEN_SPEED_STD         |
| R_TEN_SPEED_MAX    |                  | string | SOC_B.R_TEN_SPEED_MAX         |
| R_TEN_SPEED_MIN    |                  | string | SOC_B.R_TEN_SPEED_MIN         |
| FEEDP_A_STD        |                  | string | SOC_B.FEEDP_A_STD             |
| FEEDP_A_MAX        |                  | string | SOC_B.FEEDP_A_MAX             |
| FEEDP_A_MIN        |                  | string | SOC_B.FEEDP_A_MIN             |
| FEEDP_B_STD        |                  | string | SOC_B.FEEDP_B_STD             |
| FEEDP_B_MAX        |                  | string | SOC_B.FEEDP_B_MAX             |
| FEEDP_B_MIN        |                  | string | SOC_B.FEEDP_B_MIN             |
| CDT                | 建立日期         | string | SOC_B.CDT yyyy/mm/dd hh:mm:ss |
| CREATE_BY          | 建立人員         | string | SOC_B.CREATE_BY               |
| PUB_DATE           | 發行日期         | string | SOC_B.PUB_DATE                |
| PUBLIC_BY          | 發行人代碼       | string | SOC_B.PUBLIC_BY               |
| UDT                | 更新日期         | string | SOC_B.UDT yyyy/mm/dd hh:mm:ss |
| UPDATE_BY          | 更新人員         | string | SOC_B.UPDATE_BY               |
| PUBLIC_NAME        | 發行人名稱       | string | SOC_B.PUBLIC_NAME             |
| title              |                  | string |                               |
| PATTERN_NO         |                  | string |                               |
| WO_NO              |                  | string |                               |
| ISO_DESC           |                  | string |                               |
| LOGO               |                  | string |                               |
| VACUUM_UNIT        |                  | string |                               |

**jrds object 參數**
| 參數名稱 | 名稱 | 資料型別 | 資料儲存 & 說明 |
| -------- | ---- | -------- | --------------- |
| rowNo    |      | string   |                 |
| TIME_S   |      | string   |                 |
| TIME_E   |      | string   |                 |



