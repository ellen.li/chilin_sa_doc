# xx-xxxx
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

規格說明 template


## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 2023XXXX | 新增規格 |        |

## 來源URL及資料格式

| 項目   | 說明 |
|--------|------|
| URL    |      |
| method | post |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位 | 名稱 | 資料型別 | 必填 | 資料儲存 & 說明 |
|------|------|:--------:|:----:|-----------------|
|      |      |          |      |                 |
|      |      |          |      |                 |

#### Request 範例

```json
{

}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql

```


# Response 欄位
| 欄位 | 名稱 | 資料型別 | 資料儲存 & 說明 |
|------|------|----------|-----------------|
|      |      |          |                 |

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":null
  }
}
```

## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考

* Class 名稱.function 名稱 (ex: BomManageAction.saveBom)