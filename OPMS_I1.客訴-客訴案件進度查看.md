# 客訴-客訴案件進度查看
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

客訴案件進度查看


## 修改歷程

| 修改時間 | 內容                                           | 修改者 |
|----------|----------------------------------------------|--------|
| 20230707 | 新增規格                                       | Sam    |
| 20230717 | 增加說明 C.CS_CLOSE_REPORT                     | Sam    |
| 20230801 | 新增 Base64附件資料(opContent)、opInsId         | Nick   |
| 20230802 | 新增 附件的 opDocType                          | Nick   |
| 20230803 | 新增 檔案讀取異常給 null 不跳錯誤              | Nick   |
| 20230804 | 調整 【案件狀態】誤植為【案件類別】、新增【案件類別】 | Nick   |
| 20230814 | 調整附件說明                                   | Nick   |


## 來源URL及資料格式

| 項目   | 說明                     |
|--------|-------------------------|
| URL    | /cs/cs_view             |
| method | post                    |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位              |       名稱         | 資料型別  | 必填  | 資料儲存 & 說明  |
|------------------|-------------------|:--------:|:----:|-----------------|
| opCsUid          | 案件編號/案號/單號   |  string  |  M   |                |



#### Request 範例
```json
{
  "opCsUid":"A2DD9E20-819A-40DB-B190-46E60DD258A6"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* opCsUid : 客訴編號 
* 根據上述 opCsUid 可以找出單一客訴資料 
* opCsProgress，參考 SQL-1 結果，

      立案 opRegistration : ""/{ACTIVE_DATE}
         1)若 {ACTIVE_DATE} 值為 null，則回傳空字串，否則回傳 {ACTIVE_DATE}
      
      指派成員 opDispatch : "進行中"/"-"/"{UDT_B}"
         1)若 {STATUS} 值為 "4"，則回傳 "進行中" 字串，
         2)若 {STATUS} 不為 "4" 且 
            {UDT_B} 為 null，則回傳 "-"
            {UDT_B} 為不為 null，則回傳 {UDT_B}

      初步分析 opInitAnalyze : "進行中"/"-"/"{UDT_C}"
         1)若 {STATUS} 值為 "5"，則回傳 "進行中" 字串，
         2)若 {STATUS} 不為 "5" 且 
            {UDT_C} 為 null，則回傳 "-"
            {UDT_C} 為不為 null，則回傳 {UDT_C}

      初報回覆 opInitResponse : "不需回覆"/"尚未回覆"/{FIRST_REPORT_DATE}
         1)若 {First_Report_Date} 值為 null，且
            若 {FIRST_REPORT_FLAG} 值為 null 或 空字串 或 "N"，則回傳 "不需回覆"，否則回傳 "尚未回覆"
         
         2)若 {First_Report_Date} 值不為 null，則回傳 {First_Report_Date}

      真因對策 opSolution : "進行中"/"-"/"{UDT_D}"
         1)若 {STATUS} 值為 "A"，則回傳 "進行中" 字串，
         2)若 {STATUS} 不為 "A" 且 
            {UDT_D} 為 null，則回傳 "-"
            {UDT_D} 為不為 null，則回傳 {UDT_D}

      終報彙整 opCompilation : "進行中"/"-"/"{UDT_E}"
         1)若 {STATUS} 值為 "B"，則回傳 "進行中" 字串，
         2)若 {STATUS} 不為 "B" 且 
            {UDT_E} 為 null，則回傳 "-"
            {UDT_E} 為不為 null，則回傳 {UDT_E}

      終報審核 opReview : "進行中"/"-"/"{UDT_F}"
         1)若 {STATUS} 值為 "C"，則回傳 "進行中" 字串，
         2)若 {STATUS} 不為 "C" 且 
            {UDT_F} 為 null，則回傳 "-"
            {UDT_F} 為不為 null，則回傳 {UDT_F}

      回覆客戶 opReply : "進行中"/"-"/"{CS_REPORT_DATE}"
         1)若 {STATUS} 值為 "D"，則回傳 "進行中" 字串，
         2)若 {STATUS} 不為 "D" 且 
            {CS_REPORT_DATE} 為 null，則回傳 "-"
            {CS_REPORT_DATE} 為不為 null，則回傳 {CS_REPORT_DATE}

      結案    opClose : "進行中"/"-"/"{UDT_Z}"
         1)若 {STATUS} 值為 "E"，則回傳 "進行中" 字串，
         2)若 {STATUS} 不為 "E" 且 
            {UDT_Z} 為 null，則回傳 "-"
            {UDT_Z} 為不為 null，則回傳 {UDT_Z}
      
      處理時間 opProcessingPeriod : 單位為天數
         若尚未結案 （STATUS 為"E" 或是 STATUS 不為 "E" 且 {UDT_Z} 為 null)，
            則計算方式為 "當下時間" {取系統 CurrentTime} 減去 "立案時間" {ACTIVE_DATE}
         若為已結案，
            則計算方式為 "結案時間" {UDT_Z} 減去 "立案時間" {ACTIVE_DATE}

* 執行 SQL-1 取得 單一客訴案件

      1-1. 
         除以下特別說明以外，其餘參數皆為 : 若值為 null，則回傳空字串
         opDfA : 參考 SQL-5-1 
            查詢 SYS_PARA_D Table PARA_ID='CS_DF' AND PARA_VALUE LIKE {DF_CODE}，回傳 {DF_DESC}
         opDfs : 參考 SQL-5-1 
            查詢 SYS_PARA_D Table PARA_ID='CS_DF' AND PARA_VALUE LIKE {DF_CODE}，DfB 的 {DF_DESC}/DfC 的{DF_DESC}/DfD 的{DF_DESC}

* 執行 SQL-2 取得 製造記錄
* 執行 SQL-3 取得 客訴處理說明
* 執行 SQL-4 取得附件清單 result4 ，並將各欄位資料指定為 opDoc 內容，
  * opContent 內容如下:
  * {date} 取 result4.CDT 欄位日期 ex.202307
  * {opContent} = 讀取檔案 cs.file.root/{date}/{result4.CS_DOC_UID}，轉成 base64 字串，檔案讀取異常給 null 不跳錯誤。
* 執行 SQL-5 取得 真因分析與終判
* 執行 SQL-1 中的 {C.CS_CLOSE_REPORT} 取得 品保報告
* 組成 json 格式
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1
```sql
-- getCsSingle(String csUid) 取得單一客訴案件資訊
SELECT 
   C.CS_UID,      --{opCsUid}
   C.PATTERN_NO,  --廠內色號 
   C.GRADE_NO,    --規格/Grade
   C.UNIT,        --單位
   C.CS_SUBJECT,  --主旨說明
   C.CS_NO,       --單號/案件編號
   C.ORGANIZATION_ID, 
   C.WF_INS_ID,   --WF表單ID 
   C.SALES_USER_ID, 
   C.REPORT_USER_ID, 
   C.CS_ITEM_TYPE, --產品別
   C.ACTIVE_FLAG, 
   C.STATUS,       --案件狀態 
   C.CLAIM_FLAG,   --案件類別
   C.CLAIM_DESC, 
   -- C.CS_GRADE,     --風險
   P1.OPT_DISPLAY as CS_GRADE, --經 OPMS_PARAMS 所 output 風險 
   C.CUST_NAME,    --客戶名稱 
   C.CUST_NO,      --客戶編號
   C.DEST_CUST_NAME, --終端客戶
   CONVERT(VARCHAR(10),C.CS_ISSUE_DATE,111) AS CS_ISSUE_DATE, --抱怨日
   C.CS_ITEM_NO, 
   C.DF_A,             --不良現象
   C.DF_B, C.DF_C, C.DF_D, --(次) dfB/dfC/dfD
   C.DF_O, 
   C.CS_REMARK,        --客訴細節描述
   C.CS_DF_QTY,        --抱怨數量(KG)
   C.CS_USED_QTY,      --已用量(KG)
   C.CS_NON_QTY,       --未用量(KG)
   C.CS_ITEM_LOCATION, --產品所在地
   -- C.CS_RT_FLAG,       --要求換貨 : Y/N
   P3.OPT_DISPLAY as CS_RT_FLAG --經 OPMS_PARAMS 所 output 要求換貨
   C.CS_RT_QTY,        --換貨量(KG)
   -- C.CS_REFUND_FLAG,   --賠償要求
   P4.OPT_DISPLAY AS CS_REFUND_FLAG, --經 OPMS_PARAMS 所 output 賠償要求
   C.CS_REFUND_DESC, 
   C.CS_REFUND_AMOUNT,
   -- C.CS_REFUND_CURRENCY, --賠償金額 NTD/USD/RMB
   P6.OPT_DISPLAY as CS_REFUND_CURRENCY, ---經 OPMS_PARAMS 所 output 賠償金額
   C.SALES_ORDER_NO,   --銷售訂單
   C.SALES_QTY,        --訂購量(KG)
   C.CS_SHIP_QTY,      --已出貨量(KG)
   -- C.SAMPLE_D_FLAG,    --樣本到廠
   P2.OPT_DISPLAY as SAMPLE_D_FLAG, --經 OPMS_PARAMS 所 output 樣本到廠
   CONVERT(VARCHAR(10),C.ACTIVE_DATE,111) AS ACTIVE_DATE, --立案日期
   CONVERT(VARCHAR(10),C.UDT_A,111) AS UDT_A, 
   C.UUN_A, 
   CONVERT(VARCHAR(10),C.UDT_B,111) AS UDT_B, --指派成員日期
   C.UUN_B, 
   CONVERT(VARCHAR(10),C.UDT_C,111) AS UDT_C, --初步分析日期 --真因終判更新時間
   C.UUN_C, --真因分析與終判之更新人員
   CONVERT(VARCHAR(10),C.UDT_D,111) AS UDT_D, --真因對策日期
   C.UUN_D, 
   CONVERT(VARCHAR(10),C.UDT_E,111) AS UDT_E, --終報彙整日期 --品保更新時間
   C.UUN_E, --品保更新人員
   CONVERT(VARCHAR(10),C.UDT_F,111) AS UDT_F, --終報審核日期
   C.UUN_F, 
   CONVERT(VARCHAR(10),C.UDT_Z,111) AS UDT_Z, --結案日期
   C.UUN_Z, 
   CONVERT(VARCHAR(10),C.UDT,111) AS UDT,     --
   C.UPDATE_BY,
   CONVERT(VARCHAR(10),C.CDT,111) AS CDT,     --
   C.CREATE_BY,
   CONVERT(VARCHAR(10),C.CS_REPORT_DATE,111) AS CS_REPORT_DATE, --終報回覆 --回覆客戶日期
   C.CS_CLOSE_REPORT,   --品保報告
   -- C.FINAL_DIAG_FLAG,   --終判Flag
   P7.OPT_DISPLAY as FINAL_DIAG_FLAG, --責任歸屬 : XX責
   -- C.FIRST_REPORT_FLAG, --需要初報:"Y"/"N"
   P5.OPT_DISPLAY as FIRST_REPORT_FLAG,  --經 OPMS_PARAMS 所 output 需要初報
   CONVERT(VARCHAR(10),C.FIRST_REPORT_DATE,111) AS FIRST_REPORT_DATE, --初報回覆 
   US.USER_NAME SALES_USER_NAME,  --負責業務
   UR.USER_NAME REPORT_USER_NAME  --品管PM
FROM CS_H C 
LEFT JOIN SYS_USER US 
	ON C.SALES_USER_ID=US.USER_ID 
LEFT JOIN SYS_USER UR 
	ON C.REPORT_USER_ID=UR.USER_ID 
LEFT JOIN OPMS_PARAMS P1 --join 出風險值
	ON (P1.OPT_VALUE = C.CS_GRADE AND　P1.TYPE = 'CS' AND P1.PARAM_ID = 'CSGRADE')
LEFT JOIN OPMS_PARAMS P2 --join 出樣本到場
	ON (P2.OPT_VALUE = C.SAMPLE_D_FLAG AND　P2.TYPE = 'CS' AND P2.PARAM_ID = 'SAMPLE_D_FLAG')
LEFT JOIN OPMS_PARAMS P3 --join 出要求換貨 
	ON (P3.OPT_VALUE = C.CS_RT_FLAG AND　P3.TYPE = 'CS' AND P3.PARAM_ID = 'CSRTFLAG')
LEFT JOIN OPMS_PARAMS P4 --join 出賠償要求
	ON (P4.OPT_VALUE = C.CS_REFUND_FLAG AND　P4.TYPE = 'CS' AND P4.PARAM_ID = 'REFUND_FLAG')
LEFT JOIN OPMS_PARAMS P5 --join 出需要初報
	ON (P5.OPT_VALUE = C.FIRST_REPORT_FLAG AND　P5.TYPE = 'CS' AND P5.PARAM_ID = 'FIRST_REPORT_FLAG')
LEFT JOIN OPMS_PARAMS P6 --join 出賠償金額
	ON (P6.OPT_VALUE = C.CS_REFUND_CURRENCY AND　P6.TYPE = 'CS' AND P6.PARAM_ID = 'CSREFUNDCURRENCY')
LEFT JOIN OPMS_PARAMS P7 --join 出終判責任歸屬
	ON (P7.OPT_VALUE = C.FINAL_DIAG_FLAG AND　P7.TYPE = 'CS' AND P7.PARAM_ID = 'CS_DIAG_FLAG')

WHERE C.CS_UID = {opCsUid}  
```


SQL-2 
```sql
-- 製造記錄
SELECT 
   -- L.CS_UID,
   -- L.CS_LOT_SEQ,
   L.CS_LOT_NO, --LOT No.
   P.PRODUNIT_NAME, --製造單位
   E.EQUIP_NAME, --生產機台
   L.PRODUNIT_NO, L.EQUIP_ID, L.CS_LOT_QTY, --生產數量
   CONVERT(VARCHAR(10),L.MFG_DATE,111) AS MFG_DATE, --生產日期
   -- REPLACE(CONVERT(VARCHAR(19),L.CDT,120),'-','/') CDT,L.CREATE_BY, 
   U.USER_NAME CREATE_BY_NAME, --建立人
FROM CS_LOT L 
LEFT JOIN SYS_USER U 
	ON L.CREATE_BY=U.USER_ID 
LEFT JOIN PRODUNIT P 
	ON L.ORGANIZATION_ID=P.ORGANIZATION_ID AND L.PRODUNIT_NO=P.PRODUNIT_NO 
LEFT JOIN EQUIP_H E 
	ON L.EQUIP_ID=E.EQUIP_ID 
WHERE L.CS_UID={opCsUid}  
```

SQL-3 
```sql
-- 客訴處理說明
SELECT 
--	C.CS_UID, 
--	C.CS_COMMENT_SEQ, 
	C.CS_COMMENT,  --客訴處理說明
	U.USER_NAME AS UPDATE_BY_NAME, --人員
	REPLACE(CONVERT(VARCHAR(19),C.UDT,120),'-','/') UDT --日期
FROM CS_COMMENT C 
LEFT JOIN SYS_USER U 
	ON C.UPDATE_BY=U.USER_ID 
WHERE C.CS_UID = {opCsUid} 
```

SQL-4
```sql
--附件
SELECT 
 	-- D.CS_UID, 
   D.CS_DOC_UID, --Doc Uid
 	D.DOC_FLAG, --檔案類別
 	D.DOC_NAME, --檔案名稱
 	D.DOC_TYPE, 
 	REPLACE(CONVERT(VARCHAR(19),D.CDT,120),'-','/') CDT, --上傳日期/日期
 	D.CREATE_BY, 
 	U.USER_NAME CREATE_BY_NAME --上傳者/人員
FROM CS_DOC D 
LEFT JOIN SYS_USER U 
	ON D.CREATE_BY=U.USER_ID 
WHERE D.CS_UID = {opCsUid} 
```

SQL-5
```sql
--真因分析與終判
SELECT 
	-- D.CS_UID,
	-- D.CS_DIAG_SEQ,
	D.DF_CODE,     --A13 => 參考 SQL-5-1 : 不良現象
	D.DIAG_FLAG,   --C06 => 參考 SQL-5-2 : 真因分類
	D.DIAG_REMARK, --真因描述
	U.USER_NAME UPDATE_BY_NAME, --更新人員
   REPLACE(CONVERT(VARCHAR(19),D.UDT,120),'-','/') UDT, --日期
FROM CS_DIAG D 
LEFT JOIN SYS_USER U 
	ON D.UPDATE_BY=U.USER_ID
WHERE D.CS_UID = {opCsUid} 
```

SQL-5-1 
```sql
--真因分析與終判-不良現象
SELECT 
   PARA_VALUE DF_CODE,
   PARA_TEXT DF_DESC --不良現象 : 射出表面銀紋
FROM SYS_PARA_D 
WHERE PARA_ID='CS_DF' 
AND PARA_VALUE LIKE {D.DF_CODE} -- DF_CODE='A13'
ORDER BY PARA_VALUE
```

SQL-5-2
```sql
--真因分析與終判-真因分類
SELECT 
	PARA_VALUE AS DIAG_FLAG,     
	PARA_TEXT  AS DIAG_FLAG_DESC --真因分類 : 料-原料混練不均
FROM SYS_PARA_D 
WHERE PARA_ID='CS_DIAG' 
AND PARA_VALUE = {D.DIAG_FLAG} -- DIAG_FLAG='C06' 
ORDER BY PARA_VALUE
```


# Response 欄位
【 content 】 object
| 欄位          | 名稱             | 資料型別     | 資料儲存 & 說明  |
|---------------|----------------|--------------|------------------|
| opCsProgress  | 單一客訴狀態進程 | object       | 需額外判斷後寫入 |
| opCsSingle    | 單一客訴資訊     | object       |                  |
| opLOT         | 製造記錄         | array object |                  |
| opComment     | 客訴處理說明     | array object |                  |
| opDoc         | 附件             | array object |                  |
| opDiag        | 真因分析與終判   | array object |                  |
| opCloseReport | 品保報告         | object       |                  |


### 【opCsProgress】object
| 欄位               | 名稱     | 資料型別 | 資料儲存 & 說明                                      |
|--------------------|--------|----------|------------------------------------------------------|
| opRegistration     | 立案     | string   | ""/{ACTIVE_DATE}       yyyy/mm/dd                    |
| opDispatch         | 指派成員 | string   | "進行中"/"-"/"{UDT_B}"  yyyy/mm/dd                   |
| opInitAnalyze      | 初步分析 | string   | "進行中"/"-"/"{UDT_C}"  yyyy/mm/dd                   |
| opInitResponse     | 初報回覆 | string   | "不需回覆"/"尚未回覆"/{FIRST_REPORT_DATE} yyyy/mm/dd |
| opSolution         | 真因對策 | string   | "進行中"/"-"/"{UDT_D}"  yyyy/mm/dd                   |
| opCompilation      | 終報彙整 | string   | "進行中"/"-"/"{UDT_E}"  yyyy/mm/dd                   |
| opReview           | 終報審核 | string   | "進行中"/"-"/"{UDT_F}"  yyyy/mm/dd                   |
| opReply            | 回覆客戶 | string   | "進行中"/"-"/"{CS_REPORT_DATE}" yyyy/mm/dd           |
| opClose            | 結案     | string   | "進行中"/"-"/"{UDT_Z}" yyyy/mm/dd                    |
| opProcessingPeriod | 處理時間 | integer  | (當下時間 or 結案時間)-立案時間 (單位為天數)         |


### 【opCsSingle】object
| 欄位               | 名稱           | 資料型別 | 資料儲存 & 說明                                                                |
|--------------------|---------------|----------|--------------------------------------------------------------------------------|
| opCsNo             | 單號/案件編號  | string   | CS_NO                                                                          |
| opCsStatus         | 案件狀態       | string   | STATUS                                                                         |
| opCsSubject        | 主旨說明       | string   | CS_SUBJECT，預設值為 null                                                       |
| opCustName         | 客戶名稱       | string   | CUST_NAME                                                                      |
| opDestCustName     | 終端客戶       | string   | DEST_CUST_NAME                                                                 |
| opCsGrade          | 風險度         | string   | CS_GRADE                                                                       |
| opPatternNo        | 廠內色號       | string   | PATTERN_NO                                                                     |
| opGradeNo          | 規格/Grade     | string   | GRADE_NO                                                                       |
| opCsItemType       | 產品別         | string   | CS_ITEM_TYPE                                                                   |
| opDfA              | 不良現象       | string   | DF_A                                                                           |
| opDfs              | (次) --/--/--  | string   | DF_B/DF_C/DF_D                                                                 |
| opCsDfQty          | 抱怨數量(KG)   | integer  | CS_DF_QTY                                                                      |
| opCsUsedQty        | 已用數量(KG)   | integer  | CS_USED_QTY                                                                    |
| opCsNonQty         | 未用數量(KG)   | integer  | CS_NON_QTY                                                                     |
| opSalesOrderNo     | 銷售訂單       | string   | SALES_ORDER_NO                                                                 |
| opSalesQty         | 訂購量(KG)     | integer  | SALES_QTY                                                                      |
| opCsShipQty        | 已出貨數量(KG) | integer  | CS_SHIP_QTY                                                                    |
| opCsIssueDate      | 抱怨日         | string   | CS_ISSUE_DATE，YYYY/MM/DD                                                       |
| opCsItemLocation   | 產品所在地     | string   | CS_ITEM_LOCATION                                                               |
| opSampleDFlag      | 樣本到場       | string   | SAMPLE_D_FLAG ，null:'NA'; A:1~3天內; B:4~10天; C:10天以上                      |
| opCsRemark         | 客訴細節描述   | string   | CS_REMARK                                                                      |
| opCsRTFlag         | 要求換貨       | string   | CS_RT_FLAG "Y":"Yes"/"N":"No"                                                  |
| opCsRTQty          | 換貨量(KG)     | integer  | CS_RT_QTY                                                                      |
| opCsRefundFlag     | 賠償要求       | string   | CS_REFUND_FLAG ，"0"/"1"/"2"/"3"/"O"(大寫歐)                                    |
| opFirstReportFlag  | 需要初報       | string   | FIRST_REPORT_FLAG ，"Y":"需要初報"/"N":"不需初報"                               |
| opCsRefundCurrency | 賠償金額       | string   | CS_REFUND_CURRENCY ，預設 null，NTD/USD/RMB                                      |
| opFirstReportDate  | 初報回覆       | string   | FIRST_REPORT_DATE ，YYYY/MM/DD                                                  |
| opCsReportDate     | 終報回覆       | string   | CS_REPORT_DATE，YYYY/MM/DD                                                      |
| opFinalDiagFlag    | 終判           | string   | FINAL_DIAG_FLAG ，null:'NA'/'L':'奇菱責'/'S':'菱翔則'/'C':'客責'/'N':'無法判別' |
| opInsId            | WF表單ID       | string   | WF_INS_ID                                                                      |
| opClaimFlag        | 案件類別(cbo)  | string   | C.CLAIM_FLAG                                                                   |

### 【opLOT】object
| 欄位           | 名稱     | 資料型別 | 資料儲存 & 說明                 |
|----------------|----------|----------|---------------------------------|
| opCsLOTNo      | LOT No.  | string   | CS_LOT_NO                       |
| opProdUnitName | 製造單位 | string   | PRODUNIT_NAME                   |
| opEquipName    | 生產機台 | string   | EQUIP_NAME                      |
| opCsLOTQty     | 生產數量 | integer  | CS_LOT_QTY                      |
| opMFGDate      | 生產日期 | string   | MFG_DATE，預設 `null`，YYYY/MM/DD |


### 【opComment】array object
| 欄位           | 名稱         | 資料型別 | 資料儲存 & 說明                     |
|----------------|------------|----------|-------------------------------------|
| opCsComment    | 客訴處理說明 | string   | CS_COMMENT                          |
| opUpdateByName | 人員         | string   | USER_NAME renamed as UPDATE_BY_NAME |
| opUDT          | 日期         | string   | UDT，預設 `null`，YYYY/MM/DD hh:mm:ss |


### 【opDoc】array object
| 欄位           | 名稱          | 資料型別 | 資料儲存 & 說明                     |
|----------------|---------------|----------|-------------------------------------|
| opDocUid       | Doc Uid       | string   | CS_DOC_UID  檔案儲存名稱            |
| opDocFlag      | 檔案類別      | string   | DOC_FLAG                            |
| opDocName      | 原始檔案名稱  | string   | DOC_NAME                            |
| opCreateByName | 上傳者/人員   | string   | USER_NAME renamed as CREATE_BY_NAME |
| opCDT          | 上傳日期/日期 | string   | CDT，預設 `null`，YYYY/MM/DD hh:mm:ss |
| opContent      | 檔案內容      | string   | base64 encoded string               |
| opDocType      | 檔案類型      | string   | DOC_TYPE                            |


### 【opDiag】array object
| 欄位           | 名稱     | 資料型別 | 資料儲存 & 說明                     |
|----------------|--------|----------|-------------------------------------|
| opDFCode       | 不良現象 | string   | DF_CODE                             |
| opDiagFlag     | 真因分類 | string   | DIAG_FLAG                           |
| opDiagRemark   | 真因描述 | string   | DIAG_REMARK                         |
| opUpdateByName | 更新人員 | string   | USER_NAME renamed as UPDATE_BY_NAME |
| opUDT          | 日期     | string   | UDT，預設 `null`，YYYY/MM/DD hh:mm:ss |

### 【opCloseReport】object
| 欄位                 | 名稱             | 資料型別 | 資料儲存 & 說明 |
|----------------------|----------------|----------|-----------------|
| opUpdateDate         | 品保報告更新日期 | string   | UDT_E           |
| opUpdatePerson       | 品保報告更新人員 | string   | UUN_E           |
| opCloseReportContent | 品保報告         | string   | CS_CLOSE_REPORT |

#### Response 範例
```json
{
   "msgCode":null,
   "result":{
      "content":[
         {  
            "opCsProgress":{
               "opRegistration":"2015/03/03",
               "opDispatch":"2015/03/04",
               "opInitAnalyze":"2015/03/05",
               "opInitResponse":"2015/03/06",
               "opSolution":"2015/03/12",
               "opCompilation":"2015/03/18",
               "opReview":"2015/03/19",
               "opReply":"2015/03/18",
               "opClose":"進行中",
               "opProcessingPeriod":19
            },
            "opCsSingle":{
               "opCsNo":"C1502002",
               "opCsStatus":"E",
               "opCsSubject":"C09910XAA 客戶反應生產有銀紋",
               "opCustName":"家登精密",
               "opDestCustName":"台積電",
               "opCsGrade":"3",
               "opPatternNo":"C09910XAA",
               "opGradeNo":"透明ABS長效抗靜電",
               "opCsItemType":"粒材",
               "opDfA":"射出表面銀紋",
               "opDfs":"--/--/--",
               "opCsDfQty":3000,
               "opCsUsedQty":335,
               "opCsNonQty":2665,
               "opSalesOrderNo":null,
               "opSalesQty":3000,
               "opCsShipQty":3000,
               "opCsIssueDate":"2015/02/26",
               "opCsItemLocation":"台灣",
               "opSampleDFlag":"1~3天內",
               "opCsRemark":"客戶反應近期兩批料生產成型有銀紋
                             Lot No. : 4T02S251  * 1,000 KG 及Lot No. : 5T02J201 * 2,000 KG",
               "opCsRTFlag":"Yes",
               "opCsRTQty":2665,
               "opCsRefundFlag":"交換料",
               "opFirstReportFlag":"需要初報",
               "opCsRefundCurrency":null,
               "opFirstReportDate":"2015/03/06",
               "opCsReportDate":"2015/03/18",
               "opFinalDiagFlag":"奇菱責" //這部分應該放在opDiag結構中，為了PG好修改，因此放在這邊(算SingleCS資訊)
            },
            "opLOT":[
               {
                  "opCsLOTNo":"4T02S251",
                  "opProdUnitName":"染色課",
                  "opEquipName":"染色#2",
                  "opCsLOTQty":2000,
                  "opMFGDate":"2014/09/24"
               },
               {
                  "opCsLOTNo":"5T02J201",
                  "opProdUnitName":"染色課",
                  "opEquipName":"染色#2",
                  "opCsLOTQty":2000,
                  "opMFGDate":"2015/01/19"
               }],
            "opComment":[
               {
                  "opCsComment":"進行彙整103.9.24 , 104.12.11 , 104.1.20生產PDA資料",
                  "opUpdateByName":"陳進丁",
                  "opUDT":"2015/03/10 12:08:58"
               },
               {
                  "opCsComment":"3/6 不良品50kg寄回奇菱再押出+抽真空,客戶測試結果OK",
                  "opUpdateByName":"周鉦斌",
                  "opUDT":"2015/03/13 17:03:01"
               },
               {
                  "opCsComment":"客戶端不良批1,775kg已退回奇菱待安排重工",
                  "opUpdateByName":"周鉦斌",
                  "opUDT":"2015/03/13 17:03:01"
               }],
            "opDoc":[
               {
                  "opDocUid":"6760e02f-cd3f-4a71-ffd2-71748cf4065a",
                  "opDocFlag":"報表紀錄表單",
                  "opDocName":"C09910XAA生產參數比較1.xlsx",
                  "opCreateByName":"陳進丁",
                  "opCDT":"2015/03/10 12:08:58",
                  "opContent":"base64 encoded string",
                  "opDocType":"application/vnd.openxmlformats-officedocument.presentationml.presentation"
               },
               {
                  "opDocUid":"f305938f-c59d-480b-c496-71aa931e3a7e",
                  "opDocFlag":"終報",
                  "opDocName":"20150116_家登C09910XAA起蒼不良VER3.ppt",
                  
                  "opCreateByName":"周鉦斌",
                  "opCDT":"2015/03/18 10:12:41",
                  "opContent":"base64 encoded string",
                  "opDocType":"application/vnd.ms-powerpoint"
               }
               ],
            "opDiag":[
               {
                  "opDFCode":"射出表面銀紋",
                  "opDiagFlag":"料-原料混練不均",
                  "opDiagRemark":"抗靜電劑分配不均勻導致不良",
                  "opUpdateByName":"周鉦斌",
                  "opUDT":"2015/03/13 09:48:08",
                  "opContent":"base64 encoded string"
               }],
            "opCloseReport":{
               "opUpdateDate":"2015/03/18",
               "opUpdatePerson":"周鉦斌",
               "opCloseReportContent":"
               1. 不良現象:成型品表面有銀痕
               2. 不良原因: 初步判定為抗靜電劑分散問題，但需再觀察幾批生產訂單再定論。
               3. 不良品處理方式:
                  1)不良品50kg寄回奇菱再押出+抽真空,客戶測試結果OK(3/6)
                  2)客戶端還有不良批1,775kg,安排退回奇菱重工
               4. 改善對策:
                  1)減少模頭孔數，增加背壓，提高混練性 -預計4/10前新模頭裝上產線(18孔數模頭,3mm孔徑)
                  2)安裝新模頭後，試產一批產品，由品管與製工紀錄生產狀況，業務至客戶端跟線
                    確認改善狀況
               "
            },
            
         }
      ]
   }
}
```

