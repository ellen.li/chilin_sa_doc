# SOC管理-檢查是否有SOC資料
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**


## 修改歷程

| 修改時間 | 內容             | 修改者 |
| -------- | ---------------- | ------ |
| 20230814 | 新增規格         | Ellen  |
| 20230925 | opProdType改選填 | Ellen  |

## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    | /soc/check_state |
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。  
| 欄位       | 名稱     | 資料型別 | 必填  | 來源資料 & 說明                      |
| ---------- | -------- | -------- | :---: | ------------------------------------ |
| opProdType | 產品型態 | string   |   O   | C:染色 / M:複材 / B:押板 預設 `null` |
| opItemNo   | 料號     | string   |   M   |                                      |
| opEquipId  | 機台代號 | string   |   M   |                                      |
| opBomUid   | BOM代碼  | string   |   O   | 預設 `null`                          |


#### Request 範例

```json
{
  "opProdType": "C",
  "opItemNo": "LX0036XMFXTXA14B35",
  "opEquipId": "11",
  "opBomUid": "C3B04AB5-B6A0-4263-AA0C-6715B92E3EE9"
}
```

# Request 後端流程說明

可參考舊版 PsManageAction.chkSocStatus
* 若{opProdType}等於C，執行 SQL-1 檢查是否有染色SOC，若有資料opState為Y
* 若{opProdType}等於M，執行 SQL-2 檢查是否有複材SOC，若有資料opState為Y
* 若{opProdType}等於B，執行 SQL-3 檢查是否有押板SOC，若有資料opState為Y
* 若{opProdType}不等於C,M,B之一，opState為N
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1: 是否有染色SOC
```sql
SELECT
  COUNT(*) CNT
FROM
  SOC_C 
WHERE
  STATUS in ('Y','W')
  AND ITEM_NO = {opItemNo}
  AND EQUIP_ID = {opEquipId}
```

SQL-2: 是否有複材SOC
```sql
SELECT
  COUNT(*) CNT
FROM
  SOC_M 
WHERE
  STATUS in ('Y','W')
  AND ITEM_NO = {opItemNo}
  AND EQUIP_ID = {opEquipId}
  AND BOM_UID = {opBomUid}
```

SQL-1: 是否有押板SOC
```sql
SELECT
  COUNT(*) CNT
FROM
  SOC_B 
WHERE
  STATUS in ('Y','W')
  AND ITEM_NO = {opItemNo}
  AND EQUIP_ID = {opEquipId}
```

# Response 欄位
| 欄位    | 名稱      | 資料型別 | 資料儲存 & 說明    |
| ------- | --------- | -------- | ------------------ |
| opState | 是否有SOC | string   | Y/N  Y: 是 / N: 否 |


#### Response 範例

```json
{
    "msgCode": null,
    "result": {
       "content":{
          "opState": "Y"
      }
    }
}
```