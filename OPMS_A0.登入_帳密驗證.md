# 使用者登入-帳密驗證
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

使用者登入使用

## 修改歷程

| 修改時間 | 內容                                                                          | 修改者 |
|----------|-----------------------------------------------------------------------------|--------|
| 20230323 | 新增規格                                                                      | Nick   |
| 20230615 | 回傳新增 opOrganizationId                                                     | Nick   |
| 20231107 | refreshToken 時間從 30 延長到 15(token) + 30 =45，避免 idle 時間被限縮只有 15分(30-15) | Nick   |

## 來源URL及方法

| 項目   | 說明        |
|--------|-------------|
| URL    | /sso/verify |
| method | post        |

## Header 定義

| Header Key   | Header Value                    | 說明 |
|--------------|---------------------------------|------|
| Content-Type | application/json; charset=UTF-8 |      |

## Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位     | 名稱     | 資料型別 | 必填 | 資料儲存 & 說明 |
|----------|--------|:--------:|:----:|-----------------|
| opUserId | 使用者ID |  string  |  M   |                 |
| opPwd    | 密碼     |  string  |  M   |                 |

## Request 範例

```json
{
    "opUserId": "richman",
    "opPwd": "f+4Vb2GBjrDDFIV07qFPcFYsSXoF0+dLUMTY5S87yQlI6XaCVCLTrhULjqEgAWMIXao9lTzC3jkMpB6YLVi3H6XIBWhvy2NYJ2KtCnm2rnpwwcRh4sTbfLo6O982NpOdXMNM0zP8keesVeyDurYxx7WUa5RJe4LuGigVWZMcqmv+1uOfJTkJich5ylFBBgI6yJPrpCDxvuFrjbrlXy7umVXJuOCBbr/cASkTCulqsdHSA+Ici1jL7PKBI2exfgWFpA9lWAu9jktHhWHIQPSUOFvXCi9upr6pRXhdNo0LM6VuaygEtxmP77jhf4m6qCp8BJEKflbvtrQPs5N+I9z9gA=="
}
```

## Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* {opPwd} 使用 RSA 私鑰解密成 {opPwdDe} 
* 取得使用者資料(SQL-1)，無資料回 401, error.message = System.login.message.  login.ail.passworderror 對應文字，結束程式。
  ldapSite = SYS_USER.LDAP_SITE
  userId = SYS_USER.USER_ID
  passowrd = SYS_USER.PASSWORD
  username = SYS_USER.USER_NAME
* 檢查使用者是否走 AD 認證(SYS_USER.LDAP_SITE != null )
* 1-1. 走 AD 認證  (LDAP 連線可參考原程式)
  
  * 檢查{password}是否空白，空白回傳 401, error.message = System.login.message.login.fail.passworderror 對應文字，結束程式。
  * 與 LDAP 主機認證取得 UserCN
    
    * 查詢 SYS_LDAP_CONFIG(SQL-2) 取得 \<List\>SysLdapConfigTo  
       **FOR COUNT=1 TO \<List\>SysLdapConfigTo.size**  
      
      * 使用LDAP參數  
        
        
      * 如有 Exception 需儲存 logger(SLF4J)，並繼續跑FOR 迴圈  
        
        
      
      &emsp;  answer(列舉) = LdapContext.search(searchBase, searchFilter,searchCtls)  
      &emsp;  **IF answer.hasMoreElements**  
      &emsp;  &emsp;抓取下個列舉成員  
      &emsp;  &emsp;**IF 成員.attr["distinguishedName"] != null**  
      &emsp;  &emsp;&emsp;returnValue = attr["distinguishedName"]  
      &emsp;  &emsp;&emsp;Exit For  
      &emsp;  &emsp;**END IF**  
      &emsp;  **END IF**  
      **END FOR**  
  * 抓取 LDAP設定 :
    查詢 SYS_LDAP_CONFIG(SQL-3)是否可抓取到 LDAP 設定 \<List\>SysLdapConfigTo(實際List裡面只有一筆)，無法取得回傳 401, error.message = System.login.message.login.fail.passworderror 對應文字，結束程式。
  * LDAP參數
    
    | 參數                            | 內容值                              |
    | ------------------------------- | ---------------------------------- |
    | Context.PROVIDER_URL            | SysLdapConfigTo.ldapUrl            |
    | Context.INITIAL_CONTEXT_FACTORY | SysLdapConfigTo.ldapFactory        |
    | Context.SECURITY_AUTHENTICATION | SysLdapConfigTo.ldapAuthentication |
    | Context.SECURITY_PRINCIPAL      | returnValue                        |
    | Context.SECURITY_CREDENTIALS    | {opPwdDe}                             |
    
    * Exception 需儲存 logger(SLF4J)
      
      
* 1-2. 非 AD 認證
  
  * 檢查使用者帳密:
    encodedPassword = encode({opPwdDe}) 使用 Tiny Encryption Algorithm
    TEA 相關演算法請參考 com.clt.framework.security.Encryptor 及 com.clt.framework.security.EncryptionAlgorithm
    判斷 SYS_USER.USER_ID ={opUserId} 及 SYS_USER.PASSWORD=encodedPassword
    ，不相符回傳 401, error.message = System.login.message.login.fail.passworderror 對應文字，結束程式。
  * 檢查帳號是否啟用:
    SYS_USER.ACTIVE 是否等於 1 (啟用),不為 1 回傳 401， 錯誤訊息為 System.login.message.login.fail.account.inactive 對應文字，結束程式。
* 回傳結果：
* 有系統錯誤 return 500,"SYSTEM_ERROR", 正常回傳 200
* 產生 Bearer Token
  
  1. 使用 SYS_USER.USER_ID 產生JWT Token
  2. expired 15M
* 產生 refreshToken
  
  1. 使用 SYS_USER.USER_ID 產生refreshToken
  2. expired 45M


## Request 後端邏輯說明

SQL-1:

```sql
SELECT * FROM SYS_USER Where U.ACTIVE = '1' AND U.USER_ID={opUserId}
```

SQL-2:

```sql
SELECT A.LDAP_SITE, A.LDAP_URL, A.LDAP_FACTORY, A.LDAP_CN, 
A.LDAP_AUTHENTICATION, A.LDAP_USERNAME, A.LDAP_PASSWORD, A.CDT 
FROM SYS_LDAP_CONFIG A Order by A.LDAP_SITE
```

SQL-3:

```sql
SELECT A.LDAP_SITE, A.LDAP_URL, A.LDAP_FACTORY, A.LDAP_CN, 
A.LDAP_AUTHENTICATION, A.LDAP_USERNAME, A.LDAP_PASSWORD, A.CDT 
FROM SYS_LDAP_CONFIG A WHERE LDAP_SITE=' + ldapSite
```


## Response 欄位

| 欄位             | 名稱         | 資料型別 | 資料儲存 & 說明          |
|------------------|--------------|----------|--------------------------|
| opUserId         | 用戶 id      | string   | SYS_USER.USER_ID         |
| opName           | 用戶姓名     | string   | SYS_USER.USER_NAME       |
| opToken          | token        | string   | JWT token                |
| opRefreshToken   | refreshToken | string   | JWT token                |
| opOrganizationId | 工廠別       | string   | SYS_USER.ORGANIZATION_ID |


## Response 範例
```json
{
  "msgCode": null, 
  "result":{
    "content": {
      "opUserId": "tpidev2",
      "opName": "TPI Developer",
      "opToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJfaWQiOjEsInVzZXJfbmFtZSI6IkFuZHkxMCIsInVzZXJfbWFpbCI6ImFuZHlAZ21haWwuY29tIn0sImV4cCI6MTUxNTczODUxOSwiaWF0IjoxNTE1NzM3NjE5fQ.CLPeXhcxl2mdsL6-sUNFHFYABkTxmzx3YxEPyNih_FM",
      "opRefreshToken": "eyAAAhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7InVzZXJfaWQiOjEsInVzZXJfbmFtZSI6IkFuZHkxMCIsInVzZXJfbWFpbCI6ImFuZHlAZ21haWwuY29tIn0sImV4cCI6MTUxNTczODUxOSwiaWF0IjoxNTE1NzM3NjE5fQ.CLPeXhcxl2mdsL6-sUNFHFYABkTxmzx3YxEPyNih_FM",
      "opOrganizationId": "5000"
    }
  }
}
```
