# 產量回報-取得產量回報列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得產量回報列表


## 修改歷程

| 修改時間 | 內容        | 修改者 |
| -------- | ----------- | ------ |
| 20230811 | 新增規格    | shawn  |
| 20230829 | 調整api網址 | ellen  |

## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    | /psr/list_report |
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         | 名稱     | 資料型別 | 必填  | 資料儲存 & 說明 |
| ------------ | -------- | :------: | :---: | --------------- |
| opQueryDateS | 開始日期 |  string  |   O   | yyyy/MM/dd      |
| opQueryDateE | 結束日期 |  string  |   O   | yyyy/MM/dd      |
| opEquipId    | 機台     |  string  |   M   |                 |

#### Request 範例

```json
{
  "opQueryDateS":"2023/01/01",  
  "opQueryDateE":"2023/02/01",    
  "opEquipId":"1"
}
```

# Request 後端流程說明

可參考舊版 PsManageAction.ajaxQueryMfgReport
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 依傳入條件查詢sql-1，回傳列表資料
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
SELECT
  R.PSR_UID,
  W.WO_UID,
  W.PATTERN_NO,
  R.EQUIP_ID,
  E.EQUIP_NAME,
  W.WO_NO,
  W.ITEM_NO,
  W.PATTERN_NO,
  W.CTN_WEIGHT,
  W.WO_QTY,
  CONVERT(varchar(10), W.DD_MFG, 111) DD_MFG,
  R.SHIFT_MFG_QTY,
  R.SHIFT_PRE_QTY,
  CONVERT(varchar(10), R.REP_MFG_DATE, 111) AS REP_MFG_DATE,
  R.REP_SHIFT_CODE,
  case R.REP_SHIFT_CODE when 'A' then '早'
	when 'B' then '中'
	when 'C' then '晚'
	else null end REP_SHIFT_CODE_NAME,
  R.REP_SHIFT_HOURS,
  R.REP_CTN_DESC,
  R.WRN_FLAG,
  case when WRN_FLAG = 'Y' then '@'+WRN_REMARK 
	else WRN_REMARK end WRN_REMARK,
  R.REP_MIN_MFG_QTY,
  CASE
    WHEN R.REP_SHIFT_CODE = 'C' AND
      R.REP_TIME_LINE > '12' THEN '0'
    ELSE '1'
  END AS TIME_ORDER,
  R.REP_TIME_LINE,
  P.SHIFT_PLN_QTY,
  CASE WHEN WO_NO IS NULL THEN '停車'
	else '['+ ITEM_NO + ']'+WO_NO END WO_INFO
FROM PS_R R
INNER JOIN EQUIP_H E
  ON R.EQUIP_ID = E.EQUIP_ID
INNER JOIN PRODUNIT U
  ON E.ORGANIZATION_ID = U.ORGANIZATION_ID
  AND E.PRODUNIT_NO = U.PRODUNIT_NO
LEFT JOIN (SELECT
  WW.WO_UID,
  WW.WO_NO,
  BB.ITEM_NO,
  II.PATTERN_NO,
  II.MTR_NAME,
  WW.CTN_WEIGHT,
  WW.DD_MFG,
  WW.SALES_QTY WO_QTY
FROM WO_H WW
INNER JOIN BOM_MTM BB
  ON WW.BOM_UID = BB.BOM_UID
INNER JOIN ITEM_H II
  ON BB.ITEM_NO = II.ITEM_NO) W
  ON R.WO_UID = W.WO_UID
LEFT JOIN (SELECT
  PS.MFG_DATE,
  PS.SHIFT_CODE,
  PS.WO_UID,
  SUM(PS.SHIFT_PLN_QTY) SHIFT_PLN_QTY
FROM PS_H PS
INNER JOIN PS_R RR
  ON PS.MFG_DATE = RR.REP_MFG_DATE
  AND PS.SHIFT_CODE = RR.REP_SHIFT_CODE
  AND PS.WO_UID = RR.WO_UID
WHERE 
--日期有傳入才加入條件
[ 
  PS.MFG_DATE >= CONVERT(datetime, {opQueryDateS})
  AND PS.MFG_DATE <= CONVERT(datetime, {opQueryDateE})
]
GROUP BY PS.MFG_DATE,
         PS.SHIFT_CODE,
         PS.WO_UID) P
  ON R.REP_MFG_DATE = P.MFG_DATE
  AND R.REP_SHIFT_CODE = P.SHIFT_CODE
  AND R.WO_UID = P.WO_UID
WHERE 
R.EQUIP_ID = {opEquipId}
--日期有傳入才加入條件
[ 
  AND R.REP_MFG_DATE >= CONVERT(datetime, {opQueryDateS})
  AND R.REP_MFG_DATE <= CONVERT(datetime,{opQueryDateE})
] 
ORDER BY R.EQUIP_ID,
R.REP_MFG_DATE,
R.REP_SHIFT_CODE,
TIME_ORDER,
R.REP_TIME_LINE,
W.WO_NO
```


# Response 欄位
| 欄位               | 名稱        | 資料型別 | 資料儲存 & 說明                       |
| ------------------ | ----------- | -------- | ------------------------------------- |
| opPsrUid           | 產量回報UID | string   | PSR_UID(sql-1)                        |
| opItemNo           | 料號        | string   | ITEM_NO(sql-1)                        |
| opWoUid            | 工單ID      | string   | WO_UID(sql-1)                         |
| opWoNo             | 工單號碼    | string   | WO_NO(sql-1)                          |
| opEquipId          | 機台代號    | string   | EQUIP_ID(sql-1)                       |
| opEquipName        | 機台名稱    | string   | EQUIP_NAME(sql-1)                     |
| opRepShiftCode     | 班別代碼    | string   | REP_SHIFT_CODE(sql-1)                 |
| opRepShiftCodeName | 班別名稱    | string   | REP_SHIFT_CODE_NAME(sql-1)            |
| opRepMfgDate       | 日期        | string   | REP_MFG_DATE(sql-1)                   |
| opRepTimeLine      | 期間        | string   | REP_TIME_LINE(sql-1)                  |
| opRepShiftHours    | 時數        | string   | REP_SHIFT_HOURS(sql-1)                |
| opWoInfo           | 資訊        | string   | WO_INFO(sql-1)                        |
| opShiftMfgQty      | 實際產量    | string   | SHIFT_MFG_QTY(sql-1)                  |
| opRepMinMfgQty     | 吐出量      | string   | REP_MIN_MFG_QTY(sql-1)                |
| opWrnRemark        | 異常回報    | string   | WRN_REMARK(sql-1)                     |
| opWrnFlag          | 是否為異常  | string   | WRN_FLAG(sql-1)    Y/N  Y: 是 / N: 否 |
| opShiftPreQty      | 預染數量    | string   | SHIFT_PRE_QTY(sql-1)                  |
| opCtnWeight        | 桶重        | string   | CTN_WEIGHT(sql-1)                     |
| opRepCtnDesc       | 桶次        | string   | REP_CTN_DESC(sql-1)                   |


#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":[
		{
       "opPsrUid":"C79F4FEB-F608-4A9B-92CF-1A2A97F56500",
       "opItemNo":"2001F09801B8-01D",
       "opWoUid":"8EC7A383-D2ED-461C-870E-1C22489AF67D",
       "opWoNo":"CA28130103047821",
       "opEquipId":"1",
       "opEquipName":"染色#1",
		   "opRepShiftCode":"A",    
		   "opRepShiftCodeName":"早",
		   "opRepMfgDate":"2022/01/01",
		   "opRepTimeLine":null, 		   
		   "opRepShiftHours":"5",   
		   "opWoInfo":"[2001F09801B8-01D]CA28130103047821", 
		   "opShiftMfgQty":"1024",  
		   "opRepMinMfgQty":"4.8", 
		   "opWrnRemark":"@1600-1905換色",
       "opWrnFlag":"Y",
       "opShiftPreQty":"",
       "opCtnWeight":"1024.3",
       "opRepCtnDesc":""
		},
		{
			...
		}
	]
  }
}
```

## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考
* PsManageAction.ajaxQueryMfgReport