# 染色-PDA-匯出pdf
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容                  | 修改者 |
| -------- | --------------------- | ------ |
| 20230828 | 新增規格              | 黃東俞 |
| 20231012 | 合併預覽和空白PDA邏輯 | Ellen  |

## 來源URL及資料格式

| 項目   | 說明           |
| ------ | -------------- |
| URL    | /pda/pda_c_pdf |
| method | post           |


## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             | 名稱   | 資料型別 | 必填  | 資料儲存 & 說明 |
| ---------------- | ------ | -------- | :---: | --------------- |
| opPdaUid         | PDA ID | string   |   O   | 列印PDA時需有值 |
| opSocUid         | SOC ID | string   |   O   | 預覽PDA時需有值 |
| opOrganizationId | 工廠別 | string   |   O   | 空白PDA時需有值 |


#### Request 範例

```json
{
  "opPdaUid": "D5E7ABD4-6AA5-4D53-8275-CD2A493610A8"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 若 opPdaUid有值為列印PDA
  * 將opPdaUid帶入SQL-1取得[PDA資料]
  * 將[PDA資料的]WO_H.WO_UID(工單ID)再帶入SQL-2取[工單資料]
    * 取得工單之後，再用[工單資料]的WO_H.WO_NO(工單號碼)呼叫SAP-1取回傳資料
    * 取T_ORDER的第一筆資料
      * JCoTable T_ORDER=ret.getTableParameterList().getTable("T_ORDER");
      * T_ORDER.setRow(0);
    * 若有取到資料時，將資料取代SQL-2對應的欄位值
  * [工廠別]=WO_H.ORGANIZATION_ID(SQL-2)
  * 取pres欄位:
    * 一般欄位
      * 取[SOC資料]：將[PDA資料]的SOC_C_UID(SOC ID)帶入SQL-3取[SOC資料]  (和[OPMS_F3.染色-SOC管理-PDA](./OPMS_F3.染色-SOC管理-PDA.md)相同SQL)
      * 取到的[SOC資料]直接依欄位名稱填入pres
    * 其他欄位
      * title
        * [狀態]：SOC_C.STATUS(SQL-3)="W"時，狀態="(簽核中)",否則為空字串""
        * [製造日期]=PDA_C.MFG_DATE(SQL-1)，若PDA_C.MFG_DATE為空時，則改為"　　年　月　日"
        * [班別]：若未取到CMC_SO_TYPE(SAP-1)時，則為空字串，若CMC_SO_TYPE為"A"，則[班別]="早"。若CMC_SO_TYPE為"B"，[班別]="中"，CMC_SO_TYPE不為A或B，[班別]="晚"
        * title = "染色課" + SOC_C.STATUS + 狀態 + "　日期:" + [製造日期] + "　班別:" + [班別]
      * PATTERN_NO：取ITEM_H.PATTERN_NO(SQL-2)，若為null時改填入空字串""
      * WO_NO：取WO_H.WO_NO(SQL-2)
      * SALES_QTY
        * [銷售數量]：取WO_H.SALES_QTY(SQL-2 OR SAP-1)，若為null時轉為空字串""
        * SALES_QTY = [銷售數量] + WO_H.UNIT
      * BATCH_QTY
        * [每Batch用量]：取BATCH_QTY(SQL-2)，若為null時轉為空字串""
        * BATCH_QTY = [每Batch用量] + WO_H.UNIT
      * ISO_PROD_OPT
        * 參考SQL-4取得資料填入
        * SQL-4沒取到資料時，則填入"□一般級  □醫療級"
      * MFG_QTY
        * 參考SQL-5，帶入EQUIP_ID(SQL-3)和ITEM_NO(SQL-3)
        * 有查詢到結果時，將結果轉為BigDecimal填入MFG_QTY
      * MIX_DESC
        * MIX_DESC(攪拌步驟)預設為空字串
        * 參考SQL-6，帶入WO_H.WO_UID(SQL-2)和SOC_C.EQUIP_ID(SQL-1)，若有取到資料時，再進行以下處理
        * [每Batch用量]=BATCH_QTY(SQL-2)
        * [dPackage]：若PRODUNIT_NO(SQL-2 OR SAP-1)為"211040"則[dPackage]= 1，否則[dPackage]=25
        * [是否顯示整包數]：若PRODUNIT_NO(SQL-2 OR SAP-1)為"211040"則不顯示
        * [差異數]：預設0
        * [剩餘數量]：預設BATCH_QTY(SQL-2)
        * 取BOM_MIX.MIX_DESC(SQL-6)內所有包含分母為5或3的分數字串，若有取到時則重新組合MIX_DESC字串
          * 先取分母為5的分數(可能有多個)
            * 如果此分數不是分母為5的分數最後一個
              * [比例]：將分數字串轉為小數，例如"3/5"轉為0.6
              * [公斤數]=[每Batch用量]x[比例]+[差異數]
              * [整包數]=[公斤數]/[dPackage] 無條件捨去小數
              * [差異數]=[差異數]+[公斤數]-[整包數]x[dPackage]
              * [數量文字]=[整包數]x[dPackage]+ "KG"
              * 若需顯示整包數：[數量文字] += "(" + [整包數] + "包)"
              * 將取代BOM_MIX.MIX_DESC原本此分數字串的部位以產生的[數量文字]取代
              * 計算[剩餘數量]=[剩餘數量]-([整包數]x[dPackage])
            * 如果是分母為5的分數的最後一個
                * [公斤數]=[剩餘數量]
                * [整包數]=[公斤數]/[dPackage] 無條件捨去小數
                * [差異數]=[公斤數]-[整包數]x[dPackage]
                * [數量文字]= [剩餘數量] + "KG"
                * 若需顯示整包數
                  * [數量文字] += "(" + [整包數] + "包)"
                  * 若[差異數]>0則在"包"字和")"間插入 [差異數] + "KG"
                * 將取代BOM_MIX.MIX_DESC原本此分數字串的部位以產生的[數量文字]取代
            * 再取分母為3的分數(可能有多個)，用相同步驟再做一次
        * 若取代分數後的MIX_DESC中包含"N4"字串
          * [N4]：先取WO_H.CUS_N_QUANTITY_PER(SQL-2)，若為null，改取WO_H.STD_N_QUANTITY_PER(SQL-2)
          * 有取到值時進行以下處理
          * [N4數字1] = [N4]x[每Batch用量]x0.827 取到個位數
          * [N4數字2] = [N4數字1]x0.9 取到個位數
          * [N4數字3] = [N4數字1]x1.1 取到個位數
          * [N4字串] = "N4[" + [N4數字1] + " g(" + [N4數字2] + "g~" + [N4數字3] + "g)]"
          * 將MIX_DESC中的"N4"用[N4字串]取代
        * 將MIX_DESC內的換行符號"\r\n"和"\n"用空白" "取代
      * STD_LOSS_RATE(理論損失率)
        * [理論損失率]：SOC_C.EQUIP_ID(SQL-1) < 11 時為25，否則為15
        * STD_LOSS_RATE = [理論損失率] * 100 / RAW_QTY(SQL-2) + 0.5 四捨五入至小數點兩位
      * MIX_TIME、MFG_DESC
        * 取MIX_TIME、MFG_DESC(SQL-4)，若為null則填入空字串""
      * NONSTOP_FLAG(驗色不停車指示)：若SOC_C.NONSTOP_FLAG為"Y"時，改為"●"，否則改為空字串""
      * today： 取系統時間 yyyy/mm/dd hh/mm/ss
      * ISO_DESC：
        * [工廠別]為"5000"時設為"保存期限：3年 CLT-35QAA0299 CLT-40QAA0272 Ver10"
        * [工廠別]不為"5000"時設為"保存期限：3年 LS-35QAA0077 LS-40QAA0266-Ver.04"
      * LOGO:
        * [工廠別]為"5000"時取圖檔路徑 "/jasperReport/logo.png"
        * [工廠別]不為"5000"時取圖檔路徑 "/jasperReport/logoLSO.png"
  * jrds明細行欄位(tableList):
    * [工廠別]為"5000"時新增4筆資料，
      * rowNo設為"0"~"3"
      * TIME_S,TIME_E 預設填null
      * PDA_C.SHIFT_CODE(SQL-1)為"A"時，
        * TIME_S="08:00","10:00","12:00","14:00"
        * TIME_E="10:00","12:00","14:00","16:00"
      * PDA_C.SHIFT_CODE(SQL-1)為"B"時
        * TIME_S="16:00","18:00","20:00","22:00"
        * TIME_E="18:00","20:00","22:00","00:00"
      * PDA_C.SHIFT_CODE(SQL-1)為"C"時，
        * TIME_S="00:00","02:00","04:00","06:00"
        * TIME_E="02:00","04:00","06:00","08:00"
    * [工廠別]為"6000"新增6筆資料
      * rowNo設為"0"~"5"
      * PDA_C.SHIFT_CODE(SQL-1)為"A"時，
        * TIME_S="08:00","10:00","12:00","14:00","16:00","18:00"
        * TIME_E="10:00","12:00","14:00","16:00","18:00","20:00"
      * PDA_C.SHIFT_CODE(SQL-1)為"B"時
        * TIME_S="20:00","22:00","00:00","02:00","04:00","06:00"
        * TIME_E="22:00","00:00","02:00","04:00","06:00","08:00"
* 若 opPdaUid為空：
  * opSocUid有值時為預覽PDA，否則為空白PDA，參考JASPER-1設定參數
    * pres:
      * 預覽PDA時，參考SQL-7取得資料填入。若為空白PDA則不須撈取資料
      * 加入其他固定參數值
        * title：'染色課　日期:　　年　月　日　班別:'
        * PATTERN_NO: 空字串 ''
        * WO_NO: 空字串 ''
        * SALES_QTY: 空字串 ''
        * BATCH_QTY: 空字串 ''
        * MFG_QTY: null
        * MIX_DESC: 空字串 ''
        * ISO_PROD_OPT: '□一般級  □醫療級'
      * NONSTOP_FLAG(驗色不停車指示)：若SOC_C.NONSTOP_FLAG為'Y'時，改為'●'，否則改為空字串''
      * today： 取系統時間 yyyy/mm/dd hh/mm/ss
      * 工廠別：預覽PDA時，取SQL-1的ORGANIZATION_ID，否則取request傳入的opOrganizationId
      * ISO_DESC：
        * 工廠別為'5000'時設為'保存期限：3年 CLT-35QAA0299 CLT-40QAA0272 Ver10'
        * 工廠別不為'5000'時設為'保存期限：3年 LS-35QAA0077 LS-40QAA0266-Ver.04'
      * LOGO:
        * 工廠別為'5000'時取圖檔路徑 '/jasperReport/logo.png'
        * 工廠別不為'5000'時取圖檔路徑 '/jasperReport/logoLSO.png'
    * jrds:
      * 工廠別為'5000' 時新增4筆資料，rowNo設為'0'~'3'，否則新增6筆資料，rowNo設為'0'~'5'
      * TIME_S 和 TIME_E 皆填入空字串
* jasper檔案位置：
    * [工廠別]為"5000"時：/jasperReport/PDA_C.jasper
    * [工廠別]不為"5000"時：/jasperReport/PDA_C_LS.jasper
* 有系統錯誤 return 500,"SYSTEM_ERROR"，正常回傳PDF檔

# Request 後端邏輯說明

SQL-1:

```sql
-- PdaQueryTo.getPdaCSingle
SELECT P.PDA_C_UID PDA_UID, P.SOC_C_UID SOC_UID, P.WO_UID, S.SOC_C_VER SOC_VER, S.ITEM_NO,
W.WO_NO, CONVERT(VARCHAR(10), P.MFG_DATE,111) MFG_DATE, P.SHIFT_CODE, S.EQUIP_ID, E.EQUIP_NAME,
CONVERT(VARCHAR(10),S.PUB_DATE,111) PUB_DATE, U1.USER_NAME PUBLIC_NAME, REPLACE(CONVERT(VARCHAR(19),P.CDT,120),'-','/') CDT,
U2.USER_NAME CREATE_BY_NAME
FROM PDA_C P
INNER JOIN SOC_C S ON P.SOC_C_UID = S.SOC_C_UID
INNER JOIN WO_H W ON P.WO_UID = W.WO_UID
INNER JOIN BOM_MTM B ON W.BOM_UID = B.BOM_UID
INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
LEFT JOIN SYS_USER U1 ON S.PUBLIC_BY = U1.USER_ID
LEFT JOIN SYS_USER U2 ON P.CREATE_BY = U2.USER_ID
WHERE P.PDA_C_UID = {opPdaUid}
```
SQL-2

```sql
--WoQueryDao.getWoHSingleByUid
SELECT H.WO_UID, H.ACTIVE_FLAG, H.BOM_UID, H.EQUIP_ID, H.ORGANIZATION_ID, H.WO_NO, H.WO_SEQ, H.LOT_NO, H.CUST_PO_NO,
CONVERT(VARCHAR(10),H.WO_DATE,111) AS WO_DATE, H.PRODUNIT_NO, H.CUST_NO, H.CUST_NAME,
H.INDIR_CUST, B.CUST_NAME AS BASE_CUST, H.UNIT, H.WEIGHT, B.CMDI_NO, B.CMDI_NAME, B.COLOR, H.[USE], H.SALES_NO,
case H.RAW_QTY when cast(H.RAW_QTY as int) then cast(CONVERT(int,H.RAW_QTY) as varchar(20)) else cast(CONVERT(FLOAT,H.RAW_QTY) as varchar(20)) end RAW_QTY,
case H.BATCH_QTY when cast(H.BATCH_QTY as int) then cast(CONVERT(int,H.BATCH_QTY) as varchar(20)) else cast(CONVERT(FLOAT,H.BATCH_QTY) as varchar(20)) end BATCH_QTY,
H.BATCH_TIMES, case H.REMAIN_QTY when cast(H.REMAIN_QTY as int) then cast(CONVERT(int,H.REMAIN_QTY) as varchar(20)) else cast(CONVERT(FLOAT,H.REMAIN_QTY) as varchar(20)) end REMAIN_QTY,
H.REMAIN_TIMES, H.MODIFY_FLAG,H.PRINT_FLAG, H.REMARK, H.BOM_REMARK, H.ERP_FLAG,
H.WIP_CLASS_CODE, H.ALT_BOM_PART_NO,H.ALT_BOM_DESIGNATOR,H.ALT_ROUTING_PART_NO,H.ALT_ROUTING_DESIGNATOR, H.COMPLETION_SUBINV,
H.COMPLETION_LOCATOR, H.SALES_ORDER_NO, H.SALES_QTY, H.OEM_ORDER_NO, CONVERT(VARCHAR(10),H.ESTIMATED_SHIP_DATE,111) AS ESTIMATED_SHIP_DATE,
I.PATTERN_NO, B.ITEM_NO, B.ITEM_DESC, B.BOM_NO, B.BOM_NAME, B.BOM_DESC, B.[USE], B.BASE_ITEM_NO, B.BASE_BOM_NO, H.STD_N_QUANTITY_PER,
H.CUS_N_QUANTITY_PER, REPLACE(CONVERT(VARCHAR(16),H.CDT,120),'-','/') AS CDT, H.CREATE_BY, REPLACE(CONVERT(VARCHAR(16),H.UDT,120),'-','/') AS UDT, H.UPDATE_BY,
B.MULTIPLE BOM_MULTIPLE, H.CTN_WEIGHT, H.PROD_REMARK, H.SALES_ORDER_SEQ, B.BIAXIS_ONLY, B.DYE_ONLY, H.GRADE_NO, H.PROD_REMARK, H.PS_FLAG, H.WO_TYPE
FROM WO_H H
INNER JOIN BOM_MTM B ON H.BOM_UID = B.BOM_UID
INNER JOIN ITEM_H I ON B.ITEM_NO = I.ITEM_NO
WHERE H.WO_UID = {PDA_C.WO_UID(SQL-1)}
```

SQL-3
```sql
SELECT E.ORGANIZATION_ID,S.EQUIP_ID, E.EQUIP_NAME, S.ITEM_NO, S.SOC_C_UID, S.SOC_C_VER SOC_VER, S.STATUS, S.MFG_QTY, S.MOLD_SCREEN_SIZE, S.SOAK_LENGTH,
S.SCREW_TYPE, S.MOLD_HOLE, S.MOLD_APETURE, S.CUTTER_TYPE, S.SCREEN_APETURE, S.SOC_DESC, S.PROD_DESC,
S.C_1_STD, S.C_1_MAX, S.C_1_MIN, S.C_2_STD, S.C_2_MAX, S.C_2_MIN, S.C_3_STD, S.C_3_MAX, S.C_3_MIN,
S.C_4_STD, S.C_4_MAX, S.C_4_MIN, S.C_5_STD, S.C_5_MAX, S.C_5_MIN, S.C_6_STD, S.C_6_MAX, S.C_6_MIN,
S.C_7_STD, S.C_7_MAX, S.C_7_MIN, S.C_8_STD, S.C_8_MAX, S.C_8_MIN, S.C_9_STD, S.C_9_MAX, S.C_9_MIN,
S.C_A_STD, S.C_A_MAX, S.C_A_MIN, S.C_B_STD, S.C_B_MAX, S.C_B_MIN, S.C_C_STD, S.C_C_MAX, S.C_C_MIN,
S.SCREW_RPM_STD, S.SCREW_RPM_MAX, S.SCREW_RPM_MIN, S.WRENCH_RATE,S.VACUUM_INDEX,
S.FEEDER_RPM_STD, S.FEEDER_RPM_MAX, S.FEEDER_RPM_MIN,
S.IRON_F_QTY, S.IRON_ITEM_QTY, S.B_TEMP_STD, S.B_TEMP_MAX, S.B_TEMP_MIN, S.S_TEMP_STD, S.S_TEMP_MAX, S.S_TEMP_MIN,
S.E_TEMP_STD, S.E_TEMP_MAX, S.E_TEMP_MIN,S.NONSTOP_FLAG,
S.MFG_DESC, S.MFG_NOTICE, S.SCREEN_FREQ, S.RECORD_FREQ,
REPLACE(CONVERT(VARCHAR(19),S.CDT,120),'-','/') CDT, S.CREATE_BY,
CONVERT(VARCHAR(10),S.PUB_DATE,111) PUB_DATE, S.PUBLIC_BY, REPLACE(CONVERT(VARCHAR(19),S.UDT,120),'-','/') UDT, S.UPDATE_BY, U.USER_NAME PUBLIC_NAME
FROM SOC_C S
INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
LEFT JOIN SYS_USER U ON S.PUBLIC_BY = U.USER_ID
WHERE S.SOC_C_UID = {SOC_C.SOC_C_UID(SQL-1)}
```

SQL-4

```sql
-- WoQueryDao.getIsoProdOpt
SELECT OPT_DESC FROM WO_SIS WHERE LIST_HEADER_ID=158 AND WO_UID = {WO_H.WO_UID(SQL-2)}
```

SQL-5
```sql
-- IepcQueryDao.getMfgQty
SELECT MAX(MFG_QTY) AS QTY
FROM (SELECT TOP 1 IE.MS_DATE,IE.MFG_QTY
    FROM IE_PC IE
    INNER JOIN EQUIP_H E ON IE.EQUIP_ID = E.EQUIP_ID
    WHERE E.PS_EQUIP_ID = {BOM_MTM.EQUIP_ID(SQL-2)} AND IE.ITEM_NO={BOM_MTM.ITEM_NO(SQL-2 OR SAP-1)}
    ORDER BY IE.MS_DATE DESC) a
```

SQL-6
```sql
-- WoQueryDao.getWoMixDescWithEquip
SELECT B.EQUIP_ID,
  B.MIX_DESC, B.MIX_TIME, B.MFG_DESC
FROM WO_H W
INNER JOIN BOM_MIX B ON W.BOM_UID = B.BOM_UID
WHERE W.WO_UID = {WO_H.WO_UID(SQL-2)}
AND (B.EQUIP_ID IS NULL OR B.EQUIP_ID = {SOC_C.EQUIP_ID(SQL-1)})
ORDER BY EQUIP_ID DESC
```

SQL-7:

```sql
-- SocCQueryDao.getSocCMap
SELECT E.ORGANIZATION_ID,S.EQUIP_ID, E.EQUIP_NAME, S.ITEM_NO, S.SOC_C_UID, S.SOC_C_VER SOC_VER, S.STATUS, S.MFG_QTY, S.MOLD_SCREEN_SIZE, S.SOAK_LENGTH,
S.SCREW_TYPE, S.MOLD_HOLE, S.MOLD_APETURE, S.CUTTER_TYPE, S.SCREEN_APETURE, S.SOC_DESC, S.PROD_DESC,
S.C_1_STD, S.C_1_MAX, S.C_1_MIN, S.C_2_STD, S.C_2_MAX, S.C_2_MIN, S.C_3_STD, S.C_3_MAX, S.C_3_MIN,
S.C_4_STD, S.C_4_MAX, S.C_4_MIN, S.C_5_STD, S.C_5_MAX, S.C_5_MIN, S.C_6_STD, S.C_6_MAX, S.C_6_MIN,
S.C_7_STD, S.C_7_MAX, S.C_7_MIN, S.C_8_STD, S.C_8_MAX, S.C_8_MIN, S.C_9_STD, S.C_9_MAX, S.C_9_MIN,
S.C_A_STD, S.C_A_MAX, S.C_A_MIN, S.C_B_STD, S.C_B_MAX, S.C_B_MIN, S.C_C_STD, S.C_C_MAX, S.C_C_MIN,
S.SCREW_RPM_STD, S.SCREW_RPM_MAX, S.SCREW_RPM_MIN, S.WRENCH_RATE,S.VACUUM_INDEX,
S.FEEDER_RPM_STD, S.FEEDER_RPM_MAX, S.FEEDER_RPM_MIN,
S.IRON_F_QTY, S.IRON_ITEM_QTY, S.B_TEMP_STD, S.B_TEMP_MAX, S.B_TEMP_MIN, S.S_TEMP_STD, S.S_TEMP_MAX, S.S_TEMP_MIN,
S.E_TEMP_STD, S.E_TEMP_MAX, S.E_TEMP_MIN,S.NONSTOP_FLAG,
S.MFG_DESC, S.MFG_NOTICE, S.SCREEN_FREQ, S.RECORD_FREQ,
REPLACE(CONVERT(VARCHAR(19),S.CDT,120),'-','/') CDT, S.CREATE_BY,
CONVERT(VARCHAR(10),S.PUB_DATE,111) PUB_DATE, S.PUBLIC_BY, REPLACE(CONVERT(VARCHAR(19),S.UDT,120),'-','/') UDT, S.UPDATE_BY, U.USER_NAME PUBLIC_NAME
FROM SOC_C S
INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
LEFT JOIN SYS_USER U ON S.PUBLIC_BY = U.USER_ID
WHERE S.SOC_C_UID = { opSocUid }
```



SAP-1：
可參考舊版 WoQueryRfc.getWoHSingle

**獲得BAPI方法**
|              | 型態   | 參數值                     | 說明 |
| ------------ | ------ | -------------------------- | ---- |
| BAPIFunction | string | Z_PP_OPMS_PRODUCTION_ORDER |      |

**設定輸入參數**
| 參數名稱 | 參數值                      | 型態         | 說明 |
| -------- | --------------------------- | ------------ | ---- |
| I_WERKS  | WO_H.ORGANIZATION_ID(SQL-2) | string       |      |
| I_AUFNR  | WO_NO(SQL-1)                | string       |      |
| I_ERDAT  |                             | JCoStructure |      |

**設定輸入參數-I_ERDAT**
| 參數名稱 | 參數值     | 型態   | 說明 |
| -------- | ---------- | ------ | ---- |
| ERDAT_FR | "00000000" | string |      |
| ERDAT_TO | "99991231" | string |      |

| 取得回傳表格名稱 |
| ---------------- |
| T_ORDER          |

| SAP欄位名稱 | SAP資料型別 | 欄位         | 說明                                                                                         |
| ----------- | :---------: | ------------ | -------------------------------------------------------------------------------------------- |
| MATNR       |   string    | 料號         | 取代 BOM_MTM.ITEM_NO                                                                         |
| COLOR       |   string    | 色相         | 取代 BOM_MTM.COLOR                                                                           |
| STAND       |   string    | 生產單位     | 過濾前綴0再取代 WO_H.PRODUNIT_NO                                                             |
| STAND_T     |   string    | 生產單位名稱 |                                                                                              |
| AUFNR       |   string    | 工單號碼     | 過濾前綴0再取代 WO_H.WO_NO                                                                   |
| ERNAM       |   string    | 業助代號     | 取代 WO_H.SALES_NO                                                                           |
| KUNNR       |   string    | 客戶代號     | 過濾前綴0再取代 WO_H.CUST_NO                                                                 |
| SORT2       |   string    | 客戶名稱     | 取代 WO_H.CUST_NAME                                                                          |
| IHREZ       |   string    | 間接客戶     | 取代 WO_H.INDIR_CUST                                                                         |
| GRADE       |   string    | Grade No     | 取代 WO_H.GRADE_NO                                                                           |
| AUART       |   string    |              |                                                                                              |
| AUART_T     |   string    |              | 取代 WO_H.WIP_CLASS_CODE                                                                     |
| MAKTX       |   string    | 料號摘要     | 取代 BOM_MTM.ITEM_DESC                                                                       |
| CUSTPO      |   string    | 客戶PO單號   | 取代 WO_H.CUST_PO_NO                                                                         |
| VORNR       |   string    |              | 過濾前綴0                                                                                    |
| ARBPL       |   string    |              |                                                                                              |
| GAMNG       |   string    |              | 四捨五入至五位小數並轉為字串                                                                 |
| GMEIN       |   string    |              | 取代 WO_H.UNIT                                                                               |
| STATUS      |   string    |              | 取代 WO_H.ACTIVE_FLAG                                                                        |
| GSTRP       |   string    | 使用日期     | 取代 WO_H.WO_DATE                                                                            |
| GSTRP       |   string    | 使用日期     | 取代 WO_H.WO_DATE  格式：yyyy/MM/dd                                                          |
| ARBPL_T     |   string    | 機台名稱     |                                                                                              |
| SOQTY       |   string    | 銷售數量     | 四捨五入至五位小數並轉為字串 取代 WO_H.SALES_QTY                                             |
| SHIP_DATE   |   string    |              | WO_H.ESTIMATED_SHIP_DATE為空且SHIP_DATE不為空才取代WO_H.ESTIMATED_SHIP_DATE 格式：yyyy/MM/dd |
| LGORT       |   string    |              | 取代 WO_H.COMPLETION_SUBINV                                                                  |
| KDAUF       |   string    |              | 過濾前綴0再取代WO_H.SALES_ORDER_NO                                                           |
| KDPOS       |   string    |              | 過濾前綴0再取代WO_H.SALES_ORDER_SEQ                                                          |
| CMC_SO_TYPE |   string    |              | 四捨五入至五位小數並轉為字串                                                                 |


JASPER-1:
**jasper檔案位置**
|          | 型態   | 參數值           | 說明 |
| -------- | ------ | ---------------- | ---- |
| RealPath | string | 參考後端流程說明 |      |

**jasper fillReport參數**
| 參數名稱 | 名稱 | 型態                | 來源資料 & 說明 |
| -------- | ---- | ------------------- | --------------- |
| pars     |      | Map<String, Object> |                 |
| jrds     |      | JRDataSource        | {tableList}     |

**pars object 參數**
| 參數名稱         | 名稱             | 型態   | 來源資料 & 說明                |
| ---------------- | ---------------- | ------ | ------------------------------ |
| ORGANIZATION_ID  | 工廠別           | string | EQUIP_H.ORGANIZATION_ID        |
| EQUIP_ID         | 機台             | string | SOC_C.EQUIP_ID                 |
| EQUIP_NAME       | 機台名稱         | string | EQUIP_H.EQUIP_NAME             |
| ITEM_NO          | 料號             | string | SOC_C.ITEM_NO                  |
| SOC_C_UID        | ID               | string | SOC_C.SOC_C_UID                |
| SOC_VER          | 版號             | string | SOC_C.SOC_C_VER                |
| STATUS           | 狀態             | string | SOC_C.STATUS                   |
| MOLD_SCREEN_SIZE | 模頭濾網         | string | SOC_C.MOLD_SCREEN_SIZE         |
| SOAK_LENGTH      | 大槽膠條浸水長度 | string | SOC_C.SOAK_LENGTH              |
| SCREW_TYPE       | 螺桿型號         | string | SOC_C.SCREW_TYPE               |
| MOLD_HOLE        | 模嘴孔數         | string | SOC_C.MOLD_HOLE                |
| MOLD_APETURE     | 模嘴孔徑         | string | SOC_C.MOLD_APETURE             |
| CUTTER_TYPE      | 切刀型態         | string | SOC_C.CUTTER_TYPE              |
| SCREEN_APETURE   | 篩網孔徑         | string | SOC_C.SCREEN_APETURE           |
| SOC_DESC         | SOC識別文字      | string | SOC_C.SOC_DESC                 |
| PROD_DESC        | 產品說明         | string | SOC_C.PROD_DESC                |
| C_1_STD          | C1標準           | string | SOC_C.C_1_STD                  |
| C_1_MAX          | C1上限           | string | SOC_C.C_1_MAX                  |
| C_1_MIN          | C1下限           | string | SOC_C.C_1_MIN                  |
| C_2_MAX          | C2標準           | string | SOC_C.C_2_STD                  |
| C_2_MAX          | C2上限           | string | SOC_C.C_2_MAX                  |
| C_2_MIN          | C2下限           | string | SOC_C.C_2_MIN                  |
| C_3_STD          | C3標準           | string | SOC_C.C_3_STD                  |
| C_3_MAX          | C3上限           | string | SOC_C.C_3_MAX                  |
| C_3_MIN          | C3下限           | string | SOC_C.C_3_MIN                  |
| C_4_STD          | C4標準           | string | SOC_C.C_4_STD                  |
| C_4_MAX          | C4上限           | string | SOC_C.C_4_MAX                  |
| C_4_MIN          | C4下限           | string | SOC_C.C_4_MIN                  |
| C_5_STD          | C5標準           | string | SOC_C.C_5_STD                  |
| C_5_MAX          | C5上限           | string | SOC_C.C_5_MAX                  |
| C_5_MIN          | C5下限           | string | SOC_C.C_5_MIN                  |
| C_6_STD          | C6標準           | string | SOC_C.C_6_STD                  |
| C_6_MAX          | C6上限           | string | SOC_C.C_6_MAX                  |
| C_6_MIN          | C6下限           | string | SOC_C.C_6_MIN                  |
| C_7_STD          | C7標準           | string | SOC_C.C_7_STD                  |
| C_7_MAX          | C7上限           | string | SOC_C.C_7_MAX                  |
| C_7_MIN          | C7下限           | string | SOC_C.C_7_MIN                  |
| C_8_STD          | C8標準           | string | SOC_C.C_8_STD                  |
| C_8_MAX          | C8上限           | string | SOC_C.C_8_MAX                  |
| C_8_MIN          | C8下限           | string | SOC_C.C_8_MIN                  |
| C_9_STD          | C9標準           | string | SOC_C.C_9_STD                  |
| C_9_MAX          | C9上限           | string | SOC_C.C_9_MAX                  |
| C_9_MIN          | C9下限           | string | SOC_C.C_9_MIN                  |
| C_A_STD          | C10標準          | string | SOC_C.C_A_STD                  |
| C_A_MAX          | C10上限          | string | SOC_C.C_A_MAX                  |
| C_A_MIN          | C10下限          | string | SOC_C.C_A_MIN                  |
| C_B_STD          | C11標準          | string | SOC_C.C_B_STD                  |
| C_B_MAX          | C11上限          | string | SOC_C.C_B_MAX                  |
| C_B_MIN          | C11下限          | string | SOC_C.C_B_MIN                  |
| C_C_STD          | C12標準          | string | SOC_C.C_C_STD                  |
| C_C_MAX          | C12上限          | string | SOC_C.C_C_MAX                  |
| C_C_MIN          | C12下限          | string | SOC_C.C_C_MIN                  |
| SCREW_RPM_STD    | 螺桿轉速         | string | SOC_C.SCREW_RPM_STD            |
| SCREW_RPM_MAX    | 螺桿轉速上限     | string | SOC_C.SCREW_RPM_MAX            |
| SCREW_RPM_MIN    | 螺桿轉速下限     | string | SOC_C.SCREW_RPM_MIN            |
| FEEDER_RPM_STD   | 餵料機轉速       | string | SOC_C.FEEDER_RPM_STD           |
| FEEDER_RPM_MAX   | 餵料機轉速下限   | string | SOC_C.FEEDER_RPM_MAX           |
| FEEDER_RPM_MIN   | 餵料機轉速下限   | string | SOC_C.FEEDER_RPM_MIN           |
| WRENCH_RATE      | 扭力             | string | SOC_C.WRENCH_RATE              |
| VACUUM_INDEX     | 真空指數         | string | SOC_C.VACUUM_INDEX             |
| IRON_F_QTY       | 磁力架鐵屑F1     | string | SOC_C.IRON_F_QTY               |
| IRON_ITEM_QTY    | 磁力架鐵屑F1     | string | SOC_C.IRON_ITEM_QTY            |
| B_TEMP_STD       | 大槽水溫標準值   | string | SOC_C.B_TEMP_STD               |
| B_TEMP_MAX       | 大槽水溫上限     | string | SOC_C.B_TEMP_MAX               |
| B_TEMP_MIN       | 大槽水溫下限     | string | SOC_C.B_TEMP_MIN               |
| S_TEMP_STD       | 小槽水溫標準值   | string | SOC_C.S_TEMP_STD               |
| S_TEMP_MAX       | 小槽水溫上限     | string | SOC_C.S_TEMP_MAX               |
| S_TEMP_MIN       | 小槽水溫下限     | string | SOC_C.S_TEMP_MIN               |
| E_TEMP_STD       | 三槽水溫標準值   | string | SOC_C.E_TEMP_STD               |
| E_TEMP_MAX       | 三槽水溫上限     | string | SOC_C.E_TEMP_MAX               |
| E_TEMP_MIN       | 三槽水溫下限     | string | SOC_C.E_TEMP_MIN               |
| NONSTOP_FLAG     | 驗色不停車       | string | SOC_C.NONSTOP_FLAG 改為'●'或'' |
| MFG_NOTICE       | 作業注意事項     | string | SOC_C.MFG_NOTICE               |
| SCREEN_FREQ      | 網目更換         | string | SOC_C.SCREEN_FREQ              |
| RECORD_FREQ      | 記錄頻率         | string | SOC_C.RECORD_FREQ              |
| CDT              | 建立日期         | string | SOC_C.CDT yyyy/mm/dd hh:mm:ss  |
| CREATE_BY        | 建立人員         | string | SOC_C.CREATE_BY                |
| PUB_DATE         | 發行日期         | string | SOC_C.PUB_DATE  yyyy/mm/dd     |
| PUBLIC_BY        | 發行人代碼       | string | SOC_C.PUBLIC_BY                |
| PUBLIC_NAME      | 發行人名稱       | string | SYS_USER.USER_NAME             |
| UDT              | 更新日期         | string | SOC_C.UDT  yyyy/mm/dd hh:mm:ss |
| UPDATE_BY        | 更新人員         | string | SOC_C.UPDATE_BY                |
| title            |                  | string |                                |
| PATTERN_NO       |                  | string |                                |
| WO_NO            |                  | string |                                |
| SALES_QTY        |                  | string |                                |
| BATCH_QTY        |                  | string |                                |
| MFG_QTY          |                  | string |                                |
| MIX_DESC         |                  | string |                                |
| ISO_PROD_OPT     |                  | string |                                |
| STD_LOSS_RATE    |                  | string |                                |
| MIX_TIME         |                  | string |                                |
| MFG_DESC         |                  | string |                                |
| today            |                  | string | 系統日期 yyyy/mm/dd hh:mm:ss   |
| ISO_DESC         |                  | string |                                |
| LOGO             |                  | string |                                |

**jrds object 參數**
| 參數名稱 | 名稱 | 資料型別 | 資料儲存 & 說明 |
| -------- | ---- | -------- | --------------- |
| rowNo    |      | string   |                 |
| TIME_S   |      | string   |                 |
| TIME_E   |      | string   |                 |