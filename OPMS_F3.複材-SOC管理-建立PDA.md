# 複材-SOC管理-建立PDA
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20231012 | 新增規格 | Ellen |

## 來源URL及資料格式

| 項目   | 說明              |
| ------ | ----------------- |
| URL    | /soc/create_pda_m |
| method | post              |


## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位        | 名稱     | 資料型別 | 必填  | 資料儲存 & 說明         |
| ----------- | -------- | -------- | :---: | ----------------------- |
| opSocUid    | SOC ID   | string   |   M   |                         |
| opWoUid     | 工單ID   | string   |   M   |                         |
| opMfgDate   | 生產日期 | string   |   O   | yyyy/MM/dd  預設 `null` |
| opShiftCode | 班別代碼 | string   |   O   | A/B/C  預設 `null`      |

#### Request 範例

```json
{
  "opSocUid": "44CEDF0C-BE87-4CB3-B1FC-DDF98285097B",
  "opWoUid": "3CD06E36-1A97-4C8B-B50A-89BB5FE260CD"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 執行SQL-1 新增PDA資料
* 有系統錯誤 return 500,SYSTEM_ERROR,正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
-- PdaModifyDao.insertPdaM
INSERT INTO PDA_M(PDA_M_UID, SOC_M_UID, WO_UID, MFG_DATE, SHIFT_CODE, CDT, CREATE_BY, UDT, UPDATE_BY) 
VALUES(
  {產生隨機UUID},
  {opSocUid},
  {opWoUid},
  NULLIF(CONVERT(DATETIME,{opMfgDate}), ''),
  {opShiftCode}, 
  GETDATE(), 
  {登入者使用者ID},  
  GETDATE(),
  {登入者使用者ID})
```

# Response 欄位
 | 欄位     | 名稱   | 資料型別 | 資料儲存 & 說明 |
 | -------- | ------ | -------- | --------------- |
 | opPdaUid | PDA ID | string   |                 |


## Response 範例

```json
{
    "msgCode": null,
    "result": {
       "content":{
          "opPdaUid": "D5E7ABD4-6AA5-4D53-8275-CD2A493610A8"
      }
    }
}
```