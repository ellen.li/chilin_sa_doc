# 訂單出貨指示管理-取得標籤及出貨指示(表身)
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:
取得標籤及出貨指示(表身)的資料


## 修改歷程

| 修改時間 | 內容                                                | 修改者 |
|----------|---------------------------------------------------|--------|
| 20230627 | 新增規格                                            | shawn  |
| 20230710 | 調整sql-2,sql-4                                     | shawn  |
| 20230711 | 新增 opShowAssDesc 節點                             | Nick   |
| 20230720 | 補上 opShowAssDesc 後端流程說明                     | Nick   |
| 20231225 | 2.0 出貨指示設定儲存會有空白產出，預設值搜尋調整寫法 | Nick   |

## 來源URL及資料格式

| 項目   | 說明              |
|--------|-------------------|
| URL    | /sis/get_sis_so_l |
| method | post              |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             | 名稱     | 資料型別 | 必填 | 資料儲存 & 說明 |
|------------------|----------|:--------:|:----:|-----------------|
| opSoNo           | 銷售單號 |  string  |  M   |                 |
| opOrganizationId | 工廠別   |  string  |  M   |                 |
| opSoSeq          | 項次     |  string  |  O   |                 |
| opCustNo         | 客戶代號 |  string  |  O   |                 |
| opItemNo         | 料號     |  string  |  O   |                 |
| opChannel        | channel  |  string  |  O   |                 |

#### Request 範例

```json
{
  "opSoNo":"20715556",
  "opSoSeq":"10",
  "opOrganizationId":"5000",
  "opCustNo":"1006889",
  "opItemNo":"777DXXXXX1BXJ01XXX",
  "opChannel":null
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 取opSisList，查詢sql-1是否存在
  * 存在
    * 取sql-2 [configList]
  * 不存在
    * 取sql-4 [configList]，以下為迴圈執行內容: 
      * [to] 取configList第i筆
      * 取sql-5 [defaultList]，判斷是否為null
      * 判斷defaultList不為null執行以下:
        * [dto]取defaultList第一筆
        *  判斷[dto.getFlagDisplay]不為null且等於"N"，執行defaultList.clear()
      * else
        - defaultList = 執行sql-6
      * 最後判斷defaultList不為空執行以下:      
        - [dTo]取defaultList第一筆
        - to.ConfigUid = dTo.ConfigUid
        - to.OptDesc = dTo.OptDesc
        - to.AssDesc = dToAssDesc
        - to.FlagAssign = dTo.FlagAssign
        - to.ListLineId = dTo.ListLineId

  * 取opOpionList，[configList]迴圈執行sql-3
  * 自定義變數 {ShowAssDesc} 預設為 'N'，sql-3 結果中的 SHOWASSDESC 欄位有一筆為 'Y'，則configList的 {ShowAssDesc}覆寫為 'Y'
  * opSisList = configList
* 取opSoDTo
  * 查詢sql-7結果取第一筆 
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

  
# Request 後端邏輯說明
sql-1
```sql 
-- SisSoQueryDao.checkSisDExist
SELECT COUNT(*) CNT FROM SO_SIS A WHERE SO_NO={opSoNo} AND SO_SEQ={opSoSeq}
```

sql-2
```sql
--SisSoQueryDao.getSisSoForEdit
SELECT CONVERT(int, L.LIST_HEADER_ID) LIST_HEADER_ID,
CASE L.SP_INS_NAME WHEN '棧板特殊堆疊(表身)' THEN '棧板標籤擺放要求' 
	ELSE L.SP_INS_NAME END AS SP_INS_NAME,
L.CATEGORY_FLAG CATEGORY_ATTRI,
L.MODIFY_FLAG,
CASE L.LIST_HEADER_ID when 84 THEN 638 
	WHEN 154 THEN 639
	WHEN 160 THEN 640
	ELSE L.LIST_SEQ END AS LIST_SEQ_NEW,
S.CONFIG_UID,
S.OPT_DESC,
S.ASS_DESC,
S.FLAG_ASSIGN,
CONVERT(int, S.LIST_LINE_ID) LIST_LINE_ID 
FROM SYS_SIS_H L 
LEFT JOIN (
    SELECT A.SO_NO, A.SO_SEQ, A.SIS_UID, A.CONFIG_UID,A.LIST_HEADER_ID,A.LIST_LINE_ID,A.OPT_DESC,A.ASS_DESC,B.FLAG_ASSIGN 
    FROM SO_SIS A 
    INNER JOIN SYS_SIS_L B ON A.LIST_HEADER_ID=B.LIST_HEADER_ID AND A.LIST_LINE_ID=B.LIST_LINE_ID 
    WHERE A.SO_NO={opSoNo} AND A.SO_SEQ={opSoSeq}
    
) S ON L.LIST_HEADER_ID=S.LIST_HEADER_ID 
WHERE L.ORGANIZATION_ID={opOrganizationId} 
AND L.ACTIVE_FLAG='Y' AND L.MAINTAIN_FLAG='業助'
AND L.FLAG_ORDER_TYPE='L'
AND L.LIST_HEADER_ID <> 113 			
ORDER BY LIST_SEQ_NEW
```

sql-3
```sql
--SisSoQueryDao.getSisSoForEdit
SELECT 
    CASE WHEN (FLAG_ASSIGN IS NULL OR FLAG_ASSIGN = '') 
      THEN 'N'  
      ELSE FLAG_ASSIGN   
    END AS SHOWASSDESC, 
    cast(ISNULL(FLAG_ASSIGN, 'N') as varchar(20)) + cast(LIST_LINE_ID as varchar(20)) [VALUE],
    cast(ISNULL(LIST_SEQ, '') as varchar(20)) + '-' + OPT_DESC SHOW_TEXT,
    OPT_DESC [TEXT]
FROM SYS_SIS_L
WHERE LIST_HEADER_ID = { configList.LIST_HEADER_ID }
ORDER BY LIST_SEQ

```

sql-4
```sql
--SisSoQueryDao.getSisHeaderList
SELECT L.LIST_HEADER_ID,
CASE L.SP_INS_NAME WHEN '棧板特殊堆疊(表身)' THEN '棧板標籤擺放要求' 
	ELSE L.SP_INS_NAME END AS SP_INS_NAME,
L.CATEGORY_FLAG CATEGORY_ATTRI,
L.MODIFY_FLAG,
CASE L.LIST_HEADER_ID when 84 THEN 638 
	WHEN 154 THEN 639
	WHEN 160 THEN 640
	ELSE L.LIST_SEQ END AS LIST_SEQ_NEW
FROM SYS_SIS_H L 
WHERE L.ORGANIZATION_ID={opOrganizationId} 
AND L.ACTIVE_FLAG='Y' 
AND L.FLAG_ORDER_TYPE='L'
AND L.LIST_HEADER_ID <> 113
ORDER BY LIST_SEQ_NEW
```

sql-5
```sql
--SisSoQueryDao.getSisConfigDefaultL
SELECT CASE WHEN H.CUST_NO='1028028' AND H.PKG_TYPE='BX' AND NULLIF(H.CHANNEL,'') IS NOT NULL THEN 0
WHEN NULLIF(H.PKG_TYPE,'') IS NOT NULL AND NULLIF(H.ITEM_RULE,'') IS NOT NULL THEN 1
WHEN NULLIF(H.PKG_TYPE,'') IS NOT NULL AND NULLIF(H.ITEM_RULE,'') IS NULL THEN 2
WHEN NULLIF(H.ITEM_RULE,'') IS NOT NULL AND NULLIF(H.PKG_TYPE,'') IS NULL THEN 3
WHEN NULLIF(H.CUST_NO,'') IS NOT NULL AND NULLIF(H.ITEM_NO,'') IS NOT NULL THEN 4 
WHEN NULLIF(H.ITEM_NO,'') IS NULL AND NULLIF(H.CUST_NO,'') IS NOT NULL THEN 5
ELSE 6 END AS ORDER_COL,
A.CONFIG_UID,
A.LIST_HEADER_ID,
A.LIST_LINE_ID,
A.OPT_DESC,
A.ASS_DESC,
B.FLAG_ASSIGN,
B.FLAG_DISPLAY
FROM SIS_CONFIG_H H INNER JOIN SIS_CONFIG A ON A.CONFIG_UID=H.CONFIG_UID
INNER JOIN SYS_SIS_L B ON A.LIST_HEADER_ID=B.LIST_HEADER_ID AND A.LIST_LINE_ID=B.LIST_LINE_ID 
WHERE H.ORGANIZATION_ID={opOrganizationId} AND A.LIST_HEADER_ID={configList.LIST_HEADER_ID}
AND (
  (NULLIF(H.CUST_NO,'') IS NOT NULL AND NULLIF(H.ITEM_NO,'') IS NOT NULL AND H.CUST_NO={opCustNo} AND H.ITEM_NO={opItemNo}) 
  OR (NULLIF(H.ITEM_NO,'') IS NULL AND NULLIF(H.CHANNEL,'') IS NULL AND NULLIF(H.CUST_NO,'') IS NOT NULL AND H.CUST_NO={opCustNo}) 
  OR (NULLIF(H.CUST_NO,'') IS NULL AND NULLIF(H.ITEM_NO,'') IS NOT NULL AND H.ITEM_NO={opItemNo}) 
  OR (
    NULLIF(H.CUST_NO,'') IS NOT NULL AND NULLIF(H.PKG_TYPE,'') IS NOT NULL AND NULLIF(H.CHANNEL,'') IS NOT NULL AND H.CUST_NO={opCustNo} AND H.PKG_TYPE=SUBSTRING({opItemNo},11,2) AND H.CHANNEL={opChannel}
  )
  OR (
    NULLIF(H.PKG_TYPE,'') IS NOT NULL AND NULLIF(H.ITEM_RULE,'') IS NOT NULL AND H.PKG_TYPE=SUBSTRING({opItemNo},11,2) AND 
    H.ITEM_RULE_VALUE=SUBSTRING({opItemNo},CAST(SUBSTRING(REPLACE( ITEM_RULE ,'-',''), 1 + 1 -1, 1) AS INT) ,CAST(SUBSTRING(REPLACE( ITEM_RULE ,'-',''), 1 + 2 -1, 1) AS INT) ) 
  ) 
  OR (NULLIF(H.ITEM_RULE,'') IS NULL AND NULLIF(H.CHANNEL,'') IS NULL AND NULLIF(H.PKG_TYPE,'') IS NOT NULL AND H.PKG_TYPE=SUBSTRING({opItemNo},11,2))
  OR (
    NULLIF(H.PKG_TYPE,'') IS NULL AND  NULLIF(H.ITEM_RULE,'') IS NOT NULL AND H.ITEM_RULE_VALUE=SUBSTRING({opItemNo}, CAST(SUBSTRING(REPLACE( ITEM_RULE ,'-',''), 1 + 1 -1, 1) AS INT), CAST(SUBSTRING(REPLACE(ITEM_RULE, '-', ''), 1 + 2 -1, 1) AS INT) )
  ) 
)
ORDER BY ORDER_COL
```
sql-6
```sql
--SisSoQueryDao.getSisSysDefault
SELECT A.LIST_HEADER_ID,B.LIST_LINE_ID,B.OPT_DESC,B.FLAG_ASSIGN 
FROM SYS_SIS_H A
INNER JOIN SYS_SIS_L B ON A.LIST_HEADER_ID=B.LIST_HEADER_ID 
WHERE A.ORGANIZATION_ID={opOrganizationId} AND A.LIST_HEADER_ID={configList.LIST_HEADER_ID} AND B.FLAG_DEFAULT='Y'
```

sql-7
```sql
--SoQueryDao.getSoD
SELECT D.SO_NO, D.SO_SEQ, D.STATUS, D.ITEM_NO, D.ITEM_DESC, D.CUST_ITEM_NO, D.QTY, D.QTY, D.PATTERN_NO, D.COLOR, D.MTR_NAME, D.DEEP, D.WIDTH, D.LENGTH, D.SPEC, 
-- 目前前端應該用不到 
-- D.LBL_PATTERN_NO, D.LBL_COLOR, D.LBL_MTR_NAME, D.LBL_DEEP, D.LBL_WIDTH, D.LBL_LENGTH, D.LBL_SPEC, D.DD_CUSINV, 
D.ERP_FLAG, D.SIS_FLAG,REPLACE(CONVERT(VARCHAR(19),D.CDT,120),'-','/') CDT,
D.CREATE_BY,
REPLACE(CONVERT(VARCHAR(19),D.UDT,120),'-','/') UDT,
D.UPDATE_BY 
FROM SO_D D WHERE D.SO_NO={opSoNo} AND D.SO_SEQ={opSoSeq} 
```

# Response 欄位
| 欄位      | 名稱 | 資料型別 | 資料儲存 & 說明 |
|-----------|------|----------|-----------------|
| opSoDto   |      | object   |                 |
| opSisList |      | array    |                 |


### 【opSoDto】object
| 欄位         | 名稱             | 資料型別 | 資料儲存 & 說明 |
|--------------|----------------|----------|-----------------|
| opSoNo       | 銷售單號         | string   | SO_NO           |
| opSoSeq      | 銷售單序號       | string   | SO_SEQ          |
| opItemNo     | 料號             | string   | ITEM_NO         |
| opItemDesc   | 料號摘要         | string   | ITEM_DESC       |
| opCustItemNo | 客戶料號         | string   | CUST_ITEM_NO    |
| opQty        | 數量             | string   | QTY             |
| opPatternNo  | 色號             | string   | PATTERN_NO      |
| opMtrName    | 原料             | string   | MTR_NAME        |
| opColor      | 色相             | string   | COLOR           |
| opDeep       | 厚度             | string   | DEEP            |
| opWidth      | 寬度             | string   | WIDTH           |
| opLength     | 長度             | string   | LENGTH          |
| opSpec       | 規格             | string   | SPEC            |
| opSisFlag    | 出貨指示是否維護 | string   |  預設null                |

### 【opSisList】array
| 欄位            | 名稱                 | 資料型別 | 資料儲存 & 說明    |
|-----------------|----------------------|----------|--------------------|
| opSisUid        | uid                  | string   | 預設null           |
| opListHeaderId  | 出貨指示項目ID       | string   | LIST_HEADER_ID     |
| opListLineId    | 出貨指示選項ID       | string   | LIST_LINE_ID       |
| opListSeq       | 順序                 | string   | LIST_SEQ_NEW           |
| opSpInsName     | 出貨指示名稱         | string   | SP_INS_NAME        |
| opCategoryAttri | 類別                 | string   | CATEGORY_ATTRI     |
| opModifyFlag    | 編輯(是否必填)       | string   | MODIFY_FLAG        |
| opConfigUid     | uid                  | string   | CONFIG_UID         |
| opAssDesc       | 指定內容             | string   | ASS_DESC           |
| opOptDesc       | 選項內容             | string   | OPT_DESC           |
| opFlagAssign    | 是否指定內容         | string   | FLAG_ASSIGN        |
| opShowAssDesc   | 是否顯示指定內容元件 | string   | {ShowAssDesc}  Y/N |
| opOptionList    |                      | array    |                    |

### 【opOptionList】array 
| 欄位       | 名稱     | 資料型別 | 資料儲存 & 說明 |
|------------|---------|----------|-----------------|
| opValue    | 值       | string   | VALUE           |
| opText     | 選項內容 | string   | TEXT       |
| opShowText | 顯示內容 | string   | SHOW_TEXT            |
#### Response 範例

```json
{
  "msgCode": null,
  "result":{
    "content":{
        "opSoDto":{
          "opSoNo":"20715556",
          "opSoSeq":"10",
          "opItemNo":"777DXXXXX1BXJ01XXX",
          "opItemDesc":"PA-777D,BX 25KG紙袋J-01-B7",  
          "opCustItemNo":"KABS-PA777D-B",
          "opQty":"1000",       
          "opPatternNo":"J-01-B7", 
          "opMtrName":"PA-777D",   
          "opColor":"BLACK",     
          "opDeep":null,      
          "opWidth":null,     
          "opLength":null,    
          "opSpec":null     
        },
        "opSisList":[
          {
            "opSisUid":null,       
            "opListHeaderId":"47",  
            "opListLineId":null,    
            "opListSeq":"110",   
            "opSpInsName":"出庫存",        
            "opCategoryAttri":"生產",    
            "opModifyFlag":"N",       
            "opConfigUid":null,        
            "opAssDesc":null,          
            "opOptDesc":null,          
            "opFlagAssign":null,     
            "opShowAssDesc":"N",
            "opOptionList":[
            {
              "opValue":"N72",
              "opShowText":"1-出庫存不生產",
              "opText":"出庫存不生產"
            },
            {
              "opValue":"N73",
              "opShowText":"2-出庫存仍要生產",
              "opText":"出庫存仍要生產"
            }
            ]        
          }
        ]      
    }
  }
}
```

## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考

* SisSoManageAction.sisSoLEditPre