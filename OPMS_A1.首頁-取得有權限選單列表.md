# 首頁 -取得有權限選單清單
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

首頁 -取得有權限選單清單

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230405 | 新增規格 | Nick   |
| 20230517 | SYS_USER_ROLE 改 SYS_USER_ROLE_V3 | Nick |

## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    |  /menu/list |
| method | get             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
N/A
#### Request 範例
N/A!


# Request 後端流程說明

* 執行 SQL-1 查回如下圖結果
* 每一列滾資料時，需依實際資料提供子階與父階資料，完整 response 結果中，同一父階代號只提供一次資料。
* 同樣的 CODE + CNAME +PARENT_ID + 多筆 PERM_CODE 試為同一階資料
* PARENT_ID 為 0 ，代表該筆資料為父階，只需組父階資料，父階資料 {opFCode} = CODE ,{opFName} = CNAME ，{opUrl} = URL，{opParent} = PARENT_ID ，opPermsList 給 PERM_CODE 內容。 
* 如 PARENT_ID 不為 0，則該筆資料需組父、子階資料，如相同父階資料已組過就不再組，只需提供子階資料，父階資料 {opFCode} = PARENT_ID ,{opFName} = PARENT_NAME ，{opUrl} = PURL，{opParent} =0 ，opPermsList 給 null ,子階資料 {opFCode} = CODE ,{opFName} = CNAME ，{opUrl} = URL，{opParent} = PARENT_ID，opPermsList 給 PERM_CODE 內容
  
![圖 9](images/1cd8b09027e2dd8bee218aaf513a972107fba93aab82cda14519067fd7353f43.png)  

  


* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
	SELECT distinct VALID_CR.CODE,VALID_CR.CNAME,URL,PARENT_ID,PARENT_NAME,PURL,VALID_CR.PERM_CODE,VALID_CR.SORT_ID as FSORT_ID,RFP.SORT_ID as PSORT_ID  FROM 
	(
	    --過濾使用者帳號對應的角色清單
	    SELECT ROLE_ID,USER_ID FROM SYS_USER_ROLE_V3 WHERE USER_ID={登入者使用者ID}
    ) BASE_UR
	INNER JOIN
	(
		-- 所有目前完整的 角色 使用到的 功能代號清單
  		SELECT CR.CODE as CODE,FN.CNAME as CNAME,FN.URL as URL,CR.ROLE_ID as ROLE_ID,FN.PARENT as PARENT_ID,FN2.CNAME as PARENT_NAME ,FN2.URL as PURL,FN.SORT_ID as SORT_ID,PERM_CODE FROM(
		    --取得系統所有使用者的角色、功能代號、子權限對應
			SELECT CODE,ROLE_ID,null as PERM_CODE FROM SYS_ROLE_FUNCTION_V3
			UNION
			SELECT CODE,ROLE_ID,PERM_CODE FROM SYS_ROLE_FUNCTION_PERMS
		) CR
		INNER JOIN SYS_ROLE_V3 RN
		  ON CR.ROLE_ID = RN.ROLE_ID AND RN.VISIBLE=1 --過濾系統可用角色
		INNER JOIN SYS_FUNCTION_V3 FN
		  ON  CR.CODE =FN.CODE AND FN.VISIBLE=1       --過濾系統可用功能
		LEFT JOIN SYS_FUNCTION_V3 FN2
		  ON  FN.PARENT =FN2.CODE AND FN.VISIBLE=1    --找父階
	) VALID_CR
	 ON BASE_UR.ROLE_ID = VALID_CR.ROLE_ID
	 LEFT JOIN  SYS_FUNCTION_PERMS as RFP             --過濾系統可用子權限代號
	   ON VALID_CR.CODE = RFP.CODE AND VALID_CR.PERM_CODE = RFP.PERM_CODE
	ORDER BY FSORT_ID,PSORT_ID
```

# Response 欄位
#### 【content】array
| 欄位| 名稱| 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明|
| -- | --| -- | :-: | - |
|opFCode      |功能代號| string|M| 參考後端流程說明邏輯 |
|opFName      |功能名稱| string|M| 參考後端流程說明邏輯 |
|opUrl        |URL| string|M| 參考後端流程說明邏輯 |
|opParent     |父層功能代號| string|M| 參考後端流程說明邏輯 |
|opPermsList |權限清單| array|M| 參考後端流程說明邏輯 <br/>預設 `null` 無清單代表不分權限  |



#### Response 範例

```json
{
   "msgCode":"",
   "result":{
      "content":[
         {
            "opFCode":"100",
            "opFName":"BOM維護",
            "opUrl": null,
            "opParent":"0",
            "opPermsList":["QUERY","EDIT"]
         },
         {
            "opFCode":"400",
            "opFName":"生產排程",
            "opUrl": null,
            "opParent":"0",
            "opPermsList": null
         },
         {
            "opFCode":"411",
            "opFName":"已訂未交表",
            "opUrl": null,
            "opParent":"400",
            "opPermsList":null
         },
         {
            "opFCode":"404",
            "opFName":"生產排程",
            "opUrl": null,
            "opParent":"400",
            "opPermsList":null
         },
         {
            "opFCode":"407",
            "opFName":"產能對照表管理",
            "opUrl": null,
            "opParent":"400",
             "opPermsList":null
         },
         {
            "opFCode":"493",
            "opFName":"生產排程查詢",
            "opUrl": null,
            "opParent":"400",
             "opPermsList":null
         },
         {
            "opFCode":"500",
            "opFName":"製造",
            "opUrl": null,
            "opParent":"0",
             "opPermsList":null
         },
         {
            "opFCode":"511",
            "opFName":"染色:SOC管理",
            "opUrl": null,
            "opParent":"500",
            "opPermsList":null
         },
         {
            "opFCode":"512",
            "opFName":"複材:SOC管理",
            "opUrl": null,
            "opParent":"500",
            "opPermsList":null
         },
         {
            "opFCode":"513",
            "opFName":"押板:SOC管理",
            "opUrl": null,
            "opParent":"500",
            "opPermsList":null
         },
         {
            "opFCode":"514",
            "opFName":"Trouble情報管理",
            "opUrl": null,
            "opParent":"500",
            "opPermsList":null
         },
         {
            "opFCode":"521",
            "opFName":"染色:PDA列印記錄",
            "opUrl": null,
            "opParent":"500",
            "opPermsList":null
         },
         {
            "opFCode":"555",
            "opFName":"報表範例測試",
            "opUrl": "http://vm-bits02.chilintech.com.tw/ReportServer/Pages/ReportViewer.aspx?%2fCLT_Report%2fOPMS%2f%e5%b7%a5%e5%8b%99%e4%bf%ae%e7%b9%95%e7%b5%b1%e8%a8%88%e8%a1%a8_M",
            "opParent":"500",
            "opPermsList":null
         }
         
      ]
   }
}
```
