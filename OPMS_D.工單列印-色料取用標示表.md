# OPMS_D.工單列印-色料取用標示表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

列印色料取用標示表


## 修改歷程

| 修改時間 | 內容                     | 修改者 |
| -------- | ------------------------ | ------ |
| 20230808 | 新增規格                 | shawn  |
| 20230915 | 修正SQL-7                | Ellen  |
| 20230918 | 調整後端邏輯             | Ellen  |
| 20231011 | 調整後端邏輯，補凌翔部分 | Ellen  |
| 20240109 | 修正SQL-5，只取第一筆    | Ellen |

## 來源URL及資料格式

| 項目   | 說明           |
| ------ | -------------- |
| URL    | /wo/print_wo_f |
| method | post           |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位

M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。

| 欄位        | 名稱   |   資料型別   | 必填  | 資料儲存 & 說明 |
| ----------- | ------ | :----------: | :---: | --------------- |
| opWoUidList | 工單ID | array string |   M   |                 |


#### Request 範例
```json
{
  "opWoUidList": [
    "00006D90-B5F1-4A80-9F62-B9989EBAE168", 
    "00008BEB-34CC-482D-B52E-AC3E0498AD4E"
    ]
}
```

# Request 後端流程說明

請參考舊版 `WebContent\wo\WoPrintF.jsp`
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* 遍歷所有需列印的工單，{woUid}為單筆工單ID、{organizationId}為單筆工單工廠別ID
* 自定義變數{toH} = 查詢sql-1，如有資料再抓取sap工單資料，sap-1，設定{toH}各欄位值
* 自定義變數{mtrList} = 查詢sql-2
* 自定義變數{pgmList} = 查詢sql-3
* 如工廠別為"6000"時，展開二階色料：
* 遍歷{pgmList}
  * {to}為該筆資料  
  * 如 {to.selfPick}等於N 且 {to.pgmBomUid}!=null，查詢sql-8取得二階色料列表
    * {newTo}為單筆二階色料資料
    * {newTo.woNo} 等於 {toH.woNo}
    * {newTo.pgmBatchQty} 等於 `{toH.batchQty} * {newTo.quantityPer}` 到小數5位
    * {newTo.pgmRemainQty} 等於 `{toH.remainQty} * {newTo.quantityPer}` 到小數5位
    * {pgmList}新增二階色料資料
  * 刪掉已展開的色料：
    * 如 {to.selfPick}等於N 且 {to.pgmBomUid}不為空 刪除該筆{to}
* 新增變數{checkEquipIdOK:boolean} = true
* 新增變數{iFound:int}= -1
* 新增變數{fedTo:object}
* 如`{toH}.equipId為空`：
  * 查詢bomFed，sql-4：
  * 遍歷資料
    * 如`資料的{equipId}不為空`
      * checkEquipIdOK = false
    * 反之{iFound} = i
  * 最後如{iFound} >= 0，{fedTo} = 資料取第{iFound}筆
* 如`{iFound}<0`，{fedTo} = 查詢sql-5
* 如`{fedTo}不為空`：
  * 自定義變數{fedList} = 查詢sql-6
  * 自定義變數{tempList}，把pmgList的值複製過去
  * 清空{pgmList}
  * 遍歷{tempList}：
    * 自定義變數{m2}為該筆資料
    * 自定義變數{foundCount}=0
    * 如{fedList}有資料：
      * 遍歷{fedList}:
        * 自定義變數{fTo}為該筆資料
        * 如`{fTo.bomPartNo}={m2.woPgmPartNo}`
          * {foundCount}累加，{m2.FeederNo}覆寫為{fTo.FeederNo}
      * 如`下料設備有2筆以上需要拆分，{foundCount}>1`:
        * 遍歷{fedList}:
          * 自定義變數{fTo}為該筆資料
          * 如`{fTo.bomPartNo}={m2.woPgmPartNo}`
            * 自定義變數{fedM}
            * 取{m2}各欄位值覆寫到{fedM}
            * {fedM}資料放回原{pgmList}
      * 反之，{m2}資料放回原{pgmList}
    * 反之，{m2}資料放回原{pgmList}
* 如`{toH.organizationId}為5000` 執行 SQL-7 取得醫療級或一般級，只取第一筆
* 自定義變數{is1dot2}為false
* 自定義變數{totalPgmQty}為0
* 自定義變數{tableList} 
* 自定義變數{tmpList} (複材自行領料)
* {tableList}新增map資料
  * key:"groupFlag" value:"色粉室備料" 
  * key:" packName" value:"" 

* {tableList}新增map資料
  * key:"groupFlag" value:"色粉室備料" 
  * key:"packName" value:"色粉包"

* 如`{toH.organizationId}為5000`：  
  * {tableList}新增map資料
    * key:"groupFlag" value:"色粉室備料"
    * key:"packName" value:"助劑包"

* 色料資料，遍歷{pgmList}：
  * {to}為該筆資料  
  * 如`原重量為G`要換算KG
    * {to.pgmBatchQty},{to.pgmRemainQty},{to.quantityPer} 皆覆寫為除1000，四捨五入到小數3位, {to.unit}覆寫為KG
  * 如`({to.woPgmPartNo} 開頭為"05-201-W-83" 且 {to.quantityPer} <= 0.08)`
    * {isW}覆寫為true
    * {to.woNo}覆寫為"" (原邏輯用來判斷是否為1.2倍之色料)
  * 如 `{to.woPgmPartNo} 開頭為"05-251-L12"`
    * {isL12}覆寫為true
    * {to.woNo}覆寫為"" (原邏輯用來判斷是否為1.2倍之色料)
  * 如 {isW}, {isL12} 都為true
    * {is1dot2}覆寫為true
  * 如 `{to.produnitName} 開頭為"複材" 或 {toH.organizationId}為6000`，{is1dot2}覆寫為false
  * 如 `{to.pgmBatchQty} <=0 且 {to.pgmRemainQty}` 跳過此筆
  * 如 `{toH.organizationId}為6000 且 {to.selfPick}不等於N ` 跳過此筆 (凌翔只做秤料單位為色粉室)
  * {totalPgmQty} 為進行 `{to.pgmBatchQty}*{toH.batchTimes}+{to.pgmRemainQty}*{toH.remainTimes}` 的累加
  * 新增map資料 key:"packName" value:{to.woPgmPartNo} 
  * 計算包數={to.pgmBatchQty}/{to.packWeight}，ROUND_FLOOR到小數0位
  * 計算尾數={to.pgmRemainQty}/{to.packWeight}，ROUND_FLOOR到小數0位
  * 如`{toH.organizationId}為5000 且 ({to.selfPick}等於M 或 ({to.produnitName} 開頭為 "複材" 且 {to.woPgmPartNo} 開頭為 "2201W"）)`:
    * 新增map資料 key:"groupFlag" value:"複材自行領料" 
    * 如`{to.selfPick}等於M` (複材自秤，顯示總用量)：
      * 新增map資料 key:"feederNo" value:{to.feederNo} 
      * 新增map資料 key:"batchPackWeight" value:{to.pgmBatchQty}小數2位+{to.unit}
      * 新增map資料 key:"remainPackWeight" value:{to.pgmRemainQty}小數2位+{to.unit}
    * 反之 (若無標示複材自秤且為2201W,則取用整包)：
      * 如`{to.packUnit}為空或{to.packWeight}為空` 直接顯示使用量：
        * 新增map資料 key:"batchPackWeight" value:{to.pgmBatchQty}小數2位+{to.unit} 
        * 新增map資料 key:"remainPackWeight" value:{to.pgmRemainQty}小數2位+{to.unit} 
      * 反之 換算成包裝重量：
        * batchPackWeight值：         
          * 如包數大於0值為：{to.packWeight}+"Kgx"+ 包數
          * 反之給0
        * remainPackWeight值：
          * 如尾數大於0值為：{to.packWeight}+"Kgx"+ 尾數
          * 反之給0
        * 如batchPackWeight 或 remainPackWeight 不等於0
          * 新增map資料 key:"batchPackWeight" value:計算值 
          * 新增map資料 key:"remainPackWeight" value:計算值 
    * map放入{tmpList}
  * 反之
    * 新增map資料 key:"groupFlag" value:"色粉室備料" 
    * 如`{to.packUnit}為空或{to.packWeight}為空` 則跳過
    * 如`{is1dot2}為false 或 {to.woNo}為空`：
      * 如`包數大於0或尾數大於0`：
        * 新增map資料 key:"packName" value:"◎"+{to.woPgmPartNo} 
        * 值邏輯同上
        * 新增map資料 key:"batchPackWeight" value:計算值 
        * 新增map資料 key:"remainPackWeight" value:計算值  
        * map放入{tableList}
* {tmpList}放入{tmpList}末尾
* 如`{checkEquipIdOK}為false`：
  * {tableList}清除
  * 新增map資料 key:"groupFlag" value:"請通知生管指定機台以利計算下料比率"
  * map放入{tableList}
* 執行JASPER
* jasper檔案位置：
  * 工廠別為"5000"時：/jasperReport/WoPrintF.jasper
  * 工廠別不為"5000"時：/jasperReport/WoPrintF_LS.jasper
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SAP-1：
可參考舊版 WoQueryRfc.getWoHSingle
程序如有任何異常，紀錄錯誤LOG並回傳NULL
查詢SAP-API回傳結果參數如下
|              | 型態   | 參數值                     | 說明 |
| ------------ | ------ | -------------------------- | ---- |
| BAPIFunction | string | Z_PP_OPMS_PRODUCTION_ORDER |      |

| 參數名稱 | 參數值             | 型態   | 說明 |
| -------- | ------------------ | ------ | ---- |
| I_WERKS  | {opOrganizationId} | string |      |
| I_AUFNR  | {opWoNo}           | string |      |

| 取得回傳表格名稱 |
| ---------------- |
| T_ORDER          |

| 物件元素名稱      | 資料型別 | SAP欄位名稱 | SAP資料型別 | 物件元素回傳值 | 說明                                                                 |
| ----------------- | -------- | ----------- | ----------- | -------------- | -------------------------------------------------------------------- |
| itemNo            | string   | MATNR       | string      | 同SAP          |                                                                      |
| color             | string   | COLOR       | string      | 同SAP          |                                                                      |
| prodUnitNo        | string   | STAND       | string      | 同SAP          | sap回傳值做replaceFirst("^0+(?!$)", "")，如果為""給'NA'              |
| produnitName      | string   | STAND_T     | string      | 同SAP          |                                                                      |
| woNo              | string   | AUFNR       | string      | 同SAP          | sap回傳值做replaceFirst("^0+(?!$)", "")                              |
| salesNo           | string   | KUNNR       | string      | 同SAP          | sap回傳值做replaceFirst("^0+(?!$)", "")                              |
| custNo            | string   | KUNNR       | string      | 同SAP          |                                                                      |
| custName          | string   | SORT2       | string      | 同SAP          |                                                                      |
| indirCust         | string   | IHREZ       | string      | 同SAP          |                                                                      |
| gradeNo           | string   | GRADE       | string      | 同SAP          |                                                                      |
| wipClassNo        | string   | AUART       | string      | 同SAP          |                                                                      |
| wipClassCode      | string   | AUART_T     | string      | 同SAP          |                                                                      |
| itemDesc          | string   | MAKTX       | string      | 同SAP          |                                                                      |
| custPoNo          | string   | CUSTPO      | string      | 同SAP          |                                                                      |
| sapOperation      | string   | VORNR       | string      | 同SAP          | sap回傳值做replaceFirst("^0+(?!$)", "")                              |
| sapWc             | string   | ARBPL       | string      | 同SAP          |                                                                      |
| woQty             | string   | GAMNG       | double      | 同SAP          | sap回傳值四捨五入到小數5位                                           |
| rawQty            | string   |             |             |                | 如{to}.{rawQty}為空，取woQty                                         |
| unit              | string   | GMEIN       | string      | 同SAP          |                                                                      |
| activeFlag        | string   | STATUS      | string      | 同SAP          |                                                                      |
| woDate            | string   | GSTRP       | date        | 同SAP          | sap回傳值不為空則格式化yyyy/MM/dd                                    |
| equipName         | string   | ARBPL_T     | string      | 同SAP          |                                                                      |
| salesQty          | string   | SOQTY       | double      | 同SAP          | sap回傳值四捨五入到小數5位                                           |
| estimatedShipDate | string   | SHIP_DATE   | date        | 同SAP          | 如{to}的{estimatedShipDate}為空，且sap回傳值不為空則格式化yyyy/MM/dd |
| completionSubinv  | string   | LGORT       | string      | 同SAP          |                                                                      |
| salesOrderNo      | string   | KDAUF       | string      | 同SAP          | sap回傳值做replaceFirst("^0+(?!$)", "")                              |
| salesOrderSeq     | string   | KDPOS       | string      | 同SAP          | sap回傳值做replaceFirst("^0+(?!$)", "")                              |
| cmcSoType         | string   | CMC_SO_TYPE | string      | 同SAP          |                                                                      |
| sapOperation      | string   | VORNR       | string      | 同SAP          | sap回傳值做replaceFirst("^0+(?!$)", "")                              |
| sapWc             | string   | ARBPL       | string      | 同SAP          |


SQL-1:
```sql
SELECT
  H.WO_UID,
  H.ACTIVE_FLAG,
  H.BOM_UID,
  H.EQUIP_ID,
  H.ORGANIZATION_ID,
  H.WO_NO,
  H.WO_SEQ,
  H.LOT_NO,
  H.CUST_PO_NO,
  CONVERT ( VARCHAR ( 10 ), H.WO_DATE, 111 ) AS WO_DATE,
  H.PRODUNIT_NO,
  H.CUST_NO,
  H.CUST_NAME,
  H.INDIR_CUST,
  B.CUST_NAME AS BASE_CUST,
  H.UNIT,
  H.WEIGHT,
  B.CMDI_NO,
  B.CMDI_NAME,
  B.COLOR,
  H.[ USE ],
  H.SALES_NO,
CASE
  H.RAW_QTY 
  WHEN cast( H.RAW_QTY AS INT ) THEN
  cast( CONVERT ( INT, H.RAW_QTY ) AS VARCHAR ( 20 ) ) ELSE cast( CONVERT ( FLOAT, H.RAW_QTY ) AS VARCHAR ( 20 ) ) 
  END RAW_QTY,
CASE
  H.BATCH_QTY 
  WHEN cast( H.BATCH_QTY AS INT ) THEN
  cast( CONVERT ( INT, H.BATCH_QTY ) AS VARCHAR ( 20 ) ) ELSE cast( CONVERT ( FLOAT, H.BATCH_QTY ) AS VARCHAR ( 20 ) ) 
  END BATCH_QTY,
  H.BATCH_TIMES,
CASE
  H.REMAIN_QTY 
  WHEN cast( H.REMAIN_QTY AS INT ) THEN
  cast( CONVERT ( INT, H.REMAIN_QTY ) AS VARCHAR ( 20 ) ) ELSE cast( CONVERT ( FLOAT, H.REMAIN_QTY ) AS VARCHAR ( 20 ) ) 
  END REMAIN_QTY,
  H.REMAIN_TIMES,
  H.MODIFY_FLAG,
  H.PRINT_FLAG,
  H.REMARK,
  H.BOM_REMARK,
  H.ERP_FLAG,
  H.WIP_CLASS_CODE,
  H.ALT_BOM_PART_NO,
  H.ALT_BOM_DESIGNATOR,
  H.ALT_ROUTING_PART_NO,
  H.ALT_ROUTING_DESIGNATOR,
  H.COMPLETION_SUBINV,
  H.COMPLETION_LOCATOR,
  H.SALES_ORDER_NO,
  H.SALES_QTY,
  H.OEM_ORDER_NO,
  CONVERT ( VARCHAR ( 10 ), H.ESTIMATED_SHIP_DATE, 111 ) AS ESTIMATED_SHIP_DATE,
  I.PATTERN_NO,
  B.ITEM_NO,
  B.ITEM_DESC,
  B.BOM_NO,
  B.BOM_NAME,
  B.BOM_DESC,
  B.[ USE ],
  B.BASE_ITEM_NO,
  B.BASE_BOM_NO,
  H.STD_N_QUANTITY_PER,
  H.CUS_N_QUANTITY_PER,
  REPLACE ( CONVERT ( VARCHAR ( 16 ), H.CDT, 120 ), '-', '/' ) AS CDT,
  H.CREATE_BY,
  REPLACE ( CONVERT ( VARCHAR ( 16 ), H.UDT, 120 ), '-', '/' ) AS UDT,
  H.UPDATE_BY,
  B.MULTIPLE BOM_MULTIPLE,
  H.CTN_WEIGHT,
  H.PROD_REMARK,
  H.SALES_ORDER_SEQ,
  B.BIAXIS_ONLY,
  B.DYE_ONLY,
  H.GRADE_NO,
  H.PROD_REMARK,
  H.PS_FLAG,
  H.WO_TYPE 
FROM
  WO_H H
  INNER JOIN BOM_MTM B ON H.BOM_UID = B.BOM_UID
  INNER JOIN ITEM_H I ON B.ITEM_NO = I.ITEM_NO 
WHERE
  H.WO_UID = {woUid}
```

SQL-2:
```sql
SELECT
	R.WO_MTR_PART_NO,
	case
		R.QUANTITY_PER when cast(R.QUANTITY_PER as int) then cast(CONVERT(int,
		R.QUANTITY_PER) as varchar(20))
		else cast(CONVERT(FLOAT,
		R.QUANTITY_PER) as varchar(20))
	end QUANTITY_PER,
	R.UNIT
FROM
	WO_MTR R
WHERE
	R.WO_MTR_PART_NO NOT LIKE '92%'
	AND R.WO_UID ={woUid}
ORDER BY
	R.QUANTITY_PER DESC,
	R.WO_MTR_PART_NO
```

SQL-3:
```sql
SELECT
	A.WO_PGM_PART_NO,
	A.PACK_UNIT,
	A.PACK_WEIGHT,
	A.QUANTITY_PER,
	A.UNIT,
	A.SITE_PICK,
	A.SELF_PICK,
	A.PGM_BATCH_QTY,
	A.PGM_REMAIN_QTY,
	A.FEEDER_SEQ,
	B.BOM_UID PGM_BOM_UID
FROM
	(
	SELECT
		W.ORGANIZATION_ID,
		P.WO_PGM_PART_NO,
		I.UNIT PACK_UNIT,
		I.PACKING_WEIGHT PACK_WEIGHT,
		P.QUANTITY_PER,
		P.UNIT,
		SITE_PICK,
		P.SELF_PICK,
		P.PGM_BATCH_QTY,
		P.PGM_REMAIN_QTY,
		P.FEEDER_SEQ
	FROM
		WO_PGM P
	INNER JOIN WO_H W
    			  ON
		P.WO_UID = W.WO_UID
	LEFT JOIN ITEM_H I
    			  ON
		P.WO_PGM_PART_NO = I.ITEM_NO
	WHERE
		P.WO_UID ={woUid} ) A
LEFT JOIN (
	SELECT
		ORGANIZATION_ID,
		ITEM_NO,
		BOM_UID
	FROM
		BOM_MTM
	WHERE
		BOM_NO IS NULL
		AND ACTIVE_FLAG = 'Y') B
    			 ON
	A.ORGANIZATION_ID = B.ORGANIZATION_ID
	AND A.WO_PGM_PART_NO = B.ITEM_NO
ORDER BY
	WO_PGM_PART_NO

```

SQL-4: 查詢bomFed
```sql
SELECT
	A.BOM_UID,
	A.BOM_FED_UID,
	A.EQUIP_ID,
	A.F_1_RATE,
	A.F_2_RATE,
	A.F_3_RATE,
	A.F_4_RATE,
	A.F_5_RATE,
	A.F_6_RATE,
	A.BDP_RATE,
	A.PPG_RATE,
	A.DBE_RATE,
	REPLACE(CONVERT(VARCHAR(19), A.UDT, 120), '-', '/') UDT,
	A.UPDATE_BY,
	E.EQUIP_NAME,
	E.PRODUNIT_NO,
	P.PRODUNIT_NAME
FROM
	BOM_FED A
LEFT JOIN EQUIP_H E ON
	A.EQUIP_ID = E.EQUIP_ID
LEFT JOIN PRODUNIT P ON
	E.ORGANIZATION_ID = P.ORGANIZATION_ID
	AND E.PRODUNIT_NO = P.PRODUNIT_NO
WHERE
	A.BOM_UID ={toH.bomUid}
ORDER BY
	EQUIP_ID
```

SQL-5: 
```sql
SELECT TOP 1
	F.EQUIP_ID,
	F.F_1_RATE,
	F.F_2_RATE,
	F.F_3_RATE,
	F.F_4_RATE,
	F.F_5_RATE,
	F.F_6_RATE,
	F.BDP_RATE,
	CASE
		WHEN F.EQUIP_ID IS NULL THEN 1
		ELSE 0
	END EQUIP
FROM
	WO_H W,
	BOM_FED F
WHERE
	W.BOM_UID = F.BOM_UID
	AND W.WO_UID ={woUid}
	AND (F.EQUIP_ID IS NULL
		OR F.EQUIP_ID ={toH.equipId})
ORDER BY
	EQUIP
```
  
SQL-6
```sql
SELECT
	D.FEEDER_NO,
	D.BOM_PART_NO,
	D.FEEDER_RATE
FROM
	WO_H W
INNER JOIN BOM_FED F ON
	W.BOM_UID = F.BOM_UID
INNER JOIN BOM_FED_D D ON
	F.BOM_FED_UID = D.BOM_FED_UID
WHERE
	W.WO_UID ={woUid}  
  [
    -- equipId不為空
    AND F.EQUIP_ID={fedTo.equipId}
    -- equipId為空
    AND F.EQUIP_ID IS NULL
  ]
```
SQL-7: 取得醫療級或一般級
```sql
SELECT
	ISNULL((
    SELECT NULLIF(OPT_DESC, '') 
    FROM WO_SIS 
    WHERE 
      LIST_HEADER_ID = 158 
      AND WO_UID = {woUid}
  ), '□一般級  □醫療級') ISO_PROD_OPT
```

SQL-8: 取得二階色料
```sql
SELECT
  P.PGM_PART_NO WO_PGM_PART_NO,
  P.QUANTITY_PER,
  P.UNIT,
  P.SITE_PICK,
  P.SELF_PICK,
  P.FEEDER_SEQ,
  I.UNIT PACK_UNIT,
  I.PACKING_WEIGHT PACK_WEIGHT 
FROM
  BOM_PGM P
  LEFT JOIN ITEM_H I ON P.PGM_PART_NO= I.ITEM_NO 
WHERE
  P.BOM_UID = {PGM_BOM_UID(SQL-3)}
ORDER BY
  PGM_PART_NO
```

JASPER:
**jasper檔案位置**
|          | 型態   | 參數值           | 說明 |
| -------- | ------ | ---------------- | ---- |
| RealPath | string | 參考後端流程說明 |      |

**jasper fillReport參數**
| 參數名稱 | 名稱 | 型態         | 來源資料 & 說明 |
| -------- | ---- | ------------ | --------------- |
| pars     |      | array object |                 |
| jrds     |      | JRDataSource | {tableList}     |

**pars object 參數**
| 參數名稱      | 名稱           | 型態   | 來源資料 & 說明                                                           |
| ------------- | -------------- | ------ | ------------------------------------------------------------------------- |
| today         |                | string | 當下日期， yyyy/MM/dd hh:mm:ss                                            |
| patternNo     | 色號           | string | I.PATTERN_NO(SQL-1)                                                       |
| unit          | 單位           | string | H.UNIT(SQL-1)                                                             |
| woNo          | 工單號碼       | string | H.WO_NO(SQL-1)                                                            |
| produnitName  | 生產單位名稱   | string | STAND_T(SAP-1)                                                            |
| userId        | 登入者使用者ID | string | {登入者使用者ID}                                                          |
| ISO_PROD_OPT  | 醫療級或一般級 | string | ISO_PROD_OPT(SQL-7)                                                       |
| woMtrPartNo   | 原料料號       | string | R.WO_MTR_PART_NO(SQL-2) ，換行符號連接                                    |
| mtrPercentage | 原料單位用量   | string | (R.QUANTITY_PER(SQL-2) + R.UNIT(SQL-2))，換行符號連接                     |
| batchDesc     |                | string | BATCH_QTY(SQL-1) + H.UNIT(SQL-1)+ "x" + H.BATCH_TIMES + "次"              |
| remainDesc    |                | string | H.REMAIN_QTY(SQL-1) + H.UNIT(SQL-1)+ "x" + H.REMAIN_TIMES(SQL-1) + "次" ,若REMAIN_QTY等於0時REMAIN_TIMES填0 |
| totalPgmQty   |                | string | {totalPgmQty} 取到小數點後2位 加 H.UNIT(SQL-1)                            |

# Response
Content-Type: application/pdf
Content-Disposition: inline;filename=test.pdf

#### Response 範例
