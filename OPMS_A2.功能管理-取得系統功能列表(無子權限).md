# 功能管理-取得系統功能清單

###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)

取得系統功能清單(不含子權限)


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230406 | 新增規格 |  Nick  |
| 20230512 | 調整 ORDERBY 條件 |Nick|

## 來源URL及方法

| 項目   | 說明                 |
| ------ | ----------------    |
| URL    | /function/list |
| method | get                |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

## Request 欄位
N/A

## Request 範例

N/A

## Request 後端流程說明

* 執行 SQL-1 取回系統功能清單


## Request 後端邏輯說明

SQL-1:

```sql
SELECT CODE as FCODE,
       CNAME as FNAME,
       URL,
	    PARENT,
	FROM SYS_FUNCTION_V3  
   WHERE VISIBLE=1
	ORDER BY SORT_ID ASC,UDT DESC
```


## Response 欄位
| 欄位     | 名稱         | 資料型別 | 資料儲存 & 說明 |
|----------|------------|----------|-----------------|
| opFCode  | 功能代號     | string   | FCODE           |
| opFName  | 功能名稱     | string   | FNAME           |
| opUrl    | 功能URL      | string   | URL             |
| opParent | 父階功能代號 | string   | PARENT          |


## Response 範例

```json
   "msgCode":"",
   "result":{
      "content":[
         {
            "opFCode":"100",
            "opFName":"BOM維護",
            "opUrl":"http://www.google.com",
            "opParent":"0"
         },
         {
            "opFCode":"200",
            "opFName":"工單維護",
            "opUrl":null,
            "opParent":"0"
         },
         {
            "opFCode":"300",
            "opFName":"色號維護",
             "opUrl":null,
            "opParent":"0"
         },
         {
            "opFCode":"400",
            "opFName":"生產排程",
             "opUrl":null,
            "opParent":"0"
         },
         {
            "opFCode":"411",
            "opFName":"已訂未交表",
             "opUrl":null,
            "opParent":"400"
         },
         {
            "opFCode":"404",
            "opFName":"生產排程",
             "opUrl":null,
            "opParent":"400"
         },
         {
            "opFCode":"407",
            "opFName":"產能對照表管理",
            "opUrl":"http://www.google.com",
            "opParent":"400"
         },
         {
            "opFCode":"493",
            "opFName":"生產排程查詢",
             "opUrl":null,
            "opParent":"400"
         }
      ]
   }
}
```

