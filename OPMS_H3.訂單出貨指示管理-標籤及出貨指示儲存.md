# 訂單出貨指示管理-標籤及出貨指示儲存
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:
儲存標籤及出貨指示的資料


## 修改歷程

| 修改時間 | 內容                                     | 修改者 |
|----------|----------------------------------------|--------|
| 20230630 | 新增規格                                 | shawn  |
| 20230707 | 規格部分資料誤植為【opSoNo】，修正為【soSeq】 | Nick   |
| 20230721 | 修改opListHeaderId,opListLineId為必填 | shawn   |
| 20230921 | 新增LOG說明 | Nick   |

## 來源URL及資料格式

| 項目   | 說明             |
|--------|------------------|
| URL    | /sis/save_so_sis |
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             | 名稱     | 資料型別 | 必填  | 資料儲存 & 說明 |
| ---------------- | -------- | :------: | :---: | --------------- |
| opDdata           |  |  object  |   M   |
| opSisData |    |  array  |   M   |

#### 【opDdata】object
| 欄位             | 名稱     | 資料型別 | 必填  | 資料儲存 & 說明 |
| ---------------- | -------- | :------: | :---: | --------------- |
| opOrganizationId | 工廠別   |  string  |   M   |
| opSoNo       | 銷售單號         | string | M ||
| opSoSeq      | 銷售單序號       | string | O ||
| opItemNo     | 料號             | string | O ||
| opItemDesc   | 料號摘要         | string | O ||
| opCustItemNo | 客戶料號         | string | O ||
| opQty        | 數量             | string | O ||
| opPatternNo  | 色號             | string | O ||
| opMtrName    | 原料             | string | O ||
| opColor      | 色相             | string | O ||
| opDeep       | 厚度             | string | O ||
| opWidth      | 寬度             | string | O ||
| opLength     | 長度             | string | O ||
| opSpec       | 規格             | string | O ||
| opSisFlag    | 出貨指示是否維護 | string   | O | 後端會設值|    

#### 【opSisData】array
| 欄位             | 名稱           | 資料型別 | 必填 | 資料儲存 & 說明 |
|------------------|----------------|:--------:|:----:|-----------------|
| opSisUid         | uid            |  string  |  O   |  前端不用給，後端設值                |
| opSoNo           | 銷售單號       |  string  |  M   |                 |
| opSoSeq          | 銷售單序號     |  string  |  O   |                 |
| opListHeaderId   | 出貨指示項目ID |  string  |  M   |                 |
| opListLineId     | 出貨指示選項ID |  string  |  M   |                 |
| opConfigUid      | uid            |  string  |  O   |                 |
| opAssDesc        | 指定內容       |  string  |  O   |                 |
| opOptDesc        | 選項內容       |  string  |  O   |                 |

#### Request 範例

```json
{
  "opDdata":{
    "opOrganizationId":"5000",
    "opSoNo":"20715556",
    "opSoSeq":"10",
    "opItemNo":"777DXXXXX1BXJ01XXX",
    "opItemDesc":"PA-777D,BX 25KG紙袋J-01-B7",  
    "opCustItemNo":"KABS-PA777D-B",
    "opQty":"1000",       
    "opPatternNo":"J-01-B7", 
    "opMtrName":"PA-777D",   
    "opColor":"BLACK",     
    "opDeep":null,      
    "opWidth":null,     
    "opLength":null,    
    "opSpec":null,
    "opSisFlag":null     
  },
  "opSisData":[
    {
      "opSisUid":null,        
      "opSoNo":"20715556",         
      "opSoSeq":"10",        
      "opListHeaderId":"47",  
      "opListLineId":"72",       
      "opConfigUid":null,        
      "opAssDesc":null,          
      "opOptDesc":null         
    },
    ..
  ] 
} 
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* {logContent} = NULL
* opDdata儲存
  * 判斷{soSeq}是否為空
  * 是
    - 執行sql-1更新資料, 執行sql-2刪除資料
    - {logContent} = '表頭存檔'
  * 否
    - {opSisFlag} 設為"Y"
    - 查詢sql-3結果，判斷是否存在
      - 是 執行sql-4，更新資料
      - 否 執行sql-5，新增資料
    - 執行sql-6，刪除資料
    - {logContent} = '表身存檔'
* opSisData儲存
  迴圈執行
  - 設定{opSisUid} = 由程式產生uuid
  - 執行sql-7，新增資料
* 上述過程如有執行任何資料庫任何異動，執行 SQL-LOG 紀錄 LOG，{DATAKEY} = {opSoNo}
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

  
# Request 後端邏輯說明

sql-1
```sql
--SisSoModifyDao.updateSoSisFlag
UPDATE SO_H SET SIS_FLAG= 'Y', 
UDT=GETDATE(),
UPDATE_BY={登入者使用者ID}
WHERE SO_NO={opSoNo}
```

sql-2
```sql
--SisSoModifyDao.deleteSisSoH
DELETE SO_SIS WHERE SO_NO={opSoNo} AND SO_SEQ IS NULL 
```

sql-3
```sql
--SoQueryDao.checkSoDExist
SELECT COUNT(*) CNT FROM SO_D A WHERE SO_NO={opSoNo} AND SO_SEQ={opSoSeq}

```

sql-4
```sql
--SoModifyDao.updateSoD
UPDATE SO_D SET ITEM_NO={opItemNo}, ITEM_DESC={opItemDesc},
CUST_ITEM_NO={opCustItemNo}, QTY={opQty}, 
PATTERN_NO={opPatternNo}, COLOR={opColor}, 
MTR_NAME={opMtrName}, DEEP={opDeep}, 
WIDTH={opWidth}, LENGTH={opLength}, 
SPEC={opSpec}, LBL_PATTERN_NO=null, 
LBL_COLOR=null, LBL_MTR_NAME=null, 
LBL_DEEP=null, LBL_WIDTH=null, 
LBL_LENGTH=null, LBL_SPEC=null, 
SIS_FLAG={opSisFlag}, ERP_FLAG=null, 
UDT=GETDATE(), UPDATE_BY={登入者使用者ID} 
WHERE SO_NO={opSoNo} AND SO_SEQ={opSoSeq} 
```
sql-5
```sql
--SoModifyDao.insertSoD
INSERT INTO SO_D(SO_NO, SO_SEQ, STATUS, ITEM_NO, ITEM_DESC, CUST_ITEM_NO,
    QTY, PATTERN_NO, COLOR, MTR_NAME, DEEP, WIDTH, LENGTH, SPEC,
    LBL_PATTERN_NO, LBL_COLOR, LBL_MTR_NAME, LBL_DEEP, LBL_WIDTH, LBL_LENGTH, 
    LBL_SPEC, DD_CUSINV, ERP_FLAG, 
    SIS_FLAG, CDT, CREATE_BY, UDT,  UPDATE_BY
  ) 
  VALUES({opSoNo}, {opSoSeq}, 'Y', {opItemNo}, {opItemDesc}, {opCustItemNo},
    {opQty}, {opPatternNo}, {opColor}, {opMtrName}, {opDeep}, {opWidth}, {opLength}, SPEC={opSpec},
    null, null, null, null, null, null,
    null, null, null,
    {opSisFlag}, GETDATE(), {登入者使用者ID} , GETDATE(), {登入者使用者ID} 
  )
```
sql-6
```sql
--SisSoModifyDao.deleteSisSoL
DELETE SO_SIS WHERE SO_NO={opSoNo} AND SO_SEQ={opSoSeq}
```

sql-7
```sql
--SisSoModifyDao.insertSisSo
INSERT INTO SO_SIS (
  SIS_UID, SO_NO, SO_SEQ,LIST_HEADER_ID,
  LIST_LINE_ID, CONFIG_UID, OPT_DESC,ASS_DESC, 
  CDT,CREATE_BY,UDT, UPDATE_BY
)
VALUES(
  {opSisUid},{opSoNo},{opSoSeq},{opListHeaderId},
  {opListLineId},{opConfigUid},{opOptDesc},{opAssDesc},
  GETDATE(),{登入者使用者ID},GETDATE(),{登入者使用者ID}
)

```
SQL-LOG:
```sql
INSERT INTO  SYS_MODI_LOG (CATEGORY,DATAKEY,USERID,USER_NAME,LOG) VALUES ('SISSOMANAGE',{DATAKEY},{登入者使用者ID},{登入者使用者名稱}, {logContent})
```



# Response 欄位
| 欄位   | 名稱     | 資料型別 | 資料儲存 & 說明 |
|--------|--------|----------|-----------------|
| opSoNo | 銷售單號 | string   |                 |

#### Response 範例

```json
{
  "msgCode": null,
  "result":{
    "content":{
        "opSoNo":"277723"       
    }
  }
}
```

## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考

* SisSoManageAction.saveSoSis