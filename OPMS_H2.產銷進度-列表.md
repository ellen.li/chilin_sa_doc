# 產銷進度-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

查詢產銷進度


## 修改歷程

| 修改時間 | 內容                                         | 修改者 |
|----------|--------------------------------------------|--------|
| 20230801 | 新增規格                                     | 黃東俞 |
| 20230810 | 修改流程                                     | 黃東俞 |
| 20230912 | 效能優化:最多取1000筆                        | Nick   |
| 20230919 | 修改紀錄改讀SO_D_REC                         | Ellen  |
| 20231025 | 新增 後端排序(訂單號碼由大到小、項次由大到小) | Nick   |
| 20240109 | 補規格 : 排序項次部分改由小到大              | Nick   |

## 來源URL及資料格式

| 項目   | 說明       |
|--------|-----------|
| URL    | /so/list  |
| method | post      |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位            | 名稱            | 資料型別 | 必填 | 資料儲存 & 說明                              |
|-----------------|----------------|:--------:|:----:|--------------------------------------------|
| opShipDateS     | 業務交期開始日期 | string   |   O  | yyyy/mm/dd                                 |
| opShipDateE     | 業務交期結束日期 | string   |   O  | yyyy/mm/dd                                 |
| opSoDateS       | 開單日期開始日期 | string   |   O  | yyyy/mm/dd                                 |
| opSoDateE       | 開單日期結束日期 | string   |   O  | yyyy/mm/dd                                 |
| opCustName      | 客戶編號        | string   |   O  |                                            |
| opCreateUser    | 開單工號        | string   |   O  |                                            |
| opChannel       | 配銷通路        | string   |   O  | 共用參數 opType='SO',opParamId='CHANNEL'    |
| opPatternNo     | 色號            | string   |   O  |                                            |
| opType          | 生產類型        | string   |   O  | 共用參數 opType='SO',opParamId='TYPE'       |
| opProdunitNo    | 生產單位        | string   |   O  | 共用參數 opType='SO',opParamId='PROD_UNIT'  |
| opSalesOrderNo  | 訂單號碼        | string   |   O  |                                            |
| opWoNo          | 工單號碼        | string   |   O  |                                            |
| opPage          | 第幾頁          | integer  |  O   | 預設 1，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推|
| opPageSize      | 分頁筆數        | integer  |  O   | 預設 10                                                            |

#### Request 範例

```json
{
  "opShipDateS": "20230701",
  "opShipDateE": "20230731",
  "opSoDateS": "20230801",
  "opSoDateE": "20230831",
  "opCustName": null,
  "opCreateUser": null,
  "opChannel": "10",
  "opPatternNo": null,
  "opType": "1",
  "opProdunitNo": "Z510",
  "opSalesOrderNo": null,
  "opWoNo": null,
  "opPage": 1,
  "opPageSize": 10
}
```

# Request 後端流程說明

* 參考 SAP-1 自組【搜尋條件】(最多取1000筆)
  * 銷售組織 {saleOrg}：取登入者工廠別 SYS_USER.ORGANIZATION_ID，如果工廠別為 '5000' 則填入 'T090'，否則填入空字串 ''。
  * opSalesOrderNo 非null時，如果不足10位數，在前方補0至10位： String.format("%010d", Integer.valueOf(opSalesOrderNo)。
  * 前端給的條件為null時，帶入空字串。
* 如果傳入opWoNo有值，再依SAP-1回傳的AUFNR(工單號碼)，篩選符合的資料。
* AUFNR(工單號碼)有值時，再將AUFNR帶入SQL-1，取得生管交期WO_H.MFG_QTY，業務協調備註WO_H.DD_REMARK，變更記錄CNT等欄位資料。
* 將相同訂單號碼 opSalesOrderNo 和項次 opSalesOrderSeq 的資料整合成一組。
* 排序資料: opSalesOrderNo 由大到小，opSalesOrderSeq 由小到大
* 依整合後的組別進行分頁再回傳指定頁次。
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SAP-1：
可參考舊版 SoQueryRfc.getSoManageList

**獲得BAPI方法**
|              | 型態   | 參數值        | 說明 |
| ------------ | ------ | ------------ | ---- |
| BAPIFunction | string | ZSD_OPMS_004 |      |


**設定輸入參數**
| 參數名稱    | 參數值             | 型態       | 說明                                  |
| ---------- | ------------------ | -------- | -------------------------------------- |
| I_VKORG    | { saleOrg }        | string   |                                        |
| I_VTWEG    | { opChannel }      | string   |                                        |
| I_SPART    | '50'               | string   | 固定填'50'                              |
| I_ERDAT_S  | { opSoDateS }      | string   | yyyymmdd                               |
| I_ERDAT_E  | { opSoDateE }      | string   | yyyymmdd，如為null時，填入99991231      |
| I_BSTDK_S  | { opShipDateS }    | string   | yyyymmdd                               |
| I_BSTDK_E  | { opShipDateE }    | string   | yyyymmdd                               |
| I_VBELN    | { opSalesOrderNo } | string   | 非null時才要填，且前方須補0至10位數字     |
| I_KUNNR    | { opCustName }     | string   |                                        |
| I_TYPE     | { opType }         | string   |                                        |
| I_DEPT     | { opProdunitNo }   | string   |                                        |
| I_PERNR    | { opCreateUser }   | string   |                                        |
| I_COLORNO  | { opPatternNo }    | string   |                                        |


| 取得回傳表格名稱 |
| ---------------|
| ZOPMST007      |

| SAP欄位名稱 | SAP資料型別 | 欄位值   | 說明                               |
| ----------- | :---------: | -------- | -------------------------------- |
| VBELN       |   string    | 訂單號碼 | 過濾前綴0                          |
| POSNR       |   string    | 項次     | 過濾前綴0                          |
| ERDAT       |   date      | 開單日期 |                                   |
| SORT2       |   string    | 客戶     |                                   |
| MATNR       |   string    | 料號     |                                   |
| MATERIAL    |   string    | 原料     |                                   |
| KWMENG      |   string    | 訂單數量 |                                   |
| BSTDK       |   date      | 業務交期 |                                   |
| COLORNO     |   string    | 色號     |                                   |
| AUFNR       |   string    | 工單號碼 | 過濾前綴0                          |
| GAMNG       |   string    | 工單數量 |                                   |
| ERFMG       |   string    | 入庫量   |                                   |
| PRODDEPT    |   string    | 生產單位 |                                   |
| LTRMI       |   string    | 最後入庫日期 |                               |

SQL-1:

```sql
-- SoQueryDao.getSoWoSingle
SELECT H.WO_NO, H.DD_MFG, H.MFG_QTY, H.DD_REMARK, ISNULL(R.CNT, 0) CNT
FROM
  WO_H H 
  LEFT JOIN (SELECT WO_UID, COUNT (*) CNT FROM SO_D_REC GROUP BY SO_NO, SO_SEQ, WO_UID) R ON H.WO_UID= R.WO_UID 
WHERE H.WO_NO = { AUFNR }
```


# Response 欄位
| 欄位    | 名稱     | 資料型別 | 說明     |
|---------|--------|----------|----------|
| msgCode | 訊息代碼 | string   |          |
| result  | 回傳資料 | object   | 進階訊息 |

#### 【result】child node
| 欄位     | 名稱     | 資料型別 | 來源資料 & 說明 |
|----------|--------|----------|-----------------|
| pageInfo | 分頁資訊 | object   |                 |
| content  | 附帶訊息 | array    |                 |

#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明 |
|------------|------------|----------|-----------------|
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |

#### 【content】array
| 欄位            | 名稱     | 資料型別 | 資料儲存 & 說明  |
|-----------------|---------|----------|----------------|
| opSalesOrderNo  | 訂單號碼 | string   | VBELN          |
| opSalesOrderSeq | 項次    | string   | POSNR           |
| opSalesOrderData |        | array   |                 |

#### 【opSalesOrderData】 array
| 欄位            | 名稱     | 資料型別 | 資料儲存 & 說明                       |
|-----------------|---------|----------|--------------------------------------|
| opWoUid         | 工單ID  | string   | WO_H.WO_UID(SQL-1)                   |
| opCustName      | 客戶     | string  | SORT2(SAP-1)                         |
| opItemNo        | 料號     | string  | MATNR(SAP-1)                         |
| opDdSales       | 業務交期 | string  | BSTDK(SAP-1) 轉 yyyy/mm/dd 格式       |
| opDdMfg         | 生管交期 | string  | WO_H.DD_MFG(SQL-1) 轉 yyyy/mm/dd 格式 |
| opInvQty        | 入庫量   | string  | ERFMG(SAP-1)                         |
| opPatternNo     | 色號     | string  | COLORNO(SAP-1)                       |
| opWoNo          | 工單號碼 | string  | AUFNR(SAP-1)                         |
| opRawQty        | 工單數量 | string  | GAMNG(SAP-1)                         |
| opSoCdt         | 開單日期 | string  | ERDAT(SAP-1) 轉 yyyy/mm/dd 格式       |
| opMfgQty        | 產量     | string  | WO_H.MFG_QTY(SQL-1)                  |
| opMtrName       | 原料     | string  | MATERIAL(SAP-1)                      |
| opSalesQty      | 訂單數量 | string  | KWMENG(SAP-1)                        |
| opProdunitName  | 生產單位 | string  | PRODDEPT(SAP-1)                      |
| opDateCompleted | 最後入庫日期 | string |LTRMI 轉 yyyy/mm/dd 格式            |
| opDdRemark      | 業務協調備註 | string |WO_H.DD_REMARK(SQL-1)              |
| opCnt           | 變更記錄 | string  | CNT(SQL-1)                           |

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "pageInfo": {
      "pageNumber": 1,
      "pageSize": 10,
      "pages": 1,
      "total": 2
    },
    "content": [
      {
        "opSalesOrderNo": "0009012398",
        "opSalesOrderSeq": "10",
        "opSalesOrderData": [
          {
            "opCustName":"奇美實業",
            "opItemNo":"LPB1018XXXTKJ19139",
            "opDdSales":"2023/03/02",
            "opDdMfg":"2023/03/02",
            "opInvQty":"114",
            "opPatternNo":"H23202XPB",
            "opWoNo":"52019203",
            "opRawQty":"0",
            "opSoCdt":"2023/03/02",
            "opMfgQty":"0",
            "opMtrName":"PB-1202",
            "opSalesQty":"100",
            "opProdunitName":"染色少量工單(一般生產)",
            "opDateCompleted": "2023/08/01",
            "opCnt": 5
          },
          {
            "opCustName":"奇美實業",
            "opItemNo":"LPB1018XXXTKJ19139",
            "opDdSales":"2023/03/02",
            "opDdMfg":"2023/03/02",
            "opInvQty":"114",
            "opPatternNo":"H23202XPB",
            "opWoNo":"52019203",
            "opRawQty":"0",
            "opSoCdt":"2023/03/02",
            "opMfgQty":"0",
            "opMtrName":"PB-1202",
            "opSalesQty":"100",
            "opProdunitName":"染色少量工單(一般生產)",
            "opDateCompleted": "2023/08/01",
            "opCnt": 5
          },
        ]
      },
      {
        "opSalesOrderNo": "0009012398",
        "opSalesOrderSeq": "20",
        "opSalesOrderData": [
          {
            "opCustName":"奇美實業",
            "opItemNo":"LPB1018XXXTKJ19139",
            "opDdSales":"2023/03/02",
            "opDdMfg":"2023/03/02",
            "opInvQty":"114",
            "opPatternNo":"H23202XPB",
            "opWoNo":"52019203",
            "opRawQty":"0",
            "opSoCdt":"2023/03/02",
            "opMfgQty":"0",
            "opMtrName":"PB-1202",
            "opSalesQty":"100",
            "opProdunitName":"染色少量工單(一般生產)",
            "opDateCompleted": "2023/01/01",
            "opCnt": 1
          }
        ]
      }
    ]
  }
}
```