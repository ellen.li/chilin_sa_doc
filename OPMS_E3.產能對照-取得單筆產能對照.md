# 產能對照-取得單筆產能對照
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得單筆產能對照


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230828 | 新增規格 | Ellen  |

## 來源URL及資料格式

| 項目   | 說明          |
| ------ | ------------- |
| URL    | /iepc/get_one |
| method | post          |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位      | 名稱        | 資料型別 | 必填  | 資料儲存 & 說明 |
| --------- | ----------- | :------: | :---: | --------------- |
| opIepcUid | 產能資料UID |  string  |   M   |                 |

#### Request 範例

```json
{
  "opIepcUid":"5000-119E17D2-E84D-4A8C-9619-76BAA9E8199A"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 執行 SQL-1，取得產能對照資料，如果不存在 return 400 NOTFOUND
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
SELECT
  P.PRODUNIT_NAME,
  A.ORGANIZATION_ID,
  A.PRODUNIT_NO,
  A.EQUIP_NAME,
  B.IEPC_UID,
  B.EQUIP_ID,
  CONVERT ( VARCHAR ( 10 ), B.MS_DATE, 111 ) AS MS_DATE,
  B.ITEM_NO,
  B.RPM,
  B.MFG_QTY,
  B.NET_TIME,
  B.SHIFT_STD_QTY,
  B.REMARK,
  B.DEACTIVE_REMARK,
  B.ACTIVE_FLAG,
  B.STATUS,
  B.BDP,
  B.CURR,
  B.NP,
  B.HOUR_STD_QTY,
  B.PEOPLE_STD_QTY,
  A.CQ_FLAG 
FROM
  IE_PC B
  INNER JOIN EQUIP_H A ON B.EQUIP_ID = A.EQUIP_ID
  INNER JOIN PRODUNIT P ON A.PRODUNIT_NO = P.PRODUNIT_NO 
  AND A.ORGANIZATION_ID = P.ORGANIZATION_ID 
WHERE
  B.IEPC_UID = {opIepcUid}
```


# Response 欄位
| 欄位             | 名稱                     | 資料型別 | 資料儲存 & 說明                                                               |
| ---------------- | ------------------------ | -------- | ----------------------------------------------------------------------------- |
| opIepcUid        | 產能資料UID              | string   | IEPC_UID(SQL-1)                                                               |
| opOrganizationId | 工廠別                   | string   | ORGANIZATION_ID(SQL-1)                                                        |
| opActiveFlag     | 是否生效                 | string   | ACTIVE_FLAG(SQL-1)   Y/N  Y: 生效 / N: 失效                                   |
| opStatus         | 狀態                     | string   | STATUS，(SQL-1)  Y/N  Y: 非概估 / N: 概估 ，若{STATUS}等於null或空字串轉為`Y` |
| opItemNo         | 料號                     | string   | ITEM_NO(SQL-1)                                                                |
| opMsDate         | 測量日期                 | string   | MS_DATE(SQL-1)                                                                |
| opProdunitNo     | 生產單位代號             | string   | PRODUNIT_NO(SQL-1)                                                            |
| opProdunitName   | 生產單位名稱             | string   | PRODUNIT_NAME(SQL-1)                                                          |
| opEquipId        | 機台代號                 | string   | EQUIP_ID(SQL-1)                                                               |
| opEquipName      | 機台名稱                 | string   | EQUIP_NAME(SQL-1)                                                             |
| opCqFlag         | 體積或計量               | string   | CQ_FLAG(SQL-1)  C: 體積 / Q: 計量                                             |
| opBdp            | BDP含量                  | string   | BDP(SQL-1)                                                                    |
| opRpm            | 轉速(RPM) / 餵料機下料量 | string   | RPM(SQL-1)                                                                    |
| opCurr           | 電流值                   | string   | CURR(SQL-1)                                                                   |
| opHourStdQty     | 單位時間產量(Sec/Kg)     | string   | HOUR_STD_QTY(SQL-1)                                                           |
| opMfgQty         | 每分鐘產出(KG)           | string   | MFG_QTY(SQL-1)                                                                |
| opNetTime        | 換網時間(Min/Shift)      | string   | NET_TIME(SQL-1)                                                               |
| opNp             | 標準人力(人/機)          | string   | NP(SQL-1)                                                                     |
| opPeopleStdQty   | 每人單位時間產量         | string   | PEOPLE_STD_QTY(SQL-1)                                                         |
| opShiftStdQty    | 標準產能(KG/Shift)       | string   | SHIFT_STD_QTY(SQL-1)                                                          |
| opRemark         | 備註                     | string   | REMARK(SQL-1)                                                                 |


#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":{
      "opIepcUid": "5000-119E17D2-E84D-4A8C-9619-76BAA9E8199A",
      "opOrganizationId": "5000",
      "opActiveFlag": "Y",
      "opStatus": "N",
      "opItemNo": "06-199-C92B01DXXXX",
      "opMsDate": "2014/02/26",
      "opProdunitNo": "P3",
      "opProdunitName": "染色課",
      "opEquipId": "5",
      "opEquipName": "染色#9(原#3)(體積)",
      "opCqFlag": "C",
      "opBdp": "",
      "opCurr": "",
      "opHourStdQty": "9.60",
      "opMfgQty": "0.00",
      "opNetTime": "13.00",
      "opNp": "0.5",
      "opPeopleStdQty": "4.80",
      "opShiftStdQty": "3000.00",
      "opRpm": "52",
      "opRemark": ""
    }
  }
}
```