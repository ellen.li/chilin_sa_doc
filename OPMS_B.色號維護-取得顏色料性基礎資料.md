# 色號維護-取得顏色料性基礎資料
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

新增編輯取得顏色料性基礎資料

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230424 | 新增規格 | Nick   |

## 來源URL及資料格式

| 項目   | 說明               |
|--------|--------------------|
| URL    | /pattern/get_param |
| method | get                |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
N/A
#### Request 範例
N/A
# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL-1 取得顏色基礎資料
* SQL-2 取得料性基礎資料
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
--(顏色)色相選項
SELECT PARA_VALUE COLOR FROM SYS_PARA_D WHERE PARA_ID='COLOR' 
```

SQL-2
```SQL
--料性選項
SELECT PARA_VALUE MTR_TYPE FROM SYS_PARA_D WHERE PARA_ID='MTR_TYPE' 
```

# Response 欄位
| 欄位     | 名稱           | 資料型別    | 資料儲存 & 說明 |
|----------|--------------|-------------|-----------------|
| color    | 顏色(色相)選項 | arraystring | COLOR           |
| mtr_type | 料性選項       | arraystring | MTR_TYPE        |

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":{
        "color":["BLACK","BLUE","BROWN","GRAY","GREEN","IVORY","NATURAL","RED","WHITE","YELLOW","本色","透明"],
        "mtr_type":["abs","AS","ASA","MS","PC","PC/ABS","PMMA","PS","Q膠","T-ABS","擴散PC","滑石粉","玻纖","碳纖","防火ABS(溴系)","防火PC","防火PC/ABS","防火PS(溴系)"]
    }
  }
}
```

