# 產量回報-產量回報刪除
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

產量回報刪除

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230829 | 新增規格 | Ellen  |


## 來源URL及資料格式

| 項目   | 說明        |
| ------ | ----------- |
| URL    | /psr/delete |
| method | post        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。

| 欄位     | 名稱        | 資料型別 | 必填 | 資料儲存 & 說明 |
| -------- | ----------- | -------- | ---- | --------------- |
| opPsrUid | 產量回報UID | string   | M    |                 |


#### Request 範例

```json
{
  "opPsrUid": "713986FC-AA42-4283-B476-35D0F7587A48"
}
```

# Request 後端流程說明

可參考舊版 PsManageAction.DeletePsr
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* 執行SQL-1 新增產量回報LOG
* 執行SQL-2 刪除產量回報
* 上述過程如有執行任何資料庫任何異動，執行 SQL-LOG 紀錄 LOG，{DATAKEY} = {opPsrUid}
  有系統錯誤 return 500,SYSTEM_ERROR,正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1: 新增產量回報LOG
```sql
INSERT INTO PS_R_LOG (
  PSR_LOG_UID,
  PSR_UID,
  WO_UID,
  EQUIP_ID,
  REP_MFG_DATE,
  REP_SHIFT_CODE,
  SHIFT_MFG_QTY,
  SHIFT_PRE_QTY,
  REP_MIN_MFG_QTY,
  REP_SHIFT_HOURS,
  REP_TIME_LINE,
  REP_CTN_DESC,
  WRN_FLAG,
  WRN_REMARK,
  CDT,
  CREATE_BY 
) SELECT
  {產生隨機UUID},
  PSR_UID,
  WO_UID,
  EQUIP_ID,
  REP_MFG_DATE,
  REP_SHIFT_CODE,
  SHIFT_MFG_QTY,
  SHIFT_PRE_QTY,
  REP_MIN_MFG_QTY,
  REP_SHIFT_HOURS,
  REP_TIME_LINE,
  REP_CTN_DESC,
  WRN_FLAG,
  WRN_REMARK,
  GETDATE(),
  {登入者使用者ID} 
FROM
  PS_R 
WHERE
  PSR_UID = {opPsrUid}
```

SQL-2: 刪除產量回報
```sql
DELETE PS_R WHERE PSR_UID = {opPsrUid}
```

SQL-LOG:
```sql
INSERT INTO  SYS_MODI_LOG (CATEGORY,DATAKEY,USERID,USER_NAME,LOG) VALUES ('PS_R',{DATAKEY},{登入者使用者ID},{登入者使用者名稱}, '刪除')
```

# Response 欄位
 | 欄位     | 名稱        | 資料型別 | 資料儲存 & 說明 |
 | -------- | ----------- | -------- | --------------- |
 | opPsrUid | 產量回報UID | string   |                 |


## Response 範例

```json
{
    "msgCode": null,
    "result": {
       "content":{
          "opPsrUid":"713986FC-AA42-4283-B476-35D0F7587A48"
      }
    }
}
```
