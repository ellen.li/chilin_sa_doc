# 角色管理-新增修改存檔
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

角色管理-新增/修改存檔


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230405 | 新增規格 | Nick   |

## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    | /role/save |
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位    | 名稱 | 資料型別 | 必填 | 資料儲存 & 說明 |
| ------- | ---- | :------: | :--: | --------------- |
|   opRoleId |  角色ID     |  integer  |M| 新增模式 opRoleId 為 `null`         |
|   opRoleName |  角色名稱     |  string  |M|          |
|   opRoleCodeList |  角色權限清單    | arraystring|O|預設 `null`

#### Request 範例

```json
{
  "opRoleId":null,
  "opRoleName":"工務：案件管理",
  "opRoleCodeList":[
                     {"opFCode":"101","opPCode":["QUERY","EDIT"]}
                     {"opFCode":"102", "opPCode":null }
                   ]
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* {DATAKEY} = {opRoleId}
* opRoleId 非 null 則執行 SQL-1，確認角色ID是否已存在，如果不存在 [return 400,NOTFOUND]，存在繼續執行 SQL-2-1 更新角色者資訊，{logContent}內容為 "修改" 
* opRoleId 為 null ，則取最大號 + 1 `MAX(ROLE_ID) + 1` 產生一組不存在 SYS_ROLE_V3 資料表的 ROLE_ID 並指定給 {opRoleId}，並且執行 SQL-2-2 新增角色資訊，{logContent}內容為 "新增"
* 執行 SQL-3-1 及 SQL-3-2 清除原角色對應權限相關表(SYS_ROLE_FUNCTION_V3 及 SYS_ROLE_FUNCTION_PERMS )
* opRoleCodeList 如不為 null ，則執行 SQL-4-1 及 SQL-4-2 將資料更新到 SYS_ROLE_FUNCTION_V3 及 SYS_ROLE_FUNCTION_PERMS 表中
* 執行 SQL-5 紀錄LOG
  有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
SELECT * FROM  SYS_ROLE_V3 WHERE ROLE_ID={opRoleId}
```

SQL-2-1:
```sql
UPDATE SYS_ROLE_V3 SET ROLE_NAME={opRoleName},
                    UDT=SYSDATETIME(),
                    UPDATE_BY={登入者使用者ID} 
                WHERE ROLE_ID={opRoleId}
```

SQL-2-2:
```sql
INSERT INTO SYS_ROLE_V3 (ROLE_ID,ROLE_NAME,VISIBLE,CDT,CREATE_BY)
              VALUES ({opRoleId},{opRoleName},1,SYSDATETIME(),{登入者使用者ID} )
```

SQL-3-1:
```sql
DELETE FROM SYS_ROLE_FUNCTION_V3 WHERE ROLE_ID={opRoleId}
```
SQL-3-2:
```sql
DELETE FROM SYS_ROLE_FUNCTION_PERMS WHERE ROLE_ID={opRoleId}
```

SQL-4-1:
```sql
此SQL為範例，實際SQL請自己按照 opRoleCodeList 組

INSERT INTO SYS_ROLE_FUNCTION_V3(CODE,ROLE_ID,CDT,CREATE_BY) 
                          VALUES ("101",{opRoleId},SYSDATETIME(),{登入者使用者ID}), 
                                 ("101",{opRoleId},SYSDATETIME(),{登入者使用者ID})
```

SQL-4-2:
```sql
此SQL為範例，實際SQL請自己按照 opRoleCodeList 組

INSERT INTO SYS_ROLE_FUNCTION_PERMS(CODE,ROLE_ID,PERM_CODE,CDT,CREATE_BY) 
                          VALUES ("101",{opRoleId},"QUERY",SYSDATETIME(),{登入者使用者ID}),
                                 ("101",{opRoleId},"EDIT",SYSDATETIME(),{登入者使用者ID}) 
```

SQL-5:
```sql
INSERT INTO  SYS_MODI_LOG (CATEGORY,DATAKEY,USERID,USER_NAME,LOG) VALUES ('ROLE',{DATAKEY},{登入者使用者ID},{登入者使用者名稱}, {logContent})
```


# Response 欄位
| 欄位     | 名稱   | 資料型別 | 資料儲存 & 說明 |
|----------|------|----------|-----------------|
| opRoleId | 角色ID | integer  |                 |


## Response 範例

```json
{
  "msgCode": null, 
  "result":{
    "content": {
       "opRoleId":931
    }
  }
}
```

