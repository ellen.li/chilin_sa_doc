# 染色-PDA-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)


**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230828 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /pda/pda_c_list             |
| method | post                        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱        |  資料型別 | 必填 | 資料儲存 & 說明                                                     |
|--------------|-------------|----------|:----:|--------------------------------------------------------------------|
| opItemNo     | 料號        | string   |  O    |                                                                    |
| opProdunitNo | 生產單位    | string   |  O    | 參考下方說明                                                        |
| opEquipId    | 機台        | integer  |  O    | 參考下方說明                                                        |
| opBDate      | 列印日期開始 | string   |  O    | yyyy/mm/dd                                                         |
| opEDate      | 列印日期結束 | string   |  O    | yyyy/mm/dd                                                         |
| opPage       | 第幾頁      | integer  |  O    | 預設 1，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推 |
| opPageSize   | 分頁筆數    | integer  |  O    | 預設 10                                                             |
| opOrder      | 排序方式    | string   |   O   | "ASC":升冪排序 / "DESC":降冪排序   預設`DESC`                        |

* 前端生產單位選項(opProdunitNo), 由API [OPMS_F3.SOC-生產單位-列表  /soc/produnit_list](OPMS_F3.SOC-生產單位-列表.md) 取得，opOrganizationId 傳入登入者工場別，opProdType 傳入 'C'
* 前端機台選項(opEquipId)，由API [C.BOM維護-取得排程機台列表(by工廠別.生產單位.生產型態) /bom/get_ps_equipment_list](./OPMS_C.BOM維護-取得排程機台列表(by工廠別.生產單位.生產型態).md) 取得，只傳入 opProdunitNo一個欄位(可傳入null查詢全部機台)，來源為上一步由[OPMS_F.製造-生產單位-列表  /soc/produnit_list](OPMS_F3.SOC-生產單位-列表.md) 取得的 opProdunitNo

```json
{
  "opItemNo": null,
  "opProdunitNo": null,
  "opEquipId": 12,
  "opBDate": "2019/01/01",
  "opEDate": "2019/12/31",
  "opStatus": null,
  "opPage": 1,
  "opPage": 10
}
```

# Request 後端流程說明

* 取 { 登入者工廠別 } SYS_USER.ORGANIZATION_ID 欄位
* 參考SQL-1，欄位皆為非必填，有傳入的欄位才需加入SQL篩選
* 只最多取前1000筆資料並進行分頁再回傳資料
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

SQL-1:
```sql
-- PdaQueryDao.getPdaCResultList
SELECT P.PDA_C_UID PDA_UID, S.SOC_C_UID SOC_UID, S.SOC_C_VER SOC_VER, S.EQUIP_ID, E.EQUIP_NAME, S.ITEM_NO,
W.WO_NO, P.MFG_DATE, P.SHIFT_CODE, S.PUB_DATE, U1.USER_NAME PUBLIC_NAME, P.CDT, U2.USER_NAME CREATE_BY_NAME
FROM PDA_C P
INNER JOIN SOC_C S ON P.SOC_C_UID = S.SOC_C_UID
INNER JOIN WO_H W ON P.WO_UID = W.WO_UID
INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
LEFT JOIN SYS_USER U1 ON S.PUBLIC_BY = U1.USER_ID
LEFT JOIN SYS_USER U2 ON P.CREATE_BY = U2.USER_ID
WHERE E.ORGANIZATION_ID = {登入者工廠別}
AND P.CDT >= {opBDate}
AND P.CDT <= {opEDate}
AND E.PRODUNIT_NO = {opProdunitNo}
AND S.EQUIP_ID = {opEquipId}
AND S.ITEM_NO like {%opItemNo%}
ORDER BY P.CDT {opOrder}
```

# Response 欄位
| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
| -------- | -------- | -------- | --------------- |
| pageInfo | 分頁資訊 | object   |                 |
| content  | 附帶訊息 | array    |                 |
#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明 |
| ---------- | ------------ | -------- | --------------- |
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |

#### 【content】array
| 欄位        | 名稱         | 資料型別 | 資料儲存 & 說明    |
|----------------|-------------|---------|-------------------------------|
| opPdaUid       | PDA ID      | string  | PDA_UID                       |
| opSocUid       | SOC ID      | string  | SOC_C.SOC_C_UID               |
| opWoNo         | 工單號碼     | string  | WO_H.WO_NO                    |
| opEquipId      | 機台        | string  | SOC_C.EQUIP_ID                |
| opEquipName    | 機台名稱     | string  | EQUIP_H.EQUIP_NAME            |
| opItemNo       | 料號        | string  | SOC_C.ITEM_NO                 |
| opSocVer       | 版號        | string  | SOC_C.SOC_C_VER               |
| opPubDate      | 發行日期     | string  | SOC_C.PUB_DATE  yyyy/mm/dd    |
| opPublicName   | 發行人名稱   | string  | PUBLIC_NAME                   |
| opCdt          | 列印日期     | string  | PDA_C.CDT yyyy/mm/dd hh:mm:ss |
| opCreateByName | 列印人員     | string  | CREATE_BY_NAME                |
| opMfgDate      | 製造日期     | string  | MFG_DATE  yyyy/mm/dd          |

```json
{
  "msgCode":null,
  "result":{
    "pageInfo": {
      "pageNumber": 1,
      "pageSize": 10,
      "pages": 1,
      "total": 3
    },
    "content": [
      {
        "opPdaUid":"D89FBAD7-634E-4AEC-B9F6-410C6C7060B9",
        "opSocUid":"6E899047-6589-44E5-B07E-D2C13116E255",
        "opWoNo":"51031038",
        "opEquipId":"12",
        "opEquipName":"染色#2",
        "opItemNo":"LPC110UXXX25H17850",
        "opSocVer":"1",
        "opPubDate":"2023/03/23",
        "opPublicName":"陳啟銘",
        "opCdt":"2023/03/30 09:08:37",
        "opCreateByName":"陳韋學",
        "opMfgDate":"1900/01/01"
      },
      {
        "opPdaUid":"1DEE45D5-55F9-4138-8E28-1D8E72E11A50",
        "opSocUid":"6E899047-6589-44E5-B07E-D2C13116E255",
        "opWoNo":"51031038",
        "opEquipId":"12",
        "opEquipName":"染色#2",
        "opItemNo":"LPC110UXXX25H17850",
        "opSocVer":"1",
        "opPubDate":"2023/03/23",
        "opPublicName":"陳啟銘",
        "opCdt":"2023/03/30 09:00:17",
        "opCreateByName":"陳韋學",
        "opMfgDate":"1900/01/01"
      },
      {
        "opPdaUid":"62B3077F-50BE-4CA8-82C4-221F00947D42",
        "opSocUid":"6E899047-6589-44E5-B07E-D2C13116E255",
        "opWoNo":"51031038",
        "opEquipId":"12",
        "opEquipName":"染色#2",
        "opItemNo":"LPC110UXXX25H17850",
        "opSocVer":"1",
        "opPubDate":"2023/03/23",
        "opPublicName":"陳啟銘",
        "opCdt":"2023/03/30 09:08:37",
        "opCreateByName":"陳韋學",
        "opMfgDate":"1900/01/01"
      }
    ]
  }
}
```











