# 產能對照-新增修改存檔
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

產能對照-新增修改存檔

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230829 | 新增規格 | Ellen  |



## 來源URL及資料格式

| 項目   | 說明       |
| ------ | ---------- |
| URL    | /iepc/save |
| method | post       |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。

| 欄位           | 名稱                     | 資料型別 | 必填 | 資料儲存 & 說明        |
| -------------- | ------------------------ | -------- | ---- | ---------------------- |
| opIepcUid      | 產能資料UID              | string   | O    | 有值時為編輯           |
| opActiveFlag   | 是否生效                 | string   | M    | Y/N  Y: 生效 / N: 失效 |
| opStatus       | 狀態                     | string   | M    | Y/N  Y: 非概估 / N: 概估 |
| opItemNo       | 料號                     | string   | M    |                        |
| opMsDate       | 測量日期                 | string   | M    |                        |
| opEquipId      | 機台                     | string   | M    |                        |
| opBdp          | BDP                      | string   | O    |                        |
| opCurr         | 電流值 / BDP含量         | string   | O    |                        |
| opHourStdQty   | 單位時間產量             | string   | O    |                        |
| opMfgQty       | 每分鐘產出(KG)           | string   | M    |                        |
| opNetTime      | 換網時間(Min/Shift)      | string   | O    |                        |
| opNp           | 標準人力(人/機)          | string   | O    |                        |
| opPeopleStdQty | 每人單位時間產量         | string   | O    |                        |
| opShiftStdQty  | 標準產能(KG/Shift)       | string   | M    |                        |
| opRpm          | 轉速(RPM) / 餵料機下料量 | string   | M    |                        |
| opRemark       | 備註                     | string   | O    |                        |



#### Request 範例

```json
{
  "opIepcUid": "5000-119E17D2-E84D-4A8C-9619-76BAA9E8199A",
  "opActiveFlag": "Y",
  "opStatus": "N",
  "opItemNo": "06-199-C92B01DXXXX",
  "opMsDate": "2014/02/26",
  "opEquipId": "5",
  "opBdp": "",
  "opCurr": "",
  "opHourStdQty": "9.60",
  "opMfgQty": "0.00",
  "opNetTime": "13.00",
  "opNp": "0.5",
  "opPeopleStdQty": "4.80",
  "opShiftStdQty": "3000.00",
  "opRpm": "52",
  "opRemark": ""
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* 一個料號搭配一個機台只能有一筆資料，執行SQL-1檢查資料是否有重複，若重複 [return 400,DUPLICATED,欄位相關訊息]
* 若{opIepcUid}為空，執行SQL-2 新增產能對照
* 若{opIepcUid}不為空，執行SQL-3 更新產能對照
* 上述過程如有執行任何資料庫任何異動，執行 SQL-LOG 紀錄 LOG，{DATAKEY} = {opIepcUid}
  有系統錯誤 return 500,SYSTEM_ERROR,正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1: 檢查資料是否有重複
```sql
SELECT
  COUNT(*) CNT
FROM
  IE_PC
WHERE
  ITEM_NO = {opItemNo}
  AND EQUIP_ID = {opEquipId}
  AND ACTIVE_FLAG = 'Y'
  AND IEPC_UID != {opIepcUid}
```

SQL-2: 新增產能對照
```sql
INSERT INTO IE_PC (
  IEPC_UID,
  ACTIVE_FLAG,
  EQUIP_ID,
  MS_DATE,
  ITEM_NO,
  RPM,
  MFG_QTY,
  NET_TIME,
  SHIFT_STD_QTY,
  REMARK,
  STATUS,
  BDP,
  CURR,
  NP,
  HOUR_STD_QTY,
  PEOPLE_STD_QTY,
  CDT,
  CREATE_BY,
  UDT,
  UPDATE_BY 
  )
VALUES
(
  {產生隨機UUID},
  {opActiveFlag},
  {opEquipId},
  CONVERT(DATETIME, {opMsDate}),
  {opItemNo},
  {opRpm},
  {opMfgQty},
  {opNetTime},
  {opShiftStdQty},
  {opRemark},
  {opStatus},
  {opBdp},
  {opCurr},
  {opNp},
  {opHourStdQty},
  {opPeopleStdQty},
  GETDATE(),
  {登入者使用者ID},
  GETDATE(),
  {登入者使用者ID}
)
```

SQL-3: 更新產能對照
```sql
UPDATE IE_PC 
SET 
  EQUIP_ID = {opEquipId},
  ACTIVE_FLAG = {opActiveFlag},
  MS_DATE = CONVERT (DATETIME, {opMsDate}),
  ITEM_NO = {opItemNo},
  RPM = {opRpm},
  MFG_QTY = {opMfgQty},
  NET_TIME = {opNetTime},
  SHIFT_STD_QTY = {opShiftStdQty},
  REMARK = {opRemark},
  STATUS = {opStatus},
  BDP = {opBdp},
  CURR = {opCurr},
  NP = {opNp},
  HOUR_STD_QTY = {opHourStdQty},
  PEOPLE_STD_QTY = {opPeopleStdQty},
  UDT = GETDATE(),
  UPDATE_BY = {登入者使用者ID} 
WHERE
  IEPC_UID = {opIepcUid}
```

SQL-LOG:
```sql
INSERT INTO  SYS_MODI_LOG (CATEGORY,DATAKEY,USERID,USER_NAME,LOG) VALUES ('IEPC',{DATAKEY},{登入者使用者ID},{登入者使用者名稱}, NULL)
```

# Response 欄位
 | 欄位      | 名稱        | 資料型別 | 資料儲存 & 說明 |
 | --------- | ----------- | -------- | --------------- |
 | opIepcUid | 產能資料UID | string   |                 |


## Response 範例

```json
{
    "msgCode": null,
    "result": {
       "content":{
          "opIepcUid":"5000-119E17D2-E84D-4A8C-9619-76BAA9E8199A"
      }
    }
}
```
