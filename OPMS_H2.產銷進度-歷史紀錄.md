# 產銷進度-歷史紀錄
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230807 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明              |
|--------|------------------|
| URL    | /so/record_list  |
| method | get              |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位

M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位            | 名稱    | 資料型別  | 必填 | 資料儲存 & 說明                                                     |
|----------------|---------|:--------:|:----:|--------------------------------------------------------------------|
| opWoUid        | 第幾頁   | integer  |  O   |                                                                    |
| opPage         | 第幾頁   | integer  |  O   | 預設 1，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推|
| opPageSize     | 分頁筆數 | integer  |  O   | 預設 10                                                            |
| opOrder        | 排序     | string   |  O   | 預設 'desc'  順序：'asc', 'ASC' 倒序：'desc','DESC'


#### Request 範例

* /so/record?opWoUid={ opWoUid }

# Request 後端流程說明

* 參考 SQL-1 從 SO_D_REC 和 SYS_USER 撈取資料
	* 依傳入的工單ID查詢產銷追蹤紀錄 SO_D_REC
	* 從 SYS_USER 取得更新人員名稱：SYS_USER.USER_ID = SO_D_REC.CREATE_BY
* 預設依 opCdt 更新時間進行分頁倒序排序，最新的排在前面，每頁10筆，回傳時需包含分頁資訊。
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

SQL-1:

```sql
SELECT SO_D_REC.CDT, SYS_USER.USER_NAME FROM SO_D_REC
JOIN SYS_USER ON SYS_USER.USER_ID = SO_D_REC.CREATE_BY
WHERE SO_D_REC.WO_UID = { opWoUid }
```

# Response 欄位
| 欄位    | 名稱     | 資料型別 | 說明     |
|---------|--------|----------|----------|
| msgCode | 訊息代碼 | string   |          |
| result  | 回傳資料 | object   | 進階訊息 |

#### 【result】child node
| 欄位     | 名稱     | 資料型別 | 來源資料 & 說明 |
|----------|--------|----------|-----------------|
| pageInfo | 分頁資訊 | object   |                 |
| content  | 附帶訊息 | array    |                 |

#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明 |
|------------|------------|----------|-----------------|
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |

#### 【content】array
| 欄位       | 名稱     | 資料型別 | 資料儲存 & 說明    |
|------------|---------|----------|-------------------|
| opCdt      | 更新時間 | string   | yyyy/MM/dd hh:mm |
| opUserName | 更新人員 | string   |                  |

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
  	"pageInfo": {
  		"pageNumber": 1,
  		"pageSize": 10,
  		"pages": 1,
  		"total": 3
  	},
    "content": [
    	{
    		"opCdt": "2023/08/07 12:00",
    		"opUserName": "東東"
    	},
    	{
    		"opCdt": "2023/08/08 08:08",
    		"opUserName": "西西"
    	},
    	{
    		"opCdt": "2023/08/09 01:20",
    		"opUserName": "北北"
    	}
    ]
  }
}
```







