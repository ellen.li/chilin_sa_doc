# 產量回報-取得生管排程列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

產量回報-取得生管排程列表

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230829 | 新增規格 | Ellen  |


## 來源URL及資料格式

| 項目   | 說明               |
| ------ | ------------------ |
| URL    | /psr/list_schedule |
| method | post               |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         | 名稱     | 資料型別 | 必填  | 資料儲存 & 說明 |
| ------------ | -------- | :------: | :---: | --------------- |
| opQueryDateS | 開始日期 |  string  |   M   | yyyy/MM/dd      |
| opQueryDateE | 結束日期 |  string  |   M   | yyyy/MM/dd      |
| opEquipId    | 機台     |  string  |   M   |                 |

#### Request 範例

```json
{
  "opQueryDateS":"2023/01/01",  
  "opQueryDateE":"2023/02/01",    
  "opEquipId":"1"
}
```

# Request 後端流程說明

可參考舊版 PsManageAction.ajaxQueryMfgSchedule
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* {organizationId} 為登入者工廠別ID
* 使用前端給的條件，參考 SQL-1 自組【搜尋條件】，如為 null 代表不需組條件
* 參考 SQL-1 查回結果 result
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1: 
```sql
SELECT
	PS_UID,
	PS_SEQ,
	EQUIP_ID,
	EQUIP_NAME,
	MFG_DATE,
	SHIFT_CODE,
  CASE SHIFT_CODE WHEN 'A' THEN '早'
	WHEN 'B' THEN '中'
	WHEN 'C' THEN '晚'
	ELSE NULL END SHIFT_CODE_NAME,
	WO_UID,
	WO_NO,
	PATTERN_NO,
	ITEM_NO,
	STOP_DESC,
	MTR_NAME,
	CTN_WEIGHT,
	DD_MFG,
	SHIFT_HOURS,
	WO_QTY,
	SHIFT_STD_QTY,
	SHIFT_PLN_QTY,
	SHIFT_MFG_QTY,
	REMARK 
FROM
	PS_SCHEDULE_V
WHERE
	MFG_DATE >= {opQueryDateS}
	AND MFG_DATE <= {opQueryDateE}
	AND ORGANIZATION_ID = {organizationId}
  AND EQUIP_ID = {opEquipId}
ORDER BY MFG_DATE, SHIFT_CODE, PS_SEQ, WO_NO
```

# Response 欄位
| 欄位            | 名稱            | 資料型別 | 來源資料 & 說明        |
| --------------- | --------------- | -------- | ---------------------- |
| opPsUid         | 生管排程UID     | string   | PS_UID(SQL-1)          |
| opPsSeq         | 序號            | string   | PS_SEQ(SQL-1)          |
| opEquipId       | 機台            | string   | EQUIP_ID(SQL-1)        |
| opEquipName     | 機台名稱        | string   | EQUIP_NAME(SQL-1)      |
| opMfgDate       | 生產日期        | string   | MFG_DATE(SQL-1)        |
| opShiftCode     | 班別代碼        | string   | SHIFT_CODE(SQL-1)      |
| opShiftCodeName | 班別名稱        | string   | SHIFT_CODE_NAME(SQL-1) |
| opWoUid         | uid             | string   | WO_UID(SQL-1)          |
| opWoNo          | 工單號碼        | string   | WO_NO(SQL-1)           |
| opPatternNo     | 色號            | string   | PATTERN_NO(SQL-1)      |
| opItemNo        | 料號 (排程資訊) | string   | ITEM_NO(SQL-1)         |
| opStopDesc      | 停車原因        | string   | STOP_DESC(SQL-1)       |
| opMtrName       | 原料            | string   | MTR_NAME(SQL-1)        |
| opCtnWeight     | 桶重            | string   | CTN_WEIGHT(SQL-1)      |
| opDdMfg         | 現場交期        | string   | DD_MFG(SQL-1)          |
| opShiftHours    | 時數            | string   | SHIFT_HOURS(SQL-1)     |
| opWoQty         | 訂單未做        | string   | WO_QTY(SQL-1)          |
| opShiftStdQty   | 標準產量        | string   | SHIFT_STD_QTY(SQL-1)   |
| opShiftPlnQty   | 預計產量        | string   | SHIFT_PLN_QTY(SQL-1)   |
| opShiftMfgQty   | 實際產量        | string   | SHIFT_MFG_QTY(SQL-1)   |
| opRemark        | 排程說明        | string   | REMARK(SQL-1)          |


#### Response 範例

```json
{
    "msgCode": null,
    "result": {
      "content":[
        {
          "opPsUid": "B77CF423-8BAB-495D-AE4E-597E93C4E775",
          "opPsSeq": "0",
          "opEquipId": "45",
          "opEquipName": "",
          "opMfgDate": "2022/12/05",
          "opShiftCode": "A",
          "opShiftCodeName": "早",
          "opWoUid": "",
          "opWoNo": "",
          "opPatternNo": "",
          "opItemNo": "",
          "opStopDesc": "",
          "opMtrName": "",
          "opCtnWeight": "",
          "opDdMfg": "",
          "opShiftHours": "2",
          "opWoQty": "0",
          "opShiftStdQty": "0",
          "opShiftPlnQty": "0",
          "opShiftMfgQty": "",
          "opRemark": ""
        }
      ]
    }
}
```
 