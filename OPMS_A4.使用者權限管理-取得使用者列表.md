# 使用者權限管理-取得使用者清單
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得使用者清單，可過濾關鍵字跟指定欄位條件(使用者ID、關鍵字、驗證方式、狀態)

## 修改歷程

| 修改時間 | 內容                               | 修改者 |
|----------|-----------------------------------|--------|
| 20230330 | 新增規格                           | Nick   |
| 20230517 | SYS_USER_ROLE 改 SYS_USER_ROLE_V3 | Nick   |
| 20230519 | add 使用者名稱搜尋(opUserName)     | Nick   |
| 20230519 | 應該要濾掉已經刪除的角色           | Nick   |
| 20230522 | 無設定角色也需顯示                 | Nick   |
| 20230815 | 調整 TOP 1000(使用OFFSET FETCH)    | Nick   |

## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    |  /user/list
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位     | 名稱 | 資料型別 | 必填 | 資料儲存 & 說明 |
| -------  | ---- | :------: | :--: | --------------- |
| opUserId   | 搜尋使用者ID   | string    | O   | 預設: `null`              |
| opUserName   | 搜尋使用者名稱   | string    | O   | 預設: `null`              |
| opAuthType | 搜尋認證方式 | string   | O   |預設: `null`，所有認證方式: NA:自行驗證 / CMC:Chimei / CLT:Chilintech / LSO:LinShine / ZJ:鎮江奇美        |
| opActive   | 搜尋狀態     | string   |  O  | 預設: `null`，所有狀態 0:停用 / 1:啟用 |
| opKeyword   |搜尋進階關鍵字   | string   |  O  | 預設: `null`  |
| opPage      |第幾頁| integer | O| 預設`1`，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推|
| opPageSize  |分頁筆數| integer | O | 預設 `10` 
#### Request 範例

```json
{
  "opUserId":"nickjian",
  "opUserName":"路人甲",
  "opAuthType":"CLT",
  "opActive":"1",
  "opKeyword":"nick",
  "opPage":1,
  "opPageSize":50,
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 參考 SQL-1 依前端給的條件，動態調整【指定條件】及【關鍵字模糊搜尋條件】內容。前端給 null 代表該項目不需組條件，opUserId 及 opKeyword使用模糊搜尋(like 前後使用%)，關鍵字搜尋 USER_ID、USER_NAME、ROLE_NAME 欄位，其他項目必需與前端傳來值相等，且不同項目需同時成立，
* {offset} 為起始列印 row ，第一頁為 0，第二頁後為 0 + (opPageSize*(opPage-1))
* 執行 SQL-1 查回結果 result
* Response 結果需含分頁資訊，無資料 pageInfo 裡面項目皆給 0，頁數與分頁筆數前端如有提供，依前端判斷給的資料計算相關數值及拆分頁面
* 依照 result (USER_GID + LDAP_SITE + U.USERID + USER_NAME + ACTIVE) 組出不重複的資料，將同組 ROLE_NAME 資料組成陣列，LDAP_SITE 依照對應表返回VALUE內容，如LDAP_SITE值無法對應項目，直接顯示 LDAP_SITE 值，如 opRoleNameList 無資料給 null，ex: "opRoleNameList":null
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

  <font size=2>

  * LDAP_SITE 對應表
    | KEY | VALUE | 
    | - | - | 
    | NA | 自行驗證 | 
    | CMC | AD驗證-Chimei | 
    | CLT | AD驗證-Chilintech | 
    | LSO | AD驗證-LinShine | 
    | ZJ | AD驗證-鎮江奇美 | 
  </font>
# Request 後端邏輯說明

SQL-1:
```sql
SELECT *
FROM (
        SELECT *
        FROM(
                SELECT u.USER_ID,
                    u.USER_NAME,
                    CASE
                        WHEN ISNULL(LDAP_SITE, '') = '' THEN 'NA'
                        ELSE LDAP_SITE
                    END AS LDAP_SITE,
                    CASE
                        WHEN ISNULL(ACTIVE, '') = '' THEN '0'
                        ELSE ACTIVE
                    END AS ACTIVE,
                    STRING_AGG(r.ROLE_NAME, ', ') as ROLES
                FROM SYS_USER u
                    LEFT JOIN SYS_USER_ROLE_V3 ur ON u.USER_ID = ur.USER_ID
                    LEFT JOIN SYS_ROLE_V3 r ON ur.ROLE_ID = r.ROLE_ID
                    AND r.VISIBLE = '1'
                GROUP BY u.USER_ID,
                    u.USER_NAME,
                    LDAP_SITE,
                    ACTIVE
            ) ALL_DATA
        WHERE 1 = 1 --指定條件
            AND USER_ID like '%{opUserId}%'
            AND LDAP_SITE = '%{opAuthType}%'
            AND ACTIVE = '%{opActive}%'
            AND USER_NAME like '%{opUserName}%' --關鍵字模糊搜尋
            AND (
                USER_ID like '%{opKeyword}%'
                OR USER_NAME like '%{opKeyword}%'
                OR ROLES like '%{opKeyword}%'
            )
        ORDER BY USER_NAME,
            USER_ID OFFSET 0 ROWS FETCH NEXT 1000 ROWS ONLY
    ) AS TOP1000T
ORDER BY (SELECT NULL) OFFSET { offset } ROWS FETCH NEXT { opageSize } ROWS ONLY
```
# Response 欄位
| 欄位    | 名稱     | 資料型別 | 說明     |
|---------|--------|----------|----------|
| msgCode | 訊息代碼 | string   |          |
| result  | 回傳資料 | object   | 進階訊息 |


#### 【result】child node
| 欄位     | 名稱     | 資料型別 | 來源資料 & 說明 |
|----------|--------|----------|-----------------|
| pageInfo | 分頁資訊 | object   |                 |
| content  | 附帶訊息 | array    |                 |
#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明 |
|------------|------------|----------|-----------------|
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |

#### 【content】child node
| 欄位           | 名稱       | 資料型別    | 來源資料 & 說明 |
|----------------|----------|-------------|-----------------|
| opLdapSite     | 驗證方式   | string      | LDAP_SITE       |
| opUserId       | 使用者ID   | string      | U.USER_ID       |
| opUserName     | 使用者名稱 | string      | USER_NAME       |
| opRoleNameList | 角色       | arraystring | ROLE_NAME       |
| opActive       | 狀態       | string      | ACTIVE          |


#### Response 範例

```json
{
  "msgCode":"",
  "result":{
    "pageInfo": {
      "pageNumber": 1, 
      "pageSize": 20,
      "pages": 5,      
      "total": 99     
    },
    "content":[
      {
        "opLdapSite":"自行驗證",
        "opUserId":"richman",
        "opUserName":"richman",
        "opRoleNameList": ["配色-CLT","製造"],
        "opActive":"1"
      }
      ]
  }
}
```

