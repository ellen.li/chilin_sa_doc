# 工單維護-排程-更新工單狀態
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 排程功能說明:

每天7:30, 12:30, 18:30 執行更新opms工單狀態及入庫數量

## 排程修改歷程:

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230721 | 新增規格 | Ellen  |

## 排程時間
 
| 幾秒<br/>(0-59) | 幾分<br/>(0-59) | 幾時<br/>(0-23) | 哪一天<br/>(1 - 31) | 幾月<br/>(1 - 12) | 每週哪天 <br/>(0-7，0或7為週日) |
| --------------- | --------------- | --------------- | ------------------- | ----------------- | ------------------------------- |
| 0               | 30              | 7,12,18         | *                   | *                 | ?                               |

## 排程邏輯
可參考舊版 UpdateWoInvQty
* 執行 SQL-1，若{OPT_VALUE}等於Y才需執行排程，否則結束程式
* 參考 SAP-1 查詢一個月內sap工單，需注意超過1000筆時是否完整取得資料
* 執行 SQL-2 將工單狀態及入庫數量更新至opms工單資料表


SAP-1：
可參考舊版 WoQueryRfc_getUnclosedWoList
程序如有任何異常，紀錄錯誤LOG

**獲得BAPI方法**
|              | 型態   | 參數值                     | 說明 |
| ------------ | ------ | -------------------------- | ---- |
| BAPIFunction | string | Z_PP_OPMS_PRODUCTION_ORDER |      |

**設定輸入參數**
| 參數名稱 | 參數值 | 型態         | 說明 |
| -------- | ------ | ------------ | ---- |
| I_WERKS  | "5000" | string       |      |
| I_ERDAT  |        | JCoStructure |      |

I_ERDAT JCoStructure 參數
| 參數名稱 | 參數值               | 型態   | 說明 |
| -------- | -------------------- | ------ | ---- |
| ERDAT_FR | {當天日期 - 1 month} | string | yyyyMMdd     |
| ERDAT_TO | {當天日期}           | string | yyyyMMdd     |

| 取得回傳表格名稱 |
| ---------------- |
| T_ORDER          |

| SAP欄位名稱 | SAP資料型別 | 欄位值   | 說明               |
| ----------- | :---------: | -------- | ------------------ |
| AUFNR       |   string    | 工單號碼 | WoNo，過濾前綴0    |
| STATUS      |   string    | 狀態     | activeFlag         |
| LMNGA       |   double    | 入庫數量 | 取至小數點後第三位 |


SQL-1: 檢查排程開關
```sql
SELECT OPT_VALUE FROM OPMS_PARAMS 
WHERE TYPE = 'CRON' AND PARAM_ID = 'UPDATE_WO_INT_QTY'
```

SQL-2:

```sql
UPDATE WO_H SET INV_QTY = {SAP-1.LMNGA},ACTIVE_FLAG = {SAP-1.STATUS} WHERE WO_NO = {SAP-1.AUFNR}
```