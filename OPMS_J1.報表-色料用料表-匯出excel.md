# 報表-色料用料表-匯出excel
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230817 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                            |
|--------|--------------------------------|
| URL    | /report/pgm_monthly_list_excel |
| method | post                           |


## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱    |  資料型別 | 必填 | 資料儲存 & 說明 |
|--------------|---------|----------|:----:|-----------------|
| 原列表REQUEST參數 |     |          |      |                 |


# Request 後端流程說明

* 參考[OPMS_J1.報表-色料用料表-列表 /report/pgm_monthly_list](./OPMS_J1.報表-色料用料表-列表.md)，用相同方式取得資料。
* 將資料轉為excel表格匯出，不需設定檔名。
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

參考[OPMS_J1.報表-色料用料表-列表 /report/pgm_monthly_list](./OPMS_J1.報表-色料用料表-列表.md)


# Excel 欄位

| 欄位     | 資料型別 | 資料儲存 & 說明         |
|---------|---------|------------------------|
| 色料料號 | string  | WO_PGM.WO_PGM_PART_NO  |
| 合計     | string  | SUM_TOTAL              |
| 一月     | string  | SUM1                   |
| 二月     | string  | SUM2                   |
| 三月     | string  | SUM3                   |
| 四月     | string  | SUM4                   |
| 五月     | string  | SUM5                   |
| 六月     | string  | SUM6                   |
| 七月     | string  | SUM7                   |
| 八月     | string  | SUM8                   |
| 九月     | string  | SUM9                   |
| 十月     | string  | SUM10                  |
| 十一月   | string  | SUM11                  |
| 十二月   | string  | SUM12                  |
| 月平均   | string  | AVERAGE                |