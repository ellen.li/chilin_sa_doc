# 產銷進度-更新生管交期
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容                          | 修改者 |
| -------- | ----------------------------- | ------ |
| 20230801 | 新增規格                      | 黃東俞 |
| 20230824 | 增加錯誤訊息                  | Ellen  |
| 20230825 | 生管交期(修改前)改非必填      | 黃東俞 |
| 20230825 | 新增SQL-5，修改SQL-LOG        | 黃東俞 |
| 20230905 | 增加sap回傳                   | Ellen  |
| 20230912 | opDdMfgBefore未填時備註非必填 | Ellen  |
| 20231207 | 訂單號碼和訂單項次改非必填    | Ellen |


## 來源URL及資料格式

| 項目   | 說明              |
|--------|------------------|
| URL    | /so/update_ddmfg |
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位            | 名稱             | 資料型別 | 必填 | 資料儲存 & 說明           |
| --------------- | ---------------- | :------: | :--: | ------------------------- |
| opSalesOrderNo  | 訂單號碼         |  string  |  O   | 預設:空字串               |
| opSalesOrderSeq | 項次             |  string  |  O   | 預設:0                    |
| opWoUid         | 工單ID           |  string  |  M   |                           |
| opDdMfgBefore   | 生管交期(修改前) |  string  |  O   | yyyy/mm/dd                |
| opDdMfgAfter    | 生管交期(修改後) |  string  |  M   | yyyy/mm/dd                |
| opRemark        | 備註             |  string  |  O   | opDdMfgBefore有值時為必填 |


#### Request 範例

```json
{
  "opSalesOrderNo": "0009012398",
  "opSalesOrderSeq": "10",
  "opWoUid": "0000301E-EC42-4DC2-A9F4-B5B199361EF2",
  "opDdMfgBefore": null,
  "opDdMfgAfter": "2023/08/01",
  "opRemark": "科科"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 回寫現場交期至SAP，參考SAP-1。
* 檢查SAP-1回傳表格 O_RETURN：
  * 如果 O_RETURN.TYPE 為 'S','W','I'其中一種時，代表成功，繼續進行處理後續步驟。
  * 如果 O_RETURN.TYPE 不為 'S','W','I' 其中一種，代表呼叫SAP失敗，錯誤訊息在 O_RETURN.MESSAGE，[return 400,SAP_DDMFG_ERROR,欄位相關訊息]。
* 發送email通知：
  * 檢查SAP-1回傳表格ZOPMST009的 C_PERNR, L_PERNR, L_PERNR_SALES, CRE_PERNR 四個欄位，如果欄位內容轉為數字非0時，參考ODSSQL-1，以這些個欄位為 EMP_ID 查詢 email。
  * 判斷是否寄信
    * 參考SQL-1，帶入opSalesOrderNo, opSalesOrderSeq 取得訂單明細檔 SO_D 資料。
    * 參考SQL-2，帶入opSalesOrderNo 取得訂單主檔 SO_H 資料。
    * 若 SO_D 和 SO_H 和 SO_H.UPDATE_BY 都有資料時才繼續下面步驟發通知信。
  * 取得信件內容：
    * 參考SQL-3，查詢WO_H。
    * 取Eamil模板檔 SO-DDMFG-NOTICE.ftl 參考下方email模板檔輸入參數，將資料帶入並發信。
  * 發信：
      * 發信者：取登入者的 SYS_USER.EMAIL
      * 收件者：ODSSQL-1 的 OM_EMP.EMAIL
      * 信件標題：[OPMS]產銷進度追踪-生管交期變更通知:{opSalesOrderNo}-{opSalesOrderSeq}
* 將資料存入資料庫
  * 更新 WO_H.DD_MFG，參考 SQL-4
* 儲存更新紀錄
  * 取 { 登入者ID } SYS_USER.USER_ID
  * 執行 SQL-5
  * 執行 SQL-LOG
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200。

# Request 後端邏輯說明

SAP-1：
可參考舊版 SoManageAction.updateWoByColumn

**獲得BAPI方法**
|              | 型態   | 參數值        | 說明 |
| ------------ | ------ | ------------ | ---- |
| BAPIFunction | string | ZSD_OPMS_005 |      |

**設定輸入參數**
| 參數名稱    | 參數值               | 型態       | 說明                                  |
| ---------- | -------------------- | -------- | -------------------------------------- |
| I_VBELN    | { opSalesOrderNo }   | string   |                                        |
| I_POSNR    | { opSalesOrderSeq }  | string   |                                        |
| I_BSTDK_E  | { opDdMfg }          | string   |  轉成 yyyymmdd 格式字串                 |

| 取得回傳表格名稱 |
| ---------------|
| O_RETURN       |

| SAP欄位名稱 | SAP資料型別 | 欄位值   | 說明                               |
| ----------- | :---------: | -------- | -------------------------------- |
| TYPE       |   string    | 回傳代碼 |                                    |
| MESSAGE    |   string    | 回傳訊息 |                                    |


| 取得回傳表格名稱 |
| ---------------|
| ZOPMST009       |

| SAP欄位名稱       | SAP資料型別 | 欄位值         | 說明                               |
| ---------------- | :---------: | ------------- | -------------------------------- |
| ETDAT            |   date      |               |                                    |
| C_PERNR          |   string    | 奇美業務       |                                    |
| L_PERNR          |   string    | 奇菱業助       |                                    |
| L_PERNR_SALES    |   string    | 奇菱業務       |                                    |
| CRE_PERNR        |   string    | 奇美訂單建立者 |                                    |

ODSSQL-1:
```sql
select EMAIL from OM_EMP where EMP_ID IN {C_PERNR, L_PERNR, L_PERNR_SALES, CRE_PERNR };
```
SQL-1:

```sql
-- SoQueryDao.getSoD
SELECT ITEM_NO
FROM SO_D WHERE SO_NO = { opSalesOrderNo } AND SO_SEQ= { opSalesOrderSeq }
```

SQL-2:

```sql
-- SoQueryDao.getSoH
SELECT CUST_NAME, UPDATE_BY
FROM SO_H WHERE SO_NO = { opSalesOrderNo }
```

SQL-3:

```sql
-- WoQueryDao.getWoHSingleByUid
SELECT  H.WO_NO, H.CUST_PO_NO
FROM WO_H H
WHERE H.WO_UID = { opWoUid }
```
SQL-4:

```sql
-- SoModifyDao.updateWoByColumn
UPDATE WO_H SET DD_MFG = { opDdMfg }, DD_SALES_CONFIRM = NULL WHERE WO_UID = { opWoUid };
```

SQL-5
```sql
-- SoModifyDao.updateWoByColumn
INSERT INTO SO_D_REC (SO_NO, SO_SEQ, WO_UID, SO_D_REC_UID, DATE_BEFORE, DATE_AFTER, COLUMN_NAME, REMARK, CDT, CREATE_BY)
VALUES(
  { opSalesOrderNo },
  { opSalesOrderSeq },
  { opWoUid },
  CONVERT(VARCHAR, GETDATE(), 112) + REPLACE(CONVERT(VARCHAR, GETDATE(), 108),':',''),
  { opDdMfgBefore },
  { opDdMfgAfter },
  'DD_MFG'
  { opRemark },
  GETDATE(),
  { 登入者ID }
)
```

SQL-LOG:
```sql
INSERT INTO  SYS_MODI_LOG (CATEGORY, DATAKEY, USERID, USER_NAME, LOG) VALUES ('DDMFG', { opWoUid }, {登入者使用者ID}, {登入者使用者名稱}, '更新生管交期');
```


**Email模板檔SO-DDMFG-NOTICE.ftl輸入參數**
|                | 型態   | 參數值           | 說明 |
| -------------- | ------ | --------------- | ---- |
| salesOrderNo   | string | opSalesOrderNo  |      |
| salesOrderSeq  | string | opSalesOrderSeq |      |
| itemNo         | string | SO_D.ITEM_NO    |      |
| woNo           | string | WO_H.WO_NO      |      |
| custName       | string | SO_H.CUST_NAME  |      |
| dateBefore     | string | opDdMfgBefore   |      |
| dateAfter      | string | opDdMfgAfter    |      |
| remark         | string | opRemark        |      |
| ddFirst        | string | SAP-1: ETDAT    |      |
| custPoNo       | string | WO_H.CUST_PO_NO |      |


# Response 欄位
| 欄位        | 名稱        | 資料型別 | 資料儲存 & 說明 |
| ----------- | ----------- | -------- | --------------- |
| opRfcReturn | sap回傳資訊 | object   |                 |

【 opRfcReturn object】
| 欄位      | 名稱     | 資料型別 | 資料儲存 & 說明 |
| --------- | -------- | -------- | --------------- |
| opType    | 回傳類型 | string   |                 |
| opMessage | 回傳訊息 | string   |                 |

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content": {
      "opRfcReturn": null
    }
  }
}
```