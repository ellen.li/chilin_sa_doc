# OPMS_D.工單列印-包裝指示書
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

列印包裝指示書


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230803 | 新增規格 | Ellen  |

## 來源URL及資料格式

| 項目   | 說明           |
| ------ | -------------- |
| URL    | /wo/print_wo_g |
| method | post           |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位

M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。

| 欄位        | 名稱   |   資料型別   | 必填  | 資料儲存 & 說明 |
| ----------- | ------ | :----------: | :---: | --------------- |
| opWoUidList | 工單ID | array string |   M   |                 |

#### Request 範例
```json
{
  "opWoUidList": ["00006D90-B5F1-4A80-9F62-B9989EBAE168", "00008BEB-34CC-482D-B52E-AC3E0498AD4E"]
}
```

# Request 後端流程說明

請參考舊版 `WebContent\wo\WoPrintG.jsp`
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* 執行 SQL-1，取得需列印的工單，確認傳入工單ID是否已存在，如果不存在 return 400 NOTFOUND
* 遍歷所有需列印的工單，{woUid}為單筆工單ID、{woNo}為單筆工單號碼、{organizationId}為單筆工單工廠別ID
  * 執行 SQL-2 取得工單色料清單
  * 執行 SAP-1 取得工單詳細資料，
    * 若表格T_ORDER有資料，覆寫SQL-1結果各欄位值，若無則跳過該筆筆工單
    * 表格T_COMP為ERP用料需求
  * 只處理工單類別 wipClassNo 為 "Z51" 或 "Z52"開頭ˇ的工單，否則跳過
  * 遍歷SAP-1表格T_COMP結果
    * {compItem}開頭符合 "2201", "B201", "2251", "B251" 為委外廠，不顯示色料跳過 
    * 比對第2碼之後的料號，不存在SQL-2色料清單的暫存為陣列{openListM}
  * 執行 SQL-3 取得出貨指示備註
    * SQL-3結果合併{PROD_REMARK(SQL-1)}以換行符號切分後的陣列，暫存為陣列{aryProdRemark}
* 執行JASPER
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容


# Request 後端邏輯說明

SAP-1：
可參考舊版 WoQueryRfc_getWoHSingle
程序如有任何異常，紀錄錯誤LOG並回傳NULL

**獲得BAPI方法**
|              | 型態   | 參數值                     | 說明 |
| ------------ | ------ | -------------------------- | ---- |
| BAPIFunction | string | Z_PP_OPMS_PRODUCTION_ORDER |      |

**設定輸入參數**
| 參數名稱 | 參數值           | 型態         | 說明 |
| -------- | ---------------- | ------------ | ---- |
| I_WERKS  | {organizationId} | string       |      |
| I_AUFNR  | {woNo}           | string       |      |
| I_ERDAT  |                  | JCoStructure |      |

I_ERDAT JCoStructure 參數
| 參數名稱 | 參數值     | 型態   | 說明 |
| -------- | ---------- | ------ | ---- |
| ERDAT_FR | "00000000" | string |      |
| ERDAT_TO | "99991231" | string |      |


| 取得回傳表格名稱 |
| ---------------- |
| T_ORDER          |


| 物件元素名稱      | 資料型別 | SAP欄位名稱 | SAP資料型別 | 物件元素回傳值 | 說明                                                         |
| ----------------- | -------- | ----------- | ----------- | -------------- | ------------------------------------------------------------ |
| WoNo              | string   | AUFNR       | string      | 同SAP          | 工單號碼，過濾前綴0                                          |
| activeFlag        | string   | STATUS      | string      | 同SAP          | 狀態                                                         |
| woDate            | string   | GSTRP       | date        | 同SAP          | 使用日期，format yyyy/MM/dd                                  |
| color             | string   | COLOR       | string      | 同SAP          | 顏色                                                         |
| patternNo         | string   | COLOR_NO    | string      | 同SAP          | 色號                                                         |
| itemNo            | string   | MATNR       | string      | 同SAP          | 料號                                                         |
| itemDesc          | string   | MAKTX       | string      | 同SAP          | 料號摘要                                                     |
| bomNo             | string   | STLAL       | string      | 同SAP          | BOM                                                          |
| gradeNo           | string   | GRADE       | string      | 同SAP          | 原料                                                         |
| produnitNo        | string   | STAND       | string      | 同SAP          | 生產單位代碼，過濾前綴0，若為空顯示"NA"                      |
| produnitName      | string   | STAND_T     | string      | 同SAP          | 生產單位                                                     |
| salesNo           | string   | ERNAM       | string      | 同SAP          | 業助代號                                                     |
| custNo            | string   | KUNNR       | string      | 同SAP          | 客戶代號，過濾前綴0                                          |
| custName          | string   | SORT2       | string      | 同SAP          | 客戶名稱                                                     |
| indirCust         | string   | IHREZ       | string      | 同SAP          | 間接客戶                                                     |
| custPoNo          | string   | CUSTPO      | string      | 同SAP          | 客戶PO單號                                                   |
| salesQty          | string   | SOQTY       | double      | 同SAP          | 銷售數量，取至小數點後第五位                                 |
| wipClassNo        | string   | AUART       | string      | 同SAP          | 工單類別                                                     |
| wipClassCode      | string   | AUART_T     | string      | 同SAP          | 工單類別Code                                                 |
| sapOperation      | string   | VORNR       | string      | 同SAP          | 過濾前綴0                                                    |
| sapWc             | string   | ARBPL       | string      | 同SAP          | 工作中心                                                     |
| equipName         | string   | ARBPL_T     | string      | 同SAP          | 指定機台                                                     |
| woQty             | string   | GAMNG       | double      | 同SAP          | 工單數量，取至小數點後第五位                                 |
| rawQty            | string   | GAMNG       | double      | 同SAP          | 投入原料量，若H.RAW_QTY(SQL-1)為空才取值，取至小數點後第五位 |
| startQty          | string   | GAMNG       | double      | 同SAP          | 工單數量，取至整數位                                         |
| unit              | string   | GMEIN       | string      | 同SAP          | 單位                                                         |
| estimatedShipDate | string   | SHIP_DATE   | date        | 同SAP          | 預計出貨日                                                   |
| completionSubinv  | string   | LGORT       | string      | 同SAP          | 完工倉別                                                     |
| salesOrderNo      | string   | KDAUF       | string      | 同SAP          | 銷售訂單，過濾前綴0                                          |
| salesOrderSeq     | string   | KDPOS       | string      | 同SAP          | 銷售訂單序號，過濾前綴0                                      |
| cmcSoType         | string   | CMC_SO_TYPE | string      | 同SAP          | 1: 奇美內銷(大黃標籤) /  2: 奇美外銷(外銷標籤) / 3: 其他     |


| 取得回傳表格名稱 |
| ---------------- |
| T_COMP           |

| 物件元素名稱 | 資料型別 | SAP欄位名稱 | SAP資料型別 | 物件元素回傳值 | 說明 |
| ------------ | -------- | ----------- | ----------- | -------------- | ---- |
| compItem     | string   | IDNRK       | string      | 同SAP          |      |


SQL-1:
```sql
SELECT
  H.WO_UID,
  H.ACTIVE_FLAG,
  H.BOM_UID,
  H.EQUIP_ID,
  H.ORGANIZATION_ID,
  H.WO_NO,
  H.WO_SEQ,
  H.LOT_NO,
  H.CUST_PO_NO,
  CONVERT (VARCHAR (10), H.WO_DATE, 111) AS WO_DATE,
  H.PRODUNIT_NO,
  H.CUST_NO,
  H.CUST_NAME,
  H.INDIR_CUST,
  B.CUST_NAME AS BASE_CUST,
  H.UNIT,
  H.WEIGHT,
  B.CMDI_NO,
  B.CMDI_NAME,
  B.COLOR,
  H.[ USE ],
  H.SALES_NO,
CASE
  H.RAW_QTY 
  WHEN cast(H.RAW_QTY AS INT) THEN
  cast(CONVERT (INT, H.RAW_QTY) AS VARCHAR (20)) ELSE cast(CONVERT (FLOAT, H.RAW_QTY) AS VARCHAR (20)) 
  END RAW_QTY,
CASE
  H.BATCH_QTY 
  WHEN cast(H.BATCH_QTY AS INT) THEN
  cast(CONVERT (INT, H.BATCH_QTY) AS VARCHAR (20)) ELSE cast(CONVERT (FLOAT, H.BATCH_QTY) AS VARCHAR (20)) 
  END BATCH_QTY,
  H.BATCH_TIMES,
CASE
  H.REMAIN_QTY 
  WHEN cast(H.REMAIN_QTY AS INT) THEN
  cast(CONVERT (INT, H.REMAIN_QTY) AS VARCHAR (20)) ELSE cast(CONVERT (FLOAT, H.REMAIN_QTY) AS VARCHAR (20)) 
  END REMAIN_QTY,
  H.REMAIN_TIMES,
  H.MODIFY_FLAG,
  H.PRINT_FLAG,
  H.REMARK,
  H.BOM_REMARK,
  H.ERP_FLAG,
  H.WIP_CLASS_CODE,
  H.ALT_BOM_PART_NO,
  H.ALT_BOM_DESIGNATOR,
  H.ALT_ROUTING_PART_NO,
  H.ALT_ROUTING_DESIGNATOR,
  H.COMPLETION_SUBINV,
  H.COMPLETION_LOCATOR,
  H.SALES_ORDER_NO,
  H.SALES_QTY,
  H.OEM_ORDER_NO,
  CONVERT (VARCHAR (10), H.ESTIMATED_SHIP_DATE, 111) AS ESTIMATED_SHIP_DATE,
  I.PATTERN_NO,
  B.ITEM_NO,
  B.ITEM_DESC,
  B.BOM_NO,
  B.BOM_NAME,
  B.BOM_DESC,
  B.[ USE ],
  B.BASE_ITEM_NO,
  B.BASE_BOM_NO,
  H.STD_N_QUANTITY_PER,
  H.CUS_N_QUANTITY_PER,
  REPLACE (CONVERT (VARCHAR (16), H.CDT, 120), '-', '/') AS CDT,
  H.CREATE_BY,
  REPLACE (CONVERT (VARCHAR (16), H.UDT, 120), '-', '/') AS UDT,
  H.UPDATE_BY,
  B.MULTIPLE BOM_MULTIPLE,
  H.CTN_WEIGHT,
  H.PROD_REMARK,
  H.SALES_ORDER_SEQ,
  B.BIAXIS_ONLY,
  B.DYE_ONLY,
  H.GRADE_NO,
  H.PROD_REMARK,
  H.PS_FLAG,
  H.WO_TYPE 
FROM
  WO_H H
  INNER JOIN BOM_MTM B ON H.BOM_UID = B.BOM_UID
  INNER JOIN ITEM_H I ON B.ITEM_NO = I.ITEM_NO 
WHERE
  H.WO_UID = {woUid}
```

SQL-2: 取得色料清單
```sql
SELECT
  WO_PGM_PART_NO
FROM
  WO_PGM
WHERE
  WO_UID = {woUid}
```

SQL-3: 取得出貨指示備註
```sql
SELECT 
  CONVERT (INT, S.LIST_HEADER_ID) LIST_HEADER_ID,
  S.OPT_DESC + ISNULL(S.ASS_DESC, '') AS SHOW_TEXT 
FROM
  WO_H W
  INNER JOIN WO_SIS S ON W.WO_UID = S.WO_UID 
  INNER JOIN SYS_SIS_H H ON S.LIST_HEADER_ID = H.LIST_HEADER_ID
WHERE
  W.WO_UID = {woUid}
  AND H.ORGANIZATION_ID = {organizationId}
  AND H.R_OPMS_PDM = 'Y' 
  AND H.ACTIVE_FLAG = 'Y' 
ORDER BY
  S.LIST_SEQ
```

JASPER:
**jasper檔案位置**
|          | 型態   | 參數值                   | 說明 |
| -------- | ------ | ------------------------ | ---- |
| RealPath | string | /report/WoPrintD2.jasper |      |

**CollectionDataSource參數**
| 參數名稱 | 名稱 | 型態         | 來源資料 & 說明 |
| -------- | ---- | ------------ | --------------- |
| pars     |      | array object |                 |
| jrds2    |      | JRDataSource |                 |


tableList object 參數
| 參數名稱       | 參數值         | 型態         | 說明                                                            |
| -------------- | -------------- | ------------ | --------------------------------------------------------------- |
| today          | 當下日期       | string       | yyyy/MM/dd hh:mm:ss                                             |
| woDate         | 使用日期       | string       | H.WO_DATE(SQL-1)                                                |
| woSeq          | 序號           | string       | H.WO_SEQ(SQL-1)                                                 |
| patternNo      | 色號           | string       | I.PATTERN_NO(SQL-1)                                             |
| color          | 色相           | string       | B.COLOR(SQL-1)                                                  |
| unit           | 單位           | string       | H.UNIT(SQL-1)                                                   |
| woNo           | 工單號碼       | string       | H.WO_NO(SQL-1)                                                  |
| equipName      | 指定機台       | string       | equipName(SAP-1)                                                |
| lotNo          | Lot No         | string       | H.LOT_NO(SQL-1)                                                 |
| custName       | 客戶名稱       | string       | H.CUST_NAME(SQL-1)                                              |
| use            | 用途           | string       | B.USE(SQL-1)                                                    |
| itemNo         | 料號           | string       | B.ITEM_NO(SQL-1)                                                |
| produnitName   | 生產單位名稱   | string       | produnitName(SAP-1)                                             |
| salesNo        | 業助代號       | string       | H.SALES_NO(SQL-1)                                               |
| batchQty       | 每Batch用量    | string       | H.BATCH_QTY(SQL-1)                                              |
| batchTimes     | Batch用量次數  | string       | H.BATCH_TIMES(SQL-1)                                            |
| remainQty      | 尾數量         | string       | H.REMAIN_QTY(SQL-1)                                             |
| remainTimes    | 尾數量次數     | string       | H.REMAIN_TIMES(SQL-1)                                           |
| userId         | 登入者使用者ID | string       | {登入者使用者ID}                                                |
| woQty          |                | string       | {organizationId}等於"6000"時: `startQty(SAP-1) + " " + unit(SAP-1)` 否則為: `salesQty(SAP-1) + " " + unit(SAP-1)` |
| woQtyTitle     |                | string       | {organizationId}等於"6000"時: "工單數量" 否則為: "訂單數量"     |
| rawQty         |                | string       | H.RAW_QTY(SQL-1)                                                |
| custPoNo       |                | string       | H.CUST_PO_NO(SQL-1)                                             |
| shipDate       |                | string       | ESTIMATED_SHIP_DATE(SQL-1)                                      |
| SUBREPORT_DIR  |                | string       | `application.getRealPath("/jasperReport/WoPrintD_Sis2.jasper")` |
| SubReportParam |                | array object | 空陣列                                                          |
| SubReportDS    |                | JRDataSource |                                                                 |

SubReportDS JRDataSource 參數
* 遍歷{aryProdRemark}，{i}為每次遍歷index

| 參數名稱 | 名稱         | 型態   | 來源資料 & 說明 |
| -------- | ------------ | ------ | --------------- |
| sis1     | 出貨指示備註 | string | 若{i}為偶數時   |
| sis2     | 出貨指示備註 | string | 若{i}為奇數時   |


jrds JRDataSource 參數
* 遍歷{openListM}

| 參數名稱 | 名稱 | 型態   | 來源資料 & 說明      |
| -------- | ---- | ------ | -------------------- |
| compItem | 色料 | string | {openListM.compItem} |

# Response

```
Content-Type: application/pdf
Content-Disposition: inline;filename=WoPrintD.pdf
```

#### Response 範例
