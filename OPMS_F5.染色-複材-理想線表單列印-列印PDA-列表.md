# 染色-複材-理想線表單列印-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230911 | 新增規格 | 黃東俞 |
| 20231016 | 增加分頁 | Ellen  |
| 20231018 | 增加排序 | Ellen  |

## 來源URL及資料格式

| 項目   | 說明         |
| ------ | ------------ |
| URL    | /wo/soc_list |
| method | post         |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             | 名稱     | 資料型別 | 必填 | 資料儲存 & 說明                                                          |
| ---------------- | -------- | -------- | :--: | ------------------------------------------------------------------------ |
| opOrganizationId | 工廠別   | string   |  M   |                                                                          |
| opItemNo         | 料號     | string   |  M   | 查詢公版PDA時傳入 "公版"                                                 |
| opBomUid         | BOM ID   | string   |  M   |                                                                          |
| opPage           | 第幾頁   | integer  |  O   | 預設`1`，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推 |
| opPageSize       | 分頁筆數 | integer  |  O   | 預設 `10`                                                                |
| opOrder          | 排序方式 | string   |  O   | "ASC":升冪排序 / "DESC":降冪排序                                          |

#### Request 範例

```json
{
  "opOrganizationId": "5000",
  "opItemNo":"LCCGM10J1XBXXXXXXX",
  "opBomUid":"5000-LCCGM10J1XBXXXXXXX01",
  "opPage":1,
  "opPageSize":10,
}
```


# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 參考SQL-1進行查詢
* {offset} 為起始列印 row ，第一頁為 0，第二頁後為 0 + (opPageSize*(opPage-1))
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
-- WoQueryDao.getWoSocList
SELECT A.SOC_UID, A.SOC_VER, A.EQUIP_ID, A.EQUIP_NAME, A.PROD_TYPE, A.ITEM_NO, A.SOC_DESC, A.STATUS, A.PUB_DATE, A.PUBLIC_NAME
FROM (SELECT S.SOC_M_UID SOC_UID, S.SOC_M_VER SOC_VER, E.EQUIP_ID, E.EQUIP_NAME, E.PROD_TYPE, S.ITEM_NO, I.MTR_NAME SOC_DESC, S.STATUS,
  CONVERT(VARCHAR(10),S.PUB_DATE,111) PUB_DATE, U.USER_NAME PUBLIC_NAME
  FROM SOC_M S
  INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
  LEFT JOIN ITEM_H I ON S.ITEM_NO = I.ITEM_NO
  LEFT JOIN SYS_USER U ON S.PUBLIC_BY = U.USER_ID
  WHERE E.ORGANIZATION_ID = {opOrganizationId}
  AND S.ITEM_NO = {opItemNo}
  AND S.BOM_UID = {opBomUid}
  AND S.STATUS IN ('Y','W')
  UNION
  SELECT S.SOC_C_UID SOC_UID, S.SOC_C_VER SOC_VER,E.EQUIP_ID,E.EQUIP_NAME,E.PROD_TYPE, S.ITEM_NO,S.SOC_DESC,S.STATUS,
  CONVERT(VARCHAR(10),S.PUB_DATE,111) PUB_DATE, U.USER_NAME PUBLIC_NAME
  FROM SOC_C S
  INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
  LEFT JOIN SYS_USER U ON S.PUBLIC_BY = U.USER_ID
  WHERE E.ORGANIZATION_ID = {opOrganizationId}
  AND S.ITEM_NO = {opItemNo}
  AND S.STATUS IN ('Y','W')) A

--如{opOrder}為 null 則加入以下SQL
ORDER BY EQUIP_NAME, SOC_VER DESC

--如{opOrder}非 null 則加入以下SQL
ORDER BY A.PUB_DATE {opOrder}

OFFSET {offset} ROWS
FETCH NEXT {opageSize} ROWS ONLY
```

# Response 欄位
| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
| -------- | -------- | -------- | --------------- |
| pageInfo | 分頁資訊 | object   |                 |
| content  | 附帶訊息 | array    |                 |

#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明 |
| ---------- | ------------ | -------- | --------------- |
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |

#### 【content】
| 欄位         | 名稱        | 資料型別 | 資料儲存 & 說明 |
| ------------ | ----------- | -------- | --------------- |
| opSocUid     | SOC ID      | string   |                 |
| opSocVer     | 版號        | string   |                 |
| opEquipId    | 機台        | string   |                 |
| opEquipName  | 機台名稱    | string   |                 |
| opItemNo     | 料號        | string   |                 |
| opProdType   | 生產型態    | string   |                 |
| opSocDesc    | SOC識別文字 | string   |                 |
| opStatus     | 狀態        | string   |                 |
| opPubDate    | 發行日期    | string   |                 |
| opPublicName | 發行人      | string   |                 |

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "pageInfo": {
      "pageNumber": 1, 
      "pageSize": 10,
      "pages": 1,      
      "total": 1     
    },
    "content":[
      {
        "opSocUid":"CD9D1F36-8226-4353-9682-989657269C61",
        "opSocVer":"7",
        "opEquipId":"38",
        "opEquipName":"染色少8",
        "opItemNo":"LCCGM10J1XBXXXXXXX",
        "opProdType":"C",
        "opSocDesc":"ZC-FT15/PC-175",
        "opStatus":"Y",
        "opPubDate":"2017/03/06",
        "opPublicName":"陳進丁"
      }
    ]
  }
}
```