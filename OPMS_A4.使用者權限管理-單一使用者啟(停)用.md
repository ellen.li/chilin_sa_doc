# 使用者權限管理-單一使用者啟(停)用

###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  
參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)

針對單一使用者帳號啟用/停用


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230404 | 新增規格 |    |

## 來源URL及方法

| 項目   | 說明             |
| ------ | ---------------- |
| URL    | /user/update_status |
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

## Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位         | 名稱         | 資料型別 |必填| 資料儲存 & 說明          |
| ------------ | ------------ | -------- |---| ------------------------ |
|  opUserId    | 使用者ID      |  string  |    M      |  |
|  opActive    | 啟用      |  string  |    M      | "1":啟用, "0": 停用 |



## Request 範例

```json
{
  "opUserId":"richman",
  "opActive":"0"
}
```

## Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* {DATAKEY} = {opUserId}
* 執行  SQL-1 確認該使用者存在，找不到 return 400,"NOTFOUND"
* 執行  SQL-2 update 使用者啟用狀態
* 如果SQL-2 執行成功， opActive 如果為 "1" 則 {logContent}內容為 "啟用"，否則{logContent}內容為 "停用" 
* 執行 SQL-3 紀錄LOG
  有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

## Request 後端邏輯說明

SQL-1:
```sql
SELECT * FROM  SYS_USER WHERE USER_ID={opUserId}
```
SQL-2:
```sql
UPDATE SYS_USER SET ACTIVE={opActive} WHERE USER_ID={opUserId}
```
SQL-3:
```sql
INSERT INTO  SYS_MODI_LOG (CATEGORY,DATAKEY,USERID,USER_NAME,LOG) VALUES ('USER',{DATAKEY},{登入者使用者ID},{登入者使用者名稱}, {logContent})
```

## Response 欄位

| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
|----------|--------|----------|-----------------|
| opUserId | 使用者ID | string   |                 |


## Response 範例

```json
{
  "msgCode": null, 
  "result":{
    "content": {
       "opUserId":"richman"
    }
  }
}
```

