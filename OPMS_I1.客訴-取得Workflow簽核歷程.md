# 客訴-取得 Workflow 簽核歷程
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得 Workflow 簽核歷程


## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230703 | 新增規格 | Nick   |

## 來源URL及資料格式

| 項目   | 說明                  |
|--------|-----------------------|
| URL    | /cs/query_wf_task_pre |
| method | post                  |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             | 名稱     | 資料型別 | 必填 | 資料儲存 & 說明 |
|------------------|--------|:--------:|:----:|-----------------|
| opOrganizationId | 工廠別   |  string  |  M   |                |
| opInsId          | WF表單ID |  string  |  M   |                 |



#### Request 範例
```json
{
  "opOrganizationId":"5000",
  "opInsId":"Ans000003309671"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 執行 WFSQL-1 取得簽核歷程
  * 滾每一筆資料 
    * 如 MEM_USER_NAME 等於 EXE_USER_NAME，則 {opMeMExeUserName} = MEM_USER_NAME，反之則  {opMeMExeUserName} = MEM_USER_NAME + '/' + EXE_USER_NAME
    * 其餘欄位依照資料庫欄位值列印
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

WFSQL-1: 

```sql
--CsWfDao.getWfTaskList(String insid)
SELECT  P.NAME TASK_NAME,M1.USERNAME EXE_USER_NAME,M2.USERNAME MEM_USER_NAME,
CONVERT(CHAR,DATEADD (second,convert(decimal,replace(T.StartTime,' ',''))/1000+28800,{d '1970-01-01'}),120) AS START_TIME,
CASE WHEN T.EndTime='0' THEN '' ELSE CONVERT(CHAR,DATEADD (second,convert(decimal,replace(T.EndTime,' ',''))/1000+28800, {d '1970-01-01'}),120) END AS COMP_TIME,T.STATE STATUS 
 FROM ART00131387171715272_LOG I 
 INNER JOIN TASK T ON I.TSKID=T.TSKID 
 INNER JOIN PRO_GENINF P ON T.PROID=P.PROID 
 INNER JOIN MEM_GENINF M1 ON T.MEMID=M1.MEMID 
 INNER JOIN MEM_GENINF M2 ON T.EXEID=M2.MEMID 
 WHERE I.INSID={opInsId}
ORDER BY EXEORDER*1
```



# Response 欄位
【 content array 】
| 欄位             | 名稱            | 資料型別 | 資料儲存 & 說明               |
|------------------|---------------|----------|-------------------------------|
| opTaskName       | 關卡名稱        | string   | TASK_NAME                     |
| opMeMExeUserName | 處理人員/代理人 | string   | 範例: 王鮮生 or 王鮮生/白曉解 |
| opStartTime      | 到達時間        | string   | START_TIME                    |
| opCompTime       | 完成時間        | string   | COMP_TIME                     |
| opStatus         | 狀態            | string   | STATUS                        |



#### Response 範例
```json
{
   "msgCode":null,
   "result":{
      "content":[
         {
            "opTaskName":"申請者填單",
            "opMeMExeUserName":"王秋虹",
            "opStartTime":"2015-02-26 15:53:50",
            "opCompTime":"2015-02-26 15:53:50",
            "opStatus":"complete"
         },
         {
            "opTaskName":"CLT立案核決權限判斷",
            "opMeMExeUserName":"系統管理員",
            "opStartTime":"2015-02-26 15:53:50",
            "opCompTime":"2015-02-26 15:53:50",
            "opStatus":"complete"
         },
         {
            "opTaskName":"CLT主管審核",
            "opMeMExeUserName":"林憲聰",
            "opStartTime":"2015-02-26 15:53:50",
            "opCompTime":"2015-03-02 09:33:31",
            "opStatus":"complete"
         },
         {
            "opTaskName":"CLT立案核決權限判斷",
            "opMeMExeUserName":"系統管理員",
            "opStartTime":"2015-03-02 09:33:31",
            "opCompTime":"2015-03-02 09:33:31",
            "opStatus":"complete"
         },
         {
            "opTaskName":"CLT CS審核",
            "opMeMExeUserName":"陳昭安",
            "opStartTime":"2015-03-02 09:33:31",
            "opCompTime":"2015-03-02 13:26:43",
            "opStatus":"complete"
         },
         {
            "opTaskName":"CLT業務處審核",
            "opMeMExeUserName":"曾國希",
            "opStartTime":"2015-03-02 13:26:43",
            "opCompTime":"2015-03-03 11:59:31",
            "opStatus":"complete"
         },
         {
            "opTaskName":"品管部主管審核",
            "opMeMExeUserName":"陳柏村",
            "opStartTime":"2015-03-03 11:59:31",
            "opCompTime":"2015-03-04 09:49:12",
            "opStatus":"complete"
         },
         {
            "opTaskName":"品管PM審核",
            "opMeMExeUserName":"周鉦斌",
            "opStartTime":"2015-03-04 09:49:12",
            "opCompTime":"2015-03-18 10:03:35",
            "opStatus":"complete"
         },
         {
            "opTaskName":"責任部門簽核",
            "opMeMExeUserName":"系統管理員",
            "opStartTime":"2015-03-18 10:03:35",
            "opCompTime":"2015-03-18 18:11:24",
            "opStatus":"complete"
         },
         {
            "opTaskName":"品管PM匯總報告",
            "opMeMExeUserName":"周鉦斌",
            "opStartTime":"2015-03-18 18:11:24",
            "opCompTime":"2015-03-19 11:07:36",
            "opStatus":"complete"
         },
         {
            "opTaskName":"品管核決權限判斷",
            "opMeMExeUserName":"系統管理員",
            "opStartTime":"2015-03-19 11:07:36",
            "opCompTime":"2015-03-19 11:07:36",
            "opStatus":"complete"
         },
         {
            "opTaskName":"品管主管審核",
            "opMeMExeUserName":"王秋虹",
            "opStartTime":"2015-03-19 11:07:36",
            "opCompTime":"2015-03-19 11:19:02",
            "opStatus":"complete"
         },
         {
            "opTaskName":"品管核決權限判斷",
            "opMeMExeUserName":"系統管理員",
            "opStartTime":"2015-03-19 11:19:02",
            "opCompTime":"2015-03-19 11:19:02",
            "opStatus":"complete"
         },
         {
            "opTaskName":"品管主管審核",
            "opMeMExeUserName":"陳柏村",
            "opStartTime":"2015-03-19 11:19:02",
            "opCompTime":"2015-03-19 11:33:12",
            "opStatus":"complete"
         },
         {
            "opTaskName":"品管核決權限判斷",
            "opMeMExeUserName":"系統管理員",
            "opStartTime":"2015-03-19 11:33:12",
            "opCompTime":"2015-03-19 11:33:12",
            "opStatus":"complete"
         },
         {
            "opTaskName":"CLT CS審核",
            "opMeMExeUserName":"陳昭安",
            "opStartTime":"2015-03-19 11:33:12",
            "opCompTime":"2015-03-19 11:48:15",
            "opStatus":"complete"
         },
         {
            "opTaskName":"CLT業務處審核",
            "opMeMExeUserName":"鄭榮富/曾國希",
            "opStartTime":"2015-03-19 11:48:15",
            "opCompTime":"2015-03-19 11:53:07",
            "opStatus":"complete"
         },
         {
            "opTaskName":"CLT生產處",
            "opMeMExeUserName":"鄭榮富",
            "opStartTime":"2015-03-19 11:53:07",
            "opCompTime":"2015-03-19 11:53:26",
            "opStatus":"complete"
         },
         {
            "opTaskName":"CLT總經理",
            "opMeMExeUserName":"盧建良",
            "opStartTime":"2015-03-19 11:53:26",
            "opCompTime":"2015-03-20 08:21:42",
            "opStatus":"complete"
         },
         {
            "opTaskName":"董事長簽核判斷",
            "opMeMExeUserName":"系統管理員",
            "opStartTime":"2015-03-20 08:21:42",
            "opCompTime":"2015-03-20 08:21:42",
            "opStatus":"complete"
         }
      ]
   }
}
```

## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考

* CsManageAction.csQueryWfTaskPre