# 共用_歷史紀錄查看
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

此歷史紀錄查看為共用記錄，依照不同條件查詢相關 LOG


## 修改歷程

| 修改時間 | 內容                            | 修改者 |
| -------- | ------------------------------- | ------ |
| 20230405 | 新增規格                        | Nick   |
| 20230531 | 增加 PATTERN                    | Nick   |
| 20230815 | 調整 TOP 1000(使用OFFSET FETCH) | Nick   |
| 20231208 | 產銷進度紀錄改讀table SO_D_REC  | Ellen  |

## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    | /log/list        |
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位       | 名稱     | 資料型別 | 必填 | 資料儲存 & 說明                                                                              |
|------------|---------|:--------:|:----:|--------------------------------------------------------------------------------------------|
| opLogType  | LOG 類別 |  string  |  M   | 詳細選項值請參考 【opLogType 資料選項值】                                                      |
| opDataKey  | KEY值    |  string  |  O   | 預設 `null` 如為單一key 請直接給予，如果是複合鍵則中間使用\|\|合併， Ex: KEY1\|\|KEY2 依此類推 |
| opOrder    | 排序方式 |  string  |  O   | 預設`DESC`，"ASC":升冪  /"DESC" :降冪                                                         |
| opPage     | 第幾頁   | integer  |  O   | 預設`1`，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推                      |
| opPageSize | 分頁筆數 | integer  |  O   | 預設 `10`                                                                                    |


【opLogType 資料選項值】
|   選項值    | 說明         |
|:-----------:|------------|
|    ROLE     | 角色管理     |
|    USER     | 使用者管理   |
|    FUNC     | 功能管理     |
|   PATTERN   | 色號管理     |
|     BOM     | BOM管理      |
|  SISCONFIG  | 出貨指示設定 |
| SISSOMANAGE | 出貨指示管理 |
|    DDMFG    | 產銷進度-生管交期 |


#### Request 範例

```json
{
  "opLogType":"ROLE",
  "opPage":1,
  "opageSize":3
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* {offset} 為起始列印 row ，第一頁為 0，第二頁後為 0 + (opPageSize*(opPage-1))
* 如 {opDataKey} 非 null 需增加 DATAKEY 過濾，反之則不需要。
* 若{opLogType}等於`DDMFG` 參考 SQL-2 查回結果 result
* 否則 執行  SQL-1 撈取相關LOG
* 若無資料 return 200 ，pageInfo 內相關參數皆為 0，content 為 null
* 若有資料 return 200 , pageInfo 請依據實際數據填入，content 請參考範例格式，為資料陣列
  有系統錯誤 return 500,"SYSTEM_ERROR"

# Request 後端邏輯說明

SQL-1:

```sql
SELECT *
FROM (
        SELECT CDT,
            USER_NAME,
            LOG
        FROM SYS_MODI_LOG
        WHERE CATEGORY = { opLogType }
            AND DATAKEY = { opDataKey }
        ORDER BY ID { opOrder } OFFSET 0 ROWS FETCH NEXT 1000 ROWS ONLY
    ) AS TOP1000T
ORDER BY (SELECT NULL) OFFSET { offset } ROWS FETCH NEXT { opageSize } ROWS ONLY
```

SQL-2:

```sql
SELECT
  ISNULL(FORMAT(R.DATE_BEFORE, 'MM/dd'), 'N/A') + ' => ' + ISNULL(FORMAT(R.DATE_AFTER, 'MM/dd'), 'N/A') AS LOG,
  R.CDT,
  U.USER_NAME 
FROM
  SO_D_REC R
  LEFT JOIN SYS_USER U ON R.CREATE_BY = U.USER_ID
WHERE
  1=1 
  AND R.WO_UID = { opDataKey }
ORDER BY CDT DESC
```

# Response 欄位
| 欄位       | 名稱     | 資料型別 | 資料儲存 & 說明                  |
|------------|--------|----------|----------------------------------|
| opCDT      | 更新時間 | string   | CDT(轉換格式為 YYYY/MM/DD HH:mm) |
| opUserName | 更新人員 | string   | USER_NAME                        |
| opLog      | LOG 內容 | string   | LOG                              |


#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "pageInfo": {
      "pageNumber": 1, 
      "pageSize": 3,  
      "pages": 5,      
      "total": 15     
    },
    "content":[
      {
        "opCDT":"2023/01/01 15:31",
        "opUserName":"路人甲阿堃",
        "opLog":"建立"
      },
      {
        "opCDT":"2023/01/01 15:32",
        "opUserName":"路人甲阿堃",
        "opLog":"修改"
      },
      {
        "opCDT":"2023/01/01 15:33",
        "opUserName":"路人甲阿堃",
        "opLog":"停用"
      }
      ]
  }
}
```

