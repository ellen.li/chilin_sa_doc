# 報表-色料領用統計表-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

統計期間所使用之色料料號、規格以及用量。

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230731 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                 |
|--------|---------------------|
| URL    | /report/pgm_list    |
| method | post                |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱    |  資料型別 | 必填 | 資料儲存 & 說明  |
|--------------|---------|----------|:----:|-----------------|
| opBDate      | 開始日期 | String   |  O   | yyyy/mm/dd      |
| opEDate      | 結束日期 | String   |  O   | yyyy/mm/dd      |


#### Request 範例

```json
{
  "opBDate": "2019/01/01",
  "opEDate": "2019/01/31"
}
```

# Request 後端流程說明

* 取{ 登入者工廠別 } SYS_USER.ORGANIZATION_ID 欄位
* 從工單色粉檔 WO_PGM 和工單主檔 WO_H 取得資料，WO_PGM.WO_UID = WO_H.WO_UID，參考SQL-1。
  * 計算領用數量(KG)：SUM(WO_H.RAW_QTY * WO_PGM.QUANTITY_PER + WO_H.RAW_QTY * WO_PGM.MODIFY_UNIT_QTY) 並且以 WO_PGM.UNIT 為單位，若單位不是KG時需換算成KG，並取到小數點2位，輸出時轉為千分位格式字串
  * 條件-日期：H.WO_DATE >= { opBDate } 和 H.WO_DATE <= { opEDate }，如未傳入則不限制。
  * 條件-工廠別：WO_H.ORGANIZATION_ID = { 登入者工廠別 }
  * 條件-工單類別：WO_H.WIP_CLASS_CODE = '%重工%'
  * 將查詢結果依色粉料號 OWO_PGM.WO_PGM_PART_NO 進行分組加總 GROUP BY WO_PGM_PART_NO
  * 查詢結果依 OWO_PGM.WO_PGM_PART_NO 進行順序排序
* 查詢品名規格：ITEM_H.ITEM_NO = 色料料號，取MAX(ITEM_H.ITEM_DESC)為品名規格，參考SQL-2
* 計算opSumUsage(用量加總)：將所有色料的領用數量加總起來，同樣取兩位小數並轉為千分位格式
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
-- ReportQueryDao.getPgmSummeryReport
-- 查詢色料料號與領用數量(KG)
SELECT P.WO_PGM_PART_NO,
      CONVERT(float,ROUND(SUM((H.RAW_QTY * P.QUANTITY_PER + H.RAW_QTY * ISNULL(P.MODIFY_UNIT_QTY,0)) * (CASE P.UNIT WHEN 'KG' THEN 1000 ELSE 1 END)) / 1000, 2))
FROM WO_PGM P
INNER JOIN WO_H H ON P.WO_UID=H.WO_UID
WHERE H.ORGANIZATION_ID={ 登入者工廠別 } AND H.WIP_CLASS_CODE NOT lIKE '%重工%'
AND H.WO_DATE >= { opBDate }
AND H.WO_DATE <= { opEDate }
GROUP BY P.WO_PGM_PART_NO ORDER BY P.WO_PGM_PART_NO
```

SQL-2:
```sql
-- ItemQueryDao.getItemDescription
-- 查詢品名規格
SELECT MAX(ITEM_DESC)  FROM ITEM_H WHERE ITEM_NO = {OWO_PGM.WO_PGM_PART_NO}
```

# Response 欄位
| 欄位    | 名稱     | 資料型別 | 說明     |
|---------|--------|----------|----------|
| msgCode | 訊息代碼 | string   |          |
| result  | 回傳資料 | object   | 進階訊息 |

#### 【result】child node
| 欄位     | 名稱     | 資料型別 | 來源資料 & 說明 |
|----------|--------|----------|-----------------|
| content  | 附帶訊息 | array    |                 |


#### 【content】child node

| 欄位         | 名稱        | 資料型別 | 來源資料 & 說明 |
|-------------|------------ |----------|-----------------|
| opSumPgmQty | 用量加總     | string  | 加總領用數量(opSumTotal)，取兩位小數點且為千分位格式，例如：9,999.99 |
| opPgmData   | 色料用量資料 | array   |                                          |


#### 【opPgmData】child node

| 欄位            | 名稱           | 資料型別   | 資料儲存 & 說明          |
|-----------------|---------------|-----------|-------------------------|
| opWoPgmPartNo   |  色料料號      |  String   | OWO_PGM.WO_PGM_PART_NO  |
| opWoPgmPartDesc |  品名規格      |  String   | MAX(ITEM_H.ITEM_DESC)   |
| opSumTotal      |  領用數量(KG)  |  String   | 取兩位小數點且為千分位格式，例如：9,999.99  |


#### Response 範例

```json
{
  "msgCode":"",
  "result":{
    "content":{
      "opSumPgmQty": "777.77",
      "opPgmData": [
        {
          "opWoPgmPartNo":"04-251-A-BOMDJ110X",
          "opWoPgmPartDesc":"客供.環德.OM-DJ110.該客戶提供抗菌劑色母粒",
          "opSumTotal":"7.00",
        },
        {
          "opWoPgmPartNo":"04-251-A-L091XXXXX",
          "opWoPgmPartDesc":"助劑(A,L開頭).ChiMei..PETS..分散劑",
          "opSumTotal": "1,294.19",
        },
        {
          "opWoPgmPartNo":"04-251-A-O04XXXXXX",
          "opWoPgmPartDesc":"助劑(A,L開頭).CMC.A-1076.抗氧化劑",
          "opSumTotal": "102.08",
        }
      ]
    }
  }
}
```