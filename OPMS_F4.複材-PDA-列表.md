# 複材-PDA-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)


**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230830 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /pda/pda_m_list             |
| method | post                        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱        |  資料型別 | 必填 | 資料儲存 & 說明                                                     |
|--------------|-------------|----------|:----:|--------------------------------------------------------------------|
| opItemNo     | 料號        | string   |  O    |                                                                    |
| opEquipId    | 機台        | integer  |  O    | 參考下方說明                                                        |
| opBDate      | 列印日期開始 | string   |  O    | yyyy/mm/dd                                                         |
| opEDate      | 列印日期結束 | string   |  O    | yyyy/mm/dd                                                         |
| opPage       | 第幾頁      | integer  |  O    | 預設 1，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推 |
| opPageSize   | 分頁筆數    | integer  |  O    | 預設 10                                                             |
| opOrder      | 排序方式    | string   |  O    | "ASC":升冪排序 / "DESC":降冪排序   預設`DESC`                        |

* 前端機台選項(opEquipId)，由API [C.BOM維護-取得排程機台列表(by工廠別.生產單位.生產型態) /bom/get_ps_equipment_list](./OPMS_C.BOM維護-取得排程機台列表(by工廠別.生產單位.生產型態).md) 取得，傳入參數：
  * opOrganizationId：登入者工廠別
  * opProdType：固定填'M'

```json
{
  "opItemNo": null,
  "opEquipId": null,
  "opBDate": "2019/01/01",
  "opEDate": "2019/12/31",
  "opPage": 1,
  "opPage": 10
}
```

# Request 後端流程說明

* 取{登入者工廠別} SYS_USER.ORGANIZATION_ID 欄位
* 參考SQL-1，欄位皆為非必填，有傳入的欄位才需加入SQL篩選
* 只最多取前1000筆資料並進行分頁再回傳資料
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

SQL-1:
```sql
-- PdaQueryDao.getPdaMResultList
SELECT P.PDA_M_UID PDA_UID, S.SOC_M_UID SOC_UID, S.SOC_M_VER SOC_VER,S.EQUIP_ID, E.EQUIP_NAME, S.ITEM_NO,
  W.WO_NO, P.MFG_DATE ,P.SHIFT_CODE, S.PUB_DATE, U1.USER_NAME PUBLIC_NAME, P.CDT CDT, U2.USER_NAME CREATE_BY_NAME
FROM PDA_M P
INNER JOIN SOC_M S ON P.SOC_M_UID = S.SOC_M_UID
INNER JOIN WO_H W ON P.WO_UID = W.WO_UID
INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
INNER JOIN SYS_USER U1 ON S.PUBLIC_BY = U1.USER_ID
INNER JOIN SYS_USER U2 ON P.CREATE_BY = U2.USER_ID
WHERE E.ORGANIZATION_ID = {登入者工廠別}
AND P.CDT >= {opBDate}
AND P.CDT <= {opEDate}
AND S.EQUIP_ID = {opEquipId}
AND S.ITEM_NO like {%opItemNo%}
ORDER BY P.CDT {opOrder}
```

# Response 欄位
| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
| -------- | -------- | -------- | --------------- |
| pageInfo | 分頁資訊 | object   |                 |
| content  | 附帶訊息 | array    |                 |

#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明 |
| ---------- | ------------ | -------- | --------------- |
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |

#### 【content】array
| 欄位        | 名稱         | 資料型別 | 資料儲存 & 說明    |
|-------------|-------------|---------|--------------------|
| opPdaUid       | PDA ID      | string  | PDA_UID                       |
| opSocUid       | SOC ID      | string  | SOC_M.SOC_M_UID               |
| opWoNo         | 工單號碼     | string  | WO_H.WO_NO                    |
| opEquipId      | 機台        | string  | SOC_M.EQUIP_ID                |
| opEquipName    | 機台名稱     | string  | EQUIP_H.EQUIP_NAME            |
| opItemNo       | 料號        | string  | SOC_M.ITEM_NO                 |
| opSocVer       | 版號        | string  | SOC_M.SOC_M_VER               |
| opPubDate      | 發行日期     | string  | SOC_M.PUB_DATE  yyyy/mm/dd    |
| opPublicName   | 發行人名稱   | string  | PUBLIC_NAME                   |
| opCdt          | 列印日期     | string  | PDA_M.CDT yyyy/mm/dd hh:mm:ss |
| opCreateByName | 列印人員     | string  | CREATE_BY_NAME                |
| opMfgDate      | 製造日期     | string  | MFG_DATE  yyyy/mm/dd          |


```json
{
  "msgCode":null,
  "result":{
    "pageInfo": {
      "pageNumber": 1,
      "pageSize": 10,
      "pages": 1,
      "total": 3
    },
    "content": [
      {
        "opPdaUid":"2B282E8F-D4C3-4F8B-8A80-A4F76E7F45B5",
        "opSocUid":"CBA386EB-D873-47B4-99E1-A8A1171286F4",
        "opWoNo":"52019273",
        "opEquipId":"12",
        "opEquipName":"複材#2",
        "opItemNo":"978BXXXXX1BXC13018",
        "opSocVer":"2",
        "opPubDate":"2022/10/17",
        "opPublicName":"陳韋學",
        "opCdt":"2023/03/28 07:58:23",
        "opCreateByName":"王榆翔",
        "opMfgDate":"2023/03/28"
      },
      {
        "opPdaUid":"0E643E2F-938C-46D2-B738-CFC9A74AAB0F",
        "opSocUid":"CBA386EB-D873-47B4-99E1-A8A1171286F4",
        "opWoNo":"52019273",
        "opEquipId":"12",
        "opEquipName":"複材#2",
        "opItemNo":"978BXXXXX1BXC13018",
        "opSocVer":"2",
        "opPubDate":"2022/10/17",
        "opPublicName":"陳韋學",
        "opCdt":"2023/03/27 22:59:36",
        "opCreateByName":"吳政和",
        "opMfgDate":"2023/03/27"
      },
      {
        "opPdaUid":"AE6B6897-19BC-428A-BB88-9B0C2C12E772",
        "opSocUid":"CBA386EB-D873-47B4-99E1-A8A1171286F4",
        "opWoNo":"52019273",
        "opEquipId":"12",
        "opEquipName":"複材#2",
        "opItemNo":"978BXXXXX1BXC13018",
        "opSocVer":"2",
        "opPubDate":"2022/10/17",
        "opPublicName":"陳韋學",
        "opCdt":"2023/03/27 16:54:17",
        "opCreateByName":"孫文生",
        "opMfgDate":"2023/03/28"
      }
    ]
  }
}
```











