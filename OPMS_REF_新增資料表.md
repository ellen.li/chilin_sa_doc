
[返回總覽](./OPMS_00.總覽.md)  
# 新增資料表清單
<font size=2>

  # A.系統管理

* 資料表名稱:SYS_FUNCTION_V3
* 中文名稱:  新系統功能設定檔
  | 欄位名稱  |     中文     | 形態      | 大小 | 主鍵 | 說明     |
  |-----------|:------------:|-----------|------|------|----------|
  | CODE      | 前端元件代碼 | nvarchar  | 50   | PK   |          |
  | CNAME     |   功能名稱   | nvarchar  | 255  |      | NOT NULL |
  | LEVEL     |     階層     | int       |      |      |          |
  | PARENT    |   父階元件   | nvarchar  | 50   |      |          |
  | URL       |     URL      | nvarchar  | 2000 |      |          |
  | VISIBLE   |    可顯示    | bit       |      |      |          |
  | SORT_ID   |     排序     | int       |      |      |          |
  | CDT       |   建立時間   | datetime2 |      |      |          |
  | CREATE_BY |    建立者    | nvarchar  | 200  |      |          |
  | UDT       |   更新時間   | datetime2 |      |      |          |
  | UPDATE_BY |    更新者    | nvarchar  | 200  |      |          |
  ```SQL
  CREATE TABLE [SYS_FUNCTION_V3](
    CODE      nvarchar(50) PRIMARY KEY,
    CNAME     nvarchar(255) NOT NULL,
    LEVEL     integer NOT NULL,
    PARENT    nvarchar(50) ,
    URL       nvarchar(2000),
    VISIBLE   bit NOT NULL DEFAULT 1,
    SORT_ID   integer,
    CDT       datetime2,
    CREATE_BY nvarchar(200),
    UDT       datetime2,
    UPDATE_BY nvarchar(200)
  )
  ```

* 資料表名稱: SYS_FUNCTION_PERMS
* 中文名稱:  新系統功能子權限設定檔(跟權限有關係都必需抓，此table 為出貨預設資料)
  | 欄位名稱  |     中文     | 形態      | 大小 | 主鍵 | 說明                     |
  |-----------|:----------:|-----------|------|------|--------------------------|
  | CODE      | 前端元件代碼 | nvarchar  | 50   | PK   |                          |
  | PERM_CODE |   權限類別   | nvarchar  | 20   | PK   | 可用 edit,query 代表權限 |
  | CNAME     |   功能名稱   | nvarchar  | 255  |      | NOT NULL                 |
  | SORT_ID   |     排序     | int       |      |      |                          |
  | CDT       |   建立時間   | datetime2 |      |      |                          |
  | CREATE_BY |    建立者    | nvarchar  | 200  |      |                          |
  | UDT       |   更新時間   | datetime2 |      |      |                          |
  | UPDATE_BY |    更新者    | nvarchar  | 200  |      |                          |
  ```SQL
  CREATE TABLE [SYS_FUNCTION_PERMS](
    CODE  nvarchar (50),
    PERM_CODE  nvarchar (20),
    CNAME    nvarchar  (255) NOT NULL,
    SORT_ID   integer,
    CDT       datetime2,
    CREATE_BY nvarchar(200),
    UDT       datetime2,
    UPDATE_BY nvarchar(200)
    PRIMARY KEY(CODE,PERM_CODE)
  )
  ```



* 資料表名稱: SYS_ROLE_FUNCTION_V3
* 中文名稱:  新系統角色授權功能設定檔
  | 欄位名稱  |     中文     | 形態      | 大小    | 主鍵 | 說明 |
  |-----------|:----------:|-----------|---------|------|------|
  | CODE      | 前端元件代碼 | nvarchar  | 50      | PK   |      |
  | ROLE_ID   |    角色id    | numeric   | (10, 0) | PK   |      |
  | CDT       |   建立時間   | datetime2 |         |      |      |
  | CREATE_BY |    建立者    | nvarchar  | 200     |      |      |
  | UDT       |   更新時間   | datetime2 |         |      |      |
  | UPDATE_BY |    更新者    | nvarchar  | 200     |      |      |
  ```SQL
  CREATE TABLE [SYS_ROLE_FUNCTION_V3](
    CODE  nvarchar (50),
    ROLE_ID  numeric(10, 0),
    CDT       datetime2,
    CREATE_BY nvarchar(200),
    UDT       datetime2,
    UPDATE_BY nvarchar(200)
    PRIMARY KEY(CODE,ROLE_ID)
  )
  ```
* 資料表名稱: SYS_ROLE_FUNCTION_PERMS
* 中文名稱:  新系統子權限角色授權設定檔(跟權限有關係都必需抓，此table 為出貨預設資料)
  | 欄位名稱  |     中文     | 形態      | 大小    | 主鍵 | 說明 |
  |-----------|:----------:|-----------|---------|------|------|
  | CODE      | 前端元件代碼 | nvarchar  | 50      | PK   |      |
  | ROLE_ID   |    角色id    | numeric   | (10, 0) | PK   |      |
  | PERM_CODE |   權限類別   | nvarchar  | 20      | PK   |      |
  | CDT       |   建立時間   | datetime2 |         |      |      |
  | CREATE_BY |    建立者    | nvarchar  | 200     |      |      |
  | UDT       |   更新時間   | datetime2 |         |      |      |
  | UPDATE_BY |    更新者    | nvarchar  | 200     |      |      |
  ```sql
  CREATE TABLE [SYS_ROLE_FUNCTION_PERMS](
    CODE  nvarchar (50),
    ROLE_ID  numeric(10, 0),
    PERM_CODE  nvarchar(20),
    CDT       datetime2,
    CREATE_BY nvarchar(200),
    UDT       datetime2,
    UPDATE_BY nvarchar(200)
    PRIMARY KEY(CODE,ROLE_ID,PERM_CODE)
  )
  ```

* 資料表名稱: SYS_MODI_LOG
* 中文名稱:  異動記錄檔
  | 欄位名稱  |    中文     | 形態      | 大小 | 主鍵 | 說明                                             |
  |-----------|:---------:|-----------|------|------|--------------------------------------------------|
  | ID        | 自動流水編  | BIGINT    |      | PK   | IDENTITY 自動+1                                  |
  | CATEGORY  |    種類     | nvarchar  | 30   |      | 例如 SYS_ROLE 代表角色編輯的LOG                  |
  | DATAKEY   | 單筆資料KEY | nvarchar  | 120  |      | 如多KEY用 `||` 串起來                            |
  | USERID    |  使用者ID   | nvarchar  | 200  |      |                                                  |
  | USER_NAME | 使用者名稱  | nvarchar  | 512  |      |                                                  |
  | LOG       |   LOG訊息   | nvarchar  | 255  |      |                                                  |
  | CDT       |  建立時間   | datetime2 |      |      | DEFAULT: SYSDATETIME yyyy-mm-dd hh:mm:ss.sssssss |

  ```SQL
  CREATE TABLE [SYS_MODI_LOG](
    ID bigint IDENTITY NOT NULL PRIMARY KEY,
    CATEGORY nvarchar(30),
    DATAKEY nvarchar(120),
    USERID nvarchar(200),
    USER_NAME nvarchar(512),
    LOG nvarchar(255),
    CDT datetime2 NOT NULL DEFAULT SYSDATETIME()
  )
  ```

* 資料表名稱: SYS_ROLE_V3
* 中文名稱:  新系統角色設定檔
  | 欄位名稱  |   中文   | 形態      | 大小    | 主鍵 | 說明 |
  |-----------|:--------:|-----------|---------|------|------|
  | ROLE_ID   |  角色id  | numeric   | (10, 0) | PK   |      |
  | ROLE_NAME | nvarchar | 64        |         |      |      |
  | CDT       | 建立時間 | datetime2 |         |      |      |
  | CREATE_BY |  建立者  | nvarchar  | 200     |      |      |
  | UDT       | 更新時間 | datetime2 |         |      |      |
  | UPDATE_BY |  更新者  | nvarchar  | 200     |      |      |

  ```SQL
  CREATE TABLE [SYS_ROLE_V3](
    ROLE_ID   numeric(10, 0) PRIMARY KEY,
    ROLE_NAME nvarchar(64) NOT NULL,
    CDT       datetime2,
    CREATE_BY nvarchar(200),
    UDT       datetime2,
    UPDATE_BY nvarchar(200)
  )
  ```

* 資料表名稱: AOPLOG
* 中文名稱:  AOP LOG 檔
  | 欄位名稱  |      中文      | 形態      | 大小 | 主鍵 | 說明                                                                      |
  |-----------|:------------:|-----------|------|------|---------------------------------------------------------------------------|
  | ID        |   自動流水編   | BIGINT    |      | PK   | IDENTITY 自動+1                                                           |
  | URL       |  請求API URL   | nvarchar  | 2000   |      | 如果為 GET 請求，請務必連 URL 後面參數一起記錄，例如  /aa/bb/cc?A=111&b=222 |
  | REQUEST   | 請求 Json 內容 | nvarchar  | max  |      | 無請求內容直接給 null                                                     |
  | RESPONSE  | 回覆 Json 內容 | nvarchar  | max  |      |                                                                           |
  | CDT       |    建立時間    | datetime2 |      |      |                                                                           |
  | CREATE_BY |     建立者     | nvarchar  | 200  |      |                                                                           |
  | UDT       |    更新時間    | datetime2 |      |      |                                                                           |
  | UPDATE_BY |     更新者     | nvarchar  | 200  |      |                                                                           |


  ```SQL
  CREATE TABLE [AOPLOG](
    ID bigint IDENTITY NOT NULL PRIMARY KEY,
    URL nvarchar(2000) NOT NULL,
    REQUEST nvarchar(max),
    RESPONSE nvarchar(max),
    CDT datetime2,
    CREATE_BY nvarchar(200),
    UDT datetime2,
    UPDATE_BY nvarchar(200)
  )
  ```

* 資料表名稱: SYS_USER_ROLE_V3
* 中文名稱:  新使用者角色設定檔
  | 欄位名稱  |   中文   | 形態      | 大小    | 主鍵 | 空白 | 說明 |
  |-----------|:------:|-----------|---------|------|------|------|
  | USER_ID   | 使用者ID | nvarchar  | 200     | PK   |      |      |
  | ROLE_ID   |  角色id  | numeric   | (10, 0) |      |      |      |
  | CDT       | 建立時間 | datetime2 |         |      |      |      |
  | CREATE_BY |  建立者  | nvarchar  | 200     |      |      |      |
  | UDT       | 更新時間 | datetime2 |         |      |      |      |
  | UPDATE_BY |  更新者  | nvarchar  | 200     |      |      |      |

```SQL
  CREATE TABLE [SYS_USER_ROLE_V3](
    USER_ID   nvarchar(200),
    ROLE_ID   numeric(10,0),
    CDT       datetime2,
    CREATE_BY nvarchar(200),
    UDT       datetime2,
    UPDATE_BY nvarchar(200),
    PRIMARY KEY(USER_ID,ROLE_ID)
  )
```

  # B.色號維護

* 資料表名稱: PAT_UPLOAD
* 中文名稱:  色號上傳紀錄檔
* 備註，因為色號+FNAME 太大，所以用 ID 當PK
  | 欄位名稱   |    中文    | 形態      | 大小 | 主鍵 | 說明             |
  |------------|:--------:|-----------|------|------|------------------|
  | ID         | 自動流水編 | BIGINT    |      | PK   | IDENTITY 自動+1  |
  | PATTERN_NO |    色號    | nvarchar  | 30   |      |                  |
  | FNAME      |  前端檔名  | nvarchar  | 60   |      | ex. A12345-H.JPG |
  | CDT        |  建立時間  | datetime2 |      |      |                  |
  | CREATE_BY  |   建立者   | nvarchar  | 200  |      |                  |
  | UDT        |  更新時間  | datetime2 |      |      |                  |
  | UPDATE_BY  |   更新者   | nvarchar  | 200  |      |                  |

  ```SQL
  CREATE TABLE [PAT_UPLOAD](
    ID bigint IDENTITY NOT NULL PRIMARY KEY,
    PATTERN_NO nvarchar(30),  
    FNAME nvarchar(60),  
    CDT datetime2,
    CREATE_BY nvarchar(200),
    UDT datetime2,
    UPDATE_BY nvarchar(200)
  )
  ```

* 資料表名稱: OPMS_PARAMS
* 中文名稱:  OPMS 參數清單
* 備註
* 
  | 欄位名稱    |     中文     | 形態      | 大小 | 主鍵 | 說明                             |
  |-------------|:----------:|-----------|------|------|----------------------------------|
  | ID          |  自動流水編  | BIGINT    |      | PK   | IDENTITY 自動+1                  |
  | TYPE        |   模組類別   | nvarchar  | 20   |      | ex: BOM.SOC...etc 方便歸大類尋找 |
  | PARAM_ID    | 參數清單代號 | nvarchar  | 20   |      | ex: COMPANY                      |
  | SORT_ID     |     排序     | int       |      |      |                                  |
  | OPT_DISPLAY |  選項外顯值  | nvarchar  | 1000 |      |                                  |
  | OPT_VALUE   |  選項內存值  | nvarchar  | 100  |      |                                  |
  | CDT         |   建立時間   | datetime2 |      |      |                                  |
  | CREATE_BY   |    建立者    | nvarchar  | 200  |      |                                  |
  | UDT         |   更新時間   | datetime2 |      |      |                                  |
  | UPDATE_BY   |    更新者    | nvarchar  | 200  |      |                                  |

```SQL
  CREATE TABLE [OPMS_PARAMS](
    ID bigint IDENTITY NOT NULL PRIMARY KEY,
    TYPE nvarchar(20),    
    PARAM_ID nvarchar(20),     
    SORT_ID int, 
    OPT_DISPLAY  nvarchar(1000), 
    OPT_VALUE nvarchar(100),    
    CDT       datetime2,
    CREATE_BY nvarchar(200),
    UDT       datetime2,
    UPDATE_BY nvarchar(200)
  )
  ```

* 資料表名稱: MAILLOG
* 中文名稱:  MAIL LOG 檔
  | 欄位名稱  |    中文     | 形態      | 大小 | 主鍵 | 說明                   |
  |-----------|:---------:|-----------|------|------|------------------------|
  | ID        | 自動流水編  | BIGINT    |      | PK   | IDENTITY 自動+1        |
  | URL       | 請求API URL | nvarchar  | 2000  |      | ex: /test/queryorder   |
  | FROM      |   發信者    | nvarchar  | 200   |      |                        |
  | TO        |   收件者    | nvarchar  | 3000 |      |                        |
  | CC        |    副本     | nvarchar  | 3000 |      |                        |
  | BCC       |  密件副本   | nvarchar  | 3000 |      |                        |
  | SUBJECT   |    主旨     | nvarchar  | 500  |      |                        |
  | CONTENT   | email 內容  | nvarchar  | 2000 |      |                        |
  | STATUS    |    狀態     | nvarchar  | 20   |      | "已發送" or "發送失敗" |
  | ERRMSG    |  錯誤訊息   | nvarchar  | 2000 |      |                        |
  | CDT       |  建立時間   | datetime2 | 7    |      |                        |
  | CREATE_BY |   建立者    | nvarchar  | 200  |      |                        |
  | UDT       |  更新時間   | datetime2 | 7    |      |                        |
  | UPDATE_BY |   更新者    | nvarchar  | 200  |      |                        |

```SQL
CREATE TABLE [dbo].[MAILLOG](
  [ID] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,  --流水編(程式不用給)
  [URL] [nvarchar](2000) NULL,                       --請求API URL
	[FROM] [nvarchar](200) NULL,                        --發信者
	[TO][nvarchar](3000) NULL,                          --收件者
	[CC][nvarchar](3000) NULL,                          --副本
	[BCC][nvarchar](3000) NULL,                         --密件副本
	[SUBJECT][nvarchar](500) NULL,                     --主旨
	[CONTENT][nvarchar](2000) NULL,                    --email 內容
	[STATUS] NVARCHAR(20) NULL,                    --"已發送" or "發送失敗"
	[ERRMSG][nvarchar](2000) NULL,                     -- 錯誤訊息
	[CDT] [datetime2](7) NULL,                         --建立時間
	[CREATE_BY] [nvarchar](200) NULL,                  --建立者ID
	[UDT] [datetime2](7) NULL,                         --更新時間
	[UPDATE_BY] [nvarchar](200) NULL                   --更新者ID
)

```
  
## 以下目前暫不新增，請使用原 table




  ```



</font>