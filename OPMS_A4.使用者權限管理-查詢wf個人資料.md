# 使用者權限管理-查詢 wf 個人資料
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

使用者權限管理-查詢 wf 個人資料


## 修改歷程

| 修改時間 | 內容                          | 修改者 |
|----------|-----------------------------|--------|
| 20230408 | 新增規格                      | Nick   |
| 20230907 | 邏輯改為無關鍵字取全部給 NULL | Nick   |

## 來源URL及資料格式

| 項目   | 說明         |
|--------|--------------|
| URL    | /user/get_wf |
| method | post         |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位      | 名稱   | 資料型別 | 必填 | 資料儲存 & 說明    |
|-----------|------|:--------:|:----:|--------------|
| opKeyWord | 關鍵字 |  string  |  O   | 查詢員工姓名或工號 |


#### Request 範例

```json
{
  "opKeyWord":"邱志"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 使用 WFDataSource 連線，使用 SQL-1 語法，如 opKeyWord 為 null 代表不需組 like條件
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
select distinct A.ID ID,A.LOGINID LOGINID,A.USERNAME USERNAME,A.EMAIL EMAIL,C.NAME DEP from  MEM_GENINF A,ROL_GENINF B,DEP_GENINF C  
where  1=1 AND A.MAINROLEID=B.ROLID AND B.DEPID=C.DEPID AND A.RESIGN='false'

--如{opKeyWord}非 null 則加入下一行SQL
AND (A.ID like '%{opKeyWord}%' or A.USERNAME like '%{opKeyWord}%')  

order by  A.USERNAME

```


# Response 欄位
【 content array 】
| 欄位       | 名稱       | 資料型別 | 資料儲存 & 說明 |
|------------|------------|----------|-----------------|
| opLoginId  | 使用者ID   | string   | LOGINID         |
| opId       | 使用者工號 | string   | ID              |
| opUserName | 姓名       | string   | USERNAME        |
| opEmail    | EMAIL      | string   | EMAIL           |
| opDepName  | 部門名稱   | string   | DEP (備用)      |

#### Response 範例

```json
{
  "msgCode": null,
  "result":{
    "content":[
      {
        "opLoginId":"212866",
        "opId":"123456",
        "opUserName":"邱志民",
        "opEmail":"jill_jao@mail.chimei.com.tw",
        "opDepName":"公用股C班"
      },
      { "opLoginId":"212839",
        "opUserName":"邱志斌",
        "opEmail":"jill_jao@mail.chimei.com.tw",
        "opDepName":"BP股C班#"
      },
      {
        "opLoginId":"91163",
        "opUserName":"邱志誠",
        "opEmail":"stanly_chiu@mail.chimei.com.tw",
        "opDepName":"資訊應用系統部"
      },
      {
        "opLoginId":"91799",
        "opUserName":"邱志豪",
        "opEmail":"jill_jao@mail.chimei.com.tw",
        "opDepName":"複材課"
      }
    ]
  }
}
```

