# 染色-複材-理想線表單列印-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容             | 修改者 |
|----------|----------------|--------|
| 20230907 | 新增規格         | 黃東俞 |
| 20231020 | 調整排序方式     | Nick   |
| 20231128 | 增加依照班別排序 | Nick   |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /wo/scheduled_result_list   |
| method | post                        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱        |  資料型別 | 必填 | 資料儲存 & 說明                                                     |
|--------------|-------------|----------|:----:|--------------------------------------------------------------------|
| opPatternNo  | 色號        | string   |  O    |                                                                    |
| opPartNo     | 料號        | string   |  O    |                                                                    |
| opWoNo       | 工單號碼    | string   |  O    |                                                                    |
| opBDate      | 開始日期    | string   |  O    | yyyy/mm/dd                                                         |
| opEDate      | 結束日期    | string   |  O    | yyyy/mm/dd                                                         |
| opPage       | 第幾頁      | integer  |  O    | 預設 1，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推 |
| opPageSize   | 分頁筆數    | integer  |  O    | 預設 10                                                             |

#### Request 範例

```json
{
	"opPatternNo": "CC-GM10J",
	"opPartNo": "LCCGM10J1XBXXXXXXX",
	"opWoNo": "55005481",
	"opBDate": "2019/01/01",
	"opEDate": "2019/12/31",
	"opPage": 1,
	"opPageSize": 10
}
```

# Request 後端流程說明

* 取{登入者工廠別} SYS_USER.ORGANIZATION_ID 欄位
* 參考SQL-1，欄位皆為非必填，有傳入的欄位才需加入SQL篩選
* 只最多取前1000筆資料並進行分頁再回傳資料
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
-- WoQueryDao.getWoScheduledResultList
SELECT H.ORGANIZATION_ID, S.MFG_DATE, S.SHIFT_CODE, S.EQUIP_NAME, H.GRADE_NO MTR_NAME, H.WO_UID, H.WO_NO, H.PRODUNIT_NO, H.WO_SEQ,
	H.LOT_NO, H.CUST_PO_NO, H.WO_DATE, H.INDIR_CUST, H.RAW_QTY, B.PATTERN_NO, B.ITEM_NO, B.BOM_NO, B.BOM_NAME, B.BOM_DESC, B.BOM_UID,
	H.CUST_NAME, P.PRODUNIT_NAME, H.MODIFY_FLAG, H.PRINT_FLAG, H.PRINT_FLAG_D, H.ERP_FLAG, H.ACTIVE_FLAG
FROM WO_H H
INNER JOIN PRODUNIT P ON H.ORGANIZATION_ID = P.ORGANIZATION_ID AND H.PRODUNIT_NO = P.SAP_PRODUNIT
INNER JOIN BOM_MTM B ON H.BOM_UID = B.BOM_UID
LEFT JOIN (
	SELECT W.WO_UID, PS.MFG_DATE, PS.SHIFT_CODE, E.EQUIP_NAME
	FROM WO_H W
	INNER JOIN PS_H PS ON W.WO_UID = PS.WO_UID
	INNER JOIN EQUIP_H E ON PS.EQUIP_ID = E.EQUIP_ID
) S ON H.WO_UID = S.WO_UID
WHERE H.ORGANIZATION_ID = {登入者工廠別}
AND S.MFG_DATE >= {opBDate}
AND S.MFG_DATE <= {opEDate}
AND B.PATTERN_NO like {%opPatternNo%}
AND B.ITEM_NO like {%opPartNo%}
AND H.WO_NO like {%opWoNo%}
ORDER BY S.MFG_DATE DESC, H.WO_SEQ, S.SHIFT_CODE
```

# Response 欄位
| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
| -------- | -------- | -------- | --------------- |
| pageInfo | 分頁資訊 | object   |                 |
| content  | 附帶訊息 | array    |                 |
#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明 |
| ---------- | ------------ | -------- | --------------- |
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |

#### 【content】array
| 欄位           | 名稱         | 資料型別 | 資料儲存 & 說明    |
|----------------|-------------|---------|-------------------------------|
| opMfgDate      | 排程日期     | string  | yyyy/mm/dd                    |
| opShiftCode    | 班別        | string  |                               |
| opEquipName    | 機台別      | string  |                               |
| opBomDesc      | 替代說明     | string  |                               |
| opWoDate       | 使用日期     | string  |                               |
| opWoSeq        | 序號        | string  |                               |
| opPatternNo    | 色號        | string  |                               |
| opItemNo       | 料號        | string  |                               |
| opBomUid       | BOM ID      | string  |                               |
| opBomNo        | 替代BOM     | string  |                               |
| opMtrName      | 原料        | string  |                               |
| opProdunitNo   | 生產單位號碼 | string  |                               |
| opProdunitName | 生產單位     | string  |                               |
| opRawQty       | 投入原料量   | float  |                               |
| opWoUid        | 工單ID      | string  |                               |
| opWoNo         | 工單號碼     | string  |                               |
| opCustName     | 客戶名稱     | string  |                               |
| opIndirCust    | 間接客戶     | string  |                               |
| opCustPoNo     | 客戶PO單號   | string  |                               |
| opActiveFlag   | 是否生效Y/N  | string  |                               |
| opLotNo        | Lot No      | string  |                               |
| opPrintFlag    | 列印碼Y/N    | string  |                               |
| opPrintFlagD   | 列印生產指示書Y/N | string  |                           |


#### Response 範例

```json
{
  "msgCode":null,
  "result":{
  	"pageInfo": {
      "pageNumber": 1,
      "pageSize": 10,
      "pages": 1,
      "total": 3
    },
    "content":[
    	{
    		"opMfgDate":"2019/01/01",
    		"opShiftCode":"C",
    		"opEquipName":"染色少8",
    		"opBomDesc":"SA#少6#少8",
    		"opWoDate":"2018/12/18",
    		"opWoSeq":"2",
    		"opPatternNo":"CC-GM10J",
    		"opItemNo":"LCCGM10J1XBXXXXXXX",
    		"opBomUid":"5000-LCCGM10J1XBXXXXXXX01",
    		"opBomNo":"01",
    		"opMtrName":"PC",
    		"opProdunitNo":"CC-GM10J",
    		"opProdunitName":"樣品課",
    		"opRawQty":"1060.0",
    		"opWoUid":"0A18CF3D-BF78-4920-B43C-C3280EC4DC62",
    		"opWoNo":"55005481",
    		"opCustName":"楓富",
    		"opIndirCust":null,
    		"opCustPoNo":"10712170002",
    		"opActiveFlag":"REL",
    		"opLotNo":"29968",
    		"opPrintFlag":"Y",
    		"opPrintFlagD":"Y",
    	},
    	{
    		"opMfgDate":"2019/01/01",
    		"opShiftCode":"A",
    		"opEquipName":"染色#1",
    		"opBomDesc":"SA#402#403#405#406#221#222#225#6#7",
    		"opWoDate":"2018/12/26",
    		"opWoSeq":"3",
    		"opPatternNo":"A89717B1",
    		"opItemNo":"DPA717CX1XBXA89717",
    		"opBomUid":"726731E8-5741-4367-8213-AD4BC8FCB62C",
    		"opBomNo":"15",
    		"opMtrName":"PA-717C",
    		"opProdunitNo":"CC-GM10J",
    		"opProdunitName":"染色課",
    		"opRawQty":"7750.0",
    		"opWoUid":"2B2C6E5E-2550-4406-9894-292CB33632E1",
    		"opWoNo":"51016151",
    		"opCustName":"伊菱",
    		"opIndirCust":"1007092",
    		"opCustPoNo":"20181225",
    		"opActiveFlag":"TECO",
    		"opLotNo":"30148",
    		"opPrintFlag":"Y",
    		"opPrintFlagD":"Y",
    	},
    	{
    		"opMfgDate":"2019/01/01",
    		"opShiftCode":"A",
    		"opEquipName":"染色#1",
    		"opBomDesc":"SA#402#403#405#406#221#222#225#6#7",
    		"opWoDate":"2018/12/26",
    		"opWoSeq":"3",
    		"opPatternNo":"A89717B1",
    		"opItemNo":"DPA717CX1XBXA89717",
    		"opBomUid":"726731E8-5741-4367-8213-AD4BC8FCB62C",
    		"opBomNo":"15",
    		"opMtrName":"PA-717C",
    		"opProdunitNo":"CC-GM10J",
    		"opProdunitName":"染色課",
    		"opRawQty":"7750.0",
    		"opWoUid":"2B2C6E5E-2550-4406-9894-292CB33632E1",
    		"opWoNo":"51016151",
    		"opCustName":"伊菱",
    		"opIndirCust":"1007092",
    		"opCustPoNo":"20181225",
    		"opActiveFlag":"TECO",
    		"opLotNo":"30148",
    		"opPrintFlag":"Y",
    		"opPrintFlagD":"Y",
    	},

    ]
  }
}
```



