[返回總覽](./OPMS_00.總覽.md)  
#### 共用參數列表對照
| 參數分類 | 參數代號         | 定義                             |
| -------- | ---------------- | -------------------------------- |
| BOM      | ACTIVE_FLAG      | Active Flag 內存外顯值，適用與否 |
| BOM      | FEDCLT           | 奇菱下料設備清單                 |
| BOM      | FEDLS            | 菱翔下料設備清單                 |
| BOM      | MFGDESC          | 生產方式清單                     |
| BOM      | MIXDESC2         | 攪拌方式清單-染色                |
| BOM      | MIXDESC2_M       | 攪拌方式清單-複材                |
| BOM      | NOLIST           | 所有替代結構清單                 |
| BOM      | SELFPICK         | 秤料清單                         |
| CS       | CSTATUS          | 客訴處理狀態/進度                |
| CS       | CSGRADE          | 風險(值)                         |
| CS       | SAMPLE_D_FLAG    | 樣本到場                         |
| CS       | CS_DIAG_FLAG     | 真因分類                         |
| CS       | REFUND_FLAG      | 賠償要求                         |
| CS       | CSITEMTYPE       | 產品別                           |
| CS       | UNIT             | 單位                             |
| CS       | CS_DF            | 不良現象                         |
| CS       | CSRTFLAG         | 要求換貨                         |
| CS       | FIRSTREPORTFLAG  | 需要初報                         |
| CS       | CS_REPORT_DATE   | 終報回覆                         |
| CS       | PKGTYPE          | 包裝/棧板                        |
| CS       | ITEMRULE         | 料號規則                         |
| CS       | CSREFUNDCURRENCY | 賠償金額                         |
| CS       | CLAIMFLAG        | 案件類別                         |
| CS       | SALES_USER_NAME  | 負責業務                         |
| WO       | WOTYPE           | 工單生產方式                     |
| WO       | ACTIVEFLAG       | 工單狀態                         |
| SO       | CHANNEL          | 產銷進度-配銷通路                 |
| SO       | TYPE             | 產銷進度-生產類型                 |
| SO       | PROD_UNIT        | 產銷進度-生產單位                 |
| PS       | WO_PS_STATUS     | 工單排班狀態                     |
| SOC      | STATUS           | SOC-狀態                         |
