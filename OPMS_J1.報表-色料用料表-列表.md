# 報表-色料用料表-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230817 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                      |
|--------|--------------------------|
| URL    | /report/pgm_monthly_list |
| method | post                     |


## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱    |  資料型別 | 必填 | 資料儲存 & 說明  |
|--------------|---------|----------|:----:|-----------------|
| opYear  | 年      | int      |  M   |                 |
| opMonth | 月      | int      |  M   |                 |


#### Request 範例

```json
{
  "opYear": "2019",
  "opMonth": "1"
}
```

# Request 後端流程說明

* 取 {登入者工廠別} SYS_USER.ORGANIZATION_ID 欄位
* 參考SQL-1取得資料，可直接依SQL範例取得結果，或直接撈取資料再用程式計算各月份數量
* 單位若為KG時，需乘以1000轉換單位
* 數量取兩位小數並轉為千分位字串
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
-- ReportQueryDao.getPgmMonthlyReport
SELECT P.WO_PGM_PART_NO,
CONVERT(float,ROUND(SUM(H.RAW_QTY * (P.QUANTITY_PER + ISNULL(P.MODIFY_UNIT_QTY,0))*(CASE P.UNIT WHEN 'KG' THEN 1000 ELSE 1 END))/1000,2)) AS SUM_TOTAL,
CONVERT(float,ROUND(SUM(CASE WHEN CONVERT(VARCHAR(2), WO_DATE, 1)='01' THEN H.RAW_QTY*(P.QUANTITY_PER+ISNULL(P.MODIFY_UNIT_QTY,0))*(CASE P.UNIT WHEN 'KG' THEN 1000 ELSE 1 END) ELSE 0 END)/1000,2)) AS SUM1,
CONVERT(float,ROUND(SUM(CASE WHEN CONVERT(VARCHAR(2), WO_DATE, 1)='02' THEN H.RAW_QTY*(P.QUANTITY_PER+ISNULL(P.MODIFY_UNIT_QTY,0))*(CASE P.UNIT WHEN 'KG' THEN 1000 ELSE 1 END) ELSE 0 END)/1000,2)) AS SUM2,
CONVERT(float,ROUND(SUM(CASE WHEN CONVERT(VARCHAR(2), WO_DATE, 1)='03' THEN H.RAW_QTY*(P.QUANTITY_PER+ISNULL(P.MODIFY_UNIT_QTY,0))*(CASE P.UNIT WHEN 'KG' THEN 1000 ELSE 1 END) ELSE 0 END)/1000,2)) AS SUM3,
CONVERT(float,ROUND(SUM(CASE WHEN CONVERT(VARCHAR(2), WO_DATE, 1)='04' THEN H.RAW_QTY*(P.QUANTITY_PER+ISNULL(P.MODIFY_UNIT_QTY,0))*(CASE P.UNIT WHEN 'KG' THEN 1000 ELSE 1 END) ELSE 0 END)/1000,2)) AS SUM4,
CONVERT(float,ROUND(SUM(CASE WHEN CONVERT(VARCHAR(2), WO_DATE, 1)='05' THEN H.RAW_QTY*(P.QUANTITY_PER+ISNULL(P.MODIFY_UNIT_QTY,0))*(CASE P.UNIT WHEN 'KG' THEN 1000 ELSE 1 END) ELSE 0 END)/1000,2)) AS SUM5,
CONVERT(float,ROUND(SUM(CASE WHEN CONVERT(VARCHAR(2), WO_DATE, 1)='06' THEN H.RAW_QTY*(P.QUANTITY_PER+ISNULL(P.MODIFY_UNIT_QTY,0))*(CASE P.UNIT WHEN 'KG' THEN 1000 ELSE 1 END) ELSE 0 END)/1000,2)) AS SUM6,
CONVERT(float,ROUND(SUM(CASE WHEN CONVERT(VARCHAR(2), WO_DATE, 1)='07' THEN H.RAW_QTY*(P.QUANTITY_PER+ISNULL(P.MODIFY_UNIT_QTY,0))*(CASE P.UNIT WHEN 'KG' THEN 1000 ELSE 1 END) ELSE 0 END)/1000,2)) AS SUM7,
CONVERT(float,ROUND(SUM(CASE WHEN CONVERT(VARCHAR(2), WO_DATE, 1)='08' THEN H.RAW_QTY*(P.QUANTITY_PER+ISNULL(P.MODIFY_UNIT_QTY,0))*(CASE P.UNIT WHEN 'KG' THEN 1000 ELSE 1 END) ELSE 0 END)/1000,2)) AS SUM8,
CONVERT(float,ROUND(SUM(CASE WHEN CONVERT(VARCHAR(2), WO_DATE, 1)='09' THEN H.RAW_QTY*(P.QUANTITY_PER+ISNULL(P.MODIFY_UNIT_QTY,0))*(CASE P.UNIT WHEN 'KG' THEN 1000 ELSE 1 END) ELSE 0 END)/1000,2)) AS SUM9,
CONVERT(float,ROUND(SUM(CASE WHEN CONVERT(VARCHAR(2), WO_DATE, 1)='10' THEN H.RAW_QTY*(P.QUANTITY_PER+ISNULL(P.MODIFY_UNIT_QTY,0))*(CASE P.UNIT WHEN 'KG' THEN 1000 ELSE 1 END) ELSE 0 END)/1000,2)) AS SUM10,
CONVERT(float,ROUND(SUM(CASE WHEN CONVERT(VARCHAR(2), WO_DATE, 1)='11' THEN H.RAW_QTY*(P.QUANTITY_PER+ISNULL(P.MODIFY_UNIT_QTY,0))*(CASE P.UNIT WHEN 'KG' THEN 1000 ELSE 1 END) ELSE 0 END)/1000,2)) AS SUM11,
CONVERT(float,ROUND(SUM(CASE WHEN CONVERT(VARCHAR(2), WO_DATE, 1)='12' THEN H.RAW_QTY*(P.QUANTITY_PER+ISNULL(P.MODIFY_UNIT_QTY,0))*(CASE P.UNIT WHEN 'KG' THEN 1000 ELSE 1 END) ELSE 0 END)/1000,2)) AS SUM12,
CONVERT(float,ROUND(SUM(H.RAW_QTY*(P.QUANTITY_PER+ISNULL(P.MODIFY_UNIT_QTY,0))*(CASE P.UNIT WHEN 'KG' THEN 1000 ELSE 1 END))/({ opMonth }*1000),2)) AS AVERAGE
FROM WO_PGM P
INNER JOIN WO_H H ON P.WO_UID = H.WO_UID
WHERE H.ORGANIZATION_ID = { 登入者工廠別 } AND H.WIP_CLASS_CODE NOT LIKE '%重工%'
AND H.WO_DATE >= '{ opYear }/01/01'

-- 若 opMonth = 12
AND H.WO_DATE <= '{ opYear }/12/31'
-- 若 opMonth != 12
AND H.WO_DATE < '{ opYear }/{ opMonth + 1 }(若為個位數前面要補0)/01'

GROUP BY P.WO_PGM_PART_NO ORDER BY P.WO_PGM_PART_NO

```


# Response 欄位
| 欄位          | 名稱     | 資料型別 | 資料儲存 & 說明         |
|---------------|---------|---------|------------------------|
| opWoPgmPartNo | 色料料號 | string  | WO_PGM.WO_PGM_PART_NO  |
| opSumTotal    | 合計     | string  | SUM_TOTAL              |
| opSum1        | 一月     | string  | SUM1                   |
| opSum2        | 二月     | string  | SUM2                   |
| opSum3        | 三月     | string  | SUM3                   |
| opSum4        | 四月     | string  | SUM4                   |
| opSum5        | 五月     | string  | SUM5                   |
| opSum6        | 六月     | string  | SUM6                   |
| opSum7        | 七月     | string  | SUM7                   |
| opSum8        | 八月     | string  | SUM8                   |
| opSum9        | 九月     | string  | SUM9                   |
| opSum10       | 十月     | string  | SUM10                  |
| opSum11       | 十一月   | string  | SUM11                  |
| opSum12       | 十二月   | string  | SUM12                  |
| opAverage     | 月平均   | string  | AVERAGE                |


#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content": [
      {
        "opWoPgmPartNo": "04-102-DA-60PXXXXX",
        "opSumTotal": "534.6",
        "opSum1": "0",
        "opSum2": "0",
        "opSum3": "0",
        "opSum4": "0",
        "opSum5": "0",
        "opSum6": "0",
        "opSum7": "0",
        "opSum8": "0",
        "opSum9": "0",
        "opSum10": "0",
        "opSum11": "178.6",
        "opSum12": "356",
        "opAverage": "44.55"
      },
      {
        "opWoPgmPartNo": "04-106-CM-210XXXXX",
        "opSumTotal": "55",
        "opSum1": "0",
        "opSum2": "0",
        "opSum3": "55",
        "opSum4": "0",
        "opSum5": "0",
        "opSum6": "0",
        "opSum7": "0",
        "opSum8": "0",
        "opSum9": "0",
        "opSum10": "0",
        "opSum11": "0",
        "opSum12": "0",
        "opAverage": "4.58"
      },
      {
        "opWoPgmPartNo": "04-102-DA-60PXXXXX",
        "opSumTotal": "534.6",
        "opSum1": "1,294.19",
        "opSum2": "2,359.37",
        "opSum3": "2,365.5",
        "opSum4": "4,905.76",
        "opSum5": "3,927.38",
        "opSum6": "1,006.18",
        "opSum7": "3,067.06",
        "opSum8": "4716.5",
        "opSum9": "2,817.53",
        "opSum10": "2,054.34",
        "opSum11": "2,593.1",
        "opSum12": "4,375.24",
        "opAverage": "2,956.84"
      }
    ]
  }
}
```
