# OPMS_E3.產能對照-取得產能對照機台列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得產能對照機台列表

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20231006 | 新增規格 | Ellen   |

## 來源URL及資料格式

| 項目   | 說明                       |
| ------ | -------------------------- |
| URL    | /iepc/get_ie_equipment_list |
| method | post                       |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在
| 欄位             | 名稱         | 資料型別 | 必填  | 來源資料 & 說明                                       |
| ---------------- | ------------ | -------- | :---: | ----------------------------------------------------- |
| opOrganizationId | 工廠別       | string   |   O   | 預設 `null` ， null:不指定 / 5000:奇菱 / 6000:菱翔    |
| opProdunitNo     | 生產單位代碼 | string   |   O   | 預設 `null`， null:不指定 ..etc                       |

#### Request 範例

```json
{
  "opOrganizationId": "5000" ,
  "opProdunitNo" : null
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])

* 前端條件如為 null 表全部，對應的指定條件不組，否則按照前端條件指定。
* 參考 SQL-1 查回結果 result
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
SELECT
  EQUIP_ID,
  CASE CQ_FLAG 
    WHEN 'C' THEN EQUIP_NAME + '(體積)' 
    ELSE EQUIP_NAME + '(計量)' 
    END AS EQUIP_NAME,
  CQ_FLAG,
  NP 
FROM
  EQUIP_H
WHERE 1=1
--指定條件
AND ORGANIZATION_ID={opOrganizationId} --工廠別
AND PRODUNIT_NO={opProdunitNo}    --生產單位代碼
ORDER BY EQUIP_ID
```


# Response 欄位
#### 【content】array
| 欄位        | 名稱       | 資料型別 | 來源資料 & 說明 |
| ----------- | ---------- | -------- | --------------- |
| opEquipId   | 機台代號   | string   | EQUIP_ID        |
| opEquipName | 機台名稱   | string   | EQUIP_NAME      |
| opCqFlag    | 體積或計量 | string   | CQ_FLAG         |
| opNp        | 機台人數   | string   | NP              |

#### Response 範例

```json
{
    "msgCode": null,
    "result": {
       "content": [
            {"opEquipId":"601","opEquipName":"憲成#1(體積)","opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"46","opEquipName":"複材#6(計量)","opCqFlag":"Q","opNp":"1.0"}
      ]
    }
}
```

