# 已訂未交表-匯出excel
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230828 | 新增規格 | Ellen  |

## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    | /iepc/list_excel |
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。  
| 欄位              | 名稱 | 資料型別 | 必填  | 來源資料 & 說明 |
| ----------------- | ---- | -------- | :---: | --------------- |
| 原列表REQUEST參數 | ...  | ...      |  ...  | ...             |


# Request 後端流程說明

* 參考[OPMS_E3.產能對照-取得產能對照列表](./OPMS_E3.產能對照-取得產能對照列表.md) 以同樣方式取得資料，不需分頁。
* 將資料轉為excel表格匯出，不需設定檔名。
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Excel 欄位

| Eexcel欄位名稱      | 資料型別 | 資料來源 | 資料欄位和說明 |
| ------------------- | :------: | -------- | -------------- |
| 生效                |  string  | SQL-1    | ACTIVE_FLAG    |
| 量測日期            |  string  | SQL-1    | MS_DATE        |
| 機台                |  string  | SQL-1    | EQUIP_NAME     |
| 料號                |  string  | SQL-1    | ITEM_NO        |
| 轉速                |  string  | SQL-1    | RPM            |
| 電流值              |  string  | SQL-1    | CURR           |
| BDP                 |  string  | SQL-1    | BDP            |
| 每分鐘產出(KG)      |  string  | SQL-1    | MFG_QTY        |
| 換網時間(Min/Shift) |  string  | SQL-1    | NET_TIME       |
| 標準產能(KG/Shift)  |  string  | SQL-1    | SHIFT_STD_QTY  |
| 單位產量            |  string  | SQL-1    | HOUR_STD_QTY   |
| 每人單位產量        |  string  | SQL-1    | PEOPLE_STD_QTY |