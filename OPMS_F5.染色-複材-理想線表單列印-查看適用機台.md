# 染色-複材-理想線表單列印-查看適用機台
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容                 | 修改者 |
| -------- | -------------------- | ------ |
| 20230911 | 新增規格             | 黃東俞 |
| 20231011 | 回傳增加opBomFedUid  | Ellen  |
| 20231129 | 補回傳是否限雙軸生產 | Ellen  |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /bom/bom_equip              |
| method | post                        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱        |  資料型別 | 必填 | 資料儲存 & 說明                                                     |
|--------------|-------------|----------|:----:|--------------------------------------------------------------------|
| opBomUid     | BOM ID      | string   |  M   |                                                                    |


#### Request 範例

```json
{
  "opBomUid": "5000-LCCGM10J1XBXXXXXXX01"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 先查詢SQL-1 ~ SQL-4，分別對應Response欄位 opBomMtmTo, opEquipList, opMixList, opFedList
* 若SQL-4有查詢到資料時，再取每筆資料的BOM_FED.BOM_FED_UID，分別帶入SQL-5 進行查詢，對應Response的 opFedList.opData 欄位
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容


# Request 後端邏輯說明

SQL-1:
```sql
-- BomQueryDao.getBomMtmSingle
SELECT B.BOM_UID, B.ORGANIZATION_ID, B.ACTIVE_FLAG, I.PATTERN_NO, B.ITEM_NO,
  I.ITEM_DESC, I.UNIT, B.BOM_NO, B.BOM_NAME, B.BOM_DESC, B.CUST_NAME, I.CMDI_NO,
  I.CMDI_NAME, P.COLOR, B.[USE], B.MULTIPLE, B.BASE_ITEM_NO, B.BASE_BOM_NO, B.REMARK,
  B.ERP_FLAG, B.ASSEMBLY_TYPE, B.BOM_FLAG, B.CDT, B.CREATE_BY, B.UDT, B.UPDATE_BY,
  I.WEIGHT, B.STD_N_QUANTITY_PER, B.CUS_N_QUANTITY_PER, B.BIAXIS_ONLY, B.DYE_ONLY
FROM BOM_MTM B
INNER JOIN ITEM_H I ON B.ITEM_NO = I.ITEM_NO
INNER JOIN PAT_H P ON I.PATTERN_NO = P.PATTERN_NO
WHERE B.BOM_UID = {opBomUid}
```

SQL-2
```sql
-- BomQueryDao.getBomEquipList
SELECT A.BOM_UID, A.EQUIP_ID, A.ACTIVE_FLAG, A.REMARK, A.UDT, A.UPDATE_BY, E.EQUIP_NAME, E.PRODUNIT_NO, P.PRODUNIT_NAME
FROM BOM_EQUIP A
INNER JOIN EQUIP_H E ON A.EQUIP_ID = E.EQUIP_ID
INNER JOIN PRODUNIT P ON E.ORGANIZATION_ID = P.ORGANIZATION_ID AND E.PRODUNIT_NO = P.PRODUNIT_NO
WHERE A.BOM_UID = {opBomUid}
ORDER BY EQUIP_ID
```

SQL-3
```sql
-- BomQueryDao.getBomMixList
SELECT A.BOM_UID, A.BOM_MIX_UID, A.EQUIP_ID, A.MIX_DESC, A.MIX_TIME, A.MFG_DESC, A.UDT,
  A.UPDATE_BY, E.EQUIP_NAME, E.PRODUNIT_NO, P.PRODUNIT_NAME
FROM BOM_MIX A
LEFT JOIN EQUIP_H E ON A.EQUIP_ID = E.EQUIP_ID
LEFT JOIN PRODUNIT P ON E.PRODUNIT_NO = P.PRODUNIT_NO
WHERE A.BOM_UID = {opBomUid} ORDER BY EQUIP_ID
```

SQL-4
```sql
-- BomQueryDao.getBomFedList
SELECT A.BOM_UID, A.BOM_FED_UID, A.EQUIP_ID, A.F_1_RATE, A.F_2_RATE, A.F_3_RATE, A.F_4_RATE, A.F_5_RATE, A.F_6_RATE,
A.BDP_RATE, A.PPG_RATE, A.DBE_RATE, A.UDT, A.UPDATE_BY,
E.EQUIP_NAME, E.PRODUNIT_NO, P.PRODUNIT_NAME
FROM BOM_FED A
LEFT JOIN EQUIP_H E ON A.EQUIP_ID = E.EQUIP_ID
LEFT JOIN PRODUNIT P ON E.ORGANIZATION_ID = P.ORGANIZATION_ID AND E.PRODUNIT_NO = P.PRODUNIT_NO
WHERE A.BOM_UID = {opBomUid}
ORDER BY EQUIP_ID
```

SQL-5
```sql
-- BomQueryDao.getBomFedDList
SELECT BOM_UID, BOM_FED_UID, BOM_PART_NO, FEEDER_NO, FEEDER_RATE, UDT, UPDATE_BY
FROM BOM_FED_D
WHERE BOM_UID = {opBomUid}
AND BOM_FED_UID = {BOM_FED.BOM_FED_UID(SQL-4)}
ORDER BY BOM_PART_NO
```

# Response 欄位

| 欄位           | 名稱            | 資料型別 | 資料儲存 & 說明    |
|----------------|--------------------|---------|-------------------------------|
| opBomMtmTo     | 配色配方主檔資料    | object  |  SQL-1                        |
| opEquipList    | 適用機台           | array   |  SQL-2                        |
| opMixList      | 攪拌方式           | array   |  SQL-3                        |
| opFedList      | 投料設備與比例資料  | array   |  SQL-4                        |

#### 【opBomMtmTo】child node
| 欄位         | 名稱           | 資料型別 | 來源資料 & 說明                 |
| ------------ | -------------- | -------- | ------------------------------- |
| opItemNo     | 料號           | string   |                                 |
| opPatternNo  | 色號           | string   |                                 |
| opColor      | 顏色           | string   |                                 |
| opItemDesc   | 料號摘要       | string   |                                 |
| opCustName   | 客戶名稱       | string   |                                 |
| opUnit       | 單位           | string   |                                 |
| opWeight     | 重量           | float    |                                 |
| opBomNo      | 替代結構       | string   |                                 |
| opBomName    | 替代結構名稱   | string   |                                 |
| opBomDesc    | 替代說明       | string   |                                 |
| opRemark     | 備註           | string   |                                 |
| opBiaxisOnly | 是否限雙軸生產 | string   | Y/N  Y: 是 / N: 否，`null`轉為N |
| opDyeOnly    | 是否只染不押   | string   | Y/N  Y: 是 / N: 否，`null`轉為N |


#### 【opEquipList】child node
| 欄位             | 名稱             | 資料型別 | 來源資料 & 說明 |
| ---------------- | --------------- | -------- | --------------- |
| opActiveFlag     | 適用性           | string   | Y:適用, N:不適用 |
| opProdunitName   | 生產單位         | string   |                 |
| opEquipName      | 機台名稱         | string   |                 |
| opRemark         | 備註             | string   |                 |

#### 【opMixList】child node
| 欄位             | 名稱             | 資料型別 | 來源資料 & 說明 |
| ---------------- | --------------- | -------- | --------------- |
| opProdunitName   | 生產單位名稱     | string   | null:不指定      |
| opEquipName      | 機台名稱         | string   | null:不指定      |
| opMixDesc        | 攪拌方式         | string   |                 |
| opMixTime        | 時間             | string   |                 |
| opMfgDesc        | 生產方式         | string   |                 |

#### 【opFedList】child node
| 欄位             | 名稱             | 資料型別 | 來源資料 & 說明 |
| ---------------- | --------------- | -------- | --------------- |
| opProdunitName   | 生產單位名稱     | string   | null:不指定      |
| opEquipName      | 機台名稱         | string   | null:不指定      |
| opF1Rate         | F1下料比例       | float    |                 |
| opF2Rate         | F2下料比例       | float    |                 |
| opF3Rate         | F3下料比例       | float    |                 |
| opF4Rate         | F4下料比例       | float    |                 |
| opF5Rate         | F5下料比例       | float    |                 |
| opF6Rate         | F6下料比例       | float    |                 |
| opBdpRate        | BDP下料比例      | float    |                 |
| opPpgRate        | PPG下料比例      | float    |                 |
| opDbeRate        | DBE下料比例      | float    |                 |
| opBomFedUid      | 投料設備與比例ID | string   |                 |
| opData           |                 | array    | SQL-5           |

#### 【opData】child node
| 欄位         | 名稱             | 資料型別 | 來源資料 & 說明 |
| ------------ | ---------------- | -------- | --------------- |
| opBomPartNo  | 料號             | string   |                 |
| opFeederNo   | 下料設備         | string   |                 |
| opFeederRate | 百分比           | float    |                 |

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content": {
      "opBomMtmTo":{
        "opItemNo":"540XXXXOS1BX",
        "opPatternNo":"PC-540",
        "opColor":"本色",
        "opItemDesc":"",
        "opCustName":"CMC",
        "opUnit":"KG",
        "opWeight":1.0000,
        "opBomNo":"01",
        "opBomName":null,
        "opBomDesc":"SA.外購PC台化",
        "opRemark":"20210218產線提出對調F4和F5",
        "opBiaxisOnly":"Y",
        "opDyeOnly":"N"
      },
      "opEquipList":[
        {
          "opActiveFlag":"Y",
          "opProdunitName":"複材課",
          "opEquipName":"複材#4",
          "opRemark":null
        },
        {
          "opActiveFlag":"Y",
          "opProdunitName":"複材課",
          "opEquipName":"複材#5",
          "opRemark":null
        }
      ],
      "opMixList":[
        {
          "opProdunitName":"複材課",
          "opEquipName":"複材#5",
          "opMixDesc":"F1:粉(高速攪拌機) F3 : IR-2200 F4: PA-709N F5 : IR-1700 BDP注射器",
          "opMixTime":"30",
          "opMfgDesc":"以一段式攪拌"
        }
      ],
      "opFedList":[
        {
          "opProdunitName":"複材課",
          "opEquipName":"複材#5",
          "opF1Rate":3.60,
          "opF2Rate":0.00,
          "opF3Rate":54.66,
          "opF4Rate":13.91,
          "opF5Rate":27.83,
          "opF6Rate":0.00,
          "opBdpRate":14.91,
          "opPpgRate":0.00,
          "opDbeRate":0.00,
          "opBomFedUid":"2A9D309B-A0F8-4765-A87C-54FE467B15D3",
          "opData": [
            {
              "opBomPartNo":"04-102-PA-709NXXXX",
              "opFeederNo":"F4",
              "opFeederRate":13.9137
            },
            {
              "opBomPartNo":"04-107-BS-M51XXXXX",
              "opFeederNo":"F1",
              "opFeederRate":2.9815
            },
            {
              "opBomPartNo":"04-251-A-OHPAXXXXX",
              "opFeederNo":"F1",
              "opFeederRate":0.0199
            },
            {
              "opBomPartNo":"04-251-A-R246XXXXX",
              "opFeederNo":"BDP",
              "opFeederRate":14.9076
            },
            {
              "opBomPartNo":"04-251-A-R3750EXXX",
              "opFeederNo":"F1",
              "opFeederRate":0.5963
            },
            {
              "opBomPartNo":"06-105-CC-IR1700XX",
              "opFeederNo":"F5",
              "opFeederRate":27.8275
            },
            {
              "opBomPartNo":"06-105-CC-IR2200XX",
              "opFeederNo":"F3",
              "opFeederRate":54.6611
            }
          ]
        }
      ]
    }
  }
}
```