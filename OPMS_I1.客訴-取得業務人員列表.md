# 客訴-取得業務人員列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得業務人員列表

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230808 | 新增規格 | Nick   |


## 來源URL及資料格式

| 項目   | 說明           |
|--------|----------------|
| URL    | /cs/sales_list |
| method | get            |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。  

N/A


#### Request 範例

N/A

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 參考 SQL-1
* 組成 JSON 格式
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
--取得所有客訴列表中，業務員ID與姓名(排掉重複)，讓前端可供下拉選取
select 
  distinct U1.USER_ID ,
           U1.USER_NAME AS SALES_USER_NAME
FROM CS_H C
LEFT JOIN SYS_USER U1 
  ON (C.SALES_USER_ID=U1.USER_ID) --關聯欄位
```


# Response 欄位

#### 【content】array
| 欄位        | 名稱       | 資料型別 | 來源資料 & 說明 |
|-------------|----------|----------|-----------------|
| opSalesName | 業務員姓名 | string   | SALES_USER_NAME |
| opSalesID   | 業務員ID   | string   | USER_ID         |


#### Response 範例



```json
{
    "msgCode": null,
    "result": {
       "content": [
        {"opSalesName":"羅代玉","opSalesID":"3000923"},
        {"opSalesName":"李新科","opSalesID":"3001974"},
        {"opSalesName":"王金保","opSalesID":"3002112"},
        {"opSalesName":"潘紅喜","opSalesID":"3002158"}
      ]
    }
}
```
	
	
	
	