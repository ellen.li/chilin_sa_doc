# 報表-配色作業日報表-匯出pdf
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230818 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                           |
|--------|-------------------------------|
| URL    | /report/daily_report_list_pdf |
| method | post                          |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱    |  資料型別 | 必填 | 資料儲存 & 說明 |
|--------------|---------|----------|:----:|-----------------|
| 原列表REQUEST參數 |     |          |      |                 |

# Request 後端流程說明

* 參考[OPMS_J2.報表-配色作業日報表-列表 /report/pgm_monthly_list](./OPMS_J2.報表-配色作業日報表-列表.md)，但SQL-1的排序欄位改為PATTERN_NO(色號)，WO_DATE(日期)，WO_SEQ(序號)
* rawQty(原料投入量)取整數轉千分位格式並加上單位： WO_H.RAW_QTY + WO_H.UNIT
* batchQty(每Batch用量)：取整數轉千分位格式
* 使用jasper產生pdf檔並匯出
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

SQL-1:
```sql
-- ReportQueryDao.getDailyReport
SELECT H.WO_DATE, H.WO_SEQ, H.LOT_NO, I.PATTERN_NO, H.WO_NO, H.CUST_NAME, H.RAW_QTY, H.BATCH_QTY,
    H.BATCH_TIMES, B.CMDI_NO, B.CMDI_NAME, PRODUNIT.PRODUNIT_NAME, H.UNIT,H.REMARK
FROM BOM_MTM B
INNER JOIN WO_H H ON H.BOM_UID = B.BOM_UID
INNER JOIN ITEM_H I ON B.ITEM_NO = I.ITEM_NO
LEFT JOIN PRODUNIT ON H.PRODUNIT_NO = PRODUNIT.SAP_PRODUNIT AND H.ORGANIZATION_ID = PRODUNIT.ORGANIZATION_ID
WHERE H.ORGANIZATION_ID = { 登入者工廠別 }
AND H.WO_DATE >= { opBDate }
AND H.WO_DATE <= { opEDate }
ORDER BY I.PATTERN_NO, H.WO_DATE, H.WO_SEQ
```

JASPER:
**jasper檔案位置**
|          | 型態   | 參數值                            | 說明 |
| -------- | ------ | -------------------------------- | ---- |
| RealPath | string | /jasperReport/DailyReport.jasper |      |

**jasper fillReport參數**
| 參數名稱 | 名稱 | 型態         | 來源資料 & 說明 |
| -------- | ---- | ------------ | --------------- |
| pars     |      | array object |                 |
| jrds     |      | JRDataSource | {tableList}     |

**pars object 參數**
| 參數名稱      | 名稱           | 型態   | 來源資料 & 說明                                                              |
| ------------- | -------------- | ------ | --------------------------------------------------------------------------|
| today         | 列印日期        | string | 當下日期， yyyy/MM/dd hh:mm:ss                                             |
| period        | 資料期間        | string | { opBDate }~{ opEDate }                                                   |

**jrds object 參數**
| 參數名稱      | 名稱         | 資料型別 | 資料儲存 & 說明                        |
|--------------|-------------|---------|----------------------------------------|
| woDate       | 日期        | string  | WO_H.WO_DATE  yyyy/mm/dd                |
| woSeq        | 序號        | string  | WO_H.WO_SEQ                             |
| lotNo        | Lot No      | string  | WO_H.LOT_NO                             |
| patternNo    | 色號        | string  | ITEM_H.PATTERN_NO                       |
| custName     | 客戶        | string  | WO_H.CUST_NAME                          |
| rawQty       | 原料投入量   | string  | WO_H.RAW_QTY 千分位格式整數 + WO_H.UNIT  |
| woNo         | 工單號碼    | string  | WO_H.WO_NO                              |
| batchQty     | 每Batch用量 | string  | WO_H.BATCH_QTY 千分位格式整數            |
| batchTimes   | 次數        | string  | WO_H.BATCH_TIMES                        |
| cmdiNo       | 品稱代號    | string  | BOM_MTM.CMDI_NO                         |
| produnitName | 生產單位    | string  | PRODUNIT.PRODUNIT_NAME                  |
| remark       | 備註        | string  | WO_H.REMARK                             |