# 報表-日期別數量統計表-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230818 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                       |
|--------|---------------------------|
| URL    | /report/daily_summery_list|
| method | post                      |


## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱    |  資料型別 | 必填 | 資料儲存 & 說明  |
|--------------|---------|----------|:----:|-----------------|
| opBDate      | 開始日期 | string   |  O   | yyyy/mm/dd      |
| opEDate      | 結束日期 | string   |  O   | yyyy/mm/dd      |


#### Request 範例

```json
{
  "opBDate": "2019/01/01",
  "opEDate": "2019/01/31"
}
```

# Request 後端流程說明

* 取{ 登入者工廠別 } SYS_USER.ORGANIZATION_ID 欄位
* 參考SQL-1撈取資料
  * opBDate, opEDate 為非必填，有傳入時才輸入條件
  * opSumTotal(領用數量(KG))，取兩位小數轉為千分位字串
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
-- ReportQueryDao.getDailySummeryReport
SELECT H.WO_DATE, SUM(H.RAW_QTY * M.QUANTITY_PER) AS SUM_TOTAL
FROM WO_H H
INNER JOIN WO_MTR M ON H.WO_UID = M.WO_UID
WHERE H.ORGANIZATION_ID = { 登入者工廠別 }
AND H.WO_DATE >= { opBDate }
AND H.WO_DATE <= { opEDate }
GROUP BY H.WO_DATE ORDER BY H.WO_DATE
```


# Response 欄位
| 欄位           | 名稱         | 資料型別 | 資料儲存 & 說明                         |
|----------------|-------------|---------|----------------------------------------|
| opWoDate       | 日期        | string  | WO_H.WO_DATE  yyyy/mm/dd                |
| opSumTotal     | 領用數量(KG) | string  | SUM_TOTAL 取兩位小數轉為千分位字串        |

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content": [
      {
        "opWoDate": "2023/01/01",
        "opSumTotal": "6,922.88"
      },
      {
        "opWoDate": "2023/01/02",
        "opSumTotal": "1,275,265.19"
      },
      {
        "opWoDate": "2023/01/03",
        "opSumTotal": "2,678,893.42"
      }
    ]
  }
}
```