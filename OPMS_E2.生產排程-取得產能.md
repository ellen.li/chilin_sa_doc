# 生產排程-取得產能
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得產能及是否適用此機台


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230815 | 新增規格 | shawn  |

## 來源URL及資料格式

| 項目   | 說明                |
| ------ | ------------------- |
| URL    | /ps/query_iepc_data |
| method | post                |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             | 名稱   | 資料型別 | 必填  | 資料儲存 & 說明 |
| ---------------- | ------ | :------: | :---: | --------------- |
| opEquipId        | 機台   |  string  |   M   |                 |
| opItemNo         | 料號   |  string  |   M   |                 |
| opWoUid          | uid    |  string  |   M   |                 |
#### Request 範例

```json
{
  "opItemNo":"LPA747XX1XTAF92613",  
  "opWoUid":"D65D4B8B-8922-4165-8A3E-489C0B46D7B1",    
  "opEquipId":"1"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 取得產能查詢sql-1,是否適用於該機台查詢sql-2
* sql-1有資料時取第一筆回傳
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
SELECT
	IE.*
FROM
	IE_PC IE
INNER JOIN EQUIP_H E ON
	IE.EQUIP_ID = E.EQUIP_ID
WHERE
	E.ORGANIZATION_ID ={登入者工廠別}
	AND E.PS_EQUIP_ID ={opEquipId}
	AND IE.ITEM_NO ={opItemNo}
ORDER BY
	IE.MS_DATE DESC
```

SQL-2
```sql
SELECT MAX(B.ACTIVE_FLAG) ACTIVE_FLAG 
FROM BOM_EQUIP B 
INNER JOIN WO_H W ON B.BOM_UID=W.BOM_UID 
WHERE W.WO_UID={opWoUid} 
AND B.EQUIP_ID={opEquipId}
```


# Response 欄位
| 欄位      | 名稱 | 資料型別 | 資料儲存 & 說明 |
| --------- | ---- | -------- | --------------- |
| opShiftStdQty   | 標準產量  | string   | SHIFT_STD_QTY(sql-1)   |
| opActiveFlag | 是否適用此機台 | string   | ACTIVE_FLAG(sql-2) Y/N |



#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":{
      "opShiftStdQty":"2800",
      "opActiveFlag":"Y"
    }
  }
}
```

## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考
- PsManageAction.ajaxQueryIepcData