# LOT編碼-新增修改存檔
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

LOT編碼-新增修改存檔

## 修改歷程

| 修改時間 | 內容                | 修改者 |
| -------- | ------------------- | ------ |
| 20230830 | 新增規格            | Ellen  |
| 20231215 | opLotList改為非必填 | Ellen  |


## 來源URL及資料格式

| 項目   | 說明      |
| ------ | --------- |
| URL    | /lot/save |
| method | post      |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。

| 欄位      | 名稱 | 資料型別     | 必填 | 資料儲存 & 說明 |
| --------- | ---- | ------------ | ---- | --------------- |
| opMfgDate | 日期 | string       | M    |                 |
| opEquipId | 機台 | string       | M    |                 |
| opLotList | LOT  | array object | O    | 預設`[]`        |


### 【opLotList】array object
| 欄位         | 名稱     | 資料型別 | 必填 | 資料儲存 & 說明 |
| ------------ | -------- | -------- | ---- | --------------- |
| opWoUid      | 工單ID   | string   | M    |                 |
| opWoNo       | 工單號碼 | string   | M    |                 |
| opLotNo      | 標籤LOT  | string   | M    |                 |
| opLotNoMtr   | 紙袋LOT  | string   | O    |                 |
| opUseMtrFlag |          | string   | M    | Y/N             |


#### Request 範例

```json
{
  "opMfgDate": "2022/12/13",
  "opEquipId": "45",
  "opLotList": [
    {
      "opWoUid": "D39837FC-904E-4869-835A-71E8522A20B0",
      "opWoNo": "51029904",
      "opLotNo": "22T03C131",
      "opLotNoMtr": "22Q14B271",
      "opUseMtrFlag": "Y"
    }
  ]
}
```

# Request 後端流程說明

可參考舊版 PsManageAction.saveLotData
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* 檢查編碼格式：
  * 2021年1月1日起使用新編碼規則：長度為9碼
  * LOT舊規則：長度為8碼
  * 紙袋LOT{opLotNoMtr} 有可能新舊混合使用，無法依日期判斷，改判斷前兩碼為數字為新編碼，不為數字為舊編碼
  * 格式不符，{opErrLot}為格式不符的LOT清單
* 檢查LOT不可重複： 參數{opLotNo}間無重複，執行SQL-1檢查， 若重複，{opErrDupLot}為重複的LOT清單
* 執行SAP-1，檢查LOT是否存在QM：
  * 遍歷{opLotList}，若{opUseMtrFlag}等於Y且{opLotNoMtr}有值 代入檢查參數
  * 若回傳值ZRETURN等於`E` 表示LOT不存在 ，{opErrSapLot}為不存在Sap的LOT清單
* 若上述檢查有誤回傳
* 執行SQL-2，刪除舊有LOT
* 遍歷opLotList，{seq}為每次遍歷index，從0遞增
  * 執行SQL-3 新增LOT資料
* 有系統錯誤 return 500,SYSTEM_ERROR,正常回傳 200，返回 Response內容


編碼規則：
* 2021年1月1日起使用新編碼規則：年2碼＋產線3碼＋月數字1~C 1碼＋日2碼＋流水號1碼，共9碼
* 舊規則：年1碼＋產線3碼＋月1碼＋日2碼＋流水號1碼，共8碼
* 紙袋LOT 有可能新舊混合使用，無法依日期判斷，改判斷前兩碼為數字為新編碼，不為數字為舊編碼

# Request 後端邏輯說明

SAP-1：
可參考舊版 PsQueryRfc.chkLotExist
程序如有任何異常，紀錄錯誤LOG並回傳NULL

**獲得BAPI方法**
|              | 型態   | 參數值          | 說明 |
| ------------ | ------ | --------------- | ---- |
| BAPIFunction | string | Z_QM_CHEK_LOTNO |      |

**設定輸入參數**
| 參數名稱 | 參數值 | 型態     | 說明 |
| -------- | ------ | -------- | ---- |
| T_LOT    |        | JCoTable |      |

T_LOT JCoStructure 參數
| 參數名稱 | 參數值       | 型態   | 說明    |
| -------- | ------------ | ------ | ------- |
| LOTNO    | {opLotNoMtr} | string | 紙袋LOT |


| 取得回傳表格名稱 |
| ---------------- |
| T_LOT            |

| SAP欄位名稱 | SAP資料型別 | 欄位值 | 說明                    |
| ----------- | :---------: | ------ | ----------------------- |
| ZRETURN     |   string    |        | 等於`E`時 表示LOT不存在 |
| LOTNO       |   string    |        |                         |


SQL-1: 檢查LOT重複
```sql
SELECT
  LOT_NO
FROM
  PS_L L
  INNER JOIN EQUIP_H E ON L.EQUIP_ID= E.EQUIP_ID 
WHERE
  (L.MFG_DATE<> {opMfgDate} OR L.EQUIP_ID<> {opEquipId})
  AND LOT_NO IN ({所有opLotNo})
```

SQL-2: 刪除舊有LOT
```sql
DELETE FROM PS_L WHERE MFG_DATE = {opMfgDate} AND EQUIP_ID = {opEquipId}
```

SQL-3: 新增LOT資料
```sql
INSERT INTO PS_L (
  WO_UID,
  WO_NO,
  EQUIP_ID,
  MFG_DATE,
  LOT_SEQ,
  LOT_NO,
  LOT_NO_MTR,
  USE_MTR_FLAG,
  CDT,
  CREATE_BY
)
VALUES
(
  {opWoUid},
  {opWoNo},
  {opEquipId},
  {opMfgDate},
  {seq},
  {opLotNo},
  {opLotNoMtr},
  {opUseMtrFlag},
  GETDATE(),
  {登入者使用者ID}
)
```

# Response 欄位
* 3個欄位皆為空時才算儲存成功

| 欄位        | 名稱               | 資料型別     | 資料儲存 & 說明 |
| ----------- | ------------------ | ------------ | --------------- |
| opErrLot    | 格式錯誤的LOT      | array string |                 |
| opErrDupLot | 重複的LOT          | array string |                 |
| opErrSapLot | 不存在SAP的紙袋LOT | array string |                 |


## Response 範例

```json
{
    "msgCode": null,
    "result": {
      "content":{
        "opErrLot": null,
        "opErrDupLot": ["22T03C131"],
        "opErrSapLot": ["22Q14B271"],
      }
    }
}
```
