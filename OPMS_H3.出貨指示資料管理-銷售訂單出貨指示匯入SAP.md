# 出貨指示資料管理-銷售訂單出貨指示匯入SAP
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

銷售訂單出貨指示匯入SAP

## 修改歷程

| 修改時間 | 內容                                 | 修改者 |
|----------|------------------------------------|--------|
| 20230615 | 新增規格                             | Nick   |
| 20230921 | 新增LOG說明                          | Nick   |
| 20231003 | 雅嵐提出【不給指示】的資料不要丟回 SAP | Nick   |


## 來源URL及資料格式

| 項目   | 說明               |
|--------|--------------------|
| URL    | /sis/import_to_sap |
| method | post               |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位   | 名稱     | 資料型別 | 必填 | 資料儲存 & 說明 |
|--------|--------|----------|------|-----------------|
| opSoNo | 銷售單號 | string   | M    |                 |

#### Request 範例

```json
{
   "opSoNo":"20755537"
}
```
# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* 執行 SQL-1 取得該銷售訂單維護出貨指示清單 result
* 設定 {isHeader} 為 false
* 設定 {isLine} 為 false
* 檢查每一筆 result 結果
  * (每筆判斷)如果欄位 SO_SEQ 為 null 則，{isHeader} 為 true，否則 {isLine} 為 true 
* 如果 {isHeader} 為 false 代表訂單Header沒有維護出貨指示，則 [return 400,SO_NO_HEADER]，離開程式。 
* 如果 {isLine} 為 false 代表訂單Line沒有維護出貨指示，則 [return 400,SO_NO_LINE]，離開程式。 
* 呼叫 SAP-1 將銷售訂單資訊寫入SAP ，並由 SAP-1 結果取得 TYPE 與 MSGTXT，如果呼叫失敗或 TYPE 不等於 "S" 代表失敗，則 [return 400,SO_UPDATE_RFC_ERROR]，離開程式。 (請紀錄LOG)
* 如 TYPE 等於 "S" 代表成功 ，呼叫 SQL-2 更新 ERP Flag，如果更新失敗，則 [return 400,SO_UPDATE_ERP_FLAG_ERROR]，離開程式。 (請紀錄LOG)
* 如 SQL-2 更新成功，執行 SQL-LOG 紀錄 LOG，{DATAKEY} = {opSoNo}
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SAP-1:
可參考舊版  rfc.importSisToSD

程序如有任何異常，記錄錯誤 LOG 並 回傳 null

**獲得BAPI方法**
|              | 型態   | 參數值       | 說明 |
|--------------|--------|--------------|------|
| BAPIFunction | string | ZSD_OPMS_002 |      |

**設定輸入參數(getImportParameterList)**
| 參數名稱 | 參數值           | 型態   | 說明 |
|----------|------------------|--------|------|
| I_VBELN  | {opSoNo}         | string |      |
| I_NAME   | {登入者使用者ID} | string |      |


**產生 HEADER / ITEM 清單**

* 【HEADER 清單】格式
  | 參數     | 內容                               |
  |----------|------------------------------------|
  | ROW 指標 | {iHeader}                          |
  | ID       | S.LIST_HEADER_ID                   |
  | CONTENT  | S.OPT_DESC + ISNULL(S.ASS_DESC,'') |

* 【ITEM 清單】格式
  | 參數     |                                    |
  |----------|------------------------------------|
  | ROW 指標 | {iLine}                            |
  | POSNR    | S.SO_SEQ                           |
  | ID       | S.LIST_HEADER_ID                   |
  | CONTENT  | S.OPT_DESC + ISNULL(S.ASS_DESC,'') |


{iHeader} 預設為 0 , {iLine} 預設為 0
* 使用 SQL-1  檢查每一筆結果
  * 如果欄位 SO_SEQ 為 null 則參考格式，寫入【HEADER 清單】，之後將 {iHeader}++
  * 如果欄位 SO_SEQ 不為 null 則參考格式，寫入【ITEM 清單】，之後將 {iLine}++

**設定表格參數(getTableParameterList)**
| TABLE名稱 | 參數值             | 型態  | 說明 |
|-----------|-----------------|-------|------|
| T_ITEM    | 匯入 【ITEM 清單】   | Table |      |
| T_HEADER  | 匯入 【HEADER 清單】 | Table |      |


**取得匯出參數(getExportParameterList)**

| 取得回傳 Structure |
|--------------------|
| E_REMSG            |

取得 E_REMSG Structure 內容
| SAP欄位名稱 | SAP資料型別 | 說明 |
|-------------|:-----------:|------|
| TYPE        |   string    |      |
| MSGTXT      |   string    |      |

SQL-1:
```sql
SELECT S.SO_NO,
    S.SO_SEQ,
    S.LIST_HEADER_ID,
    S.OPT_DESC,
    S.ASS_DESC
FROM SO_SIS S
    INNER JOIN SYS_SIS_H H ON S.LIST_HEADER_ID = H.LIST_HEADER_ID
    INNER JOIN SYS_SIS_L L ON S.LIST_HEADER_ID = L.LIST_HEADER_ID
    AND S.LIST_LINE_ID = L.LIST_LINE_ID
WHERE S.OPT_DESC IS NOT NULL
    AND S.OPT_DESC != '不給指示' 
    AND S.SO_NO = {opSoNo}
ORDER BY H.CATEGORY_FLAG,
    H.LIST_SEQ
```

SQL-2:
```sql
UPDATE SO_H
SET ERP_FLAG = 'Y',
    UDT = GETDATE(),
    UPDATE_BY = { 登入者使用者ID }
WHERE SO_NO = { opSoNo }
```

SQL-LOG:
```sql
INSERT INTO  SYS_MODI_LOG (CATEGORY,DATAKEY,USERID,USER_NAME,LOG) VALUES ('SISSOMANAGE',{DATAKEY},{登入者使用者ID},{登入者使用者名稱}, '拋轉')
```


# Response 欄位
| 欄位   | 名稱     | 資料型別 | 資料儲存 & 說明 |
|--------|--------|----------|-----------------|
| opSoNo | 銷售單號 | string   |                 |


## Response 範例

```json
{
    "msgCode": null,
    "result": {
       "content":{
          "opSoNo":"20755537"
      }
    }
}
```
## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考

* SisSoManageAction.sisImportToSap



