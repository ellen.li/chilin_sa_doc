# 出貨指示設定-取得出貨指示資料設定列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得出貨指示設定列表

## 修改歷程

| 修改時間   | 內容                                                                                               | 修改者 |
|------------|--------------------------------------------------------------------------------------------------|--------|
| 20230606   | 新增規格                                                                                           | Sam    |
| 20230628   | 調整 opPrioritySeq 為 integer                                                                      | Sam    |
| 20230703   | 新增查詢條件 opCustName 客戶簡稱                                                                   | Sam    |
| 20230703   | 新增 opConfigUid 欄位，調整排序方式                                                                 | Sam    |
| 20230715   | 調整 request body 內容，增加 opSortFieldList ，增加 排序說明                                         | Sam    |
| 20230717   | 調整 request body 內容，增加 opIsNew，opConfigUid，增加說明                                           | Sam    |
| 20230725   | 調整排序方式與排序說明                                                                             | Sam    |
| 20230726   | 移除 request opIsNew，opConfigUid                                                                   | Sam    |
| 20230802   | 移除 opSortFieldList，opSortField，opSortType，request 改為資料庫實際欄位名稱 opOrderField 與 opOrder | Sam    |
| 20230807   | 調整 res.料號規則 為動態邏輯、SQL-1 資料改成固定 TOP 1000 、修正 SQL-1 錯誤                          | Nick   |
| 20230807-1 | 調整搜尋 模糊搜尋: 客戶簡稱、料號規則值、星星數SQL邏輯調整                                           | Nick   |
| 20230928   | 因客戶名稱有可能是 null 導致前端無法判斷是否走【客戶+料號】，故多回傳客戶代號                         | Nick   |

## 來源URL及資料格式

| 項目    | 說明                     |
| ------ | ----------------------- |
| URL    | /sis/config_manage_list |
| method | post                    |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。  
| 欄位                | 名稱            | 資料型別      | 必填(非必填節點不需存在) | 來源資料 & 說明          |  
|  ----------------- |  -------------- | ----------- |:--------------------: | :--------------------: |
|  opOrganizationId  |   工廠別         |  string     | O                     | 預設 `null`            | 
|  opCustNo          |   客戶代號       |   string    | O                     | 預設 `null`             |
|  opCustName        |   客戶簡稱       |   string    | O                     | 預設 `null`             |  
|  opItemNo          |   料號          |   string    | O                     | 預設 `null`              |  
|  opPkgType         |   包裝/棧板      |   string    | O                     | 預設 `null`              |  
|  opItemRule        |   料號規則       |   string    | O                     | 預設 `null`              |  
|  opItemRuleValue   |   料號規則值      |  string    | O                      | 預設 `null`             |
| opOrderField       | 排序欄位名稱      | string     | O                      | "PRIORITY_SEQ":表示優先順序 / "UDT":表示更新時間    |
| opOrder            | 排序方式         | string      |O                       |   "ASC":表示升冪排序 / "DESC":表示降冪排序          |  
|  opPage            |   第幾頁         |  integer   | O                      | 預設`1`，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推 |  
|  opPageSize        |   分頁筆數       | integer    | O                        | 預設 `10`             |  


#### Request 範例

```json
{
   "opOrganizationId":"5000",
   "opCustNo":null,
   "opCustName":null,
   "opItemNo":null,
   "opPkgType":null,
   "opItemRule":null,
   "opItemRuleValue":null,
   "opOrderField":"PRIORITY_SEQ",
   "opOrder":"DESC",
   "opPage":1,
   "opPageSize":10
  
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 使用前端給的條件，參考 SQL-1 自組【搜尋條件】(使用 AND 開頭，方便與原WHERE 條件串接)，如為 null 代表不需組條件
* {offset} 為起始列印 row ，第一頁為 0，第二頁後為 0 + (opPageSize*(opPage-1))
* 參考 SQL-1 查回結果 result1，如果 result1.ITEM_RERULE= NULL，opItemRule 為 null ，否則依據以下表格轉換為對應中文，取不到則沿用原 ITEM_RULE內容，並將轉換後文字與　ITEM_RULE_VALUE(如為null取空白) 合併，中間以【:】分隔。
  範例: 2-3:510　、 null 、2-3: 、依料號第1碼:561
  | 資料庫 ITEM_RULE | 對應中文              |
  |:----------------:|------------------|
  |      "0-1"       | "依料號第1碼"       |
  |      "1-6"       | "依料號第2~6碼"     |
  |      "0-3"       | "依料號第1~3碼"     |
  |      "1-4"       | "依料號第2~4碼"     |
  |      "7-8"       | "依料號第8碼"       |
  
<!-- * 設定優先順序 prioritySeq
      
      ```
      (1) custNo   不為 null 也不為空字串
      (2) itemNo   不為 null 也不為空字串
      (3) pkgType  不為 null 也不為空字串
      (4) itemRule 不為 null 也不為空字串
      
      1. 若滿足(1)且(2)，則優先順序為 : 3，前端相對應顯示 “★★★”
      2. 若滿足(1)，則優先順序為 : 2，前端相對應顯示  “★★”
      3. 若滿足(2)，則優先順序為 : 1，前端相對應顯示 “★”
      4. 若滿足(3)且(4)，則優先順序為 : 6，前端相對應顯示  “★★★★★★”
      5. 若滿足(3)，則優先順序為 : 5，前端相對應顯示 “★★★★★”
      6. 若滿足(4)，則優先順序為 : 4，前端相對應顯示 “★★★★”
      ``` 

<!-- * 根據 {opOrganizationId} 以及回傳結果的每個 configUid 查詢SQL-2 -->
<!-- * 參考 SQL-2 查回結果 result2 -->
<!-- * 排序方式預設以更新日期降冪排序(日期由新到舊 desc)，再將優先順序 prioritySeq 降冪排序(6->5->4->3->2->1)，其他按照SQL語法排序 -->

<!-- * 若 opSortFieldList 內有 {"[排序欄位]":"[排序方式]"}, 則SQL語法在排序時依照該欄位與該排序方式做排序
  [{"opSortField1":"opSortType1"}]
  放在 json array 中的欄位名稱預設先取得哪個欄位就先將該欄位放在 Order By 的第一位做該欄位排序
  e.g. : 
    [{"PRIORITY_SEQ","DESC"}] => 根據 PRIORITY_SEQ 降冪排序
    [{"UDT":"ASC"}] => 根據 UDT 升冪排序 -->

* 排序方式修改為 :  
    此例子 opOrderField 可為前端傳遞參數 
    e.g: request : {
      ....(其他 json 欄位),
      "opOrderField":"PRIORITY_SEQ", //欲排序的欄位變數
      "opOrder":"DESC"
    }

* 組成 JSON 格式
* Response 結果需含分頁資訊，無資料 pageInfo 裡面項目皆給 0，頁數與分頁筆數前端如有提供，依前端判斷給的資料計算相關數值及拆分頁面
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
SELECT * FROM (
  SELECT * FROM (
    SELECT 
        A.PRIORITY_SEQ, 
        A.CONFIG_UID, 
        A.ORGANIZATION_ID,
        A.CUST_NO,
        A.CUST_NAME,
        A.ITEM_NO,
        A.PKG_TYPE,
        A.ITEM_RULE,
        A.ITEM_RULE_VALUE,
        A.UDT,
        A.UPDATE_BY
    FROM (
        SELECT 
            CASE 
            WHEN ISNULL(S.CUST_NO,'')!= '' AND  ISNULL(ITEM_NO,'')!= '' THEN 3
            WHEN ISNULL(S.CUST_NO,'')!= '' THEN 2
            WHEN ISNULL(ITEM_NO,'')!= '' THEN 1
            WHEN ISNULL(PKG_TYPE,'')!= '' AND ISNULL(ITEM_RULE,'')!= '' THEN 6
            WHEN ISNULL(PKG_TYPE,'')!= '' THEN 5
            WHEN ISNULL(ITEM_RULE,'')!= '' THEN 4
            ELSE 0
            END PRIORITY_SEQ,
            S.CONFIG_UID, 
            S.ORGANIZATION_ID,
            S.CUST_NO,
            C.CUST_NAME,
            S.ITEM_NO,
            S.PKG_TYPE,
            S.ITEM_RULE,
            S.ITEM_RULE_VALUE,
            CONVERT(VARCHAR(10),S.UDT,111) UDT,
            ISNULL(U.USER_NAME,S.UPDATE_BY) UPDATE_BY  
        FROM SIS_CONFIG_H S 
        LEFT JOIN (
            SELECT DISTINCT CUST_NO,CUST_NAME FROM SO_H
            ) C  
            ON S.CUST_NO=C.CUST_NO  
        LEFT JOIN SYS_USER U  
            ON S.UPDATE_BY=U.USER_ID  
        WHERE 
          -- 指定搜尋條件
          S.CHANNEL IS NULL 
        AND S.ORGANIZATION_ID={opOrganizationId}
    
        /*** 以下條件請依照前端給予條件組 SQL ，勿照抄!!!!!!
        *** 以下條件如果為 null，就不加入以下SQL 
        ***/
        --{opCustNo} 如不為 null，加上這句SQL                 
        AND S.CUST_NO = { opCustNo }
        --{opCustName} 如不為 null，加上這句SQL                 
        AND S.CUST_NAME like %{opCustName}%
    
        --{opItemNo} 如不為 null，加上這句SQL                 
        AND S.ITEM_NO = LIKE %{opItemNo}%
        --{opPkgType} 如不為 null，加上這句SQL                 
        AND S.PKG_TYPE = { opPkgType }
        --{opItemRule} 如不為 null，加上這句SQL                 
        AND S.ITEM_RULE = { opItemRule }
        --{opItemRuleValue} 如不為 null，加上這句SQL                 
        AND S.ITEM_RULE_VALUE like %{opItemRuleValue}%
    ) A
  )AS MainT 
  -- 排序依據 
  ORDER BY {opOrderField} {opOrder},CUST_NO, ITEM_NO, PKG_TYPE, ITEM_RULE 
  OFFSET 0 ROWS
  FETCH NEXT 1000 ROWS ONLY
) AS TOP1000T
-- 排序依據(參考TOP1000內的 order by) 
ORDER BY {opOrderField} {opOrder},CUST_NO, ITEM_NO, PKG_TYPE, ITEM_RULE 
OFFSET {offset} ROWS
FETCH NEXT {opPageSize} ROWS ONLY
```

<!-- SQL-2:

```sql
SELECT 
    L.LIST_HEADER_ID,
    L.SP_INS_NAME,
    L.LIST_SEQ,
    S.CONFIG_UID,
    S.OPT_DESC,
    S.ASS_DESC,
    S.LAST_UPDATE_DATE,
    S.LAST_UPDATED_BY
FROM SYS_SIS_H L 
LEFT JOIN (
    SELECT 
        HH.CONFIG_UID,
        LL.LIST_HEADER_ID,
        LL.OPT_DESC,
        LL.ASS_DESC,
        CONVERT(VARCHAR(10),LL.UDT,111) LAST_UPDATE_DATE,
        LL.UPDATE_BY LAST_UPDATED_BY 
    FROM SIS_CONFIG LL 
    INNER JOIN SIS_CONFIG_H HH 
      ON HH.CONFIG_UID=LL.CONFIG_UID 
    WHERE LL.CONFIG_UID = {ConfigUid}
) S
    ON L.LIST_HEADER_ID=S.LIST_HEADER_ID 
WHERE 1=1
--搜尋條件
AND L.ORGANIZATION_ID=? 
AND L.ACTIVE_FLAG='Y' 
AND L.FLAG_CONFIG='Y' 
AND L.MAINTAIN_FLAG='業助' 
ORDER BY L.LIST_SEQ
``` -->

# Response 欄位
| 欄位         | 名稱                 | 資料型別 | 資料儲存 & 說明          |
| ------------ | ------------        | -------- | ------------------------ |
|pageInfo |分頁資訊| object|M||
|content |附帶訊息| array|M| |
#### 【pageInfo】child node
| 欄位| 名稱| 資料型別 | 必填 | 來源資料 & 說明|
| -- | --| -- | - | - |
|pageNumber |目前所在分頁|int|M||
|pageSize|分頁筆數大小|int|M||
|pages|總分頁數|int|M||
|total|總資料筆數|int|M||

#### 【content】array
| 欄位| 名稱| 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明|
| --------------- | -------------------| -------- | :-: | -------------------------- |
|  opConfigUid    |     Config Uid     |  string  |  M  |       CONFIG_UID           |
|  opPrioritySeq  |   優先順序           |  integer  |  M  |     PrioritySeq           |
|  opCustNo     |   客戶代號              |   string |  M  |    CUST_NO               | 
|  opCustName     |   客戶              |   string |  M  |    CUST_NAME               | 
|  opItemNo       |   料號              | string   |  M  |    ITEM_NO                 |
|  opPkgType      |   包裝/棧板         | string    |  M  |    PKG_TYPE               |
|  opItemRule     |   料號規則          | string    |  M  |    參考後端流程說明               |
|  opUDT          |   最後更新日        |  string   |  M  |    UDT ( YYYY/MM/DD )     |
|  opUpdateBy     |   更新人員          |  string   |  M  |    UPDATE_BY              |

#### Response 範例

```json
{
    "msgCode": null,
    "result": {
        "pageInfo": {
        "pageNumber": 1, 
        "pageSize": 10,
        "pages": 1,      
        "total": 3     
        },
       "content": [
       {
         "opConfigUid":"00071313-6DD3-4CB6-B797-6D8CE0B311EC",
         "opPrioritySeq": 3,
         "opCustNo": "1007232",
         "opCustName": "東亞合成",
         "opItemNo": "LLLDPEXMHXBXA87A04",
         "opPkgType": "BX",
         "opItemRule":"1-3",
         "opUDT": "2016/10/12",
         "opUpdateBy":"涂O涵"
       }, 
       {
         "opConfigUid":"00089D4F-9D29-4547-822A-91EB2D2140C2",
         "opPrioritySeq": 2,
         "opCustNo": "1007231",
         "opCustName": "ITCO",
         "opItemNo": "LX0036XMHXSDH19B18",
         "opPkgType": "NA",
         "opItemRule":"NA",
         "opUDT": "2022/06/27",
         "opUpdateBy":"楊O宜"
       },
       {
         "opConfigUid":"000CFB87-A102-43B9-B43E-30FBE6EAC33C",
         "opPrioritySeq": 4,
         "opCustNo": null,
         "opCustName": null,
         "opItemNo": "107X125XX1BXFGXXXX",
         "opPkgType": "NA",
         "opItemRule":"NA",
         "opUDT": "2015/06/23",
         "opUpdateBy":"陳O玲"
       }
        ]
    }
}
```

