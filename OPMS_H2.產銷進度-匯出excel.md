# 產銷進度-匯出excel
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**


## 修改歷程

| 修改時間 | 內容                  | 修改者 |
|----------|---------------------|--------|
| 20230801 | 新增規格              | 黃東俞 |
| 20230810 | 修改流程              | 黃東俞 |
| 20230912 | 新增 訂單號碼 及 項次 | Nick   | 

## 來源URL及資料格式

| 項目   | 說明            |
|--------|----------------|
| URL    | /so/list_excel |
| method | post           |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位            | 名稱            | 資料型別 | 必填 | 資料儲存 & 說明                              |
|-----------------|----------------|:--------:|:----:|--------------------------------------------|
| opShipDateS     | 業務交期開始日期 | string   |   O  | yyyymmdd                                   |
| opShipDateE     | 業務交期結束日期 | string   |   O  | yyyymmdd                                   |
| opSoDateS       | 開單日期開始日期 | string   |   O  | yyyymmdd                                   |
| opSoDateE       | 開單日期結束日期 | string   |   O  | yyyymmdd                                   |
| opCustName      | 客戶編號        | string   |   O  |                                            |
| opCreateUser    | 開單工號        | string   |   O  |                                            |
| opChannel       | 配銷通路        | string   |   O  | 共用參數 opType='SO',opParamId='CHANNEL'    |
| opPatternNo     | 色號            | string   |   O  |                                            |
| opType          | 生產類型        | string   |   O  | 共用參數 opType='SO',opParamId='TYPE'       |
| opProdunitNo    | 生產單位        | string   |   O  | 共用參數 opType='SO',opParamId='PROD_UNIT'  |
| opSalesOrderNo  | 訂單號碼        | string   |   O  |                                            |
| opWoNo          | 工單號碼        | string   |   O  |                                            |


#### Request 範例

```json
{
  "opShipDateS": "20230701",
  "opShipDateE": "20230731",
  "opSoDateS": "20230801",
  "opSoDateE": "20230831",
  "opCustName": null,
  "opCreateUser": null,
  "opChannel": "10",
  "opPatternNo": null,
  "opType": "1",
  "opProdunitNo": "Z510",
  "opSalesOrderNo": null,
  "opWoNo": null
}
```

# Request 後端流程說明

* 參考[OPMS_H2.產銷進度-列表 /so/list](./OPMS_H2.產銷進度-列表.md) 從SAP-1取和SQL-1取得資料。
* 不需整合訂單號碼 opSalesOrderNo 和項次 opSalesOrderSeq 資料，直接製成Excel檔案回傳，不需給檔名。
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

參考[OPMS_H2.產銷進度-列表 /so/list](./OPMS_H2.產銷進度-列表.md)

# Excel 欄位

| Eexcel欄位名稱 | 資料型別 | 資料欄位和說明                                   |
| ------------- | :------:| ------------------------------------------------ |
| 開單日期       | string  | ERDAT(SAP-1)  轉 yyyy/mm/dd 格式                 |
| 訂單號碼       |   string    | VBELN(SAP-1)     過濾前綴0                          |
| 項次           |   string    | POSNR(SAP-1)     過濾前綴0                          |
| 客戶          | string  | SORT2(SAP-1)                           |
| 料號          | string  | MATNR(SAP-1)                           |
| 原料          | string  | MATERIAL(SAP-1)                        |
| 訂單數量       | string  |KWMENG(SAP-1)                           |
| 業務交期       | string  |BSTDK(SAP-1) 轉 yyyy/mm/dd 格式         |
| 製委單號       | string  |AUFNR(SAP-1) (工單號碼)                  |
| 生管交期       | string  |WO_H.DD_MFG(SQL-1) 轉 yyyy/mm/dd 格式  |
| 最後入庫日期   | string  |LTRMI(SAP-1) 轉 yyyy/mm/dd 格式         |
| 工單數量      | string  | GAMNG(SAP-1)                            |
| 入庫量        | string  | ERFMG(SAP-1)                            |
| 生產單位      | string  | PRODDEPT(SAP-1)                          |
| 業務協調備註   | string  |WO_H.DD_REMARK(SQL-1)                   |
| 變更記錄      | string  | CNT(SQL-1)                              |


