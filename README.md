奇菱後端開發規範
===

 Table of Contents

[TOC]

| 修改時間  | 內容                | 修改者 |
|-----------|-------------------|--------|
| 2023/3/27 | 資料庫相關 增加補充 | Nick   |

## 建立專案
### 配置專案

| 項目         | 內容                                       |
|--------------|--------------------------------------------|
| modelVersion | 4.0.0<font color="red"><sup>1</sup></font> |
| groupId      | tw.com.chilin                              |
| artifactId   | opms                                       |
| version      | 0.0.1-SNAPSHOT                             |
| 編碼         | UTF-8                                      |
| JAVA Version | openjdk 1.8                                |
	
> **_備註:_**  
 1:https://maven.apache.org/guides/introduction/introduction-to-the-pom.html#minimal-pom
	
### 配置POM.XML
dependency需進行註解，以方便快速釐清。
範例:

![](./images/2023-02-07-15-50-01.png)

<br><br><br>
### 預計會使用的套件

<table>
<tr><th>Group</th><th>Name</th><th>Version</th></tr>
<tr><td rowspan="5">org.springframework.boot</td><td>spring-boot-starter-web</td><td> </td></tr>
<tr><td>spring-boot-starter-data-jpa   </td><td>          </td></tr>
<tr><td>spring-boot-starter-security   </td><td>          </td></tr>
<tr><td>spring-boot-starter-aop        </td><td>          </td></tr>
<tr><td>spring-boot-starter-validation </td><td>          </td></tr>
<tr><td rowspan="1">com.microsoft.sqlserver</td><td>mssql-jdbc</td><td> </td></tr>
<tr><td rowspan="1">org.projectlombok</td><td>lombok</td><td> </td></tr>
<tr><td rowspan="1">org.apache.commons</td><td>commons-lang3</td><td> </td></tr>
<tr><td>  </td><td>  </td><td>  </td></tr>
</table>

## 開發程式
### 命名程式規則
| 類別           | 規則                      | 例子                 | 備註                                  |
|----------------|--------------------------|----------------------|---------------------------------------|
| Controller     | 功能名稱+Controller       | MemberController     |                                       |
| Repository     | Table名 + Repository      | BankRepository       |                                       |
| RepositoryImpl | Table名稱 +RepositoryImpl | BankRepositoryImpl   | querydsl                              |
| Service        | 結尾為Service             | CommonService        | 業務邏輯(如:寄mail、呼叫奇菱核心API等) |
| Exception      | 結尾為Exception           | ServerErrorException |                                       |
| 其他共用類別：<br/>Filter (過濾器)<br/>Util (工具)<br/>Helper (工具)<br/>Config (常數或相關設定)<br/>Interceptor (攔截器)<br/>Aspect (AOP)<br/>. | 結尾為Filter/Util/Config/Interceptor/Helper/Aspect... | CorsFilter<br>JsonUtilLogAspect
 |  |

### 變數命名規則

||規則|範例|
|-|-|-|
|類別|大駝峰| TestClass |
|介面|大駝峰| TestInterface |
|方法|小駝峰| testMethods |
|變數|小駝峰| testVariable |
|全域變數|小駝峰| testGlobalVariable |
|常數|大寫英文+底線|TEST_CONSTANT |


### PACKAGE

#### src/main/java/tw.com.chilin.opms
```
├── system 
│    ├── config
│    ├── filter
│    └── aspect
├── config
├── controller
├── exception (Exception相關)
├── model
│    ├── entity
│    ├── request
│    └── response
├── repository
├── scheduler (排程相關)
├── service
│    └── impl
└── util (共用工具)
```

※若相同功能會使用到多隻程式，可新增package以集中存放程式。

```text
例如:
   spring security會用到的程式皆放在 src/main/java/tw.com.chilin.opms/system/config/auth
```


#### src/main/resources

application.yml

應用程式設定檔相關：
1. 須依dev/test/uat/prod環境作區別
2. 內容須含系統環境參數，如資料庫之帳號及密碼、運行環境、API主機位置等，且不可用來儲存敏感性資料、個資、加解密 Key 值等重要資料。

## 串接API
### 命名
1.欄位名稱皆是大小寫有別，使用小駝峰式命名法：第一個字小寫，2個字母合併時，第二個字大寫。
2.URLs使用小寫命名，複合式名稱以(_)相隔

### 格式
傳入及回傳資料皆以JSON格式。

|項目|說明|
|-|-|
|Request Header|	Content-Type: application/json; charset=UTF-8|
|Response Header|	Content-Type: application/json; charset=UTF-8|

### 資料型別
根據 OAS 定義的資料型別格式

| 資料型別 | 格式     | 說明                                            |
|----------|----------|-------------------------------------------------|
| integer  | int32    |                                                 |
| integer  | int64    |                                                 |
| numeric  | float    |                                                 |
| number   | double   |                                                 |
| string   | string   |                                                 |
| string   | byte     | base64 encoded characters                       |
| string   | binary   | any sequence of octets                          |
| boolean  | int      | true/false、1/0                                  |
| string   | date     | yyyy/MM/dd                                      |
| string   | datetime | yyyy/MM/dd HH:mm:ss                             |
| object   | value    | 為另一 json 格式                                |
| array    |          | 以中括號[…]前後包覆著 value 值，值與值以逗號分隔 |
| json     |          | 以大括號{}前後包覆著 value 值，key 和 value      |

### 共用欄位說明

#### Response 欄位(<font color=green>請求成功</font>)
| 欄位    | 名稱     | 資料型別 | 說明     |
|---------|--------|----------|----------|
| msgCode | 訊息代碼 | string   |          |
| result  | 回傳資料 | object   | 進階訊息 |


#### 【result】child node
| 欄位     | 名稱     | 資料型別          | 資料儲存 & 說明               |
|----------|--------|-------------------|-------------------------|
| pageInfo | 分頁資訊 | object            | | 依API需求決定是否回傳分頁資訊 |
| content  | 附帶訊息 | null/object/array | | 依API需求決定內容             |

#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 資料儲存 & 說明 |
|------------|------------|----------|-----------------|
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |


成功JSON範例
```json
HTTP/1.1 200 Success.

{
  "msgCode":null,
  "result":{
    "content": null // null , object,array
  }
}
```

有分頁-成功JSON範例
```json
無資料
HTTP/1.1 200 Success.

{
  "msgCode":null,
  "result":{
    "pageInfo": {
      "pageNumber": 0,
      "pageSize": 0,
      "pages": 0,
      "total": 0
    },
    "content": null
  }
}
```

```json
有資料
HTTP/1.1 200 Success.

{
  "msgCode": null,
  "result":{
    "pageInfo": {
      "pageNumber": 1, //第幾頁，0 -> get all
      "pageSize": 20,  //一頁幾筆 -> 如果 pageNumber 0 ,pageSize = total
      "pages": 5,      //total 頁數 -> 如果 pageNumber 0 ,pages always 1
      "total": 100     //total 筆數
    },
    "content":[{...}]
  }
}
```

#### Response 欄位(<font color =red>請求失敗</font>)
| 欄位    | 名稱     | 資料型別 | 說明                                            |
|---------|--------|----------|-----------------------------------------------|
| msgCode | 訊息代碼 | string   | 參考[訊息代碼對照表](./OPMS_REF_訊息代碼對照表) |
| result  | 回傳資料 | object   | 進階訊息                                        |


#### 【result】child node
| 欄位    | 名稱     | 資料型別          | 資料儲存 & 說明 |
|---------|--------|-------------------|-----------|
| content | 附帶訊息 | null/object/array | 依需求決定內容  |

一般錯誤格式
```json
HTTP/1.1 400 Bad Request

{
  "msgCode": "MSSSAGECODE", 
  "result":{
    "content": null  //null or object or array
  }
}
```

欄位驗證錯誤格式
```json
HTTP/1.1 400 Bad Request

{
  "msgCode": "VALIDATION_ERROR", 
  "result":{
    "content": [
      {
        "field":"username",
        "message":"使用者名稱不得為空"
      },{
        "field":"amount",
        "message":"數量欄位不得大於10"
      }
      ]
  }
}
```

HTTP/1.1 401 Unauthorized
```json
{
  "msgCode": "UNAUTHORIZED", //使用者認證失敗
  "result":{
    "content": null 
  }
}
```

HTTP/1.1 403 Forbidden
```json
{
  "msgCode": "FORBIDDEN", //使用者權限不足
  "result":{
    "content": null
  }
}
```

500 系統層級錯誤
```json
{
  "msgCode": "SYSTEM_ERROR", //[訊息代碼對照表](./OPMS_REF_訊息代碼對照表)
  "result":{
    "content": null
  }
}
```
#### Response 狀態碼與說明
回傳狀態碼(Status Code)

| 狀態碼（status） | 參考狀態碼說明                  |
|----------------|---------------------------------|
| 200            | Success.                        |
| 400            | Bad Request.                    |
| 401            | 使用者認證失敗                  |
| 403            | 使用者權限不足                  |
| 500            | 發生系統層級錯誤，請聯繫資訊人員 |

因狀態碼會共用，實際錯誤訊息請參考 msgCode 清單 [OPMS_REF_訊息代碼對照表](./OPMS_REF_訊息代碼對照表.MD)

#### 相關記錄

  呼叫其他內部平台API時應記錄相關資訊，內容至少包含：
-	URL
-	Request Header
-	Request Body
-	開始呼叫時間
-	結束呼叫時間
-	回應秒數

#### 憑證檢核

系統要能走 http 或https，且可能走ip連線，因此應加入忽略憑證檢核避免程式出現異常

### 撰寫RESPONSE & REQUEST

#### 使用方法
所有 Controller 共用一個基本的 reposne 格式
 		
####  欄位檢核規則
Request的欄位，都需要檢核格式正確性，例如日期、數字、email....等，
須依據奇菱規範作檢核。若需要自訂檢核方式，則呼叫共用工具做基本檢
核。若檢核錯誤，Response中該欄位需要包含檢核資訊。

#### 欄位檢核規則

Request的欄位，都需要檢核格式正確性，例如日期、數字、email....等，
須依據奇菱規範作檢核。若需要自訂檢核方式，則呼叫共用工具做基本檢
核。若檢核錯誤，Response中該欄位需要包含檢核資訊。

## 處理EXCEPTION

### EXCEPTION分類

#### 規則
統一拋出自訂的系統錯誤，例如:ServerErrorException。

#### EXCEPTION應包含的內容

-	自訂錯誤代碼(如果有的話)
-	自訂錯誤訊息(如果有的話)
-	其他資訊(依實際需求為主)

※原則上以上述作法為主，可視情況作調整

#### EXCEPTION處理

所有錯誤統一於@RestControllerAdvice的Handler做處理
※原則上以上述作法為主，可視情況作調整

## LOG

| 技術  | 使用情境       |
|-------|------------|
| AOP   | 記錄業務邏輯   |
| SLF4j | 依奇菱規範記錄 |

## SCHEDULE

### 使用技術

### 功能

### 記錄


## 註解

須有足夠的註解來說明程式流程

## SQL相關
- 使用JPA @Query 原生SQL + Param (避免SQL injection)
### Request 後端SQL 邏輯代號開頭連線說明

| KEYWORD | 說明                 | 範例                   |
|---------|--------------------|------------------------|
| SQL     | 需連線至 OPMS 資料庫 | SQL-1、SQL-2 ...etc     |
| WFSQL   | 需連線至 WF 資料庫   | WFSQL-1、WFSQL-2 ...etc |
| ODSSQL  | 需連線至 ODS 資料庫   | ODSSQL-1、ODSSQL-2...etc|
| SAP     | 需連線至 SAP API     | SAP-1、SAP-2 ...etc     |

## 資料庫相關
### MSSQL 與 Oracle 舊資料相容
因為奇菱採新舊程式並行，分段上程式，為了避免舊系統誤判，必需以最大相容方式來存放，故字串、數值，<font color="green"><b>如為空白('') 請強制轉型 NULL </b></font>， INSERT/UPDATE 資料庫前請特別確認。

註:因 Oracle 沒有所謂空白('') ，只有 NULL，所以原先資料庫資料空白皆為 NULL。

### 中文注意事項
有可能存中文字欄位，INSERT必需加入前置詞 N，避免難字出現亂碼或問號
如果為 WHERE 條件該欄位時也被需加入，避免查詢不到

例如
```sql
  INSERT INTO AAA VALUES (N'測試堃');
```
```sql
  SELECT * FROM AAA WHERE col1 like N'%堃%';
```
### 交易(transaction)使用
如為連續行為中間會增刪修資料，務必包入同一transaction中，錯誤必需 rollback；需最佳化流程，如內含 SELECT 語法，需跑較久，可考慮是否提前在 Transaction 外先做好，避免造成 table lock 過久

## API相關
- 每支Controller會聚集相同功能範圍的API
- 每支Controller的API URI命名以功能名為開頭，並加入適當文字區別CRUD操作(決
定寫法後須提出討論來統一用法)
	
>	例如:
>	MemberController -> API URI應為/member/**

- API必須明確限制呼叫來源是否是合法的呼叫端

## GIT

### 分支類別
| 分支名稱 | 對應環境   |
|----------|---------|
| pre-dev  | TPI測試機  |
| develop  | SIT測試機  |
| release  | UAT測試機  |
| master   | PROD正式機 |



### 程式佈署更版流程
待與奇菱討論

## 資安相關
登入編碼資訊，參考目前奇菱做法

## 其他規範

1. 若需儲存檔案，不能存資料庫，須與奇菱IT確認適合存放的實體路徑
2. 若非特殊需求，應避免hardcode
3. 暫存檔的機制，必須設定有自動刪除的功能
4. 考慮擴容，所以相關資料應處存於 MSSQL 資料庫或 Redis 主機












































