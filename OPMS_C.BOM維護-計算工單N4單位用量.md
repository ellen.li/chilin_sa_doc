# BOM維護-計算工單N4單位用量
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

計算工單N4單位用量

## 修改歷程

| 修改時間 | 內容               | 修改者 |
|----------|------------------|--------|
| 20230516 | 新增規格           | Nick   |
| 20230612 | 調整 response 格式 | Nick   |


## 來源URL及資料格式

| 項目   | 說明              |
|--------|-------------------|
| URL    | /bom/calculate_n4 |
| method | post              |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。  
| 欄位      | 名稱     | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明 |
|-----------|--------|----------|:------------------------:|---------------|
| opPgmList | 色粉清單 | array    |            M             | 預設 `null`     |
| opMtrList | 原料清單 | array    |            M             | 預設 `null`     |

#### 【 opPgmList 】array
| 欄位          | 名稱     | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明 |
|---------------|--------|----------|:------------------------:|-----------------|
| opPgmPartNo   | 色粉料號 | string   |            M             |                 |
| opQuantityPer | 單位用量 | string   |            M             |                 |
| opUnit        | 單位     | string   |            M             |                 |


#### 【 opMtrList 】array
| 欄位        | 名稱     | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明 |
|-------------|--------|----------|:------------------------:|-----------------|
| opMtrPartNo | 原料料號 | string   |            M             |                 |

#### Request 範例

sample 1:
```json
{
  "opPgmList":[
    
		
    {"opPgmPartNo":"HD08907G3", "opQuantityPer":"0.02610", "opUnit":"KGW"},
    {"opPgmPartNo":"05-201-C-07XXXXXXX", "opQuantityPer":"0.03090", "opUnit":"G"},
    {"opPgmPartNo":"04-011-R3078745276", "opQuantityPer":"0.04028", "opUnit":"PC"},
    {"opPgmPartNo":"LCS9301C1XXX025004", "opQuantityPer":"0.02969", "opUnit":"RL"}
  ],
  "opMtrList": [
    {"opMtrPartNo":"92-912-004600XXXXX"},
    {"opMtrPartNo":"06-106-JM-80NXXXXX"}
  ]  
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 定義 calculateN4 計算 function，並將 opPgmList 與 opMtrList 當輸入條件計算，最後得到字串值為 JSON 內的{opN4} 值
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

  無SQL，參考 [OPMS_共用_N4計算公式](./OPMS_共用_N4計算公式.md) 寫出 calculateN4 function

  可參考舊有程式 CalculatePgmAction 內的 calculateN4() 撰寫

# Response 欄位
| 欄位 | 名稱     | 資料型別 | 來源資料 & 說明                   |
|------|--------|----------|-----------------------------|
| opN4 | N4計算值 | string   | 回傳小數點五位之數值字串(0.xxxxx) |

#### Response 範例

```json
{
    "msgCode": null,
    "result": {
      "content":{
          "opN4":"9.12345"
        }
    }
}
```

