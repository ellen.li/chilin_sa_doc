# 生產排程-儲存排班
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

儲存排班


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230814 | 新增規格 | shawn  |

## 來源URL及資料格式

| 項目   | 說明              |
| ------ | ----------------- |
| URL    | /ps/save_schedule |
| method | post              |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位            | 名稱         | 資料型別 | 必填  | 資料儲存 & 說明 |
| --------------- | ------------ | :------: | :---: | --------------- |
| opPsUid         | uid          |  string  |   O   |  編輯為必填                |
| opPsSeq         | 序號         |  string  |   M   |                 |
| opEquipId       | 機台         |  string  |   M   |                 |             |
| opMfgDate       | 生產日期     |  string  |   M   |                 |
| opShiftCode     | 班別代碼     |  string  |   M   |                 |
| opWoUid         | uid          |  string  |   O   |                 |           |
| opItemNo        | 料號         |  string  |   O   |                 |
| opStopDesc      | 停車原因     |  string  |   O   |                 |
| opShiftHours    | 時數         |  string  |   M   |                 |
| opWoQty         | 訂單未做     |  string  |   O   |                 |
| opShiftStdQty   | 標準產量     |  string  |   M   |                 |
| opShiftPlnQty   | 預計產量     |  string  |   M   |                 |
| opShiftMfgQty   | 實際產量     |  string  |   O   |                 |
| opRemark        | 排程說明     |  string  |   O   |                 |
| opCtnWeight     | 桶重         |  string  |   O   |                 |

#### Request 範例

```json
[
    {
      "opPsUid":"E727D2E2-9A77-4C82-8AB0-F273737E9D1A",        
      "opPsSeq":"0",        
      "opEquipId":"1",    
      "opMfgDate":"2022/01/02",      
      "opShiftCode":"A",    
      "opWoUid":"36A4D4A0-DECC-4B4F-9890-4622B0E04FC3",          
      "opItemNo": "757XXXXXX1DFH95429",  
      "opStopDesc":"計劃性停車",  
      "opShiftHours": "4",  
      "opWoQty":"1400",      
      "opShiftStdQty":"2800",
      "opShiftPlnQty":"1400", 
      "opShiftMfgQty":"1000",  
      "opRemark":null,
      "opCtnWeight":"1008.4"       
    },
    ...
]

```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* 遍歷資料
  * 如`{opPsUid}為空且{opItemNo}不為空`，進行新增，執行sql-1
  * 反之為修改或刪除
    * 如{opItemNo}為空，進行刪除，執行sql-2
    * 反之為更新，執行sql-3
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
INSERT INTO PS_H(
  PS_UID, PS_SEQ, WO_UID, EQUIP_ID,
  MFG_DATE, SHIFT_CODE, CTN_WEIGHT, WO_QTY,
  SHIFT_STD_QTY, SHIFT_PLN_QTY, SHIFT_HOURS, REMARK,
  STOP_DESC, CDT,CREATE_BY,UDT,UPDATE_BY)
VALUES(
  {產生隨機UUID},{opPsSeq},{opWoUid},{opEquipId},
  {opMfgDate},{opShiftCode},{opCtnWeight},{opWoQty},
  {opShiftStdQty},{opShiftPlnQty},{opShiftHours},{opRemark},
  {opStopDesc},GETDATE(), {登入者使用者ID} ,GETDATE(), {登入者使用者ID} 
)
```

SQL-2:

```sql
DELETE PS_H WHERE PS_UID={opPsUid}
```

SQL-3:
```sql
UPDATE PS_H 
 SET PS_SEQ={opPsSeq},
 MFG_DATE= {opMfgDate},
 SHIFT_CODE={opShiftCode},
 WO_UID={WO_UID}, 
 CTN_WEIGHT={opCtnWeight}, 
 WO_QTY={opWoQty}, 
 SHIFT_STD_QTY={opShiftStdQty}, 
 SHIFT_PLN_QTY={opShiftPlnQty},
 SHIFT_HOURS={opShiftHours}, 
 REMARK={opRemark},
 STOP_DESC={opStopDesc},
 UDT=GETDATE(),
 UPDATE_BY={登入者使用者ID}
 WHERE PS_UID={opPsUid}
```

# Response 欄位

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":null
  }
}
```

## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考
- PsManageAction.saveSchedule