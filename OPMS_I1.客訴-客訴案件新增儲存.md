# OPMS_I1.客訴-客訴案件新增儲存
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

客訴案件新增或存檔


## 修改歷程

| 修改時間 | 內容                            | 修改者 |
|----------|-------------------------------|--------|
| 20230719 | 新增規格                        | shawn  |
| 20230801 | 因應結構調整(程式不需調整)      | Nick   |
| 20230811 | 調整規格說明，加強對附件檔名解說 | Nick   |
| 20230814 | 調整附件說明                    | Nick   |
| 20230818 | 前端部分資料會有小數點          | Nick   |


## 來源URL及資料格式

| 項目   | 說明        |
|--------|-------------|
| URL    | /cs/save_cs |
| method | post        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。

| 欄位         | 名稱             | 資料型別 | 必填 | 資料儲存 & 說明 |
|--------------|----------------|:--------:|:----:|-----------------|
| opIsNew      | 是否為新增       |  string  |  M   | Y/N             |
| opReadOnly   | 是否唯獨         |  string  |  M   | Y/N             |
| opEditApply  | 是否編輯申請單   |  string  |  M   | Y/N             |
| opEditDiag   | 是否編輯真因     |  string  |  M   | Y/N             |
| opEditReply  | 是否進行終報回覆 |  string  |  M   | Y/N             |
| opEditClose  | 是否進行結案     |  string  |  M   | Y/N             |
| opEditReport | 是否編輯品保報告   |  string  |  M   | Y/N             |
| opCsData     | 客訴內容         |  object  |  M   |                 |
| opLotData    | 製造記錄         |  array   |  O   |                 |
| opCommData   | 客訴處理說明     |  array   |  O   |                 |
| opDocData    | 附件列表         |  array   |  O   |                 |
| opDiagData   | 真因分析與終判   |  object  |  O   |                 |


### 【opCsData】object

| 欄位               | 名稱         | 資料型別 | 必填 | 資料儲存 & 說明    |
|--------------------|--------------|:--------:|:----:|--------------------|
| opOrganizationId   | 工廠別       |  string  |  M   |                    |
| opCsUid            | uid          |  string  |  O   | opIsNew=N，才需傳入 |
| opCsSubject        | 主旨說明     |  string  |  M   |                    |
| opCustNo           | 客戶代號     |  string  |  O   |                    |
| opCustName         | 客戶名稱     |  string  |  M   |                    |
| opDestCustName     | 終端客戶     |  string  |  O   |                    |
| opCsGrade          | 風險度       |  string  |  O   |                    |
| opPatternNo        | 廠內色號     |  string  |  M   |                    |
| opGradeNo          | 規格/Grade   |  string  |  M   |                    |
| opCsItemType       | 產品別       |  string  |  M   |                    |
| opDfa              | 不良現象     |  string  |  M   |                    |
| opDfb              | 次           |  string  |  O   | 次(第一個下拉)     |
| opDfc              | 次           |  string  |  O   | 次(第二個下拉)     |
| opDfd              | 次           |  string  |  O   | 次(第三個下拉)     |
| opCsDfQty          | 抱怨數量     |  string  |  O   | 允許小數兩位       |
| opUnit             | 抱怨數量單位 |  string  |  O   |                    |
| opCsUsedQty        | 已用量       |  string  |  O   | 允許小數兩位       |
| opCsNonQty         | 未用量       |  string  |  O   | 允許小數兩位       |
| opSalesOrderNo     | 銷售訂單     |  string  |  O   |                    |
| opSalesQty         | 訂購量       |  string  |  O   | 允許小數一位       |
| opCsShipQty        | 已出貨量     |  string  |  O   | 允許小數一位       |
| opCsIssueDate      | 抱怨日       |  string  |  M   | yyyy/MM/dd         |
| opCsItemLocation   | 產品所在地   |  string  |  O   |                    |
| opCsItemNo         | 客戶料號     |  string  |  O   |                    |
| opSampleDFlag      | 樣本到廠     |  string  |  O   |                    |
| opCsRemark         | 客訴細節描述 |  string  |  O   |                    |
| opCsRtFlag         | 要求換貨     |  string  |  O   |                    |
| opCsRtQty          | 換貨量       |  string  |  O   | 允許小數兩位       |
| opCsRefundFlag     | 賠償要求     |  string  |  O   |                    |
| opFirstReportFlag  | 需要初報     |  string  |  O   |                    |
| opCsRefundCurrency | 賠償金額     |  string  |  O   | 幣別               |
| opCsRefundAmount   | 賠償金額     |  string  |  O   | 金額               |
| opCsRefundDesc     | 賠償金額     |  string  |  O   | 備註               |
| opFirstReportDate  | 初報回覆     |  string  |  O   |                    |
| opCsReportDate     | 終報回覆     |  string  |  O   |                    |
| opCsCloseReport    | 品保報告     |  string  |  O   |                    |


### 【opLotData】array 
| 欄位         | 名稱         | 資料型別 | 必填(有資料時) | 資料儲存 & 說明      |
|--------------|--------------|----------|----------------|----------------------|
| opCsLotNo    | Lot No.      | string   | M              |                      |
| opCsLotSeq   | id           | string   | M              |                      |
| opProdUnitNo | 生產單位代碼 | string   | O              |                      |
| opEquipId    | 生產機台     | string   | O              |                      |
| opCsLotQty   | 生產數量     | string   | O              | 允許小數兩位         |
| opMfgDate    | 生產日期     | string   | O              | yyyy/MM/dd           |
| opStatus     | 狀態         | string   | O              | N:新增/M:修改/D:刪除 |

### 【opCommData】array 
| 欄位           | 名稱         | 資料型別 | 必填(有資料時) | 資料儲存 & 說明 |
|----------------|------------|----------|----------------|-----------------|
| opCsComment    | 客訴處理說明 | string   | M              |                 |
| opCsCommentSeq | 序號         | string   | M              |                 |
| opUpdateByName | 人員         | string   | O              | NULL            |
| opUDT          | 日期         | string   | O              | NULL            |

### 【opDocData】array 
| 欄位           | 名稱          | 資料型別 | 必填(有資料時) | 資料儲存 & 說明       |
|----------------|---------------|----------|----------------|-----------------------|
| opCsDocUid     | uid           | string   | O              | 刪除才需傳入          |
| opDocFlag      | 檔案類別      | string   | M              |                       |
| opDocName      | 原始檔案名稱  | string   | M              |                       |
| opDocType      | 檔案類型      | string   | M              | application/pdf       |
| opActiveFlag   | 是否生效      | string   | M              | 固定Y                 |
| opStatus       | 狀態          | string   | O              | N:新增/D:刪除         |
| opContent      | 檔案內容      | string   | M              | base64 encoded string |
| opCreateByName | 上傳者/人員   | string   | O              | NULL                  |
| opCDT          | 上傳日期/日期 | string   | O              | NULL                  |


### 【opDiagData】object
| 欄位            | 名稱     | 資料型別 | 必填 | 資料儲存 & 說明 |
|-----------------|--------|----------|------|-----------------|
| opFinalDiagFlag | 終判     | string   | O    |                 |
| opDiagList      | 列表資料 | array    | O    |                 |

### 【opDiagList】array
| 欄位           | 名稱     | 資料型別 | 必填(有資料時) | 資料儲存 & 說明      |
|----------------|----------|----------|----------------|----------------------|
| opCsDiagSeq    | id       | string   | M              |                      |
| opDfCode       | 不良現象 | string   | M              |                      |
| opDiagFlag     | 真因分類 | string   | M              |                      |
| opDiagRemark   | 真因描述 | string   | O              |                      |
| opStatus       | 狀態     | string   | O              | N:新增/M:修改/D:刪除 |
| opUpdateByName | 更新人員 | string   | O              | NULL                 |
| opUDT          | 日期     | string   | O              | NULL                 |

#### Request 範例

```json
{
    "opIsNew":"N",  
    "opReadOnly":"N",   
    "opEditApply":"Y",
    "opEditDiag":"Y",  
    "opEditReply":"Y", 
    "opEditClose":"Y", 
    "opEditReport":"Y",
    "opCsData":{
        "opOrganizationId":"5000",  
        "opCsUid":"183AAEA7-6598-415B-8D52-3E8A6D710CE2",           
        "opCsSubject":"J20609C6長支客訴",       
        "opCustNo":"C2209003",          
        "opCustName":"奇美實業",        
        "opDestCustName":"亞旭",    
        "opCsGrade":"2",         
        "opPatternNo":"J20609C6",       
        "opGradeNo":"PC-6700",         
        "opCsItemType":"粒材",      
        "opDfa":"A07",             
        "opDfb":null,             
        "opDfc":null,             
        "opDfd":null,             
        "opCsDfQty":null,         
        "opUnit":null,            
        "opCsUsedQty":null,       
        "opCsNonQty":null,        
        "opSalesOrderNo":"3000 KG",   
        "opSalesQty":null,        
        "opCsShipQty":null,       
        "opCsIssueDate":"2023/07/18",     
        "opCsItemLocation":null,  
        "opCsItemNo":"PC-6700",
        "opSampleDFlag":null,    
        "opCsRemark":"客戶反應PC-6700 J20609C6有長枝(22T028251) 暫將長支去除後繼續使用物料~客戶觀感不佳~請防止再發!",        
        "opCsRtFlag":null,        
        "opCsRtQty":null,         
        "opCsRefundFlag":null,    
        "opFirstReportFlag": null,
        "opCsRefundCurrency":null,
        "opCsRefundAmount":null,  
        "opCsRefundDesc":null,    
        "opFirstReportDate":"2022/09/26", 
        "opCsReportDate":"2023/01/09",    
        "opCsCloseReport":"1.問題:
   長條(長枝)混入正常成品

2.原因:   
   開俥料有長條，人員未by pass直接吸到成品桶，導致長條混入。廠內已有SOP，
   人員身心狀況不佳，作業疏失。此為奇菱內部管理不周，奇菱進行自我檢討改善

3.檢討改善:  
   (1)未依SOP作業: 相關人員已懲處並公告，樹立紀律
    (2)開俥戒嚴期(自9/22~11/21為期2個月): 同仁習慣養成，導正行為模式
    (3)主管對員工上班關懷不夠: 人員交接班時，由班/組長與主管主動關心當班同仁
         身心狀況

4.結論
    長條客訴對集團打造a step up的品牌形象所造成影響，奇菱深感歉意
    奇菱主管會持續走動管理與主動關心基層人員，提升員工品質意識，不允許
    再發生類似客訴
"   
    },   
    "opLotData":[{
      "opCsLotNo": "22T028251",  
      "opCsLotSeq": "1",  
      "opProdUnitNo":"P3",
      "opEquipId":"2",   
      "opCsLotQty":"3014",
      "opMfgDate":"2022/08/25",   
      "opStatus":"M"    
    }], 
    "opCommData":[{
      "opCsComment":"新增客訴",
      "opCsCommentSeq":"1"
    }], 
    "opDocData":[{
        "opCsDocUid":null,  
        "opDocFlag":"終報",   
        "opDocName":"PC-6700 J20609C6 長枝客訴報告(終報).pptx",   
        "opDocType":"application/vnd.openxmlformats-officedocument.presentationml.presentation",   
        "opActiveFlag":"Y",
        "opStatus":"N",    
        "opContent":"base64 encoded string"   
    }],   
    "opDiagData":{
      "opFinalDiagFlag":"L",     
      "opDiagList":[{
        "opCsDiagSeq":"1",
        "opDfCode":"A07",
        "opDiagFlag":"A01",
        "opDiagRemark":"人員未依抽包驗色SOP，人員未依首件取樣SOP",
        "opStatus":"M"
      }]
    }  
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性

* {opIsNew}='Y'，變數 {opCsUid} 產生隨機uuid
* {opEditApply}是否編輯申請單='Y'
  * 如果{opIsNew}='Y'，進行以下新增動作：
    * 新增變數{seqNo}，格式'000'，ex:'007'    
    * 新增變數{seq_year} 今日西元年取後2碼  ex:2023取23
    * 新增變數{seq_month} 取今日月份 格式：MM ex:07
    * 新增變數{csNo} = 'C'+{seq_year}+{seq_month}+{seqNo}    
    * 查詢sql-1判斷是否存在
      * 不存在新增，執行sql-2，{seqNo} = '001'
      * 存在時更新，取出sql-1.SEQ_NUM後，SEQ_NUM+1，再更新SEQ_NUM回去
        ，{seqNo}= SEQ_NUM ex:'002'
    * 執行sql-3  
  * else 更新：執行sql-4
* 以下更新sql-5，依各條件更新不同欄位： 
  * {opEditDiag}是否編輯真因='Y'
    更新：FINAL_DIAG_FLAG
  * {opEditReport}是否寫品保報告='Y'
    更新：CS_CLOSE_REPORT
  * {opEditReply}是否進行終報回覆='Y'
    更新：STATUS, CS_REPORT_DATE
  * {opEditClose}是否進行結案='Y'
    更新：STATUS,UUN_Z,UDT_Z 

* {opReadOnly}為'N'時，{opDocData} 附件存檔： 
  * 依據{opStatus}狀態進行新增刪除：
    * N:
      * {opCsDocUid} 產生隨機uuid，此 UUID 即是上傳至實體路徑【檔案儲存名稱】
      * {date} 今日日期 ex.202307
      * 先上傳檔案將 BASE64 解碼成檔案，寫入路徑：cs.file.root/{date}/{opCsDocUid}
      * 可參考CsDocAction.uploadDoc
      * 新增db，sql-15
    * D:
      * 執行sql=16

* {opReadOnly}為'N'時，{opLotData} 存檔：
  * 依據{opStatus}狀態進行新增修改刪除：
    * N：執行sql-6
    * M：執行sql-7
    * D：執行sql-8
* {opEditDiag}為'Y'時，{opDiagList} 存檔：
  * 依據{opStatus}狀態進行新增修改刪除：
    * N：執行sql-9
    * M：執行sql-10
    * D：執行sql-11
* {opReadOnly}為'N'時，{opCommData} 存檔：
    * 查詢sql-12，有資料更新sql-13，無資料則新增sql-14


* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明



SQL-1:
```sql
--SysSeqNumDao.isSeqNumExisted
 SELECT SEQ_YEAR, SEQ_MONTH, SEQ_NUM FROM SYS_SEQNUM 
 WHERE PREFIX='C' AND SEQ_YEAR={seq_year} AND SEQ_MONTH = {seq_month}
```

SQL-2:
```sql
--SysSeqNumDao.insertSeqNum
 INSERT INTO SYS_SEQNUM ( PREFIX, SEQ_YEAR, SEQ_MONTH, SEQ_NUM, UDT ) 
 values ('C',{seq_year},{seq_month},1 ,GETDATE() )

```

SQL-3
```sql
--CsModifyDao.insertCsH
INSERT INTO CS_H (
  CS_UID, PATTERN_NO, UNIT, CS_NO, CS_SUBJECT, 
  ORGANIZATION_ID, SALES_USER_ID, REPORT_USER_ID,GRADE_NO, 
  CS_ITEM_TYPE, ACTIVE_FLAG, STATUS, CS_GRADE, CUST_NAME, CUST_NO,
  DEST_CUST_NAME, CS_ISSUE_DATE, CS_ITEM_NO, DF_A, DF_B, DF_C, DF_D, DF_O, 
  CS_REMARK, CS_DF_QTY, CS_USED_QTY, CS_NON_QTY, CS_ITEM_LOCATION, CS_RT_FLAG, 
  CS_RT_QTY, CS_REFUND_FLAG, CS_REFUND_DESC, CS_REFUND_AMOUNT, CS_REFUND_CURRENCY,
  SALES_ORDER_NO, SALES_QTY, CS_SHIP_QTY,SAMPLE_D_FLAG, FIRST_REPORT_FLAG,FIRST_REPORT_DATE,
  UDT_A, UUN_A, CDT, CREATE_BY, UDT, UPDATE_BY
)  
VALUES (
  {opCsUid}, {opPatternNo}, {opUnit}, {csNo}, {opCsSubject},
  {opOrganizationId}, {登入者使用者ID} , null, {opGradeNo},
  {opCsItemType}, null, 0, {opCsGrade}, {opCustName}, {opCustNo},
  {opDestCustName}, {opCsIssueDate},{opCsItemNo},{opDfa},{opDfb},{opDfc},{opDfd},null,
  {opCsRemark},{opCsDfQty},{opCsUsedQty},{opCsNonQty},{opCsItemLocation},{opCsRtFlag},
  {opCsRtQty},{opCsRefundFlag},{opCsRefundDesc},{opCsRefundAmount},{opCsRefundCurrency},
  {opSalesOrderNo},{opSalesQty},{opCsShipQty},{opSampleDFlag},{opFirstReportFlag},{opFirstReportDate}, 
  GETDATE(), {登入者使用者名稱} , GETDATE(), {登入者使用者ID}, GETDATE(), {登入者使用者ID} 
)

```
SQL-4
```sql
--CsModifyDao.updateCsApply
UPDATE CS_H SET PATTERN_NO={opPatternNo}, UNIT={opUnit}, 
  CS_SUBJECT={opCsSubject}, GRADE_NO={opGradeNo}, 
  CS_ITEM_TYPE={opCsItemType}, CS_GRADE={opCsGrade}, CUST_NAME={opCustName},
  CUST_NO={opCustNo}, DEST_CUST_NAME={opDestCustName}, CS_ISSUE_DATE={opCsIssueDate}, 
  CS_ITEM_NO={opCsItemNo}, DF_A={opDfa}, DF_B={opDfb}, DF_C={opDfc}, DF_D={opDfd}, 
  DF_O=null, CS_REMARK={opCsRemark}, CS_DF_QTY={opCsDfQty}, CS_USED_QTY={opCsUsedQty}, 
  CS_NON_QTY={opCsNonQty}, CS_ITEM_LOCATION={opCsItemLocation}, CS_RT_FLAG={opCsRtFlag}, 
  CS_RT_QTY={opCsRtQty}, CS_REFUND_FLAG={opCsRefundFlag}, CS_REFUND_DESC={opCsRefundDesc}, 
  CS_REFUND_AMOUNT={opCsRefundAmount}, CS_REFUND_CURRENCY={opCsRefundCurrency}, 
  SALES_ORDER_NO={opSalesOrderNo}, SALES_QTY={opSalesQty}, CS_SHIP_QTY={opCsShipQty}, 
  SAMPLE_D_FLAG={opSampleDFlag}, CS_REPORT_DATE={opCsReportDate},
  FIRST_REPORT_FLAG={opFirstReportFlag}, FIRST_REPORT_DATE={opFirstReportDate},
  UDT_A=GETDATE(), UUN_A={登入者使用者名稱}, UDT=GETDATE(), UPDATE_BY={登入者使用者ID}
WHERE CS_UID={opCsUid}
```

SQL-5
```sql
--CsModifyDao.updateCsFinalDiagFlag
UPDATE CS_H SET FINAL_DIAG_FLAG={opFinalDiagFlag}, --{opEditDiag}=Y時更新
CS_CLOSE_REPORT={opCsCloseReport}, --{opEditReport}=Y時更新
STATUS='E', CS_REPORT_DATE={opCsReportDate}, --{opEditReply}=Y時更新
STATUS='Z', UUN_Z={登入者使用者名稱}, UDT_Z=GETDATE() --{opEditClose}=Y時更新
UDT=GETDATE(), 
UPDATE_BY={登入者使用者ID} 
WHERE CS_UID={opCsUid}
```

SQL-6
```sql
--CsModifyDao.insertLot
INSERT INTO CS_LOT(
  CS_UID, CS_LOT_SEQ,CS_LOT_NO,
  CS_LOT_QTY,ORGANIZATION_ID,PRODUNIT_NO,EQUIP_ID,
  MFG_DATE, CDT, CREATE_BY,UDT,UPDATE_BY
) 
VALUES(
  {opCsUid},{opCsLotSeq},{opCsLotNo},{opCsLotQty},{opOrganizationId},{opProdUnitNo},{opEquipId},{opMfgDate}, GETDATE(), {登入者使用者ID} ,GETDATE(),{登入者使用者ID} 
)
```

SQL-7
```sql
--CsModifyDao.updateLot
UPDATE CS_LOT SET CS_LOT_NO={opCsLotNo},PRODUNIT_NO={opProdUnitNo},
EQUIP_ID={opEquipId}, CS_LOT_QTY={opCsLotQty},
MFG_DATE={opMfgDate}, UDT=GETDATE(),
UPDATE_BY={登入者使用者ID}
WHERE CS_UID= {opCsUid} AND CS_LOT_SEQ={opCsLotSeq}

```

SQL-8
```sql
--CsModifyDao.deleteLot
DELETE CS_LOT WHERE CS_UID={opCsUid} AND CS_LOT_SEQ={opCsLotSeq}

```
SQL-9
```sql
--insertDiag
INSERT INTO CS_DIAG(
  CS_UID, CS_DIAG_SEQ, DF_CODE,
  DIAG_FLAG, DIAG_REMARK, CDT,
  CREATE_BY, UDT, UPDATE_BY
) 
VALUES(
  {opCsUid},{opCsDiagSeq}, {opDfCode},{opDiagFlag},{opDiagRemark}, GETDATE(), {登入者使用者ID},GETDATE(),{登入者使用者ID}
)
```

SQL-10
```sql
--updateDiag
UPDATE CS_DIAG SET DF_CODE={opDfCode},
DIAG_FLAG={opDiagFlag},
DIAG_REMARK={opDiagRemark},
UDT=GETDATE(),
UPDATE_BY={登入者使用者ID} 
WHERE CS_UID={opCsUid} AND CS_DIAG_SEQ={opCsDiagSeq}
```

SQL-11
```sql
DELETE CS_DIAG WHERE CS_UID={opCsUid} AND CS_DIAG_SEQ={opCsDiagSeq}
```

SQL-12
```sql
--insertComment
SELECT * FROM CS_COMMENT 
WHERE CS_UID={opCsUid} AND CS_COMMENT_SEQ={opCsCommentSeq}
```

SQL-13
```sql
UPDATE CS_COMMENT SET CS_COMMENT={opCsComment}, 
UDT=GETDATE(), 
UPDATE_BY={登入者使用者ID} 
WHERE CS_UID={opCsUid} AND CS_COMMENT_SEQ={opCsCommentSeq}
```

SQL-14
```sql
INSERT INTO CS_COMMENT(CS_UID,CS_COMMENT_SEQ,CS_COMMENT, CDT, CREATE_BY, UDT, UPDATE_BY) 
VALUES({opCsUid},{opCsCommentSeq},{opCsComment}, GETDATE(), {登入者使用者ID},GETDATE(),{登入者使用者ID})
```

SQL-15
```sql
--insertDoc
INSERT INTO CS_DOC(
  CS_UID, CS_DOC_UID, DOC_FLAG,
  DOC_NAME, DOC_TYPE, SYS_DOC_ID,
  ACTIVE_FLAG, CDT, CREATE_BY
VALUES(
  {opCsUid},{opCsDocUid},{opDocFlag},
  {opDocName}, {opDocType}, {opCsDocUid}, 
  {opActiveFlag}, GETDATE(), {登入者使用者ID} 
)
```

SQL-16
```sql
DELETE CS_DOC WHERE CS_DOC_UID={opCsDocUid}
```


# Response 欄位
| 欄位    | 名稱 | 資料型別 | 資料儲存 & 說明 |
|---------|------|----------|-----------------|
| opCsUid | uid  | string   |                 |

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":{
      "opCsUid":"183AAEA7-6598-415B-8D52-3E8A6D710CE2"
    }
  }
}
```

## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考

* CsManageAction.saveCs