# OPMS-API通用格式

###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

## 一、串接規格
### 命名
1.欄位名稱皆是大小寫有別。
2.小駝峰命名法：第一個字小寫，2個字母合併時，第二個字大寫。
2.URLs使用小寫命名，複合式名稱以(_)相隔

### 格式
傳入及回傳資料皆以JSON，UTF-8 格式。
### 通用 Header 定義
預設 Header 定義內容
<font color=red><b>如各API規格有撰寫Header定義，則以各API規格為主，不參考通用定義。</b></font>
#### Request
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。

| Header Key    | Header Value                                  | 必填 | 說明                                         |
|---------------|-----------------------------------------------|------|----------------------------------------------|
| Content-Type  | application/json; charset=UTF-8               | M    |                                              |
| Authorization | Bearer token值(refreshToken時帶 refreshToken) | O    | 預設必填，如為白名單不需填由各API規格另外定義 |
| x-uid         | 後端給的uid(取得token 時給的)                 | O    | 預設必填，如為白名單不需填由各API規格另外定義 |

#### Response
|   Header Key | Header Value                    |
| -----------: | ------------------------------- |
| Content-Type | application/json; charset=UTF-8 |

### 資料型別
根據 OAS 定義的資料型別格式
| 資料型別    | 格式     | 說明                                            |
|-------------|----------|-------------------------------------------------|
| integer     | int32    | MSSQL int                                       |
| integer     | int64    | MSSQL bigint                                    |
| numeric     | float    |                                                 |
| number      | double   |                                                 |
| string      | string   |                                                 |
| string      | byte     | base64 encoded characters                       |
| string      | binary   | any sequence of octets                          |
| boolean     | int      | true/false、1/0                                  |
| string      | date     | yyyy-mm-dd                                      |
| string      | datetime | yyyy-mm-dd hh:nn:ss                             |
| object      | value    | 為另一 json 格式                                |
| array       |          | 以中括號[…]前後包覆著 value 值，值與值以逗號分隔 |
| arraystring |          | 以中括號[…]前後包覆著 value 值，值與值以逗號分隔 |
| json        |          | 以大括號{}前後包覆著 value 值，key 和 value      |

## 二、驗証機制
### 方法
使用 jwt token
### token 欄位
| 參數 | 名稱       |
| ---- | ---------- |
| UID  | 登入者帳號 |


### refresh token 欄位
| 參數 | 名稱       |
| ---- | ---------- |
| UID  | 登入者帳號 |


### 時效
Token有效時間為15分鐘。
refresh token有效時間為30分鐘


## 三、共用欄位說明

### Response 欄位

#### Response 欄位(<font color=green>請求成功</font>)
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位    | 名稱     | 資料型別 | 必填(非必填節點不需存在) | 說明     |
| ------- | -------- | -------- | :----------------------: | -------- |
| msgCode | 訊息代碼 | string   |            M             |          |
| result  | 回傳資料 | object   |            M             | 進階訊息 |


#### 【result】child node
| 欄位     | 名稱     | 資料型別          | 必填(非必填節點不需存在) | 資料儲存 & 說明               |
| -------- | -------- | ----------------- | ------------------------ | ----------------------------- |
| pageInfo | 分頁資訊 | object            | O                        | 依API需求決定是否回傳分頁資訊 |
| content  | 附帶訊息 | null/object/array | M                        | 依API需求決定內容             |

#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 必填 | 資料儲存 & 說明 |
| ---------- | ------------ | -------- | ---- | --------------- |
| pageNumber | 目前所在分頁 | int      | M    |                 |
| pageSize   | 分頁筆數大小 | int      | M    |                 |
| pages      | 總分頁數     | int      | M    |                 |
| total      | 總資料筆數   | int      | M    |                 |


成功JSON範例
```json
HTTP/1.1 200 Success.

{
  "msgCode":"MSSSAGECODE",
  "result":{
    "content": null // null , object,array
  }
}
```

有分頁-成功JSON範例
```json
無資料
HTTP/1.1 200 Success.

{
  "msgCode":null,
  "result":{
    "pageInfo": {
      "pageNumber": 0,
      "pageSize": 0,
      "pages": 0,
      "total": 0
    },
    "content": null
  }
}
```

```json
有資料
HTTP/1.1 200 Success.

{
  "msgCode": null,
  "result":{
    "pageInfo": {
      "pageNumber": 1, //第幾頁，0 -> get all
      "pageSize": 20,  //一頁幾筆 -> 如果 pageNumber 0 ,pageSize = total
      "pages": 5,      //total 頁數 -> 如果 pageNumber 0 ,pages always 1
      "total": 100     //total 筆數
    },
    "content":[{...}]
  }
}
```

#### Response 欄位(<font color =red>請求失敗</font>)
M: 必填 O: 選填
| 欄位    | 名稱     | 資料型別 | 必填(非必填節點不需存在) | 說明                                            |
| ------- | -------- | -------- | :----------------------: | ----------------------------------------------- |
| msgCode | 訊息代碼 | string   |            M             | 參考[訊息代碼對照表](./OPMS_REF_訊息代碼對照表) |
| result  | 回傳資料 | object   |            M             | 進階訊息                                        |


#### 【result】child node
| 欄位    | 名稱     | 資料型別          | 必填(非必填節點不需存在) | 資料儲存 & 說明 |
| ------- | -------- | ----------------- | ------------------------ | --------------- |
| content | 附帶訊息 | null/object/array | M                        | 依需求決定內容  |


一般錯誤格式
```json
HTTP/1.1 400 Bad Request

{
  "msgCode": "MSSSAGECODE", 
  "result":{
    "content": null  //null or object or array
  }
}
```

欄位驗證錯誤格式
```json
HTTP/1.1 400 Bad Request

{
  "msgCode": "VALIDATION_ERROR", 
  "result":{
    "content": [
      {
        "field":"username",
        "message":"使用者名稱不得為空"
      },{
        "field":"amount",
        "message":"數量欄位不得大於10"
      }
      ]
  }
}
```

HTTP/1.1 401 Unauthorized
```json
{
  "msgCode": "UNAUTHORIZED", //使用者認證失敗
  "result":{
    "content": null 
  }
}
```

HTTP/1.1 403 Forbidden
```json
{
  "msgCode": "FORBIDDEN", //使用者權限不足
  "result":{
    "content": null
  }
}
```

500 系統層級錯誤
```json
{
  "msgCode": "SYSTEM_ERROR", //[訊息代碼對照表](./OPMS_REF_訊息代碼對照表)
  "result":{
    "content": null
  }
}
```

## 四、Response 狀態碼與說明
#### 回傳狀態碼(Status Code)
可使用的狀態碼由RFC 7231所定義並且已註冊狀態碼亦已列示於IANA Status Code Registry。

另特殊自訂狀態碼如下：

| 狀態碼（status） | 狀態碼說明                       |
| ---------------- | -------------------------------- |
| 200              | Success.                         |
| 400              | Bad Request.                     |
| 401              | 使用者認證失敗                   |
| 403              | 使用者權限不足                   |
| 500              | 發生系統層級錯誤，請聯繫資訊人員 |
