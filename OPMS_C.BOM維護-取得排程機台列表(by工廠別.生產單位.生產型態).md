# OPMS_C.BOM維護-取得排程機台列表(by工廠別.生產單位.生產型態)
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得排程機台列表(by工廠別.生產單位.生產型態)

## 修改歷程

| 修改時間 | 內容                     | 修改者 |
|----------|------------------------|--------|
| 20230516 | 新增規格                 | Nick   |
| 20230522 | opPsFlag調整預設為 null  | Nick   |
| 20231221 | 新增排序方式 opOrderType | Nick   |

## 來源URL及資料格式

| 項目   | 說明                       |
|--------|----------------------------|
| URL    | /bom/get_ps_equipment_list |
| method | post                       |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在
| 欄位             | 名稱         | 資料型別 | 必填 | 來源資料 & 說明                                      |
|------------------|------------|----------|:----:|-------------------------------------------------|
| opOrganizationId | 工廠別       | string   |  O   | 預設 `null` ， null:不指定 / 5000:奇菱 / 6000:菱翔    |
| opProdunitNo     | 生產單位代碼 | string   |  O   | 預設 `null`， null:不指定 ..etc                       |
| opProdType       | 產品型態     | string   |  O   | 預設 `null`， null:不指定  / C:單軸 / M:雙軸 / B:壓板 |
| opPsFlag         | 機器是否排程 | string   |  O   | 預設 `null` , null:不指定 / Y:是 / N:否              |
| opOrderType         | 排序方式 | string   |  O   | 預設 `null` , null:預設使用程式特殊排序 / 1:SQL Order    |

#### Request 範例

```json
{
  "opOrganizationId": null ,
  "opProdunitNo" : null,
  "opPsFlag" : "Y"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])

* 前端條件如為 null 表全部，對應的指定條件不組，否則按照前端條件指定。
* 參考 SQL-1 查回結果 result
* 如果 opOrderType = null ，則使用程式重新排序，使用 opEquipName 並將數值格式化成 XXX 三位數排序
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
SELECT EQUIP_ID,EQUIP_NAME,PROD_TYPE,CQ_FLAG,NP FROM EQUIP_H 
WHERE 1=1
--指定條件
AND PS_FLAG={opPsFlag}
AND ORGANIZATION_ID={opOrganizationId} --工廠別
AND PRODUNIT_NO={opProdunitNo}    --生產單位代碼
AND PROD_TYPE={opProdType}  --產品型態
ORDER BY M_EQUIP_ID
```


# Response 欄位
#### 【content】array
| 欄位        | 名稱       | 資料型別 | 來源資料 & 說明 |
|-------------|----------|----------|-----------------|
| opEquipId   | 機台代號   | string   | EQUIP_ID        |
| opEquipName | 機台名稱   | string   | EQUIP_NAME      |
| opProdType  | 生產型態   | string   | PROD_TYPE       |
| opCqFlag    | 體積或計量 | string   | CQ_FLAG         |
| opNp        | 機台人數   | string   | NP              |

#### Response 範例

```json
{
    "msgCode": null,
    "result": {
       "content": [
            {"opEquipId":"601","opEquipName":"憲成#1","opProdType":null,"opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"602","opEquipName":"憲成#2","opProdType":null,"opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"603","opEquipName":"憲成#3","opProdType":null,"opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"604","opEquipName":"憲成#4","opProdType":null,"opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"605","opEquipName":"憲成#5","opProdType":null,"opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"606","opEquipName":"憲成#6","opProdType":null,"opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"607","opEquipName":"憲成#7","opProdType":null,"opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"608","opEquipName":"憲成#8","opProdType":null,"opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"609","opEquipName":"憲成#9","opProdType":null,"opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"610","opEquipName":"憲成#10","opProdType":null,"opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"653","opEquipName":"憲成#11","opProdType":null,"opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"61","opEquipName":"實驗#1","opProdType":"C","opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"62","opEquipName":"實驗#2","opProdType":"C","opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"63","opEquipName":"實驗#3","opProdType":"C","opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"65","opEquipName":"實驗#4","opProdType":"C","opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"44","opEquipName":"染色少N","opProdType":"C","opCqFlag":"C","opNp":"0.5"},
            {"opEquipId":"69","opEquipName":"實驗#6","opProdType":"C","opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"70","opEquipName":"實驗#Z","opProdType":"C","opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"17","opEquipName":"染色少1","opProdType":"C","opCqFlag":"C","opNp":"0.5"},
            {"opEquipId":"32","opEquipName":"染色少2","opProdType":"C","opCqFlag":"C","opNp":"0.5"},
            {"opEquipId":"33","opEquipName":"染色少3","opProdType":"C","opCqFlag":"C","opNp":"0.5"},
            {"opEquipId":"34","opEquipName":"染色少4","opProdType":"C","opCqFlag":"C","opNp":"0.5"},
            {"opEquipId":"35","opEquipName":"染色少5","opProdType":"C","opCqFlag":"C","opNp":"0.5"},
            {"opEquipId":"36","opEquipName":"染色少6","opProdType":"C","opCqFlag":"C","opNp":"0.5"},
            {"opEquipId":"37","opEquipName":"染色少7","opProdType":"C","opCqFlag":"C","opNp":"0.5"},
            {"opEquipId":"38","opEquipName":"染色少8","opProdType":"C","opCqFlag":"C","opNp":"0.5"},
            {"opEquipId":"40","opEquipName":"染色少10","opProdType":"C","opCqFlag":"C","opNp":"0.5"},
            {"opEquipId":"46","opEquipName":"複材#6","opProdType":"M","opCqFlag":"Q","opNp":"1.0"},
            {"opEquipId":"11","opEquipName":"複材#1","opProdType":"M","opCqFlag":"C","opNp":"1.0"},
            {"opEquipId":"12","opEquipName":"複材#2","opProdType":"M","opCqFlag":"Q","opNp":"1.0"},
            {"opEquipId":"14","opEquipName":"複材#3","opProdType":"M","opCqFlag":"Q","opNp":"1.0"},
            {"opEquipId":"15","opEquipName":"複材#4","opProdType":"M","opCqFlag":"Q","opNp":"1.0"},
            {"opEquipId":"16","opEquipName":"複材#5","opProdType":"M","opCqFlag":"Q","opNp":"1.0"},
            {"opEquipId":"111","opEquipName":"複材#11(線外)","opProdType":"M","opCqFlag":null,"opNp":"1.0"},
            {"opEquipId":"121","opEquipName":"新材料課#1","opProdType":"M","opCqFlag":"Q","opNp":"1.0"},
            {"opEquipId":"122","opEquipName":"新材料課#2","opProdType":"M","opCqFlag":"Q","opNp":"1.0"},
            {"opEquipId":"123","opEquipName":"新材料課#3","opProdType":"M","opCqFlag":"Q","opNp":"1.0"},
            {"opEquipId":"124","opEquipName":"新材料課#4","opProdType":"M","opCqFlag":"Q","opNp":"1.0"}
      ]
    }
}
```

