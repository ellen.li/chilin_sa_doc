# 染色-SOC管理-查詢單筆
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容                      | 修改者 |
|----------|-------------------------|--------|
| 20230828 | 新增規格                  | 黃東俞 |
| 20231024 | 調整欄位中文名稱opSocDesc | Nick   |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /soc/soc_c_single           |
| method | post                        |


## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位      | 名稱 | 資料型別 | 必填 | 資料儲存 & 說明     |
|----------|------|----------|:----:|-------------------|
| opSocCUid | ID  | string   |  M   |                   |

#### Request 範例

```json
{
  "opSocCUid": "44CEDF0C-BE87-4CB3-B1FC-DDF98285097B"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 參考 SQL-1 取得資料並回傳
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
-- SocCQueryDao.getSocCSingle
SELECT E.ORGANIZATION_ID,S.EQUIP_ID, E.EQUIP_NAME, S.ITEM_NO, S.SOC_C_UID, S.SOC_C_VER, S.STATUS, S.MFG_QTY, S.MOLD_SCREEN_SIZE, S.SOAK_LENGTH,
S.SCREW_TYPE, S.MOLD_HOLE, S.MOLD_APETURE, S.CUTTER_TYPE, S.SCREEN_APETURE, S.SOC_DESC, S.PROD_DESC,
S.C_1_STD, S.C_1_MAX, S.C_1_MIN, S.C_2_STD, S.C_2_MAX, S.C_2_MIN, S.C_3_STD, S.C_3_MAX, S.C_3_MIN,
S.C_4_STD, S.C_4_MAX, S.C_4_MIN, S.C_5_STD, S.C_5_MAX, S.C_5_MIN, S.C_6_STD, S.C_6_MAX, S.C_6_MIN,
S.C_7_STD, S.C_7_MAX, S.C_7_MIN, S.C_8_STD, S.C_8_MAX, S.C_8_MIN, S.C_9_STD, S.C_9_MAX, S.C_9_MIN,
S.C_A_STD, S.C_A_MAX, S.C_A_MIN, S.C_B_STD, S.C_B_MAX, S.C_B_MIN, S.C_C_STD, S.C_C_MAX, S.C_C_MIN,
S.SCREW_RPM_STD, S.SCREW_RPM_MAX, S.SCREW_RPM_MIN, S.WRENCH_RATE, S.VACUUM_INDEX,
S.FEEDER_RPM_STD, S.FEEDER_RPM_MAX, S.FEEDER_RPM_MIN,
S.IRON_F_QTY, S.IRON_ITEM_QTY, S.B_TEMP_STD, S.B_TEMP_MAX, S.B_TEMP_MIN, S.S_TEMP_STD, S.S_TEMP_MAX, S.S_TEMP_MIN,
S.E_TEMP_STD, S.E_TEMP_MAX, S.E_TEMP_MIN, S.NONSTOP_FLAG,
S.MFG_DESC, S.MFG_NOTICE, S.SCREEN_FREQ, S.RECORD_FREQ,S.REMARK, S.CDT, S.CREATE_BY, S.PUB_DATE, S.PUBLIC_BY, S.UDT, S.UPDATE_BY, U.USER_NAME PUBLIC_NAME
FROM SOC_C S
INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
LEFT JOIN SYS_USER U ON S.PUBLIC_BY = U.USER_ID
WHERE S.SOC_C_UID = { opSocCUid }
```

# Response 欄位
| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
| ---------------- | -------- | -------- | --------------- |
| opSocUid         | ID            | string  | SOC_C.SOC_C_UID   |
| opEquipId        | 機台          | string  | SOC_C.EQUIP_ID     |
| opItemNo         | 料號          | string  | SOC_C.ITEM_NO      |
| opSocVer         | 版號          | string  | SOC_C.SOC_C_VER    |
| opStatus         | 狀態          | string  | SOC_C.STATUS       |
| opMfgQty         | 每分鐘產出量   | string  | SOC_C.MFG_QTY      |
| opMoldScreenSize | 模頭濾網       | string  | SOC_C.MOLD_SCREEN_SIZE |
| opSoakLength     | 大槽膠條浸水長度| string  | SOC_C.SOAK_LENGTH      |
| opScrewType      | 螺桿型號       | string  | SOC_C.SCREW_TYPE     |
| opMoldHole       | 模嘴孔數       | string  | SOC_C.MOLD_HOLE      |
| opMoldApeture    | 模嘴孔徑       | string  | SOC_C.MOLD_APETURE   |
| opCutterType     | 切刀型態       | string  | SOC_C.CUTTER_TYPE    |
| opScreenApeture  | 篩網孔徑       | string  | SOC_C.SCREEN_APETURE |
| opSocDesc        | 原料說明    | string  | SOC_C.SOC_DESC       |
| opProdDesc       | 產品說明       | string  | SOC_C.PROD_DESC      |
| opC1Std          | C1標準        | string  | SOC_C.C_1_STD     |
| opC1Max          | C1上限        | string  | SOC_C.C_1_MAX     |
| opC1Min          | C1下限        | string  | SOC_C.C_1_MIN     |
| opC2Std          | C2標準        | string  | SOC_C.C_2_STD     |
| opC2Max          | C2上限        | string  | SOC_C.C_2_MAX     |
| opC2Min          | C2下限        | string  | SOC_C.C_2_MIN     |
| opC3Std          | C3標準        | string  | SOC_C.C_3_STD     |
| opC3Max          | C3上限        | string  | SOC_C.C_3_MAX     |
| opC3Min          | C3下限        | string  | SOC_C.C_3_MIN     |
| opC4Std          | C4標準        | string  | SOC_C.C_4_STD     |
| opC4Max          | C4上限        | string  | SOC_C.C_4_MAX     |
| opC4Min          | C4下限        | string  | SOC_C.C_4_MIN     |
| opC5Std          | C5標準        | string  | SOC_C.C_5_STD     |
| opC5Max          | C5上限        | string  | SOC_C.C_5_MAX     |
| opC5Min          | C5下限        | string  | SOC_C.C_5_MIN     |
| opC6Std          | C6標準        | string  | SOC_C.C_6_STD     |
| opC6Max          | C6上限        | string  | SOC_C.C_6_MAX     |
| opC6Min          | C6下限        | string  | SOC_C.C_6_MIN     |
| opC7Std          | C7標準        | string  | SOC_C.C_7_STD     |
| opC7Max          | C7上限        | string  | SOC_C.C_7_MAX     |
| opC7Min          | C7下限        | string  | SOC_C.C_7_MIN     |
| opC8Std          | C8標準        | string  | SOC_C.C_8_STD     |
| opC8Max          | C8上限        | string  | SOC_C.C_8_MAX     |
| opC8Min          | C8下限        | string  | SOC_C.C_8_MIN     |
| opC9Std          | C9標準        | string  | SOC_C.C_9_STD     |
| opC9Max          | C9上限        | string  | SOC_C.C_9_MAX     |
| opC9Min          | C9下限        | string  | SOC_C.C_9_MIN     |
| opCAStd          | C10標準       | string  | SOC_C.C_A_STD     |
| opCAMax          | C10上限       | string  | SOC_C.C_A_MAX     |
| opCAMin          | C10下限       | string  | SOC_C.C_A_MIN     |
| opCBStd          | C11標準       | string  | SOC_C.C_B_STD     |
| opCBMax          | C11上限       | string  | SOC_C.C_B_MAX     |
| opCBMin          | C11下限       | string  | SOC_C.C_B_MIN     |
| opCCStd          | C12標準       | string  | SOC_C.C_C_STD     |
| opCCMax          | C12上限       | string  | SOC_C.C_C_MAX     |
| opCCMin          | C12下限       | string  | SOC_C.C_C_MIN     |
| opScrewRpmStd    | 螺桿轉速       | string  | SOC_C.SCREW_RPM_STD |
| opScrewRpmMax    | 螺桿轉速上限   | string  | SOC_C.SCREW_RPM_MAX  |
| opScrewRpmMin    | 螺桿轉速下限   | string  | SOC_C.SCREW_RPM_MIN  |
| opFeederRpmStd   | 餵料機轉速     | string  | SOC_C.FEEDER_RPM_STD |
| opFeederRpmMax   | 餵料機轉速下限  | string  | SOC_C.FEEDER_RPM_MAX |
| opFeederRpmMin   | 餵料機轉速下限  | string  | SOC_C.FEEDER_RPM_MIN |
| opWrenchRate     | 扭力           | string  | SOC_C.WRENCH_RATE    |
| opVacuumIndex    | 真空指數       | string  | SOC_C.VACUUM_INDEX   |
| opIronItemQty    | 磁力架鐵屑F1   | string  | SOC_C.IRON_ITEM_QTY  |
| opBTempStd       | 二槽水溫標準值 | string  | SOC_C.B_TEMP_STD     |
| opBTempMax       | 二槽水溫上限   | string  | SOC_C.B_TEMP_MAX     |
| opBTempMin       | 二槽水溫下限   | string  | SOC_C.B_TEMP_MIN     |
| opSTempStd       | 一槽水溫標準值 | string  | SOC_C.S_TEMP_STD     |
| opSTempMax       | 一槽水溫上限   | string  | SOC_C.S_TEMP_MAX     |
| opSTempMin       | 一槽水溫下限   | string  | SOC_C.S_TEMP_MIN     |
| opETempStd       | 三槽水溫標準值 | string  | SOC_C.E_TEMP_STD     |
| opETempMax       | 三槽水溫上限   | string  | SOC_C.E_TEMP_MAX     |
| opETempMin       | 三槽水溫下限   | string  | SOC_C.E_TEMP_MIN     |
| opScreenFreq     | 網目更換       | string  | SOC_C.SCREEN_FREQ    |
| opRecordFreq     | 記錄頻率       | string  | SOC_C.RECORD_FREQ    |
| opMfgNotice      | 作業注意事項   | string  | SOC_C.MFG_NOTICE     |
| opCdt            | 建立日期       | string  | SOC_C.CDT yyyy/mm/dd hh:mm:ss  |
| opCreateBy       | 建立人員       | string  | SOC_C.CREATE_BY      |
| opUdt            | 更新日期       | string  | SOC_C.UDT  yyyy/mm/dd hh:mm:ss |
| opUpdateBy       | 更新人員       | string  | SOC_C.UPDATE_BY      |
| opPubDate        | 發行日期       | string  | SOC_C.PUB_DATE  yyyy/mm/dd |
| opPublicBy       | 發行人代碼     | string  | SOC_C.PUBLIC_BY      |
| opPublicName     | 發行人名稱     | string  | SYS_USER.USER_NAME   |
| opRemark         | 備註          | string  | SOC_C.REMARK             |
| opEquipName      | 機台名稱       | string  | EQUIP_H.EQUIP_NAME   |
| opOrganizationId | 工廠別        | string  | EQUIP_H.ORGANIZATION_ID |
| opNonstopFlag    | 驗色不停車     | string  | SOC_C.NONSTOP_FLAG   |


#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":{
      "opSocCUid":"44CEDF0C-BE87-4CB3-B1FC-DDF98285097B",
      "opEquipId":"32",
      "opItemNo":"110UXXXDPC110XXXX25F21306XX1BXA15681",
      "opSocCVer":"1",
      "opStatus":"N",
      "opMfgQty":null,
      "opMoldScreenSize":"50/50",
      "opSoakLength":null,
      "opScrewType":"少2",
      "opMoldHole":"5",
      "opMoldApeture":"3.5",
      "opCutterType":"S",
      "opScreenApeture":"5",
      "opSocDesc":"PC-110U",
      "opProdDesc":"PC",
      "opC1Std":"180",
      "opC1Max":"190",
      "opC1Min":"170",
      "opC2Std":"265",
      "opC2Max":"275",
      "opC2Min":"255",
      "opC3Std":"270",
      "opC3Max":"280",
      "opC3Min":"260",
      "opC4Std":"275",
      "opC4Max":"285",
      "opC4Min":"265",
      "opC5Std":"280",
      "opC5Max":"290",
      "opC5Min":"270",
      "opC6Std":"280",
      "opC6Max":"290",
      "opC6Min":"270",
      "opC7Std":null,
      "opC7Max":null,
      "opC7Min":null,
      "opC8Std":null,
      "opC8Max":null,
      "opC8Min":null,
      "opC9Std":null,
      "opC9Max":null,
      "opC9Min":null,
      "opCAStd":null,
      "opCAMax":null,
      "opCAMin":null,
      "opCBStd":null,
      "opCBMax":null,
      "opCBMin":null,
      "opCCStd":null,
      "opCCMax":null,
      "opCCMin":null,
      "opScrewRpmStd":"600",
      "opScrewRpmMax":"900",
      "opScrewRpmMin":"300",
      "opFeederRpmStd":null,
      "opFeederRpmMax":null,
      "opFeederRpmMin":null,
      "opWrenchRate":null,
      "opVacuumIndex":null,
      "opIronFQty":null,
      "opIronItemQty":null,
      "opBTempStd":null,
      "opBTempMax":null,
      "opBTempMin":null,
      "opSTempStd":"50",
      "opSTempMax":"60",
      "opSTempMin":"40",
      "opETempStd":null,
      "opETempMax":null,
      "opETempMin":null,
      "opScreenFreq":"12",
      "opRecordFreq":"2",
      "opMfgNotice":null,
      "opCdt":"2023/03/23 10:48:54",
      "opCreateBy":"A000067",
      "opUdt":"2023/03/23 10:48:55",
      "opUpdateBy":"A000067",
      "opPubDate":null,
      "opPublicBy":null,
      "opPublicName":null,
      "opRemark":"首次生產",
      "opEquipName":"染色少2",
      "opOrganizationId":"5000",
      "nonstopFlag":"N"
    }
  }
}
```
