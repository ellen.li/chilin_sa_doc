# 複材-圖面-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容             | 修改者 |
| -------- | ---------------- | ------ |
| 20230830 | 新增規格         | 黃東俞 |
| 20231102 | 回傳增加生產方式 | Ellen  |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /soc/pic_list               |
| method | post                        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位             |  名稱    |  資料型別 | 必填 | 資料儲存 & 說明  |
|------------------|---------|----------|:----:|-----------------|
| opOrganizationId | 工廠別   | string   |  M   | 5000:奇菱 / 6000:菱翔 |

#### Request 範例

```json
{
  "opOrganizationId": "5000"
}
```
# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 參考SQL-1取得資料並回傳。
* 生產方式 opMfgDesc 取 TEXT(SQL-1)前半角括號`[]`中的文字
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
-- SocMQueryDao.getSocMPicList
SELECT PARA_VALUE VALUE, PARA_TEXT TEXT
FROM SYS_PARA_D
WHERE PARA_ID = 'SOC_M_PIC'
AND PARA_CODE = { 工廠別 }
ORDER BY PARA_VALUE
```

# Response 欄位
| 欄位      | 名稱     | 資料型別 | 資料儲存 & 說明       |
| --------- | -------- | -------- | --------------------- |
| opValue   | 值       | string   | SYS_PARA_D.PARA_VALUE |
| opText    | 文字     | string   | SYS_PARA_D.PARA_TEXT  |
| opMfgDesc | 生產方式 | string   |                       |


#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":[
      {
        "opValue":"CLT_50MM_1.jpg",
        "opText":"[單管下料(#1)]CLT_50MM_1.jpg",
        "opMfgDesc":"單管下料(#1)"
      },
      {
        "opValue":"CLT_50MM_2.jpg",
        "opText":"[單管下料(#2)]CLT_50MM_2.jpg",
        "opMfgDesc":"單管下料(#2)"
      },
      {
        "opValue":"CLT_50MM_3.jpg",
        "opText":"[雙管下料(玻纖)]CLT_50MM_3.jpg",
        "opMfgDesc":"雙管下料(玻纖)"
      },
      {
        "opValue":"CLT_50MM_4.jpg",
        "opText":"[雙管下料(副料)]CLT_50MM_4.jpg",
        "opMfgDesc":"雙管下料(副料)"
      },
      {
        "opValue":"CLT_50MM_5.jpg",
        "opText":"[雙管下料(3通)]CLT_50MM_5.jpg",
        "opMfgDesc":"雙管下料(3通)"
      },
      {
        "opValue":"CLT_50MM_6.jpg",
        "opText":"[單管下料(吐氣口3)]CLT_50MM_6.jpg",
        "opMfgDesc":"單管下料(吐氣口3)"
      }
    ]
  }
}
```


