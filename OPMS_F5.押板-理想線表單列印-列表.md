# 押板-理想線表單列印-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230913 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /wo/sap_wo_list             |
| method | post                        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位              |  名稱             |  資料型別 | 必填 | 資料儲存 & 說明    |
|-------------------|------------------|----------|:----:|-------------------|
| opItemNo          | 料號              | string   |  O    |                  |
| opWoNo            | 工單號碼          | string   |  O    |                  |
| opBDate           | 開工日開始日期     | string   |  O    | yyyy/mm/dd       |
| opEDate           | 開工日結束日期     | string   |  O    | yyyy/mm/dd       |
| opBCompletionDate | 預計完工日開始日期 | string   |  O    | yyyy/mm/dd       |
| opECompletionDate | 預計完工日結束日期 | string   |  O    | yyyy/mm/dd       |
| opPage            | 第幾頁            | integer  |  O    | 預設 1，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推 |
| opPageSize        | 分頁筆數          | integer  |  O    | 預設 10                                                             |

#### Request 範例

```json
{
  "opItemNo": null,
  "opWoNo": null,
  "opBDate": "2019/01/01",
  "opEDate": "2019/12/31",
  "opBCompletionDate":null,
  "opECompletionDate":null,
  "opPage": 1,
  "opPageSize": 10
}
```

# Request 後端流程說明

* 取{登入者工廠別} SYS_USER.ORGANIZATION_ID 欄位
* opBDate, opEDate, opBCompletionDate, opECompletionDate 格式轉為 yyyymmdd 字串
* 將傳入資料帶入SAP-1取得所有回傳資料
  * 若未傳入opWoNo(工單號碼)時，則帶入"000053*"和"000059*"各取一次資料
  * 取回傳T_ORDER內的所有資料
* 有傳入opItemNo(料號)時，則依SAP-1回傳的料號欄位篩選符合的資料
* 最多只取前1000筆資料並進行分頁再回傳資料
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SAP-1：
參考舊版 WoQueryRfc.getWipEntitiesList

**獲得BAPI方法**
|              | 型態   | 參數值                      | 說明 |
| ------------ | ------ | -------------------------- | ---- |
| BAPIFunction | string | Z_PP_OPMS_PRODUCTION_ORDER |      |

**設定輸入參數**
| 參數名稱    | 參數值                     | 型態            | 說明                                   |
| ---------- | -------------------------- | -------------- | -------------------------------------- |
| I_WERKS    | {登入者工廠別}              | string         |                                        |
| I_AUFNR    | {opWoNo}                   | string         | 未傳入opWoNo時，則填"000053*"和"000059*" |
| I_ERDAT    |                            | JCoStructure   |                                        |
| I_GLTRP    |                            | JCoStructure   |                                        |

**設定輸入參數 I_ERDAT**
| 參數名稱    | 參數值                     | 型態            | 說明                                   |
| ---------- | -------------------------- | -------------- | -------------------------------------- |
| ERDAT_FR   | {opBDate}                  | string         | yyyymmdd                               |
| ERDAT_TO   | {opEDate}                  | string         | yyyymmdd                               |

**設定輸入參數 I_GLTRP**
| 參數名稱    | 參數值                     | 型態            | 說明                                   |
| ---------- | -------------------------- | -------------- | -------------------------------------- |
| GLTRP_FR   | {opBCompletionDate}        | string         | yyyymmdd                               |
| GLTRP_TO   | {opECompletionDate}        | string         | yyyymmdd                               |

| 取得回傳表格名稱 |
| ---------------|
| T_ORDER        |

| SAP欄位名稱  | SAP資料型別 | 欄位                    | 說明                             |
| ----------- | :---------: | ---------------------- | -------------------------------- |
| AUFNR       |   string    | 工單號碼 wipEntityName  | 過濾前綴0                         |
| MATNR       |   string    | 料號 assyItem               |                                  |
| GMEIN       |   string    | 單位 assyItemUom            |                                  |
| AUART_T     |   string    | 工單類別 classCode              |                                  |
| STATUS      |   string    | 工單狀態 jobStatus              |                                  |
| GAMNG       |   double    | 工單數量 startQty               | 取五位小數轉字串                   |
| LMNGA       |   double    | 已入庫量 quantityCompleted      | 取五位小數轉字串                   |
| GSTRP       |   date      | 開工日期 scheduledStartDate     | 轉字串 yyyy/mm/dd                 |
| GLTRP       |   date      | 預計完工 scheduledCompletionDate| 轉字串 yyyy/mm/dd                 |
| GLTRI       |   date      | 完工日期 dateCompleted          | 轉字串 yyyy/mm/dd                 |
| TECO_DATE   |   date      | 結案日期 dateClosed             | 轉字串 yyyy/mm/dd                 |

# Response 欄位
| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
| -------- | -------- | -------- | --------------- |
| pageInfo | 分頁資訊 | object   |                 |
| content  | 附帶訊息 | array    |                 |
#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明 |
| ---------- | ------------ | -------- | --------------- |
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |

#### 【content】array
| 欄位                      | 名稱     | 資料型別 | 資料儲存 & 說明                |
|---------------------------|---------|---------|-------------------------------|
| opOrganizationId          | 工廠別   | string  | {登入者工廠別}                 |
| opWipEntityName           | 工單號碼 | string  |                               |
| opAssyItem                | 料號     | string  |                               |
| opAssyItemUom             | 單位     | string  |                               |
| opClassCode               | 工單類別 | string  |                               |
| opJobStatus               | 工單狀態 | string  |                               |
| opStartQty                | 工單數量 | string  |                               |
| opQuantityCompleted       | 已入庫量 | string  |                               |
| opScheduledStartDate      | 開工日期 | string  | yyyy/mm/dd                    |
| opScheduledCompletionDate | 預計完工 | string  | yyyy/mm/dd                    |
| opDateCompleted           | 完工日期 | string  | yyyy/mm/dd                    |
| opDateClosed              | 結案日期 | string  | yyyy/mm/dd                    |


#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "pageInfo": {
      "pageNumber": 1,
      "pageSize": 10,
      "pages": 1,
      "total": 3
    },
    "content":[
      {
        "opOrganizationId":"5000",
        "opWipEntityName":"xxxxxxxxx",
        "opAssyItem":"xxxxxxxx",
        "opAssyItemUom":"xx",
        "opClassCode":"xxx",
        "opJobStatus":"xxx",
        "opStartQty":"1",
        "opQuantityCompleted":"1",
        "opScheduledStartDate":"2021/01/01",
        "opScheduledCompletionDate":"2021/01/02",
        "opDateCompleted":"2021/01/03",
        "opDateClosed":"2021/01/04",
      }
    ]
  }
}
```



