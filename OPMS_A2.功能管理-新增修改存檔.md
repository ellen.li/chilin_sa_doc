# 功能管理-新增修改存檔
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

功能管理-新增/修改存檔(此功能僅供外連報表使用)
PS.只剩新增有在使用，修改移到List 統一修改


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230406 | 新增規格 | Nick   |
 |20230512 | 調整訊息格式 增加【】一致 | Nick |
 |20230512 | 新增同時回寫更新時間(UDT..etc)方便排序 |Nick|
 

## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    | /function/save |
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位    | 名稱 | 資料型別 | 必填 | 資料儲存 & 說明 |
| ------- | ---- | :------: | :--: | --------------- |
|   opFCode |  功能代號     |  integer  |M| 新增模式 opFCode 為 `null`         |
|   opFName |  功能名稱     |  string  |M|          |
|   opUrl |  URL    | string |O|預設:`null`|

#### Request 範例

```json
{
   "opFCode":null,
   "opFName":"測試報表",
   "opUrl": "https://www.test.com.tw/ReportServer/XXXXX"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* {DATAKEY} = {opFCode}
* opFCode 非 null 則執行 SQL-1，確認功能代號是否已存在，如果不存在 [return 400,NOTFOUND]，存在繼續執行 SQL-2-1 更新功能資訊，{logContent}內容為 "修改 【{CNAME}】" 
* opFCode 為 null ，則取最大號 + 1 `MAX(CODE) + 1` 產生一組不存在 SYS_FUNCTION_V3 資料表的 CODE 並指定給 {opFCode}，並且執行 SQL-2-2 新增功能資訊，{logContent}內容為 "新增 【{opFName}】"
* 執行 SQL-3 紀錄LOG
  有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
SELECT CNAME FROM  SYS_FUNCTION_V3 WHERE CODE={opFCode}
```

SQL-2-1:
```sql
UPDATE SYS_FUNCTION_V3 SET 
                   CNAME={opFName},
                   URL={opUrl},
                   UDT=SYSDATETIME(),
                   UPDATE_BY={登入者使用者ID} 
WHERE CODE={opFCode}
```

SQL-2-2:
```sql
INSERT INTO SYS_ROLE_V3 (CODE,CNAME,LEVEL,PARENT,URL,VISIBLE,SORT_ID,CDT,CREATE_BY,UDT,UPDATE_BY)
              VALUES ({opFCode},{opFName},1,"0",{opUrl},1,0,SYSDATETIME(),{登入者使用者ID},SYSDATETIME(),{登入者使用者ID} )
```
      
SQL-3:
```sql
INSERT INTO  SYS_MODI_LOG (CATEGORY,DATAKEY,USERID,USER_NAME,LOG) VALUES ('FUNC',{DATAKEY},{登入者使用者ID},{登入者使用者名稱}, {logContent})
```


# Response 欄位
| 欄位         | 名稱         | 資料型別 | 資料儲存 & 說明          |
| ------------ | ------------ | -------- | ------------------------ |
|   opFCode  |  功能代號  |  string  |        |


## Response 範例

```json
{
  "msgCode": null, 
  "result":{
    "content": {
       "opFCode":"999"
    }
  }
}
```

