#共用 function -色料計算公式


## 修改歷程

| 修改時間 | 內容                                      | 修改者 |
|----------|-----------------------------------------|--------|
| 20230807 | 新增規格                                  | Ellen  |
| 20230915 | 修正pgmBatchQty, pgmRemainQty小數位數邏輯 | Ellen  |
| 20230927 | 1.2 倍邏輯有問題                          | Nick   |


## calculatePgm

calculatePgm(List<WoPgmTo> pgmList,List<WoMtrTo> mtrList,BigDecimal batchQty, BigDecimal batchTimes, 
			BigDecimal remainQty, BigDecimal remainTimes, BigDecimal n4,boolean userMultiplePgm)

### 參數 :
| 欄位            | 名稱                | 資料型別          | 資料儲存 & 說明 |
| --------------- | ------------------- | ----------------- | --------------- |
| pgmList         | 色料                | array 【WoPgmTo】 |                 |
| mtrList         | 原料                | array 【WoMtrTo】 |                 |
| batchQty        | 每Batch用量         | BigDecimal        |                 |
| batchTimes      | Batch用量次數       | BigDecimal        |                 |
| remainQty       | 尾數量              | BigDecimal        |                 |
| remainTimes     | 尾數量次數          | BigDecimal        |                 |
| n4              | n4用量              | BigDecimal        |                 |
| userMultiplePgm | 是否以1.2倍色粉列印 | boolean           |                 |

【WoPgmTo】
| 欄位          | 名稱            | 資料型別 | 資料儲存 & 說明    |
| ------------- | --------------- | -------- | ------------------ |
| woNo          | 工單號碼        | string   |                    |
| woPgmPartNo   | 色粉料號        | string   |                    |
| quantityPer   | 單位用量        | string   |                    |
| unit          | 單位            | string   |                    |
| sitePick      | 是否為現場領料  | string   | Y/N  Y: 是 / N: 否 |
| selfPick      | 是否為自秤      | string   | Y/N  Y: 是 / N: 否 |
| pgmBatchQty   | 色粉每Batch用量 | string   |                    |
| pgmRemainQty  | 色粉尾數量      | string   |                    |
| pgmQty        | 色粉用量        | string   |                    |
| modifyFlag    | 補正碼          | string   |                    |
| modifyQty     | 補正紀錄量      | string   |                    |
| modifyUnitQty | 單位補正紀錄量  | string   |                    |
| pgmBomUid     | Bom ID          | string   |                    |
| feederNo      | 下料設備        | string   |                    |
| feederRate    | 下料比率        | string   |                    |
| feederSeq     | 下料順序        | string   |                    |


【WoMtrTo】
| 欄位        | 名稱     | 資料型別 | 資料儲存 & 說明 |
| ----------- | -------- | -------- | --------------- |
| woNo        | 工單號碼 |  string   |                 |
| woMtrPartNo | 原料料號 |  string   |                 |
| quantityPer | 單位用量 |  string   |                 |
| unit        | 單位     |  string   |                 |
| feederNo    | 下料設備 |  string   |                 |
| feederRate  | 下料比率 |  string   |                 |


### 回傳欄位:

| 欄位                | 名稱                | 資料型別          | 資料儲存 & 說明 |
| ------------------- | ------------------- | ----------------- | --------------- |
| multiplePgm         | 是否為1.2倍色粉      | boolean           |                 |
| n4BatchQty          | N4 Batch總用量      | BigDecimal        |                 |
| n4RemainQty         | N4尾數總量          | BigDecimal        |                 |
| n4TotalQty          | N4色粉總重          | BigDecimal        |                 |
| pgmTotalBatchQty    | Batch總用量         | BigDecimal        |                 |
| pgmTotalRemainQty   | 尾數總量            | BigDecimal        |                 |
| pgmTotalQuantityPer | 色粉總重            | BigDecimal        |                 |
| labelCount          | Batch色粉包數       | int               |                 |
| labelRemainCount    | 尾數色粉包數        | int               |                 |
| woPgmList           |                     | array 【WoPgmTo】 |                 |


# 後端流程說明

| 【清單1】    |
| ------------ |
| 05-251-A     |
| 04-251-A     |
| SMAXXXXXX1IX |

| 【清單2】     |
| ------------- |
| 05-251-A-L    |
| 05-251-A-77   |
| 05-251-A-6500 |
| 04-251-A-L    |
| 04-251-A-77   |
| 04-251-A-6500 |

| 【清單3】     |
| ------------- |
| 05-251-A-6500 |
| 05-251-A-6321 |
| 05-251-A-332  |
| 05-251-A-455  |
| 05-251-A-1112 |
| 04-251-A-6500 |
| 04-251-A-6321 |
| 04-251-A-332  |
| 04-251-A-455  |
| 04-251-A-1112 |

【單位色粉重量表】
| 每包重量 | 料號                                                                                      |
| -------- | ----------------------------------------------------------------------------------------- |
| 20kg     | 05-251-A-1112, 04-251-A-1112, 05-201-W-02, LMB01XXX1XBXXXXXXX, 04-201-W-02, B002MB-01-01L |
| 10kg     | 05-201-W-326, 04-201-W-326                                                                |
| 12kg     | 05-201-W-594, 04-201-W-594                                                                |
| 25kg     | 05-251-A-6500, 05-251-A-6321, 05-251-A-332, 05-251-A-455, 04-251-A-6500, 04-251-A-6321, 04-251-A-332, 04-251-A-455, 05-201-W, LMB64XXX1XBXXXXXXX, 04-201-W, B002MB-64-01L |


**Step 1 色粉處理**
* 新增變數{isW}：是否含W-83單位用量<=80G，預設false
* 新增變數{isL12}：是否含L-12，預設false
* 新增變數{wQuantityPer}：W-83用量 ，預設0
* 遍歷 {pgmList}
  * 若色料物件單位{unit}不為空且為「KG」開頭，將{unit}轉換為"G"，更新色粉{quantityPer}, {pgmBatchQty}, {pgmRemainQty}乘以1000計算。
  * 若色料物件使用色粉料號含有 「W-83」 且 {quantityPer} <= 80G，則註記{isW}為 true，{quantityPer}加總入{wQuantityPer}
  * 若色料物件使用色粉料號含有 「A-L12」，則註記{isL12}為 true
* 若{isW}等於true且{isL12}等於 true，則註記{multiplePgm}為 true

**Step 2 色粉計算**
* 新增變數{totalBatchAQty}：每Batch助劑總重
* 新增變數{totalBatchBQty}：每Batch一般色料總重
* 新增變數{totalRemainAQty}：尾數助劑總重
* 新增變數{totalRemainBQty}：尾數一般色料總重
* 遍歷 {pgmList}
  * 更新色粉{pgmQty}值為 `({pgmBatchQty} * {batchTimes}) + ({pgmRemainQty} * {remainTimes})`
  * 若色粉{SelfPick}不等於"M"，計算色粉及助劑Batch重
    * 如色粉料號開頭存在【清單1】且不存在【清單2】，則註記【是否為助劑(isA)】為 true
    * 若色粉料號為【清單3】開頭，餘數要另包成一包色粉，不與其他色粉包在一起，不加總至色粉batch總重
      * 計算每Batch餘數：色粉每Batch用量除以每包重量取餘數，若餘數大於0，{labelCount}加1
      * 計算尾數量餘數：色粉尾數量除以每包重量取餘數，若{remainTimes}等於0，尾數量餘數為0，若餘數大於0，{labelRemainCount}加1
    * 若否:
      * 計算每Batch餘數：色粉每Batch用量除以每包重量取餘數，若【是否為助劑(isA)】為 true，加總入{totalBatchAQty}；否則加總入{totalBatchBQty}
      * 計算尾數量餘數：色粉尾數量除以每包重量取餘數，若{remainTimes}等於0，尾數量餘數為0，若【是否為助劑(isA)】為 true，加總入{totalRemainAQty}；否則加總入{totalRemainBQty}
  * 若為1.2倍色粉，且user指定以1.2倍色粉列印，則W-83色粉Batch用量{pgmBatchQty}及尾數用量{pgmRemainQty}乘以1.2
  * 若為1.2倍色粉，且user指定以1.2倍色粉列印，則L-12色粉Batch用量{pgmBatchQty}減去W-83多用的部份`{wQuantityPer} * {batchQty} * 0.2`,減至0為止
  * 若為1.2倍色粉，且user指定以1.2倍色粉列印，則L-12色粉尾數用量{pgmRemainQty}減去W-83多用的部份`{wQuantityPer} * {remainQty} * 0.2`,減至0為止
  * 色粉Batch用量{pgmBatchQty}加總入{pgmTotalBatchQty}
  * 色粉尾數用量{pgmRemainQty}加總入{pgmTotalRemainQty}
  * 色粉單位用量{quantityPer}加總入{pgmTotalQuantityPer}
  * 更新色粉{quantityPer}值，取至小數後第五位
  * 更新色粉{pgmBatchQty}值，若小於1取至小數後第三位，若小於10取至小數後第二位，若小於100取至小數後第一位，其餘取至整數，需判斷不少於前面小數位數
  * 更新色粉{pgmRemainQty}值，若小於1取至小數後第三位，若小於10取至小數後第二位，若小於100取至小數後第一位，其餘取至整數，需判斷不少於前面小數位數
  * 更新色粉{pgmQty}值，取至小數後第三位


**Step 3**
* 若{n4}為空，以 {pgmList}、{mtrList} 參考 [OPMS_共用_N4計算公式](./OPMS_共用_N4計算公式.md) 計算N4值
* {n4BatchQty}等於 `{n4} * {batchQty}`
* {n4RemainQty}等於 `{n4} * {remainQty} * {remainTimes}`
* {n4TotalQty}等於 `({n4} * {batchQty} * {batchTimes}) + ( {n4} * {remainQty} * {remainTimes} )`
* 計算Batch色粉包數{labelCount}
  * 若{totalBatchAQty}大於0，{labelCount}加1
  * 若{totalBatchBQty}大於0，{labelCount}加1，若{totalBatchBQty}大於21.5kg再加1
* 計算尾數色粉包數{labelRemainCount}
  * 若{totalRemainAQty}大於0，{labelRemainCount}加1
  * 若{totalRemainBQty}大於0，{labelRemainCount}加1，若{totalBatchBQty}大於21.5kg再加1