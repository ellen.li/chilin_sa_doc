# 使用者權限管理-新增修改存檔
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

使用者權限管理-新增/修改存檔


## 修改歷程

| 修改時間 | 內容                               | 修改者 |
|----------|------------------------------------|--------|
| 20230405 | 新增規格                           | Nick   |
| 20230510 | add opProdunitNoList               | Nick   |
| 20230517 | SYS_USER_ROLE 改 SYS_USER_ROLE_V3 | Nick   |

## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    | /user/save |
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位    | 名稱 | 資料型別 | 必填 | 資料儲存 & 說明 |
| ------- | ---- | :------: | :--: | --------------- |
|   opMode |  編輯模式     |  string  |M|  ADD:新增/ MODI:修改        |
|   opLdapSite |  LDAP ID     |  string  |M|          |
|   opUserId |  使用者ID     |  string  |M|          |
|   opUserName | 使用者名稱      |  string|M  |          |
|   opEmail |  email     |  string  |O| 預設`null`         |
|   opOrgId |  工廠別     |  string  |M|          |
|   opPwd   |  密碼     |  string  |M|          |
|   opActive | 啟用      |  string  |M|          |
|   opRoleIdList |  使用者角色ID清單    |array integer|M | |
|   opProdunitNoList  | 生產單位代號清單 | array string|M| |

#### Request 範例

```json
{
  "msgCode":"",
  "result":{
    "content":
      {
        "opMode":"ADD",
        "opLdapSite":"NA",
        "opUserId":"richman",
        "opUserName":"richman",
        "opEmail":"xxx@test.com",
        "opOrgId":"5000",
        "opPwd":"12345",
        "opActive":"1",
        "opRoleIdList": [601,932,101,221],
        "opProdunitNoList":[
          {"opProdunitNo:":"001","opOrganizationId":"5000"},
          {"opProdunitNo:":"002","opOrganizationId":"5000"}
          ]
      }
  }
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* {DATAKEY} = {opUserId}
* 如 {opLdapSite} 為 "NA"，儲存資料時 LDAP_SITE 存入 null 值，否則 LDAP_SITE = {opLdapSite}
* 執行  SQL-1 確認該使用者存在，若使用者存在，且 opMode 為 ADD，[return 400,DUPLICATED]，若使用者不存在，且 opMode 為 MODI，[return 400,NOTFOUND]
* {opPwd} 如為 null 代表密碼不需變動，非 null 才需要更新密碼欄位
* {opPwd} 非 null，使用 RSA 私鑰解密後，重新使用TEA 演算法加密，並指定成{password}
* 執行 SQL-2 刪除原有使用者角色清單
* 執行 SQL-3 將 opRoleIdList 成員，新增至使用者角色清單
* opMode 為 ADD 則執行 SQL-4-1 新增使用者資訊，{logContent}內容為 "新增"
* opMode 為 MODI 則執行 SQL-4-2 更新使用者資訊，{logContent}內容為 "修改" 
* 執行 SQL-5 刪除原有使用者生產單位對應表
* 執行 SQL-6 將 opProdunitNoList 成員，新增至使用者生產單位對應表內
* 執行 SQL-7 紀錄LOG
  有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
SELECT * FROM  SYS_USER WHERE USER_ID={opUserId}
```
SQL-2:
```sql
DELETE FROM SYS_USER_ROLE_V3 WHERE USER_ID={opUserId}
```

SQL-3:
```sql
此SQL為範例，實際SQL請自己按照 opRoleIdList 內容

INSERT INTO SYS_USER_ROLE_V3(USER_ID,ROLE_ID,CDT,CREATE_BY)
              VALUES({opUserId},{opRoleId},SYSDATETIME(),{登入者使用者ID} )
```

SQL-4-1:
```sql
INSERT INTO SYS_USER (USER_ID,LDAP_SITE,USER_NAME,ACTIVE,PASSWORD,ORGANIZATION_ID,EMAIL,CDT,CREATE_BY)
              VALUES ({opUserId},{opLdapSite},{opUserName},{opActive},{password},{opOrgId},{opEmail},SYSDATETIME(),{登入者使用者ID} )
```



SQL-4-1:
```sql
UPDATE SYS_USER SET LDAP_SITE={opLdapSite},
                    USER_NAME={opUserName},
                    ACTIVE={opActive},
                    PASSWORD={password}, --PASSWORD 依照前端是否為NULL決定是否寫入
                    ORGANIZATION_ID={opOrgId},
                    EMAIL={opEmail},
                    UDT=SYSDATETIME(),
                    UPDATE_BY={登入者使用者ID} 
                WHERE USER_ID={opUserId}    
```

SQL-5:
```sql
DELETE FROM SYS_USER_PRODUNIT WHERE USER_ID={opUserId}
```

SQL-6:
```sql
INSERT INTO SYS_USER_PRODUNIT
           (USER_ID,PRODUNIT_NO,ORGANIZATION_ID,CDT,CREATE_BY,UDT,UPDATE_BY)
     VALUES
           ({opUserId},{opProdunitNo[]},{opOrganizationId[]},SYSDATETIME(),{登入者使用者ID},SYSDATETIME(),{登入者使用者ID})
```



SQL-7:
```sql
INSERT INTO  SYS_MODI_LOG (CATEGORY,DATAKEY,USERID,USER_NAME,LOG) VALUES ('USER',{DATAKEY},{登入者使用者ID},{登入者使用者名稱},{logContent})
```


# Response 欄位
| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
|----------|--------|----------|-----------------|
| opUserId | 使用者ID | string   |                 |


## Response 範例

```json
{
  "msgCode": null, 
  "result":{
    "content": {
       "opUserId":"richman"
    }
  }
}
```

