# BOM維護-取得單一BOM下料設備與比例明細資料
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得單一BOM下料設備與比例明細資料

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230526 | 新增規格 | Nick   |
| 20230731 | 根據料號ASC排序 | Sam   |

## 來源URL及資料格式

| 項目   | 說明                |
|--------|---------------------|
| URL    | /bom/get_detail_one |
| method | post                |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位        | 名稱        | 資料型別 | 必填 | 資料儲存 & 說明 |
|-------------|-------------|:--------:|:----:|-----------------|
| opBomUid    | BOM ID      |  string  |  M   |                 |
| opBomFedUid | BOM_FED_UID |  string  |  M   |                 |

#### Request 範例

```json
{
   "opBomUid":"5000-LFG0610X1XBX0610BX01",
   "opBomFedUid":"2A9D309B-A0F8-4765-A87C-54FE467B15D3"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 執行  SQL-1 確認可正常查詢到資料，找不到 return 400,"NOTFOUND"
* 執行  SQL-2 找出所有下料設備與比例明細，無資料請給 null，ex: "opBomFedDList":null
* 組出 JSON
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容


# Request 後端邏輯說明


SQL-1:
```sql
SELECT A.BOM_UID, A.BOM_FED_UID, A.EQUIP_ID, A.F_1_RATE, A.F_2_RATE, A.F_3_RATE, A.F_4_RATE, A.F_5_RATE, A.F_6_RATE, A.BDP_RATE, A.PPG_RATE, A.DBE_RATE, REPLACE(CONVERT(VARCHAR(19),A.UDT,120),'-','/') UDT, 
A.UPDATE_BY,E.EQUIP_NAME,E.PRODUNIT_NO,P.PRODUNIT_NAME  FROM BOM_FED A  
LEFT JOIN EQUIP_H E ON A.EQUIP_ID=E.EQUIP_ID  LEFT JOIN PRODUNIT P ON E.ORGANIZATION_ID=P.ORGANIZATION_ID AND E.PRODUNIT_NO=P.PRODUNIT_NO  WHERE A.BOM_UID={opBomUid} AND A.BOM_FED_UID={opBomFedUid}
```

SQL-2:
```sql
SELECT BOM_UID, BOM_FED_UID, BOM_PART_NO, FEEDER_NO, FEEDER_RATE, REPLACE(CONVERT(VARCHAR(19),UDT,120),'-','/') UDT, UPDATE_BY FROM BOM_FED_D 
WHERE BOM_UID={opBomUid} AND BOM_FED_UID={opBomFedUid} 
ORDER BY BOM_PART_NO ASC
```


# Response 欄位
| 欄位           | 名稱                   | 資料型別 | 資料儲存 & 說明         |
|----------------|------------------------|----------|-------------------------|
| opBomUid       | BOM ID                 | string   | B.BOM_UID(SQL-1)        |
| opBomFedUid    | BOM_FED_UID            | string   | A.BOM_FED_UID (SQL-1)   |
| opProdUnitName | 生產單位               | string   | P.PRODUNIT_NAME (SQL-1) |
| opEquipName    | 機台名稱               | string   | E.EQUIP_NAME (SQL-1)    |
| opF1Rate       | F1                     | string   | A.F_1_RATE (SQL-1)      |
| opF2Rate       | F2                     | string   | A.F_2_RATE (SQL-1)      |
| opF3Rate       | F3                     | string   | A.F_3_RATE (SQL-1)      |
| opF4Rate       | F4                     | string   | A.F_4_RATE (SQL-1)      |
| opF5Rate       | F5                     | string   | A.F_5_RATE (SQL-1)      |
| opF6Rate       | F6                     | string   | A.F_6_RATE (SQL-1)      |
| opBDP          | BDP                    | string   | A.BDP_RATE (SQL-1)      |
| opPPG          | PPG                    | string   | A.PPG_RATE (SQL-1)      |
| opDBE          | DBE                    | string   | A.DBE_RATE (SQL-1)      |
| opBomFedDList  | 下料比例與下料設備明細 | array    |                         |

【 opBomFedDList array object】
| 欄位         | 名稱     | 資料型別 | 來源資料 & 說明     |
|--------------|--------|----------|---------------------|
| opBomPartNo  | 料號     | string   | BOM_PART_NO (SQL-2) |
| opFeederNo   | 下料設備 | string   | FEEDER_NO (SQL-2)   |
| opFeederRate | 比例     | string   | FEEDER_RATE (SQL-2) |


#### Response 範例

```json
{
  "msgCode":"",
  "result":{
    "content":
      {
       "opBomUid":"5000-LFG0610X1XBX0610BX01",
       "opBomFedUid":"2A9D309B-A0F8-4765-A87C-54FE467B15D3",
       "opProdUnitName":"複材課",
       "opEquipName": null,
       "opF1Rate":"90.02",
       "opF2Rate":"9.98",
       "opF3Rate":"0.00",
       "opF4Rate":"0.00",
       "opF5Rate":"0.00",
       "opF6Rate":"0.00",
       "opBDP":"0.00",
       "opPPG":"0.00",
       "opDBE":"0.00",
       "opBomFedDList":[
          {"opBomPartNo":"05-251-A-L091XXXXX","opFeederNo":"F1-混合桶","opFeederRate":"0.0885"},
          {"opBomPartNo":"05-251-A-O68XXXXXX","opFeederNo":"F1-混合桶","opFeederRate":"0.0885"},
          {"opBomPartNo":"05-251-A-R3750XXXX","opFeederNo":"F1-混合桶","opFeederRate":"0.1770"},
          {"opBomPartNo":"05-251-A-REF42XXXX","opFeederNo":"F1-混合桶","opFeederRate":"0.0443"},
          {"opBomPartNo":"06-105-CC-MD1500XX","opFeederNo":"F1-混合桶","opFeederRate":"2.6556"},
          {"opBomPartNo":"06-114-JF-T511XXXX","opFeederNo":"F2-不混合","opFeederRate":"9.9761"},
          {"opBomPartNo":"110XXXXXX1BX","opFeederNo":"F1-混合桶","opFeederRate":"61.9634"},
          {"opBomPartNo":"175XXXXXX1BX","opFeederNo":"F1-混合桶","opFeederRate":"23.9002"},
          {"opBomPartNo":"LMB40PCX1XBXXXXXXX","opFeederNo":"F1-混合桶","opFeederRate":"1.1065"} 
       ]
      }
  }
}
```

