# 生產進度-取得生產進度
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

生產進度-取得生產進度

## 修改歷程

| 修改時間 | 內容                   | 修改者 |
| -------- | ---------------------- | ------ |
| 20231003 | 新增規格               | Ellen  |
| 20231020 | 調整SQL避免原料重複    | Ellen  |
| 20231023 | 修正產能回報sql排序    | Ellen  |
| 20231026 | 不同機台排班時間不累計 | Ellen  |
| 20231122 | 產量累加只累加同機台   | Ellen  |


## 來源URL及資料格式

| 項目   | 說明                    |
| ------ | ----------------------- |
| URL    | /ps/mfg_schedule_kanban |
| method | post                    |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         | 名稱         | 資料型別 | 必填  | 資料儲存 & 說明 |
| ------------ | ------------ | :------: | :---: | --------------- |
| opQueryDateS | 開始日期     |  string  |   M   | yyyy/MM/dd      |
| opQueryDateE | 結束日期     |  string  |   M   | yyyy/MM/dd      |
| opProdunitNo | 生產單位代碼 |  string  |   M   |                 |

#### Request 範例

```json
{
  "opQueryDateS":"2023/09/18",  
  "opQueryDateE":"2023/09/24",    
  "opProdunitNo":"P3"
}
```

# Request 後端流程說明

可參考舊版 PsMfgProgress.jsp
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* {organizationId} 為登入者工廠別ID
* 執行 SQL-1 取得生產排程
* 執行 SQL-2 取得產能回報
* 先後遍歷 SQL-1 及 SQL-2 結果，組成Response內容
  * 開始時間{opStartTime}: 
    * SQL-1: 參考生產日期及班別起始時間，累加同機台同班別時數 ⚠️注意晚班可能跨日
    * SQL-2: 由REP_TIME_LINE取得起始時間，若未填預設班別起始時間 ⚠️注意晚班可能跨日
  * 結束時間{opEndTime}: 
    * SQL-1: {opStartTime} 加上 SHIFT_HOURS(SQL-1)
    * SQL-2: {opStartTime} 加上 REP_SHIFT_HOURS(SQL-2)
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

班別
* 三班制，早班為`8:00~16:00`，中班`16:00~24:00`，晚班:隔日`0:00~8:00`

# Request 後端邏輯說明

SQL-1: 取得生產排程
```sql
SELECT
  H.PS_UID,
  H.PS_SEQ,
  W.WO_UID,
  W.PATTERN_NO,
  W.WO_MTR_PART_NO MTR_NAME,
  W.QUANTITY_PER,
  CONVERT (VARCHAR (10), H.MFG_DATE, 111) MFG_DATE,
  H.EQUIP_ID,
  E.EQUIP_NAME,
  H.SHIFT_CODE,
  W.WO_NO,
  H.STOP_DESC,
  W.ITEM_NO,
  H.SHIFT_HOURS,
  W.SALES_QTY WO_QTY,
  H.SHIFT_STD_QTY,
  H.SHIFT_PLN_QTY,
  R.SHIFT_MFG_QTY,
  R.REP_SHIFT_HOURS
FROM
  PS_H H
  INNER JOIN EQUIP_H E ON H.EQUIP_ID = E.EQUIP_ID
  INNER JOIN PRODUNIT U ON E.ORGANIZATION_ID = U.ORGANIZATION_ID 
  AND E.PRODUNIT_NO = U.PRODUNIT_NO
  OUTER APPLY (
    SELECT TOP 1
      WW.WO_UID,
      WW.WO_NO,
      BB.ITEM_NO,
      II.PATTERN_NO,
      WW.CTN_WEIGHT,
      MM.WO_MTR_PART_NO,
      MM.QUANTITY_PER,
      WW.SALES_QTY 
    FROM
      WO_H WW
      INNER JOIN BOM_MTM BB ON WW.BOM_UID = BB.BOM_UID
      INNER JOIN ITEM_H II ON BB.ITEM_NO = II.ITEM_NO
      LEFT JOIN WO_MTR MM ON WW.WO_UID = MM.WO_UID 
    WHERE
      H.WO_UID = WW.WO_UID
  )W
  LEFT JOIN (
    SELECT
      RR.REP_MFG_DATE,
      RR.REP_SHIFT_CODE,
      RR.EQUIP_ID,
      RR.WO_UID,
      SUM(RR.SHIFT_MFG_QTY) SHIFT_MFG_QTY,
      SUM(RR.REP_SHIFT_HOURS) REP_SHIFT_HOURS 
    FROM
      PS_R RR 
    WHERE
      RR.REP_MFG_DATE >= CONVERT(DATETIME, {opQueryDateS})
      AND RR.REP_MFG_DATE <= CONVERT(DATETIME, {opQueryDateE})
    GROUP BY
      RR.REP_MFG_DATE,
      RR.REP_SHIFT_CODE,
      RR.EQUIP_ID,
      RR.WO_UID 
)R ON H.MFG_DATE = R.REP_MFG_DATE AND H.EQUIP_ID = R.EQUIP_ID
  AND H.SHIFT_CODE = R.REP_SHIFT_CODE 
  AND H.WO_UID = R.WO_UID 
WHERE
  H.MFG_DATE >= CONVERT (DATETIME, {opQueryDateS})
  AND H.MFG_DATE <= CONVERT (DATETIME, {opQueryDateE})
  AND E.ORGANIZATION_ID = {organizationId}
  AND E.PRODUNIT_NO = {opProdunitNo}
ORDER BY
  H.EQUIP_ID,
  H.MFG_DATE,
  H.SHIFT_CODE,
  H.PS_SEQ,
  W.WO_NO,
  W.WO_UID,
  W.QUANTITY_PER DESC
```

SQL-2: 取得產能回報
```sql
SELECT
  R.PSR_UID,
  W.WO_UID,
  W.PATTERN_NO,
  W.MTR_NAME,
  R.EQUIP_ID,
  E.EQUIP_NAME,
  W.WO_NO,
  W.ITEM_NO,
  W.WO_QTY,
  R.SHIFT_MFG_QTY,
  R.SHIFT_PRE_QTY,
  CONVERT (VARCHAR (10), R.REP_MFG_DATE, 111) AS REP_MFG_DATE,
  R.REP_SHIFT_CODE,
  R.REP_SHIFT_HOURS,
  R.WRN_FLAG,
  R.WRN_REMARK,
  R.REP_TIME_LINE,
  P.SHIFT_PLN_QTY,
  CASE WHEN R.REP_SHIFT_CODE='C' AND R.REP_TIME_LINE>'12' THEN '0' ELSE '1' END AS TIME_ORDER
FROM
  PS_R R
  INNER JOIN EQUIP_H E ON R.EQUIP_ID = E.EQUIP_ID
  INNER JOIN PRODUNIT U ON E.ORGANIZATION_ID = U.ORGANIZATION_ID AND E.PRODUNIT_NO = U.PRODUNIT_NO
  LEFT JOIN (
    SELECT
      WW.WO_UID,
      WW.WO_NO,
      BB.ITEM_NO,
      II.PATTERN_NO,
      II.MTR_NAME,
      WW.CTN_WEIGHT,
      WW.DD_MFG,
      WW.SALES_QTY WO_QTY 
    FROM
      WO_H WW
      INNER JOIN BOM_MTM BB ON WW.BOM_UID = BB.BOM_UID
      INNER JOIN ITEM_H II ON BB.ITEM_NO = II.ITEM_NO 
  ) W ON R.WO_UID = W.WO_UID
  LEFT JOIN (
    SELECT
      PS.MFG_DATE,
      PS.SHIFT_CODE,
      PS.EQUIP_ID,
      PS.WO_UID,
      SUM(PS.SHIFT_PLN_QTY) SHIFT_PLN_QTY 
    FROM
      PS_H PS
    WHERE
      PS.MFG_DATE >= CONVERT (DATETIME, {opQueryDateS}) 
      AND PS.MFG_DATE <= CONVERT (DATETIME, {opQueryDateE}) 
      AND EXISTS (
        SELECT * FROM PS_R RR 
        WHERE 
        PS.MFG_DATE = RR.REP_MFG_DATE 
        AND PS.SHIFT_CODE = RR.REP_SHIFT_CODE 
        AND PS.EQUIP_ID = RR.EQUIP_ID
        AND PS.WO_UID = RR.WO_UID 
      )
    GROUP BY
      PS.MFG_DATE,
      PS.SHIFT_CODE,
      PS.EQUIP_ID,
      PS.WO_UID 
  ) P ON R.REP_MFG_DATE = P.MFG_DATE  AND R.EQUIP_ID = P.EQUIP_ID
  AND R.REP_SHIFT_CODE = P.SHIFT_CODE 
  AND R.WO_UID = P.WO_UID 
WHERE
  R.REP_MFG_DATE >= CONVERT (DATETIME, {opQueryDateS}) 
  AND R.REP_MFG_DATE <= CONVERT (DATETIME, {opQueryDateE})
  AND E.PRODUNIT_NO = {opProdunitNo}
ORDER BY
  R.EQUIP_ID,
  REP_MFG_DATE,
  R.REP_SHIFT_CODE,
  TIME_ORDER,
  R.REP_TIME_LINE,
  W.WO_NO
```

# Response 欄位
| 欄位            | 名稱           | 資料型別 | 來源資料 & 說明                                                    |
| --------------- | -------------- | -------- | ------------------------------------------------------------------ |
| opPsUid         | 生產排程UID    | string   | PS_UID(SQL-1) 或 null                                              |
| opPsSeq         | 序號           | string   | PS_SEQ(SQL-1) 或 null                                              |
| opPsrUid        | 產量回報UID    | string   | PSR_UID(SQL-2) 或 null                                             |
| opEquipId       | 機台代號       | string   | EQUIP_ID(SQL-1) 或 EQUIP_ID(SQL-2)                                 |
| opEquipName     | 機台名稱       | string   | EQUIP_NAME(SQL-1) 或 EQUIP_NAME(SQL-2)                             |
| opMfgDate       | 生產日期       | string   | MFG_DATE(SQL-1) 或 REP_MFG_DATE(SQL-2)                             |
| opStartTime     | 開始時間       | string   | yyyy/MM/dd hh:mm:ss                                                |
| opEndTime       | 結束時間       | string   | yyyy/MM/dd hh:mm:ss                                                |
| opItemNo        | 料號           | string   | ITEM_NO(SQL-1) 或 ITEM_NO(SQL-2)                                   |
| opPatternNo     | 色號           | string   | PATTERN_NO(SQL-1) 或 PATTERN_NO(SQL-2)                             |
| opMtrName       | 原料           | string   | MTR_NAME(SQL-1) 或 MTR_NAME(SQL-2)                                 |
| opShiftPlnQty   | 預計產量       | string   | SHIFT_PLN_QTY(SQL-1) 或 null                                       |
| opShiftMfgQty   | 實際產量       | string   | SHIFT_MFG_QTY(SQL-1) 或 SHIFT_MFG_QTY(SQL-2)                       |
| opShiftCode     | 班別代碼       | string   | SHIFT_CODE(SQL-1) 或 REP_SHIFT_CODE(SQL-2)                         |
| opShiftHours    | 時數           | string   | SHIFT_HOURS(SQL-1) 或 null                                         |
| opRepShiftHours | 實際回報時數   | string   | REP_SHIFT_HOURS(SQL-1) 或 REP_SHIFT_HOURS(SQL-2)                   |
| opKpiRate       | 達成比率       | string   | `SHIFT_MFG_QTY(SQL-1) / SHIFT_PLN_QTY(SQL-1)`取至小數第2位 或 null |
| opWoUid         | 工單UID        | string   | WO_UID(SQL-1) 或 WO_UID(SQL-2)                                     |
| opWoNo          | 工單號碼       | string   | WO_NO(SQL-1) 或 WO_NO(SQL-2)                                       |
| opWoQty         | 工單數量       | string   | WO_QTY(SQL-1) 或 WO_QTY(SQL-2)                                     |
| opStopDesc      | 停車原因       | string   | STOP_DESC(SQL-1) 或 null                                           |
| opIsRealData    | 是否為實際回報 | string   | Y/N  Y: 是 / N: 否  ， SQL-1為`N`，SQL-2為`Y`                      |
| opWrnRemark     | 異常回報原因   | string   | WRN_REMARK(SQL-2) 或 null                                          |


#### Response 範例

```json
{
  "msgCode": null,
  "result": {
    "content":[
      // 預排
      {
        "opPsUid": "8C74D877-9BB3-4FDE-9E3B-F6F6E75C2AD0",
        "opPsSeq": "0",
        "opPsrUid": null,
        "opEquipId": "1",
        "opEquipName": "染色#1",
        "opMfgDate": "2023/09/18",
        "opStartTime": "2023/09/19 04:00:00",
        "opEndTime": "2023/09/19 07:00:00",
        "opItemNo": "LPA747TAE15606",
        "opShiftPlnQty": "1000",
        "opShiftMfgQty": "1330.4",
        "opShiftCode": "C",
        "opShiftHours": "3",
        "opRepShiftHours": "6",
        "opKpiRate": "1.33",
        "opMtrName": "747XXXXXX1BX",
        "opWoNo": "51029842",
        "opWoQty": "6000",
        "opPatternNo": "H95429B5",
        "opStopDesc": null,
        "opIsRealData": "N",
        "opWrnRemark": null
      },
      // 預排 - 停車
      {
        "opPsUid": "00035976-CE15-48A4-ACC0-20CB9D5D0EBA",
        "opPsSeq": "0",
        "opPsrUid": null,
        "opEquipId": "1",
        "opEquipName": "染色#1",
        "opMfgDate": "2023/09/18",
        "opStartTime": "2023/09/19 07:00:00",
        "opEndTime": "2023/09/19 08:00:00",
        "opItemNo": "LPA747TAE15606",
        "opShiftPlnQty": null,
        "opShiftMfgQty": null,
        "opShiftCode": "C",
        "opShiftHours": "1",
        "opRepShiftHours": null,
        "opKpiRate": null,
        "opMtrName": null,
        "opWoNo": null,
        "opWoQty": null,
        "opPatternNo": null,
        "opStopDesc": "計畫性停車",
        "opIsRealData": "N",
        "opWrnRemark": null
      },
      // 實際
      {
        "opPsUid": null,
        "opPsSeq": null,
        "opPsrUid": "7874C162-D5DB-43E2-BDCB-2507232AFD1B",
        "opEquipId": "1",
        "opEquipName": "染色#1",
        "opMfgDate": "2023/09/18",
        "opStartTime": "2023/09/18 08:00:00",
        "opEndTime": "2023/09/18 10:00:00",
        "opItemNo": "LPA747TAE15606",
        "opShiftPlnQty": null,
        "opShiftMfgQty": "880",
        "opShiftCode": "A",
        "opShiftHours": null,
        "opRepShiftHours": "2",
        "opKpiRate": null,
        "opMtrName": "747XXXXXX1BX",
        "opWoNo": "51029842",
        "opWoQty": "6000",
        "opPatternNo": "H95429B5",
        "opStopDesc": null,
        "opIsRealData": "Y",
        "opWrnRemark": null
      },
      // 實際 - 停車
      {
        "opPsUid": null,
        "opPsSeq": null,
        "opPsrUid": "7874C162-D5DB-43E2-BDCB-2507232AFD1B",
        "opEquipId": "1",
        "opEquipName": "染色#1",
        "opMfgDate": "2023/09/18",
        "opStartTime": "2023/09/18 10:00:00",
        "opEndTime": "2023/09/18 11:00:00",
        "opItemNo": null,
        "opShiftPlnQty": null,
        "opShiftMfgQty": null,
        "opShiftCode": "A",
        "opShiftHours": null,
        "opRepShiftHours": "1",
        "opKpiRate": null,
        "opMtrName": null,
        "opWoNo": null,
        "opWoQty": null,
        "opPatternNo": null,
        "opStopDesc": null,
        "opIsRealData": "Y",
        "opWrnRemark": "換色清理"
      }
    ]
  }
}
```