# 報表-日期別色料領用報表-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230814 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                          |
|--------|------------------------------|
| URL    | /report/daily_pgm_list_excel |
| method | post                         |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱    |  資料型別 | 必填 | 資料儲存 & 說明  |
|--------------|---------|----------|:----:|-----------------|
| 原列表REQUEST參數 |     |          |      |                 |


# Request 後端流程說明

* 參考[OPMS_J6.報表-日期別色料領用報表-列表 /report/daily_pgm_list](./OPMS_J6.報表-日期別色料領用報表-列表.md)，已將同方式取得資料並篩選。
* 將資料製成excel檔案回傳，不須指定檔名。
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Excel 欄位
| 欄位       | 資料型別   | 資料儲存 & 說明          |
|------------|-----------|-------------------------|
|  日期      |  String   | WO_H.WO_DATE yyyy/mm/dd |
|  序號      |  String   | WO_H.WO_SEQ             |
|  色號      |  String   | ITEM_H.PATTERN_NO       |
|  原料一    |  String   | WO_MTR.WO_MTR_PART_NO   |
|  客戶      |  String   | WO_H.CUST_NAME          |
|  工單號碼   |  String   | WO_H.WO_NO             |
|  原料投入量 |  String   | WO_H.RAW_QTY            |
|  色料領用量 |  String   | PGM_QTY                 |
|  色料料號   |  String   | WO_PGM.WO_PGM_PART_NO   |
|  生產單位   |  String   | PRODUNIT.PRODUNIT_NAME  |