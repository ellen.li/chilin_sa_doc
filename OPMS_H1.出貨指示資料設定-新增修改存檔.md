# 出貨指示資料設定-新增修改存檔
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

新增修改存檔使用，【客戶/料號】 與 【包裝/棧板 + 料號規則】 共用同一支存檔 API

## 修改歷程

| 修改時間 | 內容                       | 修改者 |
|----------|--------------------------|--------|
| 20230608 | 新增規格                   | Nick   |
| 20230613 | 調整SQL-5 描述讓其白話一點 | Nick   |
| 20230821 | 調整優化說明               | Nick   |
| 20230821 | 異動需更新修改人員與時間   | Nick   |


## 來源URL及資料格式

| 項目   | 說明      |
| ------ | --------- |
| URL    | /sis/save |
| method | post      |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位             | 名稱         | 資料型別 | 必填 | 資料儲存 & 說明                                               |
|------------------|--------------|----------|------|---------------------------------------------------------------|
| opOrganizationId | 工廠別       | string   | M    |                                                               |
| opConfigUid      | Config UID   | string   | O    | 如為新增/複製模式下的存檔，請給 null，否則依原始 opConfigUid 給 |
| opCustNo         | 客戶代號     | string   | O    | 此欄位為【客戶/料號】使用                                       |
| opItemNo         | 料號         | string   | O    | 此欄位為【客戶/料號】使用                                       |
| opPkgType        | 包裝/棧板    | string   | O    | 此欄位為【包裝/棧板 + 料號規則】使用                            |
| opItemRule       | 料號規則     | string   | O    | 此欄位為【包裝/棧板 + 料號規則】使用                            |
| opItemRuleValue  | 料號規則值   | string   | O    | 此欄位為【包裝/棧板 + 料號規則】使用                            |
| opSisData        | 出貨指示清單 | array    | O    |                                                               |


#### 【 opSisData 】array
| 欄位           | 名稱           | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明 |
|----------------|--------------|----------|:------------------------:|-----------------|
| opListHeaderId | 出貨指示項目ID | string   |            O             |                 |
| opListLineId   | 出貨指示選項ID | string   |            O             |                 |
| opOptDesc      | 選項內容       | string   |            O             |                |
| opAssDesc      | 指定內容       | string   |            O             |                |


#### Request 範例

```json
{
   "opOrganizationId":"5000",
   "opConfigUid":null,
   "opCustNo":null,
   "opItemNo":null,
   "opPkgType":null,
   "opItemRule":null,
   "opItemRuleValue":null,
   "opSisData":[
      {
         "opListHeaderId":"68",
         "opListLineId":null,
         "opOptDesc":"-",
         "opAssDesc":null
      },
      {
         "opListHeaderId":"69",
         "opListLineId":null,
         "opOptDesc":"-",
         "opAssDesc":null
      },
      {
         "opListHeaderId":"70",
         "opListLineId":null,
         "opOptDesc":"-",
         "opAssDesc":null
      },
      {
         "opListHeaderId":"83",
         "opListLineId":null,
         "opOptDesc":"-",
         "opAssDesc":null
      },
      {
         "opListHeaderId":"157",
         "opListLineId":"1304",
         "opOptDesc":"＊專案客戶黑點管控一級色號",
         "assDesc":null
      },
      {
         "opListHeaderId":"104",
         "opListLineId":"1029",
         "opOptDesc":"明細行備註:",
         "opAssDesc":null
      }
   ]
}
```
# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* {IsNew} 等於 False
* 如 {opConfigUid} 等於 null 則 {IsNew} 等於 true 且 {opConfigUid}覆寫為 亂數產生一組全大寫 UUID (ex: 0AD9DFA8-536C-484E-BC15-D9EE457FBAA9)
* 如 {IsNew} 等於 true 且 執行 SQL-5 檢查出貨指定項目已存在 則 [return 400,SISCONFIGEXIST] 離開程式
* 如 {opSisData} 陣列內有資料
  * 如{IsNew} 等於 true，則執行 SQL-1。
  * 擷取 {opSisData} 陣列內每一筆資料
    * 如 {IsNew}為 
      * true:
        * {opListLineId} 不為 null，則執行 SQL-2 新增資料，參數參照 opSisData 內容
      * false:
        * 則執行 SQL-6 確認出貨指定是否已存在
          * 存在:
            * {opListLineId} 不為 null 則執行 SQL-3 更新資料
            * {opListLineId} 為 null 則執行 SQL-4 刪除資料，參數參照 opSisData 內容
          * 不存在:
            * {opListLineId} 不為 null 執行 SQL-2 新增資料
        * 如舊資料有作任何更新，執行 SQL-7 更新原資料修改時間與人員
* 上述過程如有執行任何資料庫任何異動，執行 SQL-LOG 紀錄 LOG，{DATAKEY} = {opConfigUid}
  有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明
| 欄位             | 名稱         | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明                    |
|------------------|--------------|----------|:------------------------:|------------------------------------|
| opOrganizationId | 工廠別       | string   |            M             |                                    |
| opConfigUid      | Config UID   | string   |            O             |                                    |
| opCustNo         | 客戶代號     | string   |            O             | 此欄位為【客戶/料號】使用            |
| opItemNo         | 料號         | string   |            O             | 此欄位為【客戶/料號】使用            |
| opPkgType        | 包裝/棧板    | string   |            O             | 此欄位為【包裝/棧板 + 料號規則】使用 |
| opItemRule       | 料號規則     | string   |            O             | 此欄位為【包裝/棧板 + 料號規則】使用 |
| opItemRuleValue  | 料號規則值   | string   |            O             | 此欄位為【包裝/棧板 + 料號規則】使用 |
| opSisData        | 出貨指示清單 | array    |            O             |                                    |


#### 【 opSisData 】array
| 欄位           | 名稱           | 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明 |
|----------------|--------------|----------|:------------------------:|-----------------|
| opListHeaderId | 出貨指示項目ID | string   |            O             |                 |
| opListLineId   | 出貨指示選項ID | string   |            O             |                 |
| opOptDesc      | 選項內容       | string   |            O             |                |
| opAssDesc      | 指定內容       | string   |            O             |                |



SQL-1:
```sql
--dao.insertSisConfigH(sConfigUid,organizationId, custNo, itemNo, pkgType, itemRule, itemRuleValue, userId);
INSERT INTO SIS_CONFIG_H (
        CONFIG_UID,
        ORGANIZATION_ID,
        CUST_NO,
        ITEM_NO,
        PKG_TYPE,
        ITEM_RULE,
        ITEM_RULE_VALUE,
        CDT,
        CREATE_BY,
        UDT,
        UPDATE_BY
    )
VALUES(
        { opConfigUid },
        { opOrganizationId },
        { opCustNo },
        { opItemNo },
        { opPkgType },
        { opItemRule },
        { opItemRuleValue },
        GETDATE(),
        { 登入者使用者ID },
        GETDATE(),
        { 登入者使用者ID }
    )
```
SQL-2:
```sql
--dao.insertSisConfig(to, userId);
INSERT INTO SIS_CONFIG (
        CONFIG_UID,
        LIST_HEADER_ID,
        LIST_LINE_ID,
        OPT_DESC,
        ASS_DESC,
        CDT,
        CREATE_BY,
        UDT,
        UPDATE_BY
    )
VALUES(
        { opConfigUid },
        { opListHeaderId },
        { opListLineId },
        { opOptDesc },
        { opAssDesc },
        GETDATE(),
        { 登入者使用者ID },
        GETDATE(),
        { 登入者使用者ID }
    )
```

SQL-3:
```sql
--dao.updateSisConfig(to, userId);			
UPDATE SIS_CONFIG
SET LIST_LINE_ID = { opListLineId },
    OPT_DESC = { opOptDesc },
    ASS_DESC = { opAssDesc },
    UDT = GETDATE(),
    UPDATE_BY = { 登入者使用者ID }
WHERE CONFIG_UID = { opConfigUid }
    AND LIST_HEADER_ID = { opListHeaderId }	
```

SQL-4:
```sql
--dao.deleteSisConfig(sConfigUid,to.getListHeaderId());						
DELETE SIS_CONFIG
WHERE CONFIG_UID = { opConfigUid } AND LIST_HEADER_ID ={ opListHeaderId }
```

SQL-5:
```sql
--queryDao.checkSisConfigExist(organizationId, custNo, itemNo, pkgType, itemRule, itemRuleValue)
--出貨指定項目是否已存在--客戶＋料號
SELECT COUNT(*) CNT
FROM SIS_CONFIG_H A
WHERE ORGANIZATION_ID = { opOrganizationId } 
-- 以下每一組註解和SQL代碼均應視變數的具體值來動態決定使用 IS NULL 或 欄位 = xxx 加入SQL語句，勿照抄!!!!!!

    --{opCustNo} 如果為NULL，則加入以下語句
    AND A.CUST_NO IS NULL     
    --{opCustNo} 如果不為NULL，則加入以下語句          
    AND A.CUST_NO = { opCustNo }   

    --{opItemNo} 如果為NULL，則加入以下語句                      
    AND A.ITEM_NO IS NULL   
    --{opItemNo} 如果不為NULL，則加入以下語句
    AND A.ITEM_NO = { opItemNo }                
    
    --{opPkgType} 如果為NULL，則加入以下語句
    AND A.PKG_TYPE IS NULL         
    --{opPkgType} 如果不為NULL，則加入以下語句       
    AND A.PKG_TYPE = { opPkgType }             

    --{opItemRule} 如果為NULL，則加入以下語句
    AND A.ITEM_RULE IS NULL       
    --{opItemRule} 如果不為NULL，則加入以下語句
    AND A.ITEM_RULE = { opItemRule }    

    --{opItemRuleValue} 如果為NULL，則加入以下語句
    AND A.ITEM_RULE_VALUE IS NULL              
    --{opItemRuleValue} 如果不為NULL，則加入以下語句
    AND A.ITEM_RULE_VALUE = { opItemRuleValue } 
```

SQL-6
```sql
--queryDao.checkSisConfigExist(sConfigUid, to.getListHeaderId())
--出貨指定是否已存在
SELECT COUNT(*) CNT FROM SIS_CONFIG A WHERE CONFIG_UID={ opConfigUid } AND A.LIST_HEADER_ID={ opListHeaderId }
```

SQL-7:
```sql
--原系統無這段，因為雅嵐測試有問題加入
UPDATE SIS_CONFIG_H SET UDT=GETDATE(),UPDATE_BY={ 登入者使用者ID } WHERE CONFIG_UID={opConfigUid}       
```


SQL-LOG:
```sql
INSERT INTO  SYS_MODI_LOG (CATEGORY,DATAKEY,USERID,USER_NAME,LOG) VALUES ('SISCONFIG',{DATAKEY},{登入者使用者ID},{登入者使用者名稱}, NULL)
```


# Response 欄位
| 欄位        | 名稱       | 資料型別 | 資料儲存 & 說明 |
|-------------|------------|----------|-----------------|
| opConfigUid | Config UID | string   |                 |


## Response 範例

```json
{
    "msgCode": null,
    "result": {
       "content":{
          "opConfigUid":"1E52FDA9-E248-485C-A48E-A6818C3EA3BD"
      }
    }
}
```
## 可參考原始碼 
客戶需求訪談可能會新增/修改/刪除原有功能，原始碼僅供參考

* SisConfigManageAction.saveSisConfig



