[返回總覽](./OPMS_00.總覽.md)  
# JWT Token

Token (SIT/UAT/PROD 相同)


| 參數    | 值                           |
|---------|------------------------------|
| Expires | 15分鐘 (refreshToken 30分鐘) |
| Secret  | <!-- T1BNU0BrZXk4ODg= -->    |

# 參數設定檔

## PROD

| 參數名稱                                         | 預設值                                 | 備註           |
|--------------------------------------------------|----------------------------------------|----------------|
| System.Menu.Root                                 | 奇菱科技                               |                |
| System.system.title                              | 應材營運生產管理系統3.0                | System message |
| System.system.contantus.email                    | stanly_chiu@chilintech.com.tw         | System message |
| System.error.access.nologin                      | 原使用者認證已逾時，請重新登入          | System message |
| System.login.message.login.fail.passworderror    | 使用者帳號或密碼輸入錯誤               | login message  |
| System.login.message.login.fail.account.inactive | 使用者帳號非有效的帳號                 | login message  |
| System.login.message.login.fail.code.notmatch    | 驗證碼輸入錯誤!                        | login message  |
| pattern.file.root                                | /OPMS_SHARE/PATTERN                    | 色號圖檔存放處 |
| cs.file.root                                | /OPMS_SHARE/CS                    | 客訴附件存放處 |
| System.rsa.backend.private.password              | 密碼請找昕力                           | rsa 私鑰       |
| System.rsa.backend.public.password               | 密碼請找昕力                           | rsa 公鑰       |
| System.jwt.secret                                | 密碼請找昕力 <!-- T1BNU0BrZXk4ODg= --> | jwt secret     |
| System.logging.file.path                         | 檔案日誌及壓縮檔存放目錄               | /OPMS_LOG                                                             |
| System.logging.file.name                         | 檔案日誌檔名                           | OPMS-%d{yyyy-MM-dd}.log                                               |
| System.logging.rolling.filename                  | 檔案日誌壓縮檔名                       | OPMS-%d{yyyy-MM-dd}.log.gz                                            |
| System.logging.pattern.console                   | 在 console 輸出的日誌的格式            | %d{yyyy-MM-dd} [%thread] %-5level %logger{50} - %msg%n                |
| System.logging.pattern.file                      | 指定檔案中日誌輸出的格式               | %d{yyyy-MM-dd} === [%thread] === %-5level === %logger{50} ==== %msg%n |
| System.logging.level                             | 指定 logger 物件日誌級別               | debug                                                                 |
| System.logging.maxhistory                        | 檔案日誌壓縮保留天數                   | 180                                                                   |
<!-- 
====================================================================
【PROD】 RSA_PKCS1_PADDING 2048bit
====================================================================

-----BEGIN RSA PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCcXCvvaWW4iiY9wAsWI7A/AmjO0CVaihhk0sJ5kHsQiUMNhBVP4RAlXNuSMV9EkicBqCwZI+6fS/W9Iw0e5CbVzD+jsHU0uIfG1WXJhxB31kAwWdX29cNirA7p7lyFiQ6uCNezRg6tiNvaGSWKICphsEaLy3EiDJSeYWhHCRH9EVO0uTpgh/8IQu4sDOuwOsq8O8H+9y/ItIrQ8jB8fW46Pg39tXLMapQlm0AQ7C8MYM+oA0t1XjlxfkTm9FSa4VqgqXNKphnnfJey5IYq/VA8yr3igwjyixaV45IU50/3nYdEPRRUXS669rvEEJCKmziQ+vCdkRbHHCYMZYSUv4E1AgMBAAECggEAa7hcVF4KRC541gXeHWyM2XvxdLmVYmDNGXjRnipZfjTySvd6L0Pgeb0VUAxPk9AUq2GOr+s/U+Ni8uCZA3UvBA/or/67YIzYjbUqoMI6KDUkl6rsa205JFthEuyyNRUNVfyljc5wAkHNJN46LISobhChKWIJGOoN4Wc+DeJD0FHwh0ALl+9jIT4Naor48O0PNt1z8q7EaTudN1DvwEiaK4p56SUTdXXdTVPOlaIClG7xPaNVWBnjSqm0AsC6otnuZh/3Apc1i2q8InwAZ9SjvhEpDLi9LkvHp5q5CcjQUYdvQPhycXFbSfkvwsXSMsQFo8jrxM3WJLxsqBLcL+rXwQKBgQDdNBItAyvgqE36FgzzCKNDniUN9whodQsyPQYC6ERpCxD/okA8IKIGsjMB7zqgciO6reVQ2k6/X7l5Au42esORomf78D0vKwYJ2PTCPYNL6Bz6NRomaD06hVX818xdDEWvHTISMVK4Kcj8rgAHNpJivy0Mv9H+Y+Xy6jQf50vKuQKBgQC09Na1l+cPtWtoOgNxu2/WSy8aLkTmSPHxJuYvOktx9N+eVO6JW+5d9qwoiDnuL0Vz46wKZOezGzti2I4Tfa3l135d5zHFFXr3yFBAjhFFn1MhAY201ZLOlb0sV00sTxoyHyEf56p3UcBim+DyyWbrtPDakahO0PIJ4Dsk6I+8XQKBgQCbVCk/Om7hPs4iwsEtF++tV6uldsaMs0O9b2R9PiRrrr2tBH4euruMAC4ZOaftarj0KZBRijqp38oZ05j64JHb/jMOktAkAw4mdBbLHu7HqqJOgGNwfNoh8edDuTVkUW3NXa7/X2s6rvzXkkeqyQNaG+zCeyA2hme+f/9CN+Tc4QKBgHKKQ9ngE97Km/j2N7Xmyeyf3Gm6/duyMorNb4hXYuf38w/afL6vVRtfBywExn5wp4qqgvx1UH/7bcSshInr1IYWuCMiM0unxBVRUxL5mg8ywUC0Jglf4rQm7f5CKFQyocc2ExaJ4knykk8YkfO77gymlIoVBq8U7Q+5Qlajak/1AoGAR/8cq9K+jew0jBqqVPEfNjpMaZ4UeCx/fco884Jitue1S2sdiACFNhck95asb81hCUFrOOSLNf1aPGPeMebWfUdzaEklhn+p6sZ/PMpPaokMtEnuk46R13OYm3rC0DMHWrsfknZWhSrmRgDyVpTF0qWbxpcQu713AJadl1/yvhI=
-----END RSA PRIVATE KEY-----


-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnFwr72lluIomPcALFiOwPwJoztAlWooYZNLCeZB7EIlDDYQVT+EQJVzbkjFfRJInAagsGSPun0v1vSMNHuQm1cw/o7B1NLiHxtVlyYcQd9ZAMFnV9vXDYqwO6e5chYkOrgjXs0YOrYjb2hkliiAqYbBGi8txIgyUnmFoRwkR/RFTtLk6YIf/CELuLAzrsDrKvDvB/vcvyLSK0PIwfH1uOj4N/bVyzGqUJZtAEOwvDGDPqANLdV45cX5E5vRUmuFaoKlzSqYZ53yXsuSGKv1QPMq94oMI8osWleOSFOdP952HRD0UVF0uuva7xBCQips4kPrwnZEWxxwmDGWElL+BNQIDAQAB
-----END PUBLIC KEY-----
-->
## SIT/UAT

| 參數名稱                                         | 預設值                                 | 備註                                                                  |
|--------------------------------------------------|----------------------------------------|-----------------------------------------------------------------------|
| System.Menu.Root                                 | 奇菱科技                               |                                                                       |
| System.system.title                              | 應材營運生產管理系統3.0                | System message                                                        |
| System.system.contantus.email                    | stanly_chiu@chilintech.com.tw         | System message                                                        |
| System.error.access.nologin                      | 原使用者認證已逾時，請重新登入          | System message                                                        |
| System.login.message.login.fail.passworderror    | 使用者帳號或密碼輸入錯誤               | login message                                                         |
| System.login.message.login.fail.account.inactive | 使用者帳號非有效的帳號                 | login message                                                         |
| System.login.message.login.fail.code.notmatch    | 驗證碼輸入錯誤!                        | login message                                                         |
| pattern.file.root                                | /OPMS_DEV/PATTERN               | 色號圖檔存放處                                                        |
| cs.file.root                                | /OPMS_DEV/CS                    | 客訴附件存放處 |
| System.rsa.backend.private.password              | 密碼請找昕力                           | rsa 私鑰                                                              |
| System.rsa.backend.public.password               | 密碼請找昕力                           | rsa 公鑰                                                              |
| System.jwt.secret                                | 密碼請找昕力 <!-- T1BNU0BrZXk4ODg= --> | jwt secret                                                            |
| System.logging.file.path                         | 檔案日誌及壓縮檔存放目錄               | /OPMS_LOG                                                             |
| System.logging.file.name                         | 檔案日誌檔名                           | OPMS-%d{yyyy-MM-dd}.log                                               |
| System.logging.rolling.filename                  | 檔案日誌壓縮檔名                       | OPMS-%d{yyyy-MM-dd}.log.gz                                            |
| System.logging.pattern.console                   | 在 console 輸出的日誌的格式            | %d{yyyy-MM-dd} [%thread] %-5level %logger{50} - %msg%n                |
| System.logging.pattern.file                      | 指定檔案中日誌輸出的格式               | %d{yyyy-MM-dd} === [%thread] === %-5level === %logger{50} ==== %msg%n |
| System.logging.level                             | 指定 logger 物件日誌級別               | debug                                                                 |
| System.logging.maxhistory                        | 檔案日誌壓縮保留天數                   | 180                                                                   |


<!--
====================================================================
【UAT/SIT】 RSA_PKCS1_PADDING 2048bit
====================================================================

-----BEGIN RSA PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC4U/7nVR0nxzt/5b72nz2W7ozsEQga44C0E6GOGe7zYDgDFJ0GS7z0iRLlgKwBkAc4huBwQzotaM2X5H5REJCBZTZxF4nYXJ9f0jzrowyi6HDuKaeiTF15Ghwu1f863hj69IBc9jMATL4NogQSl3eW3ffkQ8F9mrR324+4Zz8Dh4W0UiaTLg0Ss3EtxKUUKF/PgRKxm7yG06pUB198xlD+BIYYZ8CHET9HYHlW8NitbXcchOWpKFzaJdmP0YQ3KIalchvpKw7BPGu2wKDpVURn0O4afprbNyC2q1t70uH3szUVn/zjWk4c41D6/XpZ4Gh59NY+Opy496xInvqd7LQNAgMBAAECggEAWoUlUQWDY917Yru4o/RfkpdjrSpQCKRguA5L8DTcG7jMyPZ+hnD7QjtuhDZEQiAw65mQ7pCVKWemT0lunvC7hk/mBHPCxfX2bmAYer9/nSD5FHxwTkuiO66O2JFO3eHj8pnPz9c4BJWrX5aI2Xh4yiYHbpm3k689oP65/Zz2XQyDQOa4XyoSGRQ1NUfDeFRGsi2Aiy0IkJBdlf3WtUic9L4HhbwWY+ZWEG5VyDjDydmFNyfPXB1VlE+WeNwkIkroaIk7KTAi/1v0mFCq1IU9gZwHjH0RIqwUYucSyPgLKbOhkeTfjfvhM7y6+Hl0Mu4V0vav+O5kbcuE2LI6czbVAQKBgQDdFoy4FHs7pf+KGtd+c3UfKnuc9gaBXUpTRK6t2kC42pSiYd2cYUB8xobSZ/cFp3jRdF9LpPGEfe2wkI5VE+m2AjWhX1umGzX9s5V4SSPAwmpEDGOT17xHX5jP3LwTeo84cFo+D1wthfPZn0qNuMnIk5Rz1w702+gkfpSLqryF+QKBgQDVb24NR2GMGh6A3aAFOBZ3QUX2lh8o+2T0wiGMo3tU1acGvtjS4xbectDSN1bjnGh39lAvRVaVyS75hcOfJWqjLO1qa240ym6hhaPpRMAqcLMkrISNOFF+itfUWtnP8EOmkX6fbwqrcTeCpMeBLhVYjsez69xTMmT4Uw1mMFSTtQKBgQC33JbuR2ossuIPEj+AE7ilUIk4gYnrb4/uK8IxJr6IfI77c/P+pwogOmHI/Vj7RMk/N05A9E0+aBORsMYNjt/f03qSV2E6hIqkEAMP45NuLMB/EuaE/va/TIsi4mkusdAFmboSvmdkHHAs3UH9Fl4JBmKA66qui2tFXldex4C80QKBgCnYc/o0gYKHaG2ZhhMf4P+2k3H6EtflGfEG0q8RKlACp4GiDz6heU1Zs2lTojxz4OOMLkJmVMh/Li+Ls0PTgSnG2DFiKLQ+8BvI6M8GRKrNd8H79Oz/GrM1o4Ul3shu8cSJiArYKX5dbyW1Csxf7Z10OOEkoAjLu5IYLmtXlG11AoGAEB327o2JAmSeQKSVvxFXQTx6EfEAQ5KYawaMK3LbvhFRQzpKLSabBCLrb0QqXnq5v8tcps1XcA7zIwMBwEHA1z3EwFZvlyTiOmv4UXFUYtplO7T59AA0Pf90+t7FoIRb3OccqNYnzeAf0mVgIErpPAfyCkSf6vGQPkrMy5nD03I=
-----END RSA PRIVATE KEY-----



-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuFP+51UdJ8c7f+W+9p89lu6M7BEIGuOAtBOhjhnu82A4AxSdBku89IkS5YCsAZAHOIbgcEM6LWjNl+R+URCQgWU2cReJ2FyfX9I866MMouhw7imnokxdeRocLtX/Ot4Y+vSAXPYzAEy+DaIEEpd3lt335EPBfZq0d9uPuGc/A4eFtFImky4NErNxLcSlFChfz4ESsZu8htOqVAdffMZQ/gSGGGfAhxE/R2B5VvDYrW13HITlqShc2iXZj9GENyiGpXIb6SsOwTxrtsCg6VVEZ9DuGn6a2zcgtqtbe9Lh97M1FZ/841pOHONQ+v16WeBoefTWPjqcuPesSJ76ney0DQIDAQAB
-----END PUBLIC KEY-----
-->


# 資料庫連線設定

## PROD
| 項目            | 值                                   | 說明     | 
| ------------    | ------------------------------------ | -------- |
| WFDataSource(E奇簽) <br/>Oracle 9i    |  jdbc:sqlserver://10.1.1.130:1433;databaseName=wfdb_prd;integratedSecurity=false    <br/>帳號密碼請洽昕力 <!-- wf_prd/2wsx3edc$ -->                                |OPMS DB|
| OPMSDataSource(OPMS) <br/>MS SQL 2019  |  jdbc:sqlserver://10.200.10.135:1433;databaseName=OPMSDBPRD;integratedSecurity=false;encrypt=false;trustServerCertificate=true;user=;password=;applicationname=OPMS <br/> 帳號密碼請洽昕力 <!-- opmssys/CLT#5tgb7 -->                                 |E奇簽 DB|
|SAP_RMI_PORT     | rmi://10.200.10.127:1129/SAP |SAP API |
|ODSDataSource(ODS) MS SQL 2008(不確定) | jdbc:sqlserver://10.1.1.139:1433;databaseName=sap_ods;integratedSecurity=false     <br/>帳號密碼請洽昕力 <!-- wf_sys/1qaz2wsx -->                                |ODS DB|
|WORKFLOW_URL_CL     | http://10.1.1.132/WorkflowAdmin |奇菱workflow網址 |
|WORKFLOW_URL_LS     | http://10.201.1.197/AFExt/EAMSProcessTrigger |菱翔workflow網址 |
|WORKFLOW_EFORM_CL   | http://10.1.1.132/WebAgenda/SingleSignon.do |奇菱SSO網址 |
|WORKFLOW_EFORM_LS   | http://10.201.1.197/WebAgenda/SingleSignon.do |菱翔SSO網址 |


## SIT/UAT
| 項目            | 值                                   | 說明     | 
| ------------    | ------------------------------------ | -------- |
| WFDataSource    |  jdbc:sqlserver://sqldev.chimei.com.tw:1433;databaseName=wfdb_qas2014;integratedSecurity=false    <br/>帳號密碼請洽昕力 <!-- wf_qas/3edc2wsx -->                                |OPMS DB|
| OPMSDataSource  |  jdbc:sqlserver://10.200.10.75:1433;databaseName=OPMSDEVDB;integratedSecurity=false;encrypt=false;trustServerCertificate=true;user=;password=;applicationname=OPMS <br/> 帳號密碼請洽昕力 <!-- opmssys/OPMS%7ujm9 -->                                 |E奇簽 DB|
|SAP_RMI_PORT     | rmi://10.200.10.134:1129/SAP |SAP API |
|ODSDataSource(ODS) MS SQL 2014(不確定) | jdbc:sqlserver://sqldev.chimei.com.tw:1433;databaseName=sap_ods_test;integratedSecurity=false     <br/>帳號密碼請洽昕力 <!-- wf_sys/1qaz2wsx -->                                |ODS DB|
|WORKFLOW_URL_CL     | http://10.1.11.236/WorkflowAdmin/ |奇菱workflow網址 |
|WORKFLOW_URL_LS     | http://10.201.1.25/AFExt/EAMSProcessTrigger |菱翔workflow網址 |
|WORKFLOW_EFORM_CL   | http://10.1.11.236/WebAgenda/SingleSignon.do |奇菱SSO網址 |
|WORKFLOW_EFORM_LS   | http://10.201.1.25/WebAgenda/SingleSignon.do |菱翔SSO網址 |

## 網域資料
**註:使用 https 務必使用 同 domain 連接** 目前 https 只針對 chimei port 8443 提供服務
| 正式/測試 | 主機              | 奇菱網址(一般使用者)                        | 奇美網址(僅供E奇簽使用，不得給 user) | IP            | Port                   |
|---------|-------------------|---------------------------------------------|-------------------------------------|---------------|------------------------|
| 測試      | 前端              | opmsqas.chilintech.com.tw                   | opmsqas.chimei.com.tw               | 10.200.10.166 | http(80) https(8443)   |
| 測試      | 後端              | opmsapiqas.chilintech.com.tw                | opmsapiqas.chimei.com.tw            | 10.200.10.167 | http(8090) https(8443) |
| 測試      | Reporting Service | http://cltsqldev.chilintech.com.tw/reports/ |  N/A                                   | 10.200.10.75  |                        |
| 正式 :no_entry:     | 前端              | opmsprd.chilintech.com.tw                   | opmsprd.chimei.com.tw               | 10.200.10.168 | http(80) https(8443)   |
| 正式 :no_entry:      | 後端              | opmsapiprd.chilintech.com.tw                | opmsapiprd.chimei.com.tw            | 10.200.10.169 | http(8090) https(8443) |
| 正式 :no_entry:      | Reporting Service | http://cltopmsdb.chilintech.com.tw/reports/ |   N/A                                   | 10.200.10.135 |                        |





## E奇簽嵌入 SOC 檢視用的user帳密
帳號: SOCVIEW
密碼: 密碼請找昕力 <!-- view@3edc -->

## E奇簽網域
| 機器   | 網址                 | 起單URL                                                                    |-簽核入口|
|------|----------------------|----------------------------------------------------------------------------|-|
| 測試機 | gwtest.chimei.com.tw | https://gwtest.chimei.com.tw/uat/agents/eSign/v1/trig/PRO12621701074272046 |https://gwtest.chimei.com.tw/uat/esign?page=login&r=/uat/esign/?page=overview 帳號/密碼 AF|
| 正式機 :no_entry: | apgw.chimei.com.tw   | https://apgw.chimei.com.tw/apis/agents/eSign/v1/trig/PRO12621701074272046  ||





| 正式/測試      | 主機              | 奇菱網址(一般使用者)                        | IP            |
|--------------|-------------------|---------------------------------------------|---------------|
| 測試           | 前端              | opmsqas.chilintech.com.tw                   | 10.200.10.166 |
| 測試           | 後端              | opmsapiqas.chilintech.com.tw                | 10.200.10.167 |
| 測試           | Reporting Service | http://cltsqldev.chilintech.com.tw/reports/ | 10.200.10.75  |
| 正式 :no_entry:  | 前端              | opmsprd.chilintech.com.tw                   | 10.200.10.168 |
| 正式 :no_entry: | 後端              | opmsapiprd.chilintech.com.tw                | 10.200.10.169 |
| 正式 :no_entry: | Reporting Service | http://cltopmsdb.chilintech.com.tw/reports/ | 10.200.10.135 |


## 後端ssl 產生 P12 簽證給 SpringBoot
在openssl的bin那層下cmd
openssl pkcs12 -export -out chimei.p12 -inkey STAR_chimei.com.tw.key -in STAR_chimei.com.tw.crt