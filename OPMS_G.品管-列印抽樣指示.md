# 品管-列印抽樣指示
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容               | 修改者 |
| -------- | ------------------ | ------ |
| 20230911 | 新增規格           | Ellen  |
| 20230915 | 修正SQL-3          | Ellen  |
| 20231003 | 修正累積生產量計算 | Ellen  |


## 來源URL及資料格式

| 項目   | 說明         |
| ------ | ------------ |
| URL    | /qc/si_excel |
| method | post         |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位        | 名稱   |   資料型別   | 必填  | 資料儲存 & 說明 |
| ----------- | ------ | :----------: | :---: | --------------- |
| opWoUidList | 工單ID | array string |   M   |                 |

#### Request 範例
```json
{
  "opWoUidList": ["00006D90-B5F1-4A80-9F62-B9989EBAE168", "00008BEB-34CC-482D-B52E-AC3E0498AD4E"]
}
```

# Request 後端流程說明

請參考舊版 `SampleInstructionsAction.QcPrintSI2excel`
* 工廠別{organizationId} 為登入者工廠別ID
* 套用範本excel產生excel檔並匯出
* 範本excel檔案位置：
  * 工廠別為'6000'時：/report/SITemplateLSO.xls
  * 工廠別不為'6000'時：/report/SITemplate.xls
* Logo圖檔位置：
  * 工廠別為'6000'時：/jasperReport/logoLSO.png
  * 工廠別不為'6000'時：/jasperReport/logo.png
* 遍歷所有需列印的工單，{woUid}為單筆工單ID
  * 執行 SQL-1 取得工單資料，只取第一筆
  * 執行 SQL-2 取得PGM資料
* 每一筆工單產生一個頁籤
* 組顯示用變數，實際套用位置參考範例說明檔案
  * 醫療級或一般級{isoProdOpt}: 
    * 若 {organizationId}等於5000: ISO_PROD_OPT (SQL-3)
    * 否則為空字串
  * 批數{batchShow}: 
    * {batchQty}: BATCH_QTY(SQL-1) 去除小數點後多餘的0
    * {remainQty}: REMAIN_QTY(SQL-1) 去除小數點後多餘的0
    * 若 REMAIN_QTY(SQL-1)不等於0: `{batchQty} + "*" + BATCH_TIMES(SQL-1) + "+" + {remainQty} + "*" + REMAIN_TIMES(SQL-1)`
    * 若 REMAIN_QTY(SQL-1)等於0: `{batchQty} + "*" + BATCH_TIMES(SQL-1)`
  * 銷售數量 {salesQty}: SALES_QTY(SQL-1) 去除小數點後多餘的0
  * PGM每batch總用量 {tPgmBatchQty}: `PGM_BATCH_QTY(SQL-2)加總 / 1000`
  * PGM尾數總用量 {tPgmRemainQty}: `PGM_REMAIN_QTY(SQL-2)加總 / 1000`
  * 第一筆抽樣數量 {ipqcQtyF}:
    * {organizationId}不等於6000時
      * 若ITEM_NO(SQL-1)第8碼等於M (是色母) 為`50g`
      * 若WO_NO(SQL-1)開頭為55 (少量工單) 為`50g`
      * 其餘為`2Kg`
  * 抽樣數量 {ipqcQtyOther}:
    * {organizationId}不等於6000時
      * 若ITEM_NO(SQL-1)第8碼等於M (是色母) 為`50g`
      * 若WO_NO(SQL-1)開頭為55 (少量工單) 為`50g`
      * 其餘為`0.1Kg`
  * batch總筆數 {tbatch}: 
    * 若REMAIN_QTY(SQL-1)等於0 {tbatch}等於BATCH_TIMES(SQL-1)
    * 否則等於 BATCH_TIMES(SQL-1) + REMAIN_TIMES(SQL-1)
* 執行迴圈，迴圈索引{i}由1至{tbatch}
  * 計算累積生產量 {SumBatchQty}: 
    * 若{i}小於等於BATCH_TIMES(SQL-1) {SumBatchQty}等於 `{SumBatchQty} + {BATCH_QTY(SQL-1)} + {tPgmBatchQty}`取至整數 ❗每次計算結果填入excel表中
    * 若{i}大於BATCH_TIMES(SQL-1) {SumBatchQty}等於 `{SumBatchQty} + {REMAIN_QTY(SQL-1)} + {tPgmRemainQty}`取至整數 ❗每次計算結果填入excel表中
  * 若{organizationId}等於6000
    * 第1筆batch 取前中抽樣，抽樣數量為1Kg及0.6Kg
    * 第2筆batch 取中抽樣0.6Kg
    * 第3筆batch開始，4個batch為一個循環(前3個batch取中0.1kg、最後1個batch 取中0.6kg)
    * 最後一筆 取中後抽樣 0.6Kg
    * 填入OQC說明 (參考說明檔案)
  * 若{organizationId}不等於6000
    * 共只有一筆batch時 取前中後抽樣 (參考說明檔案)
    * 第1筆batch 取前中抽樣，抽樣數量為{ipqcQtyF}及{ipqcQtyOther}
    * 最後一筆 取中後抽樣，抽樣數量為{ipqcQtyOther}
    * 填入OQC說明 (參考說明檔案)
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

* 範例說明檔案 (紅框及紅色字體僅為說明示意)
  * [抽樣指示紀錄範例_奇菱.xls](./images/抽樣指示紀錄範例_奇菱.xls "download")
  * [抽樣指示紀錄範例_奇菱_只有單一筆batch.xls](./images/抽樣指示紀錄範例_奇菱_只有單一筆batch.xls "download")
  * [抽樣指示紀錄範例_菱翔.xls](./images/抽樣指示紀錄範例_菱翔.xls "download")

SQL-1: 取得工單資料
```sql
SELECT
  W.WO_NO,
  B.ITEM_NO,
  W.BATCH_QTY,
  W.BATCH_TIMES,
  W.REMAIN_QTY,
  W.REMAIN_TIMES,
  W.SALES_QTY,
  P.PRODUNIT_NAME,
  B.PATTERN_NO 
FROM
  WO_H W
  INNER JOIN PRODUNIT P ON W.PRODUNIT_NO= P.SAP_PRODUNIT
  INNER JOIN BOM_MTM B ON W.BOM_UID= B.BOM_UID 
WHERE
  W.WO_UID= {woUid}
```

SQL-2: 取得PGM資料
```sql
SELECT
  WO_PGM_PART_NO,
CASE
  QUANTITY_PER 
  WHEN cast(QUANTITY_PER AS INT) THEN cast(CONVERT (INT, QUANTITY_PER) AS VARCHAR (20)) 
  ELSE cast(CONVERT (FLOAT, QUANTITY_PER) AS VARCHAR (20)) 
  END QUANTITY_PER,
  UNIT,
  SITE_PICK,
  SELF_PICK,
CASE
  PGM_BATCH_QTY 
  WHEN cast(PGM_BATCH_QTY AS INT) THEN cast(CONVERT (INT, PGM_BATCH_QTY) AS VARCHAR (20)) 
  ELSE cast(CONVERT (FLOAT, PGM_BATCH_QTY) AS VARCHAR (20)) 
  END PGM_BATCH_QTY,
CASE
  PGM_REMAIN_QTY 
  WHEN cast(PGM_REMAIN_QTY AS INT) THEN cast(CONVERT (INT, PGM_REMAIN_QTY) AS VARCHAR (20)) 
  ELSE cast(CONVERT (FLOAT, PGM_REMAIN_QTY) AS VARCHAR (20)) 
  END PGM_REMAIN_QTY,
  MODIFY_QTY,
  MODIFY_UNIT_QTY,
  FEEDER_SEQ 
FROM
  WO_PGM 
WHERE
  WO_UID = {woUid}
ORDER BY
  WO_PGM_PART_NO
```

SQL-3: 取得醫療級或一般級
```sql
SELECT
	ISNULL((
    SELECT NULLIF(OPT_DESC, '') 
    FROM WO_SIS 
    WHERE 
      LIST_HEADER_ID = 158 
      AND WO_UID = {woUid}
  ), '□一般級  □醫療級') ISO_PROD_OPT
```