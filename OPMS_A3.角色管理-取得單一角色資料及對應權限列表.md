# 角色管理-取得單一角色資料及對應權限清單
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得單一角色資料及對應權限清單


## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230405 | 新增規格 | Nick   |

## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    | /role/get_one |
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。
| 欄位    | 名稱 | 資料型別 | 必填 | 資料儲存 & 說明 |
| ------- | ---- | :------: | :--: | --------------- |
| opRoleId | 角色ID |  integer  |  M  |                 |

#### Request 範例

```json
{
  "opRoleId":931
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 執行  SQL-1 確認該角色存在，找不到 return 400,"NOTFOUND"
* 執行  SQL-2 取回該角色對應權限清單，依照 result (CODE)組出不重複的資料，將同組 PERM_CODE 資料組成陣列，
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容


# Request 後端邏輯說明

SQL-1:

```sql
SELECT ROLE_ID,ROLE_NAME FROM SYS_ROLE_V3 
    WHERE ROLE_ID = {opRoleId}
```
SQL-2:

```sql
  SELECT SR.CODE as FCODE,SRP.PERM_CODE as PCODE FROM SYS_ROLE_FUNCTION_V3 SR
      LEFT JOIN SYS_ROLE_FUNCTION_PERMS SRP
  ON SR.ROLE_ID = SRP.ROLE_ID AND SR.CODE = SRP.CODE 
  WHERE SR.ROLE_ID={opRoleId}
  ORDER BY SR.CODE,SRP.PERM_CODE
```


# Response 欄位
| 欄位           | 名稱         | 資料型別 | 資料儲存 & 說明 |
|----------------|------------|----------|-----------------|
| opRoleId       | 角色ID       | integer  |                 |
| opRoleName     | 角色名稱     | string   |                 |
| opRoleCodeList | 角色權限清單 | array    | 無資料請給 null |

【opRoleCodeList array object】
| 欄位    | 名稱       | 資料型別     | 來源資料 & 說明 |
|---------|----------|--------------|-----------------|
| opFCode | 功能代號   | string       | FCODE           |
| opPCode | 子權限代號 | array string | PCODE           |

#### Response 範例

```json
{
  "msgCode":"",
  "result":{
    "content":
      {
        "opRoleId":931,
        "opRoleName":"工務：案件管理",
        "opRoleCodeList":[
                           {"opFCode":"100", "opPCode":["QUERY","EDIT"]}
                           {"opFCode":"400", "opPCode":null }
                         ]
      }
  }
}
```

