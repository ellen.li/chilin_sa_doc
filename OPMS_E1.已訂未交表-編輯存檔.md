# 已訂未交表-編輯存檔
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230816 | 新增規格 | Shawn  |


## 來源URL及資料格式

| 項目   | 說明           |
| ------ | -------------- |
| URL    | /ps/save_wo_ps |
| method | post           |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位        | 名稱         | 資料型別 | 必填  | 資料儲存 & 說明 |
| ----------- | ------------ | :------: | :---: | --------------- |
| opWoUid     | 工單ID       |  string  |   M   |                 |
| opWoNo      | 工單號碼     |  string  |   M   |                 |
| opPsRemark  | 備註         |  string  |   O   |                 |
| opPsEquipId | 預排機台id   |  string  |   O   |                 |
| opDdMfg     | 現場交期     |  string  |   O   |                 |
| opPsFlag    | 狀態         |  string  |   O   | F/N/Y              |
| opRemark    | 交期變更說明 |  string  |   O   |                 |


#### Request 範例

```json
{
  "opWoUid":"D65D4B8B-8922-4165-8A3E-489C0B46D7B1",
  "opWoNo":"51030056",
  "opPsRemark":null,
  "opPsEquipId":null,
  "opDdMfg":null,
  "opPsFlag":"F",
  "opRemark":null
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* SQL 如為連續動作務必於相關增、刪、修處用 transaction 包住，有錯誤中斷流程 RollBack，無問題則commit，確保資料一致性
* 更新工單，sql-1
* 如狀態為F，回傳工時給SAP
  * 查詢sql-2結果傳入SAP function
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200。

# Request 後端邏輯說明

SQL-1: 
```sql
UPDATE WO_H SET 
PS_EQUIP_ID={opPsEquipId},
PS_REMARK={opPsRemark},
DD_MFG={opDdMfg},
PS_FLAG={opPsFlag},
PS_UDT=GETDATE(),
PS_UPDATE_BY= {登入者使用者ID}
WHERE WO_UID={opWoUid}
```

SQL-2: 
```sql
SELECT
	W.WO_NO,
	R.PSR_UID,
	R.EQUIP_ID,
	E.EQUIP_NAME,
	CONVERT(VARCHAR(10),R.REP_MFG_DATE,	111) REP_MFG_DATE,
	R.REP_SHIFT_CODE,
	W.SAP_OPERATION,
	R.WO_UID,
	R.REP_SHIFT_HOURS,
	R.REP_TIME_LINE,
	R.REP_MIN_MFG_QTY,
	R.SHIFT_MFG_QTY,
	R.SHIFT_PRE_QTY,
	R.REP_CTN_DESC,
	R.WRN_FLAG,
	R.WRN_REMARK,
	E.SAP_WC
FROM
	PS_R R     			
INNER JOIN EQUIP_H E ON
	R.EQUIP_ID = E.EQUIP_ID     			
INNER JOIN PRODUNIT U ON
	E.ORGANIZATION_ID = U.ORGANIZATION_ID
	AND E.PRODUNIT_NO = U.PRODUNIT_NO     			
INNER JOIN WO_H W ON
	R.WO_UID = W.WO_UID   	
WHERE
	(R.ERP_FLAG is null
	OR R.ERP_RTN_TYPE = 'E')
AND W.WO_NO ={opWoNo}
ORDER BY
	R.EQUIP_ID,
	R.REP_MFG_DATE,
	R.REP_SHIFT_CODE,
	R.REP_TIME_LINE
```

SAP-1：
可參考舊版 PsModifyRfc_importHoursToSap
程序如有任何異常，紀錄錯誤LOG

**獲得BAPI方法**
|              | 型態   | 參數值                | 說明 |
| ------------ | ------ | --------------------- | ---- |
| BAPIFunction | string | Z_PP_CL_ORDER_CONFIRM |      |

**設定輸入參數**
| 參數名稱   | 參數值 | 型態     | 說明 |
| ---------- | ------ | -------- | ---- |
| IT_CONFIRM |        | JCoTable |      |

IT_CONFIRM JCoTable 參數
| 參數名稱        | 參數值                  | 型態   | 說明                       |
| --------------- | ----------------------- | ------ | -------------------------- |
| ORDERID         | {SQL-2.WO_NO}           | string | format: "%012d"            |
| OPERATION       | {SQL-2.SAP_OPERATION}   | string | format: "%04d"             |
| POSTG_DATE      | {SQL-2.REP_MFG_DATE}    | string | yyyyMMdd                           |
| WORK_CNTR       | {SQL-2.SAP_WC}          | string | SAP_WC不為空時             |
| CONF_ACTI_UNIT1 | "STD"                   | string |                            |
| CONF_ACTIVITY1  | {SQL-2.REP_SHIFT_HOURS} | double | SAP_WC為T3N時，作業1工時X2 |
| CONF_ACTI_UNIT2 | "STD"                   | string | SAP_WC不為T11、M11、T2N時  |
| CONF_ACTIVITY2  | {SQL-2.REP_SHIFT_HOURS} | double | SAP_WC不為T11、M11、T2N時  |
| CONF_ACTI_UNIT3 | "STD"                   | string | SAP_WC不為T11、M11、T2N時  |
| CONF_ACTIVITY3  | {SQL-2.REP_SHIFT_HOURS} | double | SAP_WC不為T11、M11、T2N時  |
| CONF_ACTI_UNIT4 | "STD"                   | string | SAP_WC不為T11、M11、T2N時  |
| CONF_ACTIVITY4  | {SQL-2.REP_SHIFT_HOURS} | double | SAP_WC不為T11、M11、T2N時  |
| CONF_ACTI_UNIT5 | "STD"                   | string | SAP_WC不為T11、M11、T2N時  |
| CONF_ACTIVITY5  | {SQL-2.REP_SHIFT_HOURS} | double | SAP_WC不為T11、M11、T2N時  |
| CONF_ACTI_UNIT6 | "STD"                   | string |                            |
| CONF_ACTIVITY6  | {SQL-2.REP_SHIFT_HOURS} | double |                            |


| 取得回傳表格名稱 |
| ---------------- |
| ET_RETURN        |

| SAP欄位名稱  | SAP資料型別 | 欄位值 | 說明 |
| ------------ | :---------: | ------ | ---- |
| MESSAGE_TYPE |   string    |        |      |
| MESSAGE      |   string    |        |      |
| RUECK        |   string    |        |      |

回傳表格有資料時更新log

SQL-LOG:
```sql
UPDATE PS_R 
SET ERP_FLAG='Y',ERP_RTN_TYPE = {SAP-1.MESSAGE_TYPE},ERP_RTN_MSG = {SAP-1.MESSAGE},ERP_CONFIRM_NO = {SAP-1.RUECK} 
WHERE PSR_UID = {SQL-2.PSR_UID}
```

如MESSAGE_TYPE等於E：
拋出錯誤 [return 400,SAP_HOURS_ERROR]

# Response 欄位

#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":null
  }
}
```