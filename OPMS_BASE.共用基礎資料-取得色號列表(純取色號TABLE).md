# 共用基礎資料-取得色號列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得色號列表

(因原先 pattern/list join 較多 table ，效能較慢，故分拆此API僅提供 PAT_H 資料給客訴用)
註: 如後續要加欄位，請注意執行時間勿超過2秒較佳

## 修改歷程

| 修改時間 | 內容                            | 修改者 |
|----------|-------------------------------|--------|
| 20230808 | 新增規格                        | Nick   |

## 來源URL及資料格式

| 項目   | 說明               |
|--------|--------------------|
| URL    | /comm/pattern_list |
| method | get                |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。  

N/A

#### Request 範例

N/A

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 參考 SQL-1 查回結果 result
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
SELECT DISTINCT PATTERN_NO FROM PAT_H
```


# Response 欄位

#### 【content】array
| 欄位        | 名稱 | 資料型別 | 來源資料 & 說明 |
|-------------|----|----------|-----------------|
| opPatternNo | 色號 | string   | PATTERN_NO      |

#### Response 範例

```json
{
   "msgCode":null,
   "result":{
      "content":[
         {
            "opPatternNo":"12341B5"
         },
         {
            "opPatternNo":"12341B6"
         }
      ]
   }
}
```

