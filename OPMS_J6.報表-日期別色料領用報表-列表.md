# 報表-日期別色料領用報表-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

統計期間所使用之色料料號、規格以及用量。

## 修改歷程

| 修改時間 | 內容     | 修改者 |
|----------|--------|--------|
| 20230810 | 新增規格 | 黃東俞 |

## 來源URL及資料格式

| 項目   | 說明                    |
|--------|------------------------|
| URL    | /report/daily_pgm_list |
| method | post                   |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱    |  資料型別 | 必填 | 資料儲存 & 說明  |
|--------------|---------|----------|:----:|-----------------|
| opBDate      | 開始日期 | string   |  O   | yyyy/mm/dd      |
| opEDate      | 結束日期 | string   |  O   | yyyy/mm/dd      |
| opQueryPartNo| 色料料號 | string   |  M   |                 |

```json
{
  "opBDate": "2019/01/01",
  "opEDate": "2019/01/31",
  "opQueryPartNo": "04-251-A-BOMDJ110X"
}
```

# Request 後端流程說明

* 取 SYS_USER.ORGANIZATION_ID(登入者工廠別)
* 參考SQL-1撈取資料，opBDate(開始日期) 和 opEDate(結束日期) 為非必填，有填才加入查詢條件，但opQueryPartNo為必填之條件。
* 將取得的資料，再依WO_H.WO_UID欄位去重複，相同的WO_UID只取第一筆
* 計算opSumPgmQty(用量加總)：加總 opPgmQty(色料領用量)，四捨五入至小數點第二位並轉為千分位格式
* opRawQty(原料投入量)和opPgmQty(色料領用量)轉為千分位格式再回傳
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:
```sql
-- ReportQueryDao.getDailyPgmReport
SELECT H.WO_DATE, H.WO_SEQ, H.WO_UID, I.PATTERN_NO, H.WO_NO, H.CUST_NAME, H.RAW_QTY,
    H.RAW_QTY * (P.QUANTITY_PER + ISNULL(P.MODIFY_UNIT_QTY,0)) * (CASE P.UNIT WHEN 'KG' THEN 1000 ELSE 1 END) AS PGM_QTY,
    P.WO_PGM_PART_NO, R.WO_MTR_PART_NO, PRODUNIT.PRODUNIT_NAME
FROM BOM_MTM B
INNER JOIN WO_H H ON H.BOM_UID = B.BOM_UID
INNER JOIN WO_PGM P ON H.WO_UID = P.WO_UID
INNER JOIN ITEM_H I ON B.ITEM_NO = I.ITEM_NO
LEFT JOIN WO_MTR R ON H.WO_UID = R.WO_UID
LEFT JOIN PRODUNIT ON H.PRODUNIT_NO = PRODUNIT.SAP_PRODUNIT
WHERE H.ORGANIZATION_ID = { 登入者工廠別 }
AND P.WO_PGM_PART_NO = { opQueryPartNo }
AND H.WIP_CLASS_CODE NOT LIKE '%重工%'
AND H.WO_DATE >= { opBDate }
AND H.WO_DATE <= { opEDate }
ORDER BY H.WO_DATE, H.WO_SEQ, I.PATTERN_NO, H.WO_UID, R.QUANTITY_PER DESC
```

# Response 欄位
| 欄位    | 名稱     | 資料型別 | 說明     |
|---------|--------|----------|----------|
| msgCode | 訊息代碼 | string   |          |
| result  | 回傳資料 | object   | 進階訊息 |

#### 【result】child node
| 欄位     | 名稱     | 資料型別 | 來源資料 & 說明 |
|----------|--------|----------|-----------------|
| content  | 附帶訊息 | array    |                |


#### 【content】child node

| 欄位         | 名稱        | 資料型別 | 來源資料 & 說明 |
|-------------|------------ |----------|-----------------|
| opSumPgmQty | 用量加總     | string  | 加總色料領用量(opPgmQty)，轉為千分位格式 |
| opPgmData | 色料用量資料 | array   |                                      |

#### 【opPgmData】child node
| 欄位            | 名稱       | 資料型別   | 資料儲存 & 說明          |
|-----------------|------------|-----------|-------------------------|
| opWoDate        |  日期      |  String   | WO_H.WO_DATE yyyy/mm/dd |
| opWoSeq         |  序號      |  String   | WO_H.WO_SEQ             |
| opPatternNo     |  色號      |  String   | ITEM_H.PATTERN_NO       |
| opMtrPartNo     |  原料一    |  String   | WO_MTR.WO_MTR_PART_NO   |
| opCustName      |  客戶      |  String   | WO_H.CUST_NAME          |
| opWoNo          |  工單號碼   |  String   | WO_H.WO_NO              |
| opRawQty        |  原料投入量 |  String   | WO_H.RAW_QTY 千分位格式  |
| opPgmQty        |  色料領用量 |  String   | PGM_QTY  千分位格式      |
| opPgmPartNo     |  色料料號   |  String   | WO_PGM.WO_PGM_PART_NO   |
| opProdunitName  |  生產單位   |  String   | PRODUNIT.PRODUNIT_NAME  |


```json
{
  "msgCode":"",
  "result":{
    "content":{
      "opSumUsage": "777.77",
      "opPgmData": [
        {
          "opWoDate": "2023/03/01",
          "opWoSeq": "1",
          "opPatternNo":"A15911P",
          "opMtrPartNo": "04-111-FP-868HXXXX",
          "poCustName": "環德",
          "opWoNo": "58022346",
          "opRawQty":"1,000",
          "opPgmQty": "7,000",
          "opPgmPartNo": "04-251-A-BOMDJ110X",
          "opProdunitName":"憲成",
        },
        {
          "opWoDate": "2023/03/01",
          "opWoSeq": "2",
          "opPatternNo":"A15911P",
          "opMtrPartNo": "04-111-FP-868HXXXX",
          "opCustName": "環德",
          "opWoNo": "58022347",
          "opRawQty":"2,000",
          "opPgmQty": "6,000",
          "opPgmPartNo": "04-251-A-BOMDJ110X",
          "opProdunitName":"染色課",
        },
        {
          "opWoDate": "2023/03/01",
          "opWoSeq": "3",
          "opPatternNo":"A15911P",
          "opMtrPartNo": "04-111-FP-868HXXXX",
          "opCustName": "環德",
          "opWoNo": "58022348",
          "opRawQty":"3,000",
          "opPgmQty": "8,000",
          "opPgmPartNo": "04-251-A-BOMDJ110X",
          "opProdunitName":"樣品課",
        },
      ]
    }
  }
}
```
