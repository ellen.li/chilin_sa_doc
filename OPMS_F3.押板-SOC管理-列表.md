# 押板-SOC管理-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)


**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容                   | 修改者 |
|----------|----------------------|--------|
| 20230831 | 新增規格               | 黃東俞 |
| 20231115 | 新增【更新日期】也可排序 | Nick   |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /soc/soc_b_list             |
| method | post                        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱        |  資料型別 | 必填 | 資料儲存 & 說明                                                     |
|--------------|-------------|----------|:----:|--------------------------------------------------------------------|
| opItemNo     | 料號        | string   |  O    |                                                                    |
| opEquipId    | 機台        | integer  |  O    | 參考下方說明                                                        |
| opBDate      | 更新日期開始 | string   |  O    | yyyy/mm/dd                                                         |
| opEDate      | 結束日期結束 | string   |  O    | yyyy/mm/dd                                                         |
| opStatus     | 狀態        | string   |  O    | 共用參數 opType='SOC',opParamId='STATUS'                            |
| opPage       | 第幾頁      | integer  |  O    | 預設 1，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推 |
| opPageSize   | 分頁筆數    | integer  |  O    | 預設 10                                                             |
| opOrderField | 排序欄位名稱 | string   |   O   | 預設 "UDT"， 可排序的參數 ， UDT:更新日期 / PUB_DATE:發行日          |
| opOrder      | 排序方式    | string   |  O    | "ASC":升冪排序 / "DESC":降冪排序                                     |

#### Request 範例

```json
{
  "opItemNo": null,
  "opEquipId": null,
  "opBDate": "2019/01/01",
  "opEDate": "2019/12/31",
  "opStatus": null,
  "opPage": 1,
  "opPage": 10
}
```

* 前端機台選項(opEquipId)，由API [C.BOM維護-取得排程機台列表(by工廠別.生產單位.生產型態) /bom/get_ps_equipment_list](./OPMS_C.BOM維護-取得排程機台列表(by工廠別.生產單位.生產型態).md) 取得，傳入參數：
  * opOrganizationId：登入者工廠別
  * opProdType：固定填'B'

# Request 後端流程說明

* 取 { 登入者工廠別 } SYS_USER.ORGANIZATION_ID 欄位
* 參考SQL-1，欄位皆為非必填，有傳入的欄位才需加入SQL篩選
* 只最多取前1000筆資料並進行分頁再回傳資料
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

SQL-1:
```sql
-- SocBQueryDao.getSocBResultList
SELECT S.SOC_B_UID SOC_UID, S.EQUIP_ID, E.EQUIP_NAME, S.ITEM_NO, S.SOC_B_VER SOC_VER, S.STATUS,
S.PUB_DATE, S.PUBLIC_BY, S.UDT, S.UPDATE_BY, U.USER_NAME PUBLIC_NAME, S.SOC_DESC, S.REMARK
FROM SOC_B S
INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
LEFT JOIN ITEM_H I ON S.ITEM_NO = I.ITEM_NO
LEFT JOIN SYS_USER U ON S.PUBLIC_BY = U.USER_ID
WHERE E.ORGANIZATION_ID = { 登入者工廠別 }
AND S.UDT >= { opBDate }
AND S.UDT <= { opEDate }
AND S.EQUIP_ID = { opEquipId }
AND S.ITEM_NO LIKE { %opItemNo% }
AND S.STATUS = { opStatus }

-- 如 {opOrderField}="UDT" OR null => S.UDT
-- 如 {opOrderField}="PUB_DATE"    => S.PUB_DATE
ORDER BY {opOrderField} {opOrder}
```

# Response 欄位
| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
| -------- | -------- | -------- | --------------- |
| pageInfo | 分頁資訊 | object   |                 |
| content  | 附帶訊息 | array    |                 |
#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明 |
| ---------- | ------------ | -------- | --------------- |
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |

#### 【content】array
| 欄位        | 名稱         | 資料型別 | 資料儲存 & 說明    |
|-------------|-------------|---------|--------------------|
| opSocUid    | ID          | string  | SOC_B.SOC_B_UID    |
| opEquipId   | 機台        | string  | SOC_B.EQUIP_ID     |
| opEquipName | 機台名稱     | string  | EQUIP_H.EQUIP_NAME |
| opItemNo    | 料號        | string  | SOC_B.ITEM_NO      |
| opSocVer    | 版號        | string  | SOC_B.SOC_B_VER    |
| opStatus    | 狀態        | string  | SOC_B.STATUS       |
| opUdt       | 更新日期     | string  | SOC_B.UDT  yyyy/mm/dd hh:mm:ss |
| opUpdateBy  | 更新人員     | string  | SOC_B.UPDATE_BY    |
| opPubDate   | 發行日期     | string  | SOC_B.PUB_DATE  yyyy/mm/dd |
| opPublicBy  | 發行人代碼   | string  | SOC_B.PUBLIC_BY    |
| opPublicName| 發行人名稱   | string  | SYS_USER.USER_NAME |
| opRemark    | 備註        | string  | SOC_B.REMARK       |
| opSocDesc   | 原料        | string  | SOC_B.SOC_DESC     |


```json
{
  "msgCode":null,
  "result":{
    "pageInfo": {
      "pageNumber": 1,
      "pageSize": 10,
      "pages": 1,
      "total": 3
    },
    "content": [
      {
        "opSocUid":"6A135E88-190D-4C61-861A-969EFFA37DD6",
        "opEquipId":"42",
        "opEquipName":"片材#3",
        "opItemNo":"DN01XXXXXXXX025001",
        "opSocVer":"1",
        "opStatus":"Y",
        "opUdt":"2019/10/28 09:41:09",
        "opUpdateBy":"A000117",
        "opPubDate":"2019/10/28",
        "opPublicBy":"A000117",
        "opPublicName":"陳嘉峰",
        "opRemark":null,
        "opSocDesc":null
      },
      {
        "opSocUid":"7592DD87-1E08-4FD4-95AE-E7D5760D4CFC",
        "opEquipId":"43",
        "opEquipName":"片材#2",
        "opItemNo":"DN01XXXXXXXX025003",
        "opSocVer":"8",
        "opStatus":"D",
        "opUdt":"2019/09/29 10:47:06",
        "opUpdateBy":"70138",
        "opPubDate":"2019/09/29",
        "opPublicBy":"70138",
        "opPublicName":"李俊賢",
        "opRemark":null,
        "opSocDesc":null
      },
      {
        "opSocUid":"6A135E88-190D-4C61-861A-969EFFA37DD6",
        "opEquipId":"43",
        "opEquipName":"片材#2",
        "opItemNo":"DN01XXXXXXXX025003",
        "opSocVer":"7",
        "opStatus":"D",
        "opUdt":"2019/01/07 10:11:56",
        "opUpdateBy":"70138",
        "opPubDate":"2019/01/07",
        "opPublicBy":"A000117",
        "opPublicName":"陳嘉峰",
        "opRemark":null,
        "opSocDesc":null
      }
    ]
  }
}
```











