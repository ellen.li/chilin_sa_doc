# 複材-SOC管理-列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)


**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容                   | 修改者 |
|----------|----------------------|--------|
| 20230830 | 新增規格               | 黃東俞 |
| 20231115 | 新增【更新日期】也可排序 | Nick   |

## 來源URL及資料格式

| 項目   | 說明                         |
|--------|-----------------------------|
| URL    | /soc/soc_m_list             |
| method | post                        |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位         |  名稱        |  資料型別 | 必填 | 資料儲存 & 說明                                                     |
|--------------|-------------|----------|:----:|--------------------------------------------------------------------|
| opItemNo     | 料號        | string   |  O    |                                                                    |
| opEquipId    | 機台        | integer  |  O    | 參考下方說明                                                        |
| opBDate      | 更新日期開始 | string   |  O    | yyyy/mm/dd                                                         |
| opEDate      | 結束日期結束 | string   |  O    | yyyy/mm/dd                                                         |
| opStatus     | 狀態        | string   |  O    | 共用參數 opType='SOC',opParamId='STATUS'                            |
| opPage       | 第幾頁      | integer  |  O    | 預設 1，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推 |
| opPageSize   | 分頁筆數    | integer  |  O    | 預設 10                                                             |
| opOrderField | 排序欄位名稱 | string   |   O   | 預設 "UDT"， 可排序的參數 ， UDT:更新日期 / PUB_DATE:發行日          |
| opOrder      | 排序方式    | string   |  O     | "ASC":升冪排序 / "DESC":降冪排序                                     |

* 前端機台選項(opEquipId)，由API [C.BOM維護-取得排程機台列表(by工廠別.生產單位.生產型態) /bom/get_ps_equipment_list](./OPMS_C.BOM維護-取得排程機台列表(by工廠別.生產單位.生產型態).md) 取得，傳入參數：
  * opOrganizationId：登入者工廠別
  * opProdType：固定填'M'

```json
{
  "opItemNo": null,
  "opEquipId": null,
  "opBDate": "2019/01/01",
  "opEDate": "2019/12/31",
  "opStatus": null,
  "opPage": 1,
  "opPage": 10
}
```

# Request 後端流程說明

* 取 { 登入者工廠別 } SYS_USER.ORGANIZATION_ID 欄位
* 參考SQL-1，欄位皆為非必填，有傳入的欄位才需加入SQL篩選
* 只最多取前1000筆資料並進行分頁再回傳資料
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

SQL-1:
```sql
-- SocMQueryDao.getSocMResultList
SELECT S.SOC_M_UID SOC_UID, S.EQUIP_ID, E.EQUIP_NAME, S.ITEM_NO, B.BOM_NO, S.SOC_M_VER SOC_VER, S.STATUS, S.REMARK, S.PROD_DESC,
  S.PUB_DATE, S.PUBLIC_BY, S.UDT, S.UPDATE_BY, U.USER_NAME PUBLIC_NAME, I.MTR_NAME SOC_DESC
FROM SOC_M S LEFT JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
LEFT JOIN ITEM_H I ON S.ITEM_NO = I.ITEM_NO
LEFT JOIN SYS_USER U ON S.PUBLIC_BY = U.USER_ID
LEFT JOIN BOM_MTM B ON S.BOM_UID = B.BOM_UID
WHERE E.ORGANIZATION_ID = { 登入者工廠別 }
AND S.UDT >= { opBDate }
AND S.UDT <= { opEDate }
AND S.EQUIP_ID = { opEquipId }
AND S.ITEM_NO like { %opItemNo% }
AND S.STATUS = { opStatus }

-- 如 {opOrderField}="UDT" OR null => S.UDT
-- 如 {opOrderField}="PUB_DATE"    => S.PUB_DATE
ORDER BY {opOrderField} {opOrder}
```

# Response 欄位
| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
| -------- | -------- | -------- | --------------- |
| pageInfo | 分頁資訊 | object   |                 |
| content  | 附帶訊息 | array    |                 |
#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明 |
| ---------- | ------------ | -------- | --------------- |
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |

#### 【content】array
| 欄位        | 名稱         | 資料型別 | 資料儲存 & 說明    |
|-------------|-------------|---------|--------------------|
| opSocUid    | ID          | string  | SOC_M.SOC_M_UID    |
| opEquipId   | 機台        | string  | SOC_M.EQUIP_ID     |
| opEquipName | 機台名稱     | string  | EQUIP_H.EQUIP_NAME |
| opItemNo    | 料號        | string  | SOC_M.ITEM_NO      |
| opBomNo     | 配方BOM     | string  | BOM_MTM.BOM_NO      |
| opSocVer    | 版號        | string  | SOC_M.SOC_M_VER    |
| opStatus    | 狀態        | string  | SOC_M.STATUS       |
| opUdt       | 更新日期     | string  | SOC_M.UDT  yyyy/mm/dd hh:mm:ss |
| opUpdateBy  | 更新人員     | string  | SOC_M.UPDATE_BY    |
| opPubDate   | 發行日期     | string  | SOC_M.PUB_DATE  yyyy/mm/dd |
| opPublicBy  | 發行人代碼   | string  | SOC_M.PUBLIC_BY    |
| opPublicName| 發行人名稱   | string  | SYS_USER.USER_NAME |
| opRemark    | 備註        | string  | SOC_M.REMARK        |
| opProdDesc  | 產品說明     | string  | SOC_M.PROD_DESC    |
| opSocDesc   | 原料        | string  | SOC_M.SOC_DESC     |


```json
{
  "msgCode":null,
  "result":{
    "pageInfo": {
      "pageNumber": 1,
      "pageSize": 10,
      "pages": 1,
      "total": 3
    },
    "content": [
      {
        "opSocUid":"5BBB6876-5805-4F50-B07A-CB3F6EFEEC2A",
        "opEquipId":"45",
        "opEquipName":"染色#3(新機台)",
        "opBomNo":null,
        "opItemNo":"06-199-PC110XXXXXX",
        "opSocVer":"5",
        "opStatus":"Y",
        "opUdt":"2019/08/02 15:54:30",
        "opUpdateBy":"A000346",
        "opPubDate":"2019/08/02",
        "opPublicBy":"A000346",
        "opPublicName":"陳進丁",
        "opRemark":null,
        "opProdDesc": "PC",
        "opSocDesc":null
      },
      {
        "opSocUid":"9D3B73F7-D6B9-4975-B403-73851FE54A33",
        "opEquipId":"45",
        "opEquipName":"染色#3(新機台)",
        "opBomNo":null,
        "opItemNo":"115PXXXOS1BXF17111",
        "opSocVer":"2",
        "opStatus":"Y",
        "opUdt":"2019/10/25 17:04:25",
        "opUpdateBy":"A000346",
        "opPubDate":"2019/10/28",
        "opPublicBy":"A000346",
        "opPublicName":"陳進丁",
        "opRemark":null,
        "opProdDesc":"醫療級PC",
        "opSocDesc": "PC-115P"
      },
      {
        "opSocUid":"B61455CC-52AA-48D2-B6DE-3D2AEC016F5F",
        "opEquipId":"45",
        "opEquipName":"染色#3(新機台)",
        "opBomNo":null,
        "opItemNo":"11PXXXOL1BXF17111",
        "opSocVer":"1",
        "opStatus":"Y",
        "opUdt":"2019/08/01 13:05:53",
        "opUpdateBy":"A000346",
        "opPubDate":"2019/08/01",
        "opPublicBy":"A000346",
        "opPublicName":"陳進丁",
        "opRemark":null,
        "opProdDesc":"醫療級PC",
        "opSocDesc":null
      }
    ]
  }
}
```











