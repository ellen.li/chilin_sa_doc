# 工單列印-取得可列印工單列表
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得可列印工單列表

## 修改歷程

| 修改時間 | 內容                            | 修改者 |
| -------- | ------------------------------- | ------ |
| 20230720 | 新增規格                        | Ellen  |
| 20230726 | 調整Response                    | Ellen  |
| 20230802 | 調整排序方式                    | Ellen  |
| 20230808 | 移除排序欄位opOrderField        | Ellen  |
| 20230814 | 回傳增加工單及銷售數量          | Ellen  |
| 20230815 | 調整 TOP 1000(使用OFFSET FETCH) | Nick   |
| 20230927 | 生產單位改讀SAP_PRODUNIT        | Ellen  |
| 20231002 | 調整取生產單位sql               | Ellen  |
| 20231220 | 過濾使用者工廠別                | Ellen  |
| 20240104 | 料號改成精準搜尋                | Jerry  |
| 20240115 | 投入原料量取至小數點後第二位    | Ellen |

## 來源URL及資料格式

| 項目   | 說明          |
| ------ | ------------- |
| URL    | /wo/opms_list |
| method | post          |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。  
| 欄位         | 名稱         | 資料型別 | 必填  | 來源資料 & 說明                                                                                      |
| ------------ | ------------ | -------- | :---: | ---------------------------------------------------------------------------------------------------- |
| opPatternNo  | 色號         | string   |   O   | 預設 `null`，模糊搜尋                                                                                |
| opItemNo     | 料號         | string   |   O   | 預設 `null`，模糊搜尋                                                                                |
| opWoNo       | 工單號碼     | string   |   O   | 預設 `null`，模糊搜尋                                                                                |
| opActiveFlag | 狀態         | string   |   O   | 預設 `null`，CRTD: 已開立未核發 / REL: 已核發未結案 / TECO: 結案 / DLFL: 已上刪除旗標                |
| opCustPoNo   | 客戶po       | string   |   O   | 預設 `null`，模糊搜尋                                                                                |
| opBDate      | 開始時間     | string   |   O   | 預設 `null`    yyyy/MM/dd                                                                            |
| opEDate      | 結束時間     | string   |   O   | 預設 `null`   yyyy/MM/dd                                                                             |
| opPage       | 第幾頁       | integer  |   O   | 預設`1`，-1:取得全部 / 1  :第一頁 / 2:第二頁 以此類推/ 3:第三頁 以此類推                             |
| opPageSize   | 分頁筆數     | integer  |   O   | 預設 `10`                                                                                            |
| opOrderField | 排序欄位名稱 | string   |   O   | 預設 "WO_DATE"，可排序的參數，WO_DATE:使用日期 / WO_SEQ:序號 / PRINT_FLAG:指圖 / PRINT_FLAG_D:指示書 |
| opOrder      | 排序方式     | string   |   O   | "ASC":升冪排序 / "DESC":降冪排序                                                                     |


#### Request 範例

```json
{
  "opPatternNo":null,
  "opItemNo":null,
  "opWoNo":null,
  "opActiveFlag":"CRTD",
  "opBDate":null,
  "opEDate":null,
  "opPage":1,
  "opPageSize":10,
  "opOrder": "ASC"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* {organizationId} 為登入者工廠別ID
* 使用前端給的條件，參考 SQL 自組【搜尋條件】，如為 null 代表不需組條件
* {offset} 為起始列印 row ，第一頁為 0，第二頁後為 0 + (opPageSize*(opPage-1))
* 參考 SQL-1 查回結果 result
* Response 結果需含分頁資訊，無資料 pageInfo 裡面項目皆給 0，頁數與分頁筆數前端如有提供，依前端判斷給的資料計算相關數值及拆分頁面
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL:

```sql
SELECT *
FROM (
SELECT
  CONVERT(VARCHAR(10),H.WO_DATE,111) WO_DATE,
  H.WO_UID,
  H.WO_NO,
  H.WO_SEQ,
  H.ACTIVE_FLAG,
  H.GRADE_NO,
  H.RAW_QTY,
  H.WO_QTY,
  H.SALES_QTY,
  ISNULL(H.PRINT_FLAG,'N') PRINT_FLAG,
  ISNULL(H.PRINT_FLAG_D,'N') PRINT_FLAG_D,
  H.CUST_NAME,
  H.CUST_PO_NO,
  H.INDIR_CUST,
  B.BOM_NO,
  I.PATTERN_NO,
  I.ITEM_NO,
  ISNULL(P.PRODUNIT_NAME, PP.PRODUNIT_NAME) PRODUNIT_NAME,
  S.ASS_DESC OS_LOT 
FROM
  WO_H H
  INNER JOIN BOM_MTM B ON H.BOM_UID = B.BOM_UID
  INNER JOIN ITEM_H I ON B.ITEM_NO = I.ITEM_NO
  LEFT JOIN PRODUNIT P ON H.PRODUNIT_NO = P.SAP_PRODUNIT
  LEFT JOIN PRODUNIT PP ON H.PRODUNIT_NO = PP.PRODUNIT_NO
  LEFT JOIN ( SELECT DISTINCT WO_UID, ASS_DESC FROM WO_SIS WHERE LIST_HEADER_ID = '159' ) S ON H.WO_UID= S.WO_UID 
WHERE H.ORGANIZATION_ID = {organizationId}
    --搜尋條件，請依前端變數是否為 null 決定是否要組

    --如{opPatternNo}非 null 則加入以下SQL
    AND PATTERN_NO LIKE %{opPatternNo}%
    --如{opItemNo}非 null 則加入以下SQL
    AND ITEM_NO = {opItemNo}
    --如{opWoNo}非 null 則加入以下SQL
    AND WO_NO LIKE %{opWoNo}%
    --如{opActiveFlag}非 null 則加入以下SQL
    AND ACTIVE_FLAG = {opActiveFlag}
    --如{opCustPoNo}非 null 則加入以下SQL
    AND CUST_PO_NO LIKE %{opCustPoNo}%
    --如{opBDate}非 null 則加入以下SQL
    AND WO_DATE >= CONVERT(DATETIME,{opBDate})
    --如{opEDate}非 null 則加入以下SQL 
    AND WO_DATE < CONVERT(DATETIME,{opEDate})+1 
ORDER BY {opOrderField} {opOrder}
OFFSET 0 ROWS
FETCH NEXT 1000 ROWS ONLY
    ) AS TOP1000T
ORDER BY (SELECT NULL) OFFSET { offset } ROWS FETCH NEXT { opageSize } ROWS ONLY
```


# Response 欄位
| 欄位     | 名稱     | 資料型別 | 資料儲存 & 說明 |
| -------- | -------- | -------- | --------------- |
| pageInfo | 分頁資訊 | object   |                 |
| content  | 附帶訊息 | array    |                 |
#### 【pageInfo】child node
| 欄位       | 名稱         | 資料型別 | 來源資料 & 說明 |
| ---------- | ------------ | -------- | --------------- |
| pageNumber | 目前所在分頁 | int      |                 |
| pageSize   | 分頁筆數大小 | int      |                 |
| pages      | 總分頁數     | int      |                 |
| total      | 總資料筆數   | int      |                 |

#### 【content】array
| 欄位           | 名稱           | 資料型別 | 來源資料 & 說明               |
| -------------- | -------------- | -------- | ----------------------------- |
| opDate         | 使用日期       | string   | H.WO_DATE (YYYY/MM/DD)        |
| opWoSeq        | 序號           | string   | H.WO_SEQ，不存在填0           |
| opWoUid        | opms工單ID     | string   | H.WO_UID                      |
| opPrintFlag    | 是否有指圖     | string   | H.PRINT_FLAG， Y/N            |
| opPrintFlagD   | 是否有指示書   | string   | H.PRINT_FLAG_D，Y/N           |
| opPatternNo    | 色號           | string   | I.PATTERN_NO                  |
| opItemNo       | 料號           | string   | I.ITEM_NO                     |
| opGradeNo      | 原料 (gradeNo) | string   | H.GRADE_NO                    |
| opProdunitName | 生產單位       | string   | P.PRODUNIT_NAME               |
| opWoNo         | 工單號碼       | string   | H.WO_NO，過濾前綴0            |
| opBomNo        | BOM代碼        | string   | B.BOM_NO                      |
| opOsLot        | 委外標籤LOT    | string   | S.ASS_DESC                    |
| opWoQty        | 工單數量       | string   | WO_QTY                        |
| opSalesQty     | 銷售數量       | string   | SALES_QTY                     |
| opRawQty       | 投入原料量     | string   | H.RAW_QTY，取至小數點後第二位 |
| opCustName     | 客戶           | string   | H.CUST_NAME                   |
| opCustPoNo     | 客戶po單號     | string   | H.CUST_PO_NO                  |
| opIndirCust    | 間接客戶       | string   | H.INDIR_CUST                  |


#### Response 範例

```json
{
    "msgCode": null,
    "result": {
      "pageInfo": {
        "pageNumber": 1, 
        "pageSize": 10,
        "pages": 1,      
        "total": 2     
      },
      "content": [
        {
          "opDate": "2016/06/13",
          "opWoSeq": "31",
          "opWoUid": "01B9E3DF-68E5-4A3C-BDD1-84F8EA308C7B",
          "opPrintFlag": "N",
          "opPrintFlagD": "Y",
          "opPatternNo": "H17307B6",
          "opItemNo": "L80NXXXMAX25H16548",
          "opGradeNo": "GA-1535",
          "opProdunitName": "染色課",
          "opWoNo": "58028534",
          "opBomNo": "10",
          "opOsLot": "0T53Y075",
          "opRawQty": "275.2",
          "opCustName": "奇美實業",
          "opCustPoNo": "5500040161",
          "opIndirCust": ""
        }
      ]
    }
}
```

