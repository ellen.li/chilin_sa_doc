# BOM維護-取得料號資訊
###### tags: `OPMS`
[返回總覽](.https://hackmd.io//OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得料號資訊

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230525 | 新增規格 | Tim   |

## 來源URL及資料格式

| 項目   | 說明             |
| ------ | ---------------- |
| URL    | /bom/get_item_info |
| method | post             |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填。  
| 欄位| 名稱| 資料型別 | 必填(非必填節點不需存在) | 來源資料 & 說明|
| --            | --            | --     |:-:|               - |
|  opItemNo     | 料號           |string  |M|                   |


#### Request 範例

```json
{
  "opItemNo":"H16548MM"  
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 使用前端給的條件 參考 SQL-1 查回結果 , 欄位Weight[重量] 參考 SQL-2 查回結果
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
SELECT ITEM_NO,UNIT ITEM_UOM,CMDI_NO,CMDI_NAME,P.COLOR,A.PATTERN_NO COLOR_NO,ITEM_DESC DESCRIPTION 
	    			  FROM ITEM_H A 
	    			  INNER JOIN PAT_H P ON A.PATTERN_NO = P.PATTERN_NO 
	    			  WHERE A.ITEM_NO = {opItemNo}
```

SQL-2:

```sql
SELECT WEIGHT FROM ITEM_H WHERE ITEM_NO = {opItemNo}
```


# Response 欄位
| 欄位          | 名稱        | 資料型別     | 必填(非必填節點不需存在) | 來源資料 & 說明|
| ------------ | ---------- | ----------- | -------------------- |------------- |
|opCmdiNo      |品號         | string      |M                     | CMDI_NO      |
|opCmdiName    |品稱         | string      |M                     | CMDI_NAME    |
|opColor       |顏色         | string      |M                     | COLOR        |
|opColorNo     |色號         | string      |M                     | COLOR_NO     |
|opDescription |料號摘要      | string      |M                     | DESCRIPTION  |
|opItemUom     |單位         | string      |M                     | ITEM_UOM     |
|opWeight      |重量         | int         |M                     | WEIGHT       |



#### Response 範例

```json
{
    "msgCode": null,
    "result": {
        "opCmdiNo": "LB01",
        "opCmdiName": "ABS 色粒",
        "opColor": "WHITE",
        "opColorNo": "A09A34B7",
        "opDescription": "染色保稅.A09B04B0.PA-707.WHITE",
        "opItemUom": "KG",
        "opWeight": "2"
    }
}
```

