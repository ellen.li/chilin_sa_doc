# 複材-SOC管理-查詢單筆
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 修改歷程

| 修改時間 | 內容                           | 修改者 |
| -------- | ------------------------------ | ------ |
| 20230830 | 新增規格                       | 黃東俞 |
| 20231016 | 移除多餘欄位                   | Ellen  |
| 20231026 | 複材押出機溫度條件遺漏 C13~C15 | Nick   |

## 來源URL及資料格式

| 項目   | 說明              |
| ------ | ----------------- |
| URL    | /soc/soc_m_single |
| method | post              |


## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)

# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。
| 欄位      | 名稱 | 資料型別 | 必填  | 資料儲存 & 說明 |
| --------- | ---- | -------- | :---: | --------------- |
| opSocMUid | ID   | string   |   M   |                 |

#### Request 範例

```json
{
  "opSocMUid": "5BBB6876-5805-4F50-B07A-CB3F6EFEEC2A"
}
```

# Request 後端流程說明

* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* 參考 SQL-1 取得資料並回傳
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明

SQL-1:

```sql
-- SocMQueryDao.getSocMSingle
SELECT E.ORGANIZATION_ID, S.EQUIP_ID, S.BOM_UID, B.BOM_NO, E.EQUIP_NAME, S.ITEM_NO, S.SOC_M_UID, S.SOC_M_VER, S.STATUS, S.MOLD_SCREEN_SIZE, S.SOAK_LENGTH,
  S.SCREW_TYPE, S.MOLD_HOLE, S.MOLD_APETURE, S.CUTTER_TYPE, S.CUT_TYPE, S.SCREEN_APETURE, S.FEEDER_QTY, S.FEEDER_UNIT, S.METAL_CHECK, S.PROD_DESC,
  S.F_1_MTR_DESC, S.F_1_MTR_RATE, S.F_1_MTR_MIX, S.F_1_SCREW,
  S.F_2_MTR_DESC, S.F_2_MTR_RATE, S.F_2_MTR_MIX, S.F_2_SCREW,
  S.F_3_MTR_DESC, S.F_3_MTR_RATE, S.F_3_MTR_MIX, S.F_3_SCREW,
  S.F_4_MTR_DESC, S.F_4_MTR_RATE, S.F_4_MTR_MIX, S.F_4_SCREW,
  S.F_5_MTR_DESC, S.F_5_MTR_RATE, S.F_5_MTR_MIX, S.F_5_SCREW,
  S.F_6_MTR_DESC, S.F_6_MTR_RATE, S.F_6_MTR_MIX, S.F_6_SCREW,
  S.C_1_STD, S.C_1_MAX, S.C_1_MIN, S.C_2_STD, S.C_2_MAX, S.C_2_MIN, S.C_3_STD, S.C_3_MAX, S.C_3_MIN,
  S.C_4_STD, S.C_4_MAX, S.C_4_MIN, S.C_5_STD, S.C_5_MAX, S.C_5_MIN, S.C_6_STD, S.C_6_MAX, S.C_6_MIN,
  S.C_7_STD, S.C_7_MAX, S.C_7_MIN, S.C_8_STD, S.C_8_MAX, S.C_8_MIN, S.C_9_STD, S.C_9_MAX, S.C_9_MIN,
  S.C_A_STD, S.C_A_MAX, S.C_A_MIN, S.C_B_STD, S.C_B_MAX, S.C_B_MIN, S.C_C_STD, S.C_C_MAX, S.C_C_MIN,
  S.C_D_STD, S.C_D_MAX, S.C_D_MIN, S.C_E_STD, S.C_E_MAX, S.C_E_MIN, S.C_F_STD, S.C_F_MAX, S.C_F_MIN,
  S.F_1_STD, S.F_1_MAX, S.F_1_MIN, S.F_2_STD, S.F_2_MAX, S.F_2_MIN, S.F_3_STD, S.F_3_MAX, S.F_3_MIN,
  S.F_4_STD, S.F_4_MAX, S.F_4_MIN, S.F_5_STD, S.F_5_MAX, S.F_5_MIN, S.F_6_STD, S.F_6_MAX, S.F_6_MIN,
  S.GRANULATOR_RPM_STD, S.GRANULATOR_RPM_MAX, S.GRANULATOR_RPM_MIN,
  S.SCREW_RPM_STD, S.SCREW_RPM_MAX, S.SCREW_RPM_MIN, S.SIDE_RPM_STD, S.SIDE_RPM_MAX, S.SIDE_RPM_MIN, S.WRENCH_RATE, S.RESIN_TEMP, S.DIE_PRESSURE,
  S.VACUUM_INDEX_STD, S.VACUUM_INDEX_MAX, S.VACUUM_INDEX_MIN,
  S.BDP_STD, S.BDP_MAX, S.BDP_MIN, S.B_TEMP_STD, S.B_TEMP_MAX, S.B_TEMP_MIN, S.S_TEMP_STD, S.S_TEMP_MAX, S.S_TEMP_MIN, S.E_TEMP_STD, S.E_TEMP_MAX, S.E_TEMP_MIN,
  S.BDP_EXTRA_RATE, S.SCREEN_FREQ, S.RECORD_FREQ,S.MFG_NOTICE, S.REMARK, S.FILE_NAME, S.MIX_DESC, S.MFG_DESC,
  S.CDT, S.CREATE_BY, S.PUB_DATE, S.PUBLIC_BY, S.UDT, S.UPDATE_BY, U.USER_NAME PUBLIC_NAME
FROM SOC_M S
INNER JOIN EQUIP_H E ON S.EQUIP_ID = E.EQUIP_ID
LEFT JOIN SYS_USER U ON S.PUBLIC_BY = U.USER_ID
LEFT JOIN BOM_MTM B ON S.BOM_UID = B.BOM_UID
WHERE S.SOC_M_UID = { opSocMUid }
```

# Response 欄位
| 欄位               | 名稱                | 資料型別 | 資料儲存 & 說明                |
| ------------------ | ------------------- | -------- | ------------------------------ |
| opSocMUid          | ID                  | string   | SOC_M.SOC_M_UID                |
| opEquipId          | 機台                | string   | SOC_M.EQUIP_ID                 |
| opEquipName        | 機台名稱            | string   | EQUIP_H.EQUIP_NAME             |
| opItemNo           | 料號                | string   | SOC_M.ITEM_NO                  |
| opBomUid           | BOM ID              | string   | SOC_M.BOM_UID                  |
| opBomNo            | 配方BOM             | string   | BOM_MTM.BOM_NO                 |
| opSocMVer          | 版號                | string   | SOC_M.SOC_M_VER                |
| opStatus           | 狀態                | string   | SOC_M.STATUS                   |
| opMoldScreenSize   | 模頭濾網            | string   | SOC_M.MOLD_SCREEN_SIZE         |
| opScrewType        | 螺桿型號            | string   | SOC_M.SCREW_TYPE               |
| opMoldHole         | 模頭孔數            | string   | SOC_M.MOLD_HOLE                |
| opMoldApeture      | 模頭孔徑            | string   | SOC_M.MOLD_APETURE             |
| opCutterType       | 切刀型態            | string   | SOC_M.CUTTER_TYPE              |
| opCutType          | 造粒方式            | string   | SOC_M.CUT_TYPE                 |
| opScreenApeture    | 篩網孔徑            | string   | SOC_M.SCREEN_APETURE           |
| opFeederQty        | 餵料機下料量        | string   | SOC_M.FEEDER_QTY               |
| opFeederUnit       | 下料單位            | string   | SOC_M.FEEDER_UNIT              |
| opProdDesc         | 產品說明            | string   | SOC_M.PROD_DESC                |
| opF1MtrDesc        | F1餵料機說明        | string   | SOC_M.F_1_MTR_DESC             |
| opF1MtrRate        | F1下料比例          | string   | SOC_M.F_1_MTR_RATE             |
| opF1MtrMix         | F1混合方式          | string   | SOC_M.F_1_MTR_MIX              |
| opF1Screw          | F1餵料螺桿          | string   | SOC_M.F_1_SCREW                |
| opF2MtrDesc        | F2餵料機說明        | string   | SOC_M.F_2_MTR_DESC             |
| opF2MtrRate        | F2下料比例          | string   | SOC_M.F_2_MTR_RATE             |
| opF2MtrMix         | F2混合方式          | string   | SOC_M.F_2_MTR_MIX              |
| opF2Screw          | F2餵料螺桿          | string   | SOC_M.F_2_SCREW                |
| opF3MtrDesc        | F3餵料機說明        | string   | SOC_M.F_3_MTR_DESC             |
| opF3MtrRate        | F3下料比例          | string   | SOC_M.F_3_MTR_RATE             |
| opF3MtrMix         | F3混合方式          | string   | SOC_M.F_3_MTR_MIX              |
| opF3Screw          | F3餵料螺桿          | string   | SOC_M.F_3_SCREW                |
| opF4MtrDesc        | F4餵料機說明        | string   | SOC_M.F_4_MTR_DESC             |
| opF4MtrRate        | F4下料比例          | string   | SOC_M.F_4_MTR_RATE             |
| opF4MtrMix         | F4混合方式          | string   | SOC_M.F_4_MTR_MIX              |
| opF4Screw          | F4餵料螺桿          | string   | SOC_M.F_4_SCREW                |
| opF5MtrDesc        | F5餵料機說明        | string   | SOC_M.F_5_MTR_DESC             |
| opF5MtrRate        | F5下料比例          | string   | SOC_M.F_5_MTR_RATE             |
| opF5MtrMix         | F5混合方式          | string   | SOC_M.F_5_MTR_MIX              |
| opF5Screw          | F5餵料螺桿          | string   | SOC_M.F_5_SCREW                |
| opF6MtrDesc        | F6餵料機說明        | string   | SOC_M.F_6_MTR_DESC             |
| opF6MtrRate        | F6下料比例          | string   | SOC_M.F_6_MTR_RATE             |
| opF6MtrMix         | F6混合方式          | string   | SOC_M.F_6_MTR_MIX              |
| opF6Screw          | F6餵料螺桿          | string   | SOC_M.F_6_SCREW                |
| opC1Std            | (押出機溫度)C1標準  | string   | SOC_M.C_1_STD                  |
| opC1Max            | (押出機溫度)C1上限  | string   | SOC_M.C_1_MAX                  |
| opC1Min            | (押出機溫度)C1下限  | string   | SOC_M.C_1_MIN                  |
| opC2Std            | (押出機溫度)C2標準  | string   | SOC_M.C_2_STD                  |
| opC2Max            | (押出機溫度)C2上限  | string   | SOC_M.C_2_MAX                  |
| opC2Min            | (押出機溫度)C2下限  | string   | SOC_M.C_2_MIN                  |
| opC3Std            | (押出機溫度)C3標準  | string   | SOC_M.C_3_STD                  |
| opC3Max            | (押出機溫度)C3上限  | string   | SOC_M.C_3_MAX                  |
| opC3Min            | (押出機溫度)C3下限  | string   | SOC_M.C_3_MIN                  |
| opC4Std            | (押出機溫度)C4標準  | string   | SOC_M.C_4_STD                  |
| opC4Max            | (押出機溫度)C4上限  | string   | SOC_M.C_4_MAX                  |
| opC4Min            | (押出機溫度)C4下限  | string   | SOC_M.C_4_MIN                  |
| opC5Std            | (押出機溫度)C5標準  | string   | SOC_M.C_5_STD                  |
| opC5Max            | (押出機溫度)C5上限  | string   | SOC_M.C_5_MAX                  |
| opC5Min            | (押出機溫度)C5下限  | string   | SOC_M.C_5_MIN                  |
| opC6Std            | (押出機溫度)C6標準  | string   | SOC_M.C_6_STD                  |
| opC6Max            | (押出機溫度)C6上限  | string   | SOC_M.C_6_MAX                  |
| opC6Min            | (押出機溫度)C6下限  | string   | SOC_M.C_6_MIN                  |
| opC7Std            | (押出機溫度)C7標準  | string   | SOC_M.C_7_STD                  |
| opC7Max            | (押出機溫度)C7上限  | string   | SOC_M.C_7_MAX                  |
| opC7Min            | (押出機溫度)C7下限  | string   | SOC_M.C_7_MIN                  |
| opC8Std            | (押出機溫度)C8標準  | string   | SOC_M.C_8_STD                  |
| opC8Max            | (押出機溫度)C8上限  | string   | SOC_M.C_8_MAX                  |
| opC8Min            | (押出機溫度)C8下限  | string   | SOC_M.C_8_MIN                  |
| opC9Std            | (押出機溫度)C9標準  | string   | SOC_M.C_9_STD                  |
| opC9Max            | (押出機溫度)C9上限  | string   | SOC_M.C_9_MAX                  |
| opC9Min            | (押出機溫度)C9下限  | string   | SOC_M.C_9_MIN                  |
| opCAStd            | (押出機溫度)C10標準 | string   | SOC_M.C_A_STD                  |
| opCAMax            | (押出機溫度)C10上限 | string   | SOC_M.C_A_MAX                  |
| opCAMin            | (押出機溫度)C10下限 | string   | SOC_M.C_A_MIN                  |
| opCBStd            | (押出機溫度)C11標準 | string   | SOC_M.C_B_STD                  |
| opCBMax            | (押出機溫度)C11上限 | string   | SOC_M.C_B_MAX                  |
| opCBMin            | (押出機溫度)C11下限 | string   | SOC_M.C_B_MIN                  |
| opCCStd            | (押出機溫度)C12標準 | string   | SOC_M.C_C_STD                  |
| opCCMax            | (押出機溫度)C12上限 | string   | SOC_M.C_C_MAX                  |
| opCCMin            | (押出機溫度)C12下限 | string   | SOC_M.C_C_MIN                  |
| opCDStd            | (押出機溫度)C13標準 | string   | SOC_M.C_D_STD                  |
| opCDMax            | (押出機溫度)C13上限 | string   | SOC_M.C_D_MAX                  |
| opCDMin            | (押出機溫度)C13下限 | string   | SOC_M.C_D_MIN                  |
| opCEStd            | (押出機溫度)C14標準 | string   | SOC_M.C_E_STD                  |
| opCEMax            | (押出機溫度)C14上限 | string   | SOC_M.C_E_MAX                  |
| opCEMin            | (押出機溫度)C14下限 | string   | SOC_M.C_E_MIN                  |
| opCFStd            | (押出機溫度)C15標準 | string   | SOC_M.C_F_STD                  |
| opCFMax            | (押出機溫度)C15上限 | string   | SOC_M.C_F_MAX                  |
| opCFMin            | (押出機溫度)C15下限 | string   | SOC_M.C_F_MIN                  |
| opF1Std            | F1標準              | string   | SOC_M.F_1_STD                  |
| opF1Max            | F1上限              | string   | SOC_M.F_1_MAX                  |
| opF1Min            | F1下限              | string   | SOC_M.F_1_MIN                  |
| opF2Std            | F2標準              | string   | SOC_M.F_2_STD                  |
| opF2Max            | F2上限              | string   | SOC_M.F_2_MAX                  |
| opF2Min            | F2下限              | string   | SOC_M.F_2_MIN                  |
| opF3Std            | F3標準              | string   | SOC_M.F_3_STD                  |
| opF3Max            | F3上限              | string   | SOC_M.F_3_MAX                  |
| opF3Min            | F3下限              | string   | SOC_M.F_3_MIN                  |
| opF4Std            | F4標準              | string   | SOC_M.F_4_STD                  |
| opF4Max            | F4上限              | string   | SOC_M.F_4_MAX                  |
| opF4Min            | F4下限              | string   | SOC_M.F_4_MIN                  |
| opF5Std            | F5標準              | string   | SOC_M.F_5_STD                  |
| opF5Max            | F5上限              | string   | SOC_M.F_5_MAX                  |
| opF5Min            | F5下限              | string   | SOC_M.F_5_MIN                  |
| opF6Std            | F6標準              | string   | SOC_M.F_6_STD                  |
| opF6Max            | F6上限              | string   | SOC_M.F_6_MAX                  |
| opF6Min            | F6下限              | string   | SOC_M.F_6_MIN                  |
| opScrewRpmStd      | 螺桿轉速標準        | string   | SOC_M.SCREW_RPM_STD            |
| opScrewRpmMax      | 螺桿轉速上限        | string   | SOC_M.SCREW_RPM_MAX            |
| opScrewRpmMin      | 螺桿轉速下限        | string   | SOC_M.SCREW_RPM_MIN            |
| opSideRpmStd       | 側餵料機轉速標準    | string   | SOC_M.SIDE_RPM_STD             |
| opSideRpmMax       | 側餵料機轉速上限    | string   | SOC_M.SIDE_RPM_MAX             |
| opSideRpmMin       | 側餵料機轉速下限    | string   | SOC_M.SIDE_RPM_MIN             |
| opGranulatorRpmStd | 切粒機標準          | string   | SOC_M.GRANULATOR_RPM_STD       |
| opGranulatorRpmMax | 切粒機上限          | string   | SOC_M.GRANULATOR_RPM_MAX       |
| opGranulatorRpmMin | 切粒機下限          | string   | SOC_M.GRANULATOR_RPM_MIN       |
| opWrenchRate       | 扭力                | string   | SOC_M.WRENCH_RATE              |
| opResinTemp        | 樹脂溫度            | string   | SOC_M.RESIN_TEMP               |
| opDiePressure      | 模頭壓力            | string   | SOC_M.DIE_PRESSURE             |
| opVacuumIndexStd   | 真空指數標準        | string   | SOC_M.VACUUM_INDEX_STD         |
| opVacuumIndexMax   | 真空指數上限        | string   | SOC_M.VACUUM_INDEX_MAX         |
| opVacuumIndexMin   | 真空指數下限        | string   | SOC_M.VACUUM_INDEX_MIN         |
| opBdpStd           | BDP標準             | string   | SOC_M.BDP_STD                  |
| opBdpMax           | BDP上限             | string   | SOC_M.BDP_MAX                  |
| opBdpMin           | BDP下限             | string   | SOC_M.BDP_MIN                  |
| opBTempStd         | (第二段水溫)大槽水溫標準值      | string   | SOC_M.B_TEMP_STD               |
| opBTempMax         | (第二段水溫)大槽水溫上限        | string   | SOC_M.B_TEMP_MAX               |
| opBTempMin         | (第二段水溫)大槽水溫下限        | string   | SOC_M.B_TEMP_MIN               |
| opSTempStd         | (第一段水溫)小槽水溫標準值      | string   | SOC_M.S_TEMP_STD               |
| opSTempMax         | (第一段水溫)小槽水溫上限        | string   | SOC_M.S_TEMP_MAX               |
| opSTempMin         | (第一段水溫)小槽水溫下限        | string   | SOC_M.S_TEMP_MIN               |
| opETempStd         | 第三段水溫標準值    | string   | SOC_M.E_TEMP_STD               |
| opETempMax         | 第三段水溫上限      | string   | SOC_M.E_TEMP_MAX               |
| opETempMin         | 第三段水溫下限      | string   | SOC_M.E_TEMP_MIN               |
| opBdpExtraRate     | BDP外加比例         | string   | SOC_M.BDP_EXTRA_RATE           |
| opScreenFreq       | 網目更換            | string   | SOC_M.SCREEN_FREQ              |
| opRecordFreq       | 紀錄間隔            | string   | SOC_M.RECORD_FREQ              |
| opCdt              | 建立日期            | string   | SOC_M.CDT yyyy/mm/dd hh:mm:ss  |
| opCreateBy         | 建立人員            | string   | SOC_M.CREATE_BY                |
| opUdt              | 更新日期            | string   | SOC_M.UDT  yyyy/mm/dd hh:mm:ss |
| opUpdateBy         | 更新人員            | string   | SOC_M.UPDATE_BY                |
| opPubDate          | 發行日期            | string   | SOC_M.PUB_DATE  yyyy/mm/dd     |
| opPublicBy         | 發行人代碼          | string   | SOC_M.PUBLIC_BY                |
| opPublicName       | 發行人名稱          | string   | SYS_USER.USER_NAME             |
| opRemark           | 備註                | string   | SOC_M.UDT                      |
| opMfgNotice        | 作業注意事項        | string   | SOC_M.MFG_NOTICE               |
| opFileName         | 圖面                | string   | SOC_M.FILE_NAME                |
| opMfgDesc          | 生產方式說明        | string   | SOC_M.MFG_DESC                 |
| opMetalCheck       | 金檢                | string   | SOC_M.METAL_CHECK              |
| opOrganizationId   | 工廠別              | string   | EQUIP_H.ORGANIZATION_ID        |


#### Response 範例

```json
{
  "msgCode":null,
  "result":{
    "content":{
      "opSocMUid":"5BBB6876-5805-4F50-B07A-CB3F6EFEEC2A",
      "opEquipId":"45",
      "opEquipName":"染色#3(新機台)",
      "opItemNo":"06-199-PC110XXXXXX",
      "opBomUid":null,
      "opBomNo":null,
      "opSocMVer":"5",
      "opStatus":"Y",
      "opMoldScreenSize":"50/50mesh",
      "opScrewType":"PC-365-1",
      "opMoldHole":"17",
      "opMoldApeture":"4.5",
      "opCutterType":"S/W",
      "opCutType":"牽條",
      "opScreenApeture":"5",
      "opFeederQty":"300.00",
      "opFeederUnit":"Kg/hr",
      "opProdDesc":"PC",
      "opF1MtrDesc":"主原料+副料",
      "opF1MtrRate":"100%",
      "opF1MtrMix":null,
      "opF1Screw":null,
      "opF2MtrDesc":null,
      "opF2MtrRate":null,
      "opF2MtrMix":null,
      "opF2Screw":null,
      "opF3MtrDesc":"-",
      "opF3MtrRate":"-",
      "opF3MtrMix":"-",
      "opF3Screw":"-",
      "opF4MtrDesc":null,
      "opF4MtrRate":null,
      "opF4MtrMix":null,
      "opF4Screw":null,
      "opF5MtrDesc":null,
      "opF5MtrRate":null,
      "opF5MtrMix":null,
      "opF5Screw":null,
      "opF6MtrDesc":null,
      "opF6MtrRate":null,
      "opF6MtrMix":null,
      "opF6Screw":null,
      "opC1Std":"180",
      "opC1Max":"190",
      "opC1Min":"170",
      "opC2Std":"195",
      "opC2Max":"205",
      "opC2Min":"185",
      "opC3Std":"200",
      "opC3Max":"210",
      "opC3Min":"190",
      "opC4Std":"215",
      "opC4Max":"225",
      "opC4Min":"205",
      "opC5Std":"220",
      "opC5Max":"230",
      "opC5Min":"210",
      "opC6Std":"215",
      "opC6Max":"225",
      "opC6Min":"205",
      "opC7Std":"220",
      "opC7Max":"230",
      "opC7Min":"210",
      "opC8Std":"215",
      "opC8Max":"225",
      "opC8Min":"205",
      "opC9Std":"210",
      "opC9Max":"220",
      "opC9Min":"200",
      "opCAStd":"220",
      "opCAMax":"230",
      "opCAMin":"210",
      "opCBStd":"225",
      "opCBMax":"235",
      "opCBMin":"215",
      "opCCStd":null,
      "opCCMax":null,
      "opCCMin":null,
      "opF1Std":"300.00",
      "opF1Max":"350.00",
      "opF1Min":"200.00",
      "opF2Std":"0.00",
      "opF2Max":"0.00",
      "opF2Min":"0.00",
      "opF3Std":null,
      "opF3Max":"0.00",
      "opF3Min":"0.00",
      "opF4Std":null,
      "opF4Max":null,
      "opF4Min":null,
      "opF5Std":null,
      "opF5Max":null,
      "opF5Min":null,
      "opF6Std":null,
      "opF6Max":null,
      "opF6Min":null,
      "opScrewRpmStd":"300",
      "opScrewRpmMax":"350",
      "opScrewRpmMin":"200",
      "opSideRpmStd":null,
      "opSideRpmMax":null,
      "opSideRpmMin":null,
      "opGranulatorRpmStd":null,
      "opGranulatorRpmMax":null,
      "opGranulatorRpmMin":null,
      "opWrenchRate":"200",
      "opResinTemp":null,
      "opDiePressure":null,
      "opVacuumIndexStd":"-50",
      "opVacuumIndexMax":"-55",
      "opVacuumIndexMin":"-45",
      "opBdpStd":null,
      "opBdpMax":"0.000",
      "opBdpMin":"0.000",
      "opBTempStd":"50",
      "opBTempMax":"60",
      "opBTempMin":"40",
      "opSTempStd":"70",
      "opSTempMax":"80",
      "opSTempMin":"60",
      "opETempStd":null,
      "opETempMax":null,
      "opETempMin":null,
      "opBdpExtraRate":null,
      "opScreenFreq":"每24小時",
      "opRecordFreq":"2",
      "opCdt":"2019/08/02 15:54:30",
      "opCreateBy":"A000346",
      "opUdt":"2019/08/02 15:54:30",
      "opUpdateBy":"A000346",
      "opPubDate":"2019-08-02",
      "opPublicBy":"A000346",
      "opPublicName":"陳進丁",
      "opRemark":null,
      "opMfgNotice":"a.此為粉碎料大小不一(有長條狀),3F下料須以濾網過濾及放磁力架 b.因粉碎料較澎鬆,F1本為40Kg補料會溢出,因此改為23Kg補料,生產完工F1請調回40Kg",
      "opFileName":"CLT_50MM_1.jpg",
      "opMfgDesc":"單管下料(#1)",
      "opMetalCheck":"-",
      "opOrganizationId":""
    }
  }
}
```
