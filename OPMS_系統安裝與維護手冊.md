OPMS_系統安裝與維護手冊
===

 Table of Contents

[TOC]

| 修改時間  | 內容                | 修改者 |
|-----------|-------------------|--------|
| 2024/1/12 | 初版 | Jerry   |

## 建立 Jenkins 執行環境與設定
### 安裝 Jenkins

撰寫Dockerfile
```yaml
FROM jenkins/jenkins:lts

USER root

# Update System
RUN apt -y update && apt -y upgrade

# # Install pre-requisites
RUN apt -y install \
  apt-utils \
  gcc \
  make \
  cmake \
  git \
  btrfs-progs \
  # golang-go \
  go-md2man \
  iptables \
  libassuan-dev \
  libc6-dev \
  libdevmapper-dev \
  libglib2.0-dev \
  libgpgme-dev \
  libgpg-error-dev \
  libostree-dev \
  libprotobuf-dev \
  libprotobuf-c-dev \
  libseccomp-dev \
  libselinux1-dev \
  libsystemd-dev \
  pkg-config \
  runc \
  uidmap \
  libapparmor-dev

# Install go
RUN curl https://dl.google.com/go/go1.19.9.linux-amd64.tar.gz -o go1.19.9.linux-amd64.tar.gz && \
    tar -xvf go1.19.9.linux-amd64.tar.gz && \
    chown -R root:root ./go && \
    mv go /usr/local
    
RUN ["/bin/bash", "-c", "ln -s /usr/local/go/bin/go /usr/bin/go"]

ENV GOPATH /usr/local/go
ENV PATH /usr/local/go/bin:$PATH

# Install conmon
RUN git clone https://github.com/containers/conmon && \
    cd conmon && \
    make && \
    make podman && \
    cp /usr/local/libexec/podman/conmon  /usr/local/bin/

# Install CNI plugins
RUN git clone https://github.com/containernetworking/plugins.git $GOPATH/src/github.com/containernetworking/plugins && \
	cd $GOPATH/src/github.com/containernetworking/plugins && \
	./build_linux.sh && \
	mkdir -p /usr/libexec/cni && \
	cp bin/* /usr/libexec/cni

#Setup CNI networking
RUN mkdir -p /etc/cni/net.d && \
	curl -qsSL https://raw.githubusercontent.com/containers/libpod/master/cni/87-podman-bridge.conflist | tee /etc/cni/net.d/99-loopback.conf

# Populate configuration files
RUN mkdir -p /etc/containers && \
	curl https://raw.githubusercontent.com/projectatomic/registries/master/registries.fedora -o /etc/containers/registries.conf && \
	curl https://raw.githubusercontent.com/containers/skopeo/master/default-policy.json -o /etc/containers/policy.json


# Install Podman
RUN git clone https://github.com/containers/libpod/ $GOPATH/src/github.com/containers/libpod && \
	cd $GOPATH/src/github.com/containers/libpod && \
	make && \
	make install

RUN echo 'alias docker=podman' >> ~/.bashrc
```

執行 podman指令
```yaml
podman run -u 0 --name jenkins -d --restart always -e CONTAINER_HOST=unix://var/run/podman/podman.sock -p 8080:8080 -p 50000:50000 -v /data:/var/jenkins_home  -v /var/run/podman:/var/run/podman localhost/jenkins-with-podman:2023050801
```

### 設定Jenkins  
在瀏覽器輸入 => http://10.20.30.48:8080 (jenkins指定的Port)  
輸入安裝時顯示的密碼，或到紅底紅字的地方找密碼  
P.S.若後面設定帳號密碼沒設定到，Admin的密碼同這組  
![](./images/Jenkins設定密碼.png)

安裝套件  
輸入選擇要安裝那些套件，基本上新手推薦使用Jenkins預設的即可(左邊)  
或是你已經對Jenkins滿熟悉，知道自己需要什麼不要什麼，那就可以自己選擇(右邊)  
![](./images/Jenkins安裝套件.png)

等待安裝  
![](./images/Jenkins等待安裝.png)

輸入管理者相關資料，按下【Save and Finish】  
此使用者名稱及密碼是下次再登入Jenkins的帳密  
![](./images/Jenkins輸入管理者相關訊息.png)

按下【Start using Jenkins】，進入 Jenkins 主畫面  
![](./images/Jenkins進入主畫面.png)

### 安裝其他外掛
點選 【管理Jenkins】->【管理外掛程式】  
![](./images/Jenkins安裝其他外掛.png)

新增的套件  
點選頁籤 【可用的】，輸入要需要的套件名稱，並將他們打 &#x2705;  
![](./images/Jenkins新增套件.png)

&#x2705;完，點選【安裝後不重新啟動】  
![](./images/Jenkins安裝後不要重開機.png)

### 連接Git設定
點選 【管理Jenkins】->【Manage Credentials】  
![](./images/Jenkins-Manage-Credentials.png)

點選 【System】  
![](./images/Jenkins-System.png)

點選 【Global credentials (unrestricted)】  
![](./images/Jenkins-Global-credentials-(unrestricted).png)

點選 【Add Credentials】  
![](./images/Jenkins-Add-Credentials.png)

輸入gitlab的帳號密碼->Create  
![](./images/Jenkins輸入gitlab帳密.png)

複製ID到Jenkinsfile或是Pipeline Script設定  
![](./images/Jenkins複製ID.png)

### 設定Jenkinsfile參數(opms_front_end)
設定Pipeline Script  
輸入Repo URL  
Credentials選擇前面建立的帳號  
Script Path輸入Jenkinsfile檔案名稱  
![](./images/Jenkins設定Pipeline-Script(frontend).png)

撰寫Jenkinsfile  
```yaml
/* groovylint-disable-next-line CompileStatic */
pipeline {
    agent any

    // Git Parameter plugin https://plugins.jenkins.io/git-parameter/
    parameters {
        /* groovylint-disable-next-line LineLength */
        gitParameter branchFilter: 'origin/(master|release|develop|pre-develop)', defaultValue: 'pre-develop', name: 'BRANCH_NAME', type: 'PT_BRANCH'

        booleanParam(name: 'IS_RUN_SCAN', defaultValue: true, description: 'Set true to run scan')
        booleanParam(name: 'IS_RUN_UNIT_TEST', defaultValue: true, description: 'Set true to run unit test')
    }

    environment {
        APP_NAME = 'opms-front-end'
        GIT_BRANCH = "${params.BRANCH_NAME}"
        IS_RUN_SCAN = "${params.IS_RUN_SCAN}"
        IS_RUN_UNIT_TEST = "${params.IS_RUN_UNIT_TEST}"
        JOB_LINK = "${env.JOB_URL}${env.BUILD_NUMBER}"
        GIT_COMMIT_HASH = sh(script: 'git rev-parse --short HEAD', returnStdout: true).trim()
        BUILD_ENV = getBuildEnv(GIT_BRANCH)
        SONAR_HOST_URL = 'http://10.20.30.199:9000/'
        SONAR_AUTH_TOKEN = 'sqa_ed44ac50393808d5e1ec2bb2d3c23a90e09fe2d1'
        /* groovylint-disable-next-line LineLength */
        NOTIFY_API = "https://discord.com/api/v9/channels/1101147645004742737/messages -H 'Content-Type: application/json' -H 'Authorization: Bot MTA0ODE3MDcxMTEzMjQwOTkzNg.GGGfaB.i16Z2Cxx9JGDiMh3uYkY2PVaNTe9U-Btte3mPU'"
        allCommitMessages = ""
    }

    stages {
        stage('Prepare ENV') {
            steps {
                script {
                    sh "git checkout ${GIT_BRANCH}"

                    /* groovylint-disable-next-line NoDef, VariableTypeRequired */
                    def version = 'v0.0.0'

                    /* groovylint-disable-next-line NestedBlockDepth */
                    try {
                        /* groovylint-disable-next-line LineLength */
                        version = sh(script: "git describe --abbrev=0 --tags --match 'v[0-9]*.[0-9]*.[0-9]*' ${GIT_BRANCH} 2>/dev/null", returnStdout: true).trim()
                     /* groovylint-disable-next-line CatchException, NestedBlockDepth */
                    } catch (Exception e) {
                        echo "No tags found. Using default version: ${version}"
                    }

                    env.GIT_VERSION_TAG = version
                    env.APP_VERSION = "${GIT_VERSION_TAG}-${GIT_COMMIT_HASH}"

                    /* groovylint-disable-next-line LineLength */
                    env.APP_NAME_WITH_ID = "${APP_NAME}_${GIT_BRANCH}_${APP_VERSION}-${env.BUILD_NUMBER}"
                    env.ARCHIVE_FILE_NAME = "${APP_NAME}_${GIT_BRANCH}_${env.BUILD_NUMBER}"
                }

                script {
                    env.SONAR_PROJECT_NAME = "${APP_NAME}_${GIT_BRANCH}"
                }

                sh 'printenv | sort'
            }
        }

        stage('Start') {
            when {
                expression {
                    return GIT_BRANCH ==~ /(origin\/)?(pre-develop)/
                }
            }
            steps {
                echo "${APP_NAME_WITH_ID} 上版中"

                // 顯示 git commit message
                script {
                    def changeLogSets = currentBuild.changeSets
                    def commitMessages = []
                    def maxMessages = 40
                    def maxMessageLength = 50
                    def maxMessageContentLength = 2000

                    for (int i = 0; i < changeLogSets.size(); i++) {
                        def entries = changeLogSets[i].items
                        for (int j = 0; j < entries.length && commitMessages.size() < maxMessages; j++) {
                            def entry = entries[j]
                            def msg = "${entry.msg}"
                            def author = "${entry.author}"
                            if (msg.length() > maxMessageLength) {
                                msg = msg.substring(0, maxMessageLength - 3) + '...'
                            }
                            commitMessages.add("${j + 1}. ${msg} by ${author}")
                        }
                    }

                    allCommitMessages = commitMessages.join("\n")
                    if (allCommitMessages.length() > maxMessageContentLength) {
                        allCommitMessages = allCommitMessages.substring(0, maxMessageContentLength - 3) + '...'
                    }
                    echo allCommitMessages

                    allCommitMessages = allCommitMessages.replaceAll('\n', '\\\\n').replaceAll('"', '').replaceAll("'", '')

                }
                sh(script: "curl ${NOTIFY_API} -d '{\"content\": \"${APP_NAME_WITH_ID} 上版中\\n變更項目:\\n${allCommitMessages}\"}'")

                // echo "${APP_NAME_WITH_ID} 上版中"
                // sh "curl ${NOTIFY_API} -d '{\"content\": \"${APP_NAME_WITH_ID} 上版中\"}'"
            }
        }

        stage('Build') {
            steps {
                echo 'build image'
                /* groovylint-disable-next-line LineLength */
                sh "podman build -t ${APP_NAME} --build-arg BUILD_ENV=${BUILD_ENV} --build-arg GIT_COMMIT_HASH=${GIT_COMMIT_HASH} --build-arg IS_RUN_UNIT_TEST=${IS_RUN_UNIT_TEST} ."


            }
        }

        stage('Coverage') {
            when {
                expression { return params.IS_RUN_SCAN == true && params.IS_RUN_UNIT_TEST == true }
            }
            steps {
                script {
                    echo 'start coverage'
                    try {
                        // copy coverage to host
                        sh "podman run -d --name copy-coverage ${APP_NAME}:latest && " +
                        "podman cp copy-coverage:/coverage ./coverage && " +
                        "podman stop copy-coverage && " +
                        "podman rm copy-coverage"
                    } catch (Exception e) {
                        // Handle any errors here
                        // currentBuild.result = 'FAILURE' // Set the build result to FAILURE
                        // error("Failed to copy coverage: ${e.getMessage()}")
                        echo "Error during coverage step: ${e.getMessage()}"
                    }
                }
            }
        }

        stage('Scan') {
            when {
                expression { return params.IS_RUN_SCAN == true }
            }
            steps {
                echo 'start sonar-scanner'
                sh 'podman build --rm -f Dockerfile.SonarScanner -t sonar-scanner:latest .'
                /* groovylint-disable-next-line LineLength */
                sh "podman run --rm -t --user sonarqube sonar-scanner:latest -Dsonar.projectKey=$SONAR_PROJECT_NAME -Dsonar.projectName=$SONAR_PROJECT_NAME -Dsonar.projectVersion=$APP_NAME_WITH_ID -Dsonar.host.url=$SONAR_HOST_URL -Dsonar.token=$SONAR_AUTH_TOKEN"
            }
        }

        stage('Deploy') {
            when {
                expression {
                    return GIT_BRANCH ==~ /(origin\/)?(pre-develop)/
                }
            }
            steps {
                echo 'delete old container...'
                sh "podman rm -f ${APP_NAME}"

                echo 'run container'
                sh "podman run -ti -d --name ${APP_NAME} -p 80:80 -p 443:443 ${APP_NAME}"
            }
        }

        stage('Success') {
            when {
                expression {
                    return GIT_BRANCH ==~ /(origin\/)?(pre-develop)/
                }
            }
            steps {
                echo "${APP_NAME_WITH_ID} 上版完成"
                /* groovylint-disable-next-line LineLength */
                sh "curl ${NOTIFY_API} -d '{\"content\": \"${APP_NAME_WITH_ID} 上版完成 版號: ${APP_VERSION}\",\"embeds\": [{\"image\": {\"url\": \"https://c.tenor.com/jq3TOO2LoS0AAAAd/tenor.gif\"}}]}'"
            }
        }

        stage('Archive image') {
            when {
                expression {
                    return GIT_BRANCH ==~ /(origin\/)?(master|release|develop)/
                }
            }

            steps {
                echo "${APP_NAME_WITH_ID} image 建立中"
                // sh "curl ${NOTIFY_API} -d '{\"content\": \"${APP_NAME_WITH_ID} image 建立中\"}'"

                // 顯示 git commit message
                script {
                    def changeLogSets = currentBuild.changeSets
                    def commitMessages = []
                    def maxMessages = 40
                    def maxMessageLength = 50
                    def maxMessageContentLength = 2000

                    for (int i = 0; i < changeLogSets.size(); i++) {
                        def entries = changeLogSets[i].items
                        for (int j = 0; j < entries.length && commitMessages.size() < maxMessages; j++) {
                            def entry = entries[j]
                            def msg = "${entry.msg}"
                            def author = "${entry.author}"
                            if (msg.length() > maxMessageLength) {
                                msg = msg.substring(0, maxMessageLength - 3) + '...'
                            }
                            commitMessages.add("${j + 1}. ${msg} by ${author}")
                        }
                    }

                    allCommitMessages = commitMessages.join("\n")
                    if (allCommitMessages.length() > maxMessageContentLength) {
                        allCommitMessages = allCommitMessages.substring(0, maxMessageContentLength - 3) + '...'
                    }
                    echo allCommitMessages

                    allCommitMessages = allCommitMessages.replaceAll('\n', '\\\\n').replaceAll('"', '')

                }
                sh(script: "curl ${NOTIFY_API} -d '{\"content\": \"${APP_NAME_WITH_ID} image 建立中\\n變更項目:\\n${allCommitMessages}\"}'")

                echo 'save docker image'
                sh "podman save -o ${ARCHIVE_FILE_NAME}.tar ${APP_NAME}"

                echo 'gzip docker image'
                sh "gzip -c ${ARCHIVE_FILE_NAME}.tar > ${ARCHIVE_FILE_NAME}.tar.gz"

                echo 'archive docker image'
                archiveArtifacts artifacts: "${ARCHIVE_FILE_NAME}.tar.gz", followSymlinks: false, fingerprint: true

                 /* groovylint-disable-next-line LineLength */
                sh "curl ${NOTIFY_API} -d '{\"content\": \"${APP_NAME_WITH_ID} image 建立完成 版號: ${APP_VERSION}\",\"embeds\": [{\"image\": {\"url\": \"https://c.tenor.com/jq3TOO2LoS0AAAAd/tenor.gif\"}}]}'"
            }
        }
    }

    post {
        always {
            echo 'clear unused docker image'
            sh 'podman image prune -f'

            echo 'clear workspace'
            cleanWs()
        }

        // 錯誤處理
        failure {
            script {
                if (GIT_BRANCH ==~ /(origin\/)?(pre-develop)/) {
                    echo "${APP_NAME_WITH_ID} 上版失敗"
                    /* groovylint-disable-next-line LineLength */
                    sh "curl ${NOTIFY_API} -d '{\"content\": \"${APP_NAME_WITH_ID} 上版失敗 版號: ${APP_VERSION} job-link: ${JOB_LINK}\",\"embeds\": [{\"image\": {\"url\": \"https://media.tenor.com/wFUJx3u1I3MAAAAC/kid-scared.gif\"}}]}'"
                }

                if (GIT_BRANCH ==~ /(origin\/)?(master|release|develop)/) {
                    echo "${APP_NAME_WITH_ID} image 建立失敗"
                    /* groovylint-disable-next-line LineLength */
                    sh "curl ${NOTIFY_API} -d '{\"content\": \"${APP_NAME_WITH_ID} image 建立失敗 版號: ${APP_VERSION} job-link: ${JOB_LINK}\",\"embeds\": [{\"image\": {\"url\": \"https://media.tenor.com/wFUJx3u1I3MAAAAC/kid-scared.gif\"}}]}'"
                }
            }
        }
    }
}

/* groovylint-disable-next-line MethodParameterTypeRequired, NoDef */
String getBuildEnv(branch) {
    /* groovylint-disable-next-line NoDef, VariableTypeRequired */
    def env = ''
    switch (branch) {
        case ~/(origin\/)?master$/:
            env = 'production'
            break
        case ~/(origin\/)?release$/:
            env = 'uat'
            break
        case ~/(origin\/)?pre-develop$/:
            env = 'tpi'
            break
        default:
            env = 'unknown'
            break
    }
    return env
}

```

### 設定Pipeline Script參數(opms_back_end)
設定Pipeline Script  
輸入Repo URL  
![](./images/Jenkins設定Pipeline-Script(backend).png)

撰寫Pipeline Script
```yaml
pipeline {
    agent any
    
    environment {
        APP_NAME = 'opms-back-end'
        GIT_BRANCH = "${params.BRANCH}"
        IMAGE_NAME = getImageName(GIT_BRANCH)
        CONTAINER_NAME = 'opms-back-end'
        IS_SCAN = "${params.IS_SCAN}"
        BUILD_ENV = getBuildEnv(GIT_BRANCH)
        APP_NAME_WITH_ID = "${APP_NAME}:${GIT_BRANCH}-${env.BUILD_NUMBER}"
        ARCHIVE_FILE_NAME = "${APP_NAME}_${GIT_BRANCH}_${env.BUILD_NUMBER}"
        SONAR_HOST_URL = 'http://10.20.30.199:9000/'
        SONAR_AUTH_TOKEN = 'sqa_ed44ac50393808d5e1ec2bb2d3c23a90e09fe2d1'
        NOTIFY_API = "https://discord.com/api/v9/channels/1101147645004742737/messages -H 'Content-Type: application/json' -H 'Authorization: Bot MTA0ODE3MDcxMTEzMjQwOTkzNg.GGGfaB.i16Z2Cxx9JGDiMh3uYkY2PVaNTe9U-Btte3mPU'"
        allCommitMessages = ""
    }
    
    stages {
        stage('pull') {
            steps {
                echo 'bitbucket pull'
                // Get some code from a GitHub repository
                git branch: "${GIT_BRANCH}", credentialsId: '52be6499-ea2e-4488-b946-1e6884bba357', url: 'https://jerryyang66@bitbucket.org/tpowerapp/opms-back-end.git'
            }
        }
        stage('start') {
            when {
                expression {
                    return GIT_BRANCH ==~ /(origin\/)?(pre-develop)/
                }
            }
            steps {
                echo "${APP_NAME_WITH_ID} 上版中"
                
                // 顯示 git commit message
                script {
                    def changeLogSets = currentBuild.changeSets
                    def commitMessages = []
                    def maxMessages = 40
                    def maxMessageLength = 50
                    def maxMessageContentLength = 2000

                    for (int i = 0; i < changeLogSets.size(); i++) {
                        def entries = changeLogSets[i].items
                        for (int j = 0; j < entries.length && commitMessages.size() < maxMessages; j++) {
                            def entry = entries[j]
                            def msg = "${entry.msg}"
                            def author = "${entry.author}"
                            if (msg.length() > maxMessageLength) {
                                msg = msg.substring(0, maxMessageLength - 3) + '...'
                            }
                            commitMessages.add("${j + 1}. ${msg} by ${author}")
                        }
                    }

                    allCommitMessages = commitMessages.join("\n")
                    if (allCommitMessages.length() > maxMessageContentLength) {
                        allCommitMessages = allCommitMessages.substring(0, maxMessageContentLength - 3) + '...'
                    }
                    echo allCommitMessages

                    allCommitMessages = allCommitMessages.replaceAll('\n', '\\\\n')

                }
                sh(script: "curl ${NOTIFY_API} -d '{\"content\": \"${APP_NAME_WITH_ID} 上版中\\n變更項目:\\n${allCommitMessages}\"}'")
                //sh "curl ${NOTIFY_API} -d '{\"content\": \"${APP_NAME_WITH_ID} 上版中\",\"embeds\": [{\"image\": {\"url\": \"https://media.tenor.com/pRIi24X3tAQAAAAC/tkthao219-capoo.gif\"}}]}'"
            }
        }
        stage("build") {
            steps {
                script {
                    echo 'get git commit hash'
                    env.gitTag = sh (script: 'git rev-parse --short HEAD', returnStdout: true).trim()


                    echo 'build image'
                    sh "podman build -t ${IMAGE_NAME} -f ./opms/Dockerfile --build-arg BUILD_ENV=${BUILD_ENV} --build-arg GIT_COMMIT_HASH=${gitTag} ."
                    
                    //sh "podman logs ${CONTAINER_NAME}"
                }
            }
        }
        stage('deploy'){
            when {
                expression {
                    return GIT_BRANCH ==~ /(origin\/)?(pre-develop)/
                }
            }
            steps {
                 script {
                    echo "delete old container..."
                    sh "podman rm -f ${CONTAINER_NAME}"

                    echo "pwd"
                    sh "pwd"
                    sh "ls"

                    echo "run container"
                    // sh 'podman run -e "SPRING_PROFILES_ACTIVE=${BUILD_ENV}" -it -uroot -d --name ${APP_NAME} -p 8091:8080 ${APP_NAME}  '
                    sh 'podman run -e "spring.profiles.active=dev" -it -uroot -d -v /OPMS_TEST:/OPMS_SHARE:rshared -v /OPMS_LOG:/OPMS_LOG:z --name ${CONTAINER_NAME} -p 8090:8080 -p 8443:443 ${IMAGE_NAME} '
                 }
            }
        }
        stage('scan') {
            when { 
                expression { return params.IS_SCAN }
            }
            steps {
                echo 'start sonar-scanner'
                script {
                    if (GIT_BRANCH ==~ /(origin\/)?(pre-develop)/) {
                        sh 'podman build --rm -f ./opms/Dockerfile.SonarScanner -t sonar-scanner:latest .'
                        sh "podman run --rm -t sonar-scanner:latest -Dsonar.projectKey=${APP_NAME}_pre-develop -Dsonar.projectName=${APP_NAME}_pre-develop -Dsonar.projectVersion=$ARCHIVE_FILE_NAME -Dsonar.host.url=$SONAR_HOST_URL -Dsonar.token=$SONAR_AUTH_TOKEN"
                    }
                    if (GIT_BRANCH ==~ /(origin\/)?(release)/) {
                        sh 'podman build --rm -f ./opms/Dockerfile.SonarScanner -t sonar-scanner:latest .'
                        sh "podman run --rm -t sonar-scanner:latest -Dsonar.projectKey=${APP_NAME}_release -Dsonar.projectName=${APP_NAME}_release -Dsonar.projectVersion=$ARCHIVE_FILE_NAME -Dsonar.host.url=$SONAR_HOST_URL -Dsonar.token=$SONAR_AUTH_TOKEN"
                    }
                    if (GIT_BRANCH ==~ /(origin\/)?(master)/) {
                        sh 'podman build --rm -f ./opms/Dockerfile.SonarScanner -t sonar-scanner:latest .'
                        sh "podman run --rm -t sonar-scanner:latest -Dsonar.projectKey=${APP_NAME}_master -Dsonar.projectName=${APP_NAME}_master -Dsonar.projectVersion=$ARCHIVE_FILE_NAME -Dsonar.host.url=$SONAR_HOST_URL -Dsonar.token=$SONAR_AUTH_TOKEN"
                    }
                }
            }
        }
        stage('Archive image') {
            when {
                expression {
                    return GIT_BRANCH ==~ /(origin\/)?(master|release|release2|develop|master-test)/
                }
            }

            steps {
                echo "${APP_NAME_WITH_ID} image 建立中"
                // 顯示 git commit message
                script {
                    def changeLogSets = currentBuild.changeSets
                    def commitMessages = []
                    def maxMessages = 40
                    def maxMessageLength = 50
                    def maxMessageContentLength = 2000

                    for (int i = 0; i < changeLogSets.size(); i++) {
                        def entries = changeLogSets[i].items
                        for (int j = 0; j < entries.length && commitMessages.size() < maxMessages; j++) {
                            def entry = entries[j]
                            def msg = "${entry.msg}"
                            def author = "${entry.author}"
                            if (msg.length() > maxMessageLength) {
                                msg = msg.substring(0, maxMessageLength - 3) + '...'
                            }
                            commitMessages.add("${j + 1}. ${msg} by ${author}")
                        }
                    }

                    allCommitMessages = commitMessages.join("\n")
                    if (allCommitMessages.length() > maxMessageContentLength) {
                        allCommitMessages = allCommitMessages.substring(0, maxMessageContentLength - 3) + '...'
                    }
                    echo allCommitMessages

                    allCommitMessages = allCommitMessages.replaceAll('\n', '\\\\n')

                }
                
                sh(script: "curl ${NOTIFY_API} -d '{\"content\": \"${APP_NAME_WITH_ID} image 建立中\\n變更項目:\\n${allCommitMessages}\"}'")
           
                //sh "curl ${NOTIFY_API} -d '{\"content\": \"${APP_NAME_WITH_ID} image 建立中\"}'"

                echo 'save docker image'
                sh "podman save -o ${ARCHIVE_FILE_NAME}.tar ${IMAGE_NAME}"

                echo 'gzip docker image'
                sh "gzip -c ${ARCHIVE_FILE_NAME}.tar > ${ARCHIVE_FILE_NAME}.tar.gz"

                echo 'archive docker image'
                archiveArtifacts artifacts: "${ARCHIVE_FILE_NAME}.tar.gz", followSymlinks: false, fingerprint: true

                /* groovylint-disable-next-line LineLength */
                sh "curl ${NOTIFY_API} -d '{\"content\": \"${APP_NAME_WITH_ID} image 建立完成 GitTag: ${gitTag}\",\"embeds\": [{\"image\": {\"url\": \"https://media.tenor.com/yTs3fJHYLI0AAAAC/pikachu-yeah.gif\"}}]}'"
            }
        }
        stage('clean'){
            steps {
                cleanWs()
                sh "podman image prune -f"
            }
        }
        stage('end') {
            when {
                expression {
                    return GIT_BRANCH ==~ /(origin\/)?(pre-develop)/
                }
            }
            steps {
                echo "${APP_NAME_WITH_ID} 上版結束"
                sh "curl ${NOTIFY_API} -d '{\"content\": \"${APP_NAME_WITH_ID} 上版完成 GitTag: ${gitTag}\",\"embeds\": [{\"image\": {\"url\": \"https://2.bp.blogspot.com/-RwKl7rSsJ7o/WD6hruu-KkI/AAAAAAALo9o/JdUdt-aBuI4SFxOFUK2rhsdPuX1yR9TwwCLcB/s1600/AS002044_03.gif\"}}]}'"
            }
        }
    }
    post {
        // 錯誤處理
        failure {
            script {
                if (GIT_BRANCH ==~ /(origin\/)?(pre-develop)/) {
                    echo "${APP_NAME_WITH_ID} 上版失敗"
                    sh "curl ${NOTIFY_API} -d '{\"content\": \"${APP_NAME_WITH_ID} 上版失敗 gitTag: ${gitTag} job-link: ${JOB_LINK}\",\"embeds\": [{\"image\": {\"url\": \"https://tenor.com/zh-TW/view/bugcat-gif-13001670\"}}]}'"
                }
    
                if (GIT_BRANCH ==~ /(origin\/)?(master|release|develop)/) {
                    echo "${APP_NAME_WITH_ID} image 建立失敗"
                    sh "curl ${NOTIFY_API} -d '{\"content\": \"${APP_NAME_WITH_ID} image 建立失敗 gitTag: ${gitTag} job-link: ${JOB_LINK}\",\"embeds\": [{\"image\": {\"url\": \"https://tenor.com/zh-TW/view/bugcat-gif-13001670\"}}]}'"
                }
            }
        }        
    }
}

String getBuildEnv(branch) {
    /* groovylint-disable-next-line NoDef, VariableTypeRequired */
    def env = ''
    switch (branch) {
        case ~/(origin\/)?develop$/:
            env = 'sit'
            break
        case ~/(origin\/)?master$/:
            env = 'prod'
            break
        case ~/(origin\/)?master-test$/:
            env = 'prod-test'
            break
        case ~/(origin\/)?release$/:
            env = 'uat'
            break
        case ~/(origin\/)?release2$/:
            env = 'uat'
            break
        case ~/(origin\/)?pre-develop$/:
            env = 'dev'
            break
        default:
            env = 'unknown'
            break
    }
    return env
}

String getImageName(branch) {
    def imageName = ''
    switch (branch) {
        case ~/(origin\/)?master-test$/:
            imageName = 'opms-back-end2'
            break
        default:
            imageName = 'opms-back-end'
            break
    }
    return imageName
}
```