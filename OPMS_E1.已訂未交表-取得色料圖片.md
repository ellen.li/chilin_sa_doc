# 已訂未交表-取得色料圖片
###### tags: `OPMS`
[返回總覽](./OPMS_00.總覽.md)  

**參考API通用格式 : [OPMS_REF_API通用格式.md](./OPMS_REF_API通用格式.md)**

## 功能說明:

取得色料圖片

## 修改歷程

| 修改時間 | 內容     | 修改者 |
| -------- | -------- | ------ |
| 20230829 | 新增規格 | Ellen  |



## 來源URL及資料格式

| 項目   | 說明         |
| ------ | ------------ |
| URL    | /pattern/img |
| method | post          |

## Header 定義

Header 欄位參考 [API通用格式-通用Header定義](./OPMS_REF_API通用格式.md#通用Header定義)


# Request 欄位
M=Mandatory，表示該欄位為必填；O=Optional，表示該欄位為選填，非必填節點不需存在。  
| 欄位        | 名稱 | 資料型別 | 必填  | 來源資料 & 說明 |
| ----------- | ---- | -------- | :---: | --------------- |
| opPatternNo | 色號 | string   |   M   |                 |



#### Request 範例

```json
{
   "opPatternNo": "12341B5"
}
```

# Request 後端流程說明

可參考舊版 PatternManageAction.showPatternImg
* 檢核必填欄位，非必填未給，則給預設值(檢查不通過 [return 400,VALIDATION_ERROR,欄位相關訊息])
* [OPMS_REF_雜項資料.md](./OPMS_REF_雜項資料.md#參數設定檔) 讀取色料圖片 
* 若檔案存在 `pattern.file.root/{opPatternNo} + ".JPG"` 回傳圖片內容
* 若檔案存在 `pattern.file.root/{opPatternNo} + "-C.JPG"` 回傳圖片內容
* 若檔案都不存在 [return 404,NOTFOUND]
* 有系統錯誤 return 500,"SYSTEM_ERROR",正常回傳 200，返回 Response內容

# Request 後端邏輯說明


# Response

```
Content-Type: image/jpeg
Content-Disposition: inline;filename={圖片檔名}
Content-Length: {圖片大小}
Cache-Control: no-cache
```

#### Response 範例
